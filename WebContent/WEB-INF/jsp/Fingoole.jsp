<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<title>Fingoole</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" crossorigin="anonymous">

	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/fingoole.css">
</head>
<body>

	<!-- header Section -->
	<!-- <nav class="navbar navbar-expand-lg navbar-light ">
	  	<div class="container">
	  		<a class="navbar-brand" href="#"><img src="logo.png" alt="Fingoole" style="width: 128px;"></a>
	  	</div>
	</nav> -->

	<!-- body Section -->
	<section>
		<div class="container">
			<!-- Pricing Section -->
			<div class="row">
				<!-- Pricing Item Start -->
				<div class="col-lg-4 col-md-6 col-sm-12">
					<div class="pricing_item">
						<div class="pricing_header">
							<h3 class="pricing_title">Plan 1</h3>
						</div>
						<div class="pricing_body">
							<div class="price_wrapper">
								<span class="currency">₹</span>
								<span class="price">2 Lakh</span>
							</div>
							<ul class="list">
								<li class="active">Personal Accident Death & Permanent Total Disability ₹ 2 Lakh</li>
								<li class="active">Hospitalization/Medical Expenses due to personal Injury up to ₹ 50,000/- within SI of ₹ 2 Lakh.</li>
								<li class="active">Evacuation, Repatriation upto ₹ 2,000/- within SI of ₹ 2 Lakh</li>
								<li class="active">Accident OPD Expense Up to ₹ 2,500/- within SI of ₹ 2 Lakh (Max-21 Days)</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- Pricing Item End -->

				<!-- Pricing Item Start -->
				<div class="col-lg-4 col-md-6 col-sm-12">
					<div class="pricing_item">
						<div class="pricing_header">
							<h3 class="pricing_title">Plan 2</h3>
						</div>
						<div class="pricing_body">
							<div class="price_wrapper">
								<span class="currency">₹</span>
								<span class="price">5 Lakh</span>
							</div>
							<ul class="list">
								<li class="active">Personal Accident Death & Permanent Total Disability ₹ 5 Lakh </li>
								<li class="active">Hospitalization/Medical Expenses due to personal Injury up to ₹ 1 Lakh within SI of ₹ 5 Lakh</li>
								<li class="active">Evacuation, Repatriation upto ₹ 5,000/- within SI of ₹ 5 Lakh</li>
								<li class="active">Accident OPD Expense up to ₹ 5,000/- within SI of ₹ 5 Lakh (Max-21 Days)</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- Pricing Item End -->

				<!-- Pricing Item Start -->
				<div class="col-lg-4 col-md-6 col-sm-12">
					<div class="pricing_item">
						<div class="pricing_header">
							<h3 class="pricing_title">Plan 3</h3>
						</div>
						<div class="pricing_body">
							<div class="price_wrapper">
								<span class="currency">₹</span>
								<span class="price">10 Lakh</span>
							</div>
							<ul class="list">
								<li class="active">Personal Accident Death &amp; Permanent Total Disability ₹ 10 Lakh </li>
								<li class="active">Hospitalization/Medical Expenses due to personal Injury up to ₹ 2 Lakh within SI of ₹ 10 Lakh.</li>
								<li class="active">Evacuation, Repatriation upto ₹ 10,000/- within SI of ₹ 10 Lakh</li>
								<li class="active">Accident OPD Expense Up to ₹ 10,000/- within SI of ₹ 10 Lakh (Max-21 Days)</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- Pricing Item End -->
			</div>

			<div class="row">
				<div class="col-12">
					<div id="sticky-anchor"></div>
					<ul class="nav nav-pills nav-justified mb-3 menu" id="pills-tab" role="tablist">
					  	<li class="nav-item">
					    	<a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#safety_policy" role="tab" aria-controls="pills-home" aria-selected="true">Customer Safety Policy</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#claim_process" role="tab" aria-controls="pills-profile" aria-selected="false">Claim Process</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#doc_required" role="tab" aria-controls="pills-contact" aria-selected="false">Claim Document Requirements</a>
					  	</li>
					</ul>
					<div class="tab-content" id="pills-tabContent">
					  	<div class="tab-pane fade show active" id="safety_policy" role="tabpanel" aria-labelledby="pills-home-tab">
					  		<div class="fig_wrapper">
					  			<div class="fig_heading">
						  			<h5 class="title" style="text-transform: uppercase;">Customer safety policy</h5>
						  		</div>
						  		<div class="fig_body">
						  			<strong>IMPORTANT</strong>
						  			<p class="text-justify">
						  				CUSTOMER'S SAFETY POLICY is designed for large chain of merchants i.e. Cab Aggregator, Bus Aggregator, Telecom Aggregators (Outlets), Hotels, Resorts, Online Travel Booking Sites, Amusement Park, Logistic, Courier, Food Aggregators, Corporates, Cooperative Societies, Event Aggregators, Payment Banks, Microfinance Company and/or any type of other aggregators, Fingoole On-Demand Insurance App users to make the loss of its ultimate customers good resulting due to Personal Accident at any time during the currency of certificate of insurance issued to them, subject to terms, conditions, exclusions and limitations as laid down hereunder.
						  			</p>

						  			<p class="text-justify">
						  				PLEASE MAKE SURE TO HAVE READ AND FULLY UNDERSTOOD THIS POLICY DOCUMENT FOR BEING AWARE WITH THE FULL DETAILS OF PROCEDURE AND OBTAINING ASSISTANCE AND CLAIM. <b>FAILURE TO FOLLOW THE INSTRUCTIONS GIVEN SHALL RESULT IN REJECTION OF CLAIM</b>.
						  			</p>

						  			<strong>THE POLICY</strong>
						  			<p class="text-justify">WHEREAS THE INSURED PERSON designated in the schedule of Certificate of Insurance hereto having applied to United India Insurance Company Limited (hereinafter called the Insurer or the Underwriter or the Company) for insurance benefits set forth and having paid the premium for insurance as specified hereunder for the number of days and customers stated in the Policy Schedule or endorsed thereon time to time.</p>

						  			<span class="mb-1" style="display: block;">Now this policy provides as follows:</span>

						  			<h5>DEFINITIONS</h5>
						  			<p class="text-justify">The following definitions apply throughout this insurance policy and you must understand the meaning of such terms clearly.</p>

						  			<ul>
						  				<li class="text-justify"><strong>TECHNOLOGY PROVIDER</strong> means M/s. Fingoole Technologies Pvt. Ltd. who are providing a digital platform to Insurance Company, different merchants and ultimate customers for monitoring the policy i.e. collecting customer enrolment data from merchants, Online or Offline agents, providing it to Insurance Company and assisting in claim service to the customers for their intended event.</li>
						  				<li class="text-justify"><strong>ACCIDENT</strong> means a sudden, unforeseen and involuntary event caused by external, visible and violent means to cause an injury or disablement.</li>
						  				<li class="text-justify"><strong>PERMANENT TOTAL DISABLEMENT</strong> means a condition wherein the insured person is permanently, totally and absolutely disabled from engaging in any employment or occupation of whatsoever description.</li>
						  				<li class="text-justify"><strong>LOSS OF EYE</strong> means the total and irrecoverable loss of sight from one or both eyes.</li>
						  				<li class="text-justify"><strong>LOSS OF LIMB</strong> means the loss of a hand or foot by permanent physical severances at or above the wrist or ankle including total and permanent loss of use of a hand or foot.</li>
						  				<li class="text-justify"><strong>PHYSICIAN</strong> means a person legally qualified to practice in medicine or surgery including other legally qualified medical practitioner duly licensed by their respective jurisdiction and which person is not a member of the insured person's family.</li>
						  				<li class="text-justify"><strong>INSURED PERSON/CUSTOMER</strong> means the Insured person up to the age of 75 years, who permanently resides in India or has come to India for a short visit or the eligible spouse and/or eligible children and is named in the policy schedule as being eligible to become insured under this policy.</li>
						  				<li class="text-justify"><strong>MEDICAL RELATED EXPENSES REASONABLY AND NECESSARILY INCURRED</strong> means expenses that in the opinion of the treating physician are medically necessary in order to maintain life and/or relieve immediate pain or distress or illness or repair of injuries sustained in an accident first manifested/occurring during the period of insurance.</li>
						  			</ul>

						  			<strong>PERIOD OF INSURANCE</strong>
						  			<p class="text-justify">This insurance is valid from the First Day of Insurance specified on the schedule till the expiry date and time as specified in the certificate of insurance issued to customer through Fingoole's App/ API.</p>

						  			<h5>GENERAL CONDITIONS:</h5>
						  			<p class="text-justify">The conditions below shall apply throughout this insurance. Failure to comply with them may be prejudicial to a claim.</p>
						  			<ul>
						  				<li class="text-justify">No cancellation of certificate of insurance by Insured Person is permissible under any circumstances and hence Insurance Company shall not allow any refund of premium once it is received.</li>
						  				<li class="text-justify">It is a condition precedent to liability hereunder that in the event of any occurrence likely to give rise to a claim under this Insurance, that the Insured Person/ Customer, or his representative, must notify "Fingoole" or Insurance Company immediately according to the procedure laid down under the head "Claims Procedure" The customer or his representative should quote as much information concerning the occurrence of claim as is available, including the name of the treating doctor, name and telephone number of the hospital, the insurance certificate number and its date of issue. This document, together with Invoices, and any other relevant details/ documents must be uploaded on Fingoole's App clearly visible. Please note that if medical treatment has been received, medical certificates showing the nature of the injury together with all bills, and receipts if already paid, must be uploaded and provided to Fingoole.</li>
						  				<li class="text-justify">If any new illness/injury/accident is contracted beyond the expiry date of the policy, treatment for the same would not be covered.</li>
						  				<li class="text-justify">Only those expenses which are incurred within India will be covered.</li>
						  				<li class="text-justify">The disease or injury for which claim has been preferred under any policy, shall be treated as pre-existing disease/ injury for any renewed or extended policy and no payment shall be made under any such renewed or extended policy.</li>
						  				<li class="text-justify">Insurers shall be fully and completely subrogated to the rights of the Insured Person against parties who may be liable to provide indemnity or make a contribution in respect of any matter which is the subject of a claim under this insurance. The Insured Person further agrees to co-operate fully with insurers in seeking such indemnity or contribution including where appropriate insurers instituting proceedings at their own expense against such parties in the name of the Insured Person.</li>
						  				<li class="text-justify">The Insurers may require the Insured Person to furnish at his own expense all certificates, information, proofs or other evidence of claims. The insurers may approach any physician who might have treated the Insured Person, and the Insured Person must co-operate in this respect.</li>
						  				<li class="text-justify">The Insured Person shall take all reasonable and proper care to safeguard against any accident as if this insurance is not in force. Failure to do so will prejudice the Insured Person's claim under this insurance.</li>
						  				<li class="text-justify">The Insured Person cannot transfer his interest in this Insurance. However, the legal representatives (Firstly Spouse, secondly children and lastly the parents only) of the Insured Person shall have the right to act for and on behalf of the Insured Person who is Incapacitated or deceased. No other person shall be entitled to receive the benefit under the policy.</li>
						  				<li class="text-justify">This policy and the Customer's Safety Policy Schedule shall be read together as one contract and any wording or expression to which a specific meaning has been attached in any part of the Policy and Schedule shall bear such specific meaning wherever it may appear.</li>
						  				<li>The Company may at any time cancel the Policy by sending the Insured 30 days notice by registered letter at Insured's last known address and in such event the Company shall refund to the Insured the unutilized premium for remaining period.</li>
						  				<li class="text-justify">Dispute resolution clause and procedure: This Contract of Insurance includes the following dispute resolution procedure which is exclusive and a material part of this Contract of Insurance.</li>
						  				<li class="text-justify"><strong>Pre-existing Exclusions</strong> :This policy is not designed to provide an indemnity in respect of medical services arising out of a pre-existing condition as defined below in General Condition 12(b).</li>
						  				<li class="text-justify"><strong>Pre-existing condition</strong>: The pre-existing condition means any disability/ sickness or illness, which existed prior to the effective date of this insurance including whether or not the insured person had knowledge that symptoms were related to the sickness / illness. Complication arising from a pre-existing condition will also be considered part of the pre-existing condition.</li>
						  				<li class="text-justify"><strong>Choice of Law</strong>: The parties to this insurance policy expressly agree that the laws of the Republic of India shall govern the validity, construction, interpretation and effect of this policy.</li>
						  				<li class="text-justify"><strong>Arbitration</strong>: If any dispute or difference shall arise as to the quantum to be paid under the Policy (liability being otherwise admitted) such difference shall independently of all other questions be referred to the decision of the sole arbitrator to be appointed in writing by the parties to or if they cannot agree upon a single arbitrator within 30 days of any party invoking arbitration the same shall be referred to the panel of three arbitrators, comprising of two arbitrators, one to be appointed by each of the parties to the dispute/difference and the third arbitrator to be appointed by such two arbitrators and arbitration shall be conducted under and in accordance with the provisions of the Arbitration and Conciliation Act, 1996.</li>
						  				<p class="text-justify">It is clearly agreed and understood that no difference or dispute shall be referable to arbitration as herein before provided, if the Company has disputed or not accepted liability under or in respect of this policy.</p>
						  				<p class="text-justify">It is hereby expressly stipulated and declared that is shall be a condition preceden to any right of action or suit upon this policy that award by such arbitrator/arbitrators of the amount of the loss or damage shall be first obtained.</p>
						  				<li class="text-justify">It is also hereby further expressly agreed and declared that if the Company shall disclaim liability to the insured for any claim hereunder and such claim shall not within 12 calendar months from the date of such disclaimer have been made the subject matter of a suit in the court of law, then the claim shall for all purposes be deemed to have been abandoned and shall not thereafter be recoverable hereunder.</li>
						  				<li class="text-justify">Any claim under this policy that is fraudulent or if fraudulent means are used to secure payment of benefits under the certificate of insurance issued to Insured Person, then such action shall render his/her COI null and void and all claims hereunder shall be forfeited.</li>
						  				<li class="text-justify">No sum payable under this policy shall carry interest.</li>
						  				<li class="text-justify">In the event of the Insured Person's death, Insurers shall have the right to ask for a post mortem report at the expenses of insured.</li>
						  				<li class="text-justify">Any claim which has not been conclusively proven and the amount thereof substantiated shall not be payable.</li>
						  				<li class="text-justify">All covers under the policy shall continue to be in force till the expiry date of certificate of insurance provided the same have been incepted within policy period as issued by the insurers.</li>
						  			</ul>
						  			<h5>GENERAL EXCLUSIONS:</h5>
						  			<ul>
						  				<li class="text-justify">No claim will be paid where the Insured Person is under treatment or on a waiting list for specified medical treatment covered under this policy as declared in the Physician's report or certificate at the time of insurance.</li>
						  				<li class="text-justify">No claim will be paid arising from suicide, attempted suicide or wilfully self-inflicted injury or illness, mental disorder, anxiety, stress or depression, venereal disease, alcoholism, drunkenness or the abuse of the drugs, or any loss arising directly or indirectly from any injury, illness, death, loss, expenses, or other liability attributable to HIV (Human Immunodeficiency Virus) and/or any HIV related illness Including AIDS (Acquired Immune Deficiency Syndrome) and/or any mutant derivative or variation thereof however caused.</li>
						  				<li class="text-justify">No claim will be paid arising from the insured person taking part in Naval, Military or Air force operations.</li>
						  				<li class="text-justify">No claim will be paid arising from War, invasion, acts of foreign enemy, hostilities (Whether war be declared or not), civil war, rebellion, revolution, insurrection, military or usurped power or confiscation or nationalisation or requisition of or destruction of or damage to property by or under the order of any government or local authority.</li>
						  				<li class="text-justify">This insurance does not cover any claim arising from or any consequential loss directly or indirectly caused by or contributed to by :</li>
						  				<li class="text-justify">ionizing radiation or contamination by radioactivity from any nuclear waste from the combustion of nuclear fuel; or</li>
						  				<li class="text-justify">The radioactive, toxic, explosive or other hazardous properties of any explosive nuclear assembly or nuclear component thereof.</li>
						  				<li class="text-justify">No claim will be paid which arises from the insured Person engaging in Air Travel  unless he or she flies as a passenger on an aircraft properly licensed to carry passengers. For the purpose of this exclusion, Air Travel means being in or on, or boarding an aircraft for the purpose of flying therein or alighting therefrom following a flight.</li>
						  				<li class="text-justify">No claim will be paid arising from the participation of the Insured Person in winter sports, mountaineering (where ropes or guides are customarily used), riding or driving in races or rallies carving or potholing, hunting or equestrian, scuba diving or other underwater activity, rafting or canoeing involving white water rapids, yachting or boating outside coastal waters (2 miles). Further no claim will be paid in case Insured Person Participates in any adventures (professional sports or any other hazardous sports). The claim is neither payable if arises from participation in potentially dangerous sports for which the Insured Person is either untrained or physically unfit or using improper equipment.</li>
						  				<li class="text-justify">No claims will be paid for losses arising directly or indirectly from manual work or hazardous occupation, self-exposure to needless peril (except in an attempt to save human life) or if engaging in any criminal or illegal act.</li>
						  				<li class="text-justify">No claim shall be paid for any consequential results arising out of covered accident.</li>
						  			</ul>
						  			<strong>Specific Conditions</strong>
						  			<p class="text-justify">Proof satisfactory to the company shall be uploaded/ provided of all matters upon which a claim is based. Any medical or other agent of the Company shall be allowed to examine the Insured Person in the event of any alleged injury or disablement and so often as the same may reasonably be required on behalf of the Company. In the event of the death Insured or his representative shall allow for a post-mortem examination of the body of the deceased person. Such evidences and the post-mortem report as the Company may require shall be furnished within the period of fourteen days after the demand in writing and in the event of a claim in respect of loss of sight the Insured shall undergo at the Insured's expense such operation or treatment as the Company may reasonably deem desirable.</p>

						  			<h5>SCOPE OF THE POLICY</h5>
						  			<p class="text-justify">Now this policy witnesses that subject to the terms, conditions, exclusions and definitions, contained herein or endorsed or otherwise expressed hereon the Company will pay the sum or sums hereinafter set forth to the insured or his legal personal representative(s) , as the case may be, if at any time during the currency of this Policy the Insured of the age up to 75 years shall sustain any bodily injury resulting solely and directly from accident caused by external violent and visible means that is to say</p>

						  			<ul>
						  				<li class="text-justify">If such injury shall within twelve (12) calendar months of its occurrence be the sole and direct cause of the death of the Insured, the capital sum insured stated in the Schedule hereto.</li>
						  				<li class="text-justify">If such injury shall within twelve (12) calendar months of its occurrence be the sole and direct cause of the total and irrecoverable loss of:</li>
						  				<li class="text-justify">Sight of both eyes, or of the actual loss by physical separation of the two entire hands or two entire feet or one entire hand and one entire foot or of such loss of sight of one eye and such loss of one entire hand or one entire foot, the capital sum insured stated in the Schedule hereto.</li>
						  				<li class="text-justify">use of two hands or two feet, or of one hand and one foot or of such loss of sight of one eye and such loss of use of one hand or one foot the capital sum insured stated in the Schedule hereto.</li>
						  				<li class="text-justify">The sight of one eye or of the actual loss by physical separation of one entire hand or one entire foot, fifty present (50%) of the capital sum insured stated in the Schedule hereto.</li>
						  				<li class="text-justify">Total and irrecoverable loss of a hand or a foot without physical separation, fifty present (50%) of the capital sum insured stated in the Schedule hereto.</li>

						  				<p class="text-justify"><b>Note:</b> For the purpose of Clause (a) to Clause (d) above physical separation of a hand or foot means separation at or above the wrist and/or of the foot at or above the ankle respectively.</p>

						  				<li class="text-justify">If such injury shall as a direct consequence thereof, immediately permanently totally and absolutely, disable the Insured from engaging in any employment or occupation of any description whatsoever, a lump sum equal to hundred present (100%) of the capital sum insured.</li>
						  			</ul>

						  			<h5>EVACUATION AND REPATRIATION</h5>

						  			<strong>Nature of coverage:</strong>
						  			<ul>
						  				<li class="text-justify">Expenses of maximum of 1% of Total Sum Insured for physician ordered emergency medical evacuation, including medically appropriate transportation and necessary medical care in route, to the nearest suitable hospital when the insured Person is critically injured due to an accident and no suitable local care is available. In extreme emergency, in remote areas where the insurance company cannot be contacted the medical evacuation must be reported to the first available physician.</li>
						  				<li class="text-justify">Expenses of maximum of 1% of Total Sum Insured for medical evacuation, including transportation and medical care en-route to a hospital in the Insured Persons' normal place of residence in India when deemed medically advisable by the attending physician.</li>
						  				<li class="text-justify">If the insured Person dies while travelling and while being covered under the Customer Safety policy, the expenses of maximum of 1% of total Sum Insured for preparing the air / surface transportation of the remains for repatriation to place of residence of the deceased in India or up to an equivalent amount for a local burial or cremation in India where the death occurred.</li>
						  			</ul>

						  			<p class="text-justify"><b>Subject otherwise to the condition that total sum/s payable under the policy for one or more sections including evacuation and repatriation as described hereinabove shall not exceed the total sum insured as stated in the certificate issued by Fingoole.</b></p>

						  			<h5>EXPENSES FOR EMERGENCY ACCIDENTAL HOSPITALISATION</h5>

						  			<p class="text-justify">Insurer will pay the Reasonable and Customary Charges for Emergency Hospitalization Expenses incurred in the Republic of India by insured person for immediate medical services obtained consequent upon an <b>Accidental Injury</b> sustained during the period of policy by him, maximum up to the amount as specified in the schedule of certificate issued by Fingoole.</p>
						  			<p class="text-justify">For the purpose of availing claim for hospitalization benefit the Insured person has to confined in the hospital for minimum period of 24 hours with active treatment therein.</p>

						  			<h5>EXPENSES FOR EMERGENCY OUT PATIENT TREATMENT</h5>

						  			<p class="text-justify">In case of other than hospitalization, Insurer will pay the Reasonable and Customary Charges for such Expenses on OPD treatment incurred in the Republic of India by insured for immediate medical services obtained consequent upon an <b>Accidental Injury</b> sustained by him during the period of policy maximum up to the amount as specified in the schedule of certificate issued by Fingoole and that too for a period not exceeding 21 days from the date of accident.</p>

						  			<h5>SPECIFIC EXCLUSIONS</h5>
						  			<strong>The Company shall not be liable under this Policy for:-</strong>
						  			<ul>
						  				<li class="text-justify">Compensation under more than one of the foregoing clauses 1 to 3 above, in respect of same period of disablement.</li>
						  				<li class="text-justify">Any other payment after a claim under any one of the clauses 1 to 3 above has been admitted and become payable.</li>
						  				<li class="text-justify">Any payment in case of more than one claim under the Policy during the period of insurance by which the maximum liability of the Company in that period would exceed the sum payable under clause 1 of this policy.</li>
						  				<li class="text-justify">Payment of compensation in respect of Death, injury or Disablement of the Insured (a) from intentional self-injury, suicide or attempted suicide (b) whilst under the influence of intoxicating liquor or drugs (c) directly or indirectly caused by venereal or insanity disease/s and (d) arising or resulting from the Insured committing any breach of the law with criminal intent.</li>
						  				<li class="text-justify">No claim shall be payable if the age of insured exceeds 75 years at the time of commencement of policy.</li>
						  				<li class="text-justify"><b>Pregnancy Exclusion Clause :</b> The Insurance under this Policy shall not extend to cover death or disablement resulting directly or indirectly caused by, contributed to or prolonged by childbirth or pregnancy or in consequence thereof.</li>
						  			</ul>

						  			<p class="text-justify">Provided also that the due observance and fulfilment of the terms and conditions of this Policy (which conditions and all endorsements hereon are to be read as part of this Policy) shall so far as they relate to anything to be done or not to be done by the Insured be a condition precedent to any liability of the Company under this policy.</p>

						  			<h5>CLAIMS PROCEDURE.</h5>
						  			<p class="text-justify">On happening of any event during the currency of the policy which may give rise to a claim, the following procedure shall be followed:</p>

						  			<ul>
						  				<li class="text-justify">Fingoole shall maintain all details of reported claims in its software and shall provide to Insurance Company immediately.</li>
						  				<li class="text-justify">On happening of any event the Insured person has to register the claim in the App provided by the Fingoole within 24 hours of its happening. However in case of Death or disability of Insured Person, the intimation may be given maximum within a period of 14 days of happening the death or disability.</li>
						  				<li class="text-justify">Fingoole shall intimate the loss to Insurance Company maximum within 24 working hours of the receipt of intimation from customer through its dashboard.</li>
						  				<li class="text-justify">The Insured person shall be required to upload the desired documents within 14 days of the claim intimation or any such extended period as allowed by the Insurance Company.</li>
						  				<li class="text-justify">For the claims exceeding limit of Rs.20,000/- Company's appointed surveyor shall verify the documents and shall submit their report to insurance company recommending the loss within 7 days of submission of all documents.</li>
						  				<li class="text-justify">For the claims below limit of Rs. 20,000/- company's may its discretion that appoint the surveyor on case to case basis.</li>
						  			</ul>
						  		</div>
					  		</div>
					  	</div>
					  	<div class="tab-pane fade" id="claim_process" role="tabpanel" aria-labelledby="pills-profile-tab">
					  		<div class="fig_wrapper">
					  			<div class="fig_heading">
					  				<h5 class="title" style="text-transform: uppercase;">Claim Process Flow chart for Customers</h5>
					  			</div>
					  			<div class="fig_body">
					  				<em class="text-justify">In case of any happening of any event covered under the policy/ polices, following system is to be followed within stipulated time limit:</em><br>

					  				<strong>Step 1</strong>
					  				<ul>
					  					<li class="text-justify">For Claim Intimation please call on 9699 400 500 or send mail to <a href="mailto:support@fingoole.in">support@fingoole.in</a></li>
					  					<li class="text-justify">Describe your Certificate of Insurance Number, Date & Place of incidence and Section covered</li>
					  				</ul>
					  				<hr>
					  				<strong>Step 2</strong>
					  				<ul>
					  					<li class="text-justify">Confirmed having the claim intimate from Fingoole within 12 hours</li>
					  					<li class="text-justify">Fingoole shall raise query to confirm nature of claim within 24 hours of registration of claim.</li>
					  					<li class="text-justify">Customer shall be required to respond within 24 hours of raising the query.</li>
					  				</ul>
					  				<hr>
					  				<strong>Step 3</strong>
					  				<ul>
					  					<li class="text-justify">Send relevant within 7 days documents via email <a href="mailto:support@fingoole.in">support@fingoole.in</a></li>
					  					<li class="text-justify">For Personal Accident Claims under the head Hospitalization, the customer has to confirm the probable date of discharge from hospital or probable date of completion of treatment in case of OPD treatment.</li>
					  					<li class="text-justify">Fingoole shall inform the appointment of investigator/ surveyor if the claim warrants so.</li>
					  				</ul>
					  				<hr>
					  				<strong>Step 4</strong>
					  				<ul>
					  					<li class="text-justify">Fingoole shall examine the received claim documents and raise the query, if any.</li>
					  					<li class="text-justify">Fingoole shall provide the received documents to investigator/ surveyor, if required so.</li>
					  					<li class="text-justify">Fingoole shall confirm receipt of investigation/ surveyor report and raise further query in case such reports warrant so.</li>
					  				</ul>
					  				<hr>
					  				<strong>Step 5</strong>
					  				<ul>
					  					<li class="text-justify">Fingoole shall process the claim.</li>
					  					<li class="text-justify">Fingoole shall confirm the admissibility of claim and amount thereof on Insurance Company being satisfied with the documents.</li>
					  					<li class="text-justify">Fingoole shall then ask for Bank Details for making payment of the claim.</li>
					  					<li class="text-justify">Insurance Company will making payment of the claim.</li>
					  				</ul>
					  			</div>
					  		</div>
					  	</div>
					  	<div class="tab-pane fade" id="doc_required" role="tabpanel" aria-labelledby="pills-contact-tab">
					  		<div class="fig_wrapper">
					  			<div class="fig_heading">
					  				<h5 class="title">Claim Documents Requirement for Flexible Personal Accident Cover.</h5>
					  			</div>
					  			<div class="fig_body">
					  				<strong>Personal Accident Cover</strong><br>

					  				<ol type="a">
					  					<li>Date, Time and Place of Accident</li>
					  					<li>Cause of Accident</li>
					  					<li>
					  						Nature of Accident - Death or Disability<br>
					  						Email: Death Certificate with cause of Death<br>
					  						Email: Post Mortem Report
					  					</li>

					  					<strong>For Disability Claims:</strong>
					  					<li>Nature of Disability</li>
					  					<li>Name of Treating Doctor/Hospital</li>
					  					<li>Extent of Disability</li>
					  					<li>Documents to be Emailed</li>
					  					<ol type="i">
					  						<li>Doctor's Treatment Advice or Hospital Discharge Card</li>
					  						<li>Investigation Reports</li>
					  						<li>Disability certificate of a Specialist giving percentage of disability.</li>
					  						<li>Claim From with Hospital Sign &amp; Stamp.</li>
					  					</ol>
					  				</ol>

					  				<strong>For Medical expenses claims:</strong>
					  				<ol type="a">
					  					<li>Name of treating Doctor/Hospital</li>
					  					<li>Doctor Certificate</li>
					  					<li>Details of treatment received</li>
					  					<li>Documents to be Emailed</li>
					  					<ol type="i">
					  						<li>Copy of Doctor's Treatment Advice or Hospital Discharge Card</li>
					  						<li>Original Bills/ Invoice</li>
					  						<li>Investigation Reports</li>
					  						<li>Claim Form with Hospital Sign &amp; Stamp.</li>
					  					</ol>
					  				</ol>
					  			</div>
					  		</div>
					  	</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="foot_img">
						<img src="${pageContext.request.contextPath}/resources/assets/img/fingoole.png" alt="Fingoole">
					</div>
				</div>
			</div>
		</div>
	</footer>

	<!-- Scripts -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" crossorigin="anonymous"></script>

	<script>
		function sticky_relocate() {
			var window_top = $(window).scrollTop();
			var div_top = $('#sticky-anchor').offset().top;
			if (window_top > div_top) {
				$('.menu').addClass('sticky');
			} else {
				$('.menu').removeClass('sticky');
			}
		}

		$(function() {
			$(window).scroll(sticky_relocate);
			sticky_relocate();
		})
	</script>
</body>
</html>