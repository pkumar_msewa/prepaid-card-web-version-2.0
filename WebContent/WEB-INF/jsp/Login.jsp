<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%@page isELIgnored="false"%>
<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
	<meta Http-Equiv="Cache-Control" Content="no-cache">
    <meta Http-Equiv="Pragma" Content="no-cache">
    <meta Http-Equiv="Expires" Content="0">
	<sec:csrfMetaTags/>
<style>
.radio-pink-gap [type="radio"].with-gap:checked+label:before {
    border-color: #e91e63;
}
</style>

	<title>Login</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/linearicons/style.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="${pageContext.request.contextPath}/resources/assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="${pageContext.request.contextPath}/resources/assets/img/favicon.png">
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery/jquery.min.js"></script>
</head>
 <script type="text/javascript">
        window.history.forward();
        function noBack() {
            window.history.forward();
        }
    </script>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle">
				<div class="auth-box ">
					<div class="left">
						<div class="content">
							<div class="header">
								<div class="logo text-center"><img src="${pageContext.request.contextPath}/resources/assets/img/logo-dark.png" alt="Klorofil Logo"></div>
								<p class="lead">Login to your account</p>
							</div>
							<form id="formId" class="form-auth-small"  method="post" class="login">
								<div class="form-group">
									<label for="emailId" class="control-label sr-only">Email</label>
									<input type="email" name="username" class="form-control" id="emailId" value="" placeholder="Email">
								</div><p id="emailError"></p>
								<div class="form-group">
									<label for="passwordId" class="control-label sr-only">Password</label>
									<input type="password" name="password" class="form-control" id="passwordId" value="" placeholder="Password">
								</div><p id="passError"></p>
								<div >
								Select Login Type
									<div class="form-group radio-pink-gap ">
										<input name="loginWith" type="radio" class="with-gap"
											id="radio1" value="Admin/Home" checked> <label for="radio109">Admin</label>
										<input name="loginWith" type="radio" class="with-gap"
											id="radio2" value="Client/Home"> <label for="radio110">Client</label>
									</div><p id="errorId"></p>
								</div>
								<button type="button" onclick="validation();" class="btn btn-primary btn-lg btn-block">LOGIN</button>
								<p style="color: red; font-size: 20px">${sessionScope.loginMsg}</p>
								<!-- <div class="bottom">
									<span class="helper-text"><i class="fa fa-lock"></i> <a href="#">Forgot password?</a></span>
								</div> -->
							</form>
						</div>
					</div>
					<div class="right">
						<div class="overlay"></div>
						<div class="content text">
							<h1 class="heading">Welcome</h1>
							<p>to Mdex help center</p>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- END WRAPPER -->
</body>

<script>
function validation(){
	var checkRadio = false;
	$("input:radio").each(function(){
	  var name = $(this).attr("name");
	  if($("input:radio[name="+name+"]:checked").length >0 && $('#emailId').val()!='' && $('#passwordId').val()!='')
	  {
		  checkRadio = true;
	  } if($('#emailId').val()=='' || $('#emailId').val()==null ){
		  $('#emailError').html('<p style="color: red; font-size: 20px">Please enter email</p>')
	  }if($('#passwordId').val()=='' || $('#passwordId').val()==null){
		  $('#passError').html('<p style="color: red; font-size: 20px">Please enter password</p>')
	  }if($("input:radio[name="+name+"]:checked").length ==0){
		  $('#errorId').html('<p style="color: red; font-size: 20px">Please select login type</p>')
	  } 
	});
	if(checkRadio){
		var ctx = "${pageContext.request.contextPath}";
		var radioValue = $("input[name='loginWith']:checked").val();
		var urlPath=ctx+"/"+radioValue;
		console.log(urlPath);
		  document.getElementById('formId').action = urlPath;
		  $('#formId').submit();
	}
}
</script>
<script type="text/javascript">
// $(document).ready(function(){
// 	var logintype='${sessionScope.loginMsg}';
// 	console.log(logintype);
// 	if(logintype!=null){
// 		$("#radio1").checked = true;
// 	}else{
// 		$("#radio2").checked = true;
// 	}
// });

</script>


<%HttpSession httpSession= request.getSession();
	if(httpSession!=null){
		httpSession.invalidate();
	}
%>
</html>
