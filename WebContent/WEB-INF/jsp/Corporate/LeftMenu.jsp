<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="sidebar" data-background-color="white"
	data-active-color="danger">
	<div class="sidebar-wrapper">
		<div class="logo">
			<a href="" class="simple-text">
				<center>
					<img
						src="${pageContext.request.contextPath}/resources/corporate/assets/img/logo.png"
						class="img-responsive" style="width: 161px;">
				</center>
			</a>
		</div>

		<ul class="nav">
			<li class="active"><a
				href="${pageContext.request.contextPath}/Corporate/Home"> <i
					class="ti-panel"></i>
					<p>Dashboard</p>
			</a></li>
			<li><a
				href="${pageContext.request.contextPath}/Corporate/BulkRegister">
					<i class="ti-view-list-alt"></i>
					<p>
						BULK REGISTRATION <b class="" style="margin-top: 14px;"></b>
					</p>

			</a></li>

			<li><a
				href="${pageContext.request.contextPath}/Corporate/BulkTransfer">
					<i class="ti-view-list-alt"></i>
					<p>
						BULK TRANSFER <b class="" style="margin-top: 14px;"></b>
					</p>

			</a></li>

			<li><a
				href="${pageContext.request.contextPath}/Corporate/Transactions">
					<i class="ti-view-list-alt"></i>
					<p>
						User Transactions<b class="" style="margin-top: 14px;"></b>
					</p>

			</a></li>

			<li><a
				href="${pageContext.request.contextPath}/Corporate/BulkCardGenerate">
					<i class="ti-view-list-alt"></i>
					<p>
						Bulk Card Generate<b class="" style="margin-top: 14px;"></b>
					</p>

			</a></li>
		</ul>
	</div>
</div>