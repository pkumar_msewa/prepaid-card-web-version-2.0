<!-- partial:partials/_sidebar.html -->
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<nav class="sidebar sidebar-offcanvas" id="sidebar">
	<ul class="nav">
		<li class="nav-item"><a class="nav-link"
			href="${pageContext.request.contextPath}/Corporate/Home"> <i
				class="ti-home menu-icon"></i> <span class="menu-title">Dashboard</span>
		</a></li>

		<c:if test="${bulkRegistration==true}">
			<li class="nav-item"><a class="nav-link"
				href="${pageContext.request.contextPath}/Corporate/CorporateBulkRegister">
					<i class="ti-pencil-alt menu-icon"></i> <span class="menu-title">Bulk
						Registration</span>
			</a></li>
		</c:if>

		<c:if test="${BulkCL==true}">
			<li class="nav-item"><a class="nav-link"
				href="${pageContext.request.contextPath}/Corporate/CorporateBulkCardLoad">
					<i class="icon-server menu-icon"></i> <span class="menu-title">Bulk
						Card Load</span>
			</a></li>
		</c:if>

		<c:if test="${UserType==true}">
			<li class="nav-item"><a class="nav-link"
				href="${pageContext.request.contextPath}/Corporate/CorporateBulkKYCApproval">
					<i class="icon-server menu-icon"></i> <span class="menu-title">Bulk
						KYC Upload</span>
			</a></li>
		</c:if>

		<c:if test="${SingleCardLoad==true}">
			<%-- <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/Corporate/CorporateSinleCardLoad">
              <i class="ti-credit-card menu-icon"></i>
              <span class="menu-title">Single Card Load</span>
            </a>
          </li> --%>
		</c:if>

		<c:if test="${singleCardAssignment==true}">
			<li class="nav-item"><a class="nav-link"
				href="${pageContext.request.contextPath}/Corporate/CorporateSingleCardAssignment">
					<i class="ti-check-box menu-icon"></i> <span class="menu-title">Single
						Card Assignment</span>
			</a></li>
		</c:if>

		<!--   <li class="nav-item">
            <a class="nav-link" href="#">
              <i class="ti-unlock menu-icon"></i>
              <span class="menu-title">Card Block/Unblock</span>
            </a>
          </li> -->

		<li class="nav-item"><a class="nav-link" data-toggle="collapse"
			href="#sidebar-layouts" aria-expanded="false"
			aria-controls="sidebar-layouts"> <i class="icon-paper menu-icon"></i>
				<span class="menu-title">Reports</span> <i class="menu-arrow"></i>
		</a>
			<div class="collapse" id="sidebar-layouts">
				<ul class="nav flex-column sub-menu">
					<li class="nav-item"><a class="nav-link"
						href="${pageContext.request.contextPath}/Corporate/UserReport">User
							report</a></li>
					<c:if test="${UserType==true}">
						<li class="nav-item"><a class="nav-link"
							href="${pageContext.request.contextPath}/Corporate/PrefundHistory">Prefund
								History</a></li>
					</c:if>

					<c:if test="${UserType==true}">
						<li class="nav-item"><a class="nav-link"
							href="${pageContext.request.contextPath}/Corporate/FailedRegistration">Failed
								Registration</a></li>
					</c:if>

					<c:if test="${UserType==true}">
						<li class="nav-item"><a class="nav-link"
							href="${pageContext.request.contextPath}/Corporate/BulkLoadReport">Bulk
								Card Load</a></li>
					</c:if>

					<c:if test="${UserType==true}">
						<li class="nav-item"><a class="nav-link"
							href="${pageContext.request.contextPath}/Corporate/FailedBulkLoad">Failed
								Load</a></li>
					</c:if>

					<c:if test="${UserType==true}">
						<li class="nav-item"><a class="nav-link"
							href="${pageContext.request.contextPath}/Corporate/BlockedCards">Blocked
								Cards</a></li>
					</c:if>
				</ul>
			</div></li>

		<!-- <li class="nav-item"><a class="nav-link" href="#"> <i
				class="ti-comments menu-icon"></i> <span class="menu-title">Bulk
					SMS</span>
		</a></li> -->

		<!-- <li class="nav-item"><a class="nav-link" href="#"> <i
				class="ti-bell menu-icon"></i> <span class="menu-title">Notification
					Center</span>
		</a></li> -->

		<c:if test="${UserType==true}">
			<li class="nav-item"><a class="nav-link"
				href="${pageContext.request.contextPath}/Corporate/PrefundRequest">
					<i class="ti-share-alt menu-icon"></i> <span class="menu-title">Prefund
						Request</span>
			</a></li>
		</c:if>

		<li class="nav-item"><a class="nav-link" data-toggle="collapse"
			href="#sidebar-layouts-Partner" aria-expanded="false"
			aria-controls="sidebar-layouts-Partner"> <i
				class="icon-paper menu-icon"></i> <span class="menu-title">Partner</span>
				<i class="menu-arrow"></i>

		</a>
			<div class="collapse" id="sidebar-layouts-Partner">
				<ul class="nav flex-column sub-menu">
					<c:if test="${UserType==true}">
						<li class="nav-item"><a class="nav-link"
							href="${pageContext.request.contextPath}/Corporate/AddCorporatePartner"><i
								class="ti-user menu-icon"></i>Add Partner</a></li>
					</c:if>

					<c:if test="${UserType==true}">
						<li class="nav-item"><a class="nav-link"
							href="${pageContext.request.contextPath}/Corporate/ListCorporatePartner"><i
								class="ti-user menu-icon"></i>List Partner</a></li>
					</c:if>
				</ul>
			</div></li>

		<li class="nav-item"><a class="nav-link"
			href="${pageContext.request.contextPath}/Corporate/ChangePassword">
				<i class="ti-user menu-icon"></i> <span class="menu-title">Change
					Password</span>
		</a></li>

		<li class="nav-item nav-profile">
			<div class="nav-link d-flex">
				<div class="profile-name">
					<p class="name">Powered by:</p>
				</div>
				<div class="profile-image">
					<img
						src="${pageContext.request.contextPath}/resources/corporate/images/logo.png"
						alt="powered by" />
				</div>
			</div>
		</li>

	</ul>
</nav>