<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Corporate | Prefund Request</title>
<!-- plugins:css -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/puse-icons-feather/feather.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.base.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.addons.css">
<!-- endinject -->
<!-- plugin css for this page -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/simple-line-icon/css/simple-line-icons.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/ti-icons/css/themify-icons.css">
<!-- End plugin css for this page -->
<!-- inject:css -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/css/style.css">
<!-- endinject -->
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/resources/corporate/images/favicon.png" />
</head>

<body class="sidebar-fixed" oncontextmenu="return false">
	<div class="container-scroller">
		<jsp:include page="Header.jsp" />
		<div class="container-fluid page-body-wrapper">
			<jsp:include page="SideMenu.jsp" />
			<div class="main-panel">
				<div class="content-wrapper">
					<div class="row">
						<div class="col-md-8 offset-md-2 grid-margin stretch-card">
							<div class="card text-black">
								<div class="card-body">
									<h4 class="card-title">Prefund Request</h4>
									<form
										action="${pageContext.request.contextPath}/Corporate/CorporatePrefund"
										method="Post" enctype="multipart/form-data">
										<div class="row">
											<div class="col-6">
												<div class="form-group">
													<label>Corporate Name</label> <input type="text"
														name="clientName" placeholder="Enter Corporate Name"
														class="form-control">
												</div>
											</div>
											<div class="col-6">
												<div class="form-group">
													<label>Amount</label> <input type="text" name="amount"
														placeholder="Enter Amount" class="form-control">
												</div>
											</div>
											<div class="col-6">
												<div class="form-group">
													<label>Transaction Ref No</label> <input type="text"
														name="transactionRefNo" placeholder="Enter Bank Ref No"
														class="form-control">
												</div>
											</div>
											<div class="col-6">
												<div class="form-group">
													<label>Upload file</label> <input type="file" name="file"
														placeholder="Upload" class="form-control">
												</div>
											</div>
										</div>
										<div class="form-group">
											<div align="center">
												<button class="btn btn-primary" type="submit">Submit</button>
											</div>
										</div>
									</form>
									<div class="form-group">
										<h6 class="card-title" style="color: red;">${successMsg}</h6>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<jsp:include page="Footer.jsp"></jsp:include>
			</div>
		</div>
	</div>

	<!-- plugins:js -->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/vendors/js/vendor.bundle.base.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/vendors/js/vendor.bundle.addons.js"></script>
	<!-- endinject -->
	<!-- inject:js -->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/off-canvas.js"></script>
	<!-- <script src="js/hoverable-collapse.js"></script> -->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/misc.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/settings.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/todolist.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/jquery-file-upload.js"></script>
	<!-- endinject -->
	<!-- Custom js for this page-->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/dashboard.js"></script>
	<!-- End custom js for this page-->

	<script>
		document.onkeydown = function(e) {
			if (event.keyCode == 123) {
				return false;
			}
			if (e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
				return false;
			}
			if (e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
				return false;
			}
			if (e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
				return false;
			}
		}
	</script>
</body>
</html>