<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>Admin | User List</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta content="" name="description" />
<meta content="" name="author" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<!-- App favicon -->
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/resources/images/favicon.png">

<!-- Table Export -->
<link
	href="${pageContext.request.contextPath}/resources/admin/assets/table_export/css/tableexport.css"
	rel="stylesheet" type="text/css" />

<!-- App css -->
<link
	href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css"
	rel="stylesheet" type="text/css" />
<script> var contextPath="${pageContext.request.contextPath}";</script>
<script
	src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

<script>var contextPath="${pageContext.request.contextPath}";</script>
<script
	src="${pageContext.request.contextPath}/resources/assets-group/js/customercarepost.js"></script>

<script type="text/javascript">
    var context_path="${pageContext.request.contextPath}";
    </script>
</head>

<body>
	<div id="wrapper">
		<jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />

		<div class="content-page">
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<div class="col-12">
							<div class="page-title-box">
								<h4 class="page-title float-left">List of Users</h4>
								<div class="clearfix"></div>
								<span id="stst" style="margin-left: 40%; color: #3c86d8fa;">${statusUpdt}</span>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-12">
							<div class="card-box">
								<div class="row" style="margin-left: -15px; margin-top: -15px;">
									<div class="col-md-4 col-sm-4 col-xs-4">
										<form action="" method="post">
											<div class="form-row">
												<div class="col-sm-8">
													<div id="" class="pull-left" style="cursor: pointer;">
														<label class="sr-only" for="filterBy">Filter By:</label> <input
															id="reportrange" name="toDate" class="form-control"
															readonly="readonly" />
													</div>
												</div>
												<div class="col-sm-3">
													<button class="btn btn-primary" onclick="fetchlist()"
														type="button">Filter</button>
												</div>
											</div>
										</form>
									</div>

									<div class="col-md-4 col-sm-4 col-xs-4">
										<div class="row">
											<div class="form-group">
												<input id="username" name="userName" class="form-control"
													placeholder="Enter Name/Email/Contact No." />
											</div>
											<div class="form-group">
												<button class="btn btn-primary" onclick="fetchSincgleCard()"
													type="button">Search</button>
											</div>
											<span id="err"
												style="color: red; position: fixed; margin-top: 30px;"></span>
										</div>
									</div>

									<div class="col-md-2 col-sm-4 col-xs-4">
										<div class="row">
											<select class="form-control" name="contactNo" id="operatorId"
												style="width: 100% !important; margin-left: 10px; margin-top: 2px;">
												<option value="All" selected>Select Group</option>
												<c:forEach var="operator" items="${groupList}">
													<option value="${operator.contactNo}">${operator.groupName}</option>
												</c:forEach>
											</select> <input type="hidden" id="ContactNo" name="ContactNo"
												value="${operator.contactNo}" />
											<div class="col-md-4 col-sm-4 col-xs-4">
												<button type="button" class="btn btn-primary"
													onclick="fetchGrouplist()" title="Search"
													style="margin-top: -64px; margin-left: 171px;">
													Search</button>
											</div>
											<span id="err"
												style="color: red; position: fixed; margin-top: 27px;"></span>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-12">
							<div class="card-box table-responsive">
								<table id="Cashier-userList"
									class="table table-striped table-bordered" width="100%">
									<thead>
										<tr>
											<th>S. No.</th>
											<th>User Details</th>
											<th>Registered Date</th>
											<th>Mobile Token</th>
											<th>Status</th>
											<th id="blockunblock">Action</th>
										</tr>
									</thead>
									<tbody id="ashok">
									</tbody>
								</table>
								<nav style="float: right;">
									<ul class="pagination" id="paginationn"></ul>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</div>
			<footer class="footer text-right"> 2017 © Copyright EWire. </footer>
		</div>
	</div>

	<div id="common_success_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_success_msg" class="alert alert-success"></center>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="myModalHorizontal" tabindex="-1"
		role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Change Group</h4>
				</div>

				<div class="modal-body">
					<form class="form-horizontal" role="form">
						<div class="form-group">
							<div class="col-sm-10">
								<select class="form-control" name="contactNo" id="groupop"
									style="width: 100% !important; margin-left: -8px; margin-top: -2px;">
									<option value="All" selected>Select Group</option>
									<c:forEach var="operator" items="${groupList}">
										<option value="${operator.contactNo}">${operator.groupName}</option>
									</c:forEach>
								</select> <input type="hidden" id="groupMob" name="groupMob"
									value="${operator.contactNo}" /> <input type="hidden"
									id="userContactNo" name="userContactNo" /> <span class="error"
									style="color: red;" id="reasonError"></span>
							</div>
						</div>
					</form>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary"
						onclick="changeGroupPost()">Add Group</button>
				</div>
			</div>
		</div>
	</div>

	<!-- jQuery  -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>

	<script type="text/javascript"
		src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	<!-- Include Date Range Picker -->
	<script type="text/javascript"
		src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

	<!-- Table Export js -->
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/admin/assets/table_export/js/FileSaver.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/admin/assets/table_export/js/tableexport.js"></script>

	<!-- App js -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>
</body>

<script>
     function blockUser(va){
    	 var contact=va;
    	 var auth="ROLE_USER,ROLE_LOCKED";
    		$.ajax({
    			type : "POST",
    			contentType : "application/json",
    			url : context_path+"/Admin/Status/block/unblock",
    			dataType : 'json',
    			data : JSON.stringify({
    				"authority" :" "+auth+"",
    				"userName" :""+va+""
    			}),
    			success : function(response) {
    				 $("#statusUpdt").html(response.message);
    				 window.location.href="${pageContext.request.contextPath}/Admin/UserList";
    			},
    		});
    	 
     }
    </script>

<script>
     function unblockUser(va){
    	 var auth="ROLE_USER,ROLE_AUTHENTICATED";
    		$.ajax({
    			type : "POST",
    			contentType : "application/json",
    			url : context_path+"/Admin/Status/block/unblock",
    			dataType : 'json',
    			data : JSON.stringify({
    				"authority" :" "+auth+"",
    				"userName" :""+va+""
    			}),
    			success : function(response) {
    				 $("#statusUpdt").html(response.message);
    				 window.location.href="${pageContext.request.contextPath}/Admin/UserList";
    			},
    		});
     }
    </script>

<script>
		function fetchlist(){
				var date=$("#reportrange").val();				
			 var paging='0';
			 var size='';

			 $.ajax({
					type:"POST",
					url:"${pageContext.request.contextPath}/Admin/UserList",
					data:{
						page:paging,
						size:'10',
						Daterange:date						
						},
				dataType:"json",
				success:function(data){
					 $("#reportrange").html(data.date);
					var trHTML='';
						if(trHTML==''){
						$(".testingg").empty();
						$(data.jsonArray).each(function(i,item){
							if(data.jsonArray[i].role){
								if(!data.jsonArray[i].addUserBool) {
									trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'</tr>';
								} else {
									trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="" onclick="blockUser('+data.jsonArray[i].contactNO+')" value="">Block</button></td>'+'</tr>';
								}
								}else{
									if(!data.jsonArray[i].addUserBool) {
										trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'</tr>';
									} else {
										trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-primary" id="" onclick="unblockUser('+data.jsonArray[i].contactNO+')" value="">Unblock</button></td>'+'</tr>';
									}
								}});

						$('#ashok').append(trHTML);
						}
						else{
							$(data.jsonArray).each(function(i,item){
								if(data.jsonArray[i].role){
									if(!data.jsonArray[i].addUserBool) {
										trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'</tr>';
									} else {
										trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="" onclick="blockUser('+data.jsonArray[i].contactNO+')" value="">Block</button></td>'+'</tr>';
									}
									}else{
										if(!data.jsonArray[i].addUserBool) {
											trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'</tr>';
										} else {
											trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-primary" id="" onclick="unblockUser('+data.jsonArray[i].contactNO+')" value="">Unblock</button></td>'+'</tr>';
										}
									}});
							$('#ashok').append(trHTML);
						}
						
						 $(function () {
							 $('#paginationn').twbsPagination({
								 totalPages: data.totalPages,
								 visiblePages: 7,
					         onPageClick: function (event, page) {
					        	 fetchMe(page-1);
					         }
							 });
							});
							}
						 });
		}
		</script>
		
<script>		
		function fetchGrouplist(){
			var date=$("#reportrange").val();
			var contact = $("#operatorId").val();  
		 var paging='0';
		 
		 $.ajax({
				type:"POST",
				url:"${pageContext.request.contextPath}/Admin/UserGroupList",
				data:{
					page:paging,
					size:'10',
					Daterange:date,
					contact : contact
					},
			dataType:"json",
			success:function(data){
				 $("#reportrange").html(data.date);
				var trHTML='';
					if(trHTML==''){
					$(".testingg").empty();
					$(data.jsonArray).each(function(i,item){
						if(data.jsonArray[i].role){
							if(!data.jsonArray[i].addUserBool) {
								trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'</tr>';
							} else {
								trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="" onclick="blockUser('+data.jsonArray[i].contactNO+')" value="">Block</button></td>'+'</tr>';
							}
							}else{
								if(!data.jsonArray[i].addUserBool) {
									trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'</tr>';
								} else {
									trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-primary" id="" onclick="unblockUser('+data.jsonArray[i].contactNO+')" value="">Unblock</button></td>'+'</tr>';
								}
							}});
					$('#ashok').append(trHTML);
					}
					else{
						$(data.jsonArray).each(function(i,item){
							if(data.jsonArray[i].role){
								if(!data.jsonArray[i].addUserBool) {
									trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'</tr>';
								} else {
									trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="" onclick="blockUser('+data.jsonArray[i].contactNO+')" value="">Block</button></td>'+'</tr>';
								}
								}else{
									if(!data.jsonArray[i].addUserBool) {
										trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'</tr>';
									} else {
										trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-primary" id="" onclick="unblockUser('+data.jsonArray[i].contactNO+')" value="">Unblock</button></td>'+'</tr>';
									}
								}});
						$('#ashok').append(trHTML);
					}
					
					 $(function () {
						 $('#paginationn').twbsPagination({
							 totalPages: data.totalPages,
							 visiblePages: 7,
				         onPageClick: function (event, page) {
				        	 fetchMe(page-1);
				         }
						 });
						});
						}
					 });
	}
		</script>

<script>
 function fetchSincgleCard(){
	var username=$("#username").val();
	var valid=true;
		if(username == ''){
			valid=false;
			$("#err").html("Please enter details")
		}
		
		if(valid == true){
			 var paging='0';
			 var size='10';
			 $.ajax({
					type:"POST",
					url:"${pageContext.request.contextPath}/Admin/SingleUser",
					data:{
						userName:username,
						page: paging,
						size: size
						},
				dataType:"json",
				success:function(data){
					var trHTML='';
						if(trHTML==''){
						$(".testingg").empty();
						$(data.jsonArray).each(function(i,item){
							if(data.jsonArray[i].role){
								if(!data.jsonArray[i].addUserBool) {
									trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName +  '<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'</tr>';
								} else {
									trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName +  '<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="" onclick="blockUser('+data.jsonArray[i].contactNO+')" value="">Block</button></td>'+'</tr>';
								}
								}else{
									if(!data.jsonArray[i].addUserBool) {
										trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName +  '<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'</tr>';
									} else {
										trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName +  '<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-primary" id="" onclick="unblockUser('+data.jsonArray[i].contactNO+')" value="">Unblock</button></td>'+'</tr>';
									}
								}});
						$('#ashok').append(trHTML);
						}
						else{
							$(data.jsonArray).each(function(i,item){
								if(data.jsonArray[i].role){
									if(!data.jsonArray[i].addUserBool) {
										trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName +  '<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'</tr>';
									} else {
										trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName +  '<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="" onclick="blockUser('+data.jsonArray[i].contactNO+')" value="">Block</button></td>'+'</tr>';
									}
									}else{
										if(!data.jsonArray[i].addUserBool) {
											trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName +  '<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'</tr>';
										} else {
											trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName +  '<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-primary" id="" onclick="unblockUser('+data.jsonArray[i].contactNO+')" value="">Unblock</button></td>'+'</tr>';
										}
									}});
							$('#ashok').append(trHTML);
						}
						
						 $(function () {
							 $('#paginationn').twbsPagination('destroy');
							 $('#paginationn').twbsPagination({
								 totalPages: data.totalPages,
					         onPageClick: function (event, page) {
					        		 fetchSingleUserList(page-1);
					         }
							 });
							});
							}
						 });
		}
 }
 </script>
 
 <script>
	 function fetchSingleUserList(value){
		var paging=value;
		var username=$("#username").val();
		
	$.ajax({
	type:"POST",
	url:"${pageContext.request.contextPath}/Admin/SingleUser",
	data:{page:paging,size:'10',userName:username},
	dataType:"json",
	success:function(data){
		var trHTML='';
			if(trHTML==''){
			$(".testingg").empty();
			$(data.jsonArray).each(function(i,item){
				if(data.jsonArray[i].role){
					if(!data.jsonArray[i].addUserBool) {
						trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName +  '<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'</tr>';
					} else {
						trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName +  '<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="" onclick="blockUser('+data.jsonArray[i].contactNO+')" value="">Block</button></td>'+'</tr>';
					}
					}else{
						if(!data.jsonArray[i].addUserBool) {
							trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName +  '<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'</tr>';
						} else {
							trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName +  '<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-primary" id="" onclick="unblockUser('+data.jsonArray[i].contactNO+')" value="">Unblock</button></td>'+'</tr>';
						}
					}});
			$('#ashok').append(trHTML);
			}
			else{
				$(data.jsonArray).each(function(i,item){
					if(data.jsonArray[i].role){
						if(!data.jsonArray[i].addUserBool) {
							trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName +  '<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'</tr>';
						} else {
							trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName +  '<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="" onclick="blockUser('+data.jsonArray[i].contactNO+')" value="">Block</button></td>'+'</tr>';
						}
						}else{
							if(!data.jsonArray[i].addUserBool) {
								trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName +  '<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'</tr>';
							} else {
								trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName +  '<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-primary" id="" onclick="unblockUser('+data.jsonArray[i].contactNO+')" value="">Unblock</button></td>'+'</tr>';
							}
						}});
				$('#ashok').append(trHTML);
			}
	}
	});
			 }
	 </script>

<script>
		$(function() {
		    var start = moment().subtract(29, 'days');
		    var end = moment();
		
		    function cb(start, end) {
		        $('#reportrange').html(start.format('MM-dd-yyyy') + ' - ' + end.format('MM-dd-yyyy'));
		    }
		
		    $('#reportrange').daterangepicker({
		        startDate: start,
		        endDate: end,
		        locale: {
		        	format: 'YYYY-MM-DD'
		        },
		        dateLimit: {
		            "days": 30
		        },
		        ranges: {
		           'Today': [moment(), moment()],
		           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		           'This Month': [moment().startOf('month'), moment().endOf('month')],
		           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		        }
		    }, cb);
		
		    cb(start, end);
		});
		</script>

<script>
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>

<script>
            $("#Cashier-userList").tableExport();
        </script>

<script>
        	(function($) {
			  $.fn.removeClasses = function(classes) {
			    return this.removeClass(classes.join(' '));
			  };
			  $.fn.switchify = function(config) {
			    config = config || {};
			    var prefix   =           config.prefix   || 'range-';
			    var onCls    = prefix + (config.onCls    || 'on'   );
			    var offCls   = prefix + (config.offCls   || 'off'  );
			    var unsetCls = prefix + (config.unsetCls || 'unset');
			    var $self = this;
			    return this.on('change', function(e) {
			      var value = parseInt(this.value, 10);
			      switch (value) {
			        case 1  :  return $self.removeClasses([unsetCls, offCls]).addClass(onCls);
			        case 2  :  return $self.removeClasses([onCls, offCls]).addClass(unsetCls);
			        case 3  :  return $self.removeClasses([onCls, unsetCls]).addClass(offCls);
			        default :  return $self;
			      }
			    });
			  };
			})(jQuery);

			$('#range-filter').switchify({
			   onCls    : 'active',
			   offCls   : 'passive',
			   unsetCls : 'all'
			}).on('change', function(e) {
			  var $self = $(this);
			  if      ($self.hasClass('range-active'))  $('.range-txt').text('User Name'), $('#username').attr('placeholder', 'Enter Username');
			  else if ($self.hasClass('range-passive')) $('.range-txt').text('Mobile'), $('#username').attr('placeholder', 'Enter Mobile No.');
			  else if ($self.hasClass('range-all'))     $('.range-txt').text('Email'), $('#username').attr('placeholder', 'Enter Email-Id');
			  else                                      $('.range-txt').text('Error!');
			});
        </script>

<script>
    $(document).ready(function() {
    	$('#username').val('');
		 getAjaxData();
		 var timeout = setTimeout(function(){
     		$("#stst").html("");
     	}, 3000); 
	 });
	</script>

<script>
	 function fetchMe(value){
		var paging=value;
		var daterangeVal=$('#reportrange').val();
		
	$.ajax({
	type:"POST",
	url:"${pageContext.request.contextPath}/Admin/UserList",
	data:{page:paging,size:'10',Daterange:daterangeVal},
	dataType:"json",
	success:function(data){
	var trHTML='';
		if(trHTML==''){
		$(".testingg").empty();
		$(data.jsonArray).each(function(i,item){
			
			if(data.jsonArray[i].role){
				if(!data.jsonArray[i].addUserBool) {
					trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + 
					data.jsonArray[i].firstName + '<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+
					data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ 
					data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ 
					data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ 
					data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+
					data.jsonArray[i].status+'</td>'+'</tr>';
				} else {
		trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + 
			data.jsonArray[i].firstName + '<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+
			data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ 
			data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ 
			data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ 
			data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+
			data.jsonArray[i].status+'</td>'+'<td>'+
			'<button type="submit" class="btn btn-sm btn-danger" id="" onclick="blockUser('+data.jsonArray[i].contactNO+')" value="">Block</button></td>'+'</tr>';
			}
				}else{
					if(!data.jsonArray[i].addUserBool) {
						trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'</tr>';
					} else {
				trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-primary" id="" onclick="unblockUser('+data.jsonArray[i].contactNO+')" value="">Unblock</button></td>'+'</tr>';
					}
		}});
		
		$('#ashok').append(trHTML);
		}
		else{
			$(data.jsonArray).each(function(i,item){
				if(data.jsonArray[i].role){
					if(!data.jsonArray[i].addUserBool) {
						trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + 
						data.jsonArray[i].firstName + '<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+
						data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ 
						data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ 
						data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ 
						data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+
						data.jsonArray[i].status+'</td>'+'</tr>';

					} else {
					trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + 
					data.jsonArray[i].firstName + '<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+
					data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ 
					data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ 
					data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ 
					data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+
					data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="" onclick="blockUser('+data.jsonArray[i].contactNO+')" value="">Block</button></td></div>'+'</tr>';
					}
					}else{		
						if(!data.jsonArray[i].addUserBool) {
							trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'</tr>';

						} else {
							trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-primary" id="" onclick="unblockUser('+data.jsonArray[i].contactNO+')" value="">Unblock</button></td>'+'</tr>';
						}
					}});
			$('#ashok').append(trHTML);
		}
	}
	});
			 }
	 </script>
	 
<script>
	 function  getAjaxData(){
		
		 var paging='0';
		 var size='';
		 
		 $.ajax({
				type:"POST",
				url:"${pageContext.request.contextPath}/Admin/UserList",
				data:{
					page:paging,
					size:'10'
					},
			dataType:"json",
			success:function(data){
				 
				var trHTML='';
					if(trHTML==''){
					$(".testingg").empty();
					$(data.jsonArray).each(function(i,item){
						if(data.jsonArray[i].role){
							if(!data.jsonArray[i].addUserBool) {
								trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'</tr>';
							} else {
								trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="" onclick="blockUser('+data.jsonArray[i].contactNO+')" value="">Block</button></td>'+'</tr>';
							}
							}else{
								if(!data.jsonArray[i].addUserBool) {
									trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'</tr>';
								} else {
									trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-primary" id="" onclick="unblockUser('+data.jsonArray[i].contactNO+')" value="">Unblock</button></td>'+'</tr>';
								}	
							}});
					$('#ashok').append(trHTML);
					}
					else{
						$(data.jsonArray).each(function(i,item){
							if(data.jsonArray[i].role){
								if(!data.jsonArray[i].addUserBool) {
									trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName +'<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'</tr>';
								} else {
									trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName +'<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="" onclick="blockUser('+data.jsonArray[i].contactNO+')" value="">Block</button></td>'+'</tr>';
								}
								}else{
									if(!data.jsonArray[i].addUserBool) {
										trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName +'<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'</tr>';
									} else {
										trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName +'<br>Mobile No:<a href="${pageContext.request.contextPath}/Admin/userDetails/'+data.jsonArray[i].contactNO+'">'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'<br>Group:'+data.jsonArray[i].group+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-primary" id="" onclick="unblockUser('+data.jsonArray[i].contactNO+')" value="">Unblock</button></td>'+'</tr>';
									}
								}});
						$('#ashok').append(trHTML);
					}
					
					  $(function () {
						 $('#paginationn').twbsPagination({
							 totalPages: data.totalPages,
							 visiblePages: 7,
				         onPageClick: function (event, page) {
				        	 fetchMe(page-1);
				         }
						 });
						}); 
						}
					 });
				 }
</script>
</html>