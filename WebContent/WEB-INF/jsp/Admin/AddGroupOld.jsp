<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta content="" name="description" />
<meta content="" name="author" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title>Admin | Add Group</title>
<script type="text/javascript">
var contextPath="${pageContext.request.contextPath}";
</script>

<!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">

        <!-- DataTables -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />
        
         <!-- jQuery  -->
  		<script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
  		

        <style>
            fieldset {
                min-width: 0;
                padding: 10px 30px;
                margin: 0;
                border: 1px solid #efefef;
            }
            fieldset legend {
                width: auto;
                font-size: 1.3rem;
            }
            .fileUpload {
              position: relative;
              overflow: hidden;
              height: 43px;
              margin-top: 0;
            }
            
            .error{
            color:red;
            }

            .fileUpload input.uploadlogo {
              position: absolute;
              top: 0;
              right: 0;
              margin: 0;
              padding: 0;
              font-size: 20px;
              cursor: pointer;
              opacity: 0;
              filter: alpha(opacity=0);
              width: 100%;
              height: 42px;
            }

            /*Chrome fix*/
            input::-webkit-file-upload-button {
              cursor: pointer !important;
              height: 42px;
              width: 100%;
            }
            .blue-btn:hover,
            .blue-btn:active,
            .blue-btn:focus,
            .blue-btn {
              background: transparent;
              border: solid 1px #27a9e0;
              border-radius: 3px;
              color: #27a9e0;
              font-size: 14px;
              margin-bottom: 20px;
              outline: none !important;
              padding: 10px 20px;
            }
        </style>

</head>

<body oncontextmenu="return false">

<input type="hidden" name="successMsg" id="successMsg" value="${groupAddMsg}">
<input type="hidden" name="errorMsg" id="errorMsg" value="${error}">
 <!-- Begin page -->
        <div id="wrapper">
						   <!-- Top Bar Start -->
           <jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />

            </div>
            <!-- Left Sidebar End -->
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Add Group</h4>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-12">
						<div class="card-box">
							<center>
								<div>
									<h6 style="font: bold; color: red;">${msg}</h6>
							</center>

							<form action="${pageContext.request.contextPath}/Admin/AddGroup"
								method="POST" id="formId" enctype="multipart/form-data">

								<fieldset>
									<legend>Add Group</legend>
									<div class="row">
										<div class="col-6">
											<div class="form-group">
												<label for="name">Group Name</label>
												<!--   <input type="text" name="name" placeholder="Enter Group Name" id="name" class="form-control" maxlength="60" onkeypress="return isAlphKey(event); groupname_error(event);"> -->
												<input type="text" name="name"
													placeholder="Enter Group Name" id="name"
													class="form-control" maxlength="60"
													onkeypress="return groupname_error(event); groupname_error(event);">
												<p class="error" id="error_fullName"></p>
											</div>
										</div>
										<div class="col-6">
											<div class="form-group">
												<label for="name">Display Name</label>
												<!-- input type="text" name="name" placeholder="Enter Group Name" id="name" class="form-control" maxlength="60" onkeypress="return isAlphKey(event); groupname_error(event);"> -->
												<input type="text" name="dname"
													oninput="this.value = this.value.toUpperCase()"
													placeholder="Display name (max 4 letters only)" id="dname"
													class="form-control" maxlength="4"
													onkeypress="return displayname_error(event);">
												<p class="error" id="error_displayName"></p>
											</div>
										</div>

										<div class="col-6">
											<div class="form-group">
												<label>Email</label>
												<!--  <input type="email" name="emailAddressId" placeholder="Enter Email" id="emailAddressId" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" class="form-control"> -->
												<input type="email" name="emailAddressId"
													placeholder="Enter Email" id="emailAddressId"
													pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"
													onkeypress="email_error(event);" class="form-control">
												<p class="error" id="error_email"></p>
											</div>
										</div>
										<div class="col-6">
											<div class="form-group">
												<label>Contact Number</label>
												<!--   <input type="text" name="mobileNoId" placeholder="Enter Mobile" id="mobileNoId" class="form-control" minlength="10" maxlength="10" onkeypress="return isNumberKey(event);"> -->
												<input type="text" name="mobileNoId"
													placeholder="Enter Mobile" id="mobileNoId"
													class="form-control" minlength="10" maxlength="10"
													onkeypress="return mobile_error(event);">
												<p class="error" id="error_mobile"></p>
											</div>
											<input type="hidden" id="countryCodeId" name="countryCode" />
										</div>
									</div>
									<div class="row">
										<div class="col">

											<div class="from-group">
												<div class="form-group">
													<div class="fileUpload blue-btn btn width100">
														<span>Upload Documents</span> <input type="file"
															class="uploadlogo" name="image" id="aadhar_Image" />

													</div>
													<p class="error" id="error_aaadharCardImg"></p>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col">
											<div class="togl_wrap">
												<!-- Toggle -->
											</div>						
										</div>
									</div>
									<div class="row">
										<div class="col">
											<div class="services_chek" style="border-right: 1px solid #e5e5e5;">
												<!-- Check boxes -->											
											</div>							
										</div>
										<div class="col">
											<div class="description_wrap">
												<div class="desc_heading">
													<h4>Description</h4>												
												</div>
												<div class="desc_txt">
													<p>lorem Ipsum</p>												
												</div>											
											</div>										
										</div>									
									</div>
								</fieldset>

								<center>
									<button type="button" id="addAgent"
										class="btn btn-primary mt-4" onclick="addCorporateAgent()">Add
										Group</button>
								</center>
							</form>
						</div>
					</div>
                            </div>
                        </div> <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    &copy; <script>
																					document
																							.write(new Date()
																									.getFullYear());
																				</script> Copyright EWire.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->
        
        <div id="common_success_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_success_msg" class="alert alert-success"></center>
				</div>
			</div>
		</div>
	</div>
	
	
	<div id="common_error_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_error_msg" class="alert alert-danger"></center>
					<%-- <center><label id="common_error_msg" class="alert alert-danger"></label></center> --%>
				</div>
			</div>
		</div>
	</div>

<script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/jquery.dataTables.min.js"></script>
  		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>

        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>       
</body>
</html>