<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
    
<head>
        <meta charset="utf-8" />
        <title>Admin | Add Category</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">
	<script type="text/javascript">var contextPath = "${pageContext.request.contextPath}";</script>
        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />
	 <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.3/combined/css/gijgo.min.css" rel="stylesheet" type="text/css" />
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
		
		<script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
		<script>var contextPath="${pageContext.request.contextPath}";</script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/customercarepost.js"></script>

        <style>
            fieldset {
                min-width: 0;
                padding: 10px 30px;
                margin: 0;
                border: 1px solid #efefef;
            }
            fieldset legend {
                width: auto;
                font-size: 1.3rem;
            }
            .blue-btn:hover,
            .blue-btn:active,
            .blue-btn:focus,
            .blue-btn {
              background: transparent;
              border: solid 1px #27a9e0;
              border-radius: 3px;
              color: #27a9e0;
              font-size: 14px;
              margin-bottom: 20px;
              outline: none !important;
              padding: 10px 20px;
            }
            .tgl {
              position: relative;
              display: inline-block;
              height: 30px;
              cursor: pointer;
              margin-top: 27px;
            }
            .tgl > input {
              position: absolute;
              opacity: 0;
              z-index: -1;
              /* Put the input behind the label so it doesn't overlay text */
              visibility: hidden;
            }
            .tgl .tgl_body {
              width: 60px;
              height: 30px;
              background: white;
              border: 1px solid #dadde1;
              display: inline-block;
              position: relative;
              border-radius: 50px;
            }
            .tgl .tgl_switch {
              width: 30px;
              height: 30px;
              display: inline-block;
              background-color: white;
              position: absolute;
              left: -1px;
              top: -1px;
              border-radius: 50%;
              border: 1px solid #ccd0d6;
              -moz-box-shadow: 0 2px 2px rgba(0, 0, 0, 0.13);
              -webkit-box-shadow: 0 2px 2px rgba(0, 0, 0, 0.13);
              box-shadow: 0 2px 2px rgba(0, 0, 0, 0.13);
              -moz-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), -moz-transform 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              -o-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), -o-transform 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              -webkit-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), -webkit-transform 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), transform 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              z-index: 1;
            }
            .tgl .tgl_track {
              position: absolute;
              left: 0;
              top: 0;
              right: 0;
              bottom: 0;
              overflow: hidden;
              border-radius: 50px;
            }
            .tgl .tgl_bgd {
              position: absolute;
              right: -10px;
              top: 0;
              bottom: 0;
              width: 55px;
              -moz-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), right 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              -o-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), right 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              -webkit-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), right 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), right 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              background: #439fd8 url("http://petelada.com/images/toggle/tgl_check.png") center center no-repeat;
            }
            .tgl .tgl_bgd-negative {
              right: auto;
              left: -45px;
              background: white url("http://petelada.com/images/toggle/tgl_x.png") center center no-repeat;
            }
            .tgl:hover .tgl_switch {
              border-color: #b5bbc3;
              -moz-transform: scale(1.06);
              -ms-transform: scale(1.06);
              -webkit-transform: scale(1.06);
              transform: scale(1.06);
            }
            .tgl:active .tgl_switch {
              -moz-transform: scale(0.95);
              -ms-transform: scale(0.95);
              -webkit-transform: scale(0.95);
              transform: scale(0.95);
            }
            .tgl > :not(:checked) ~ .tgl_body > .tgl_switch {
              left: 30px;
            }
            .tgl > :not(:checked) ~ .tgl_body .tgl_bgd {
              right: -45px;
            }
            .tgl > :not(:checked) ~ .tgl_body .tgl_bgd.tgl_bgd-negative {
              right: auto;
              left: -10px;
            }
            .options {
                font-size: 16px;
            }
            .gj-datepicker-bootstrap [role=right-icon] button {
            	padding: 18px 0;
            }
        </style>

    </head>


    <body oncontextmenu="return false">
		<!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
           <jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
            <!-- Top Bar End -->
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Add Category</h4>

                                    <!-- <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="#">Abstack</a></li>
                                        <li class="breadcrumb-item"><a href="#">Tables</a></li>
                                        <li class="breadcrumb-item active">Datatable</li>
                                    </ol> -->

                                    <div class="clearfix"></div>
                                    <div style="color: green; text-align: center; ">${regmessage}</div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-12">
                                <div class="card-box">
                                    <!-- <h4 class="m-t-0 header-title"><b>Default Example</b></h4>
                                    <p class="text-muted font-14 m-b-30">
                                        DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>.
                                    </p> -->
                                    
                                    <form action="${pageContext.request.contextPath}/Admin/Category" id="formId"  method="post" enctype="multipart/form-data">
                                            <div class="row">
                                                <div class="col-8 offset-md-2 offset-md-3t-sm-3">
                                                    <fieldset>
                                                        <legend>Category Details</legend>
												<div class="row">
													<div class="col-6">
														<div class="form-group">
															<label for="categoryName">Category Name</label> <input
																type="text" name="name" id="catName"
																class="form-control" placeholder="Category Name"
																onkeypress="return isAlphKey(event);">
															<p id="nameerr" style="color: red"></p>
														</div>
													</div>
													<div class="col-6">
														<div class="form-group">
															<label for="defaultAmount">Default Amount</label> <input
																type="text" name="default_amount" id="defltAmount"
																class="form-control" placeholder="Amount"
																onkeypress="return isNumberKey(event);">
															<p id="amterr" style="color: red"></p>
														</div>
													</div>
													<div class="col-6">
														<div class="form-group">
															<label for="mcc">MCC</label> <input type="text"
																id="mccode" name="mcc" class="form-control"
																placeholder="MCC" onkeypress="return isNumberKey(event);">
															<p id="mccerr" style="color: red"></p>
														</div>
													</div>
													<div class="col-6">
													<!-- 	<div class="form-group"> -->
															 <label for="defaultAmount">Upload File</label><input type="file"
																id="imgUrl" name="imgUrl" class="form-control" placeholder="filepath">
															<p id="uploaderr" style="color: red"></p>
														<!-- </div> -->
													</div>
												</div>
											</fieldset>
                                                </div>
                                            </div>
                                            <center><button type="button" class="btn btn-primary mt-4" onclick="validateform()">Submit</button></center>
                                        </form>
                                </div>
                            </div>
                        </div> <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                     2017 Â© Copyright EWire.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->


         <!-- jQuery  -->
       
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
        
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        
        <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.3/combined/js/gijgo.min.js" type="text/javascript"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>
    
    
    <script>
    	
    	function validateform(){
    	var valid = true;
    	var namepattern = "[A-Za-z]";
    	var categoryName=$("#catName").val();
    	console.log("valid: "+categoryName);
    	var default_amount= $("#defltAmount").val();
    	var mcc = $("#mccode").val();
    	var imageUrl=$("#imgUrl").val();
   		
    	
    	if(categoryName.length == 0 ){
    		valid=false;
    		$("#nameerr").html("Please Enter Category Name");    		
    	}
    	if(mcc.length == 0 ){
    		valid=false;
    		$("#mccerr").html("Please Enter Merchant Category Code");    		
    	}
    	if(default_amount.length == 0){
    		valid=false;
    		$("#amterr").html("Please Enter Default Amount");
    	}
    	if(imageUrl.length == 0){
    		valid=false;
    		$("#uploaderr").html("Please Choose file");
    	}else{
    		valid=true;	
    	}

    if(valid == true) {
    	$("#formId").submit();
    } 
    
    var timeout = setTimeout(function(){
    	$("#nameerr").html("");
    	$("#amterr").html("");
    	$("#mccerr").html("");
    }, 4000);
    }
    </script>
    
    
    <script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>
    
    <script type="text/javascript">
function isAlphNumberKey(evt){
    var k = (evt.which) ? evt.which : evt.keyCode
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 190 || k == 188);
}
</script>

<script type="text/javascript">
function isAlphKey(evt){
	if (categoryName.length == 0 ){
		$("#nameerr").html("Please enter Category Name");
		valid = false;
	}
	if(categoryName.length <= 10 && categoryName.match(namepattern)){
		$("#nameerr").html("Please enter Category Name");
		valid = true;
	}
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
}
</script>
    
<script type="text/javascript">
function checklength(evnt){
	var namepattern = "[A-Za-z]";
	if(Window.event){
		if(categoryName.length == 0){
			$("#nameerr").html("Plaease Enter Category Name");
			valid = false;
		}
		else if(categoryName.length >10 && categoryName.match(namepattern)){
			$("#nameerr").html("Category Name Should be with in 10 Characters");
		}else
			valid = true;
	}
}

</script>

    
    </body>

</html>