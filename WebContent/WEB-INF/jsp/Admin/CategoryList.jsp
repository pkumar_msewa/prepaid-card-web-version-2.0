<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <title>Admin | Category List</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">

        <!-- Table Export -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/table_export/css/tableexport.css" rel="stylesheet" type="text/css" />
        
        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />
		<script> var contextPath="${pageContext.request.contextPath}";</script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
         <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
         <script src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.min.js"></script>
    
    <script>var contextPath="${pageContext.request.contextPath}";</script>
   		<script src="${pageContext.request.contextPath}/resources/assets-group/js/customercarepost.js"></script>
    <script type="text/javascript">
    var context_path="${pageContext.request.contextPath}";
    </script>

    </head>


    <body oncontextmenu="return false" >
        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
           <jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
          
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

					<div class="row">
						<div class="col-12">
							<div class="page-title-box" style="padding: 32px 20px;">
								<h3 class="page-title float-left">List of Categories</h3>
							</div>
						</div>
					</div>
					<!-- end row -->
					
					<div class="row">
						<div class="col-12">
							<div class="card-box">
								<div class="row">
									<div class="col-12">
										<div class="table-responsive">
											<table class="table table-striped table-bordered" width="100%">
												<thead>
													<tr>
														<th id="id">ID</th>
									                    <th id=name>Name</th>
									                    <th id="amount">Default Amount</th>
									                    <th id="factor">Factor</th>
									                    <th id ="mcode">MCC</th>
									                    <th id="country">Country</th>
									                    <th id ="edit">Edit</th>
									                    <th>Delete</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${Categories}" var="fields" varStatus="loop">
														<tr id="content">
															<td>${loop.count}</td>
															<td>
															<input type= "hidden" id="d_name${loop.count}" value = "${fields.name}"/>${fields.name}</td>
															<td><input type= "text" id="d_amount${loop.count}" value = "${fields.default_amount}" disabled/></td>
															<td>
															<input type= "hidden" id="d_fact${loop.count}" value = "${fields.factor}"/>${fields.factor}
															</td>
															<td>
															<input type= "hidden"  id="d_mcc${loop.count}" value = "${fields.mcc}"/>
															${fields.mcc}</td>
															<td>
															<input type= "hidden"  id="d_country${loop.count}" value = "${fields.country}"/>
															${fields.country}</td>
															<td>
																<button class="edit_me" type="button" id="edit${loop.count}" onclick="setvalue('${loop.count}')">Edit</button>
																<div style="display: none;">
																	<button class="btn_sav" type="button" onclick="saveData('d_name${loop.count}','d_amount${loop.count}','d_fact${loop.count}','d_mcc${loop.count}','d_country${loop.count}')">Save</button>
																	<button class="btn_cncl" type="button">Cancel</button>
																</div>
															</td>
															<td ><input type= "button"  id="d_dlt${loop.count}" value = "Delete" onclick="deletepocket('d_name${loop.count}')"/>
															</td>
														</tr>
													</c:forEach>
												</tbody>											
											</table>
											<nav style="float: right;">
												<ul class="pagination" id="paginationn"></ul>
											</nav>
										</div>						
									</div>								
								</div>							
							</div>						
						</div>					
					</div>
                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2017 © Copyright EWire.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->

         <script type="text/javascript">
         function submit(a){
        	 
        	 console.log("row value is  "+a);
        	 
         }
         
         </script>
         
         <script type="text/javascript">
		
         function setvalue(val){
        	 var amount='d_amount'+val;
        	 var tab='tab'+val;
        	 console.log(tab);
        	 console.log(amount);
        	 var name='d_name'+val;
        	 document.getElementById(amount).removeAttribute("disabled");
         }
         
         </script>
         
         <script type="text/javascript">
         
         function deletepocket(dname){
        	 var deleteName = $("#"+dname).val(); 
        	 
        	 console.log("  "+dname)
        	 
        	 console.log("Delete name   "+deleteName) 
        	 $.ajax({
        		type:"DELETE",
        		url:"${pageContext.request.contextPath}/Admin/DeleteCategory",
        		dataType : "json",
                contentType : "application/json",
                data : JSON.stringify({
                    "name":deleteName,
                }),
                success:function(){
        			window.location.href="${pageContext.request.contextPath}/Admin/GetCategories";
        		}
         })
      }
         
         </script>

        <script type="text/javascript">
        
        function saveData(name,balance){
        	console.log(name+ " " +balance)
        	var uniquename= $("#"+name).val();
        	var amount= $("#"+balance).val();
        
        	
        	console.log("information:: "+amount);
        	console.log("uniquename:: "+uniquename);
        	
        	$.ajax({
        		type:"PUT",
        		url:"${pageContext.request.contextPath}/Admin/UpdateCategory",
        		data:{
        			default_amount : amount,
        			name : uniquename,
        		
        			},
        		dataType:"json",
        		success:function(){
        			console.log("data saved successfully")
        			
        		}
        	})
        }
        </script>

        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
	
		 <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
		<!-- Include Date Range Picker -->
		<script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
		
		<!-- Table Export js -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/admin/assets/table_export/js/FileSaver.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/admin/assets/table_export/js/tableexport.js"></script>
		
        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>

    </body>
		
		<script type="text/javascript">
		$(document).ready(function() {
			$('.edit_me').click(function() {
				$(this).hide();
				$(this).next().show();
				$('.my_amt').hide();
				$('input[name="edit_field"]').attr('type', 'text').focus();
			});

			// ========== Cancel The Editing ===============
			$('.btn_cncl').click(function() {
				$(this).parent().hide();
				$('.my_amt').show();
				$('input[name="edit_field"]').attr('type', 'hidden');
				$('.edit_me').show();
			});

			
		})
	</script>
		
		
<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>   
      
</html>