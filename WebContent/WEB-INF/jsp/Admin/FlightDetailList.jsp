<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Flight Detail</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta content="" name="description" />
<meta content="" name="author" />
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title>Bus Details</title>
<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">

<!-- DataTables -->
<link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

<!-- App css -->
<link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
<script>var contextPath="${pageContext.request.contextPath}";</script>
<script src="${pageContext.request.contextPath}/resources/assets-group/js/customercarepost.js"></script>
<style>
.se-pre-con {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(${pageContext.request.contextPath}/resources/spinner.gif) center no-repeat #fff;
		}
</style>
</head>
<body oncontextmenu="return false">
<div id="wrapper">

            <!-- Top Bar Start -->
           <jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
          
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Flight Detail List</h4>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
						
						<!-- datepicker strats here -->
						
						<div class="row">
							<div class="col-12">
								<div class="card-box">
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
										
											<form action="<c:url value="/Admin/FlightDetailList"/>" 
												method="post" class="form form-inline">
												
												<div class="form-group">
													<div id="" class="pull-left" style="cursor: pointer;">
														<label class="" for="filterBy" style="justify-content: left;">Filter By:</label>
														<input id="reportrange" name="reportrange" class="form-control"  value="${dat}" readonly="readonly"/>
													</div>
												</div>
												<div class="form-group">
													<button type="submit" class="btn btn-primary"
														title="Search" style="margin-left: 10px; margin-top: 20px;">
														Search
													</button>
												</div>
											</form>
																		
										</div>
									</div>
								</div>
							</div>
						</div>

                        <div class="row">
                            <div class="col-12">
                                <div class="card-box table-responsive">
                                    <!-- <h4 class="m-t-0 header-title"><b>Default Example</b></h4>
                                    <p class="text-muted font-14 m-b-30">
                                        DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>.
                                    </p> -->

                                    <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                              <th>S.No</th>
												<th>User Details</th>
												<th>User Contact Details</th>
												<th>Transaction ID</th>
												<th>Transaction Date</th>
												<th>Booking Ref No.</th>
												<th>Booking Status</th>
												<th>Transaction Status</th>
												<th>Amount</th>
												<th>Account No.</th>
												<th>Flight Onward</th>
												<th>Flight Return</th>
												<th>Trip Type</th>
												<th>Commission</th>
												<th>Ticket Details</th>
												<th>Ticket PDF</th>														
                                            </tr>
                                        </thead>
                                      <tbody id=userList>
											<c:forEach items="${FlightDeatils}" var="slist" varStatus="loopCount">
											<tr>
											<td>${loopCount.count}</td>
											<td> ${slist.userDetails}</td>
											<td><c:out value="${slist.userName}"></c:out>
											</td>
											<td> <c:choose><c:when test="${slist.transactionRefNo != 'null' }">
											${slist.transactionRefNo}
											</c:when>
											<c:otherwise>
											 NA
										    </c:otherwise>
											</c:choose></td>
											<td>${slist.txnDate}</td>
											<td><c:choose><c:when test="${slist.bookingRefId != 'null' }">
											${slist.bookingRefId}
											</c:when>
											<c:otherwise>
											 NA
										    </c:otherwise>
											</c:choose></td>
											<td><c:choose><c:when test="${slist.bookingStatus != 'null' }">
											${slist.bookingStatus}
											</c:when>
											<c:otherwise>
											 NA
										    </c:otherwise>
											</c:choose></td>
											<td><c:choose><c:when test="${slist.transactionStatus != 'null' }">
											${slist.transactionStatus}
											</c:when>
											<c:otherwise>
											 NA
										    </c:otherwise>
											</c:choose></td>
											<td>
											${slist.paymentAmount}
											</td>
											<td>
											${slist.accountNumber}
										   </td>
										   <td>${slist.flightNameOnward}</td>
										   <td>${slist.flightNameReturn}</td>
										   <td>${slist.tripType}</td>
											<td>
											<c:choose>
											<c:when test="${slist.commsissionAmount>0}">
												${slist.commsissionAmount}
											</c:when>
											<c:otherwise>
												0
											</c:otherwise>
											</c:choose>
											
											</td>
					 						<td>
											<button id="view_ticket" type="button" style="background-color: #5cb85c; color: #fff;"
											onclick="viewTicketDetails('${slist.flightTicketId}')">View</button>
											</td>
											<td>
											<c:choose>
											<c:when test="${slist.bookingStatus== 'Booked'}">
											<form action="${pageContext.request.contextPath}/Admin/VpayQwikFlightTicket" target="new"  method="post">
											<input type="hidden" id="flightTicketId" name="flightTicketId" value="${slist.flightTicketId}"/>
											<button id="Pdf_ticket${loopCount.count}"  type="submit"  style="background-color: #5cb85c; color: #fff;">Download</button>
											</form>	
											</c:when>
											<c:otherwise>
												NA
											</c:otherwise>
											</c:choose>
											</td>
											
											</c:forEach>
										</tbody>
                                    </table>
                                </div>
                            </div>
                        </div> <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    &copy; <script>document.write(new Date().getFullYear());</script> Copyright EWire.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->
        
        	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix"
			data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>


  <!-- Modal -->
  <div class="modal fade" id="ErrmyModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="margin-top: 40%;">
       
        <div class="modal-body">
          <center>
<%-- 		  <img src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/search.gif" style="width: 150px; margin-top: -91px;"> --%>
        <div style="background: white; margin-top: -20px; padding: 2.5% 2%; padding-top: 32px;">
          <h3 id="payErrMsgVal"><b><br><small></b></small></h3>
        </div></center>
        </div>
       
      </div>
      
    </div>
  </div>
  
  
  <!-- Flight -->

	<!-- ========================= Modal For Traveler Details ==================================== -->
	<div id="travellerDetails" class="modal fade" role="dialog"
		hidden="hidden">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">

				<div class="modal-body">
					<button type="button" class="close close2" data-dismiss="modal"
						id="clsesits">&times;</button>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<img src="${pageContext.request.contextPath}/resources/images/User/travel/img/airline/plane.png" style="width: 35px;">
							<span class="modal-title bookinghead" id="m_flight" style="font-size: 18px;"><b>Flight Details</b>&nbsp;<i id="collaps" class="fa fa-plus-square-o" data-toggle="collapse" data-target="#flgt_dtls"></i></span>

						</div>
						
						<div id="flgt_dtls" class="collapse">
    						<div id="flDtls"></div>
  						</div>
						
					</div>
					
					
					<hr style="margin-top: 5px;">
                     <h4>Traveler Details</h4>
					<div class="row">
						<div class="row">
							<div class="col-md-12">
								<!-- TABLE HOVER -->
								<!-- page content -->
								<table id="editedtable" class="table table-striped ">
									<tr id="xyz">
										<th>SL.NO</th>
										<th>First Name</th>
										<th>Last Name</th>
										<th>Age</th>
										<th>Gender</th>
										<th>Seat No.</th>
										<th>Fare</th>
									</tr>
								</table>
								<nav>
									<ul class="pagination" id="paginationn">
									</ul>
								</nav>
								<!-- End Page Content -->
								<!-- END TABLE HOVER -->

								<!-- <span style="color: red; padding-left: 696px;">Refunded Amount: </span><span
										id="refAmt"></span><br>
										<span style="color: red; padding-left: 696px;">Cancellation  Charge: </span><span
										id="cnclAmt"></span> -->
										
								<%-- <center>
									<br><input type="hidden" id="bookingRefNo">
									<input type="hidden" id="email">
									<button type="button" id="flDetails"  style="background-color: #d43f3a; border-color: #d43f3a;color: #FFFFFF;">Get Flight Details</button>
								</center> --%>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	
	
	
        <!-- jQuery  -->
        
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
        
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        
        <!-- Include Date Range Picker -->
		<script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>


        <!-- Required datatable js -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.js"></script>
        <%-- <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/jszip.min.js"></script> --%>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/pdfmake.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/vfs_fonts.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.html5.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.print.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
        <!-- Responsive examples -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('#datatable').DataTable({
                    responsive: true
                });

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
            } );
		
        </script>
        
        <script>
        $(document).ready(function() {
            var table = $('#example').DataTable( {
                lengthChange: false,
                buttons: [ 'excel', 'pdf', 'csv' ]
            } );
         
            table.buttons().container()
                .appendTo( '#example_wrapper .col-md-6:eq(0)' );
        } );
        
        </script>
        
               
        <script>
		$(function() {
		
		    var start = moment().subtract(29, 'days');
		    var end = moment();
		
		    function cb(start, end) {
		        $('#reportrange').html(start.format('MM-dd-yyyy') + ' - ' + end.format('MM-dd-yyyy'));
		    }
		
		    $('#reportrange').daterangepicker({
		        startDate: start,
		        endDate: end,
		        locale: {
		        	format: 'YYYY-MM-DD'
		        },
		        dateLimit: {
		            "days": 30
		        },
		        ranges: {
		           'Today': [moment(), moment()],
		           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		           'This Month': [moment().startOf('month'), moment().endOf('month')],
		           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		        }
		    }, cb);
		
		    cb(start, end);
		    
		});
		</script>
	
	<script type="text/javascript">
    function viewTicketDetails(flightTicketId) {
    	 $('#flDtls').html('');
    	$("#axz").show();
    	 $("#cncl_msg").hide();
    	 $("#cnclTckt").hide();
    	 $('#editedtable').html('<tr class="testingg"><th>SL.NO</th><th>First Name</th><th>Last Name</th><th>Gender</th><th>Ticket No.</th><th>Passenger Type</th></tr>');
    	var contextPath = "${pageContext.request.contextPath}";
    	var spinnerUrl = "Please wait <img src="+contextPath+"'/resources/images/spinner.gif' style='width:20px; hight:10px;'>"
    	$("#view_ticket").addClass("disabled");
    	$("#view_ticket").html(spinnerUrl);
     	 $.ajax({
   	       type : "POST",
   	        contentType : "application/json",
   	        url : "${pageContext.request.contextPath}/Admin/getSingleFlightTicketDetails",
   			dataType : 'json',
   			data :JSON.stringify({
   				"flightTicketId" : flightTicketId,
   			}),
   	       success : function(response) {
   	    	$(".se-pre-con").fadeOut("slow");
   	       console.log("Response :: "+response.code);
   	       
   	       if (response.code.includes("S00")) {
		
   	  	   var trHTML='';
   	  	   
   	       for (var i = 0; i < response.details.travellerDetails.length; i++) {
   	    	   
   	    	console.log("First Name: "+response.details.travellerDetails[i].fName);
   	    	var trHTML='';
   	    	 trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>' + response.details.travellerDetails[i].fName + '</td><td>' + response.details.travellerDetails[i].lName + '</td><td>' + response.details.travellerDetails[i].gender + '</td><td>'+ response.details.travellerDetails[i].ticketNo +'</td><td>' + response.details.travellerDetails[i].type + '</td></tr>';
   	    	 
   	    	$('#bookingRefNo').val(response.details.bookingRefNo);
   	    	/* console.log("abc:: "+response.details.bookingRefNo); */
   	    	$('#email').val(response.details.email);
   	    	 
   	    	$('#editedtable').append(trHTML);
   	    	
		  }
   	      
   	   		 console.log("First Name: "+response.details.ticketsResp.Tickets.Oneway[0].origin); 
   	    
   	   	   for (var i = 0; i < response.details.ticketsResp.Tickets.Oneway.length; i++) {
	    	   
   	   		
		    $('#flDtls').append('<br><div class="row" style="margin-right: 0px; margin-left: 0px; background: #fff; margin-top: 10px; width: 100%;">'
		   	   		+'<div class="lftside" style="width: 45%; float: left; padding-left: 20px; text-align: left;">'
					+'<span style="font-size: 15px; font-weight: 600; color: #737373;">'+response.details.ticketsResp.Tickets.Oneway[i].origin+'</span><br>'
					+'<span style="font-size: 12px;">'+response.details.ticketsResp.Tickets.Oneway[i].departureDate+'</span><br>'
					+'<span style="font-size: 12px;">'+response.details.ticketsResp.Tickets.Oneway[i].cabin+'</span><br>'
				    +'<span style="font-size: 14px;">'+response.details.ticketsResp.Tickets.Oneway[i].airlineName+ '-'+response.details.ticketsResp.Tickets.Oneway[i].flightNumber+'</span><br>'
				    +'<span style="font-size: 12px;">'+ response.details.ticketsResp.Tickets.Oneway[i].departureTime+'</span><br>'
					+'</div>'
					+'<div class="cntr" style="width: 10%; float: left; padding-left: 20px;">'
					+'<span style="font-size: 13px;">'+response.details.ticketsResp.Tickets.Oneway[i].duration+'</span>'
					+'</div>'
					+'<div class="r8side" style="width: 45%; float: left; text-align: right; padding-right: 20px; padding-bottom: 15px;">'
					+'<span style="font-size: 15px; font-weight: 600; color: #737373;">'+response.details.ticketsResp.Tickets.Oneway[i].destination+'</span><br>'
					+'<span style="font-size: 12px;">'+response.details.ticketsResp.Tickets.Oneway[i].departureDate+'</span><br>'
					+'<span style="font-size: 12px;">'+ response.details.ticketsResp.Tickets.Oneway[i].cabin+'</span><br>'
				    +'<span style="font-size: 14px;">'+response.details.ticketsResp.Tickets.Oneway[i].airlineName+ '-' +response.details.ticketsResp.Tickets.Oneway[i].flightNumber+'</span><br>'
				    +'<span style="font-size: 12px;">'+response.details.ticketsResp.Tickets.Oneway[i].arrivalTime+'</span><br>'
				    +'</div><br><br> </div>'); 
		   } 
   	   	   
   	   	   if (response.details.ticketsResp.Tickets.Roundway.length>0) {
   	   		$('#flDtls').append('<center><h4><b>Return</b></h4></center>');
			}
   	   	   
   	   	   
			for (var i = 0; i < response.details.ticketsResp.Tickets.Roundway.length; i++) {
   	   		
		    $('#flDtls').append('<br><div class="row" style="margin-right: 0px; margin-left: 0px; background: #fff; margin-top: 10px; width: 100%;">'
			   		+'<div class="lftside" style="width: 45%; float: left; padding-left: 20px; text-align: left;">'
					+'<span style="font-size: 15px; font-weight: 600; color: #737373;">'+response.details.ticketsResp.Tickets.Roundway[i].origin+'</span><br>'
					+'<span style="font-size: 12px;">'+response.details.ticketsResp.Tickets.Roundway[i].departureDate+'</span><br>'
					+'<span style="font-size: 12px;">'+response.details.ticketsResp.Tickets.Roundway[i].cabin+'</span><br>'
				    +'<span style="font-size: 14px;">'+response.details.ticketsResp.Tickets.Roundway[i].airlineName+ '-'+response.details.ticketsResp.Tickets.Roundway[i].flightNumber+'</span><br>'
				    +'<span style="font-size: 12px;">'+ response.details.ticketsResp.Tickets.Roundway[i].departureTime+'</span><br>'
					+'</div>'
					+'<div class="cntr" style="width: 10%; float: left; padding-left: 20px;">'
					+'<span style="font-size: 13px;">'+response.details.ticketsResp.Tickets.Roundway[i].duration+'</span>'
					+'</div>'
					+'<div class="r8side" style="width: 45%; float: left; text-align: right; padding-right: 20px; padding-bottom: 15px;">'
					+'<span style="font-size: 15px; font-weight: 600; color: #737373;">'+response.details.ticketsResp.Tickets.Roundway[i].destination+'</span><br>'
					+'<span style="font-size: 12px;">'+response.details.ticketsResp.Tickets.Roundway[i].departureDate+'</span><br>'
					+'<span style="font-size: 12px;">'+ response.details.ticketsResp.Tickets.Roundway[i].cabin+'</span><br>'
				    +'<span style="font-size: 14px;">'+response.details.ticketsResp.Tickets.Roundway[i].airlineName+ '-' +response.details.ticketsResp.Tickets.Roundway[i].flightNumber+'</span><br>'
				    +'<span style="font-size: 12px;">'+response.details.ticketsResp.Tickets.Roundway[i].arrivalTime+'</span><br>'
				    +'</div><br><br> </div>'); 
		   }
   	   	   
   	 		$("#view_ticket").addClass("removeClass");
    		$("#view_ticket").html("View");
   	 	    $("#travellerDetails").modal('show'); 
   	       }
   	       else{
   	    	   
   	        $('#payErrMsgVal').html("Ticket details not available");	
   	    	$("#ErrmyModal").modal('show'); 
   	    	location.reload();	 
   	       }
   	 	   
   	       } 
   	   }); 
      }
    
    
    
    $("#flDetails").click(function () {
    	
   	 $.ajax({
 	       type : "POST",
 	        contentType : "application/json",
 	        url : "${pageContext.request.contextPath}/Admin/getFlightDetails",
 			dataType : 'json',
 			data :JSON.stringify({
 				"bookingRefId" : flightTicketId,
 				"email" : flightTicketId,
 			}),
 	       success : function(response) {
 	    	
 	       console.log("Response :: "+response.code);
 	       
 	       if (response.code.includes("S00")) {
 	  	   
 	       }
 	       } 
 	   }); 
		
	});  
    
	</script>
	
	<script>
		function validateDate(){
			var fromDate=$('#fromDate').val();
			var toDate=$('#toDate').val();
			if (fromDate.length <=0 || toDate.length <=0) {
				$('#error_msg').html("Please select start & end date");
				return false;
			}
			var date1=Date.parse(fromDate);
			var date2= Date.parse(toDate);
			var timeDiff=date2-date1;
			daysDiff = Math.floor(timeDiff / (1000 * 60 * 60 * 24));
			/* console.log("days diff"+daysDiff); */
			if (daysDiff <0  || daysDiff >30) {
			$('#error_msg').html("Date should be between 30 days")
			return false;
			}
			/* console.log(date1+":"+date2+" sub: "+(date2-date1));
			console.log(date1+":"+date2); */
			 var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
			 $('#fromDate2').val(Base64.encode(fromDate));
			 $('#toDate2').val(Base64.encode(toDate));
			return true;
		}
	</script>
  
  
</body>
</html>