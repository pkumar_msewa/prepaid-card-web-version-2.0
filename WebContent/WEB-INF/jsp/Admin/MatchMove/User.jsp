<!doctype html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <title>User Profile</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/linearicons/style.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/chartist/css/chartist-custom.css">
    <!-- MAIN CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/main.css">
    <!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/demo.css">
    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
    <!-- ICONS -->
    <link rel="apple-touch-icon" sizes="76x76" href="${pageContext.request.contextPath}/resources/assets/img/apple-icon.png">
    <link rel="icon" type="image/png" sizes="96x96" href="${pageContext.request.contextPath}/resources/assets/img/favicon.png">
    <link
            href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
            rel="stylesheet" type="text/css" />
    <link
            href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
            rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<c:url value="/resources/assets/vendor/bootstrap/css/bootstrap.min.css"/>">
    <link
            href="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.css"
            rel="stylesheet" type="text/css" />

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
 <script type="text/javascript">
        window.history.forward();
        function noBack() {
            window.history.forward();
        }
    </script>

</head>

<body>
<!-- WRAPPER -->
<div id="wrapper">
    <!-- HEADER -->
    <jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
    <jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <div class="panel panel-profile">
                    <div class="clearfix">
                        <!-- LEFT COLUMN -->
                        <div class="profile-left">
                            <!-- PROFILE HEADER -->
                            <div class="profile-header">
                                <div class="overlay"></div>
                                <div class="profile-user">
                                    <center>
                                        <div class="user-img">
                                            <img src="${pageContext.request.contextPath}\resources\admin\images\default.png" class="img-responsive" alt="Avatar">
                                        </div>
                                    </center>
                                    <h3 class="name"><span><c:out value="${user.firstName}" escapeXml="true"/></span>&nbsp;<span><c:out value="${user.lastName}" escapeXml="true"/></span></h3>
                                </div>
                                <div class="profile-stat" style="border-bottom: 1px solid #33bbff;">
                                    <div class="row">
                                        <div class="col-md-12 stat-item">
                                             <span style="letter-spacing: 0.5pt; font-weight: 600;"><c:out value="${user.mmUserId}" escapeXml="true"/></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="profile-stat">
                                    <div class="row">
                                        <div class="col-md-6 stat-item">
                                            Balance <span><c:out value="${user.fundAvailableAmt}" escapeXml="true"/></span>
                                        </div>
                                        <div class="col-md-6 stat-item">
                                            Status
                                            <c:if test="${user.status eq true}">
                                                <span><b>ACTIVE</b></span>
                                            </c:if>
                                            <c:if test="${user.status eq false}">
                                                <span><b>INACTIVE</b></span>
                                            </c:if>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END PROFILE HEADER -->
                            <!-- PROFILE DETAIL -->
                            <div class="profile-detail">
                                <div class="profile-info">
                                    <%--<h4 class="heading">User Info</h4>--%>
                                    <ul class="list-unstyled list-justify">
                                        <li><i class="fa fa-envelope fa-2x"></i> <span><c:out value="${user.email}" escapeXml="true"/></span></li>
                                        <li><i class="material-icons" style="font-size:36px; margin-left: -5px;">phone_in_talk</i> <span>+<c:out value="${user.mobileCountryCode}" escapeXml="true"/>-<c:out value="${user.mobile}" escapeXml="true"/></span></li>
                                        <li><i class="fa fa-calendar-plus-o fa-2x"></i> <span><c:out value="${user.issueDate}" escapeXml="true"/></span></li>
                                        <li><i class="material-icons" style="font-size:36px; margin-left: -5px;">account_balance_wallet</i> <span><c:out value="${user.walletId}" escapeXml="true"/></span></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- END PROFILE DETAIL -->
                        </div>
                        <!-- END LEFT COLUMN -->
                        <!-- RIGHT COLUMN -->
                        <div class="profile-right">
                            <c:if test="${user.client ne null}">
                            <div class="profile-info">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="client-img">
                                            <img src="http://paulpay.in/resources/images/logo.png" class="img-responsive" alt="client_logo">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div style="text-align: center; margin-top: 44px;">
                                            <span><c:out value="${user.client.account.user.userDetail.firstName}" escapeXml="true"/></span><span>-</span><span><c:out value="${user.client.account.accountType.country.code}" escapeXml="true"/></span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            </c:if>
                            <h4 class="heading" style="color: #0af;">Cards</h4>
                            <!-- CARD LIST -->
                            <div class="awards">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table id="editedtable"
                                               class="table table-striped table-bordered" style="text-align: center;">
                                            <thead>
                                            <tr>
                                                <th>S.NO</th>
                                                <th>Card ID</th>
                                                <th>Issue Date</th>
                                                <th>Valid Till</th>
                                                <th>Card Type</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <c:forEach items="${cards}" var="card">
                                                <tr>
                                                    <td><c:out value="${card.id}" escapeXml="true"/></td>
                                                    <td><a style="color: #00aaff;" href="<c:url value="/Admin/MatchMove/Card/${card.cardIdentifier}"/>" target="_blank"><c:out value="${card.cardIdentifier}" escapeXml="true"/></a></td>
                                                    <td><c:out value="${card.issueDate}" escapeXml="true"/></td>
                                                    <td><c:out value="${card.expiryDate}" escapeXml="true"/></td>
                                                    <td>
                                                        <c:if test="${card.cardTypes ne null}">
                                                            <span style="font-weight: 600">Code:</span>&nbsp;<span><c:out value="${card.cardTypes.cardCode}" escapeXml="true"/></span>
                                                            <span style="font-weight: 600">Desc.:</span>&nbsp;<span><c:out value="${card.cardTypes.description}" escapeXml="true"/></span>
                                                        </c:if>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END CARD LIST -->
                        </div>
                        <!-- END RIGHT COLUMN -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
</div>
<!-- END MAIN -->
<div class="clearfix"></div>
<footer>
    <div class="container-fluid">
        <p class="copyright">
            &copy; 2017 <a href="https://www.msewa.com" target="_blank">MSewa
            Software Solution Pvt. Ltd.</a>. All Rights Reserved.
        </p>
    </div>
</footer>
<!-- END WRAPPER -->
<!-- Javascript -->
<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/assets/vendor/chartist/js/chartist.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/assets/scripts/klorofil-common.js"></script>
<script src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.responsive.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.js"></script>



<script>
    $(document).ready(function() {
        $('#editedtable').DataTable({
            "bLengthChange": false,
            responsive: true,
            "bPaginate" : true,
            columnDefs : [ {
                orderable : true,
                targets : [ 0 ]
            }, {
                orderable : true,
                targets : [ 1 ]
            }, {
                orderable : true,
                targets : [ 2 ]
            }, {
                orderable : true,
                targets : [ 3 ]
            }, {
                orderable : true,
                targets : [ 4 ]
            }, {
                orderable : true,
                targets : [ 5 ]
            } ],
        });
        // $("#editedtable").removeClass("dataTable");
    });
</script>



</body>

</html>
