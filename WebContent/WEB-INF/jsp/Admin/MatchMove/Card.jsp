<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html lang="en">

<head>
	<title>Card Details</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/linearicons/style.css">

	<!-- MAIN CSS -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/main.css">
	
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Barlow+Semi+Condensed" rel="stylesheet">

	<link href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
	<link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />

	 <script type="text/javascript">
        window.history.forward();
        function noBack() {
            window.history.forward();
        }
    </script>
	


	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="${pageContext.request.contextPath}/resources/assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="${pageContext.request.contextPath}/resources/assets/img/favicon.png">

	<style>
		.card-wrp {
			margin-top: 30px;
			color: #1c1c1c;
		}
		.card-wrp .card-num {
			position: absolute;
			margin-top: 69px;
			margin-left: 19px;
			letter-spacing: 2pt;
			font-weight: 600;
			font-size: 19px;
		}
		.card-wrp .card-valid {
			position: absolute;
			margin-top: 103px;
			margin-left: 37px;
			letter-spacing: 2pt;
			font-size: 13px;
			font-weight: 900;
		}
		.card-wrp .card-cvv {
			position: absolute;
			margin-top: 103px;
			margin-left: 120px;
			letter-spacing: 2pt;
			font-weight: 900;
			font-size: 13px;
		}
		.card-wrp .card-nme {
			position: absolute;
			margin-top: 125px;
			margin-left: 19px;
			text-transform: uppercase;
			letter-spacing: 2pt;
			font-size: 18px;
			font-weight: 600;
		}
		.img_block {

		}

	</style>
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- HEADER -->
		<jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
		<!-- MAIN -->
		<div class="main">
			<c:set value="${card.matchMoveUser.client.account.user.username}" var="merchant_username"/>
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<div class="panel panel-profile">
						<div class="clearfix">
							<!-- LEFT COLUMN -->
							<div class="profile-left">
								<!-- PROFILE HEADER -->
								<div class="profile-header">
									<div class="overlay"></div>
									<div class="profile-main">
										<%--<div class="row top-logos">--%>
											<%--<div class="col-md-5">--%>
												<%--<img src="http://paulpay.in/resources/images/logo.png" class="img-responsive" alt="Client Logo">--%>
											<%--</div>--%>
											<%--&lt;%&ndash;<div class="col-md-5 pull-right">&ndash;%&gt;--%>
												<%--&lt;%&ndash;<img src="<c:url value="/resources/assets/img/fbank.jpg"/>" class="img-responsive" alt="Bank Logo" >&ndash;%&gt;--%>
											<%--&lt;%&ndash;</div>&ndash;%&gt;--%>
										<%--</div>--%>
										<div class="row">
											<div class="col-md-12">
												<div class="card-wrp">
													<div class="lft-logo">
														<div class="col-md-5">
															<c:if test="${merchant_username eq 'wallet@paulpay.com'}">
																<img src="<c:url value="/resources/assets/img/paul_logo.png"/>" class="img-responsive" alt="Client Logo">
															</c:if>
														</div>
													</div>
													<div class="card-num">
														<c:if test="${min.success eq true}">
															<c:forEach items="${min.cardDigits}" var="digit" varStatus="count">
																<span><c:out value="${digit}" escapeXml="true"/></span>&nbsp;
															</c:forEach>

														</c:if>
													</div>
													<div class="card-valid">
														<span><c:out value="${min.expiry}"/></span>
													</div>
													<div class="card-cvv">
														<span><c:out value="${tokens.secureCode}" escapeXml="true"/></span>
													</div>
													<div class="card-nme">
														<span><c:out value="${card.holderName}" escapeXml="true"/></span>
													</div>
												</div>
											</div>
										</div>
										<%--<div class="row">--%>
											<%--<div class="col-md-12">--%>
												<%--<span class="cardh-nme"></span>--%>
											<%--</div>--%>
										<%--</div>--%>
										<%--<div class="row">--%>
											<%--<div class="col-md-12">--%>
												<%--<div class="val-wrap">--%>
													<%--<span class="validity">valid thru</span><br>--%>
													<%--<span class="valid-dt"></span>--%>
												<%--</div>--%>
												<%--<div class="cvv-wrap">--%>
													<%--<span class="validity">CVV</span><br>--%>
													<%--<span class="valid-dt"></span>--%>
												<%--</div>--%>
											<%--</div>--%>
										<%--</div>--%>
									</div>
									<div class="profile-stat" style="border-bottom: 1px solid #33bbff;">
										<div class="row">
											<div class="col-md-12 stat-item">
												<span><c:out value="${card.cardIdentifier}" escapeXml="true"/></span>
											</div>
										</div>
									</div>
									<div class="profile-stat">
										<div class="row">
											<div class="col-md-6 stat-item">
												Limit <span><c:out value="${card.fundsWithholdingAmt}" escapeXml="true"/></span>
											</div>
											<div class="col-md-6 stat-item">
												Balance <span><c:out value="${card.fundsAvailableAmt}" escapeXml="true"/></span>
											</div>
										</div>

									</div>
								</div>
								<!-- END PROFILE HEADER -->
								<!-- PROFILE DETAIL -->
								<div class="profile-detail">
									<div class="profile-info">
										<center>
											<c:if test="${min.status eq 'blocked'}">
												<img src="<c:url value="/resources/assets/img/blocked.png"/>" width="300" height="150"/>
											</c:if>

											<c:if test="${min.status eq 'active'}">
												<div class="alert alert-success">ACTIVE</div>
											</c:if>
											<c:if test="${min.status eq 'locked'}">
												<div class="alert alert-warning">LOCKED</div>
											</c:if>
											<c:if test="${min.status eq 'lost'}">
												<div class="alert alert-danger">LOST</div>
											</c:if>
											<c:if test="${min.status eq 'damaged'}">
												<div class="alert alert-danger">DAMAGED</div>
											</c:if>
											<c:if test="${min.status eq 'stolen'}">
												<div class="alert alert-danger">STOLEN</div>
											</c:if>
										</center>
										<c:if test="${card.matchMoveUser ne null}">
										<h4 class="heading">User Info</h4>
										<ul class="list-unstyled list-justify">
											<li>First Name <span><c:out value="${card.matchMoveUser.firstName}"/></span></li>
											<li>Last Name <span><c:out value="${card.matchMoveUser.lastName}"/></span></li>
											<li>Preferred Name <span><c:out value="${card.matchMoveUser.preferredName}"/></span></li>
											<li>Wallet Id <span><c:out value="${card.matchMoveUser.walletId}"/></span></li>
											<li>User Id <span><c:out value="${card.matchMoveUser.mmUserId}"/></span></li>
											<li>Email <span><c:out value="${card.matchMoveUser.email}"/></span></li>
											<li>Mobile <span>+<c:out value="${card.matchMoveUser.mobileCountryCode}" escapeXml="true"/>-<c:out value="${card.matchMoveUser.mobile}"/></span></li>

										</ul>
										</c:if>
									</div>
									<div class="profile-info">
										<c:if test="${card.matchMoveUser.client ne null}">
										<h4 class="heading">Client Info</h4>
										<ul class="list-unstyled list-justify">
											<li> Name <span><c:out value="${card.matchMoveUser.client.account.user.userDetail.firstName}" escapeXml="true"/> - <c:out value="${card.matchMoveUser.client.account.accountType.country.code}" escapeXml="true"/></span></li>
											<li> Id <span><c:out value="${card.matchMoveUser.client.account.user.username}" escapeXml="true"/></span></li>
										</ul>
										</c:if>
									</div>

									<div class="text-center">

										<c:if test="${min.status eq 'locked'}">
											<a href="<c:url value="/Admin/MatchMove/UnblockCard/${card.cardIdentifier}"/>"  class="btn btn-success"><span class="glyphicon glyphicon-ok-circle"></span> Unblock </a>
										</c:if>

										<c:if test="${min.status eq 'active'}">
										<a href="#" data-toggle="modal" data-target="#block" class="btn btn-danger"><span class="glyphicon glyphicon-ban-circle"></span> Block </a>
										</c:if>
									</div>
								</div>
								<!-- END PROFILE DETAIL -->
							</div>
							<!-- END LEFT COLUMN -->
							<!-- RIGHT COLUMN -->
							<div class="profile-right">
								<h4 class="heading">Transactions</h4>
								<!-- AWARDS -->
								<div class="awards">
									<div class="row">
										<div class="col-md-12 col-sm-12">
											<table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
												<thead>
													<tr>
														<th>S.NO</th>
														<th>Amount</th>
														<th>Merchant</th>
														<th>Network</th>
														<th>MCC</th>
														<th>Merchant ID</th>
														<th>Terminal ID</th>
														<th>Transaction #</th>
														<th>RRN #</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${transactions}" var="txn">
													<tr>
														<td><c:out value="${txn.id}" escapeXml="true"/></td>
														<td><c:out value="${txn.amount}" escapeXml="true"/></td>
														<td><c:out value="${txn.merchant_name}" escapeXml="true"/></td>
														<td><c:out value="${txn.network}" escapeXml="true"/></td>
														<td><c:out value="${txn.mcc}" escapeXml="true"/></td>
														<td><c:out value="${txn.merchant_id}" escapeXml="true"/></td>
														<td><c:out value="${txn.terminal_id}" escapeXml="true"/></td>
														<td><c:out value="${txn.transactionId}" escapeXml="true"/></td>
														<td><c:out value="${txn.refernceNo}" escapeXml="true"/></td>

													</tr>
													</c:forEach>
												</tbody>
											</table>
											<%--<table id="editedtable"--%>
												   <%--class="table table-striped table-bordered" style="text-align: center;">--%>
												<%--<thead>--%>
												<%--<tr>--%>
													<%--<th>S.NO</th>--%>
													<%--<th>Amount</th>--%>
													<%--<th>Merchant</th>--%>
													<%--<th>Network</th>--%>
													<%--<th>MCC</th>--%>
													<%--<th>Merchant ID</th>--%>
													<%--<th>Terminal ID</th>--%>
													<%--<th>Transaction #</th>--%>
													<%--<th>RRN #</th>--%>
												<%--</tr>--%>
												<%--</thead>--%>
												<%--<tbody>--%>
												<%--<c:forEach items="${transactions}" var="txn">--%>
													<%--<tr>--%>
														<%--<td><c:out value="${txn.amount}" escapeXml="true"/></td>--%>
														<%--<td><c:out value="${txn.merchant_name}" escapeXml="true"/></td>--%>
														<%--<td><c:out value="${txn.network}" escapeXml="true"/></td>--%>
														<%--<td><c:out value="${txn.mcc}" escapeXml="true"/></td>--%>
														<%--<td><c:out value="${txn.merchant_id}" escapeXml="true"/></td>--%>
														<%--<td><c:out value="${txn.terminal_id}" escapeXml="true"/></td>--%>
														<%--<td><c:out value="${txn.transactionId}" escapeXml="true"/></td>--%>
														<%--<td><c:out value="${txn.refernceNo}" escapeXml="true"/></td>--%>
														<%--<td>ajsdfhja</td>--%>
													<%--</tr>--%>
												<%--</c:forEach>--%>
												<%--</tbody>--%>
											<%--</table>--%>
										</div>
										
									</div>
									<!-- <div class="text-center"><a href="#" class="btn btn-default">See all awards</a></div> -->
								</div>
								<!-- END AWARDS -->
								<!-- TABBED CONTENT -->

								<!-- END TABBED CONTENT -->
							</div>
							<!-- END RIGHT COLUMN -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<footer>
			<div class="container-fluid">
				<p class="copyright">
					&copy; 2017 <a href="https://www.msewa.com" target="_blank">MSewa
					Software Solution Pvt. Ltd.</a>. All Rights Reserved.
				</p>
			</div>
		</footer>
	</div>
	<!-- Modal for block -->
	<div id="block" class="modal fade" role="dialog">
		 <div class="modal-dialog">
			 <div class="modal-content">
				 <div class="modal-header">
					 <button type="button" class="close" data-dismiss="modal">&times;</button>
					 <h4 class="modal-title">Select an action</h4>
				 </div>
				 <div class="modal-body">
					 <center>
					 <form action="<c:url value="/Admin/MatchMove/BlockCard/${card.cardIdentifier}"/>" method="post" class="form form-inline">
						 <select name="type" class="form-control">
							 <option value="suspend">Suspend (Temporarily Block)</option>
							 <option value="lost">Lost (Permanent Block)</option>
							 <option value="stolen">Stolen (Permanent Block)</option>
							 <option value="damaged">Damaged (Permanent Block)</option>
						 </select>
						 <button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-ban-circle"></span> Block</button>
					 </form>
					 </center>
				 </div>

			 </div>
		 </div>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/scripts/klorofil-common.js"></script>

	<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
	<script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
	<script src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script>


	<script>
        $(document).ready(function() {
            $('#example').DataTable({
				responsive: true,
				details: false
			});
        } );
	</script>

</body>

</html>
