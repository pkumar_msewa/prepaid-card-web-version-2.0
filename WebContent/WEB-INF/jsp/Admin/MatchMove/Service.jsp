<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
<head>
	<title> Transactions </title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/linearicons/style.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/main.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="${pageContext.request.contextPath}/resources/assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="${pageContext.request.contextPath}/resources/assets/img/favicon.png">

 <script type="text/javascript">
        window.history.forward();
        function noBack() {
            window.history.forward();
        }
    </script>

	<style>
		.shape{
			border-style: solid; border-width: 0 70px 40px 0; float:right; height: 0px; width: 0px;
			-ms-transform:rotate(360deg); /* IE 9 */
			-o-transform: rotate(360deg);  /* Opera 10.5 */
			-webkit-transform:rotate(360deg); /* Safari and Chrome */
			transform:rotate(360deg);
		}
		.offer{
			background:#fff; border:1px solid #ddd; box-shadow: 0 10px 20px rgba(0, 0, 0, 0.2); margin: 15px 0; overflow:hidden;
		}
		.shape {
			border-color: rgba(255,255,255,0) #d9534f rgba(255,255,255,0) rgba(255,255,255,0);
		}
		.offer-radius{
			border-radius:7px;
		}
		.offer-danger {	border-color: #d9534f; }
		.offer-danger .shape{
			border-color: transparent #d9534f transparent transparent;
		}
		.offer-success {	border-color: #5cb85c; }
		.offer-success .shape{
			border-color: transparent #5cb85c transparent transparent;
		}
		.offer-default {	border-color: #999999; }
		.offer-default .shape{
			border-color: transparent #999999 transparent transparent;
		}
		.offer-primary {	border-color: #428bca; }
		.offer-primary .shape{
			border-color: transparent #428bca transparent transparent;
		}
		.offer-info {	border-color: #5bc0de; }
		.offer-info .shape{
			border-color: transparent #5bc0de transparent transparent;
		}
		.offer-warning {	border-color: #f0ad4e; }
		.offer-warning .shape{
			border-color: transparent #f0ad4e transparent transparent;
		}

		.shape-text{
			color:#fff;
			font-size:10px;
			font-weight:bold;
			position:relative;
			right:-44px;
			top: 3px;
			white-space: nowrap;
			-ms-transform:rotate(30deg); /* IE 9 */
			-o-transform: rotate(360deg);  /* Opera 10.5 */
			-webkit-transform:rotate(30deg); /* Safari and Chrome */
			transform:rotate(30deg);
		}
		.offer-content{
			padding:0 20px 10px;
		}
		@media (min-width: 487px) {
			.container {
				max-width: 750px;
			}
			.col-sm-6 {
				width: 50%;
			}
		}
		@media (min-width: 900px) {
			.container {
				max-width: 970px;
			}
			.col-md-4 {
				width: 33.33333333333333%;
			}
		}

		@media (min-width: 1200px) {
			.container {
				max-width: 1170px;
			}
			.col-lg-3 {
				width: 25%;
			}
		}

		.fw-600 {
			font-weight: 600;
		}
	</style>

</head>

<body>
<!-- WRAPPER -->
<div id="wrapper">
	<!-- HEADER -->
	<jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
	<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />

	<!-- MAIN -->
	<div class="main">
		<!-- MAIN CONTENT -->
		<div class="main-content">
			<div class="container-fluid">
				<!-- <h3 class="page-title">Panels</h3> -->
				<div class="row">
					<div class="col-md-8">
						<!-- PANEL HEADLINE -->
						<div class="offer offer-info">
							<div class="shape">
								<div class="shape-text">
									<i class="fa fa-info-circle fa-2x"></i>
								</div>
							</div>
							<div class="offer-content">
								<h3 class="lead fw-600">
									Service
								</h3>
								<p style="color: #249ce0;">${result.name} <span class="fa fa-cc-mastercard fa-2x"></span> (${result.description}) <img src="<c:url value="/resources/admin/images/MM.png"/>" style="width: 110px; float: right;"></p>

							</div>
						</div>
						<!-- END PANEL HEADLINE -->
					</div>
					<div class="col-md-4">
						<!-- PANEL NO PADDING -->
						<div class="offer offer-info">
							<div class="shape">
								<div class="shape-text">
									<i class="fa fa-cog fa-2x"></i>
								</div>
							</div>
							<div class="offer-content">
								<h3 class="lead fw-600">
									Service Type
								</h3>
								<p style="color: #249ce0;">${result.serviceType}</p>
							</div>
						</div>
						<!-- END PANEL NO PADDING -->
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<!-- PANEL NO PADDING -->
						<div class="offer offer-info">
							<div class="shape">
								<div class="shape-text">
									<i class="fa fa-qrcode fa-2x"></i>
								</div>
							</div>
							<div class="offer-content">
								<h3 class="lead fw-600">
									Service Code
								</h3>
								<p style="color: #249ce0;">${result.code}</p>
							</div>
						</div>
						<!-- END PANEL NO PADDING -->
					</div>
					<div class="col-md-4">
						<!-- PANEL NO PADDING -->
						<div class="offer offer-info">
							<div class="shape">
								<div class="shape-text">
									<i class="fa fa-headphones fa-2x"></i>
								</div>
							</div>
							<div class="offer-content">
								<h3 class="lead fw-600">
									Operator
								</h3>
								<img src="<c:url value="/resources/admin/images/MM_1.png"/>" style="width: 70px; margin-top: -16px;">
							</div>
						</div>
						<!-- END PANEL NO PADDING -->
					</div>
					<div class="col-md-4">
						<!-- PANEL NO PADDING -->
						<div class="offer offer-info">
							<div class="shape">
								<div class="shape-text">
									<i class="fa fa-user-circle-o fa-2x"></i>
								</div>
							</div>
							<div class="offer-content">
								<h3 class="lead fw-600">
									Status
								</h3>
								<c:if test="${result.status eq 'Active'}">
									<p><span class="label label-success">${result.status}</span></p>
								</c:if>
								<c:if test="${result.status eq 'Inactive'}">
									<p><span class="label label-danger">${result.status}</span></p>
								</c:if>
							</div>
						</div>
						<!-- END PANEL NO PADDING -->
					</div>
				</div>

				<div class="row">
					<div class="col-md-4">
						<!-- PANEL NO PADDING -->
						<a href="${pageContext.request.contextPath}/Admin/MatchMove/ClientServices">
							<div class="offer offer-info">
								<div class="shape">
									<div class="shape-text">
										<i class="fa fa-users fa-2x"></i>
									</div>
								</div>
								<div class="offer-content">
									<h3 class="lead fw-600">
										Clients
									</h3>
									<p>${result.nclients}</p>
								</div>
							</div>
						</a>
						<!-- END PANEL NO PADDING -->
					</div>
					<div class="col-md-4">
						<!-- PANEL NO PADDING -->
						<a href="${pageContext.request.contextPath}/Admin/MatchMove/MerchantServices">
							<div class="offer offer-info">
								<div class="shape">
									<div class="shape-text">
										<i class="fa fa-shopping-bag fa-2x"></i>
									</div>
								</div>
								<div class="offer-content">
									<h3 class="lead fw-600">
										Merchant Services
									</h3>
									<p>${result.mservices}</p>
								</div>
							</div>
						</a>
						<!-- END PANEL NO PADDING -->
					</div>
					<div class="col-md-4">
						<!-- PANEL NO PADDING -->
						<a href="${pageContext.request.contextPath}/Admin/MatchMove/Transactions">
							<div class="offer offer-info">
								<div class="shape">
									<div class="shape-text">
										<i class="fa fa-bar-chart-o fa-2x"></i>
									</div>
								</div>
								<div class="offer-content">
									<h3 class="lead fw-600">
										Transactions
									</h3>
									<p>${result.ntransactions}</p>
								</div>
							</div>
						</a>
						<!-- END PANEL NO PADDING -->
					</div>
				</div>
			</div>
		</div>
		<!-- END MAIN CONTENT -->
	</div>
	<!-- END MAIN -->
	<div class="clearfix"></div>

	<footer>
		<div class="container-fluid">
			<p class="copyright">
				&copy; 2017 <a href="https://www.msewa.com" target="_blank">MSewa
				Software Solution Pvt. Ltd.</a>. All Rights Reserved.
			</p>
		</div>
	</footer>


<!-- END WRAPPER -->
<!-- Javascript -->
<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/assets/scripts/klorofil-common.js"></script>

</body>
</html>