<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
	<title> Cards </title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/linearicons/style.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/chartist/css/chartist-custom.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="${pageContext.request.contextPath}/resources/assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="${pageContext.request.contextPath}/resources/assets/img/favicon.png">
	<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
	rel="stylesheet" type="text/css" />
	<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<c:url value="/resources/assets/vendor/bootstrap/css/bootstrap.min.css"/>">
	<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
 <script type="text/javascript">
        window.history.forward();
        function noBack() {
            window.history.forward();
        }
    </script>

</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- HEADER -->
		<jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
							<div class="panel panel-headline">
								<div class="panel-heading">
							<h3 class="panel-title">Cards</h3></div>
							
<!-- 							MODEL WINDOW -->

							<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
								aria-labelledby="exampleModalLabel" aria-hidden="true" style="padding-top: 4%;">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<div class="col-md-12" >
												<div class="col-md-8"  class="form-control">
											<h4 id="headingId"></h4>
											</div>
												<div class="col-md-4"  class="form-control" align="right">
											<button type="button" class="close" data-dismiss="modal"
												aria-label="Close">
												<span><i title ="Close"class="fa fa-times" aria-hidden="true"   style="color:red"></i></span>
											</button>	</div></div>
										</div>
									<div class="modal-body">
											<div class="row">
												<div class="col-md-12">
													<!-- TABLE HOVER -->
													<!-- page content -->
													<table id="modelTable"
														class="table table-striped table-bordered date_sorted">
														<thead>
															<tr>
															</tr>
														</thead>
													</table>
												</div>
											</div>

									</div>
								</div>
								</div>
							</div>
							
							
<!-- 							MODEL WINDOW -->
							<input type="hidden" name="val" value="" id="usernameId">
							<input type="hidden" name="service" value="All" >

							
						<div class="panel-body" style="background: #eaeaea;">
                            <form id="formId" action="<c:url value="/Admin/MatchMove/Cards"/>" method="post" >

                            <div class="row">
								<div class="col-md-12">
									<h4
											style="position: absolute; font-size: 14px; font-weight: 600; margin-top: 2px;">Select
										Date Range*</h4>
									<div class="col-md-4 col-sm-4 col-xs-3" id="reportrange"
										 style="background: #fff; cursor: pointer; border: 1px solid #ccc; margin-top: 26px; padding: 5px; border-radius: 4px;">
										<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
										<span></span> <b class="caret"></b>
									</div>

									<input type="hidden" id="daterange" name="daterange" value=""
										   class="form-control" readonly />

									<div class="col-md-3 col-sm-3 col-xs-3">
										<label>Clients*</label> <select id="userId" name="val"
																		class="form-control" >
										<option value="All">All Clients</option>
										<c:if test="${clients ne null}">
											<c:forEach var="client"
													   items="${clients}">
												<option value="${client.id}"
													${fn:contains(id,client.id) ? 'selected="selected"' : ''}>
													<c:out value="${client.clientName}" /> - <c:out value="${client.code}" /></option>
											</c:forEach>
										</c:if>
									</select>
										<p id="userMsg"></p>
									</div>

									<div class="col-md-2 col-sm-2 col-xs-2">
										<button type="submit" class="btn btn-primary" title="Search"
												style="margin-top: 25px;">
											<span class="glyphicon glyphicon-filter"></span>
										</button>
									</div>
								</div>
							</div>
                            </form>

						<div class="row">
								<div class="col-md-12" >
								<table id="editedtable"
									class="table table-striped table-bordered" style="text-align: center;">
									<thead>
										<tr>
											<th>S.NO</th>
                                            <th>Card ID</th>
											<th>User Details</th>
											<th>Client Details</th>
											<th>Issue Date</th>
											<th>Valid Till</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${cards}" var="card">
											<tr>
												<td><c:out value="${card.id}" escapeXml="true"/></td>
												<td><a href="<c:url value="/Admin/MatchMove/Card/${card.cardIdentifier}"/>"><c:out value="${card.cardIdentifier}" escapeXml="true"/></a></td>
												<td>
                                                    <c:if test="${card.matchMoveUser ne null}">
                                                        <span><b>First Name </b></span><br> <c:out value="${card.matchMoveUser.firstName}" escapeXml="true"/><br/>
                                                        <span><b>Last Name </b></span><br> <c:out value="${card.matchMoveUser.lastName}" escapeXml="true"/><br/>
                                                        <span><b>Preferred Name </b></span><br> <c:out value="${card.matchMoveUser.preferredName}" escapeXml="true"/><br/>
                                                        <span><b>Mobile No </b></span><br> <c:out value="${card.matchMoveUser.mobile}" escapeXml="true"/><br/>
                                                        <span><b>Email </b></span><br> <c:out value="${card.matchMoveUser.email}" escapeXml="true"/>
                                                    </c:if>
												</td>
												<td>
                                                    <c:if test="${card.cardTypes ne null}">
                                                        <c:if test="${card.cardTypes.detail ne null}">
                                                            <span><b>Name </b></span><br> <c:out value="${card.cardTypes.detail.account.user.userDetail.firstName}" escapeXml="true"/> - <c:out value="${card.cardTypes.detail.account.accountType.country.code}" escapeXml="true"/><br/>
                                                            <span><b>Username </b></span><br> <c:out value="${card.cardTypes.detail.account.user.username}" escapeXml="true"/>
                                                        </c:if>
                                                    </c:if>
												</td>
												<td><c:out value="${card.issueDate}" escapeXml="true"/></td>
												<td><c:out value="${card.expiryDate}" escapeXml="true"/></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div></div>
								</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
			<!-- END MAIN -->
			<div class="clearfix"></div>
			<footer>
				<div class="container-fluid">
					<p class="copyright">
						&copy; 2017 <a href="https://www.msewa.com" target="_blank">MSewa
							Software Solution Pvt. Ltd.</a>. All Rights Reserved.
					</p>
				</div>
			</footer>
		<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/chartist/js/chartist.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/scripts/klorofil-common.js"></script>
	 <script src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
<script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.responsive.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.js"></script>

	<!-- Daterange picker -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jsdeliver/moment.min.js"></script>
	<%--<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/js/jsdeliver/bootstrap.css" />--%>

	<!-- Include Date Range Picker -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jsdeliver/daterangepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/js/jsdeliver/daterangepicker.css" />


	<script>
		$(document).ready(function() {
			$('#editedtable').DataTable({
				 "bLengthChange": false,
				responsive: true,
				 "bPaginate" : true,
                "ordering":false
			});
			// $("#editedtable").removeClass("dataTable");
		});
	</script>

	<script type="text/javascript">
        $(function() {

            var start = moment().subtract(29, 'days');
            var end = moment();
            <c:if test="${startDate ne null}">
            var startDate='${startDate}';
            var endDate='${endDate}';
            var start = moment(startDate);
            var end = moment(endDate);
            </c:if>
            console.log("startDate:"+start+":"+end);
            function cb(start, end) {
                console.log(start+":"+end);
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                $('#daterange').val(start + ' - ' + end)
                console.log(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end);

        });
	</script>

</body>

</html>
