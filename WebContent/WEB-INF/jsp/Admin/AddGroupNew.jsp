<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta content="" name="description" />
<meta content="" name="author" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title>Admin | Add Group</title>
<script type="text/javascript">
var contextPath="${pageContext.request.contextPath}";
</script>

<!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">

        <!-- DataTables -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/switchery.min.css" rel="stylesheet" type="text/css"/>
        
         <!-- jQuery  -->
  		<script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
  		<script src="${pageContext.request.contextPath}/resources/admin/assets/js/switchery.min.js"></script>
  		
  		<script>var contextPath="${pageContext.request.contextPath}";</script>
   		<script src="${pageContext.request.contextPath}/resources/assets-group/js/customercarepost.js"></script>

        <style>
            fieldset {
                min-width: 0;
                padding: 10px 30px;
                margin: 0;
                border: 1px solid #efefef;
            }
            fieldset legend {
                width: auto;
                font-size: 1.3rem;
            }
            .fileUpload {
              position: relative;
              overflow: hidden;
              height: 43px;
              margin-top: 0;
            }
            
            .error{
            color:red;
            }

            .fileUpload input.uploadlogo {
              position: absolute;
              top: 0;
              right: 0;
              margin: 0;
              padding: 0;
              font-size: 20px;
              cursor: pointer;
              opacity: 0;
              filter: alpha(opacity=0);
              width: 100%;
              height: 42px;
            }

            /*Chrome fix*/
            input::-webkit-file-upload-button {
              cursor: pointer !important;
              height: 42px;
              width: 100%;
            }
            .blue-btn:hover,
            .blue-btn:active,
            .blue-btn:focus,
            .blue-btn {
              background: transparent;
              border: solid 1px #27a9e0;
              border-radius: 3px;
              color: #27a9e0;
              font-size: 14px;
              margin-bottom: 20px;
              outline: none !important;
              padding: 10px 20px;
            }
        </style>

</head>

<body oncontextmenu="return false">

<input type="hidden" name="successMsg" id="successMsg" value="${groupAddMsg}">
<input type="hidden" name="errorMsg" id="errorMsg" value="${error}">
 <!-- Begin page -->
        <div id="wrapper">
						   <!-- Top Bar Start -->
           <jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />

            </div>
            <!-- Left Sidebar End -->
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Add Group</h4>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-12">
						<div class="card-box">
							<center>
								<div>
									<h6 style="font: bold; color: red;">${msg}</h6>
							</center>

							<form action="${pageContext.request.contextPath}/Admin/AddGroup"
								method="POST" id="formId" enctype="multipart/form-data">

								<fieldset>
									<legend>Add Group</legend>
									<div class="row">
										<div class="col-6">
											<div class="form-group">
												<label for="name">Group Name</label>
												<!--   <input type="text" name="name" placeholder="Enter Group Name" id="name" class="form-control" maxlength="60" onkeypress="return isAlphKey(event); groupname_error(event);"> -->
												<input type="text" name="name"
													placeholder="Enter Group Name" id="name"
													class="form-control" maxlength="60"
													onkeypress="return groupname_error(event); groupname_error(event);">
												<p class="error" id="error_fullName"></p>
											</div>
										</div>
										<div class="col-6">
											<div class="form-group">
												<label for="name">Display Name</label>
												<!-- input type="text" name="name" placeholder="Enter Group Name" id="name" class="form-control" maxlength="60" onkeypress="return isAlphKey(event); groupname_error(event);"> -->
												<input type="text" name="dname"
													oninput="this.value = this.value.toUpperCase()"
													placeholder="Display name (max 7 letters only)" id="dname"
													class="form-control" maxlength="7"
													onkeypress="return displayname_error(event);">
												<p class="error" id="error_displayName"></p>
											</div>
										</div>

										<div class="col-6">
											<div class="form-group">
												<label>Email</label>
												<!--  <input type="email" name="emailAddressId" placeholder="Enter Email" id="emailAddressId" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" class="form-control"> -->
												<input type="email" name="emailAddressId"
													placeholder="Enter Email" id="emailAddressId"
													pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"
													onkeypress="email_error(event);" class="form-control">
												<p class="error" id="error_email"></p>
											</div>
										</div>
										<div class="col-6">
											<div class="form-group">
												<label>Contact Number</label>
												<!--   <input type="text" name="mobileNoId" placeholder="Enter Mobile" id="mobileNoId" class="form-control" minlength="10" maxlength="10" onkeypress="return isNumberKey(event);"> -->
												<input type="text" name="mobileNoId"
													placeholder="Enter Mobile" id="mobileNoId"
													class="form-control" minlength="10" maxlength="10"
													onkeypress="return mobile_error(event);">
												<p class="error" id="error_mobile"></p>
											</div>
											<input type="hidden" id="countryCodeId" name="countryCode" />
										</div>
																												
										<div class="col-6">										
											<div class="services_chek" style="border-right: 1px solid #e5e5e5; max-height: 250px; overflow: hidden; overflow-y: auto;">
												<!-- Check boxes -->		
												<div class="checkbox checkbox-primary checkbox-circle">
                                                   <input id="ggrl" type="checkbox" value="16">
                                                   <label for="checkbox-9">
                                                       Group Request List
                                                   </label>
                                               </div>
                                               <div class="checkbox checkbox-primary checkbox-circle">
                                                   <input id="ggal" type="checkbox" value="17">
                                                   <label for="checkbox-9">
                                                       Group Accepted List
                                                   </label>
                                               </div>
                                               <div class="checkbox checkbox-primary checkbox-circle">
                                                   <input id="ggrj" type="checkbox" value="18">
                                                   <label for="checkbox-9">
                                                       Group Rejected List
                                                   </label>
                                               </div>
                                               <div class="checkbox checkbox-primary checkbox-circle">
                                                   <input id="gbrr" type="checkbox" value="20">
                                                   <label for="checkbox-9">
                                                       Group Bulk Register
                                                   </label>
                                               </div>
                                              <!--  <div class="checkbox checkbox-primary checkbox-circle">
                                                   <input id="gbrfl" type="checkbox" value="22">
                                                   <label for="checkbox-9">
                                                       Group Bulk Register Fail List
                                                   </label>
                                               </div> -->
                                                 
                                               <div class="checkbox checkbox-primary checkbox-circle">
                                                   <input id="bsca" type="checkbox" value="5">
                                                   <label for="checkbox-9">
                                                       Group Bulk card assignment
                                                   </label>
                                               </div>	
                                               <div class="checkbox checkbox-primary checkbox-circle">
                                                   <input id="bka" type="checkbox" value="3">
                                                   <label for="checkbox-9">
                                                       Bulk KYC Approval
                                                   </label>
                                               </div>	
                                               <div class="checkbox checkbox-primary checkbox-circle">
                                                   <input id="blsm" type="checkbox" value="11">
                                                   <label for="checkbox-9">
                                                       Bulk SMS
                                                   </label>
                                               </div>
                                               <div class="checkbox checkbox-primary checkbox-circle">
                                                   <input id="noticenter" type="checkbox" value="12">
                                                   <label for="checkbox-9">
                                                       Push Notification
                                                   </label>
                                               </div>	
                                        </div>                                        
                                        </div>
                                        <div class="col">
											<div class="description_wrap">
												<div class="desc_heading">
													<h5>Description</h5>												
												</div>
												<div class="desc_txt">
													<p>Assign Group Services</p>												
												</div>											
											</div>										
										</div>		
									</div>
								
									<div class="row mt-2 mb-3">
										<div class="col">
											<div class="togl_wrap text-center">
												<!-- Toggle -->
												<input type="checkbox" data-plugin="switchery" data-color="#039cfd" id="corptog"/> Corporate
											</div>						
										</div>
									</div>
									<div class="row" id="corporatediv">
																		
										<div class="col">
											<div class="services_chek" style="border-right: 1px solid #e5e5e5; max-height: 250px; overflow: hidden; overflow-y: auto;">
												<!-- Check boxes -->		
												<div class="checkbox checkbox-primary checkbox-circle">
                                                   <input id="bulkReg" type="checkbox" value="1">
                                                   <label for="checkbox-9">
                                                       Bulk Register
                                                   </label>
                                               </div>
                                               <!-- Check boxes -->		
												<div class="checkbox checkbox-primary checkbox-circle">
                                                   <input id="bcl" type="checkbox" value="2">
                                                   <label for="checkbox-9">
                                                       Bulk Card Load
                                                   </label>
                                               </div>
                                               <!-- Check boxes -->																							
												<div class="checkbox checkbox-primary checkbox-circle">
                                                   <input id="cscl" type="checkbox" value="4">
                                                   <label for="checkbox-9">
                                                       Corporate Single card load
                                                   </label>
                                               </div>	
                                             
                                               
                                               <div class="checkbox checkbox-primary checkbox-circle">
                                                   <input id="ur" type="checkbox" value="6">
                                                   <label for="checkbox-9">
                                                       User report
                                                   </label>
                                               </div>	
                                               
                                               <!-- <div class="checkbox checkbox-primary checkbox-circle">
                                                   <input id="ph" type="checkbox" value="7">
                                                   <label for="checkbox-9">
                                                       Prefund History
                                                   </label>
                                               </div>	 -->
                                               
                                               <div class="checkbox checkbox-primary checkbox-circle">
                                                   <input id="fr" type="checkbox" value="8">
                                                   <label for="checkbox-9">
                                                       Failed Registration
                                                   </label>
                                               </div>
                                                <div class="checkbox checkbox-primary checkbox-circle">
                                                   <input id="bcll" type="checkbox" value="19">
                                                   <label for="checkbox-9">
                                                       Bulk Card Load
                                                   </label>
                                               </div>		                                             
                                               <div class="checkbox checkbox-primary checkbox-circle">
                                                   <input id="fl" type="checkbox" value="9">
                                                   <label for="checkbox-9">
                                                       Failed Load
                                                   </label>
                                               </div>		
                                               	<div class="checkbox checkbox-primary checkbox-circle">
                                                   <input id="blca" type="checkbox" value="10">
                                                   <label for="checkbox-9">
                                                       Blocked Cards
                                                   </label>
                                               </div>		
												
                                              <!--  <div class="checkbox checkbox-primary checkbox-circle">
                                                   <input id="prefreq" type="checkbox" value="13">
                                                   <label for="checkbox-9">
                                                       Prefund Request
                                                   </label>
                                               </div>	 -->
                                               <div class="checkbox checkbox-primary checkbox-circle">
                                                   <input id="addpart" type="checkbox" value="14">
                                                   <label for="checkbox-9">
                                                       Add Partner
                                                   </label>
                                               </div>
                                               <div class="checkbox checkbox-primary checkbox-circle">
                                                   <input id="listpart" type="checkbox" value="15">
                                                   <label for="checkbox-9">
                                                       List Partner
                                                   </label>
                                               </div>	
                                              <!--  <div class="checkbox checkbox-primary checkbox-circle">
                                                   <input id="chanpass" type="checkbox" value="16">
                                                   <label for="checkbox-9">
                                                       Change Password
                                                   </label>
                                               </div> -->
                                               							
											</div>							
										</div>
										<div class="col">
											<div class="description_wrap">
												<div class="desc_heading">
													<h5>Description</h5>												
												</div>
												<div class="desc_txt">
													<p>Assign Corporate Services</p>												
												</div>											
											</div>										
										</div>													
									</div>
								</fieldset>

								<center>
									<button type="button" id="addAgent"
										class="btn btn-primary mt-4" onclick="addCorporateAgent()">Add
										Group</button>
								</center>
							</form>
						</div>
					</div>
                            </div>
                        </div> <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    &copy; <script>
																					document
																							.write(new Date()
																									.getFullYear());
																				</script> Copyright EWire.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->
        
        <div id="common_success_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_success_msg" class="alert alert-success"></center>
				</div>
			</div>
		</div>
	</div>
	
	
	<div id="common_error_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_error_msg" class="alert alert-danger"></center>
					<%-- <center><label id="common_error_msg" class="alert alert-danger"></label></center> --%>
				</div>
			</div>
		</div>
	</div>

<script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/jquery.dataTables.min.js"></script>
  		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>

        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>   
        
        <script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>    

  <script>
                // You can modify the upload files to pdf's, docs etc
            //Currently it will upload only images

            $(document).ready(function($) {
            	$("#corporatediv").hide();
              // Upload btn on change call function
              $(".uploadlogo").change(function() {
                var filename = readURL(this);
                $(this).parent().children('span').html(filename);
              });
	
              // Read File and return value  
              function readURL(input) {
                var url = input.value;
                var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
                if (input.files && input.files[0] && (
                  ext == "jpg"
                  ) || ext == "png" || ext == "jpeg") {
                  var path = $(input).val();
                  var filename = path.replace(/^.*\\/, "");
                  // $('.fileUpload span').html('Uploaded Proof : ' + filename);
                  return "Uploaded file : "+filename;
                } else {
                  $(input).val("");
                  return "Only jpg/png/jpeg format is allowed!";
                }
              }
              // Upload btn end

            });
            </script> 
            
            <script>
            $('#corptog').change( function() {
            	   if($('#corptog').prop("checked") == true){
            		   $("#corporatediv").show();            		  
            	   } else {
            		   $("#corporatediv").hide();
            	   }  
            	});                       
                      
            </script>

   
           <script type="text/javascript">
           function addCorporateAgent(){
        		var valid=true;
        		var corporate = false;
        		/* var ifscPattern= "[A-Za-z]{4}0[A-Z0-9a-z]{6}$";
        		var panPattern= "[A-Za-z]{5}[0-9]{4}[A-Za-z]{1}"; */
        		if($('#corptog').prop("checked") == true){
        			
        			var bulkRegister = $("#bulkReg").is(":checked");                	
                	var bulkcardload = $("#bcl").is(":checked");  
                	var sincarload = $("#cscl").is(":checked");               	
                	             	
                	var userReport =  $("#ur").is(":checked");                	
    				/* var prefundHistory = $("#ph").is(":checked"); */    				
    				var failedReg =  $("#fr").is(":checked");  
    				var bulkCardLoadList = $("#bcll").is(":checked");
    				var failedLoad = $("#fl").is(":checked");    				
    				var blockedCard = $("#blca").is(":checked"); 			
    				
    				/* var prefundRequest = $("#prefreq").is(":checked"); */
    				var listPart = $("#listpart").is(":checked");    				
    				var addPartner = $("#addpart").is(":checked");   
    				
    				//var changePass = $("#chanpass").is(":checked");
    				corporate = true;
    				
    				console.log("bulkRegister: "+bulkRegister);
    				console.log("bulkcardload: "+bulkcardload);
    				console.log("bulkkyc: "+bulkkyc);
    				console.log("sincarload: "+sincarload);
    				console.log("singcarassign: "+singcarassign);
    				console.log("userReport: "+userReport);
    				console.log("prefundHistory: "+prefundHistory);
    				console.log("failedReg: "+failedReg);
    				console.log("failedLoad: "+failedLoad);
    				console.log("blockedCard: "+blockedCard);
    				console.log("bulkSms: "+bulkSms);
    				console.log("noticenter: "+noticenter);
    				console.log("listPart: "+listPart);
    				console.log("addPartner: "+addPartner);
    				
        		} else {
        			corporate = false;
        		}
        		var groupRequestList = $("#ggrl").is(":checked"); 
        		var groupAcceptList = $("#ggal").is(":checked"); 
        		var groupRejectList = $("#ggrj").is(":checked"); 
        		var groupBulkRegister = $("#gbrr").is(":checked");
				/* var groupBulkRegisterFail = $("#gbrfl").is(":checked"); */
				var bulkSms = $("#blsm").is(":checked");    				
				var noticenter = $("#noticenter").is(":checked");  
				var singcarassign = $("#bsca").is(":checked");   
				var bulkkyc = $("#bka").is(":checked");
        		
        		console.log("groupRequestList: "+groupRequestList);
				console.log("groupAcceptList: "+groupAcceptList);
				console.log("groupRejectList: "+groupRejectList);
        		
        		$("#error_fullName").html("");
        		$("#error_displayname").html("");
        		$("#error_email").html("");
        		/* $("#error_address").html("");
        		$("#error_state").html("");
        		$("#error_city").html(""); */
        		$("#error_mobile").html("");
        		$("#error_aaadharCardImg").html("");
        		
        		var groupName=$('#name').val();
        		var displayname = $("#dname").val();
        		var mobile=$("#mobileNoId").val();
        		var email=$('#emailAddressId').val();        		
        		var aadharCardImg=$('#aadhar_Image').val();
        		var countryCode = "91";
    			
    			$("#countryCodeId").val(countryCode);
        		       		
        		if(groupName=='') {
        			$("#error_fullName").html("Please enter Group Name");
        			valid = false;
        			console.log("valid: "+"fullName");
        		}
        		if(displayname=='') {
        			$("#error_displayName").html("Please enter display name");
        			valid = false;
        			console.log("valid: "+"displayname");
        		}
        		if(email=='') {
        			$("#error_email").html("Please enter email id");
        			valid = false;
        			console.log("valid: "+"bankName");
        		}
        		if(email!=''){
        			if(!email.match("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}")){
            			$("#error_email").html("Please enter valid email id");
							valid=false;
        			}
        		}
        		        		
        		if(mobile=='') {
        			$("#error_mobile").html("Please enter contact no");
        			valid = false;
        			console.log("valid: "+"mobile");
        		}
        		if(mobile!=''){
        			if(mobile.length!=10){
        			$("#error_mobile").html("Please enter valid contact no");
        			valid = false;
        			console.log("valid: "+"mobile");
        		}
        	}
        		
        		/* if(aadharCardImg=='') {
        			$("#error_aaadharCardImg").html("Please attach Image");
        			valid = false;
        		}   */      		
        		
        		if (valid) {
        			$.ajax({
        				type: "POST",
          				contentType : "application/json",
          				url : "${pageContext.request.contextPath}/Admin/AddGroupAdmin",
          				dataType : 'json',  
          				data: JSON.stringify ({
          					"bulkRegister" : bulkRegister,
          					"bulkcardload" : bulkcardload,
          					"bulkkyc" : bulkkyc,
          					"sincarload" : sincarload,
          					"singcarassign" : singcarassign,
          					"userReport" : userReport,
          					/* "prefundHistory" : prefundHistory, */
          					"failedReg" : failedReg,
          					"failedLoad" : failedLoad,
          					"blockedCard" : blockedCard,
          					"bulkSms" : bulkSms,
          					"noticenter" : noticenter,
          					"listPart" : listPart,
          					"addPartner" : addPartner,          					
          					"bulkCardLoadList" : bulkCardLoadList,
          					/* "prefundRequest" : prefundRequest, */
          					"corporate" : corporate,
          					"groupBulkRegister" :groupBulkRegister,
          					"groupRequestList" : groupRequestList,
          					"groupAcceptList" : groupAcceptList,
          					"groupRejectList" : groupRejectList,          					
          					"groupName" : groupName,
          					"displayname" : displayname,
          					"mobile" : mobile,
          					"email" : email	          					
          				}),
          				success : function(response) {
          					if(response.code == "S00") {
          						$("#common_success_true").modal("show");
          						$("#common_success_msg").html(response.message);
          						console.log(response.message);
          						var timeout = setTimeout(function() {
          							$("#common_success_true").modal("hide");      								      							
          							$("#name").val("");
          							$("#dname").val("");
          							$("#mobileNoId").val("");
          							$("#emailAddressId").val("");
          							$("#aadhar_Image").val("");
          							
          						}, 2000);
          						$('input[type=checkbox]').each(function() {  
          							this.checked = false; 
          						}); 
          					} else {
          						$("#common_error_true").modal("show");
          						$("#common_error_msg").html(response.message);
          						var timeout = setTimeout(function() {
          							$("#common_error_true").modal("hide");      							
          							$("#name").val("");
          							$("#dname").val("");
          							$("#mobileNoId").val("");
          							$("#emailAddressId").val("");
          							$("#aadhar_Image").val("");          							
          						}, 3000);
          						$('input[type=checkbox]').each(function() {  
          							this.checked = false; 
          						}); 
          					}
          				}
        			});
        			
        			/* $('#formId').submit();
        			$("#addAgent").addClass("disabled");
        			document.forms["formId"].reset();
        			console.log("") */

        		}
        	}

        	</script>

<script type="text/javascript">
		function isAlphNumberKey(evt){
		    var k = (evt.which) ? evt.which : evt.keyCode
		    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 190 || k == 188);
		}
		
		function groupname_error(evt) {
			$("#error_fullName").html("");	
			 var charCode = (evt.which) ? evt.which : evt.keyCode
			return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
				
		}
		
		function displayname_error(evt) {
			$("#error_displayName").html("");	
			 var charCode = (evt.which) ? evt.which : evt.keyCode
			return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 && charCode == 32)
		}
		
		function email_error(evt) {
			$("#error_email").html("");			
		}
		
		/* function address_error(evt) {
			$("#error_address").html("");			
		}
		
		function state_error(evt) {
			$("#error_state").html("");
			 var charCode = (evt.which) ? evt.which : evt.keyCode
			 return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
		}
		
		function city_error(evt) {
			$("#error_city").html("");
			 var charCode = (evt.which) ? evt.which : evt.keyCode
			return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
		} */
		
		function mobile_error(evt) {
			$("#error_mobile").html("");
			var charCode = (evt.which) ? evt.which : evt.keyCode
			return !(charCode > 31 && (charCode < 48 || charCode > 57));
		}

	</script>


</body>
</html>