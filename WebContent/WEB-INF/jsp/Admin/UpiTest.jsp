<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
<h3>Please Wait while we're redirecting you to Payment Gateway...</h3>

<!-- 	<form method="post" id="pay" action="https://mycashier.in/ws/api/authValidate"> -->
	<form method="post" id="pay" action="${pageContext.request.contextPath}/Admin/UpiTest">	
		<input type="text" class="form-control" name="sessionId" />	
		<input type="text" class="form-control" name="transactionRefNo" />
		<input type="text" class="form-control" name="amount" />
		<button type="submit" id="submit">Submit</button>
	</form>
  	<%-- <script>document.getElementById('pay').submit();</script>  --%>
</body>
</html>