<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<title>Ewire | About Us</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<meta name="keywords" content="Ewire Prepaid Card - Enjoy great offers online & offline using Ewire Prepaid card. Fast & Easy Recharge. Pay Utility Bills. Swipe & Save.">
    <meta name="description" content="India&#039;s Digital Cash. Go cashless with Ewire Prepaid Card. Experience a seamless, safe payments platform secured with Mastercard.">

    <!-- favicon -->
  	<link rel="icon" href="${pageContext.request.contextPath}/resources/assets/img/favicon.png" type="image/gif" sizes="32x32">

	<!-- Bootstrap css -->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/responsive.css">

	<!-- google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

	<!-- slick slider -->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/slick/slick.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/slick/slick-theme.css">

</head>
<body>

<a href="javascript:void(0);" id="top"><i class="fa fa-chevron-up"></i></a>

<nav class="navbar navbar-white">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="#">
      	<img src="${pageContext.request.contextPath}/resources/assets/img/logo.png" class="img-responsive">
      </a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <!-- <ul class="nav navbar-nav">
        <li class="active"><a href="#">About Us</a></li>
        <li><a href="#">contact</a></li>
        <li><a href="#">offers</a></li> 
        <li><a href="#">Page 3</a></li> 
      </ul> -->
      <ul class="nav navbar-nav navbar-right">
        <li><a href="${pageContext.request.contextPath}/About">about us</a></li>
        <li><a href="#section1">offers</a></li>
        <!-- <li><a href="#section2">testimonial</a></li> -->
        <li><a href="#section3">contact</a></li>
        <!-- <li><a href="#">refer &amp; earn</a></li> -->
        <!-- <li><a href="#" class="btn btn-sm btn-custom" style="margin-right: 6px;">login</a></li>
        <li><a href="#" class="btn btn-sm btn-custom">register</a></li> -->
      </ul>
    </div>
  </div>
</nav>

<div class="body-container">
	<div class="container" id="hero-image">

		<div class="aboutWrp">
			<div class="secHeading text-center">
				<span>Privacy Policy</span>
			</div>
			<div class="aboutTxt">
				<strong>At Ewire powered by Cashier, we give utmost value to your trust and to your privacy.</strong>

				<h3 class="sub-head">Your privacy is important to us</h3>
				<p class="text-justified">This Privacy Policy provides to you with details about the manner in which your data is collected, stored and used by us. Please read this Privacy Policy carefully before using any of our services or products. By visiting Ewire's Web Applications or Mobile Applications you expressly give us consent to use and disclose your personal information in accordance with this Privacy Policy. If you do not agree to the terms of the policy, please do not use or access Ewire website, WAP site or mobile applications or any of our services. Kindly review this Privacy Policy periodically as we may change our policy or the terms contained in it without any prior notice. This Privacy Policy shall apply uniformly to Ewire website, Ewire mobile WAP site & Ewire mobile applications or any of our services.</p>

				<hr>
				<h3 class="sub-head">General</h3>
				<p class="text-justified">We will not sell, share or rent your personal information to any third party or use your email address/mobile number for unsolicited emails and/or SMS's. Any emails and/or SMS sent by Ewire will only be in connection with the provision of agreed services products and this Privacy Policy. We reserve the right to communicate your personal information to any third party that makes a legally-compliant request for its disclosure. General Statistical Information about Ewire its users, such as number of visitors, number and type of goods and services purchased, etc. may be revealed .</p>

				<hr>
				<h3 class="sub-head">Personal Information</h3>
				<p class="text-justified">Personal Information means and includes all information that can be linked to a specific individual or to identify any individual, such as name, address, mailing address, telephone number, email ID, credit card number, cardholder name, card expiration date, information about your mobile phone, DTH service, data card, electricity connection, Smart Tags and any details that may have been voluntarily provide by the user in connection with availing any of the services on Ewire When you browse through Ewire, we may collect information regarding the domain and host from which you access the internet, the Internet Protocol [IP] address of the computer or Internet service provider [ISP] you are using, and anonymous site statistical data.</p>

				<hr>
				<h3 class="sub-head">Use of Personal Information</h3>
				<p class="text-justified">We use personal information to provide you with services products you explicitly requested for, to resolve disputes, troubleshoot concerns, help promote safe services, collect money, measure consumer interest in our services, inform you about offers, products, services, updates, customize your experience, detect protect us against error, fraud and other criminal activity, enforce our terms and conditions, etc. We also use your contact information to send you offers based on your previous orders and interests. We may occasionally ask you to complete optional online surveys. These surveys may ask you for contact information and demographic information (like zip code, age, gender, etc.). We use this data to customize your experience at Ewire, providing you with content that we think you might be interested in and to display content according to your preferences.</p>

				<hr>
				<h3 class="sub-head">Cookies</h3>
				<p class="text-justified">A "cookie" is a small piece of information stored by a web server on a web browser so it can be later read back from that browser. Ewire uses cookie and tracking technology depending on the features offered. No personal information will be collected via cookies and other tracking technology; however, if you previously provided personally identifiable information, cookies may be tied to such information. Aggregate cookie and tracking information may be shared with third parties.</p>

				<hr>
				<h3 class="sub-head">Links to Other Sites</h3>
				<p class="text-justified">Our site links to other websites that may collect personally identifiable information about you. Ewire is not responsible for the privacy practices or the content of those linked websites.</p>
				
				<hr>
				<h3 class="sub-head">Security</h3>
				<p class="text-justified">Ewire powered by Cashier has stringent security measures in place to protect the loss, misuse, and alteration of the information under our control. Whenever you change or access your account information, we offer the use of a secure server. Once your information is in our possession we adhere to strict security guidelines, protecting it against unauthorized access.</p>

				<hr>
				<h3 class="sub-head">Consent</h3>
				<p class="text-justified">By using Ewire powered by Cashier and/or by providing your information, you consent to the collection and use of the information you disclose on Ewire in accordance with this Privacy Policy, including but not limited to your consent for sharing your information as per this privacy policy.</p>

			</div>
		</div>

		<!-- Download App from store -->
		<div class="download_app">
			<div class="app_heading text-center">
				<span>In the palm of your hands</span>
			</div>
			<div class="app_subhead text-center">
				<span>Download our App Now</span>
			</div>
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					<div class="col-sm-6 col-xs-6">
						<div>
							<a href="https://play.google.com/store/apps/details?id=in.MadMoveGlobal.CashierCard" target="_blank"><img src="${pageContext.request.contextPath}/resources/assets/img/landing/playS.png" class="img-responsive store-img right-align"></a>
						</div>
					</div>
					<div class="col-sm-6 col-xs-6">
						<div>
							<a href="https://itunes.apple.com/in/app/cashier-prepaid-cards/id1374882309?mt=8" target="_blank"><img src="${pageContext.request.contextPath}/resources/assets/img/landing/appS.png" class="img-responsive store-img left-align"></a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- footer starts here -->
		<div class="footer">
			<div class="row">
				<div class="col-sm-6 col-xs-12" id="terms_link">
					<span><a href="${pageContext.request.contextPath}/PrivacyPolicy">privacy policy</a> | <a href="${pageContext.request.contextPath}/TermsnConditions">terms &amp; conditions</a> <%-- | <a href="${pageContext.request.contextPath}/faq">faq<small>s</small></a> --%></span>
				</div>
				<div class="col-sm-6 col-xs-12 text-right" id="copyR">
					<div class="copyright">
						<span><script type="text/javascript">document.write(new Date().getFullYear());</script> &copy; copyright Ewire. | Powered by Cashier.</span>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>




<!-- scripts starts here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets/bootstrap/js/bootstrap.min.js"></script>
<!-- slick slider -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets/slick/slick.min.js"></script>

<!-- custom js for dashboard -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets/js/dashboard.js"></script>

</body>
</html>