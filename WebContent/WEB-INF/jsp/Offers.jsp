<!DOCTYPE html>
<html>
<head>
	<title>Cashier Card | Offers</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<meta name="keywords" content="Cashier Card - Enjoy great offers online & offline using Cashier Prepaid card. Fast & Easy Recharge. Pay Utility Bills. Swipe & Save.">
    <meta name="description" content="India&#039;s Digital Cash. Go cashless with Cashier Prepaid Card. Experience a seamless, safe payments platform secured with Mastercard.">

    <!-- favicon -->

 
 	<link rel="icon" href="${pageContext.request.contextPath}/resources/assets-newui/img/favicon.png" type="image/gif" sizes="32x32">

	<!-- Bootstrap css -->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/css/style.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/css/responsive.css">

	<!-- google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

	<!-- slick slider -->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/slick/slick.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/slick/slick-theme.css">
	<style>
		.offer_dtls {
			max-height: 285px;
		    overflow: hidden;
		    overflow-y: auto;
		}

		.offer_dtls ol li {
			font-size: 12px;
		}

		.modal-content {
			margin-top: 70px;
		}
	</style>

</head>
<body>

<a href="javascript:void(0);" id="top"><i class="fa fa-chevron-up"></i></a>

<nav class="navbar navbar-white">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="${pageContext.request.contextPath}/Home">
      	<img src="${pageContext.request.contextPath}/resources/assets-newui/img/logo.png" class="img-responsive">
      </a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <!-- <ul class="nav navbar-nav">
        <li class="active"><a href="#">About Us</a></li>
        <li><a href="#">contact</a></li>
        <li><a href="#">offers</a></li> 
        <li><a href="#">Page 3</a></li> 
      </ul> -->
      <ul class="nav navbar-nav navbar-right">
        <li><a href="${pageContext.request.contextPath}/About">about us</a></li>
        <li><a href="#">offers</a></li>
        <!-- <li><a href="#section2">testimonial</a></li> -->
        <li><a href="${pageContext.request.contextPath}/Home#section3">contact</a></li>
        <!-- <li><a href="#">refer &amp; earn</a></li> -->
        <li><a href="${pageContext.request.contextPath}/User/Login" class="btn btn-sm btn-custom" style="margin-right: 6px;">login</a></li>
        <li><a href="${pageContext.request.contextPath}/User/SignUp" class="btn btn-sm btn-custom">register</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="body-container">
	<div class="container" id="hero-image">

		<!-- offer carousel -->
		<div class="slider-wrp">
			<div class="offers_banners">
				<div>
			    	<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/slider/slider16.png" class="img-responsive">
			    </div>
				<div>
			    	<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/slider/slider13.png" class="img-responsive">
			    </div>
			    <div>
			    	<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/slider/slider14.png" class="img-responsive">
			    </div>
			    <div>
			    	<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/slider/slider15.png" class="img-responsive">
			    </div>
				<div>
			    	<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/slider/slider9.png" class="img-responsive">
			    </div>
				<div>
			    	<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/slider/slider12.png" class="img-responsive">
			    </div>
				<div>
			    	<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/slider/slider11.png" class="img-responsive">
			    </div>
				<div>
			    	<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/slider/slider10.png" class="img-responsive">
			    </div>
			</div>
		</div>

		<!-- single offers -->
		<div class="singlOffrs" id="section1">
			<div class="singlOffrs_head text-center hidden-xs">
				<span>our exciting offers</span>
			</div>
			<div class="singlOffrs_body">
				<div class="innersingl_offrs">
					<ul class="row">
						<!-- Zingoy -->
						<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
							<div class="singl_offfers">
								<center>
									<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o45.png" class="img-responsive">
								</center>
							</div>
							<div style="position: absolute; margin-top: -16px; cursor: pointer;font-weight: 600;color: #d02929;">
								<small data-toggle="modal" data-target="#Modal47">*T&amp;C Apply</small>
							</div>
						</li>
						<!-- SihvSagar -->
						<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
							<div class="singl_offfers">
								<center>
									<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o44.png" class="img-responsive">
								</center>
							</div>
							<div style="position: absolute; margin-top: -16px; cursor: pointer;font-weight: 600;color: #d02929;">
								<small data-toggle="modal" data-target="#Modal46">*T&amp;C Apply</small>
							</div>
						</li>
						<!-- Health & Glow -->
						<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
							<div class="singl_offfers">
								<center>
									<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o43.png" class="img-responsive">
								</center>
							</div>
							<div style="position: absolute; margin-top: -16px; cursor: pointer;font-weight: 600;color: #d02929;">
								<small data-toggle="modal" data-target="#Modal45">*T&amp;C Apply</small>
							</div>
						</li>
						<!-- OLA -->
						<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
							<div class="singl_offfers">
								<center>
									<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o41.png" class="img-responsive">
								</center>
							</div>
							<div style="position: absolute; margin-top: -16px; cursor: pointer;font-weight: 600;color: #d02929;">
								<small data-toggle="modal" data-target="#Modal43">*T&amp;C Apply</small>
							</div>
						</li>
						<!-- Lensekart -->
						<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
							<div class="singl_offfers">
								<center>
									<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o38.png" class="img-responsive">
								</center>
							</div>
							<div style="position: absolute; margin-top: -16px; cursor: pointer;font-weight: 600;color: #d02929;">
								<small data-toggle="modal" data-target="#Modal40">*T&amp;C Apply</small>
							</div>
						</li>
						<!-- Coolwinks -->
						<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
							<div class="singl_offfers">
								<center>
									<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o39.png" class="img-responsive">
								</center>
							</div>
							<div style="position: absolute; margin-top: -16px; cursor: pointer;font-weight: 600;color: #d02929;">
								<small data-toggle="modal" data-target="#Modal41">*T&amp;C Apply</small>
							</div>
						</li>
						<!-- BookMyShow -->
						<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
							<div class="singl_offfers">
								<center>
									<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o40.png" class="img-responsive">
								</center>
							</div>
							<div style="position: absolute; margin-top: -16px; cursor: pointer;font-weight: 600;color: #d02929;">
								<small data-toggle="modal" data-target="#Modal42">*T&amp;C Apply</small>
							</div>
						</li>
						<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
							<div class="singl_offfers">
								<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o31.png" class="img-responsive"></center>
							</div>
							<div style="position: absolute; margin-top: -16px; cursor: pointer;font-weight: 600;color: #d02929;">
								<small data-toggle="modal" data-target="#Modal31">*T&amp;C Apply</small>
							</div>
						</li>
						<!-- oyo -->
						<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
							<div class="singl_offfers">
								<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o42.png" class="img-responsive"></center>
							</div>
							<div style="position: absolute; margin-top: -16px; cursor: pointer;font-weight: 600;color: #d02929;">
								<small data-toggle="modal" data-target="#Modal44">*T&amp;C Apply</small>
							</div>
						</li>
						<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
							<div class="singl_offfers">
								<center>
									<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o34.png" class="img-responsive">
								</center>
							</div>
							<div style="position: absolute; margin-top: -16px; cursor: pointer;font-weight: 600;color: #d02929;">
								<small data-toggle="modal" data-target="#Modal39">*T&amp;C Apply</small>
							</div>
						</li>
						<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
							<div class="singl_offfers">
								<center>
									<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o35.png" class="img-responsive">
								</center>
							</div>
							<div style="position: absolute; margin-top: -16px; cursor: pointer;font-weight: 600;color: #d02929;">
								<small data-toggle="modal" data-target="#Modal36">*T&amp;C Apply</small>
							</div>
						</li>
						<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
							<div class="singl_offfers">
								<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o36.png" class="img-responsive"></center>
							</div>
							<div style="position: absolute; margin-top: -16px; cursor: pointer;font-weight: 600;color: #d02929;">
								<small data-toggle="modal" data-target="#Modal38">*T&amp;C Apply</small>
							</div>
						</li>
						<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
							<div class="singl_offfers">
								<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o37.png" class="img-responsive"></center>
							</div>
							<div style="position: absolute; margin-top: -16px; cursor: pointer;font-weight: 600;color: #d02929;">
								<small data-toggle="modal" data-target="#Modal37">*T&amp;C Apply</small>
							</div>
						</li>
						<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
							<div class="singl_offfers">
								<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o32.png" class="img-responsive"></center>
							</div>
							<div style="position: absolute; margin-top: -16px; cursor: pointer;font-weight: 600;color: #d02929;">
								<small data-toggle="modal" data-target="#Modal34">*T&amp;C Apply</small>
							</div>
						</li>
						<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
							<div class="singl_offfers">
								<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o1.png" class="img-responsive"></center>
							</div>
							<div style="position: absolute; margin-top: -16px; cursor: pointer;font-weight: 600;color: #d02929;">
								<small data-toggle="modal" data-target="#Modal15">*T&amp;C Apply</small>
							</div>
						</li>
						<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
							<div class="singl_offfers">
								<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o9.png" class="img-responsive"></center>
							</div>
							<div style="position: absolute; margin-top: -16px; cursor: pointer;font-weight: 600;color: #d02929;">
								<small data-toggle="modal" data-target="#Modal10">*T&amp;C Apply</small>
							</div>
						</li>
						<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
							<div class="singl_offfers">
								<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o11.png" class="img-responsive"></center>
							</div>
							<div style="position: absolute; margin-top: -16px; cursor: pointer;font-weight: 600;color: #d02929;">
								<small data-toggle="modal" data-target="#Modal28">*T&amp;C Apply</small>
							</div>
						</li>
						<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
							<div class="singl_offfers">
								<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o15.png" class="img-responsive"></center>
							</div>
							<div style="position: absolute; margin-top: -16px; cursor: pointer;font-weight: 600;color: #d02929;">
								<small data-toggle="modal" data-target="#Modal30">*T&amp;C Apply</small>
							</div>
						</li>
						<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
							<div class="singl_offfers">
								<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o17.png" class="img-responsive"></center>
							</div>
							<div style="position: absolute; margin-top: -16px; cursor: pointer;font-weight: 600;color: #d02929;">
								<small data-toggle="modal" data-target="#Modal9">*T&amp;C Apply</small>
							</div>
						</li>
						<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
							<div class="singl_offfers">
								<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o23.png" class="img-responsive"></center>
							</div>
							<div style="position: absolute; margin-top: -16px; cursor: pointer;font-weight: 600;color: #d02929;">
								<small data-toggle="modal" data-target="#Modal3">*T&amp;C Apply</small>
							</div>
						</li>
						<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
							<div class="singl_offfers">
								<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o29.png" class="img-responsive"></center>
							</div>
							<div style="position: absolute; margin-top: -16px; cursor: pointer;font-weight: 600;color: #d02929;">
								<small data-toggle="modal" data-target="#Modal12">*T&amp;C Apply</small>
							</div>
						</li>
						<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
							<div class="singl_offfers">
								<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o30.png" class="img-responsive"></center>
							</div>
							<div style="position: absolute; margin-top: -16px; cursor: pointer;font-weight: 600;color: #d02929;">
								<small data-toggle="modal" data-target="#Modal24">*T&amp;C Apply</small>
							</div>
						</li>
						<!-- =================================== -->
						<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
							<div class="singl_offfers">
								<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o20.png" class="img-responsive"></center>
							</div>
							<div style="position: absolute; margin-top: -16px; cursor: pointer;font-weight: 600;color: #d02929;">
								<small data-toggle="modal" data-target="#Modal25">*T&amp;C Apply</small>
							</div>
						</li>
						<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
							<div class="singl_offfers">
								<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o14.png" class="img-responsive"></center>
							</div>
							<div style="position: absolute; margin-top: -16px; cursor: pointer;font-weight: 600;color: #d02929;">
								<small data-toggle="modal" data-target="#Modal26">*T&amp;C Apply</small>
							</div>
						</li>
						<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
							<div class="singl_offfers">
								<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o10.png" class="img-responsive"></center>
							</div>
							<div style="position: absolute; margin-top: -16px; cursor: pointer;font-weight: 600;color: #d02929;">
								<small data-toggle="modal" data-target="#Modal27">*T&amp;C Apply</small>
							</div>
						</li>
						<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
							<div class="singl_offfers">
								<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o7.png" class="img-responsive"></center>
							</div>
							<div style="position: absolute; margin-top: -16px; cursor: pointer;font-weight: 600;color: #d02929;">
								<small data-toggle="modal" data-target="#Modal29">*T&amp;C Apply</small>
							</div>
						</li>
						<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
							<div class="singl_offfers">
								<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o3.png" class="img-responsive"></center>
							</div>
							<div style="position: absolute; margin-top: -16px; cursor: pointer;font-weight: 600;color: #d02929;">
								<small data-toggle="modal" data-target="#Modal32">*T&amp;C Apply</small>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<!-- <div class="singlOffrs_footer">
				<div class="load_more text-center">
					<a href="#" class="custm_btn" id="loadMore">Load More</a>
				</div>
			</div> -->
		</div>

		<!-- Download App from store -->
		<div class="download_app">
			<div class="app_heading text-center">
				<span>In the palm of your hands</span>
			</div>
			<div class="app_subhead text-center">
				<span>Download our App Now</span>
			</div>
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					<div class="col-sm-6 col-xs-6">
						<div>
							<a href="https://play.google.com/store/apps/details?id=in.MadMoveGlobal.CashierCard" target="_blank"><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/playS.png" class="img-responsive store-img right-align"></a>
						</div>
					</div>
					<div class="col-sm-6 col-xs-6">
						<div>
							<a href="https://itunes.apple.com/in/app/cashier-prepaid-cards/id1374882309?mt=8" target="_blank"><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/appS.png" class="img-responsive store-img left-align"></a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- contact us -->
		<div class="cntct_wrap" id="section3">
			<div class="cntct_hed text-center">
				<span>contact us</span>
			</div>
			<div class="cntct_body">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-8 col-sm-12 col-xs-12">
							<div class="contact_frm_hed">
								<h2>Reach out to us!</h2>
								<span>Got a question about Cashier. Are you intrested in partnering with us? Have some suggestions or just want to say hi? contact us:</span>
							</div>
							<div class="contact_frm">
								<form>
									<div class="col-sm-6">
										<div class="form-group">
											<input type="text" name="" class="form-control" placeholder="Enter First Name">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<input type="text" name="" class="form-control" placeholder="Enter Last Name">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<input type="text" name="" class="form-control" placeholder="Enter Mobile Number">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<input type="text" name="" class="form-control" placeholder="Enter E-mail">
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											<textarea class="form-control" placeholder="Message..." rows="4"></textarea>
										</div>
									</div>
									<div class="form-group text-right">
										<a class="custm_btn" href="#">Submit</a>
									</div>
								</form>
							</div>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12">
							<div class="customcare_contain">
								<div class="contact_hed">
									<h2>Customer Care</h2>
									<span>Not sure where to start? Need help? Just visit our <span>help center</span> or get in touch with us:</span>
								</div>
								<div class="contact_bdy">
									<div class="col-md-12 col-sm-6 col-xs-12 singl_contac">
										<div class="col-sm-3 col-xs-3">
											<div>
												<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/send.svg" class="img-responsive">
											</div>
										</div>
										<div class="col-sm-9 col-xs-9">
											<div>
												<strong>Mail us at:</strong><br>
												<span><a href="mailto:care@madmoveglobal.com">care@madmoveglobal.com</a></span>
											</div>
										</div>
									</div>

									<div class="col-md-12 col-sm-6 col-xs-12 singl_contac">
										<div class="col-sm-3 col-xs-3">
											<div>
												<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/tel.svg" class="img-responsive">
											</div>
										</div>
										<div class="col-sm-9 col-xs-9">
											<div>
												<strong>Call us at:</strong><br>
												<span><a href="tel:+91-7259195449">+91-7259195449</a><br><em>(10 AM - 7 PM all days. Except Holidays)</em></span>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="social_wrapper">
								<div class="social_hed">
									<h4>Follow us at:</h4>
								</div>
								<div class="social_links">
									<ul class="row">
										<li>
											<a href="https://www.facebook.com/CashierCard/" target="_blank">
												<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/social/facebook.svg" class="img-responsive"></center>
											</a>
										</li>
										<li>
											<a href="https://twitter.com/cashiercard?lang=en" target="_blank">
												<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/social/twitter.svg" class="img-responsive"></center>
											</a>
										</li>
										<li>
											<a href="https://www.instagram.com/cashiercard/" target="_blank">
												<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/social/instagram.svg" class="img-responsive"></center>
											</a>
										</li>
										<li>
											<a href="https://www.youtube.com/channel/UCCt9wky_iLlTBU28_EpGSTw?pbjreload=10" target="_blank">
												<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/social/youtube.svg" class="img-responsive"></center>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- footer starts here -->
	<div class="footer">
			<div class="row">
				<div class="col-sm-6 col-xs-12" id="terms_link">
					<span><a href="${pageContext.request.contextPath}/PrivacyPolicy">privacy policy</a> | <a href="${pageContext.request.contextPath}/TermsnConditions">terms &amp; conditions</a> | <a href="#">faq<small>s</small></a></span>
				</div>
				<div class="col-sm-6 col-xs-12 text-right" id="copyR">
					<div class="copyright">
						<span><script type="text/javascript">document.write(new Date().getFullYear());</script> &copy; copyright EWire.</span>
					</div>
				</div>
			</div>
		</div>
	
	</div>
</div>

<!-- <div class="get-app hidden-xs">
	<span  class="pull-right" id="app_get">Get App</span>
	<div class="get-app-inner pull-right">
		<input type="text" name="getApp" id="get_app" maxlength="10" class="form-control" placeholder="Enter 10 Digit Mobile Number">
	</div>
	<span class="pull-right">
		<i class="glyphicon glyphicon-menu-left trans-03-sec"></i>
	</span>
</div>
 -->


<!-- ================================================================================ -->
<!-- offer modal -->

    <!-- Modal 1 -->
    <div class="modal fade" id="Modal3" tabindex="-1" role="dialog" aria-labelledby="bModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Ferns N Petals</h4>
	      </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="offer_condi">
                        <h4>Terms &amp; Conditions</h4>
                    </div>
                    <div class="offer_dtls">
                        <ol>
                            <li>Offer valid till <strong>31st August 2018</strong>.</li>
                            <li>Offer valid only on Cashier Physical Cards only</li>
                            <li>Under this offer, User is eligible only for once with one card for his 1st transaction with the card for the purchase.</li>
                            <li>Customer will get Flat <strong>Rs.100</strong> Cashback</li>
                            <li>Offer valid on a minimum transaction value of <strong>Rs.500</strong></li>
                            <li>Cashback will be credited to customer's cashier card account within 7 working days on successful completion of the transaction.</li>
                        </ol>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 1 -->
    <div class="modal fade" id="Modal6" tabindex="-1" role="dialog" aria-labelledby="bModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">          
          <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Hypercity</h4>
	      </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-9">
                    <div class="offer_condi">
                        <h4>Terms &amp; Conditions</h4>
                    </div>
                    <div class="offer_dtls">
                        <ol>
                            <li>The Offer is valid till <strong>31 March 2018</strong>.</li>
                            <ol style="list-style-type: upper-alpha;">Under this Offer,
                                <li>Cardholders get Rs. 150 discount on purchases amounting to Rs. 3,000 and above on any category by quoting the promotion code <strong>101682274</strong>.</li>
                                <li>Cardholders get Rs. 300 discount on purchases amounting to Rs.3,000 and above on the fashion category by quoting the promotion code <strong>101682275</strong>.</li>
                            </ol>
                            <li>Cardholders need to indicate their intention to redeem the Offer before the bill is generated and may be asked to present their card for confirmation.</li>
                            <li>Cardholders need to quote the promotion code at the billing counter to redeem the Offer.</li>
                            <li> The Offer is valid only at Hypercity stores in India.</li>
                            <li>The Offer cannot be combined with any other offer, promotion or scheme.</li>
                            <li>The Offer is available only upon making full payment by a valid and unexpired Mastercard card issued in India.</li>
                            <li>The Offer is non-transferable and cannot be exchanged for cash, cheque or any form of credit.</li>
                            <li> The total minimum spend will be calculated based on the product cost only. This excludes all taxes, GST, delivery charges, etc. Any taxes, liabilities or charges payable to the government or any other authority or body, if any, shall be borne directly by the cardholder and/ or billed to the account of the cardholder.</li>
                            <li>For any queries regarding the Offer, customers may contact the Merchant by calling at <a href="tel:+91-22-40695555">+91-22-40695555</a> or sending an email to <a href="mailto:feedback@hypercityindia.com">feedback@hypercityindia.com</a> </li>
                            <li>Cardholders are not bound in any way to participate in the Offer. Any participation shall be voluntary and all participants understand, acknowledge and agree that the Offer is purely on a best effort basis and Mastercard does not assume any liability regarding the Offer, the delivery of services or any incidental matter.</li>
                            <li>Any cardholder who utilises, participates in, or attempts to utilise or participate in the Mastercard Card Offers programme, irrevocably agrees to the following: Mastercard will not be liable under contract, tort or any other theory of law for any claim regarding loss or damage howsoever incurred in relation to the use (or attempted use) of the Offer; and accordingly, the cardholder will not make any claim against Mastercard regarding the same.</li>
                            <li>Any claims, complaints or issues regarding service, quality or products etc. need to be made to Hypercity directly. Mastercard or the issuing bank will not be responsible for the same.</li>
                            <li>These terms and conditions shall be governed by the laws of India and any dispute arising out of or in connection with these terms and conditions shall be subject to the exclusive jurisdiction of the courts in Mumbai.</li>
                            <li>Each benefit or privilege may be subject to additional terms and conditions imposed by Hypercity. Cardholders are solely responsible for checking and complying with the same.</li>
                            <li> Mastercard reserves the right to add, alter, modify, change or vary any of these terms and conditions or to replace, wholly or in part, this Offer by another offer, whether similar or not, or to withdraw it altogether at any point in time, without any prior notice.</li>
                        </ol>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="offer-txt text-center mt-2">
                        <span>Promotion Code</span>
                    </div>
                    <div class="coupon_cd text-center">
                        <a class="btn btn-danger btn-block">101682274</a>
                        <small>Click button to copy code(For any Category)</small>
                    </div>
                    <div class="coupon_cd text-center">
                        <a class="btn btn-danger btn-block">101682275</a>
                        <small>Click button to copy code(For Fashion Shopping)</small>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 1 -->
    <div class="modal fade" id="Modal9" tabindex="-1" role="dialog" aria-labelledby="bModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Enrich Salon</h4>
	      </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-9">
                    <div class="offer_condi">
                        <h4>Terms &amp; Conditions</h4>
                    </div>
                    <div class="offer_dtls">
                        <ol>
                            <li> The Offer is valid till <strong>31 December 2018</strong>.</li>
                            <li> Under this offer, the cardholder gets a 15% discount on a minimum billing of Rs 750. Offer is not valid on Products, Bridal makeup packages or the Series package.</li>
                            <li>The Offer is available only upon making full payment by a valid and unexpired Mastercard card issued in India.</li>
                            <li> Cardholder needs to indicate intention to redeem the Offer before the bill is generated and may be asked to present the card for confirmation.</li>
                            <li>Offer cannot be combined with any other offer/ promotion/ scheme.</li>
                            <li>Offer is non-transferable and cannot be exchanged for cash or cheque or any form of credit.</li>
                            <li>Cardholder is required to pay any applicable service charges and taxes.</li>
                            <li> In case of any queries regarding the Offer, customers may contact the Merchant by calling  <a href="tel:1-860-266-3000">1-860-266-3000</a> or sending an email to <a href="mailto:customercare@enrichsalon.com">customercare@enrichsalon.com</a></li>
                            <li>Cardholders are not bound in any way to participate in the Offer. Any participation shall be voluntary and all participants understand, acknowledge and agree that the Offer is purely on a best effort basis and Mastercard does not assume any liability whatsoever regarding the Offer, the delivery of services or any incidental matter.</li>
                            <li>Any cardholder who utilises, participates in, or attempts to utilise or participate in the Mastercard Card Offers programme, irrevocably agrees to the following: Mastercard will not be liable under contract, tort or any other theory of law for any claim regarding loss or damage howsoever incurred in relation to the use (or attempted use) of the Offer; and accordingly, the cardholder will not make any claim against Mastercard regarding the same.</li>
                            <li>Any claims, complaints or issues regarding service, quality or products etc. need to be made to Enrich Salon directly. Mastercard or the issuing bank will not be responsible for the same.</li>
                            <li>These terms and conditions shall be governed by the laws of India and any dispute arising out of or in connection with these terms and conditions shall be subjected to the exclusive jurisdiction of the courts in Mumbai.</li>
                            <li> Each benefit/ privilege may be subjected to additional terms and conditions imposed by Enrich Salon. Cardholders are solely responsible for checking and complying with the same.</li>
                            <li>Mastercard reserves the right to add, alter, modify, change or vary any of these terms and conditions or to replace, wholly or in part, this Offer by another offer, whether similar or not, or to withdraw it altogether at any point in time, without any prior notice.</li>
                        </ol>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="offer-txt text-center mt-2">
                        <span>Promotion Code</span>
                    </div>
                    <div class="coupon_cd text-center">
                        <a class="btn btn-danger btn-block">No Coupon Code</a>
                        <small>Click button to copy code</small>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- end here upto changes -->

    <!-- Modal 1 -->
    <div class="modal fade" id="Modal10" tabindex="-1" role="dialog" aria-labelledby="bModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Swiggy</h4>
	      </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-9">
                    <div class="offer_condi">
                        <h4>Terms &amp; Conditions</h4>
                    </div>
                    <div class="offer_dtls">
                        <ol>
                            <li>The Offer is valid till <strong>30 April 2019</strong>.</li>
                            <li> Under this Offer, Cardholders get Rs 100 off on minimum order of Rs.400 and above at <a href="www.swiggy.com">www.swiggy.com</a> by quoting the promotion code <strong>MASTER100</strong>. Alternatively, Cardholders get Rs.75 off on minimum order of Rs.300 and above using promotion code <strong>MASTER75</strong></li>
                            <li>Offer is valid for first order only.</li>
                            <li>The Offer is available only upon making full payment by a valid and unexpired Mastercard card issued in India. Offer is not valid for Cash on Delivery.</li>
                            <li> To redeem this offer, cardholders should create an account to book their first order.</li>
                            <li>The Offer is non-transferable and cannot be exchanged for cash, cheque or any form of credit.</li>
                            <li>The Offer must be redeemed in full and not in parts.</li>
                            <li>The Offer is non-binding, non-encashable and non-negotiable.</li>
                            <li> The total spend will be calculated based on the product cost only. This excludes all taxes, GST etc. Any taxes, liabilities or charges payable to the government or any other authority or body, if any, shall be borne directly by the cardholder and/ or billed to the account of the cardholder.</li>
                            <li>Any dispute regarding delivery, service, suitability, merchantability, availability or quality of the Offer/or product/services under this Offer must be addressed by sending an email to <a href="mailto:support@swiggy.in">support@swiggy.in</a>. Mastercard or the issuing bank will not be liable for the same.</li>
                            <li>Offer can be availed in operational areas - Bangalore, Hyderabad, Mumbai, Pune, Delhi NCR, Kolkata, Chennai.</li>
                            <li>The Offer cannot be combined with any other promotion or code-based offer and no additional discount or offer will be applicable.</li>
                            <li>Offer cannot be used in conjunction with Swiggy money received through referral amount.</li>
                            <li> Bundl Technologies Pvt. Ltd. (Swiggy) reserves the right to cancel/ change/modify/add/delete any of the terms and conditions of the offer at any time without notice</li>
                            <li>Bundl Technologies Pvt. Ltd. (Swiggy) reserves the right to terminate the offer at any time without notice</li>
                            <li>Bundl Technologies Pvt. Ltd. (Swiggy) reserves the right to deny honouring the offer on the grounds of suspicion or abuse of the offer by any customer without providing customer any explanation thereof</li>
                            <li>In no event shall Bundl Technologies Pvt. Ltd. (Swiggy) be liable for any abuse or misuse of the code due to the negligence of the customer</li>
                            <li>Cardholders are not bound in any way to participate in the Offer. Any participation shall be voluntary and all participants understand, acknowledge and agree that the Offer is purely on a best effort basis and Mastercard does not assume any liability whatsoever regarding the Offer, the delivery of services or any incidental matter.</li>
                            <li>Any cardholder who utilises, participates in, or attempts to utilise or participate in the Mastercard Card Offers programme, irrevocably agrees to the following: Mastercard will not be liable under contract, tort or any other theory of law for any claim regarding loss or damage howsoever incurred in relation to the use (or attempted use) of the Offer; and accordingly, the cardholder will not make any claim against Mastercard regarding the same.</li>
                            <li>Each benefit/privilege may be subject to additional terms and conditions imposed by Swiggy. The services shall be governed by the terms and conditions set out in <a href="https://www.swiggy.com/">www.swiggy.com</a> terms-and-conditions. Cardholders are solely responsible for checking and complying with the same.</li>
                            <li>These terms and conditions shall be governed by the laws of India and any dispute arising out of or in connection with these terms and conditions shall be subjected to the exclusive jurisdiction of the courts in Mumbai.</li>
                            <li>Mastercard reserves the right to add, alter, modify, change or vary any of these terms and conditions or to replace, wholly or in part, this Offer by another offer, whether similar or not, or to withdraw it altogether at any point in time, without any prior notice.</li>
                        </ol>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="offer-txt text-center mt-2">
                        <span>Promotion Code</span>
                    </div>
                    <div class="coupon_cd text-center">
                        <a class="btn btn-danger btn-block">MASTER100</a>
                        <small>Click button to copy code</small>
                    </div>
                    <div class="coupon_cd text-center">
                        <a class="btn btn-danger btn-block">MASTER75</a>
                        <small>Click button to copy code</small>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 1 -->
    <div class="modal fade" id="Modal12" tabindex="-1" role="dialog" aria-labelledby="bModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">VOYLLA</h4>
	      </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-9">
                    <div class="offer_condi">
                        <h4>Terms &amp; Conditions</h4>
                    </div>
                    <div class="offer_dtls">
                        <ol>
                            <li>The Offer is valid till <strong>31 October 2018</strong>.</li>
                            <li>Under this Offer, the cardholder will get 15% off with a minimum spend of Rs. 1,000 and above by quoting the promotion code <strong>VCIDMRCD15</strong>. The Offer is applicable on the website <a href="www.voylla.com">www.voylla.com</a> and the Voylla mobile app.</li>
                            <li>The Offer is available only upon making full payment by a valid and unexpired Mastercard card issued in India.</li>
                            <li>To redeem this Offer, cardholders should login to their account or create an account, if they are not registered.</li>
                            <li>The Offer cannot be combined together with any other promo code-based offer and no additional discount will be applicable.</li>
                            <li>The Offer is non-binding, non-encashable and non-negotiable.</li>
                            <li>Cardholders are required to pay any applicable service charges and taxes.</li>
                            <li>Any dispute regarding delivery, service, suitability, merchantability, availability or quality of the Offer, or product /services related to this Offer must be addressed in writing, by the cardholder directly to Voylla at <a href="mailto:help@voylla.com">help@voylla.com</a> or by calling <a href="tel:+91-7676111022">+91-7676111022</a>. Mastercard or the issuing bank will not be liable for the same.</li>
                            <li>Any dispute relating to the Offer will be settled under the sole and exclusive jurisdiction of Mumbai courts only.</li>
                            <li>Cardholders are not bound in any way to participate in the Offer. Any participation shall be voluntary and all participants understand, acknowledge and agree that the Offer is purely on a best effort basis and Mastercard does not assume any liability whatsoever regarding the Offer, the delivery of services or any incidental matter.</li>
                            <li>Any cardholder who utilises, participates in, or attempts to utilise or participate in the Mastercard Card Offers programme, irrevocably agrees to the following: Mastercard will not be liable under contract, tort or any other theory of law for any claim regarding loss or damage howsoever incurred in relation to the use (or attempted use) of the Offer; and accordingly, the cardholder will not make any claim against Mastercard regarding the same.</li>
                            <li>Each benefit or privilege may be subject to additional terms and conditions imposed by <a href="www.voylla.com">www.voylla.com</a> Cardholders are solely responsible for checking and complying with the same.</li>
                            <li>Mastercard reserves the right to add, alter, modify, change or vary any of these terms and conditions or to replace, wholly or in part, this Offer by another offer, whether similar or not, or to withdraw it altogether at any point in time, without any prior notice.</li>
                        </ol>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="offer-txt text-center mt-2">
                        <span>Promotion Code</span>
                    </div>
                    <div class="coupon_cd text-center">
                        <a class="btn btn-danger btn-block">VCIDMRCD15</a>
                        <small>Click button to copy code</small>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 1 -->
    <div class="modal fade" id="Modal15" tabindex="-1" role="dialog" aria-labelledby="bModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
         
          <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Shoppers Stop</h4>
	      </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-9">
                    <div class="offer_condi">
                        <h4>Terms &amp; Conditions</h4>
                    </div>
                    <div class="offer_dtls">
                        <ol>
                            <li>The Offer is valid till <strong>30 June 2019</strong>.</li>
                            <li>Under this Offer, cardholder will get 12% Off on spend of Rs 1800 or above, using the promotion code <strong>MC12</strong>. Maximum discount upto Rs.1,000.</li>
                            <li>Offer valid only for online orders placed on <a href="www.shoppersstop.com">www.shoppersstop.com</a> website and App.</li>
                            <li>The Discount will not be applicable on Casio Watches, Nautica, Tommy Hilfiger, Fastrack, FCUK, Police, Sonata, Timberland, Titan, Xylys, Zoop, Nebula, Loccitane, Fine Jewellery, Gold Coins, Gift Vouchers/Cards/Topups, E-Gift Voucher, First Citizen membership card.</li>
                            <li>Offer can be availed only one time per user during the offer period and will not be applicable during End of Season Sale.</li>
                            <li>The approval of the order is at the sole discretion of Shoppers Stop and using the promo code is mandatory to avail of the offer.</li>
                            <li>Minimum threshold spend to qualify for the offer will be calculated on basis of product/service costs only excluding taxes, GST, delivery charges, etc. Any taxes or liabilities or charges payable to the Government or any other authority or body, if any, shall be borne directly by the cardholder and/ or billed to the account of the cardholder.</li>
                            <li>The offer can be availed only on transactions done using an eligible Mastercard Card issued in India. Offer is not valid for Cash on Delivery.</li>
                            <li>To redeem this offer, cardholders should login to their account or create an account, if they are not registered.</li>
                            <li>Offer is non-transferable and cannot be exchanged for cash or cheque or any form of credit.</li>
                            <li>Shoppers Stop does not return or exchange Gift Vouchers, Fine Jewellery, Gold Jewellery, Gold Coins, Undergarments, Altered Garments, Cosmetics &amp; Perfumes, Sunglasses, First Citizen Card, Maxit Products, Electronic Products and Watches for hygiene and other reasons.</li>
                            <li> Any dispute regarding delivery, service, suitability, merchantability, availability or quality of the offer/or product/services availed under this offer must be addressed in writing, by the cardholder directly to Shoppers Stop at <a href="mailto:estore@shoppersstop.com">estore@shoppersstop.com</a> or by calling <a href="tel:1-800-419-6648">1-800-419-6648</a> . Mastercard or the issuing bank will  not be liable for the same.</li>
                            <li> Cardholders are not bound in any way to participate in the Offer. Any participation shall be voluntary and all participants understand, acknowledge and agree that the Offer is purely on a best effort basis and Mastercard does not assume any liability whatsoever regarding the Offer, the delivery of services or any incidental matter.</li>
                            <li>No two offers can be clubbed together and no additional discount/offer will be applicable.</li>
                            <li>Any cardholder who utilises, participates in, or attempts to utilise or participate in the Mastercard Card Offers programme, irrevocably agrees to the following: Mastercard will not be liable under contract, tort or any other theory of law for any claim regarding loss or damage howsoever incurred in relation to the use (or attempted use) of the Offer; and accordingly, the cardholder will not make any claim against Mastercard regarding the same.</li>
                            <li>Each benefit/privilege may be subject to additional terms and conditions imposed by Shoppers Stop. Cardholders are solely responsible for checking and complying with the same.  For details, visit  <a href="https://www.shoppersstop.com/term-condition">https://www.shoppersstop.com/term-condition</a></li>
                            <li> These terms and conditions shall be governed by the laws of India and any dispute arising out of or in connection with these terms and conditions shall be subject to the exclusive jurisdiction of the courts in Mumbai.</li>
                            <li> In the event of non-happening, cancellation or partial happening of the offer for reasons beyond anybody's control or Act of God, neither Shoppers Stop nor their agent, sponsors nor their agent, successors or assigns shall be liable to any of the customers participating in the offer.</li>
                            <li>Mastercard reserves the right to add, alter, modify, change or vary any of these terms and conditions or to replace, wholly or in part, this Offer by another offer, whether similar or not, or to withdraw it altogether at any point in time, without any prior notice.</li>
                        </ol>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="offer-txt text-center mt-2">
                        <span>Promotion Code</span>
                    </div>
                    <div class="coupon_cd text-center">
                        <a class="btn btn-danger btn-block">MC12</a>
                        <small>Click button to copy code</small>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 1 -->
    <div class="modal fade" id="Modal24" tabindex="-1" role="dialog" aria-labelledby="bModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">KAPILS SALON</h4>
	      </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-9">
                    <div class="offer_condi">
                        <h4>Terms &amp; Conditions</h4>
                    </div>
                    <div class="offer_dtls">
                        <ol>
                            <li>The Offer is valid till <strong>30 September 2018</strong>.</li>
                            <li>Under this Offer, cardholders will get 15% off on all services at paricipating Kapils Salon outlets in India.</li>
                            <li>At the salon, the cardholder must also indicate intention to redeem the Offer before the final bill is generated and may be asked to present the Mastercard card for confirmation.</li>
                            <li>The Offer is available only upon making full payment by a valid and unexpired Mastercard card issued in India.</li>
                            <li>The Offer cannot be combined with any other offer, promotion, scheme or voucher and no additional discount or offer will be applicable. </li>
                            <li>Cardholders are required to pay any applicable service charges and taxes.</li>
                            <li>The Offer is non-binding, non-encashable and non-negotiable.</li>
                            <li>The total spend will be calculated based on the product cost only. This excludes all taxes, GST, delivery charges, etc. Any taxes, liabilities or charges payable to the government or any other authority or body, if any, shall be borne directly by the cardholder and/ or billed to the account of the cardholder.</li>
                            <li>Any dispute regarding delivery, service, suitability, merchantability, availability or quality of the Offer, product or services availed under this Offer must be addressed in writing, by the cardholder directly to Kapils Salon at <a href="mailto:info@kapilssalon.com">info@kapilssalon.com</a>. Mastercard or the issuing bank will not be liable for the same.</li>
                            <li>Any dispute relating to the Offer will be settled under the sole and exclusive jurisdiction of Mumbai courts only.</li>
                            <li> Cardholders are not bound in any way to participate in the Offer. Any participation shall be voluntary and all participants understand, acknowledge and agree that the Offer is purely on a best effort basis and Mastercard does not assume any liability whatsoever regarding the Offer, the delivery of services or any incidental matter.</li>
                            <li>Any cardholder who utilises, participates in, or attempts to utilise or participate in the Mastercard Card Offers programme, irrevocably agrees to the following: Mastercard will not be liable under contract, tort or any other theory of law for any claim regarding loss or damage howsoever incurred in relation to the use (or attempted use) of the Offer; and accordingly, the cardholder will not make any claim against Mastercard regarding the same.</li>
                            <li>Each benefit or privilege may be subject to additional terms and conditions imposed by Kapils Salon. Cardholders are solely responsible for checking and complying with the same.</li>
                            <li>Mastercard reserves the right to add, alter, modify, change or vary any of these terms and conditions or to replace, wholly or in part, this Offer by another offer, whether similar or not, or to withdraw it altogether at any point in time, without any prior notice.</li>
                        </ol>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="offer-txt text-center mt-2">
                        <span>Promotion Code</span>
                    </div>
                    <div class="coupon_cd text-center">
                        <a class="btn btn-danger btn-block">Ask for the Offer</a>
                        <!-- <small>Click button to copy code</small> -->
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 1 -->
    <div class="modal fade" id="Modal28" tabindex="-1" role="dialog" aria-labelledby="bModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">MYLESCARS.COM</h4>
	      </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-9">
                    <div class="offer_condi">
                        <h4>Terms &amp; Conditions</h4>
                    </div>
                    <div class="offer_dtls">
                        <ol>
                            <li>The Offer is valid till <strong>30 September 2018</strong>.</li>
                            <p>Under the Offer, </p>
                            <ul>
                                <li>Cardholders will receive 20% off on bookings made for self-drive rental cars by quoting the promotion code <strong>MCRD20</strong>.</li>
                                <li> Cardholders will receive flat Rs. 600 off on bookings made for self-drive rental cars by quoting the promotion code <strong>MYMC600</strong>.</li>
                            </ul>
                            <li>Discount under the Offer will be calculated only on the base rate. Offer not valid on hourly booking.</li>
                            <li> The Offer is applicable only from Monday to Thursday and not applicable on weekends (i.e. Friday to Sunday).</li>
                            <li> The Offer is applicable only if booking is made via the website <a href="www.mylescars.com">www.mylescars.com</a>, mobile App or via the call centre by calling <a href="tel:+91-8882222222"></a>.</li>
                            <li>The Offer is available only upon making full payment by a valid and unexpired Mastercard card issued in India.</li>
                            <li>To redeem this Offer, cardholders should log in to their account or create an account, if they are not registered.</li>
                            <li>The Offer cannot be combined together with any other promotion code-based offer and no additional discount/offer will be applicable.</li>
                            <li> The Offer is non-binding, non-encashable and non-negotiable.</li>
                            <li>The Offer must be redeemed in full and not in parts.</li>
                            <li>Cardholder is required to pay any applicable service charges and taxes</li>
                            <li> Any dispute regarding delivery, service, suitability, merchantability, availability or quality of the Offer/or product/services redeemed under this Offer must be addressed by calling the call centre at <a href="tel:+91-8882222222">+91-8882222222</a>. Mastercard or the issuing bank will not be liable for the same.</li>
                            <li>Any dispute relating to the Offer will be settled under the sole and exclusive jurisdiction of Mumbai courts only.</li>
                            <li>Cardholders are not bound in any way to participate in the Offer. Any participation shall be voluntary and all participants understand, acknowledge and agree that the Offer is purely on a best effort basis and Mastercard does not assume any liability whatsoever regarding the Offer, the delivery of services or any incidental matter.</li>
                            <li>Any cardholder who utilises, participates in, or attempts to utilise or participate in the Mastercard Card Offers programme, irrevocably agrees to the following: Mastercard will not be liable under contract, tort or any other theory of law for any claim regarding loss or damage howsoever incurred in relation to the use (or attempted use) of the Offer; and accordingly, the cardholder will not make any claim against Mastercard regarding the same.</li>
                            <li> Each benefit/privilege may be subject to additional terms and conditions imposed by Mylescars.com. Cardholders are solely responsible for checking and complying with the same.</li>
                            <li>Mastercard reserves the right to add, alter, modify, change or vary any of these terms and conditions or to replace, wholly or in part, this Offer by another offer, whether similar or not, or to withdraw it altogether at any point in time, without any prior notice.</li>
                        </ol>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="offer-txt text-center mt-2">
                        <span>Promotion Code</span>
                    </div>
                    <div class="coupon_cd text-center">
                        <a class="btn btn-danger btn-block">MYMC600</a>
                        <small>[for Rs600 offer]</small>
                    </div>
                    <div class="coupon_cd text-center">
                        <a class="btn btn-danger btn-block">MCRD20</a>
                        <small>[for 20% Off offer]</small>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 1 -->
    <div class="modal fade" id="Modal30" tabindex="-1" role="dialog" aria-labelledby="bModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">TREEBO HOTELS</h4>
	      </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-9">
                    <div class="offer_condi">
                        <h4>Terms &amp; Conditions</h4>
                    </div>
                    <div class="offer_dtls">
                        <ol>
                            <li>The Offer is valid till <strong>31 December 2018</strong>.</li>
                            <li>Under this Offer, Cardholders get Flat 25% Off at http://bit.ly/trbmc  (https://www.treebo.com/?utm_source=treeboalliance&utm_campaign=mastercard&utm_medium=Campaign)   by quoting the promotion code TRBMC.</li>
                            <li>Offer is valid for bookings made till 31st December 2018 for stay dates till 31st December 2018.  All bookings are subject to availability.</li>
                            <li>Black-out dates applicable. The offer cannot be applied if the staying period falls between the black-out dates either fully or partially. All Treebo Hotel policies apply.</li>
                            <li>The offer can be availed only on transactions done using an eligible Mastercard Card issued in India.</li>
                            <li> To redeem this offer, cardholders should login to their account or create an account, if they are not registered. Promo code valid only on booking done at  https://www.treebo.com/?utm_source=treeboalliance&utm_campaign=mastercard&utm_medium=Campaign </li>
                            <li>Offer is non-transferable and cannot be exchanged for cash or cheque or any form of credit.</li>
                            <li>Discount applicable on pre tax amount. Taxes as applicable to be borne by customer.</li>
                            <li>Any dispute regarding delivery, service, suitability, merchantability, availability or quality of the offer/or product/services availed under this offer must be addressed in writing, by the cardholder directly to Treebo Hotels at hello@treebo.com or by calling +91 9322-800-100. Mastercard or the issuing bank will  not be liable for the same.  Cancellation charges will be applicable as per the Treebo policy.</li>
                            <li>Cardholders are not bound in any way to participate in the Offer. Any participation shall be voluntary and all participants understand, acknowledge and agree that the Offer is purely on a best effort basis and Mastercard does not assume any liability whatsoever regarding the Offer, the delivery of services or any incidental matter.</li>
                            <li>No two offers can be clubbed together and no additional discount/ offer will be applicable.</li>
                            <li>Any cardholder who utilises, participates in, or attempts to utilise or participate in the Mastercard Card Offers programme, irrevocably agrees to the following: Mastercard will not be liable under contract, tort or any other theory of law for any claim regarding loss or damage howsoever incurred in relation to the use (or attempted use) of the Offer; and accordingly, the cardholder will not make any claim against Mastercard regarding the same.</li>
                            <li>Each benefit/privilege may be subject to additional terms and conditions imposed by Treebo Hotels. Cardholders are solely responsible for checking and complying with the same.</li>
                            <li>Any dispute relating to the offer will be settled under the sole and exclusive jurisdiction of Mumbai courts only.</li>
                            <li>Mastercard reserves the right to add, alter, modify, change or vary any of these terms and conditions or to replace, wholly or in part, this Offer by another offer, whether similar or not, or to withdraw it altogether at any point in time, without any prior notice.</li>
                        </ol>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="offer-txt text-center mt-2">
                        <span>Promotion Code</span>
                    </div>
                    <div class="coupon_cd text-center">
                        <a class="btn btn-danger btn-block">TRBMC</a>
                        <small>Click button to copy code</small>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 1 -->
    <div class="modal fade" id="Modal31" tabindex="-1" role="dialog" aria-labelledby="bModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Myntra</h4>
	      </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="offer_condi">
                        <h4>Eligibility and Disqualification:</h4>
                    </div>
                    <div class="offer_dtls">
                    	<p>To participate in this Offer, Participants are required to do the following:</p>
                        <ol>
                            <li>Make purchases of any product available on the Myntra Page for a minimum of INR 1000.</li>
                            <li>Any participation is voluntary, and the Offer is being made purely on a best effort basis.</li>
                            <li>Myntra/Sponsor, in its discretion, reserves the right to disqualify the Participant from the benefits of this Offer, if any fraudulent activity is identified as being carried out for the purpose of availing the benefits under the said Offer.</li>
                        </ol>
                        <p>Other Terms:</p>
                        <ol>
                        	<li>Myntra/Sponsor, in its sole discretion, reserves the right to disqualify You from the benefits of this Offer, if any fraudulent activity is identified as being carried out for the purpose of availing the benefits under the said Offer.</li>
                        	<li>Participants can avail this Offer only once in a calendar month.</li>
                        	<li>The maximum cashback that can be available per Participant for every Card is limited to INR 250.</li>
                        	<li>If a participant has more than one Card, the Participant has to call up the customer care to confirm which Card is applicable under this Offer.</li>
                        	<li>The Offer is valid on physical cards. The bin range for physical card is bin 538629 and range - 1400000010 to 1405000009.</li>
                        	<li>A participant does not require any promo code to avail this Offer.</li>
                        	<li>The said cashback under this Offer is automatic and eligible amount will get credited to customer's account within 7 working days.</li>
                        	<li>Myntra/Sponsor reserves the right, at their sole discretion, to change, modify, add or remove portions of this Policy, at any time without any prior written notice to You.</li>
                        	<li>This is a limited period Offer.</li>
                        	<li>This Offer is valid for holders of EWire only.</li>
                        	<li>This Offer is valid only in India. The minimum age of such Participant shall be 18 years.</li>
                        	<li>The Offer shall be subject to force majeure events and on occurrence of such an event, the Offer may be withdrawn at the discretion of Sponsor.</li>
                        	<li>Sponsor reserve the right, in their sole discretion, to disqualify any Participant that tampers or attempts to violate the Policy.</li>
                        	<li>Myntra/Sponsor shall not be responsible for any loss, injury or any other liability arising due to participation by any person in the Offer.</li>
                        	<li>You hereby agree to indemnify and keep Myntra/Sponsor harmless against all damages, liabilities, costs, expenses, claims, suits and proceedings (including reasonable attorney's fee) that may be suffered by Myntra/Sponsor as a consequence of (i) violation of terms of this Policy by You; (ii) violation of applicable laws; (iii) any action or inaction resulting in willful misconduct or negligence on Your part.</li>
                        	<li>This Policy shall be governed in accordance with the applicable laws in India. Courts at Bangalore shall have the exclusive jurisdiction to settle any dispute that may arise under this Policy.</li>
                        	<li>This Offer is non-encashable, not extendable and non-negotiable.</li>
                        </ol>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 1 -->
    <div class="modal fade" id="Modal34" tabindex="-1" role="dialog" aria-labelledby="bModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Croma</h4>
	      </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="offer_condi">
                        <h4>Terms &amp; Conditions:</h4>
                    </div>
                    <div class="offer_dtls">
                        <ol>
                        	<li>The Offer is valid till <strong>31 March 2019</strong>.</li>
                            <li>Under this offer, cardholder will get Rs. 1000 off on spend of Rs 20,000 and above.</li>
                            <li>Offer is not valid on purchase of mobiles.</li>
                            <li>The offer can be availed only on transactions done using an eligible Mastercard Card issued in India. </li>
                        
                        	<p>To avail the Offer, cardholder will need to follow steps: </p>
                        
                            <ol>
                            	<li>Cardholder must send an SMS from their mobile: <strong>GCACT <code>space</code> CRMMCA</strong> to <strong>72006 66000</strong>.</li>
                        		<li>Cardholder receives a response SMS with eGift Card Number, PIN Number, eGiftCard expiry date and denomination.</li>
                            </ol>
                        	<li>The eGiftCard can be redeemed at Croma stores only. Not valid for online purchases</li>
                        	<li>Cardholder must announce the intention to avail the offer and present the eGiftCard and PIN number before the bill is generated.</li>
                        	<li>Cardholders are required to pay any applicable GST, service charges and other taxes.</li>
                        	<li>The offer is non-binding, non-encashable and non-negotiable. </li>
                        	<li>No two offers can be clubbed together and no additional discount/ offer will be applicable. </li>
                        	<li>The offer can be availed only once during the offer period. </li>
                        	<li>Any dispute regarding delivery, service, suitability, merchantability, availability or quality of the offer/or product/services availed under this offer must be addressed in writing, by the cardholder directly to Croma at <a href="Corporate@croma.com">Corporate@croma.com</a> or call <a href="tel:+91-7207 666 000">+91-7207 666 000</a>. Mastercard or the issuing bank will not be liable for the same.</li>
                        	<li>Any dispute relating to the offer will be settled under the sole and exclusive jurisdiction of Mumbai courts only. </li>
                        	<li>Any cardholder who utilises, participates in, or attempts to utilise or participate in the Mastercard Card Offers programme, irrevocably agrees to the following: Mastercard will not be liable under contract, tort or any other theory of law for any claim regarding loss or damage howsoever incurred in relation to the use (or attempted use) of the Offer; and accordingly, the cardholder will not make any claim against Mastercard regarding the same.</li>
                        	<li>Each benefit/privilege may be subject to additional terms and conditions imposed by Croma. Cardholders are solely responsible for checking and complying with the same.</li>
                        	<li>Mastercard reserves the right to add, alter, modify, change or vary any of these terms and conditions or to replace, wholly or in part, this offer by another offer, whether similar or not, or to withdraw it altogether at any point in time, without any prior notice.</li>
                        </ol>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 1 -->
    <div class="modal fade" id="Modal36" tabindex="-1" role="dialog" aria-labelledby="bModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Grofers</h4>
	      </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="offer_condi">
                        <h4>Terms &amp; Conditions:</h4>
                    </div>
                    <div class="offer_dtls">
                        <ol>
                        	<li>The Offer is valid till <strong>28 Feburary 2019</strong>.</li>
                            <li>Under this Offer</li>
                            <ol type="a">
                            	<li>Cardholders who are first-time users will get Rs. 300 cashback on minimum purchase of Rs. 1500  at <a href="www.grofers.com" target="_blank">www.grofers.com</a> by quoting the promotion code <strong>MCNEW</strong></li>
                            	<li>Cardholders who are registered users will get Rs. 100 cashback on minimum purchase of Rs. 2000  at <a href="www.grofers.com" target="_blank">www.grofers.com</a> by quoting the promotion code <strong>MASTERCARD100</strong></li>
                            </ol>
                            <li>Cashback will be credited as Grofers Cash in users Grofers Account.</li>
                            <li>Offer not valid on oil, ghee and baby food products.</li>
                            <li>The Offer is available only upon making full payment by a valid and unexpired Mastercard card issued in India. Not valid on Netbanking or Wallet payments.</li>
                            <li>Cardholder must apply the promo code before paying online to avail the offer.</li>
                            <li>To redeem this Offer, cardholders should log in to their account or create an account, if they are not registered. </li>
                            <li>The Offer cannot be combined together with any other promotion or code-based offer and no additional discount or offer will be applicable.</li>
                            <li>The Offer is non-binding, non-encashable and non-negotiable.</li>
                            <li>Offer is non-transferable and cannot be exchanged for cash or cheque or any form of credit.</li>
                            <li> Total minimum spends will be calculated on the basis of product cost only excluding all taxes, GST, delivery charges, etc. Any taxes or liabilities or charges payable to the Government or any other authority or body, if any, shall be borne directly by the cardholder and/ or billed to the account of the cardholder</li>
                            <li>Any dispute regarding delivery, service, suitability, merchantability, availability or quality of the offer/or product/services availed under this offer must be addressed in writing, by the cardholder directly to Grofers at <a href="mailto:info@grofers.com">info@grofers.com</a>. Mastercard or the issuing bank will not be liable for the same.</li>
                            <li> Cardholders are not bound in any way to participate in the Offer. Any participation shall be voluntary and all participants understand, acknowledge and agree that the Offer is purely on a best effort basis and Mastercard does not assume any liability whatsoever regarding the Offer, the delivery of services or any incidental matter.</li>
                            <li> Any cardholder who utilises, participates in, or attempts to utilise or participate in the Mastercard Card Offers programme, irrevocably agrees to the following: Mastercard will not be liable under contract, tort or any other theory of law for any claim regarding loss or damage howsoever incurred in relation to the use (or attempted use) of the Offer; and accordingly, the cardholder will not make any claim against Mastercard regarding the same.</li>
                            <li>Each benefit/privilege may be subject to additional terms and conditions imposed by Grofers.com. Cardholders are solely responsible for checking and complying with the same</li>
                            <li>These terms and conditions shall be governed by the laws of India and any dispute arising out of or in connection with these terms and conditions shall be subject to the exclusive jurisdiction of the courts in Mumbai.</li>
                            <li>Mastercard reserves the right to add, alter, modify, change or vary any of these terms and conditions or to replace, wholly or in part, this Offer by another offer, whether similar or not, or to withdraw it altogether at any point in time, without any prior notice.</li>
                        </ol>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 1 -->
    <div class="modal fade" id="Modal37" tabindex="-1" role="dialog" aria-labelledby="bModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Fab Hotels</h4>
	      </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="offer_condi">
                        <h4>Terms &amp; Conditions:</h4>
                    </div>
                    <div class="offer_dtls">
                        <ol>
                        	<li>The Offer is valid till <strong>31 December 2018</strong>.</li>
                            <li>Under this Offer</li>
                            <ol type="a">
                            	<li>Cardholders get Rs.750 off on hotel bookings of Rs.1,999 and above at <a href="www.fabhotels.com" target="_blank">www.fabhotels.com</a> by quoting the promotion code <strong>FHMC750</strong>.</li>
                            </ol>
                            <li>The offer can be availed only once during the offer period.</li>
                            <li>The offer can be availed only on transactions done using an eligible Mastercard Card issued in India.</li>
                            <li>To redeem this offer, cardholders should login to their account or create an account, if they are not registered.</li>
                            <li>Offer is non-transferable and cannot be exchanged for cash or cheque or any form of credit.</li>
                            <li>Total minimum spends will be calculated on the basis of product cost only excluding all taxes, GST, delivery charges, etc. Any taxes or liabilities or charges payable to the Government or any other authority or body, if any, shall be borne directly by the cardholder and/ or billed to the account of the cardholder</li>
                            <li> Any dispute regarding delivery, service, suitability, merchantability, availability or quality of the offer/or product/services availed under this offer must be addressed in writing, by the cardholder directly to Fab Hotels at <a href="mailto:bookings@fabhotels.com">bookings@fabhotels.com</a> or by calling <a href="tel:+91 70 4242 4242">+91 70 4242 4242</a>. Mastercard or the issuing bank will  not be liable for the same.</li>
                            <li>Cardholders are not bound in any way to participate in the Offer. Any participation shall be voluntary and all participants understand, acknowledge and agree that the Offer is purely on a best effort basis and Mastercard does not assume any liability whatsoever regarding the Offer, the delivery of services or any incidental matter.</li>
                            <li>No two offers can be clubbed together and no additional discount/offer will be applicable.</li>
                            <li> Any cardholder who utilises, participates in, or attempts to utilise or participate in the Mastercard Card Offers programme, irrevocably agrees to the following: Mastercard will not be liable under contract, tort or any other theory of law for any claim regarding loss or damage howsoever incurred in relation to the use (or attempted use) of the Offer; and accordingly, the cardholder will not make any claim against Mastercard regarding the same.</li>
                            <li>Each benefit/privilege may be subject to additional terms and conditions imposed by Fab Hotels. Cardholders are solely responsible for checking and complying with the same.</li>
                            <li>Any dispute relating to the offer will be settled under the sole and exclusive jurisdiction of Mumbai courts only.</li>
                            <li>Mastercard reserves the right to add, alter, modify, change or vary any of these terms and conditions or to replace, wholly or in part, this Offer by another offer, whether similar or not, or to withdraw it altogether at any point in time, without any prior notice.</li>
                        </ol>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 1 -->
    <div class="modal fade" id="Modal38" tabindex="-1" role="dialog" aria-labelledby="bModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Urban Clap</h4>
	      </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="offer_condi">
                        <h4>Terms &amp; Conditions:</h4>
                    </div>
                    <div class="offer_dtls">
                        <ol>
                        	<li>The Offer is valid till <strong>31 March 2019</strong>.</li>
                            <li>Under this Offer</li>
                            <ol type="a">
                            	<li> Cardholders who are first-time users will get Rs. 200 cashback on minimum purchase of Rs. 1000  at UrbanClap Mobile App by quoting the promotion code <strong>MC200UC</strong></li>
                            	<li>Cardholders who are registered users will get 15% cashback on minimum purchase of Rs. 300 (maximum cashback of Rs. 100)  at UrbanClap Mobile App by quoting the promotion code <strong>MC15X</strong></li>
                            </ol>
                            <li>Offer can be availed only once on Mobile App.</li>
                            <li> To redeem this offer on the mobile app, cardholders should login to their account or create an account, if they are not registered.</li>
                            <li> The offer can be availed only on transactions done using an eligible Mastercard Card issued in India. Not valid on Netbanking or Wallet payments.</li>
                            <li>Offer is non-transferable and cannot be exchanged for cash or cheque or any form of credit.</li>
                            <li>Total minimum spends will be calculated on the basis of product cost only excluding all taxes, GST, delivery charges, etc. Any taxes or liabilities or charges payable to the Government or any other authority or body, if any, shall be borne directly by the cardholder and/ or billed to the account of the cardholder</li>
                            <li>Any dispute regarding delivery, service, suitability, merchantability, availability or quality of the offer/or product/services availed under this offer must be addressed in writing, by the cardholder directly to UrbanClap at <a href="mailto:help@urbanclap.com">help@urbanclap.com</a> or by calling <a href="tel:+1-800-419-0020">+1-800-419-0020</a>. Mastercard or the issuing bank will  not be liable for the same.</li>
                            <li> Cardholders are not bound in any way to participate in the Offer. Any participation shall be voluntary and all participants understand, acknowledge and agree that the Offer is purely on a best effort basis and Mastercard does not assume any liability whatsoever regarding the Offer, the delivery of services or any incidental matter.</li>
                            <li>No two offers can be clubbed together and no additional discount/offer will be applicable.</li>
                            <li>Any cardholder who utilises, participates in, or attempts to utilise or participate in the Mastercard Card Offers programme, irrevocably agrees to the following: Mastercard will not be liable under contract, tort or any other theory of law for any claim regarding loss or damage howsoever incurred in relation to the use (or attempted use) of the Offer; and accordingly, the cardholder will not make any claim against Mastercard regarding the same.</li>
                            <li> Each benefit/privilege may be subject to additional terms and conditions imposed by UrbanClap. Cardholders are solely responsible for checking and complying with the same.</li>
                            <li>Any dispute relating to the offer will be settled under the sole and exclusive jurisdiction of Mumbai courts only.</li>
                            <li> Mastercard reserves the right to add, alter, modify, change or vary any of these terms and conditions or to replace, wholly or in part, this Offer by another offer, whether similar or not, or to withdraw it altogether at any point in time, without any prior notice.</li>
                        </ol>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 1 -->
    <div class="modal fade" id="Modal39" tabindex="-1" role="dialog" aria-labelledby="bModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">FoodPanda</h4>
	      </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="offer_condi">
                        <h4>Terms &amp; Conditions:</h4>
                    </div>
                    <div class="offer_dtls">
                        <ol>
                        	<li>The Offer is valid till <strong>30 September 2018</strong>.</li>
                            <li>Under this Offer</li>
                            <ol type="a">
                            	<li>Cardholders who are first-time users will get Rs. 100 Off on spend of Rs. 275 and above by quoting the promotion code <strong>FPMASTER</strong>.</li>
                            </ol>
                            <li>Offer is not valid on Dominos, Subway & Burger King.</li>
                            <li>Offer is valid only on delivery orders, and cannot be availed for pick up orders. In no case whatsoever will the discount amount be refunded, encashed or partly encashed.</li>
                            <li>Offer valid only once for online orders placed on www.foodpanda.in website and mobile app.</li>
                            <li> To redeem this offer, cardholders should login to their account or create an account, if they are not registered.</li>
                            <li>The offer can be availed only on transactions done using an eligible Mastercard Card issued in India. Not valid on Netbanking or Wallet payments.</li>
                            <li>Cardholder must apply the promo code before paying online to avail the offer.</li>
                            <li>Offer is non-transferable and cannot be exchanged for cash or cheque or any form of credit.</li>
                            <li> Total minimum spends will be calculated on the basis of product cost only excluding all taxes, GST, delivery charges, etc. Any taxes or liabilities or charges payable to the Government or any other authority or body, if any, shall be borne directly by the cardholder and/ or billed to the account of the cardholder</li>
                            <li> Any dispute regarding delivery, service, suitability, merchantability, availability or quality of the offer/or product/services availed under this offer must be addressed in writing, by the cardholder directly to Foodpanda at <a href="mailto:customercare@foodpanda.in">customercare@foodpanda.in</a>. Mastercard or the issuing bank will not be liable for the same.</li>
                            <li>Cardholders are not bound in any way to participate in the Offer. Any participation shall be voluntary and all participants understand, acknowledge and agree that the Offer is purely on a best effort basis and Mastercard does not assume any liability whatsoever regarding the Offer, the delivery of services or any incidental matter.</li>
                            <li>No two offers can be clubbed together and no additional discount/offer will be applicable.</li>
                            <li> Any cardholder who utilises, participates in, or attempts to utilise or participate in the Mastercard Card Offers programme, irrevocably agrees to the following: Mastercard will not be liable under contract, tort or any other theory of law for any claim regarding loss or damage howsoever incurred in relation to the use (or attempted use) of the Offer; and accordingly, the cardholder will not make any claim against Mastercard regarding the same.</li>
                            <li>Each benefit/privilege may be subject to additional terms and conditions imposed by Foodpanda. Cardholders are solely responsible for checking and complying with the same.</li>
                            <li>Any dispute relating to the offer will be settled under the sole and exclusive jurisdiction of New Delhi courts only.</li>
                            <li>Mastercard reserves the right to add, alter, modify, change or vary any of these terms and conditions or to replace, wholly or in part, this Offer by another offer, whether similar or not, or to withdraw it altogether at any point in time, without any prior notice.</li>
                        </ol>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- ------------------------------------------------------------ -->
    <!-- Modal 1 -->
    <div class="modal fade" id="Modal40" tabindex="-1" role="dialog" aria-labelledby="bModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Lenskart</h4>
	      </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="offer_condi">
                        <h4>Terms &amp; Conditions:</h4>
                    </div>
                    <div class="offer_dtls">
                        <ol>
                        	<li>The Offer is valid till <strong>30 September 2018</strong>.</li>
                            <li>Under this Offer</li>
                            <!-- <ol type="a">
                            	<li>Cardholders who are first-time users will get Rs. 100 Off on spend of Rs. 275 and above by quoting the promotion code <strong>FPMASTER</strong>.</li>
                            	<li>Voucher applicable on purchase of Rs.4000 & above.</li>
                            </ol> -->
                            <li>Voucher applicable on purchase of <strong>Rs.4000</strong> & above.</li>
                            <li>Offer available only on Cashier physical card transaction.</li>
                            <li>This offer cannot be clubbed with any other offer or schemes.</li>
                            <li>Applicable on Vincent chase and john Jacobs only.</li>
                            <li>Not valid on contact lenses, Lens solution, FFF & accessories.</li>
                            <li><a href="http://www.lenskart.com/">Lenskart.com</a> reserves the right to change/modify terms and conditions of gift voucher</li>
                        </ol>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 1 -->
    <div class="modal fade" id="Modal41" tabindex="-1" role="dialog" aria-labelledby="bModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Coolwinks</h4>
	      </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="offer_condi">
                        <h4>Terms &amp; Conditions:</h4>
                    </div>
                    <div class="offer_dtls">
                        <ol>
                        	<li>The Offer is valid till <strong>30 August 2018</strong>.</li>
                            <li>Under this Offer</li>
                            <ol type="a">
                            	<li>Customer will get Flat <strong>Rs.100</strong> Cashback.</li>
                            </ol>
                            <li>Offer valid only on Cashier Physical Cards only</li>
                            <li>User is eligible only for once with one card for his 1st transaction with the card for the purchase.</li>
                            <li>Cashback will be credited to customer's cashier card account within 7 working days on successful completion of the transaction.</li>
                        </ol>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 1 -->
    <div class="modal fade" id="Modal42" tabindex="-1" role="dialog" aria-labelledby="bModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">BookMyShow</h4>
	      </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="offer_condi">
                        <h4>Terms &amp; Conditions:</h4>
                    </div>
                    <div class="offer_dtls">
                        <ol>
                        	<li>The Offer is valid till <strong>31 July 2018</strong>.</li>
                            <li>Under this Offer</li>
                            <ol type="a">
                            	<li>Customer will get Flat <strong>Rs.100</strong> Cashback.</li>
                            </ol>
                            <li>Offer valid only on Cashier Physical Cards only</li>
                            <li>User is eligible only for once with one card for his 1st transaction with the card for the purchase.</li>
                            <li>Cashback will be credited to customer's cashier card account within 7 working days on successful completion of the transaction.</li>
                        </ol>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 1 -->
    <div class="modal fade" id="Modal43" tabindex="-1" role="dialog" aria-labelledby="bModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">OLA</h4>
	      </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="offer_condi">
                        <h4>Terms &amp; Conditions:</h4>
                    </div>
                    <div class="offer_dtls">
                        <ol>
                        	<li>The Offer is valid till <strong>31 August 2018</strong>.</li>
                        	<li>Offer valid only on Cashier Physical Cards only.</li>
                        	<li>User is eligible only for once with one card for his 1st transaction with the card for the purchase.</li>
                            <li>Offer vaild on a minimum transaction value of <strong>Rs.200</strong> and is valid for bookings done for mico/mini/prime only.</li>
                            <li>Under this Offer: Customer will get <strong>Flat Rs.100</strong> Cashback</li>
                            <li>Cashback will be credited to customer's cashier card account within 7 working days on successful completion of the transaction.</li>
                        </ol>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 1 -->
    <div class="modal fade" id="Modal44" tabindex="-1" role="dialog" aria-labelledby="bModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">OYO</h4>
	      </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="offer_condi">
                        <h4>Terms &amp; Conditions:</h4>
                    </div>
                    <div class="offer_dtls">
                        <ol>
                        	<li>The offer is valid for bookings made from <strong>24th May 2018</strong> till <strong>30th June 2018</strong>, 11:59 PM only.</li>
                        	<li>The offer is valid for check-ins from <strong>24th May 2018</strong> till <strong>30th June 2018</strong>, 11:59 PM only.</li>
                        	<li>The offer is applicable on  OYO app, web and mweb.</li>
                            <li>The offer is applicable for a maximum of 4 nights only.</li>
                            <li>The offer is valid for once per user.</li>
                            <li>All booking using code <strong>CASHOYO</strong> is non-modifiable.</li>
                        </ol>
                        <strong>General Terms and Conditions:</strong>
                        <ol>
                        	<li>Cancellation charges will be applicable as per the OYO policy.</li>
                        	<li>This offer cannot be combined with any other offer currently applicable on OYO website, Android app and iOS app. </li>
                        	<li>All bookings are subject to availability.</li>
                        	<li>Discounted bookings cannot be modified.</li>
                        	<li>All disputes relating to this promotion/offer or OYO booking are subject to exclusive jurisdiction of Delhi courts.</li>
                        	<li>OYO reserves the right to discontinue the offer without any prior notice.</li>
                        </ol>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 1 -->
    <div class="modal fade" id="Modal45" tabindex="-1" role="dialog" aria-labelledby="bModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Health & Glow</h4>
	      </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="offer_condi">
                        <h4>Terms &amp; Conditions:</h4>
                    </div>
                    <div class="offer_dtls">
                        <ol>
                        	<li>Offer valid till <strong>31st August 2018</strong>.</li>
                        	<li>Offer valid only on Cashier Physical Cards only.</li>
                        	<li>User is eligible only for once with one card for his 1st transaction with the card for the purchase.</li>
                            <li>Customer  will get Flat Rs. 100 Cashback.</li>
                            <li>Cashback will be credited to customer's cashier card account within 7 working days on successful completion of the transaction.</li>
                        </ol>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 1 -->
    <div class="modal fade" id="Modal46" tabindex="-1" role="dialog" aria-labelledby="bModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">SHIV SAGAR</h4>
	      </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="offer_condi">
                        <h4>Terms &amp; Conditions:</h4>
                    </div>
                    <div class="offer_dtls">
                        <ol>
                        	<li>Offer valid till <strong>31st August 2018</strong>.</li>
                        	<li>Offer valid only on Cashier Physical Cards only.</li>
                        	<li>User is eligible only for once with one card for his 1st transaction with the card for the purchase.</li>
                            <li>Customer  will get Flat Rs. 100 Cashback.</li>
                            <li>Offer valid on a minimum transaction value of Rs.150</li>
                            <li>Valid on  outlets - Kamala Mills, Lower Parel,  T2  Airport, Andheri, Marketcity Mall, Kurla, Mira Road ,Kalina, Santacruz only</li>
                            <li>Cashback will be credited to customer's cashier card account within 7 working days on successful completion of the transaction.</li>
                        </ol>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 1 -->
    <div class="modal fade" id="Modal47" tabindex="-1" role="dialog" aria-labelledby="bModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Zingoy</h4>
	      </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="offer_condi">
                        <h4>Terms &amp; Conditions</h4>
                    </div>
                    <div class="offer_dtls">
                        <ol>
                            <li>Offer valid till <strong>31st September 2018</strong>.</li>
                            <li>Offer valid only on Cashier Physical Cards only</li>
                            <li>Under this offer, User is eligible only for once with one card for his 1st transaction with the card for the purchase.</li>
                            <li>Customer will get Flat <strong>Rs.100</strong> Cashback</li>
                            <li>Cashback will be credited to customer's cashier card account within 7 working days on successful completion of the transaction.</li>
                        </ol>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 1 -->
    <div class="modal fade" id="Modal25" tabindex="-1" role="dialog" aria-labelledby="bModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Bigbasket</h4>
	      </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-9">
                    <div class="offer_condi">
                        <h4>Terms &amp; Conditions</h4>
                    </div>
                    <div class="offer_dtls">
                        <ol>
                            <li>The Offer is valid till <strong>31 December 2018</strong>.</li>
                            <li>Under this Offer, Cardholders get 20% off on purchases amounting to Rs.1,000 and above at <a href="www.bigbasket.com">www.bigbasket.com</a> by quoting the promotion code <strong>BBMC20</strong> . Maximum discount upto Rs.200.</li>
                            <li>Offer valid only for First Time users on Big Basket.</li>
                            <li>Products and services are subject to availability. Offer not valid in Surat, Indore and Bhopal.</li>
                            <li>The Offer is available only upon making full payment by a valid and unexpired Mastercard card issued in India. Offer is not valid for Cash on Delivery.</li>
                            <li>To redeem this offer, cardholders should login to their account or create an account, if they are not registered.</li>
                            <li>The Offer is non-transferable and cannot be exchanged for cash or cheque or any form of credit.</li>
                            <li>The total minimum spend will be calculated based on the product cost only. This excludes all taxes, GST, delivery charges, etc. Any taxes, liabilities or charges payable to the government or any other authority or body, if any, shall be borne directly by the cardholder and/or billed to the account of the cardholder.</li>
                            <li>Any dispute regarding delivery, service, suitability, merchantability, availability or quality of the offer/or product/services related to this offer must be addressed in writing, by the cardholder directly to Big Basket at <a href="mailto:customerservice@bigbasket.com">customerservice@bigbasket.com</a> or by calling 1860-123-1000. Mastercard or the issuing bank will not be liable for the same.</li>
                            <li>Cardholders are not bound in any way to participate in the Offer. Any participation shall be voluntary and all participants understand, acknowledge and agree that the Offer is purely on a best effort basis and Mastercard does not assume any liability whatsoever regarding the Offer, the delivery of services or any incidental matter.</li>
                            <li>No two offers can be clubbed together and no additional discount/offer will be applicable.</li>
                            <li>Any cardholder who utilises, participates in, or attempts to utilise or participate in the Mastercard Card Offers programme, irrevocably agrees to the following: Mastercard will not be liable under contract, tort or any other theory of law for any claim regarding loss or damage howsoever incurred in relation to the use (or attempted use) of the Offer; and accordingly, the cardholder will not make any claim against Mastercard regarding the same.</li>
                            <li>Each benefit/privilege may be subject to additional terms and conditions imposed by Big Basket. Cardholders are solely responsible for checking and complying with the same.</li>
                            <li>These terms and conditions shall be governed by the laws of India and any dispute arising out of or in connection with these terms and conditions shall be subject to the exclusive jurisdiction of the courts in Mumbai.</li>
                            <li>Mastercard reserves the right to add, alter, modify, change or vary any of these terms and conditions or to replace, wholly or in part, this Offer by another offer, whether similar or not, or to withdraw it altogether at any point in time, without any prior notice.</li>
                        </ol>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="offer-txt text-center mt-2">
                        <span>Promotion Code</span>
                    </div>
                    <div class="coupon_cd text-center">
                        <a class="btn btn-danger btn-block">TRBMC</a>
                        <small>Click button to copy code</small>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 1 -->
    <div class="modal fade" id="Modal26" tabindex="-1" role="dialog" aria-labelledby="bModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Citrus Hotel</h4>
	      </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="offer_condi">
                        <h4>Terms &amp; Conditions</h4>
                    </div>
                    <div class="offer_dtls">
                        <ol>
                            <li>The Offer is valid till <strong>30th March 2019</strong>.</li>
                            <li>Under the Offer, cardholders will get 15% off on  best available rates on accommodation and Cardholders will also get 15% off on F&B</li>
                            <li>The Offer is available only upon making full payment by a valid and unexpired Mastercard card issued in South Asia.</li>
                            <li>The Offer is available on bookings atÂ Citrus Hotels & ResortsÂ properties POS counters or through Citrus Hotel Reservation toll-free line 1800 3001 4001.</li>
                            <li>Cardholder must also indicate intention to redeem the Offer before making a booking.</li>
                            <li>Discount on room will not be available if the booking is done through Travel Agent , OTA or by any third party.</li>
                            <li>To redeem the Offer, full payment must be made using a valid and unexpired Mastercard card at the time of bill settlement. </li>
                            <li>This Offer is valid only on the best available rates displayed at time of booking. Cardholders are required to pay any applicable GST, service charge on other Taxes.</li>
                            <li>The Offer cannot be combined together with any other promotion code-based offer and no additional discount/ offer will be applicable.</li>
                            <li>The Offer is non-binding, non-encashable and non-negotiable.</li>
                            <li>Any dispute regarding delivery, service, suitability, merchantability, availability or quality of the Offer or services availed under this Offer must be addressed in writing by the cardholder directly to Citrus Hotel at <a href="mailto:reservations@citrushotels.in">reservations@citrushotels.in</a> or by calling the toll-free number 1800 3001 4001. Mastercard or the issuing bank will not be liable for the same.</li>
                            <li>Any dispute relating to the Offer will be settled under the sole and exclusive jurisdiction of Mumbai courts only.</li>
                            <li>Cardholders are not bound in any way to participate in the Offer. Any participation shall be voluntary and all participants understand, acknowledge and agree that the Offer is purely on a best effort basis and Mastercard does not assume any liability whatsoever regarding the Offer, the delivery of services or any incidental matter.</li>
                            <li>Any cardholder who utilises, participates in, or attempts to utilise or participate in the Mastercard Card Offers programme, irrevocably agrees to the following: Mastercard will not be liable under contract, tort or any other theory of law for any claim regarding loss or damage howsoever incurred in relation to the use (or attempted use) of the Offer; and accordingly, the cardholder will not make any claim against Mastercard regarding the same.</li>
                            <li>Each benefit/privilege may be subject to additional terms and conditions imposed by Citrus Hotels. Cardholders are solely responsible for checking and complying with the same.</li>
                            <li>Mastercard reserves the right to add, alter, modify, change or vary any of these terms and conditions or to replace, wholly or in part, this offer by another offer, whether similar or not, or to withdraw it altogether at any point in time, without any prior notice.</li>
                        </ol>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 1 -->
    <div class="modal fade" id="Modal27" tabindex="-1" role="dialog" aria-labelledby="bModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">US Pizza</h4>
	      </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="offer_condi">
                        <h4>Terms &amp; Conditions</h4>
                    </div>
                    <div class="offer_dtls">
                        <ol>
                            <li>The Offer is valid till <strong>31st March 2019</strong>.</li>
                            <li>Valid for Dine in and Take away only.</li>
                            <li>Offer not valid on Sunday.</li>
                            <li>Offer is applicable for bill value excluding GST, service charges etc.</li>
                            <li>Offer NOT valid for Margarita Pizza. </li>
                            <li>The complimentary Pizza must be of the same or lesser value than the one being purchased.</li>
                            <li>Offer is valid across all U.S. Pizza outlets except Bellary (Karnataka), Navasari (Gujarat) and institutional cafeteria outlets.</li>
                            <li>The Offer is available only upon making full payment with a valid and unexpired World Elite, World, Platinum, Titanium, Gold, Standard, Prepaid, Maestro, Commercial Mastercards card issued in South Asia.</li>
                            <li>Cardholder needs to ask for the offer  before the bill is generated and may be asked to show an eligible card for confirmation.</li>
                            <li>Offer cannot be combined with any other offer/promotion/discount applicable at the participating outlets.</li>
                            <li>The Offer is non-transferable and cannot be exchanged for cash or any other merchandise, products, discounts, coupons or special offers.</li>
                            <li>Offer is intended for individual consumption and not for resale. to U.S. Pizza reserves the right to deny the offer if it has reason to believe otherwise.</li>
                            <li>Cardholders are not bound in any way to participate in the Offer. Any participation shall be voluntary and all participants understand, acknowledge and agree that the Offer is purely on a best effort basis and Mastercard does not assume any liability whatsoever regarding the Offer, the delivery of services or any incidental matter.</li>
                            <li>Any cardholder who utilises, participates in, or attempts to utilise or participate in the Mastercard Card Offers programme, irrevocably agrees to the following: Mastercard will not be liable under contract, tort or any other theory of law for any claim regarding loss or damage howsoever incurred in relation to the use (or attempted use) of the Offer; and accordingly, the cardholder will not make any claim against Mastercard regarding the same.</li>
                            <li>Any claims, complaints or issues regarding service, quality or products etc. need to be made to U.S. Pizza directly. Mastercard or the issuing bank will not be responsible for the same.</li>
                            <li>These terms and conditions shall be governed by the laws of India and any dispute arising out of or in connection with these terms and conditions shall be subject to the exclusive jurisdiction of the courts in Mumbai.</li>
                            <li>Each benefit/privilege may be subject to additional terms and conditions imposed by U.S. Pizza Cardholders are solely responsible for checking and complying with the same.</li>
                            <li> Mastercard reserves the right to add, alter, modify, change or vary any of these terms and conditions or to replace, wholly or in part, this Offer by another offer, whether similar or not, or to withdraw it altogether at any point in time, without any prior notice.</li>
                        </ol>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 1 -->
    <div class="modal fade" id="Modal29" tabindex="-1" role="dialog" aria-labelledby="bModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Awfis.com</h4>
	      </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="offer_condi">
                        <h4>Terms &amp; Conditions</h4>
                    </div>
                    <div class="offer_dtls">
                        <ol>
                            <li>The Offer is valid till <strong>31st March 2019</strong>.</li>
                            <li>Under the Offer, cardholders will get 20% off on all meeting rooms at Awifis centres and Hotels listed on <a href="www.awfis.com">www.awfis.com</a> by quoting the promotion code AWFMS20. Cardholders can also get 15% off all working desks (Cabin, Fixed, Flexi desks) at all Awfis pro-working centres with the promotion code <strong>AWFMS15</strong>.</li>
                            <li>The Offer is applicable only on the website <a href="www.awfis.com">www.awfis.com</a>  and mobile application.</li>
                            <li>The Offer is applicable for bookings of any duration from 1 day to 11 months.</li>
                            <li>Bookings are subject to availability and are governed by Awfisâ€™ user agreement and standard terms and conditions of Awfis.</li>
                            <li>The Offer is available only upon making full payment by a valid and unexpired Mastercard card issued in India.</li>
                            <li>To redeem this Offer, cardholders should log in to their account or create an account, if they are not registered.</li>
                            <li>Total spends will be calculated on the basis of product cost only excluding all taxes, GST, delivery charges, etc. Any taxes or liabilities or charges payable to the Government or any other authority or body, if any, shall be borne directly by the cardholder and/ or billed to the account of the cardholder</li>
                            <li>The Offer cannot be combined together with any other promotion code based offer and no additional discount/offer will be applicable.</li>
                            <li>The Offer is non-transferable and cannot be exchanged for cash or cheque or any form of credit.</li>
                            <li> The Offer is non-binding, non-encashable and non-negotiable.</li>
                            <li>The Offer must be redeemed in full and not in parts.</li>
                            <li>Any dispute regarding delivery, service, suitability, merchantability, availability or quality of the Offer/or product/services availed under this Offer must be addressed by the cardholder directly to the Awfis Community Manager at the respective centre in writing to <a href="mailto:contact@awfis.com">contact@awfis.com</a>  or by calling the toll-free number 1860 258 6633. Mastercard or the issuing bank will not be liable for the same.</li>
                            <li>Cardholders are not bound in any way to participate in the Offer. Any participation shall be voluntary and all participants understand, acknowledge and agree that the Offer is purely on a best effort basis and Mastercard does not assume any liability whatsoever regarding the Offer, the delivery of services or any incidental matter.</li>
                            <li>Any cardholder who utilises, participates in, or attempts to utilise or participate in the Mastercard Card Offers programme, irrevocably agrees to the following: Mastercard will not be liable under contract, tort or any other theory of law for any claim regarding loss or damage howsoever incurred in relation to the use (or attempted use) of the Offer; and accordingly, the cardholder will not make any claim against Mastercard regarding the same.</li>
                            <li>These terms and conditions shall be governed by the laws of India and any dispute arising out of or in connection with these terms and conditions shall be subject to the exclusive jurisdiction of the courts in Mumbai.</li>
                            <li>Each benefit/privilege may be subject to additional terms and conditions imposed by Awfis. Cardholders are solely responsible for checking and complying with the same.</li>
                            <li>Mastercard reserves the right to add, alter, modify, change or vary any of these terms and conditions or to replace, wholly or in part, this Offer by another offer, whether similar or not, or to withdraw it altogether at any point in time, without any prior notice.</li>
                        </ol>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 1 -->
    <div class="modal fade" id="Modal32" tabindex="-1" role="dialog" aria-labelledby="bModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Joyalukkas</h4>
	      </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="offer_condi">
                        <h4>Terms &amp; Conditions</h4>
                    </div>
                    <div class="offer_dtls">
                        <ol>
                            <li>The Offer is valid till <strong>30th June 2019</strong>.</li>
                            <li>Under this offer, cardholders get Rs.3,000 Off on a minimum purchase of Rs.50,000 on Diamond jewellery. Also, save 25% off on Gold jewellery making charges at Joyalukkas stores. Offer is not valid on Gold coins and Gold bars.</li>
                            <li>Cardholders need to indicate their intention to redeem the Offer before the bill is generated and may be asked to present their card for confirmation.</li>
                            <li>Offer is valid only at Joyalukkas stores in India and not online purchases.</li>
                            <li>Offer cannot be combined with any other offer/ promotion/ scheme.</li>
                            <li>The Offer is available only upon making full payment by a valid and unexpired Mastercard card issued in India.</li>
                            <li>Offer is non-transferable and cannot be exchanged for cash, cheque or any form of credit.</li>
                            <li>Total minimum spends will be calculated on the basis of product cost only excluding all taxes, GST, delivery charges, etc. Any taxes or liabilities or charges payable to the Government or any other authority or body, if any, shall be borne directly by the cardholder and/ or billed to the account of the cardholder.</li>
                            <li>In the event of any queries regarding the Offer, customers may contact the Merchant by calling at +91-484-2385035 or sending an email to <a href="mailto:customercare@joyalukkas.com">customercare@joyalukkas.com</a></li>
                            <li>Cardholders are not bound in any way to participate in the Offer. Any participation shall be voluntary and all participants understand, acknowledge and agree that the Offer is purely on a best effort basis and Mastercard does not assume any liability whatsoever regarding the Offer, the delivery of services or any incidental matter.</li>
                            <li> Any cardholder who utilises, participates in, or attempts to utilise or participate in the Mastercard Card Offers programme, irrevocably agrees to the following: Mastercard will not be liable under contract, tort or any other theory of law for any claim regarding loss or damage howsoever incurred in relation to the use (or attempted use) of the Offer; and accordingly, the cardholder will not make any claim against Mastercard regarding the same.</li>
                            <li>Any claims, complaints or issues regarding service, quality or products etc. need to be made to Joyalukkas directly. Mastercard or the issuing bank will not be responsible for the same.</li>
                            <li>These terms and conditions shall be governed by the laws of India and any dispute arising out of or in connection with these terms and conditions shall be subject to the exclusive jurisdiction of the courts in Mumbai.</li>
                            <li>Each benefit/ privilege may be subject to additional terms and conditions imposed by Joyalukkas. Cardholders are solely responsible for checking and complying with the same.</li>
                            <li>Mastercard reserves the right to add, alter, modify, change or vary any of these terms and conditions or to replace, wholly or in part, this Offer by another offer, whether similar or not, or to withdraw it altogether at any point in time, without any prior notice.</li>
                        </ol>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- ==================================================== -->



	<!-- scripts starts here -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/js/jquery.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/bootstrap/js/bootstrap.min.js"></script>
	<!-- slick slider -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/slick/slick.min.js"></script>

	<!-- custom js for dashboard -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/js/dashboard.js"></script>

</body>
</html>