<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<title>EWire |FAQs</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<meta name="keywords" content="Cashier Card - Enjoy great offers online & offline using Cashier Prepaid card. Fast & Easy Recharge. Pay Utility Bills. Swipe & Save.">
    <meta name="description" content="India&#039;s Digital Cash. Go cashless with Cashier Prepaid Card. Experience a seamless, safe payments platform secured with Mastercard.">

    <!-- favicon -->
  	<link rel="icon" href="${pageContext.request.contextPath}/resources/assets/img/favicon.png" type="image/gif" sizes="32x32">

	<!-- Bootstrap css -->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/responsive.css">

	<!-- google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

	<!-- slick slider -->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/slick/slick.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/slick/slick-theme.css">

</head>
<body>

<a href="javascript:void(0);" id="top"><i class="fa fa-chevron-up"></i></a>

<nav class="navbar navbar-white">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="${pageContext.request.contextPath}/Home">
      	<img src="${pageContext.request.contextPath}/resources/assets/img/logo.png" class="img-responsive">
      </a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <!-- <ul class="nav navbar-nav">
        <li class="active"><a href="#">About Us</a></li>
        <li><a href="#">contact</a></li>
        <li><a href="#">offers</a></li> 
        <li><a href="#">Page 3</a></li> 
      </ul> -->
      <ul class="nav navbar-nav navbar-right">
        <li><a href="${pageContext.request.contextPath}/About">about us</a></li>
        <li><a href="#section1">offers</a></li>
        <!-- <li><a href="#section2">testimonial</a></li> -->
        <li><a href="#section3">contact</a></li>
        <!-- <li><a href="#">refer &amp; earn</a></li> -->
      <!--   <li><a href="#" class="btn btn-sm btn-custom" style="margin-right: 6px;">login</a></li>
        <li><a href="#" class="btn btn-sm btn-custom">register</a></li> -->
      </ul>
    </div>
  </div>
</nav>

<div class="body-container">
	<div class="container" id="hero-image">

		<div class="aboutWrp">
			<div class="secHeading text-center">
				<span>Frequently Asked Questions</span>
			</div>

			 <div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading"><h4>What is Ewire?</h4></div>
      <div class="panel-body"><div class="content">
                                <p>Ewire is a Mobile wallet which allows you to make online payments to various merchants, transfer funds to various banks and many more. For example with Ewire, you can topup your mobile, pay postpaid, landline and electricity bills, buy bus tickets, airline tickets and movie tickets, shop online, transfer money from one bank to another.</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>What are the benifits of Ewire Wallet?</h4></div>
      <div class="panel-body"> <div class="content">
                                <p>
                                    <strong>Ewire Wallet is a smart new way to pay:</strong>
                                    <ul>
                                        <li><strong>Safety:- </strong>Your Ewire Wallet is secure with a secret password and MPIN. Your MPIN will work as a security password just like an ATM pin, when you want to open Ewire app you will have to provide your MPIN for authorization to open the app.</li>
                                        <li><strong>Flexible:- </strong>We do not require you to maintain a minimum balance in your Ewire Wallet. Load money as you want, use money as you want. We just help you to manage it better and pay quicker &amp; a little smarter.</li>
                                        <li><strong>Instant:- </strong>Ewire Wallet Money transfer is instant. Its available 24X7 to you at your convenience. Using Ewire Wallet various payments can be made from anywhere, at anytime to anyone in less than 5 seconds.</li>
                                        <li><strong>Smart:- </strong>Ewire Wallet money gives you exclusive value on Ewire Wallet payments. There are a lot of other offers with our merchant partners.</li>
                                    </ul>
                                </p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>How safe and secure is Ewire wallet?</h4></div>
      <div class="panel-body"> <div class="content">
                                <p>Ewire Wallet is secured using highest level of encryption. There are multiple layers of security implemented to protect the Ewire Wallet from any unauthorized access. There is no possibility that a Wallet may be hacked. Whenever you try to open Ewire app, you need to put a Personal Identification Number (MPIN) in your Ewire mobile application for authorization as an extra level of security.</p>
                              </div></div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading"><h4>How do i sign up for an Ewire wallet?</h4></div>
      <div class="panel-body"><div class="content">
                                <p>Signing up for an Ewire Wallet is free and extremely easy. You can opt to sign up in any of the following methods: Go to Google Play Store to Download.</p>
                                <p>Free Ewire Wallet mobile app for android sign up by filling the registration form.</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>If my transaction done through Ewire Wallet is not successful, is my money refunded?</h4></div>
      <div class="panel-body"> <div class="content">
                                <p>Yes, your money will be refunded when your transaction fails while paying through Ewire Wallet. The money automatically gets refunded to your Ewire Wallet within a week. For full details on refunds, please refer to the Refund section in FAQ</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>How can I contact Ewire Wallet?</h4></div>
      <div class="panel-body"><div class="content">
                                <p>For any query, suggestion or help do reach out to us or contact us on Support at Ewire Wallet website or call at our Customer Care number by taping on the call tap mentioned in mobile app.</p>
                              </div></div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading"> <h4>Why should I open a Wallet when I can use my debit card or creditcard?</h4></div>
      <div class="panel-body"><div class="content">
                                <p>Ewire Wallet is safe and lets you do faster check out. Every time you transact through Ewire Wallet you need not furnish sensitive details of your credit and debit card. All you need to do is save your bank details on your My Profile page, load your Wallet through your preferred mode and make easy payments as you recharge, pay bills, transfer money or shop online.</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>Can I transfer funds to a phone number outside the country?</h4></div>
      <div class="panel-body"><div class="content">
                                <p>No, you can transfer funds only to phone numbers registered within India.</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>How do I load my wallet?</h4></div>
      <div class="panel-body"><div class="content">
                                <p>On registering with Ewire Wallet you can load your Wallet in any of the following ways: UPI, Credit card, Debit card and Internet Banking.</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>I forgot my Ewire Wallet password. Now what?</h4></div>
      <div class="panel-body"> <div class="content">
                                <p>You do not need to worry if you forgot your password. You need to follow the steps mentioned below: Reset your password through mobile app itself Click on forgot password tab You will receive a verification code on your registered mobile number By entering the verification code, you can reset the password If you want to change the password again Go to setting at profile page choose option change password Choose a new password.</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"> <h4>I forgot my Ewire Wallet MPIN. What should I do?</h4></div>
      <div class="panel-body"> <div class="content">
                                <p>Do not need to worry if you forgot your MPIN. You need do following: Reset your MPIN through mobile app itself Click on forget MPIN Enter the current Password and Your DOB click on reset Generate new MPIN</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>How do I redeem my reward points?</h4></div>
      <div class="panel-body"> <div class="content">
                                <p>Once you do the any transaction through Ewire wallet by using Ewire balance. You will earn 1(One) reward point on each transaction of Rs.100. It will be shown in you profile page. In order to redeem those points, please contact us on Support at Ewire Wallet App or call at our Customer Care number by taping on the call tap mentioned in mobile app.</p>
                              </div></div>
    </div>
     <h3>Sign Up</h3>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>How do I sign up?</h4></div>
      <div class="panel-body"><div class="content">
                                <p>Signing up for an Ewire Wallet is free and extremely easy. You can opt to sign up in any of the following methods: </p>
                                <ol>
                                    <li>Go to Google Play Store.</li>
                                    <li>Download free Ewiresoft mobile app for android/ios.</li>
                                    <li>Sign up by filling the registration form and finally you will get a OTP. Enter the OTP and you will login into Ewire.</li>
                                </ol>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>What information do I require to sign up?</h4></div>
      <div class="panel-body"><div class="content">
                                <p>We need some very basic information from you like your name, phone number, e-mail id for opening a Ewire Wallet.</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>Where is my mobile verification code?</h4></div>
      <div class="panel-body"><div class="content">
                                <p>Your mobile verification code will be sent to your mobile phone number that you have provided to us during sign up.</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>Can I get a new mobile verification code?</h4></div>
      <div class="panel-body"><div class="content">
                                <p>If you do not receive your verification code within 1 minute, click on the resend verification tab on the sign up page or call Ewire Wallet customer service by clicking on call tap in order to get your new mobile verification code</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>Do I need to sign up to download the Ewire Wallet Android Application?</h4></div>
      <div class="panel-body"><div class="content">
                                <p>No, you do not need to sign up to download the Ewire Wallet Android application. You can directly download the Ewire Wallet app by visiting Google Play store and then use it to sign up for an Ewire Wallet.</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h3>My Account</h3></div>
      <div class="panel-body"> <div class="content">
                                <p>To update any of your profile information, first log in to your Ewire Wallet. To change your state or password, click on Settings tab go to Edit Profile. Your name, date of birth cannot be changed once entered. To view your balance or transaction history, log in to your Ewire Wallet app. You can view last 5 transactions for the last 3 months on your Profile page after you have logged in. Click on Receipts tab to view all transactions.</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>How do I view my previous Wallet statements?</h4></div>
      <div class="panel-body"> <div class="content">
                                <p>In order to view your Wallet statement you will have to first log in to your Ewire Wallet. Under Profile page, Click on My Receipts. you can see last 20 transactions there which you have done through Ewire wallet.</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>Does Ewire Wallet have an expiration date?</h4></div>
      <div class="panel-body"><div class="content">
                                <p>Any wallet not transacting for 6 months or more may be treated as expired and the value may be forfeited. Such wallet user will be intimated through SMS information to this effect in accordance with regulatory frame work prevailing and amended from time to time.</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>Why is it a good idea to keep my money in my Ewire Wallet?</h4></div>
      <div class="panel-body"><div class="content">
                                <p>It's not a good but a great idea to keep money in your Ewire Wallet. If you're looking to send money to your friend, recharge your phone or pay your electricity bill you will not have to enter any card or bank account details to complete the transaction. In fact, there are many who are even uncomfortable doing so. Besides, with Ewire Wallet, you can make your transactions anytime, anywhere 24x7 using your mobile application</p>
                              </div></div>
    </div>
      <h3>Loading, Sending and Withdrawing Money</h3>
    <div class="panel panel-default">
      <div class="panel-heading"> <h4>How do I load money to my Ewire Wallet?</h4></div>
      <div class="panel-body"><div class="content">
                                <p>Log in to your Ewire Wallet and then click on Load money under Profile. You can load money to your Ewire Wallet through any of the following methods: Through NEFT, Select your bank from the list, enter the required details, click to proceed enter your secure code click to proceed Through debit/credit card Through Internet Banking, Put the amount, Select your bank from the list, enter your personal internet banking details, click to proceed enter your secure code click to proceed.</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>How do I send money?</h4></div>
      <div class="panel-body"> <div class="content">
                                <p>Log in to your Ewire Wallet and then click on Fund Transfer. You can send money to any phone number in India regardless the beneficiary is registered to Ewire Wallet or not. Beneficiary will receive the instruction message and have to register with Ewire Wallet first to access the funds.</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>How do I withdraw money?</h4></div>
      <div class="panel-body"> <div class="content">
                                <p>Log in to your Ewire Wallet and then click on Fund Transfer, select Bank Transfer. You can withdraw money from your Ewire Wallet by transferring the money to your bank account.</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>Can I cancel send money transaction incase beneficiary is not registered with Ewire Wallet?</h4></div>
      <div class="panel-body"><div class="content">
                                <p>Ewire Wallet allows you to review every send money transaction before you confirm payment. Unfortunately, you cannot cancel a send money transaction once you have confirmed it after review. However, when you are sending money to a phone number &amp; email Id, if your beneficiary does not open a Wallet within 15 to 20 days of your sending the money, the same is credited back into your Ewire Wallet.</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>Can I send money to a person who is not an Ewire Wallet holder?</h4></div>
      <div class="panel-body"> <div class="content">
                                <p>Yes you can send money to a person who is not an Ewire Wallet holder. The person you choose to send money to will receive an SMS to sign up for an Ewire Wallet in order to accept the amount you are sending. In case the person doesn&#39;t sign up with Ewire Wallet within the stipulated time of 15 to 20 days your Wallet will be credited back with that amount.</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>How do I know when money has been received by the beneficiary?</h4></div>
      <div class="panel-body"><div class="content">
                                <p>We will send you SMS &amp; email notification (if email is registered with us) stating that the beneficiary has received the amount &amp; your Ewire Wallet has been debited.</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h3>Recharge &amp; Pay Bills</h3></div>
      <div class="panel-body"> <div class="accordion-item">
                              <h4>Can I recharge any mobile, DTH etc?</h4>
                              <div class="content">
                                <p>At Ewire Wallet, we support all operators&#39; recharges across all circles in India so that we have available for everyone.</p>
                              </div>
                            </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>Can I pay any postpaid mobile bills through Ewire Wallet?</h4></div>
      <div class="panel-body"><div class="content">
                                <p>Absolutely! Ewire Wallet users can make their postpaid bill payments. All this convenience comes to you for a minimal convenience charge.</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>How do I know what I will get by paying a particular recharge amount?</h4></div>
      <div class="panel-body"> <div class="content">
                                <p>We only display indicative brows plans for recharges. This keeps changing frequently; therefore any updated information on recharge plans will be available only with your operator.</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>How will I get to know whether my recharge or bill payment transaction has been successful?</h4></div>
      <div class="panel-body"><div class="content">
                                <p>Once you complete your two-steps process of recharging on Ewire mobile app; you will receive the following from us: Immediate Success/Failure response on your screen Receipt in an email SMS on your mobile SMS from your operator However in case of Post Paid Bill Payment there maybe some delay in few cases due to connectivity issues</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>My recharge was successful, but I have not yet received my recharge.</h4></div>
      <div class="panel-body"><div class="content">
                                <p>At Ewire Wallet, all transaction requests are processed immediately. If you do not receive any confirmation SMS from your mobile operator, then kindly contact us on Support at Ewire Wallet App or call at our Customer Care number by taping on the call tap mentioned in mobile app.</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>When I was doing the recharge on mobile app, I got a response that my recharge was unsuccessful. What does this mean?</h4></div>
      <div class="panel-body"> <div class="content">
                                <p>Possible reasons for such an error could be. Operatorâ€™s server was temporarily unavailable. Entered amount was not accepted by your mobile operator as a valid recharge value. Sometimes it so happens that we are unable to connect to the mobile operator/server due to a temporary network problem. We request you to try again after a while. If the problem persists, please contact us on Support at Ewire Wallet App or call at our Customer Care number by taping on the call tap mentioned in mobile app. we will be happy to help.</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>I have switched my operator through MNP. How can I now get a recharge done?</h4></div>
      <div class="panel-body"><div class="content">
                                <p>You can recharge your mobile with your new operator by following these simple steps on Ewire Mobile App.</p>
                                <p><strong>Step 1:</strong> Enter the mobile number.</p>
                                <p><strong>Step 2:</strong> Select your current operator (if it&#39;s showing your previous operator).</p>
                                <p><strong>Step 3:</strong> Select your state</p>
                                <p><strong>Step 4:</strong> Enter recharge value.</p>
                                <p><strong>Step 5:</strong> Select your payment method (Ewire Wallet, credit card, debit card).</p>
                              </div></div>
    </div>
    <h3>Payment</h3>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>I do not have a credit card. How can I make a payment on Ewire Wallet?</h4></div>
      <div class="panel-body"><div class="content">
                                <p>You can choose any of the following options to make a payment on Ewire Wallet: Ewire Wallet Balance ( if your Wallet has been loaded with money) NEFT Debit cards (MasterCard/ Visa cards) Internet banking. We recommend you to load money into your Ewire Wallet for a simpler, safer and faster transaction on Ewire Wallet</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"> <h4>How do I pay for my recharge on Ewire Wallet?</h4></div>
      <div class="panel-body"><div class="content">
                                <p>We have many payment options like: Ewire Wallet Balance (in case your Wallet is loaded with money) NEFT Debit cards (MasterCard/Visa cards) Credit cards (MasterCard/Visa cards) Internet Banking</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>What is an MPIN (Mobile Personal Identification Number)?</h4></div>
      <div class="panel-body"><div class="content">
                                <p>You need to go to profile page and need to set 4 numerical digits MPIN for e.g. 8574 which is difficult to guess for others but easy for you to remember. Your MPIN will work as a security password just like an ATM pin, when you want to open Ewire app you will have to provide your MPIN to open the app.</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>What is an OTP (One Time Password)?</h4></div>
      <div class="panel-body"> <div class="content">
                                <p>One Time Password (OTP) has been introduced as an additional security feature and has been made mandatory by the Reserve Bank of India (RBI). OTP is intended to reduce the possibility of fraudulent money transfer transactions and to safeguard the customer.</p>
                              </div></div>
    </div>
    <h3>Refund</h3>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>How do I initiate a refund?</h4></div>
      <div class="panel-body"> <div class="content">
                                <p>Whenever you face such a situation, where you have made the payment but have not received the recharge, just write to us on Support at Ewire Wallet website or call at our Customer Care number by taping on the call tap mentioned in mobile app. We will try to resolve it in latest possible time. No refund will be made in case of service fulfillment.</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>How long does it take to get a refund?</h4></div>
      <div class="panel-body"><div class="content">
                                <p>We initiate your request as soon as you inform us with the order number. In case of transactions through Ewire Wallet refund happens instantaneously. Otherwise refunds can take up to 3-6 working days from the date of your transaction as entities like banks, credit card companies have varied periods of time in which they process the refund. However, if you still have some queries feel free to write to us on Support at Ewire Wallet website or call at our Customer Care number by taping on the call tap mentioned in mobile app.</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>I entered a wrong number. How can I get my money back?</h4></div>
      <div class="panel-body"> <div class="content">
                                <p>If the operator has already processed the recharge or bill payment, the transaction will be considered as successful for the number you entered. We regret to inform that there isn&#39;t a way to get your money back.</p>
                              </div></div>
    </div>
    <h3>Security</h3>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>Is Ewire Wallet safe and secure?</h4></div>
      <div class="panel-body"> <div class="content">
                                <p>There are multiple layers of security implemented to protect your Ewire Wallet from any unauthorized access. Whenever you want to open Ewire app, you need to put a Personal Identification Number (MPIN) in your Ewire mobile application for authorization as an extra level of security. You are sent alerts on SMS and/or Email so that you are up to date on all activities occurring through your Ewire Wallet.</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>Will I be informed when my Ewire Wallet gets credited or debited?</h4></div>
      <div class="panel-body"> <div class="content">
                                <p>Yes. A confirmation SMS and/or an email notification (if email is registered with us) will be sent whenever a transaction is processed from your Wallet.</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>What if I lose my mobile phone?</h4></div>
      <div class="panel-body"> <div class="content">
                                <p>Even if you lose your mobile phone, no one will be able to access your Ewire Wallet without knowing your personal Ewire Wallet password. Since the password is not stored on the phone, your money is safe. However do keep us informed by calling our Customer care executive, so that we can block your Wallet as further safeguard.</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>What if my registered email ID is hacked?</h4></div>
      <div class="panel-body"> <div class="content">
                                <p>Your E-mail ID is just used as a reference for sending receipts / Ewire Wallet transaction summary. So in case your E-mail ID is hacked, there is little or no chance of your Ewire Wallet being misused.</p>
                              </div></div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading"><h4>Who is liable for any misuse of my Ewire Wallet if I lose my Password?</h4></div>
      <div class="panel-body"><div class="content">
                                <p>It is always advised to keep your password confidential and in case your account is accessed by any unauthorized person due to loss of password, Ewire team will not be liable for any misuse for your Ewire Wallet. For more details, please refer to our T&amp;C page.</p>
                              </div></div>
    </div>
    

  </div>
			
		</div>

		<!-- Download App from store -->
		<div class="download_app">
			<div class="app_heading text-center">
				<span>In the palm of your hands</span>
			</div>
			<div class="app_subhead text-center">
				<span>Download our App Now</span>
			</div>
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					<div class="col-sm-6 col-xs-6">
						<div>
							<a href="https://play.google.com/store/apps/details?id=in.MadMoveGlobal.CashierCard" target="_blank"><img src="${pageContext.request.contextPath}/resources/assets/img/landing/playS.png" class="img-responsive store-img right-align"></a>
						</div>
					</div>
					<div class="col-sm-6 col-xs-6">
						<div>
							<a href="https://itunes.apple.com/in/app/cashier-prepaid-cards/id1374882309?mt=8" target="_blank"><img src="${pageContext.request.contextPath}/resources/assets/img/landing/appS.png" class="img-responsive store-img left-align"></a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- footer starts here -->
		<div class="footer">
			<div class="row">
				<div class="col-sm-6 col-xs-12" id="terms_link">
					<span><a href="${pageContext.request.contextPath}/PrivacyPolicy">privacy policy</a> | <a href="${pageContext.request.contextPath}/TermsnConditions">terms &amp; conditions</a> | <a href="${pageContext.request.contextPath}/faq">faq<small>s</small></a></span>
				</div>
				<div class="col-sm-6 col-xs-12 text-right" id="copyR">
					<div class="copyright">
						<span><script type="text/javascript">document.write(new Date().getFullYear());</script> &copy; copyright EWire.</span>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>




<!-- scripts starts here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets/bootstrap/js/bootstrap.min.js"></script>
<!-- slick slider -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets/slick/slick.min.js"></script>

<!-- custom js for dashboard -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets/js/dashboard.js"></script>

</body>

</html>