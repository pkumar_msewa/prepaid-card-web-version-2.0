<div class="left side-menu">
	<button type="button"
		class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
		<i class="mdi mdi-close"></i>
	</button>

	<!-- LOGO -->
	<div class="topbar-left">
		<div class="text-center">
			<a href="${pageContext.request.contextPath}/Group/Home" class="logo">
				<img
				src="${pageContext.request.contextPath}/resources/assets-group/images/white.png"
				alt="" class="logo-large">
			</a>
		</div>
	</div>

	<div class="sidebar-inner slimscrollleft" id="sidebar-main">
		<div id="sidebar-menu">
			<ul>
				<li><a href="${pageContext.request.contextPath}/Group/Home"
					class="waves-effect"> <i class="mdi mdi-view-dashboard"></i> <span>
							Dashboard <span
							class="badge badge-pill badge-primary float-right"></span>
					</span>
				</a></li>

				<li class="menu-title">Components</li>

				<li class="has_sub" id="groupServiceId"><a
					href="javascript:void(0);" class="waves-effect"> <i
						class="mdi mdi-animation"></i> <span>Group Services</span> <span
						class="float-right"> <i class="mdi mdi-chevron-right"></i>
					</span>
				</a>
					<ul class="list-unstyled">
						<li id="GSRL"><a
							href="${pageContext.request.contextPath}/Group/GroupRequestList">Group
								Request List</a></li>
						<li id="GSRL"><a
							href="${pageContext.request.contextPath}/Group/ChangeRequestList"
							title="Group Change Request List"
							style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">Group
								Change Request List</a></li>
						<li id="GSAL"><a
							href="${pageContext.request.contextPath}/Group/AcceptedUserGroup">Approved
								User List</a></li>
						<li id="GSRJ"><a
							href="${pageContext.request.contextPath}/Group/RejectedUserGroup">Rejected
								User List</a></li>
						<li id="GBRF"><a
							href="${pageContext.request.contextPath}/Group/GrBulkRegFailList"
							title="Group Bulk Register Fail List"
							style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">Bulk
								Register Fail List</a></li>
						<li id="GBCA"><a
							href="${pageContext.request.contextPath}/Group/GrBulkCarFailList"
							title="Group Bulk Register Fail List"
							style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">Bulk
								Card Fail List</a></li>
						<li id="BKY"><a
							href="${pageContext.request.contextPath}/Group/GrBulkKycFailList"
							title="Group Bulk Kyc Fail List"
							style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">Bulk
								Kyc Fail List</a></li>
					</ul></li>

				<li id="GBLR"><a
					href="${pageContext.request.contextPath}/Group/BulkRegister"
					class="waves-effect"> <i class="mdi mdi-table"></i> <span>Group
							Bulk Register <span
							class="badge badge-pill badge-primary float-right"></span>
					</span>
				</a></li>

				<li id="GBCA"><a
					href="${pageContext.request.contextPath}/Group/BulkCardAssignment"
					class="waves-effect" title="Group Bulk Card Assignment"
					style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
						<i class="mdi mdi-table"></i> <span>Bulk Card Assignment <span
							class="badge badge-pill badge-primary float-right"></span>
					</span>
				</a></li>

				<li id="BKY"><a
					href="${pageContext.request.contextPath}/Group/GroupBulkKYCApproval"
					class="waves-effect"> <i class="mdi mdi-dictionary"></i> <span>Group
							Bulk KYC <span class="badge badge-pill badge-primary float-right"></span>
					</span>
				</a></li>

				<li class="has_sub" id="BSMS"><a href="javascript:void(0);"
					class="waves-effect"> <i class="mdi mdi-cards"></i> <span>Bulk
							SMS</span> <span class="float-right"> <i
							class="mdi mdi-chevron-right"></i>
					</span>
				</a>
					<ul class="list-unstyled">
						<li><a
							href="${pageContext.request.contextPath}/Group/BulkSMS">Bulk
								SMS</a></li>
						<li><a
							href="${pageContext.request.contextPath}/Group/BulkUploadSmsList">Bulk
								SMS History</a></li>
						<li><a
							href="${pageContext.request.contextPath}/Group/SendGroupSms">Send
								SMS</a></li>
					</ul></li>

				<li id="NOTC"><a
					href="${pageContext.request.contextPath}/Group/SendNotification"
					class="waves-effect"> <i class="mdi mdi-border-outside"></i> <span>Push
							Notification <span
							class="badge badge-pill badge-primary float-right"></span>
					</span>
				</a></li>

				<%-- <li class="has_sub" id="NOTC"><a href="javascript:void(0);"
					class="waves-effect"> <i class="mdi mdi-border-outside"></i> <span>Push
							Notification</span> <span class="float-right"> <i
							class="mdi mdi-chevron-right"></i>
					</span>
				</a>
					<ul class="list-unstyled">
						<li><a
							href="${pageContext.request.contextPath}/Group/SendNotification">Push
								Notification</a></li>
					</ul></li> --%>

				<%--  <li id="GBRF">
                                <a href="${pageContext.request.contextPath}/Group/GrBulkRegFailList" class="waves-effect" title="Group Bulk Register Fail List" style="white-space: nowrap;
   									overflow: hidden; text-overflow: ellipsis;">
                                     <i class="mdi mdi-table"></i>
                                    <span >Group Register Fail List
                               <li id="GBCA">
                                <a href="${pageContext.request.contextPath}/Group/BulkCardAssignment" class="waves-effect">
                                     <i class="mdi mdi-table"></i>
                                    <span>Bulk Card Assignment
                                        <span class="badge badge-pill badge-primary float-right"></span>
                                    </span>
                                </a>result
                            </li>
                             --%>


				<!-- Corporate Panel -->

				<%-- <li id="BLR"><a
					href="${pageContext.request.contextPath}/Group/CorporateBulkRegister"
					class="waves-effect"> <i class="ti-pencil-alt menu-icon"></i> <span>
							Corp. Bulk Register <span
							class="badge badge-pill badge-primary float-right"></span>
					</span>
				</a></li> --%>

				<%-- <li id="BCL"><a
					href="${pageContext.request.contextPath}/Group/CorporateBulkCardLoad"
					class="waves-effect" title="Corp. Bulk Card Load"
					style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
						<i class="mdi mdi-shape-rectangle-plus"></i> <span> Corp.
							Bulk Card Load <span
							class="badge badge-pill badge-primary float-right"></span>
					</span>
				</a></li> --%>

				<%-- <li id="CSCL"><a
					href="${pageContext.request.contextPath}/Group/CorporateSinleCardLoad"
					class="waves-effect" title="Corp. Single Card Load"
					style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
						<i class="ti-credit-card menu-icon"></i> <span> Corp.
							Single Card Load <span
							class="badge badge-pill badge-primary float-right"></span>
					</span>
				</a></li> --%>

				<%-- <li id="CSCA"><a
					href="${pageContext.request.contextPath}/Group/CorporateSingleCardAssignment"
					class="waves-effect" title="Corp. Single Card Assignment"
					style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
						<i class="ti-check-box menu-icon"></i> <span> Corp. Single
							Card Assignment <span
							class="badge badge-pill badge-primary float-right"></span>
					</span>
				</a></li> --%>

				<%-- <li class="has_sub" id="CRD"><a href="javascript:void(0);"
					class="waves-effect"> <i class="mdi mdi-folder-multiple"></i> <span>Corporate
							Reports</span> <span class="float-right"> <i
							class="mdi mdi-chevron-right"></i>
					</span>
				</a>
					<ul class="list-unstyled">
						<li id="CUR"><a
							href="${pageContext.request.contextPath}/Group/UserReport">User
								report</a></li>
						 <li id="CPH">
                                        <a href="${pageContext.request.contextPath}/Group/PrefundHistory">Prefund History</a>
                                    </li>  
						<li id="CFR"><a
							href="${pageContext.request.contextPath}/Group/FailedRegistration">Failed
								Registration</a></li>
						<li id="CBLR"><a
							href="${pageContext.request.contextPath}/Group/BulkLoadReport">Bulk
								Card Load</a></li>
						<li id="CFLL"><a
							href="${pageContext.request.contextPath}/Group/FailedBulkLoad">Failed
								Load</a></li>
						<li id="CBCL"><a
							href="${pageContext.request.contextPath}/Group/BlockedCards">Blocked
								Cards</a></li>
					</ul></li> --%>

				<%--   <li id="CPR">
                                <a href="${pageContext.request.contextPath}/Group/Prefund" class="waves-effect" title="Corp. Prefund Request" style="white-space: nowrap;
   									overflow: hidden; text-overflow: ellipsis;">
                                   <i class="ti-check-box menu-icon"></i>
                                    <span> Corp. Prefund Request
                                        <span class="badge badge-pill badge-primary float-right"></span>
                                    </span>
                                </a>
                            </li> --%>

				<%-- <li class="has_sub" id="corppart"><a href="javascript:void(0);"
					class="waves-effect"> <i class="mdi mdi-collage"></i> <span>Corporate
							Partner</span> <span class="float-right"> <i
							class="mdi mdi-chevron-right"></i>
					</span>
				</a>
					<ul class="list-unstyled">
						<li id="CAP"><a
							href="${pageContext.request.contextPath}/Group/AddCorporatePartner">Add
								Partner</a></li>
						<li id="CLP"><a
							href="${pageContext.request.contextPath}/Group/ListCorporatePartner">List
								Partner</a></li>
					</ul></li> --%>

			</ul>
		</div>
		<div class="clearfix"></div>
	</div>
</div>