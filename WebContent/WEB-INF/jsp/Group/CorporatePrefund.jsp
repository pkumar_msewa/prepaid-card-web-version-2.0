<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Ewire</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Mannatthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/assets-group/images/favicon.png">

        <!-- CSS -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/plugins/animate/animate.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/bootstrap-material-design.min.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/icons.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/style.css" type="text/css">
        <%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-group/css/custom.css"> --%>
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/corporate/assets/dropify/css/dropify.min.css">
    
    
      <!-- plugins:css -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/puse-icons-feather/feather.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.addons.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/font-awesome/css/font-awesome.min.css" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/simple-line-icon/css/simple-line-icons.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/ti-icons/css/themify-icons.css">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/group/style.css">
  <!-- endinject -->
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    
     <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.min.js"></script>
    <script>var contextPath = "${pageContext.request.contextPath}";</script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/GassignService.js"></script>
    
    <style type="text/css">
.tgl {
              position: relative;
              display: inline-block;
              height: 30px;
              cursor: pointer;
            }
    .tgl > input {
              position: absolute;
              opacity: 0;
              z-index: -1;
              /* Put the input behind the label so it doesn't overlay text */
              visibility: hidden;
            }
            .tgl .tgl_body {
              width: 60px;
              height: 30px;
              background: white;
              border: 1px solid #dadde1;
              display: inline-block;
              position: relative;
              border-radius: 50px;
            }
            .tgl .tgl_switch {
              width: 30px;
              height: 30px;
              display: inline-block;
              background-color: white;
              position: absolute;
              left: -1px;
              top: -1px;
              border-radius: 50%;
              border: 1px solid #ccd0d6;
              -moz-box-shadow: 0 2px 2px rgba(0, 0, 0, 0.13);
              -webkit-box-shadow: 0 2px 2px rgba(0, 0, 0, 0.13);
              box-shadow: 0 2px 2px rgba(0, 0, 0, 0.13);
              -moz-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), -moz-transform 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              -o-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), -o-transform 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              -webkit-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), -webkit-transform 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), transform 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              z-index: 1;
            }
            .tgl .tgl_track {
              position: absolute;
              left: 0;
              top: 0;
              right: 0;
              bottom: 0;
              overflow: hidden;
              border-radius: 50px;
            }
            .tgl .tgl_bgd {
              position: absolute;
              right: -10px;
              top: 0;
              bottom: 0;
              width: 55px;
              -moz-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), right 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              -o-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), right 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              -webkit-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), right 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), right 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              background: #439fd8 url("http://petelada.com/images/toggle/tgl_check.png") center center no-repeat;
            }
            .tgl .tgl_bgd-negative {
              right: auto;
              left: -45px;
              background: white url("http://petelada.com/images/toggle/tgl_x.png") center center no-repeat;
            }
            .tgl:hover .tgl_switch {
              border-color: #b5bbc3;
              -moz-transform: scale(1.06);
              -ms-transform: scale(1.06);
              -webkit-transform: scale(1.06);
              transform: scale(1.06);
            }
            .tgl:active .tgl_switch {
              -moz-transform: scale(0.95);
              -ms-transform: scale(0.95);
              -webkit-transform: scale(0.95);
              transform: scale(0.95);
            }
            .tgl > :not(:checked) ~ .tgl_body > .tgl_switch {
              left: 30px;
            }
            .tgl > :not(:checked) ~ .tgl_body .tgl_bgd {
              right: -45px;
            }
            .tgl > :not(:checked) ~ .tgl_body .tgl_bgd.tgl_bgd-negative {
              right: auto;
              left: -10px;
            }        
</style>
    
    </head>


    <body class="fixed-left">

        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner"></div>
            </div>
        </div>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <jsp:include page="/WEB-INF/jsp/Group/LeftMenu.jsp" />
            <!-- Left Sidebar End -->

            <!-- Start right Content here -->

            <div class="content-page">
                <!-- Start content -->
                <div class="content">

                    <!-- Top Bar Start -->
                        <jsp:include page="/WEB-INF/jsp/Group/Header.jsp" />
                    <!-- Top Bar End -->

                    <div class="page-content-wrapper dashborad-v">

                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="page-title-box">
                                        <h4 class="page-title">Corporate Prefund Request</h4>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 col-xl-12">
                                    <div class="card m-b-30">
                                         <div class="card-body">
				                  <h4 class="card-title">Prefund Request</h4>
				                  <form action="${pageContext.request.contextPath}/Group/CorporatePrefund" method="Post" enctype="multipart/form-data">
				
				                    <div class="row">
				                      <div class="col-6">
				                        <div class="form-group">
				                          <label>Corporate Name</label>
				                          <input type="text" name="clientName" placeholder="Enter Corporate Name" onkeypress="return isAlphKey(event);" class="form-control">
				                        </div>
				                      </div>
				                      <div class="col-6">
				                        <div class="form-group">
				                          <label>Amount</label>
				                          <input type="text" name="amount" placeholder="Enter Amount" onkeypress="return isNumberKey(event);" class="form-control">
				                        </div>
				                      </div>
				                      <div class="col-6">
				                        <div class="form-group">
				                          <label>Transaction Ref No</label>
				                          <input type="text" name="transactionRefNo" placeholder="Enter Bank Ref No" class="form-control">
				                        </div>
				                      </div>
				                      <div class="col-6">
				                        <div class="form-group">
				                          <label>Upload file</label>
				                          <input type="file" name="file" placeholder="Upload" class="form-control">
				                        </div>
				                      </div>
				     
				                    </div>
				                    <div class="form-group">
				                      <center><button class="btn btn-primary" type="submit">Submit</button></center>
				                    </div>
				                  </form>
				               <center><h6 class="card-title" style="color: red;">${successMsg}</h6></center></div>
                                    </div>
                                </div> 
                            </div> 

                        </div>
                        <!-- container -->

                    </div>
                    <!-- Page content Wrapper -->
                </div>
                <!-- content -->

                <footer class="footer">
                    &copy; <script type="text/javascript">document.write(new Date().getFullYear())</script> MSS Payments Pvt. Ltd.
                </footer>

            </div>
            <!-- End Right content here -->

        </div>
        <!-- END wrapper -->


        <!-- jQuery  -->
       
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/bootstrap-material-design.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/modernizr.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.scrollTo.min.js"></script>

        <script src="${pageContext.request.contextPath}/resources/assets-group/plugins/carousel/owl.carousel.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/plugins/fullcalendar/vanillaCalendar.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/plugins/peity/jquery.peity.min.js"></script>
        <%-- <script src="${pageContext.request.contextPath}/resources/assets-group/pages/dashborad.js"></script> --%>
		<script src="${pageContext.request.contextPath}/resources/corporate/js/jquery-file-upload.js"></script>
		<script src="${pageContext.request.contextPath}/resources/corporate/assets/dropify/js/dropify.min.js"></script>
		
        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/app.js"></script>
        <%-- <script src="${pageContext.request.contextPath}/resources/assets-group/js/custom.js"></script> --%>
              
		  <script src="${pageContext.request.contextPath}/resources/corporate/js/jquery-file-upload.js"></script>
		  <script src="${pageContext.request.contextPath}/resources/corporate/js/dropify.js"></script>
		  <!-- endinject -->
		  <!-- Custom js for this page-->
		  <script src="${pageContext.request.contextPath}/resources/corporate/js/dashboard.js"></script>
		  <!-- End custom js for this page-->
		  <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
        
         
        
        <script>
            $('.datepicker').datepicker({
                uiLibrary: 'bootstrap4',
                autoClose: true
            });
    	</script>
        
        	
	<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>

    
    <script type="text/javascript">
function isAlphNumberKey(evt){
    var k = (evt.which) ? evt.which : evt.keyCode
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 190 || k == 188);
}
</script>

<script type="text/javascript">
function isAlphKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
}
</script>
        
    </body>

</html>