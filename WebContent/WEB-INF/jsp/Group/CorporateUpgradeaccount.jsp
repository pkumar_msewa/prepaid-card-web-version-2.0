<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Ewire</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Mannatthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/assets-group/images/favicon.png">

        <!-- CSS -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/plugins/animate/animate.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/bootstrap-material-design.min.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/icons.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/style.css" type="text/css">
        <%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-group/css/custom.css"> --%>
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/corporate/assets/dropify/css/dropify.min.css">
    
    
      <!-- plugins:css -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/puse-icons-feather/feather.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.addons.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/font-awesome/css/font-awesome.min.css" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/simple-line-icon/css/simple-line-icons.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/ti-icons/css/themify-icons.css">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/group/style.css">
  <!-- endinject -->
    
     <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.min.js"></script>
    <script>var contextPath = "${pageContext.request.contextPath}";</script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/GassignService.js"></script>
    
    
    </head>


    <body class="fixed-left">

        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner"></div>
            </div>
        </div>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <jsp:include page="/WEB-INF/jsp/Group/LeftMenu.jsp" />
            <!-- Left Sidebar End -->

            <!-- Start right Content here -->

            <div class="content-page">
                <!-- Start content -->
                <div class="content">

                    <!-- Top Bar Start -->
                        <jsp:include page="/WEB-INF/jsp/Group/Header.jsp" />
                    <!-- Top Bar End -->

                    <div class="page-content-wrapper dashborad-v">

                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="page-title-box">
                                        <h4 class="page-title">Corporate Bulk KYC Upload</h4>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 col-xl-12">
                                    <div class="card m-b-30">
                                    	<div class="card-body">
					                  <h4 class="card-title">UPGRADE ACCOUNT</h4>
					                  <form action="${pageContext.request.contextPath}/Group/UpgradeAccount" method="Post" enctype="multipart/form-data">
					
					                    <div class="row">
					                      <div class="col-6">
					                      <div class="form-group">
							              <label for="">Id Type</label>
							              <select class="form-control" id="idType" name="idType">
							              	<option value="">Select Id Type</option>
							                <option value="drivers_id">Driving License(DL)</option>
							                <option value="passport">Passport</option>
							                <option value="aadhaar">Aadhar Card</option>
							              </select>
							              </div>
					                      </div>
					                      <div class="col-6">
					                          <div class="form-group">
							              <label for="" class="sr-on/ly">Id Number</label>
							              <input type="text" id="idNumber" name="idNumber"class="form-control" placeholder="eg.1288 8273 8123 1231" maxlength="15">
							              <p id="error_idNumber" style="color:red "></p>
							            </div>
							             </div>
							              <div class="col-6">
							              <div class="form-group">
							              <label for="" class="sr-on/ly">Upload Id(Front)</label>
							              <input type="file" name="image"  id="aadhar_Image" class="form-control">
							              <p id="error_aaadharCardImg" style="color:red "></p>
							            </div>
							                      </div>
							                      <div class="form-group">
							              <label for="" class="sr-on/ly">Upload Id(Back)</label>
							              <input type="file" name="image2"  id="aadhar_Image2" class="form-control">
							              <p id="error_aaadharCardImg2" style="color:red "></p>
							            </div>
					                      <div class="col-6">
					                        <div class="form-group">
					                    <label 
					                              for="address1">Address1</label>
					                    <div>
					                        <input type="text" class="form-control" 
					                        id="address1" placeholder="Address1" name="address1"/>
					                    </div>
					                  </div>
					                      </div>
					                        <div class="col-6">
					                        <div class="form-group">
					                    <label 
					                              for="dob">DOB</label>
					                    <div>
					                        <input type="text" class="form-control" 
					                        id="dob" placeholder="DOB" name="dob"/>
					                    </div>
					                  </div>
					                      </div>
					     			<input type="hidden" name="username" value="${userc}"/>
					     			<div class="col-6">
					                       
					                        <div class="form-group">
					                    <label 
					                              for="address2">Address2</label>
					                    <div>
					                        <input type="text" class="form-control" 
					                        id="address2" placeholder="Address2" name="address2"/>
					                    </div>
					                  </div>
					                      </div>
					                      <div class="col-6">
					                      <div class="form-group">
					                    <label
					                              for="city">City</label>
					                    <div>
					                        <input type="text" class="form-control" 
					                        id="city" placeholder="City" name="city"/>
					                    </div>
					                  </div>
					                      </div>
					                      <div class="col-6">
					                       <div class="form-group">
					                    <label
					                              for="state">State</label>
					                    <div>
					                        <input type="text" class="form-control" 
					                        id="state" placeholder="State" name="state"/>
					                    </div>
					                  </div>
					                      </div>
					                      <div class="col-6">
					                       <div class="form-group">
					                    <label
					                              for="country">Country</label>
					                    <div>
					                        <input type="text" class="form-control" 
					                        id="country" placeholder="Country" name="Country"/>
					                    </div>
					                  </div>
					                      </div>
					                      <div class="col-6">
					                       <div class="form-group">
					                    <label
					                              for="pincode">Pincode</label>
					                    <div>
					                        <input type="text" class="form-control" 
					                        id="pincode" placeholder="Pincode" name="pincode"/>
					                    </div>
					                  </div>
					                      </div>
					                     
					                    </div>
					                    <div class="form-group">
					                      <center><button class="btn btn-primary" type="submit">Submit</button></center>
					                    </div>
					                  </form>
					               <center><h6 class="card-title" style="color: red;">${message}</h6></center></div>
                                    </div>
                                </div> 
                            </div> 

                        </div>
                        <!-- container -->

                    </div>
                    <!-- Page content Wrapper -->
                </div>
                <!-- content -->

                <footer class="footer">
                    &copy; <script type="text/javascript">document.write(new Date().getFullYear())</script> MSS Payments Pvt. Ltd.
                </footer>

            </div>
            <!-- End Right content here -->

        </div>
        <!-- END wrapper -->


        <!-- jQuery  -->
       
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/bootstrap-material-design.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/modernizr.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.scrollTo.min.js"></script>

        <script src="${pageContext.request.contextPath}/resources/assets-group/plugins/carousel/owl.carousel.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/plugins/fullcalendar/vanillaCalendar.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/plugins/peity/jquery.peity.min.js"></script>
        <%-- <script src="${pageContext.request.contextPath}/resources/assets-group/pages/dashborad.js"></script> --%>
		<script src="${pageContext.request.contextPath}/resources/corporate/js/jquery-file-upload.js"></script>
		<script src="${pageContext.request.contextPath}/resources/corporate/assets/dropify/js/dropify.min.js"></script>
		
        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/app.js"></script>
        <%-- <script src="${pageContext.request.contextPath}/resources/assets-group/js/custom.js"></script> --%>
              
		  <script src="${pageContext.request.contextPath}/resources/corporate/js/jquery-file-upload.js"></script>
		  <script src="${pageContext.request.contextPath}/resources/corporate/js/dropify.js"></script>
		  <!-- endinject -->
		  <!-- Custom js for this page-->
		  <script src="${pageContext.request.contextPath}/resources/corporate/js/dashboard.js"></script>
		  <!-- End custom js for this page-->
        
        
        
        <script type="text/javascript">
    	$(document).ready(function(){

        	// demo.initChartist();
        	 $(".dropify").change(function() {
                 var filename = readURL(this);
                 $(this).parent().children('span').html(filename);
               });
 
               // Read File and return value  
               function readURL(input) {
                 var url = input.value;
                 var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
                 console.log(ext);
                 if (input.files && input.files[0] && (
                   ext == "csv"
                   )) {
                   var path = $(input).val();
                   var filename = path.replace(/^.*\\/, "");
                   // $('.fileUpload span').html('Uploaded Proof : ' + filename);
                  // document.getElementById('uploadDoc').enabled = 'enabled';
                  $('button:submit').attr('disabled',false); 
                   
                   return "Uploaded file : "+filename;
                 } else {
                	  document.getElementById('uploadDoc').disabled = 'disabled';
                   $(input).val("");
                   return "Only csv format are allowed!";
                 }
               }


        

    	});
	</script>
        
    </body>

</html>