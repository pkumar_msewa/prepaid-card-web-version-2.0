<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Ewire</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Mannatthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/assets-group/images/favicon.png">

        <!-- CSS -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/plugins/animate/animate.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/bootstrap-material-design.min.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/icons.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/style.css" type="text/css">
        <%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-group/css/custom.css"> --%>
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/corporate/assets/dropify/css/dropify.min.css">
    
    
      <!-- plugins:css -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/puse-icons-feather/feather.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.addons.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/font-awesome/css/font-awesome.min.css" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/simple-line-icon/css/simple-line-icons.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/ti-icons/css/themify-icons.css">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/group/style.css">
  <!-- endinject -->
    
     <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.min.js"></script>
    <script>var contextPath = "${pageContext.request.contextPath}";</script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/GassignService.js"></script>
    
    
    </head>


    <body class="fixed-left">

        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner"></div>
            </div>
        </div>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <jsp:include page="/WEB-INF/jsp/Group/LeftMenu.jsp" />
            <!-- Left Sidebar End -->

            <!-- Start right Content here -->

            <div class="content-page">
                <!-- Start content -->
                <div class="content">

                    <!-- Top Bar Start -->
                        <jsp:include page="/WEB-INF/jsp/Group/Header.jsp" />
                    <!-- Top Bar End -->

                    <div class="page-content-wrapper dashborad-v">

                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="page-title-box">
                                        <h4 class="page-title">Corporate Single Card Load</h4>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 col-xl-12">
                                    <div class="card m-b-30">
                                         <div class="card-body">
						                  <h4 class="card-title">Single Card Load</h4>
						                  <div style="color: green; text-align: center; ">${succMsg}</div>
						                  <div style="color: red; text-align: center; ">${errorMsg}</div>
						                  <form action="${pageContext.request.contextPath}/Group/LoadCard" id="formId"  method="post">
                                            <div class="row">
                                                <div class="col-6 offset-sm-3">
                                                    <fieldset>
                                                        <legend>User Details</legend>
                                                        <div class="form-group">
                                                            <label for="mobile">Mobile</label>
                                                            <input type="text" name="contactNo" id="contactNo" class="form-control"  onkeypress="return isNumberKey(event);" maxlength="10">
                                                        <p id="ferror" style="color: red" ></p>
                                                        </div>
                                                       
                                                        <div class="form-group">
                                                            <label for="amount">Amount</label>
                                                            <input type="text" name="amount" id="amount" class="form-control"  onkeypress="return isNumberKey(event);" maxlength="4">
                                                        <p id="amountError" style="color: red" ></p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="lname">Comment(optional)</label>
<!--                                                             <input type="text" name="lastName" id="lastName" class="form-control"  onkeypress="return isAlphKey(event);"> -->
                                                       	<textarea rows="5" cols="30" maxlength="250" name="comment" id="comment" class="form-control"></textarea>
                                                       	<p id="lastError" style="color: red" ></p>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <center><button type="button" class="btn btn-primary mt-4" onclick="validateform()" disabled="disabled">Load</button></center>
                                        </form>
                						</div>
                                    </div>
                                </div> 
                            </div> 

                        </div>
                        <!-- container -->

                    </div>
                    <!-- Page content Wrapper -->
                </div>
                <!-- content -->

                <footer class="footer">
                    &copy; <script type="text/javascript">document.write(new Date().getFullYear())</script> MSS Payments Pvt. Ltd.
                </footer>

            </div>
            <!-- End Right content here -->

        </div>
        <!-- END wrapper -->


        <!-- jQuery  -->
       
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/bootstrap-material-design.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/modernizr.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.scrollTo.min.js"></script>

        <script src="${pageContext.request.contextPath}/resources/assets-group/plugins/carousel/owl.carousel.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/plugins/fullcalendar/vanillaCalendar.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/plugins/peity/jquery.peity.min.js"></script>
        <%-- <script src="${pageContext.request.contextPath}/resources/assets-group/pages/dashborad.js"></script> --%>
		<script src="${pageContext.request.contextPath}/resources/corporate/js/jquery-file-upload.js"></script>
		<script src="${pageContext.request.contextPath}/resources/corporate/assets/dropify/js/dropify.min.js"></script>
		
        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/app.js"></script>
        <%-- <script src="${pageContext.request.contextPath}/resources/assets-group/js/custom.js"></script> --%>
        
        
  <script src="${pageContext.request.contextPath}/resources/corporate/js/jquery-file-upload.js"></script>
  <script src="${pageContext.request.contextPath}/resources/corporate/js/dropify.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="${pageContext.request.contextPath}/resources/corporate/js/dashboard.js"></script>
  <!-- End custom js for this page-->
        
        
        
        <script type="text/javascript">
    	$(document).ready(function(){

        	// demo.initChartist();
        	 $(".dropify").change(function() {
                 var filename = readURL(this);
                 $(this).parent().children('span').html(filename);
               });
 
               // Read File and return value  
               function readURL(input) {
                 var url = input.value;
                 var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
                 console.log(ext);
                 if (input.files && input.files[0] && (
                   ext == "csv"
                   )) {
                   var path = $(input).val();
                   var filename = path.replace(/^.*\\/, "");
                   // $('.fileUpload span').html('Uploaded Proof : ' + filename);
                  // document.getElementById('uploadDoc').enabled = 'enabled';
                  $('button:submit').attr('disabled',false); 
                   
                   return "Uploaded file : "+filename;
                 } else {
                	  document.getElementById('uploadDoc').disabled = 'disabled';
                   $(input).val("");
                   return "Only csv format are allowed!";
                 }
               }


        

    	});
	</script>
	<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>


  <script>
    	
    	function validateform(){
    	var valid = true;
    	
    	var contactNo  = $('#contactNo').val();
    	var amount = $('#amount').val() ;
    	var driver= $('#driver_id').val();
    	if(contactNo.length <=0){
    		$("#ferror").html("Please enter your contact number");
    		valid = false;
    	}
    	if(amount.length  <= 0){
    		$("#amountError").html("Please enter the amount ");
    		valid = false; 
    	}
    	if(driver.length<=0){
    		$("#derror").html("Please enter driver's id");
    	}

    if(valid == true) {
    	$("#formId").submit();
    } 
    
    var timeout = setTimeout(function(){
    	$("#ferror").html("");
    	$("#amountError").html("");
    }, 4000);
    }
    </script>
    
     
    <script type="text/javascript">
function isAlphNumberKey(evt){
    var k = (evt.which) ? evt.which : evt.keyCode
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 190 || k == 188);
}
</script>
        
    </body>

</html>