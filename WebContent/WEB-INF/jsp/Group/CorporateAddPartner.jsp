<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Ewire</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Mannatthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/assets-group/images/favicon.png">

        <!-- CSS -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/plugins/animate/animate.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/bootstrap-material-design.min.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/icons.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/style.css" type="text/css">
        <%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-group/css/custom.css"> --%>
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/corporate/assets/dropify/css/dropify.min.css">
    
    
  <!-- plugins:css -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/puse-icons-feather/feather.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.addons.css">
  <!-- endinject -->
  
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/font-awesome/css/font-awesome.min.css" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/simple-line-icon/css/simple-line-icons.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/ti-icons/css/themify-icons.css">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/group/style.css">
  <!-- endinject -->
  
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    
    <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.min.js"></script>
    <script>var contextPath = "${pageContext.request.contextPath}";</script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/GassignService.js"></script>
  
    
    </head>


    <body class="fixed-left">

        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner"></div>
            </div>
        </div>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <jsp:include page="/WEB-INF/jsp/Group/LeftMenu.jsp" />
            <!-- Left Sidebar End -->

            <!-- Start right Content here -->

            <div class="content-page">
                <!-- Start content -->
                <div class="content">

                    <!-- Top Bar Start -->
                        <jsp:include page="/WEB-INF/jsp/Group/Header.jsp" />
                    <!-- Top Bar End -->

                    <div class="page-content-wrapper dashborad-v">

                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="page-title-box">
                                        <h4 class="page-title">Corporate Add Partner</h4>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 col-xl-12">
                                    <div class="card m-b-30">
                                         <div class="card-body">
                  							<h4 class="card-title">Add Partner</h4>
                   							<div style="color: green; text-align: center; ">${message}</div>
                 	
                   							<form action="${pageContext.request.contextPath}/Group/AddCorporatePartner" id="formId"  method="post">
                                            <fieldset>
                                            	<legend>Partner Details</legend>
                                            	<div class="row">
                                                <div class="col-6">
                                                	<div class="form-group">
                                                            <label for="fname">Partner Name</label>
                                                            <input type="text" name="partnerName" id="firstName" class="form-control"  onkeypress="return isAlphKey(event);">
                                                        	<p id="firstError" style="color: red" ></p>
                                                        </div>
                                                </div>
                                                        
                                                        <div class="col-6">
                                                        	<div class="form-group">
	                                                            <label for="email">Email</label>
	                                                            <input type="email" name="partnerEmail" id="email" class="form-control">
	                                                        <p id="mailError" style="color: red" ></p>
	                                                        </div>
                                                        </div>
                                                        <div class="col-6">
                                                        	<div class="form-group">
	                                                            <label for="mobile">Mobile</label>
	                                                            <input type="text" name="partnerMobile" id="contactNo" maxlength="10" class="form-control"  onkeypress="return isNumberKey(event);">
	                                                        <p id="ferror" style="color: red" ></p>
	                                                        </div>
                                                        </div>
                                                        <div class="col-6">
                                                        	<div class="form-group">
	                                                            <label for="mobile">Load Card Max Limit</label>
	                                                            <input type="text" name="loadCardMaxLimit" id="maxLimitId" maxlength="8" class="form-control"  onkeypress="return isNumberKey(event);">
	                                                        <p id="lcerror" style="color: red" ></p>
	                                                        </div>
                                                        </div>
                                                        <div class="col-6">
                                                          <div class="form-group">
                                                            <label for="mobile">Multiple Select</label>
                                                            <select class="multi-select" name="services" multiple="multiple">
                                                             <c:forEach items="${serviceList}" var="services" varStatus="loopCounter">
                                                              <option value="${services.code}">${services.description}</option>
                                                             
                                                              </c:forEach>
                                                            </select>
                                                          </div>
                                                        </div>
                                                        
                                            </div>
                                            </fieldset>
                                            <center><button type="button" id="adduse" class="btn btn-primary mt-4" onclick="validateform()">Submit</button></center>
                                        </form>
                						</div>
                                    </div>
                                </div> 
                            </div> 

                        </div>
                        <!-- container -->

                    </div>
                    <!-- Page content Wrapper -->
                </div>
                <!-- content -->

                <footer class="footer">
                    &copy; <script type="text/javascript">document.write(new Date().getFullYear())</script> MSS Payments Pvt. Ltd.
                </footer>

            </div>
            <!-- End Right content here -->

        </div>
        <!-- END wrapper -->


        <!-- jQuery  -->
       
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/bootstrap-material-design.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/modernizr.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.scrollTo.min.js"></script>

        <script src="${pageContext.request.contextPath}/resources/assets-group/plugins/carousel/owl.carousel.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/plugins/fullcalendar/vanillaCalendar.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/plugins/peity/jquery.peity.min.js"></script>
        <%-- <script src="${pageContext.request.contextPath}/resources/assets-group/pages/dashborad.js"></script> --%>
		<script src="${pageContext.request.contextPath}/resources/corporate/js/jquery-file-upload.js"></script>
		<script src="${pageContext.request.contextPath}/resources/corporate/assets/dropify/js/dropify.min.js"></script>
		
        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/app.js"></script>
        <%-- <script src="${pageContext.request.contextPath}/resources/assets-group/js/custom.js"></script> --%>
        
        
  	<script src="${pageContext.request.contextPath}/resources/corporate/js/jquery-file-upload.js"></script>
  	<script src="${pageContext.request.contextPath}/resources/corporate/js/dropify.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="${pageContext.request.contextPath}/resources/corporate/js/dashboard.js"></script>
  <!-- End custom js for this page-->
  
    <script>
      $(document).ready(function() {
          $('.multi-select').select2();
      });
    </script>  
        
      
 <script>
    	function validateform(){
    	var valid = true;
    	var pattern = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum|in)";
    	var passwordPattern = "[a-zA-z0-9]"; //pattern for password
    	var firstName=$("#firstName").val();
    	var contactNo  = $('#contactNo').val();
    	var email = $('#email').val() ;
    	var maxLimit= $('#maxLimitId').val();
    	var tetee=document.getElementsByName("physicalCard");
    
    	if(firstName.length <= 0){
    	$("#firstError").html("Please enter your first name");
    	valid = false;
    	}
    	if(contactNo.length <=0){
    		$("#ferror").html("Please enter your contact no");
    		valid = false;
    	}
    	if(maxLimit.length <=0){
    		$("#lcerror").html("Please enter load card max limit");
    		valid = false;
    	}
    	if(email.length  <= 0){
    		$("#mailError").html("Please enter your email Id ");
    		valid = false; 
    	}else if(!email.match(pattern)) {
    		console.log("inside mail")
    		$("#mailError").html("Enter valid email Id");
    		valid = false;	
    	}

    if(valid == true) {
    	$("#formId").submit();
    	$("#adduse").addClass("disabled");
    } 
    
    var timeout = setTimeout(function(){
        $("#firstError").html("");
    	$("#lastError").html("");
    	$("#ferror").html("");
    	$("#mailError").html("");
    	$("#proxyNumbererror").html("");
    	$("#passError").html("");
    	$("#doberror").html("");
    	$("#lcerror").html("");
    }, 4000);
    }
    </script>
    
    
    <script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>
    
    <script type="text/javascript">
function isAlphNumberKey(evt){
    var k = (evt.which) ? evt.which : evt.keyCode
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 190 || k == 188);
}
</script>

<script type="text/javascript">
function isAlphKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
}
</script>
        
    </body>

</html>