<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Ewire</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Mannatthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/assets-group/images/favicon.png">

        <!-- CSS -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/plugins/animate/animate.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/bootstrap-material-design.min.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/icons.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/style.css" type="text/css">
       <%--  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-group/css/custom.css"> --%>

        <!-- Datatables -->
        <link href="${pageContext.request.contextPath}/resources/assets-group/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/assets-group/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
     	<!-- Responsive datatable examples -->
      	<link href="${pageContext.request.contextPath}/resources/assets-group/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
     
		
		<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
		<script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.min.js"></script>
		<script>var contextPath = "${pageContext.request.contextPath}";</script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/GassignService.js"></script>
   
    </head>


    <body class="fixed-left">

        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner"></div>
            </div>
        </div>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <jsp:include page="/WEB-INF/jsp/Group/LeftMenu.jsp" />
            <!-- Left Sidebar End -->

            <!-- Start right Content here -->

            <div class="content-page">
                <!-- Start content -->
                <div class="content">

                    <!-- Top Bar Start -->
                        <jsp:include page="/WEB-INF/jsp/Group/Header.jsp" />
                    <!-- Top Bar End -->

                    <div class="page-content-wrapper dashborad-v">

                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="page-title-box">
                                        <h4 class="page-title">List Corporate Partner</h4>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="row">
                               
                                <div class="col-md-12 col-xl-12">
                                    <div class="card m-b-30">
                                          <div class="card-body">
						                					                 
						                  
						                  <div class="tabble text-black">
						                    <table id="datatable-buttons" class="table table-striped table-bordered w-100">
						                      <thead>
						                        <tr>
						                            <th>Sl No.</th>
						                            <th>Partner Name</th>
						                            <th>email</th>
						                            <th>mobile</th>
						                            <th>Date Created</th>
						                            <th>Load Card Max Limit</th>
						                            <th>Services Assigned</th>
						                            <th>Action</th>
						                        </tr>
						                      </thead>
						                      <tbody>
												<c:forEach items="${partnerDetailsList}" var="partner"
													varStatus="loopCounter">
													<tr>
														<td>${loopCounter.count}</td>
														<td><c:out value="${partner.partnerName} " default=""
																escapeXml="true" /></td>
																<td> <a
															href="${pageContext.request.contextPath}/Group/getPartnerTransaction?partner=${partner.id}">
															${partner.partnerEmail}</a>${partner.partnerEmail}</td>
														<td><a href="#"><c:out
																	value="${partner.partnerMobile}" default=""
																	escapeXml="true" /></a></td>
														<td><c:out value="${partner.created}" default=""
																escapeXml="true" /></td>
															<td>${partner.loadCardMaxLimit}</td>	
														<input type="hidden" id="hiddenId" name="hiddenId" value="" />
														<td style="color: purple;"><a href="" data-toggle="modal" data-target="#myModalHorizontal" data-book-id="${partner.id}"> <c:forEach
																	items="${partner.partnerServices}" var="partner1" varStatus="loopCounter">
														<c:out value="${partner1.description}" default="" escapeXml="true" />
														</c:forEach></td>
														</a>
														<td><a
															href="${pageContext.request.contextPath}/Group/inactivePartner?partner=${partner.partnerUser.id}"><button
																	type="button" class="btn btn-primary">Delete</button></a></td>
													</tr>
												</c:forEach>

											</tbody>
						                    </table>
						                  </div>
						                </div>
                                    </div>
                                </div> 
                            </div> 

                        </div>
                        <!-- container -->

                    </div>
                    <!-- Page content Wrapper -->
                </div>
                <!-- content -->

                <footer class="footer">
                    &copy; <script type="text/javascript">document.write(new Date().getFullYear())</script> MSS Payments Pvt. Ltd.
                </footer>

            </div>
            <!-- End Right content here -->

        </div>
        <!-- END wrapper -->


        <!-- jQuery  -->
        
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/bootstrap-material-design.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/modernizr.min.js"></script>
<%--         <script src="${pageContext.request.contextPath}/resources/assets-group/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/fastclick.js"></script>
 --%>        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.scrollTo.min.js"></script>

        <script src="${pageContext.request.contextPath}/resources/assets-group/plugins/carousel/owl.carousel.min.js"></script>


       <!-- Datatable -->
       
        <!-- Required datatable js -->
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/jquery.dataTables.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/dataTables.buttons.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/buttons.bootstrap4.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/jszip.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/pdfmake.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/vfs_fonts.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/buttons.html5.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/buttons.print.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/buttons.colVis.min.js"></script>
        <!-- Responsive examples -->
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/dataTables.responsive.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/responsive.bootstrap4.min.js"></script>

  		<!-- Datatable init js -->
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/datatables.init.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/app.js"></script>
        
         <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        
	    <script>
		   
		    $(document).ready(function() {
		        var table = $('#example').DataTable( {
		            lengthChange: true,
		            pageLength: 100,
		           
		            buttons: [  'excel', 'csv' ]
		        } );
		     
		        table.buttons().container()
		            .appendTo( '#example_wrapper .col-md-6:eq(0)' );
		    });
	    </script>
        
           <script>
		$(function() {
		
		    var start = moment().subtract(29, 'days');
		    var end = moment();
		
		    function cb(start, end) {
		        $('#reportrange').html(start.format('MM-dd-yyyy') + ' - ' + end.format('MM-dd-yyyy'));
		    }
		
		    $('#reportrange').daterangepicker({
		        startDate: start,
		        endDate: end,
		        locale: {
		        	format: 'YYYY-MM-DD'
		        },
		        dateLimit: {
		            "days": 30
		        },
		        ranges: {
		           'Today': [moment(), moment()],
		           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		           'This Month': [moment().startOf('month'), moment().endOf('month')],
		           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		        }
		    }, cb);
		
		    cb(start, end);
		    
		});
		</script>
		
		
<!-- Modal -->
<div class="modal fade" id="myModalHorizontal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Services</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="form">
             <div class="">
             	<div class="form-group">
                	<select class="multi-select" name="services" multiple="multiple" id="services">
	                    <c:forEach items="${serviceExcluded}" var="services" varStatus="loopCounter">
	                    	<option value="${services.code}" selected="selected">${services.description}</option>
	                    </c:forEach>
	                    <c:forEach items="${serviceExcluded}" var="operators" varStatus="loopCounter">
	                    	<option value="${operators.code}">${operators.description}</option>
	                    </c:forEach>
                    </select>
                 </div>
              </div>
         </form>
      </div>
      <div class="modal-footer">
       <!--  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
        <button type="button" class="btn btn-primary" id="action">Update Services</button>
      </div>
    </div>
  </div>
</div>
		
		
		
		 <script>
      $(document).ready(function() {
          $('.multi-select').select2();
          var mesg= '${mesg}';
		  $('#errorMesg').html(mesg);   
		  
						setTimeout(function() {
								$('#errorMesg').html('');
							}, 3000);
						});
					</script>
    
    <script type="text/javascript">
    var agentId;
    $('#myModalHorizontal').on('show.bs.modal', function(e) {
    	console.log("i am here");
        var bookId = $(e.relatedTarget).data('book-id');
        console.log("this is the book Id:"+bookId);
        $(e.currentTarget).find('input[name="hiddenId"]').val(bookId);
        agentId=bookId;
    });
    $("#action").click(function(){
    	var servicesList=$("#services").val();
    	var realvalues = new Array();//storing the selected values inside an array
        $('#services :selected').each(function(i, selected) {
            realvalues[i] = $(selected).val();
        });
    	console.log("ServiceList:::"+servicesList);
    	console.log("realValues:::"+realvalues);
    	console.log("agent:::"+agentId);
    	$.ajax({
    		type:"POST",
    		url:"${pageContext.request.contextPath}/Group/EditPartnerServices",
    		dataType:"json",
    		data:{'id':agentId,'services':""+servicesList+""},
    		
    		success:function(data){
				console.log(data.code);
		        setTimeout("$('#myModalHorizontal').modal('hide');",30000);

				window.location.href="${pageContext.request.contextPath}/Group/ListCorporatePartner";
			},
		});
    });

    </script>
		
	

<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>

    </body>

</html>