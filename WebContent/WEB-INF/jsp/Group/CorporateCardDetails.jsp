<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Ewire</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Mannatthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/assets-group/images/favicon.png">

        <!-- CSS -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/plugins/animate/animate.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/bootstrap-material-design.min.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/icons.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/style.css" type="text/css">
       <%--  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-group/css/custom.css"> --%>

        <!-- Datatables -->
		<link href="${pageContext.request.contextPath}/resources/assets-group/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/assets-group/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
     	<!-- Responsive datatable examples -->
      	<link href="${pageContext.request.contextPath}/resources/assets-group/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
      			
		<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
		<script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.min.js"></script>
		<script>var contextPath = "${pageContext.request.contextPath}";</script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/GassignService.js"></script>
   
    </head>


    <body class="fixed-left">

        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner"></div>
            </div>
        </div>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <jsp:include page="/WEB-INF/jsp/Group/LeftMenu.jsp" />
            <!-- Left Sidebar End -->

            <!-- Start right Content here -->

            <div class="content-page">
                <!-- Start content -->
                <div class="content">

                    <!-- Top Bar Start -->
                        <jsp:include page="/WEB-INF/jsp/Group/Header.jsp" />
                    <!-- Top Bar End -->

                    <div class="page-content-wrapper dashborad-v">

                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="page-title-box">
                                        <h4 class="page-title">Corporate User Report</h4>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                          <div class="row mb-3">
            <div class="col-md-12">
              <div class="card text-black">
                <div class="card-body">
                  <!-- <h4 class="card-title">Amount Prefunded</h4> -->
                  <!-- <canvas id="lineChart" style="height:250px"></canvas> -->
                  <div class="row">
                    <div class="col">
                      <div class="avail-bal">
                        <span class="upper">Available Balance :</span>&nbsp;<span class="fa fa-inr"></span>&nbsp;<span>${Ubalance}</span>
                      </div>
                    </div>
                    <div class="col">
                      <div class="total-cred">
                        <span class="upper">Total Debit :</span>&nbsp;<span class="fa fa-inr"></span>&nbsp;<span>NA</span>
                      </div>
                    </div>
                    <div class="col">
                      <div class="total-deb">
                        <span class="upper">Total Credit :</span>&nbsp;<span class="fa fa-inr"></span>&nbsp;<span>NA</span>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <!-- <h4 class="card-title">Recent Transaction</h4> -->
                  <div class="row">
                    <div class="col-sm-5">
                    <div class="card-wrp">
                      <img src="${pageContext.request.contextPath}/resources/corporate/images/card.png" class="img-fluid">
                      <div class="card-txt">
                        <div class="cvv">
                          <span>CVV</span>&nbsp;<span>${cardDetail.cvv}</span>
                        </div>
                        <div class="card-num">
                          <span>${cardDetail.walletNumber}</span>
                        </div>
                        <div class="expDt">
                          <span>Valid Thru</span><br><span>${cardDetail.expiryDate}</span>
                        </div>
                        <div class="holdNme">
                          <span>${cardDetail.holderName}</span>
                        </div>
                      </div>
                      <div class="usrBlock text-center mt-3">
                      <c:choose>
                                                                 <c:when test="${cardstatus == 'false'}">
                        <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#myModal">BLOCK</button>
                        </c:when>
                                                                <c:otherwise>
                                                                
                        <button class="btn btn-success" type="button" id="myUnblock"  onclick="unblk()">UNBLOCK</button>
                        </c:otherwise>
                        </c:choose>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-7">
                    <div class="wrapper d-flex align-items-center py-2 mb-4 table-responsive">
                      <table id="one_card" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                         <thead>
                                            <tr>
                                                <th>Sl No</th>
                                                <th>Amount</th>
                                                <th>TransactionType</th>
                                                <th>Description</th>
                                                <th>Date</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                       <tbody>
                                        <c:forEach items="${transactions}" var="card"
									   varStatus="loopCounter">
								<tr>
									<td>${loopCounter.count}</td>
									<td> <c:out value="${card.amount}" default="" escapeXml="true" /></td>
										<td> <c:out value="${card.transactionType}" default="" escapeXml="true" /></td>
										<td><c:out value="${card.description}" default="" escapeXml="true" /></td>
										<td> <c:out value="${card.date}" default="" escapeXml="true" />
									<td> <c:out value="${card.status}" default="" escapeXml="true" /></td>
								</tr>
							</c:forEach>
                                    </tbody>
                      </table>
                    </div>
                  </div>
                  </div>
                </div>
              </div>
            </div>
          </div> 

                        </div>
                        <!-- container -->

                    </div>
                    <!-- Page content Wrapper -->
                </div>
                <!-- content -->

                <footer class="footer">
                    &copy; <script type="text/javascript">document.write(new Date().getFullYear())</script> MSS Payments Pvt. Ltd.
                </footer>

            </div>
            <!-- End Right content here -->

        </div>
        <!-- END wrapper -->


        <!-- jQuery  -->
        
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/bootstrap-material-design.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/modernizr.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.scrollTo.min.js"></script>

        <script src="${pageContext.request.contextPath}/resources/assets-group/plugins/carousel/owl.carousel.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/plugins/fullcalendar/vanillaCalendar.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/plugins/peity/jquery.peity.min.js"></script>
       <%--  <script src="${pageContext.request.contextPath}/resources/assets-group/pages/dashborad.js"></script> --%>

       <!-- Datatable -->
       
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/jquery.dataTables.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/dataTables.buttons.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/buttons.bootstrap4.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/jszip.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/pdfmake.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/vfs_fonts.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/buttons.html5.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/buttons.print.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/buttons.colVis.min.js"></script>
        <!-- Responsive examples -->
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/dataTables.responsive.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/responsive.bootstrap4.min.js"></script>

  		<!-- Datatable init js -->
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/datatables.init.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/app.js"></script>
        
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        
        <!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Card Block</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		     <div class="modal-body">
		        <div class="blck_resnm">
		        	<form>
		        		<div class="form-group">
		        			<label for="option">Reason</label>
							  <select class="form-control" id="blockType" name="blockType">
							    <option value="suspend">Suspend</option>
							    <option value="Lost">Lost</option>
							    <option value="Stolen">Stolen</option>
							    <option value="Damaged">Damaged</option>
							  </select>
		        		</div>		        		
		        	</form>
		        </div>
		      </div>
		      <div class="modal-footer">
		       <!--  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
		        <button type="button" class="btn btn-primary" id="blockCard">Submit</button>
		      </div>
		    </div>
		  </div>
		</div>
		
	    <script>
		   
		    $(document).ready(function() {
		        var table = $('#example').DataTable( {
		            lengthChange: true,
		            pageLength: 100,
		            buttons: [  'excel', 'csv' ]
		        } );
		     
		        table.buttons().container()
		            .appendTo( '#example_wrapper .col-md-6:eq(0)' );
		    });
	    </script>
        
        <script>
		$(function() {
		
		    var start = moment().subtract(29, 'days');
		    var end = moment();
		
		    function cb(start, end) {
		        $('#reportrange').html(start.format('MM-dd-yyyy') + ' - ' + end.format('MM-dd-yyyy'));
		    }
		
		    $('#reportrange').daterangepicker({
		        startDate: start,
		        endDate: end,
		        locale: {
		        	format: 'YYYY-MM-DD'
		        },
		        dateLimit: {
		            "days": 30
		        },
		        ranges: {
		           'Today': [moment(), moment()],
		           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		           'This Month': [moment().startOf('month'), moment().endOf('month')],
		           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		        }
		    }, cb);
		
		    cb(start, end);
		    
		});
		</script>
		
		  <script type="text/javascript">

function unblk(){
	$.ajax({
	    type:"POST",
	    contentType : "application/json",
	    url:"${pageContext.request.contextPath}/Group/UnblockCard",
	    dataType : 'json',
		data : JSON.stringify({
			"cardId" : "" + cardId + "",
			"requestType" : "Active"
		}),
		success : function(response) {
	         /*  your code here */
	        if (response.code.includes("S00")) {
	        	$('#myModal').modal('hide');
				swal("Success!!", response.message, "success");}
	        else{
	        	$('#myModal').modal('hide');
	        	swal({
					  type: 'error',
					  title: 'Sorry!!',
					  text: response.message
					});
	        }
	    }
	   
	});
	
}

</script>

 <script>
 var cardId="${cardDetail.cardId}";
 var cont_path="${pageContext.request.contextPath}";
 // Get the modal
 var modal = document.getElementById('myModal');
 var blockBtn=document.getElementById('blockType').value;
 

 $("#blockCard").click(function(){
	 console.log('Hi....');
	$.ajax({
	    type:"POST",
	    contentType : "application/json",
	    url:"${pageContext.request.contextPath}/Group/BlockCard",
	    dataType : 'json',
	    data : JSON.stringify({
			"cardId" : "" + cardId + "",
			"requestType" : ""+blockBtn+""
		}),
		success : function(response) {
	         /*  your code here */
	        if (response.code.includes("S00")) {
	        	$('#myModal').modal('hide');
				swal("Success!!", response.message, "success");}
	        else{
	        	$('#myModal').modal('hide');
	        	swal({
					  type: 'error',
					  title: 'Sorry!!',
					  text: response.message
					});
	        }
	    }
	   
	});
	
});

</script>
		
		

<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>

    </body>

</html>