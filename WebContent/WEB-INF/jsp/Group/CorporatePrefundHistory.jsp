<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Ewire</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Mannatthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/assets-group/images/favicon.png">

        <!-- CSS -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/plugins/animate/animate.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/bootstrap-material-design.min.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/icons.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/style.css" type="text/css">
       <%--  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-group/css/custom.css"> --%>

        <!-- Datatables -->
        <link href="${pageContext.request.contextPath}/resources/assets-group/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/assets-group/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
     <!-- Responsive datatable examples -->
      	<link href="${pageContext.request.contextPath}/resources/assets-group/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
		
		<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
		<script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.min.js"></script>
		<script>var contextPath = "${pageContext.request.contextPath}";</script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/GassignService.js"></script>
   
    </head>


    <body class="fixed-left">

        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner"></div>
            </div>
        </div>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <jsp:include page="/WEB-INF/jsp/Group/LeftMenu.jsp" />
            <!-- Left Sidebar End -->

            <!-- Start right Content here -->

            <div class="content-page">
                <!-- Start content -->
                <div class="content">

                    <!-- Top Bar Start -->
                        <jsp:include page="/WEB-INF/jsp/Group/Header.jsp" />
                    <!-- Top Bar End -->

                    <div class="page-content-wrapper dashborad-v">

                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="page-title-box">
                                        <h4 class="page-title">Corporate Prefund History</h4>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="row">
                               
                                <div class="col-md-12 col-xl-12">
                                    <div class="card m-b-30">
                                          <div class="card-body">
						                 
						                  <div class="row">
													<div class="col-12">
														<div class="card-box">
															<div class="row">
																<div class="col-md-4 col-sm-4 col-xs-4">
																	<form action="${pageContext.request.contextPath}/Group/PrefundHistory" method="post">
																		<div class="form-row">
																			<div class="col-sm-8">
																				<div id="" class="pull-left form-group bmd-form-group" style="cursor: pointer;">
																					<label class="sr-only bmd-label-floating" for="filterBy">Filter By:</label>
																				   	<input type="text" id="reportrange" name="reportrange" class="form-control" readonly="readonly"/>
																				</div>
																			</div>
																			<div class="col-sm-3">
																				<button class="btn btn-primary" onclick="fetchlist()" type="submit" style="position:relative; margin-top:38%;">Filter</button>
																			</div>
																		</div>
																	</form>
																</div>
																																
															</div>
															<hr>
														</div>
													</div>
												</div>
						                  
						                  <div class="tabble text-black">
						                     <table id="datatable-buttons" class="table table-striped table-bordered w-100">
						                      <thead>
						                        <tr>						                           
						                          <th><b>Sl No.</b></th>
						                          <th><b>Date</b></th>
						                          <th><b>Transaction Ref No</b></th>
						                          <th><b>Amount</b></th>
						                          <th><b>Status</b></th>                       
						                        </tr>
						                      </thead>
						                      <tbody>
						                      <c:forEach items="${prefund}" var="partner" varStatus="loopCounter">
													<tr>
														<td>${loopCounter.count}</td>
														<td> <c:out value="${partner.created} " default="" escapeXml="true" /></td>
															<td> <c:out value="${partner.transactionRefNo}" default="" escapeXml="true" /></td>
															<td> <c:out value="${partner.amount}" default="" escapeXml="true" /></td>
															<c:choose>
															<c:when test="${partner.prefundStatus==true}">
															<td> <c:out value="Success" default="" escapeXml="true" />
														</td>
														</c:when>
														<c:otherwise>
														<td> <c:out value="Failed" default="" escapeXml="true" /></td>
														</c:otherwise>
														</c:choose>
													 </tr>
												</c:forEach>						                      
						                      </tbody>
						                    </table>
						                  </div>
						                </div>
                                    </div>
                                </div> 
                            </div> 

                        </div>
                        <!-- container -->

                    </div>
                    <!-- Page content Wrapper -->
                </div>
                <!-- content -->

                <footer class="footer">
                    &copy; <script type="text/javascript">document.write(new Date().getFullYear())</script> MSS Payments Pvt. Ltd.
                </footer>

            </div>
            <!-- End Right content here -->

        </div>
        <!-- END wrapper -->


        <!-- jQuery  -->
        
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/bootstrap-material-design.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/modernizr.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.scrollTo.min.js"></script>

        <script src="${pageContext.request.contextPath}/resources/assets-group/plugins/carousel/owl.carousel.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/plugins/fullcalendar/vanillaCalendar.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/plugins/peity/jquery.peity.min.js"></script>
       <%--  <script src="${pageContext.request.contextPath}/resources/assets-group/pages/dashborad.js"></script> --%>

       <!-- Datatable -->
       
         <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/jquery.dataTables.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/dataTables.buttons.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/buttons.bootstrap4.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/jszip.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/pdfmake.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/vfs_fonts.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/buttons.html5.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/buttons.print.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/buttons.colVis.min.js"></script>
        <!-- Responsive examples -->
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/dataTables.responsive.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/datatables/responsive.bootstrap4.min.js"></script>

  		<!-- Datatable init js -->
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/datatables.init.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/app.js"></script>
       <%--  <script src="${pageContext.request.contextPath}/resources/assets-group/js/custom.js"></script> --%>
        
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        
        
	    <script>
		   
		    $(document).ready(function() {
		        var table = $('#example').DataTable( {
		            lengthChange: true,
		            pageLength: 100,
		            buttons: [  'excel', 'csv' ]
		        } );
		     
		        table.buttons().container()
		            .appendTo( '#example_wrapper .col-md-6:eq(0)' );
		    });
	    </script>
        
           <script>
		$(function() {
		
		    var start = moment().subtract(29, 'days');
		    var end = moment();
		
		    function cb(start, end) {
		        $('#reportrange').html(start.format('MM-dd-yyyy') + ' - ' + end.format('MM-dd-yyyy'));
		    }
		
		    $('#reportrange').daterangepicker({
		        startDate: start,
		        endDate: end,
		        locale: {
		        	format: 'YYYY-MM-DD'
		        },
		        dateLimit: {
		            "days": 30
		        },
		        ranges: {
		           'Today': [moment(), moment()],
		           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		           'This Month': [moment().startOf('month'), moment().endOf('month')],
		           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		        }
		    }, cb);
		
		    cb(start, end);
		    
		});
		</script>
		
	<script>
	function getSingleUserDetails() {
		var username=$("#username").val();
		var valid=true;
		if(username == ''){
			valid=false;
			$("#err").html("Please enter the username")
		}
		
		else if(username.length !=10){
			valid=false;
			$("#err").html("Please enter the valid username")
		}
		
		if(valid == true) {
			$("#myForm").submit();
		}
	}

	</script>

		

<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>

    </body>

</html>