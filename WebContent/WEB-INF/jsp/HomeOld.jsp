<!DOCTYPE html>
<html>
<head>
	<title>Cashier Card | Home</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<meta name="keywords" content="Cashier Card - Enjoy great offers online & offline using Cashier Prepaid card. Fast & Easy Recharge. Pay Utility Bills. Swipe & Save.">
    <meta name="description" content="India&#039;s Digital Cash. Go cashless with Cashier Prepaid Card. Experience a seamless, safe payments platform secured with Mastercard.">

    <!-- favicon -->
  	<link rel="icon" href="${pageContext.request.contextPath}/resources/assets-newui/img/favicon.png" type="image/gif" sizes="32x32">

	<!-- Bootstrap css -->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/css/style.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/css/responsive.css">

	<!-- google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

	<!-- slick slider -->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/slick/slick.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/slick/slick-theme.css">
    <meta name="google-site-verification" content="MwnpqAtORMtO_a1f-wwNoLVO2mQi8xQjhjpL4-vg_kQ" />


    <style>
        .slider--vdo .slider__main .item__video {
		  width: 100%;
		  height: auto;
		  padding-bottom: 56.25%;
		  position: relative;
		}
		.slider--vdo .slider__main .item__video iframe {
		  position: absolute;
		  top: 50%;
		  left: 50%;
		  transform: translate(-50%, -50%);
		  max-width: 100%;
		  width: 100%;
		  height: 100%;
		}
		.slider--vdo .slider__main .item__video:after {
		  display: none;
		}
		@media screen and (min-width: 1200px) {
		  .slider--vdo .slider__main .item__video:after {
		    display: block;
		    position: absolute;
		    top: 0;
		    left: 0;
		    right: 0;
		    bottom: 0;
		    background: rgba(0, 0, 0, 0.5);
		    z-index: 3;
		    cursor: move;
		  }
		}
		.slider--vdo .slider__main .item.slick-current .item-video__video:after {
		  display: none;
		}

		    </style>

 
 <script type="text/javascript">
       //window.history.forward();
        function noBack() {
            window.history.forward();
        }
    </script>
    
<script>
var context_path="${pageContext.request.contextPath}";
</script>


</head>
<body onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload="">

<a href="javascript:void(0);" id="top"><i class="fa fa-chevron-up"></i></a>

<nav class="navbar navbar-white">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="${pageContext.request.contextPath}/Home">
      	<img src="${pageContext.request.contextPath}/resources/assets-newui/img/logo.png" class="img-responsive">
      </a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <!-- <ul class="nav navbar-nav">
        <li class="active"><a href="#">About Us</a></li>
        <li><a href="#">contact</a></li>
        <li><a href="#">offers</a></li> 
        <li><a href="#">Page 3</a></li> 
      </ul> -->
      <ul class="nav navbar-nav navbar-right">
        <li><a href="${pageContext.request.contextPath}/About">about us</a></li>
        <li><a href="#section1">offers</a></li>
        <!-- <li><a href="#section2">testimonial</a></li> -->
        <li><a href="#section3">contact</a></li>
        <!-- <li><a href="#">refer &amp; earn</a></li> -->
        <li><a href="${pageContext.request.contextPath}/User/Login" class="btn btn-sm btn-custom" style="margin-right: 6px;">login</a></li>
        <li><a href="${pageContext.request.contextPath}/User/SignUp" class="btn btn-sm btn-custom">register</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="body-container">
	<div class="container" id="hero-image">

		<div class="inner-container">
			<!-- Banner -->
			<div class="home">
				<!-- <div class="bannner"></div> -->
				<div class="bannerWrp">
					<div>
						<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/banner1.png" class="img-responsive">
					</div>
					<div>
						<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/bg1.png" class="img-responsive">
					</div>
					<div>
						<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/bg2.png" class="img-responsive">
					</div>
					<div>
						<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/bg3.png" class="img-responsive">
					</div>
				</div>
				<!-- <a href="${pageContext.request.contextPath}/User/SignUp" class="btn hero-btn">get started</a> -->
			</div>

			<!-- Get Started -->
			<div class="getStarted">
				<div class="getStarted_head text-center">
					<span>getting started with cashier card</span>
				</div>
				<div class="getStarted_body">
					<div class="row">
						<div class="col-sm-4 col-xs-12">
							<div class="get-head text-center">
								<p>1. Create a account</p>
							</div>
							<div class="get-body">
								<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/p1.png" class="img-responsive imgsize-1"></center>
							</div>
							<div class="get-foot text-center">
								<span>To use Cashier Card you need to<br> sign up with your mobile number<br> and you are ready to go.</span>
							</div>
						</div>
						<div class="col-sm-4 col-xs-12">
							<div class="get-head text-center">
								<p>2. Add your money</p>
							</div>
							<div class="get-body">
								<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/p2.png" class="img-responsive imgsize-1"></center>
							</div>
							<div class="get-foot text-center">
								<span>Next Step is to add your money<br> from your bank account to your<br> Cashier card wallet.</span>
							</div>
						</div>
						<div class="col-sm-4 col-xs-12">
							<div class="get-head text-center">
								<p>3. Start spending</p>
							</div>
							<div class="get-body">
								<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/p3.png" class="img-responsive imgsize-2"></center>
							</div>
							<div class="get-foot text-center">
								<span>Use your cashier card to pay<br> anywhere on the internet or online stores<br> where the Mastercard logo is displayed</span>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- video Section -->
			<div class="video_wrp">
				<div class="row">
					<div class="col-sm-12 col-xs-12">
						<div class="col-xs-12" id="mobDisp">
							<div class="video_play">
								<iframe id="promoVid" width="520" height="315" src="https://www.youtube-nocookie.com/embed/N5e8ce7anUU?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
							</div>
						</div>
						<div class="col-sm-6 col-xs-12">
							<div class="video_content">
								<h3>Trend is changing and so do we, making cashless payments is the new trend. Here, we offer you the "Cashier Card". Forget your usual Bank debit/credit card, go ahead and use your own wallets/prepaid card.</h3>

								<!-- <p>Use Cashier Card across various categories ranging form utilities, food, travel, shopping, entertainment while having complete control.</p> -->

								<button class="btn hero-btn">Read More&nbsp;<i class="fa fa-angle-right"></i></button>
							</div>
						</div>
						<div class="col-sm-6" id="tabDsip">
							<!-- <div class="video_play">
								<iframe id="promoVid" width="520" height="315" src="https://www.youtube-nocookie.com/embed/N5e8ce7anUU?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
							</div> -->
							<div class="slider slider--vdo">
					            <div class="slider__main">
					                <div class="item">
					                    <div class="item__video">
					                        <iframe width="560" height="315" src="https://www.youtube.com/embed/JikDFh233_0?rel=0&showinfo=0&enablejsapi=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					                    </div>
					                </div>
					                <div class="item">
					                    <div class="item__video">
					                        <iframe width="560" height="315" src="https://www.youtube.com/embed/bFaL6rFCylE?rel=0&showinfo=0&enablejsapi=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					                    </div>
					                </div>
					                <div class="item">
					                    <div class="item__video">
					                        <iframe width="560" height="315" src="https://www.youtube.com/embed/N5e8ce7anUU?rel=0&showinfo=0&enablejsapi=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					                    </div>
					                </div>
					            </div>
					        </div>
						</div>
					</div>
				</div>
			</div>

			<!-- offer carousel -->
			<div class="slider-wrp">
				<div class="offers_banners">
					<div>
			    	<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/slider/slider13.png" class="img-responsive">
			    </div>
			    <div>
			    	<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/slider/slider14.png" class="img-responsive">
			    </div>
			    <div>
			    	<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/slider/slider15.png" class="img-responsive">
			    </div>
				<div>
			    	<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/slider/slider9.png" class="img-responsive">
			    </div>
				<div>
			    	<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/slider/slider12.png" class="img-responsive">
			    </div>
				<div>
			    	<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/slider/slider11.png" class="img-responsive">
			    </div>
				<div>
			    	<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/slider/slider10.png" class="img-responsive">
			    </div>
				<div>
			    	<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/slider/slider7.png" class="img-responsive">
			    </div>
			    <div>
			    	<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/slider/slider8.png" class="img-responsive">
			    </div>
					
				</div>
			</div>

			<!-- single offers -->
			<div class="singlOffrs" id="section1">
				<div class="singlOffrs_head text-center">
					<span>exciting offers with cashier card</span>
				</div>
				<div class="singlOffrs_body">
					<div class="innersingl_offrs">
						<ul class="row">
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o41.png" class="img-responsive"></center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o38.png" class="img-responsive"></center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o39.png" class="img-responsive"></center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o40.png" class="img-responsive"></center>
								</div>
							</li>
							<!-- =================== -->
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o31.png" class="img-responsive"></center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o1.png" class="img-responsive"></center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o9.png" class="img-responsive"></center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o15.png" class="img-responsive"></center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o17.png" class="img-responsive"></center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/offers/o23.png" class="img-responsive"></center>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="singlOffrs_footer">
					<div class="load_more text-center">
						<a href="${pageContext.request.contextPath}/Offers" class="custm_btn" id="">View More</a>
					</div>
				</div>
			</div>

			<!-- testimonial slider -->
			<!-- <div class="testi_wrap" id="section2">
				<div class="testi_hed text-center">
					<span>testimonial</span>
				</div>
				<div class="testi_body">
					<div class="row slide">
						<div class="col-sm-3">
							<div class="thumbnail">
						        <img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/testimonial/t1.png" alt="..." class="img-responsive">
						        <div class="caption">
						        	<p>loremipsum loremipsumlorem ipsumloremip sumloremipsumlorem ipsum loremipsuml oremipsum</p>
						        </div>
						        <div class="row">
						        	<div class="col-sm-12 thumb-foot">
						        		<div class="col-sm-6">
							        		<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/sign/yuv.png" class="img-responsive" style="width: 50%;"></center>
							        	</div>
							        	<div class="col-sm-6">
							        		<div class="testi_person">
							        			<span>Yuvraj Singh</span><br>
							        			<small>Player(Kings XI Punjab)</small>
							        		</div>
							        	</div>
						        	</div>
						        </div>
						    </div>
						</div>
						<div class="col-sm-3">
							<div class="thumbnail">
						        <img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/testimonial/t2.png" alt="..." class="img-responsive">
						        <div class="caption">
						        	<p>loremipsuml oremipsu mloremipsuml oremipsumlor emipsumlore mipsumlore mipsum loremipsum</p>
						        </div>
						        <div class="row">
						        	<div class="col-sm-12 thumb-foot">
						        		<div class="col-sm-6">
							        		<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/sign/asw.png" class="img-responsive" style="width: 50%;"></center>
							        	</div>
							        	<div class="col-sm-6">
							        		<div class="testi_person">
							        			<span>R Ashwin</span><br>
							        			<small>Captain(Kings XI Punjab)</small>
							        		</div>
							        	</div>
						        	</div>
						        </div>
						    </div>
						</div>
						<div class="col-sm-3">
							<div class="thumbnail">
						        <img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/testimonial/t3.png" alt="..." class="img-responsive">
						        <div class="caption">
						        	<p>loremips umloremipsumloremipsuml oremipsumlor emipsumlo remipsu mlorem ipsumlorem ipsum</p>
						        </div>
						        <div class="row">
						        	<div class="col-sm-12 thumb-foot">
						        		<div class="col-sm-6">
							        		<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/sign/gale.png" class="img-responsive" style="width: 50%;"></center>
							        	</div>
							        	<div class="col-sm-6">
							        		<div class="testi_person">
							        			<span>Chris Gale</span><br>
							        			<small>Player(Kings XI Punjab)</small>
							        		</div>
							        	</div>
						        	</div>
						        </div>
						    </div>
						</div>
						<div class="col-sm-3">
							<div class="thumbnail">
						        <img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/testimonial/t4.png" alt="..." class="img-responsive">
						        <div class="caption">
						        	<p>loremip sumloremipsumlo remipsumloremi psumlorem ipsu mloremipsuml oremips umloremipsum</p>
						        </div>
						        <div class="row">
						        	<div class="col-sm-12 thumb-foot">
						        		<div class="col-sm-6">
							        		<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/sign/brad.png" class="img-responsive" style="width: 50%;"></center>
							        	</div>
							        	<div class="col-sm-6">
							        		<div class="testi_person">
							        			<span>Brad Hodge</span><br>
							        			<small>Coach(Kings XI Punjab)</small>
							        		</div>
							        	</div>
						        	</div>
						        </div>
						    </div>
						</div>
					</div>
				</div>
			</div> -->

			<!-- Download App from store -->
			<div class="download_app">
				<div class="app_heading text-center">
					<span>In the palm of your hands</span>
				</div>
				<div class="app_subhead text-center">
					<span>Download our App Now</span>
				</div>
				<div class="row">
					<div class="col-sm-8 col-sm-offset-2">
						<div class="col-sm-6 col-xs-6">
							<div>
								<a href="https://play.google.com/store/apps/details?id=in.MadMoveGlobal.CashierCard" target="_blank"><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/playS.png" class="img-responsive store-img right-align"></a>
							</div>
						</div>
						<div class="col-sm-6 col-xs-6">
							<div>
								<a href="https://itunes.apple.com/in/app/cashier-prepaid-cards/id1374882309?mt=8" target="_blank"><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/appS.png" class="img-responsive store-img left-align"></a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- contact us -->
			<div class="cntct_wrap" id="section3">
				<div class="cntct_hed text-center">
					<span>contact us</span>
				</div>
				<div class="cntct_body">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-8 col-sm-12 col-xs-12">
								<div class="contact_frm_hed">
									<h2>Reach out to us!</h2>
									<span>Got a question about Cashier.Are you interested in partnering with us?Have some suggestions or just want to say hi? contact us:</span>
								</div>
								<div class="contact_frm">
									<form>
										<div class="col-sm-6">
											<div class="form-group">
												<input type="text" id="firName" name="" class="form-control" placeholder="Enter First Name">
											<p id="error_firstname" style="color: red"></p>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<input type="text" name="" id="lasName" class="form-control" placeholder="Enter Last Name">
											<p id="error_lastName" style="color: red"></p>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<input type="text" name="" id="contatNum" class="form-control" placeholder="Enter Mobile Number">
											<p id="error_contact" style="color: red"></p>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<input type="text" name="" id="emai" class="form-control" placeholder="Enter E-mail">
											<p id="error_imail" style="color: red"></p>
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group">
												<textarea class="form-control" id="messg" placeholder="Message..." rows="4"></textarea>
											<p id="error_message" style="color: red"></p>
											</div>
										</div>
										<div class="form-group text-right">
											<a class="custm_btn" type="button" id="email_request"  onclick="sendMail()">Submit</a>
										</div>
									</form>
								</div>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12">
								<div class="customcare_contain">
									<div class="contact_hed">
										<h2>Customer Care</h2>
										<span>Not sure where to start? Need help? Just visit our <span>help center</span> or get in touch with us:</span>
									</div>
									<div class="contact_bdy">
										<div class="col-md-12 col-sm-6 col-xs-12 singl_contac">
											<div class="col-sm-3 col-xs-3">
												<div>
													<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/send.svg" class="img-responsive">
												</div>
											</div>
											<div class="col-sm-9 col-xs-9">
												<div>
													<strong>Mail us at:</strong><br>
													<span><a href="mailto:care@madmoveglobal.com">care@madmoveglobal.com</a></span>
												</div>
											</div>
										</div>

										<div class="col-md-12 col-sm-6 col-xs-12 singl_contac">
											<div class="col-sm-3 col-xs-3">
												<div>
													<img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/tel.svg" class="img-responsive">
												</div>
											</div>
											<div class="col-sm-9 col-xs-9">
												<div>
													<strong>Call us at:</strong><br>
													<span><a href="tel:+91-7259195449">+91-7259195449</a><br><em>(10 AM - 7 PM all days. Except Holidays)</em></span>
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<div class="social_wrapper">
									<div class="social_hed">
										<h4>Follow us at:</h4>
									</div>
									<div class="social_links">
										<ul class="row">
											<li>
												<a href="https://www.facebook.com/CashierCard/" target="_blank">
													<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/social/facebook.svg" class="img-responsive"></center>
												</a>
											</li>
											<li>
												<a href="https://twitter.com/cashiercard?lang=en" target="_blank">
													<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/social/twitter.svg" class="img-responsive"></center>
												</a>
											</li>
											<li>
												<a href="https://www.instagram.com/cashiercard/" target="_blank">
													<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/social/instagram.svg" class="img-responsive"></center>
												</a>
											</li>
											<li>
												<a href="https://www.youtube.com/channel/UCCt9wky_iLlTBU28_EpGSTw?pbjreload=10" target="_blank">
													<center><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/social/youtube.svg" class="img-responsive"></center>
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- footer starts here -->
			<div class="footer">
				<div class="row">
					<div class="col-sm-6 col-xs-12" id="terms_link">
						<span><a href="${pageContext.request.contextPath}/PrivacyPolicy">privacy policy</a> | <a href="${pageContext.request.contextPath}/TermsnConditions">terms &amp; conditions</a> | <a href="#">faq<small>s</small></a></span>
					</div>
					<div class="col-sm-6 col-xs-12 text-right" id="copyR">
						<div class="copyright">
							<span><script type="text/javascript">document.write(new Date().getFullYear());</script> &copy; copyright EWire.</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>
<!-- 
<div class="get-app hidden-xs">
	<span  class="pull-right" id="app_get">Get App</span>
	<div class="get-app-inner pull-right">
		<input type="text" name="getApp" id="get_app" maxlength="10" class="form-control" placeholder="Enter 10 Digit Mobile Number">
	</div>
	<span class="pull-right">
		<i class="glyphicon glyphicon-menu-left trans-03-sec"></i>
	</span>
</div> -->



<!-- scripts starts here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/bootstrap/js/bootstrap.min.js"></script>
<!-- slick slider -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/slick/slick.min.js"></script>

<script type="text/javascript" src="http://www.youtube.com/player_api"></script>
<!-- custom js for dashboard -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/js/dashboard.js"></script>


<script>
	// initialize slick slider(Home Banner)
	$(document).ready(function(){
	    $('.bannerWrp').slick({
	      slidesToShow: 1,
	      slidesToScroll: 1,
	      autoplay: true,
	      autoplaySpeed: 2000,
	      arrows: false,
	    });
	  });
</script>

<script type="text/javascript">
function sendMail(){
	var valid = true;
	var contact  = $('#contatNum').val();
	var email  = $('#emai').val();
	var firstname  = $('#firName').val();
	var lastName = $('#lasName').val() ;
	var message = $('#messg').val() ;
	
	if(contact.length <= 0){
	$("#error_contact").html("Please enter the contact number");
	valid = false;
	}
	
	if(email.length <= 0){
		$("#error_imail").html("Please enter the email id");
		valid = false;
		}
	if(firstname.length <= 0){
		$("#error_firstname").html("Please enter the first name");
		valid = false;
		}
	if(lastName.length <= 0){
		$("#error_lastName").html("Please enter the lastName");
		valid = false;
		}
	
	if(message.length <= 0){
		$("#error_message").html("Please enter the message");
		valid = false;
		}
	
	if(valid==true){
		$("#email_request").addClass("disabled");
		$.ajax({
			type : "POST",
			contentType : "application/json",
			url : context_path+"/Send/Mail",
			dataType : 'json',
			data : JSON.stringify({
				"contactNo" : "" + contact + "",
				"email":""+email+"",
				"firstName":""+firstname+"",
				"lastName":""+lastName+"",
				"message":""+message+""
			}),
			success : function(response) {
				$("#email_request").removeClass("disabled");
				$("#email_request").html("Sent");
				console.log(response);
// 				if (response.code.includes("S00")) {
// 					swal("Congrats!!", response.message, "success");
// 					$("#imps_success").html(response.message);
// 					$("#imps_success").css("color", "green");
					$('#contatNum').val("");
					 $('#emai').val("");
					$('#firName').val("") ;
					$('#lasName').val("");
					$('#messg').val("") ;
// 				}
// 				else if (response.code.includes("F00")) {
// // 					$("#imps_success").html(response.message);
// // 					$("#imps_success").css("color", "red");
// // 					swal({
// // 						  type: 'error',
// // 						  title: 'Sorry!!',
// // 						  text: response.message
// // 						});
// 					$('#contatNum').val("");
// 					 $('#emai').val("");
// 					$('#firName').val("") ;
// 					$('#lasName').val("");
// 					$('#messg').val("") ;
// 				}
			}
		});	
		}
		var timeout = setTimeout(function(){
			$("#error_contact").html("");
			$("#error_imail").html("");
			$("#error_firstname").html("");
			$("#error_lastName").html("");
			$("#error_message").html("");
		}, 3000);
}
</script>

<script>
		
$(document).ready(function() {
   $('.slider__main').slick({
     infinite: true,
     slidesToShow: 1,
     slidesToScroll: 1,
     arrows: true,
     dots: false,
     autoplay: true,
     autoplaySpeed: 17000
   });

  // $('.slider__main').on('swipe', function(event, slick, direction){
  //   var currentIndex = $('.slider__main').slick('slickCurrentSlide');
  //   $('.slider__thumbs .item').removeClass('active');
  //   $('.slider__thumbs .item').eq(currentIndex).addClass('active');
  //   $('.slider__thumbs').slick('slickGoTo', currentIndex);
  // });

  $('.slider__main').on('beforeChange', function(event, slick, currentSlide, nextSlide){
    var data = {"event":"command","func":"pauseVideo","args":""};
    var message = JSON.stringify(data);
    $("iframe", slick.$slides[currentSlide])[0].contentWindow.postMessage(message, '*');
});

 /* $('.slider--vdo .slider__main').on('beforeChange', function(event, slick, direction){
    $('#v1')[0].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
    $('#v2')[0].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
    $('#v3')[0].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
  });*/
  
  // $('.slider__thumbs .item').width($('.slider__main').width()/3 - 20);
});

	</script>


</body>
</html>