<!DOCTYPE html>
<html>
<head>
	<title>Cashier Card | Terms and Conditions</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<meta name="keywords" content="Cashier Card - Enjoy great offers online & offline using Cashier Prepaid card. Fast & Easy Recharge. Pay Utility Bills. Swipe & Save.">
    <meta name="description" content="India&#039;s Digital Cash. Go cashless with Cashier Prepaid Card. Experience a seamless, safe payments platform secured with Mastercard.">

    <!-- favicon -->
  	<link rel="icon" href="${pageContext.request.contextPath}/resources/assets-newui/img/favicon.png" type="image/gif" sizes="32x32">

	<!-- Bootstrap css -->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/css/style.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/css/responsive.css">

	<!-- google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

	<!-- slick slider -->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/slick/slick.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-newui/slick/slick-theme.css">

</head>
<body>

<a href="javascript:void(0);" id="top"><i class="fa fa-chevron-up"></i></a>
<nav class="navbar navbar-white">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="${pageContext.request.contextPath}/Home">
      	<img src="${pageContext.request.contextPath}/resources/assets-newui/img/logo.png" class="img-responsive">
      </a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <!-- <ul class="nav navbar-nav">
        <li class="active"><a href="#">About Us</a></li>
        <li><a href="#">contact</a></li>
        <li><a href="#">offers</a></li> 
        <li><a href="#">Page 3</a></li> 
      </ul> -->
      <ul class="nav navbar-nav navbar-right">
        <li><a href="${pageContext.request.contextPath}/About">about us</a></li>
        <li><a href="${pageContext.request.contextPath}/Offers">offers</a></li>
        <li><a href="${pageContext.request.contextPath}/Home#section3">contact</a></li>
        <!-- <li><a href="#">refer &amp; earn</a></li> -->
        <li><a href="${pageContext.request.contextPath}/User/Login" class="btn btn-sm btn-custom" style="margin-right: 6px;">login</a></li>
        <li><a href="${pageContext.request.contextPath}/User/SignUp" class="btn btn-sm btn-custom">register</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="body-container">
	<div class="container" id="hero-image">

		<div class="aboutWrp">
			<div class="secHeading text-center">
				<span>Terms and Conditions</span>
			</div>
			<div class="aboutTxt">
				<strong>This document lays out the Terms and Conditions ("Terms") which shall be applicable to the Co-branded Prepaid Cards issued by FEDERAL BANK and CASHIER.</strong>

				<h3 class="sub-head">Definitions:</h3>
				<p class="text-justified">1. CASHIER shall mean "Registered CASHIER" having its registered office at "Innova Pearl, 17,1st Main Rd, Koramangala Industrial Layout, 5th Block, Koramangala, Bengaluru, Karnataka-560095", India and its successors and permitted assigns.</p>

				<p class="text-justified">2. "FEDERAL BANK" shall mean FEDERAL BANK Ltd., a banking company within the meaning of section 5 (c) of the banking Regulation Act, 1949 incorporated under the Companies Act 1956/2013, having its registered office at Federal Towers ,Ernakulam ,Kerala 682031.</p>

				<p class="text-justified">3. The "Cardholder" means a person to whom card is issued and who are authorized to hold and use the card.</p>

				<hr>
				<h3 class="sub-head">2. Fees and Charges:</h3>
				<p class="text-justified">ATM withdrawal- Rs 20/-</p>

				<p class="text-justified">Two transactions fees will be waived-off every month if withdrawn from Federal Bank ATM.</p>

				<p class="text-justified">All the charges are exclusive of all taxes. All taxes as applicable will be levied. Charges and fees mentioned above are subject to review/changes from time to time at the sole discretion of "CASHIER".</p>

				<hr>
				<h3 class="sub-head">3. Withdrawal limits:</h3>
				<p class="text-justified">ATM Cash withdrawal is allowed on the card. Withdrawal limit will be as per the rules and regulations pertaining to the bank which owns the atm.</p>

				<hr>
				<h3 class="sub-head">4. Billing:</h3>
				<p class="text-justified">You can view your transaction details by downloading "CASHIER" Application from Playstore/Appstore</p>

				<p class="text-justified">i. The cardholder must immediately raise any disputes pertaining to transactions to "CASHIER" Helpline on "Number" (Mon-Sat: 9.00 am to 6.00 pm)</p>
				<p class="text-justified">ii. Grievance Redressal Escalation: We have a Grievance Redressal Centre within the Organization. If you want to make a complaint, please write to "Email" or call our Helpline on "Number" (Mon-Sat: 9.00 am to 6.00 pm)</p>

				<hr>
				<h3 class="sub-head">5. Cancellation/Withdrawal of the Card:</h3>
				<p class="text-justified">The Card shall remain the property of FEDERAL BANK at all times. If the Card Holder decides to cancel the Card, he/she shall give a written notice of at least "15"(fifteen) days and surrender Card to "CASHIER". The Card Holder shall also pay dues, if any, payable to FEDERALBANK/ "CASHIER" in connection with the Card. Similarly, if FEDERAL BANK / "CASHIER" decide to cancel/withdraw the Card, FEDERAL BANK/ "CASHIER" Limited shall give a prior written notice of "15" (fifteen) days to the Cardholder or the Corporate purchaser of the cards. The replacement of the Card will be subject to the tenure of the Card being still live and money being available on the Card. FEDERAL BANK/ "CASHIER" may at any time terminate the card.</p>

				<p class="text-justified">Upon the expiry of the Card, the remaining unused amounts shall be forfeited."CASHIER" will provide a prior notice of 30 (thirty) days before the expiry of the Card by way of SMS on your registered phone number with us. Kindly ensure that the contact information maintained with us is kept up to date. Any change in the same has to be communicated immediately.</p>

				<hr>
				<h3 class="sub-head">6. Loss/theft/misuse of card:</h3>
				<p class="text-justified">i. If a Card is lost or stolen, the Cardholder must immediately report such loss/theft to "CASHIER" Helpline on "Number" (Mon-Sat: 9.00am to 6.00 pm)</p>
				<p class="text-justified">ii. "CASHIER" will, upon adequate verification, suspend the Card and terminate all facilities in relation, thereto.</p>
				<p class="text-justified">The Cardholder's right to use the Card shall be terminated by "CASHIER" forthwith in the event of Loss or theft of the Card reported by the Cardholder.</p>
				<p class="text-justified">iii. The Cardholder shall accept full responsibility for any wrongful or fraudulent use of the Card and which is or may be in contravention of these Terms and Conditions.</p>

				<hr>
				<h3 class="sub-head">7. Disclosure:</h3>
				<p class="text-justified">Type of information relating to card holder to be disclosed with and without approval of cardholder</p>
				<p class="text-justified">i. The Cardholder acknowledges that the information on usage of the Card could be exchanged amongst banks and financial entities that provide similar facilities.</p>
				<p class="text-justified">ii. "CASHIER" shall not be obliged to disclose to the Cardholder the CASHIER of the bank or financial entity to which it disclosed the information.</p>
				<p class="text-justified">iii. The Cardholder shall	prior to availing the Card Services from "CASHIER", obtain appropriate advice and shall familiarize himself with the associated risks and all the terms and conditions pertaining to the service.</p>
				<p class="text-justified">iv. The Cardholder shall be bound by these Terms & Conditions and/or any other policy (ies) that may be stipulated by "CASHIER" or FEDERAL BANK, from time to time in this regard.</p>
				<p class="text-justified">v. The card can be used only in India after activation and can be used at ATM's.</p>
				<p class="text-justified">vi. The Card is not transferable under any circumstances. The Card is Reloadable in nature.</p>
				<p class="text-justified">vii. The Card can be used at any ATM which accepts MasterCard. For each transaction done with the card at an ATM the applicable charges will be debited from the Card.</p>
				<p class="text-justified">viii. Any request for Card replacement by the Cardholder will be processed by "CASHIER" and the Card will be sent to the customer address/corporate address only. In case of insufficient funds in the Card, "CASHIER" and FEDERAL BANK will not be liable to entertain such requests for Card replacement or Registration code re-issuance.</p>
				<p class="text-justified">ix. The Cardholder shall indemnify "CASHIER" & FEDERAL BANK to make good any loss, damage, interest, or any other financial charge that "CASHIER" may incur and/or suffer, whether directly or indirectly, as a result of the Cardholder committing violations of these Terms and Conditions.</p>
				<p class="text-justified">x. Cardholder and Corporate purchasers agree that the issuing of Card is subject to rules and regulations introduced or amended from time to time by the Reserve Bank of India or any other regulatory body.</p>

			</div>
		</div>

		<!-- Download App from store -->
		<div class="download_app">
			<div class="app_heading text-center">
				<span>In the palm of your hands</span>
			</div>
			<div class="app_subhead text-center">
				<span>Download our App Now</span>
			</div>
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					<div class="col-sm-6 col-xs-6">
						<div>
							<a href="https://play.google.com/store/apps/details?id=in.MadMoveGlobal.CashierCard" target="_blank"><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/playS.png" class="img-responsive store-img right-align"></a>
						</div>
					</div>
					<div class="col-sm-6 col-xs-6">
						<div>
							<a href="https://itunes.apple.com/in/app/cashier-prepaid-cards/id1374882309?mt=8" target="_blank"><img src="${pageContext.request.contextPath}/resources/assets-newui/img/landing/appS.png" class="img-responsive store-img left-align"></a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- footer starts here -->
		<div class="footer">
			<div class="row">
				<div class="col-sm-6 col-xs-12" id="terms_link">
					<span><a href="${pageContext.request.contextPath}/PrivacyPolicy">privacy policy</a> | <a href="${pageContext.request.contextPath}/TermsnConditions">terms &amp; conditions</a> | <a href="#">faq<small>s</small></a></span>
				</div>
				<div class="col-sm-6 col-xs-12 text-right" id="copyR">
					<div class="copyright">
						<span><script type="text/javascript">document.write(new Date().getFullYear());</script> &copy; copyright EWire.</span>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>




<!-- scripts starts here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/bootstrap/js/bootstrap.min.js"></script>
<!-- slick slider -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/slick/slick.min.js"></script>

<!-- custom js for dashboard -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets-newui/js/dashboard.js"></script>

</body>
</html>