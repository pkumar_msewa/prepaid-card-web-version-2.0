<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body onLoad="document.myForm.submit()">
<h3>Please Wait while we're redirecting you to Payment Gateway...</h3>

<form id='myForm' name='myForm' action="${fn:escapeXml(pg.saleUrl)}" method='post' 
enctype="application/x-www-form-urlencoded">
<input type="hidden" name="merchantID" value="${fn:escapeXml(pg.merchantId)}" />
<input type="hidden" name="merchantTxnNo" value="${fn:escapeXml(pg.merchantTxnNo)}" />
<input type="hidden" name="amount" value="${fn:escapeXml(pg.amount)}" />
<input type="hidden" name="currencyCode" value="${fn:escapeXml(pg.currencyCode)}" />
<input type="hidden" name="payType" value="${fn:escapeXml(pg.payType)}" />
<input type="hidden" name="customerEmailId" value="${fn:escapeXml(pg.customerEmailId)}" />
<input type="hidden" name="transactionType" value="${fn:escapeXml(pg.transactionType)}" />
<input type="hidden" name="returnURL" 
value="${fn:escapeXml(pg.returnURL)}" />
<input type="hidden" name="txnDate" value="${fn:escapeXml(pg.txnDate)}" />
<input type="hidden" name="customerMobileNo" value="${fn:escapeXml(pg.customerMobileNo)}" />
<input type="hidden" name="secureHash" 
value="${fn:escapeXml(pg.secureHash)}" />
</form>
  	<script>document.getElementById('myForm').submit();</script> 
</body>
</html>