<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<head>
	<title>Client Services</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/linearicons/style.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/chartist/css/chartist-custom.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link rel="stylesheet" href="<c:url value="/resources/assets/vendor/bootstrap/css/bootstrap.min.css"/>">
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="${pageContext.request.contextPath}/resources/assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="${pageContext.request.contextPath}/resources/assets/img/favicon.png">
	<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
	rel="stylesheet" type="text/css" />
	<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- HEADER -->
		<jsp:include page="/WEB-INF/jsp/Client/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Client/LeftMenu.jsp" />
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
							<div class="panel panel-headline">
								<div class="panel-heading">
							<h3 class="panel-title">CLIENT SERVICES</h3></div>
						<div class="panel-body" style="background: #eaeaea;">
						
						<!-- 						Model window -->

							<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
								aria-labelledby="exampleModalLabel" aria-hidden="true" style="padding-top: 4%;">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<div class="col-md-12" >
												<div class="col-md-6"  class="form-control">
											<h4 class="modal-title" id="exampleModalLabel"><c:out value="${clientName}"></c:out></h4>
											</div>
												<div class="col-md-6"  class="form-control" align="right">
											<button type="button" class="close" data-dismiss="modal"
												aria-label="Close">
												<span><i title ="Close"class="fa fa-times" aria-hidden="true"   style="color:red"></i></span>
											</button>	</div></div>
										</div>
										<div class="modal-body">

											<form class="form-auth-small" action="${pageContext.request.contextPath}/Client/updateService" method="post">
												<div class="row">
													<input type="hidden" id="merchantId" name="merchantId"
														value="">
														 <input type="hidden" id="cdId"
														name="clientDetailId" value=""> <input type="hidden" id="uId" name="val"
														value="${userId}">
														<input type="hidden" id="cstatus" value="">
<!-- 													<div class="col-md-4" class="form-control"> -->
<!-- 														<label>Merchant Name</label> <input type="text" -->
<!-- 														style="cursor:not-allowed"	name="merchantName" class="form-control" id="mId" -->
<!-- 															value="" placeholder="" readonly required> -->
<!-- 													</div> -->
													<div class="col-md-4">
														<label>Service Name</label> <input type="text"
														style="cursor:not-allowed"	name="serviceName" class="form-control" id="sId" value=""
															placeholder="" readonly>
													</div>
													<div class="col-md-4">
														<label id="lableId">Commission*</label> <input type="text"
															name="commission" class="form-control" id="cId" value="" maxlength="10"
															placeholder="Enter Commission" readonly onkeypress="return isNumberKey(event)"><p id="errorMsg"></p>
													</div>
													<div class="col-md-4">
														<label>Status</label><br /> <select name="status"
															id="statusId" class="form-control" onchange="validateChanges(this.value);"   >
															<option value="Active" >Active</option>
															<option value="Inactive">Inactive</option>
														</select>
													</div>
												</div>
												<br/>
												<div class="row">
													<div class="col-md-4">
														<label>Type</label><br /> <select name="type" id="typeId"
															class="form-control" disabled >
															<option value="POST">POST</option>
															<option value="PRE">PRE</option>
														</select>
													</div>
													<div class="col-md-4">
														<label>Is Fixed</label><br /> <select name="fixed"
															id="fixeId" class="form-control" disabled>
															<option value="true" >True</option>
															<option value="false">False</option>
														</select>
													</div>
													
													<div class="col-md-4" align="left" style="margin-top: 26px;">
														<button type="submit" class="btn btn-primary" id="buttonId">Submit</button>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
							<!-- 						================= -->
						
						
								<div class="row" id="table-wrapper" >
								 <div id="table-scroll">
								<table id="editedtable" class="table table-striped table-bordered date_sorted" >
									<thead>
										<tr>
										    <th>S.NO</th>
											<th>Name</th>
<!-- 											<th>Code</th> -->
<!-- 											<th>Min Amount</th> -->
											<th>Amount Range</th>
											<th>Commission</th>
											<th>Commission Type</th>
											<th>Status</th>
											<th>Action</th>
										</tr></thead>
										<tbody>
										<c:forEach items="${clientServices}" var="clientServices" varStatus="loopCount">
											<tr>
											<td>${loopCount.count}</td>
											<td><c:out value="${clientServices.name}"/></td>
<%-- 											<td><c:out value="${clientServics.code}"/></td> --%>
											<td><c:out value="${clientServices.minAmount}"/> - <c:out value="${clientServices.maxAmount}"/></td>
											<td><c:out value="${clientServices.commission}"/></td>
											<td><c:out value="${clientServices.fixedVal}"/></td>
											<td><c:out value="${clientServices.status}"/></td>
											<td><i title="Edit" style="cursor: pointer; color: #d86b1d;" onclick="setCommission('${clientServices.name}','${clientServices.commission}',
                                           '${clientServices.clientDetailId}','${clientServices.merchantServiceId}','${clientServices.serviceType}','${clientServices.fixedVal}','${clientServices.status}')" class="fa fa-pencil-square-o" aria-hidden="true" ></i></td>
												</tr>	
											</c:forEach>
										</tbody>					
									</table>
								</div>
								</div>
						</div>
					</div>
					<!-- END OVERVIEW -->
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
			<!-- END MAIN -->
			<div class="clearfix"></div>
			<footer>
				<div class="container-fluid">
					<p class="copyright">
						&copy; 2017 <a href="https://www.msewa.com" target="_blank">MSewa
							Software Solution Pvt. Ltd.</a>. All Rights Reserved.
					</p>
				</div>
			</footer>
		</div>
		<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/chartist/js/chartist.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/scripts/klorofil-common.js"></script>
	 <script src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
	<script>
		$(document).ready(function() {
			$('#editedtable').DataTable({
				 "bLengthChange": false,
				 "bPaginate" : true,	
		 		    "bFilter" : true,
		 		    "bInfo" : false,
				"order" : [],
				columnDefs : [ {
					orderable : true,
					targets : [ 0 ]
				}, {
					orderable : true,
					targets : [ 1 ]
				}, {
					orderable : true,
					targets : [ 2 ]
				}, {
					orderable : true,
					targets : [ 3 ]
				}, {
					orderable : true,
					targets : [ 4 ]
				}, {
					orderable : true,
					targets : [ 5 ]
				} ],
			});
		});
	</script>
	
	<script>
	function setCommission(sname,commission,clientDetailId,merchantId,type,fixed,status){
		console.log(sname+":"+commission+":"+clientDetailId+":"+merchantId+":"+type+":"+fixed+":"+status);
		$('#sId').val(sname);
// 		$('#mId').val(mname);
		$('#cId').val(commission);
		$('#cdId').val(clientDetailId);
		$('#merchantId').val(merchantId);
// 		$('#typeValId').val(type);
		$('#cstatus').val(status);
// 		$('#maxCom').val(merchantCommission);
// 		$('#lableId').html('<label id="lableId">Commission* Max('+merchantCommission+')</label>')
		 $('#errorMsg').hide();
		 $('#buttonId').attr('disabled','disabled');
		if(type!=null && type!='' && type=='PRE')
		{
			document.getElementById('typeId').getElementsByTagName('option')[1].selected = 'selected'
		}else{
			document.getElementById('typeId').getElementsByTagName('option')[0].selected = 'selected'
		}
		if(fixed!=null && fixed!='' && fixed=='%')
		{
			document.getElementById('fixeId').getElementsByTagName('option')[1].selected = 'selected'
		}else{
			document.getElementById('fixeId').getElementsByTagName('option')[0].selected = 'selected'
		}	
		if(status!=null && status!='' && status=='Inactive')
		{
			document.getElementById('statusId').getElementsByTagName('option')[1].selected = 'selected'
		}else{
			document.getElementById('statusId').getElementsByTagName('option')[0].selected = 'selected'
		}
		$('#myModal').modal('show'); 
	}
	
	
	function validateChanges(status){
		var currentStatus=$('#cstatus').val();
		console.log(status+":"+currentStatus);
		if(currentStatus == status){
			$('#buttonId').attr('disabled','disabled');
		}else{
			$('#buttonId').removeAttr('disabled');
		}
	}
	
	</script>
	
	
</body>

</html>
