<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<head>
<title>Dashboard</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!-- VENDOR CSS -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/assets/vendor/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/assets/vendor/linearicons/style.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/assets/vendor/chartist/css/chartist-custom.css">
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<!-- MAIN CSS -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/assets/css/main.css">
<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/assets/css/demo.css">
<!-- GOOGLE FONTS -->
<link
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700"
	rel="stylesheet">
<!-- ICONS -->
<link rel="apple-touch-icon" sizes="76x76"
	href="${pageContext.request.contextPath}/resources/assets/img/apple-icon.png">
<link rel="icon" type="image/png" sizes="96x96"
	href="${pageContext.request.contextPath}/resources/assets/img/favicon.png">
<link rel="stylesheet"
	href="<c:url value="/resources/assets/vendor/bootstrap/css/bootstrap.min.css"/>">
<script
	src="${pageContext.request.contextPath}/resources/admin/js/jquery.min.js"></script>

</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- HEADER -->
		<jsp:include page="/WEB-INF/jsp/Client/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Client/LeftMenu.jsp" />
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<div class="panel panel-headline">
						<div class="panel-body">
							<div class="panel-heading" align="center">
								<h3 class="panel-title">Change Password</h3>
							</div>
							<form id="formId" class="form-auth-small"
								action="<c:url value="/Client/changePassword"/>" method="post"
								onsubmit="return validate();">
								<input type="hidden" name="id" value="${userId}">
								<div class="row">
									<div class="col-md-4" class="form-control"></div>
									<div class="col-md-4" class="form-control">
										<label>Current Password*</label> <input type="text"
											name="currentPass" class="form-control" id="cPass" value=""
											placeholder="Current Password" maxlength="25">
											<p id="cError"></p>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4" class="form-control"></div>
									<div class="col-md-4">
										<label>New Password*</label> <input type="password"
											name="newPass" class="form-control" id="nPass" value=""
											placeholder="New Password" maxlength="25">
											<p id="nError"></p>
									</div>
									</div>
									<div class="row">
									<div class="col-md-4" class="form-control"></div>
									<div class="col-md-4">
										<label>Confirm New Password*</label> <input type="password"
											name="conNewPass" class="form-control" id="cnPass" value=""
											placeholder="Confirm New Password" maxlength="25">
											<p id="cnError"></p>
									</div>
								</div>
								<div align="center">
								<c:choose>
										<c:when test="${passSuccessMsg !=null && passSuccessMsg!=''}">
											<p style="color:green">
												<c:out value="${passSuccessMsg}"></c:out>
											</p>
										</c:when>
										<c:otherwise>
											<p style="color:red">
												<c:out value="${passErrorMsg}"></c:out>
											</p>
										</c:otherwise>
									</c:choose>
								</div>
								<div class="row">
									<div class="col-md-4" class="form-control"></div>
									<div class="col-md-6" class="form-control">
										<div class="col-md-3" align="right">
											<button type="submit" class="btn btn-primary" title="Save" style="margin-left: 56px;">Save</button>
										</div>
										<div class="col-md-3" align="right" >
											<button type="button" onclick="location.href='${pageContext.request.contextPath}/Client/Home'"
												title="Cancel"  class="btn btn-info ">Cancel</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>


				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<footer>
			<div class="container-fluid">
				<p class="copyright">
					&copy; 2017 <a href="https://www.msewa.com" target="_blank">MSewa
						Software Solution Pvt. Ltd.</a>. All Rights Reserved.
				</p>
			</div>
		</footer>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->

	<script
		src="${pageContext.request.contextPath}/resources/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets/vendor/chartist/js/chartist.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets/scripts/klorofil-common.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>

</body>
<script>
	function validate() {
		var cPass = $('#cPass').val();
		var newPass = $('#nPass').val();
		var conNewPass = $('#cnPass').val();
		if (cPass == null || cPass == '') {
			$('#cError').html('<p style="color:red" id="cError">Please enter current password</p>');
			return false;
		}
		if (newPass == null || newPass == '') {
			$('#cError').html('<p style="color:red" id="cError"></p>');
			$('#nError').html('<p style="color:red" id="nError">Please enter new password</p>');
			return false;
		}
		if(newPass.length<6){
			$('#nError').html('<p style="color:red" id="nError"></p>');
			$('#nError').html('<p style="color:red" id="nError">Password length should be greater or equal to 6</p>');
			return false;
		}
		if (conNewPass == null || conNewPass == '') {
			$('#nError').html('<p style="color:red" id="nError"></p>');
			$('#cnError').html('<p style="color:red" id="cnError">Please confirm your new password</p>');
			return false;
		}
		if (newPass != conNewPass) {
			$('#cnError').html('<p style="color:red" id="cnError"></p>');
			$('#cnError').html('<p style="color:red" id="cnError">Password missmatch! confirm your new password </p>');
			return false;
		}
		return true;
	}
</script>
</html>
