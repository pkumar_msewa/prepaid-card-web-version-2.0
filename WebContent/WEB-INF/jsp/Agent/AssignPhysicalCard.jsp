<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<%
	response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
	response.setDateHeader("Expires", 0);
	response.setHeader("Pragma", "no-cache");
%>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Assign Physical Card</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">
    <!-- Bootstrap core CSS     -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Animation library for notifications   -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/animate.min.css" rel="stylesheet"/>
    <!--  Paper Dashboard core CSS    -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/paper-dashboard.css" rel="stylesheet"/>
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/demo.css" rel="stylesheet" />
    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/themify-icons.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>    
    <style type="text/css">
    fieldset {
	    padding: .35em 1.625em .75em;
	    margin: 0 2px;
	    border: 1px solid silver;
	    margin-bottom: 15px;
	}
	legend {
		width: auto;
	}
	.error{
    	color:red;
    	font-size:16px;
    }
    
    .success{
    	color:green;
    	font-size:16x;
    }
    </style>
</head>
<body oncopy="return false" oncut="return false" onpaste="return false">
	<div class="wrapper">
		<jsp:include page="LeftMenu.jsp"></jsp:include>
		<div class="main-panel">
			<jsp:include page="Header.jsp"></jsp:include>
			<div class="content">
				<div class="container-fluid">
					<div class="row">
                        <div class="col-md-10">
                            <div class="page-title-box">
                                <div class="clearfix"></div>
                            </div>
                        </div>
					</div>
					<center><strong id="errormsg" class="error">${error}</strong></center>
					<center><strong id="successmsg">${success}</strong></center>
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="col-md-6 col-md-offset-3">
										<fieldset>
											<legend>Assign Physical Card</legend>
		                                    <div class="form-group">
		                                        <label for="mobile">Kit No</label>
		                                        <input type="text" name="kitNo" placeholder="Enter kit no" id="kitNo" class="form-control" maxlength="20" onkeypress="return isNumberKey(event);">
		                                  		<p class="error" id="error_kitNo"></p>
		                                    </div>
		                                    <div class="form-group">
		                                        <label for="mobile">User Name/Phone No</label>
		                                        <input type="text" name="userName" placeholder="Enter Phone No" id="userName" class="form-control" maxlength="10" onkeypress="return isNumberKey(event);">
		                                  		<p class="error" id="error_userName"></p>
		                                    </div>
										</fieldset>
										<div class="cta-actions text-center">
		                              		<button class="btn btn-primary" type="button" onclick="validatePhysicalCard()" id="register">SUBMIT</button>
		                          		</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">OTP verify</h4>
        </div>
        <div class="modal-body">
	        <div class="form-group">
	            <label for="mobile">OTP</label>
	            <input type="text" name="otp" placeholder="Enter OTP" id="otp" class="form-control" maxlength="10" onkeypress="return isNumberKey(event);">
	      		<p class="error" id="error_otp"></p>
	      		<input type="hidden" value="" id="userName1">
	      		<input type="hidden" value="" id="kitNo1">
	        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-default" onclick="activateCard();">Submit</button>
        </div>
      </div>
    </div>
  </div>
	
	<script>
		function isNumberKey(evt){
		    var charCode = (evt.which) ? evt.which : evt.keyCode
		    return !(charCode > 31 && (charCode < 45 || charCode > 57));
		}
		function validatePhysicalCard(){
			var cont_path="${pageContext.request.contextPath}";
			var valid = true;
			var kitNo = $('#kitNo').val();
			var userName = $('#userName').val();
			var contactNoPattern = "[7-9]{1}[0-9]{9}";
			if (kitNo.length <= 0){
				$("#error_kitNo").html("Please enter kit no.");
				valid = false;
			} else if(kitNo.length != 12){
				$("#error_kitNo").html("Please enter your 12 digit kit no.");
				valid = false;
			}
			if (userName.length <= 0){
				$("#error_userName").html("Please enter your mobile no.");
				valid = false;
			}
			
			if(userName.length == 10) {
    			if(!userName.match(contactNoPattern)) {
	    			console.log("invalid contact no");
	    			$("#error_userName").html("Please enter valid mobile number");
		    		valid = false; 
	    		}
    		} else {
    			console.log("else invalid contact no block");
    			$("#error_userName").html("Please enter 10 digit contact number");
	    		valid = false; 
    		}
			if(valid == true) {
				$.ajax({
					type:"POST",
	        	    contentType : "application/json",
	        	    url: cont_path+"/Agent/AssignPhysicalCard",
	        	    dataType : 'json',
	    			data : JSON.stringify({
	    				"kitNo" : "" + kitNo + "",
	    				"userName" : ""+userName+""
	    			}),
	    			success : function(response){
	    				if (response.code.includes("S00")) {
	    					$('#userName1').val(userName);
	    					$('#kitNo1').val(kitNo);
	    					$('#myModal').modal('show'); 
	    				} else {
	    					$('#errormsg').html(response.message);
	    				}
	    			}
				});
		    }
			var timeout = setTimeout(function(){
		    	$("#error_kitNo").html("");
		    	$("#error_userName").html("");
		    	$('#errormsg').html("");
		    }, 3000);
			
		}
		
		function activateCard(){
			var cont_path="${pageContext.request.contextPath}";
			var valid = true;
			var otp = $('#otp').val();
			var userName = $('#userName1').val();
			var kitNo = $('#kitNo1').val();
			if (otp.length <= 0){
				$("#error_otp").html("Please enter OTP.");
				valid = false;
			}
			if(valid == true) {
				$.ajax({
					type:"POST",
	        	    contentType : "application/json",
	        	    url: cont_path+"/Agent/VerifyPhysicalCard",
	        	    dataType : 'json',
	    			data : JSON.stringify({
	    				"otp" : "" + otp + "",
	    				"userName" : ""+userName+ "",
	    				"kitNo" : "" +kitNo+ ""
	    			}),
	    			success : function(response){
	    				if (response.code.includes("S00")) {
	    					alert("Successfully activated your physical card.");
	    					location.reload();
	    				} else {
	    					$('#errormsg').html(response.message);
	    				}
	    			}
				});
			}
			var timeout = setTimeout(function(){
				$("#error_otp").html("");
		    }, 3000);
			
		}
	</script>
</body>
</html>