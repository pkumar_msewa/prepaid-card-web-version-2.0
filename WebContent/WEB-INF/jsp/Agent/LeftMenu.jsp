<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
  <div class="sidebar" data-background-color="white" data-active-color="danger">
    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="" class="simple-text">
                    <center><img src="${pageContext.request.contextPath}/resources/corporate/assets/img/logo.png" class="img-responsive" style="width: 140px;"></center>
                </a>
            </div>
			
            <ul class="nav">
                <li class="active">
                    <a href="${pageContext.request.contextPath}/Agent/Home">
                        <i class="ti-dashboard"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li>
                    <a href="${pageContext.request.contextPath}/Agent/UserRegister" >
                        <i class="ti-user"></i>
                        <p>USER REGISTRATION <b class="" style="margin-top: 14px;"></b></p>
						
                    </a>
                </li>
                
                 <li>
                    <a href="${pageContext.request.contextPath}/Agent/LoadCardForUser">
                        <i class="ti-credit-card"></i>
                        <p>Load Card<b class="" style="margin-top: 14px;"></b></p>
                    </a>
                </li>
                
                <li>
                    <a href="${pageContext.request.contextPath}/Agent/AgentPrefund">
                        <i class="ti-bar-chart"></i>
                        <p>Agent Prefund<b class="" style="margin-top: 14px;"></b></p>
                    </a>
                </li>
                
                <li>
                    <a href="${pageContext.request.contextPath}/Agent/AssignPhysicalCard">
                        <i class="ti-credit-card"></i>
                        <p>Assign Physical Card<b class="" style="margin-top: 14px;"></b></p>
                    </a>
                </li>
                
                <li>
                    <a href="${pageContext.request.contextPath}/Agent/UserList">
                        <i class="ti-layout-list-thumb"></i>
                        <p>User List<b class="" style="margin-top: 14px;"></b></p>
                    </a>
                </li>
                
                <li>
                    <a href="${pageContext.request.contextPath}/Agent/LoadCardList">
                        <i class="ti-view-list-alt"></i>
                        <p>Load Card List<b class="" style="margin-top: 14px;"></b></p>
                    </a>
                </li>
                
                <li>
                    <a href="${pageContext.request.contextPath}/Agent/AssignPhysicalCardRequestList">
                        <i class="ti-layout-cta-left"></i>
                        <p>Physical Card List<b class="" style="margin-top: 14px;"></b></p>
                    </a>
                </li>
                
                <li>
                    <a href="${pageContext.request.contextPath}/Agent/ChangePassword">
                        <i class="ti-lock"></i>
                        <p>Change Password<b class="" style="margin-top: 14px;"></b></p>
                    </a>
                </li>
                
               
                <%-- <li>
                    <a href="${pageContext.request.contextPath}/Corporate/Transactions">
                        <i class="ti-view-list-alt"></i>
                        <p>User Transactions<b class="" style="    margin-top: 14px;"></b></p>
						
                    </a>
                </li> --%>
                
                 <%-- <li>
                    <a href="${pageContext.request.contextPath}/Corporate/BulkCardGenerate">
                        <i class="ti-view-list-alt"></i>
                        <p>Bulk Card Generate<b class="" style="    margin-top: 14px;"></b></p>
						
                    </a>
                </li> --%>
            </ul>
    	</div>
    </div>
