<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<%
	response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
	response.setDateHeader("Expires", 0);
	response.setHeader("Pragma", "no-cache");
%>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Agent | User Registration</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">
    <!-- Bootstrap core CSS     -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Animation library for notifications   -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/animate.min.css" rel="stylesheet"/>
    <!--  Paper Dashboard core CSS    -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/paper-dashboard.css" rel="stylesheet"/>
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/demo.css" rel="stylesheet" />
    <!--  Fonts and icons     -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/themify-icons.css" rel="stylesheet">

    <link href="${pageContext.request.contextPath}/resources/corporate/assets/dropify/css/dropify.css" rel="stylesheet">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" />


    <style type="text/css">
    fieldset {
	    padding: .35em 1.625em .75em;
	    margin: 0 2px;
	    border: 1px solid silver;
	    margin-bottom: 15px;
	}
	legend {
		width: auto;
	}
	.input-group {
		display: flex;
	}
	.btn, .navbar .navbar-nav > li > a.btn {
		border-radius: 0;
		padding: 19px 19px;
	}
    </style>
</head>
<body oncopy="return false" oncut="return false" onpaste="return false">
	<div class="wrapper">
		<jsp:include page="LeftMenu.jsp"></jsp:include>
		<div class="main-panel">
			<jsp:include page="Header.jsp"></jsp:include>
			<div class="content">
				<div class="container-fluid">
					<div class="row">
                        <div class="col-md-10">
                            <div class="page-title-box">
                                <h4 class="page-title float-left">Add User</h4>
                                <div class="clearfix"></div>
                            </div>
                        </div>
					</div>
					<center><strong id="errormsg" style="color:red; font-size:16px;">${error}</strong></center>
					<center><strong id="errormsg" style="color:green; font-size:16px;">${success}</strong></center>
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<form action="${pageContext.request.contextPath}/Agent/UserRegister" method="post" id="formId" enctype="multipart/form-data">
									<fieldset>
                                        <legend>User Details</legend>
                                        <div class="col-sm-6">
                                        	<div class="form-group">
							                    <label for="name">First Name:</label>
							                    <input type="text" class="form-control" id="firstName" name="firstName" maxlength="30" placeholder="Enter Your FirstName" autocomplete="off" onkeypress="return isAlphKey(event);">
							                    <span id="firstError" style="color:red; font-size:12px;"></span>
						                    </div>
                                        </div>
                                        <div class="col-sm-6">
                                        	<div class="form-group">
	                            				<label for="name">Last Name:</label>
							                    <input type="text" class="form-control" id="lastName" name="lastName" maxlength="30" placeholder="Enter Your LastName" autocomplete="off" onkeypress="return isAlphKey(event);">
							                    <span id="lastError" style="color:red; font-size:12px;"></span>
						                    </div>
                                        </div>
                                        <div class="col-sm-6">
                                        	<div class="form-group">
							                    <label for="email">Email address:</label>
							                    <input type="email" class="form-control" id="email" name="email" maxlength="50" placeholder="Enter Your Email" autocomplete="off" >
							                    <span id="mailError" style="color:red; font-size:12px;"></span>
						                    </div>
                                        </div>
				                        <div class="col-sm-6">
				                        	<div class="form-group">
							                    <label for="dob">Date of Birth:</label>
							                    <input type="text" class="form-control datepicker" id="datepicker" name="dateOfBirth" placeholder="Enter DOB" readonly="readonly" autocomplete="off">
							                	<span id="doberror" style="color:red; font-size:12px;"></span>
						                    </div>
				                        </div>
                                        <div class="col-sm-6">
                                        	<div class="form-group">
							                    <label for="phone">Phone:</label>
							                    <input type="text" class="form-control" id="phone" name="contactNo" placeholder="Enter Your Number"  maxlength="10" autocomplete="off" onkeypress="return isNumberKey(event)">
							                    <span id="ferror" style="color:red; font-size:12px;"></span>
						                    </div>
                                        </div>
					                    <div class="col-sm-6">
					                    	<!-- <div class="form-group">
					                        	<label for="pincode">PinCode:</label>
						                        <input type="text" class="form-control" id="pinCode" name="locationCode" placeholder="Enter Your PinCode" autocomplete="off"  maxlength="6" onkeypress="return isNumberKey(event)">
						                        <span id="codeError" style="color:red; font-size:12px;"></span>
					                       	</div> -->
					                       	<div class="form-group">
						                        <label for="pwd">Id Type:</label>
						                        <select id="idType" name="idType" class="form-control">
						                         	<option value="#">Select Id type </option>
						                            <option value="Aadhaar Card">Aadhaar Card </option>
						                            <option value="Pan Card">Pan Card</option>
						                            <option value="Voter Id">Voter Id</option>
						                            <option value="Passport">Passport</option>
						                        </select>
						                        <span id="idTyeError" style="color:red; font-size:12px;"></span>
					                        </div>
					                    </div>
					                          	<!-- <div class="form-group">
						                            <label for="pwd">Password:</label>
						                            <input type="password" class="form-control" id="password" name="password" placeholder="Enter Your Password" autocomplete="off" minlength="4" maxlength="6">
						                            <span id="cpassError" style="color:red; font-size: 12px;"></span>
				                        	  	</div>
				                        	  	<div class="form-group">
						                            <label for="pwd">Confirm Password:</label>
						                            <input type="password" class="form-control" id="cpassword" name="confirmPassword" placeholder="Enter Your Confirm Password" autocomplete="off" minlength="4" maxlength="6">
						                            <span id="passError" style="color:red; font-size: 12px;"></span>
				                        	  	</div> -->
				                       	<!-- <div class="col-sm-6">
				                       		<div class="form-group">
						                        <label for="pwd">Id Type:</label>
						                        <select id="idType" name="idType" class="form-control">
						                         	<option value="#">Select Id type </option>
						                            <option value="Aadhaar Card">Aadhaar Card </option>
						                            <option value="Pan Card">Pan Card</option>
						                            <option value="Voter Id">Voter Id</option>
						                            <option value="Passport">Passport</option>
						                        </select>
						                        <span id="idTyeError" style="color:red; font-size: 12px;"></span>
					                        </div>
				                       	</div> -->
					                    <div class="col-sm-6">
					                    	<div class="form-group">
							                    <label for="pwd">Id Number:</label>
							                    <input type="text" class="form-control" id="idNo" name="idNo" placeholder="Enter Your Id number" autocomplete="off"  maxlength="16">
							                    <span id="idNoError" style="color:red; font-size:12px;"></span>
						                    </div>
					                    </div>
                                   	</fieldset>
                                    <fieldset>
	                                    <legend>Upload Documents</legend>
	                                        <div class="col-sm-4">
	                                          	<div class="form-group">
		                                            <!-- <div class="fileUpload blue-btn btn width100">
		                                                <span>Upload Your Aadhaar Card Front Side</span>
		                                                <input type="file" class="uploadlogo" name="aadharImage1"  id="aadhar_Image1"/>
		                                            </div> -->
		                                            <label>Upload Aadhaar(Front Side)</label>
		                                            <input type="file" class="dropify" data-default-file="" data-allowed-file-extensions="png jpg jpeg" 
		                                            	data-max-file-size="2M" name="aadharImage1"/>
		                                            <span class="error" id="error_aaadharCardImg1"></span>
		                                        </div>
	                                        </div>
	                                        <div class="col-sm-4">
	                                          	<div class="form-group">
		                                            <!-- <div class="fileUpload blue-btn btn width100">
		                                                <span>Upload Your Aadhaar Card Back Side</span>
		                                                <input type="file" class="uploadlogo" name="aadharImage2"  id="aadhar_Image2"/>
		                                            </div> -->
		                                            <label>Upload Aadhaar(Back Side)</label>
		                                            <input type="file" class="dropify" data-default-file="" data-allowed-file-extensions="png jpg jpeg" 
		                                            	data-max-file-size="2M" name="aadharImage2"/>
		                                            <span class="error" id="error_aaadharCardImg2"></span>
		                                        </div>
	                                        </div>
	                                        <div class="col-sm-4">
	                                          	<div class="form-group">
		                                            <!-- <div class="fileUpload blue-btn btn width100">
		                                                <span>Upload Your PAN Card</span>
		                                                <input type="file" class="uploadlogo" name="panCardImage" id="pan_Image" />
		                                            </div> -->
		                                            <label>Upload PAN/Passport/Voter Card</label>
		                                            <input type="file" class="dropify" data-default-file="" data-allowed-file-extensions="png jpg jpeg" 
		                                            	data-max-file-size="2M" name="panCardImage"/>
		                                         	<span class="error" id="error_panCardImg"></span>
		                                        </div>
	                                            </div>
	                                        </fieldset>
	                                        <div class="cta-actions text-center">
				                              	<button class="btn signLog_btn" type="button" onclick="validateform()" id="register">CREATE ACCOUNT</button>
				                          	</div>
									</form>								
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script> -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/corporate/assets/dropify/js/dropify.min.js"></script>

	<!-- dropify initialization -->
	<script>
		$('.dropify').dropify();
	</script>
	
	
	<script>
		function isNumberKey(evt){
		    var charCode = (evt.which) ? evt.which : evt.keyCode
		    return !(charCode > 31 && (charCode < 48 || charCode > 57));
		}	
		
		function isAlphNumberKey(evt){
		    var k = (evt.which) ? evt.which : evt.keyCode
		    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 190 || k == 188);
		}
		
		function isAlphKey(evt){
		    var charCode = (evt.which) ? evt.which : evt.keyCode
		    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
		}
		
		$("#idType").change(function(){
		    //alert("The text has been changed.");
		    var idNo  = $('#idNo').val();
	    	var idType  = $('#idType').val();
	    	if (idType == 'Aadhaar Card') {
	    		$('#idNo').attr('maxlength','12');
	    	}
	    	if (idType == 'Pan Card') {
	    		$('#idNo').attr('maxlength','10');
	    	}
	    	if (idType == 'Voter Id') {
	    		$('#idNo').attr('maxlength','10');
	    	}
		}); 
		
	
		
		function validateform(){
	    	var valid = true;
	    	var pattern = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum|in)";
	    	var emailPattern = "^[a-zA-Z0-9]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
	    	var passwordPattern = "[a-zA-z0-9]"; //pattern for password
	    	var aadharPattern = "[0-9]{12}";
	    	var panPattern= "[A-Z]{5}[0-9]{4}[A-Z]{1}";
	    	var voterCardPattern="[A-Z]{3}[0-9]{7}";
	    	var firstName=$("#firstName").val();
	    	var lastName  = $('#lastName').val();
	    	var contactNo  = $('#phone').val();
	    	var email = $('#email').val() ;
	    	var dateOfBirth  = $('#datepicker').val();
	    	//var password  = $('#password').val();
	    	//var pinCode  = $('#pinCode').val();
	    	var pinCodePattern = "[0-9]{6}";
	    	var idNo  = $('#idNo').val();
	    	var idType  = $('#idType').val();
	    	var contactNoPattern = "[7-9]{1}[0-9]{9}";
	    	//var cpassword = $('#cpassword').val();
	    	var passportPattern ="^[A-Z][0-9]{8}$";
	    	console.log("email:: "+email);
	    	
	    	if(firstName.length <= 0){
	    		$("#firstError").html("Please enter your first name");
	    		valid = false;
	    	}
	    	if(lastName.length <= 0){
	    		$("#lastError").html("Please enter your last name");
	    		valid = false;
	    	}
	    	
	    	if(contactNo.length <=0){
	    		$("#ferror").html("Please enter your contact no");
	    		valid = false;
	    	}
	    	
	    	
    		if(contactNo.length == 10) {
    			if(!contactNo.match(contactNoPattern)) {
	    			console.log("invalid contact no");
	    			$("#ferror").html("Please enter valid contact number");
		    		valid = false; 
	    		}
    		} else {
    			console.log("else invalid contact no block");
    			$("#ferror").html("Please enter 10 digit contact number");
	    		valid = false; 
    		}
	    
	    	
	    	
	    	if(email.length  <= 0){
	    		$("#mailError").html("Please enter your email Id ");
	    		valid = false; 
	    	}else if(!email.match(emailPattern)) {
	    		console.log("inside mail")
	    		$("#mailError").html("Enter valid email Id");
	    		valid = false;	
	    	}
	    	
	    	/* if(pinCode.length  <= 0){
	    		$("#codeError").html("Please enter pincode");
	    		valid = false; 
	    	}
	    	if(pinCode.length == 6) {
	    		if(!pinCode.match(pinCodePattern)) {
	    			$('#codeError').html("Please enter valid pincode");
	    		}
	    	} else {
	    		$('#codeError').html("Please enter 6 digit pincode");
	    	} */
	    	
	    	/* if(password.length  <= 0){
	    		$("#passError").html("Please enter password");
	    		valid = false; 
	    	} */
	    	
	    	if(dateOfBirth.length  <= 0){
	    		$("#doberror").html("Please select date of birth");
	    		valid = false; 
	    	} else if(!isValidDate(dateOfBirth)){
	    		console.log("wrong date format");
	    		valid = false;
	    		$("#doberror").html("Please choose the proper format.(yyyy-mm-dd)");
	    	}
	    	
	    	if(idNo.length  <= 0){
	    		$("#idNoError").html("Please enter id number");
	    		valid = false; 
	    	}
	    	
	    	if(idType == '#'){
	    		$("#idTyeError").html("Please select id type");
	    		valid = false; 
	    	}
	    	
	    	if (idType == 'Aadhaar Card') {
	    		if(idNo.length == 12) {
	    			if(!idNo.match(aadharPattern)) {
		    			console.log("invalid aadhar");
		    			$("#idNoError").html("Please enter valid aadhar card number");
			    		valid = false; 
		    		}
	    		} else {
	    			console.log("else aadhar block");
	    			$("#idNoError").html("Please enter 12 digit aadhar card number");
		    		valid = false; 
	    		}
	    	}
	    	
	    	if (idType == 'Pan Card') {
	    		if(idNo.length == 10) {
	    			if(!idNo.match(panPattern)) {
	    				console.log("invalid pan");
		    			$("#idNoError").html("Please enter valid pan card number");
			    		valid = false; 
		    		}
	    		} else {
	    			console.log("else pan card block");
	    			$("#idNoError").html("Please enter 10 digit pan card number");
		    		valid = false; 
	    		}
	    	}
	    	
	    	if (idType == 'Voter Id') {
	    		if(idNo.length == 10) {
	    			if(!idNo.match(voterCardPattern)) {
	    				console.log("invalid voter");
		    			$("#idNoError").html("Please enter valid voter card number");
			    		valid = false; 
		    		}
	    		} else {
	    			console.log("else voter card block");
	    			$("#idNoError").html("Please enter 10 digit voter card number");
		    		valid = false;
	    		}
	    	}
	    	
	    	/* if(!isValidDate(dateOfBirth)){
	    		console.log("wrong date format");
	    		valid = false;
	    		$("#doberror").html("Please choose the proper format.");
	    	} */
	    	
	    	/* if (idType == 'Passport') {
	    		if(idNo.length == 9) {
	    			if(!idNo.match(passportPattern)) {
	    				console.log("invalid passport if");
		    			$("#idNoError").html("Please enter valid passport number");
			    		valid = false; 
		    		} 
	    		} else {
	    			console.log("invalid passport else");
	    			$("#idNoError").html("Please enter 9 digit passport number");
		    		valid = false;
	    		}
	    		
	    	} */
	    	/* if(cpassword.length <= 0){
		    	$("#cpassError").html("Please enter your confirm password");
		    	valid = false;
		    } */
	    	
	    	/* if(password != cpassword) {
	    		$("#cpassError").html("Password and Confirm Password does not match");
		    	valid = false;
	    	} */
	    	
	    	var today = new Date();
	    	var dd = today.getDate();
	    	var mm = today.getMonth()+1;
	    	var yyyy = today.getFullYear();

	    	today = yyyy+'-'+"0"+mm+'-'+dd;
	    	console.log("current date:::::"+today);
	    	if(new Date(dateOfBirth) >= new Date("1998-12-31")) {
				$("#doberror").html("You must be at least 18 years old to sign up");
				valid = false;
	    	}
	    	
	    	console.log("valid: "+valid);

	    if(valid == true) {
	    	$("#formId").submit();
	    } 
	    
	    var timeout = setTimeout(function(){
	        $("#firstError").html("");
	    	$("#lastError").html("");
	    	$("#ferror").html("");
	    	$("#mailError").html("");
	    	$("#codeError").html("");
	    	$("#passError").html("");
	    	$("#doberror").html("");
	    	$("#idTyeError").html("");
	    	$("#idNoError").html("");
	    	$("#cpassError").html("");
	    }, 3000);
	    }
		
		function isValidDate(dateString) {
			if(dateString.length>0){
				  var regEx = /^\d{4}-\d{2}-\d{2}$/;
				  if(!dateString.match(regEx)) return false;  // Invalid format
				  var d = new Date(dateString);
				  if(!d.getTime() && d.getTime() !== 0) return false; // Invalid date
				  return d.toISOString().slice(0,10) === dateString;
			}
		}
		
	</script>
	
	<script>
	     var today, datepicker;
	     today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
	     datepicker = $('#datepicker').datepicker({
	     uiLibrary: 'bootstrap4',
	     format: 'yyyy-mm-dd',
	     maxDate: today
     });
 	</script>
	
	
	
</body>
</html>