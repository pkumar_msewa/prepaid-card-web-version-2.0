// scripts for landing page

// initialize slick slider
$(document).ready(function(){
	  $('.offers_banners').slick({
	    slidesToShow: 1,
	  	slidesToScroll: 1,
	  	autoplay: true,
	  	autoplaySpeed: 2000,
	  	arrows: false,
	  });
	});

//script for LoadMore 

$(function () {
    $(".singl_item").slice(0, 4).show();
    $("#loadMore").on('click', function (e) {
        e.preventDefault();
        $(".singl_item:hidden").slice(0, 4).slideDown();
      var $this = $('.btn');
  $this.button('loading');
        if ($(".singl_item:hidden").length == 0) {
            $("#load").fadeOut('slow');
          $this.button('reset');
        }
    });
});

// smooth scroll script
$(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 1000, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});

// testimonial slider

$('.slide').slick({
  centerMode: true,
  centerPadding: '60px',
  slidesToShow: 3,
  autoplay: true,
  autoplaySpeed: 2000,
  arrows: false,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 3
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    }
  ]
});

// scroll to top

// ===== Scroll to Top ==== 
$(window).scroll(function() {
    if ($(this).scrollTop() >= 200) {    // If page is scrolled more than 50px
        $('#top').fadeIn("fast");       // Fade in the arrow
    } else {
        $('#top').fadeOut("fast");      // Else fade out the arrow
    }
});
$('#top').click(function() {            // When arrow is clicked
    $('body,html').animate({
        scrollTop : 0                   // Scroll to top of body
    }, 500);
});


// form validate
(function ($) {
    "use strict";

    
    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit',function(){
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }

        return check;
    });


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else {
            if($(input).val().trim() == ''){
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }
    
    

})(jQuery);

// Tablet view responsive

$(document).ready(function(){
    if ($(window).width() <= 768){  
      // remove container class 
      // console.log("hi");
      $("#hero-image").removeClass("container");
    } 
    else {
      $("#hero-image").addClass("container");
    }
});

$(window).resize(function(){
    if ($(window).width() <= 768){  
      // remove container class 
      // console.log("hello");
      $("#hero-image").removeClass("container");
    } else {
      $("#hero-image").addClass("container");
    }
  });

// Mobile view

$(document).ready(function(){
    if ($(window).width() <= 425){  
      // remove container class 
      // console.log("hi");
      $("#copyR").removeClass("text-right");
    } 
    else {
      $("#copyR").addClass("text-right");
    }
});

$(window).resize(function(){
    if ($(window).width() <= 425){  
      // remove container class 
      // console.log("hello");
      $("#copyR").removeClass("text-right");
    } else {
      $("#copyR").addClass("text-right");
    }
  });