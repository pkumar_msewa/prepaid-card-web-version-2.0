$(document).ready(function(){	
	$.ajax({
		type:"GET",
		url:contextPath+"/Group/GetAssignService",		
		success:function(response) {
			console.log(response);
			console.log(response.details);	
			$("#GSRL").hide(); // group request list
			$("#GSAL").hide(); // group approved list
			$("#GSRJ").hide(); // group rejected list
			$("#BLR").hide(); // corporate bulk register
			$("#GBLR").hide(); // group bulk register
			$("#BCL").hide(); // corporate bulk card load
			$("#BKY").hide(); // corporate bulk KYC
			$("#BSMS").hide(); // bulk SMS				
			$("#CAP").hide(); // add corporate partner
			$("#CLP").hide(); // list corporate partner			
			$("#CBCL").hide(); // blocked card report
			$("#CFLL").hide(); // failed load report
			$("#CFR").hide(); // failed registration report
			$("#CUR").hide(); // user report
			$("#CPH").hide(); // prefund history report
			$("#CSCA").hide(); // corporate single card assign
			$("#CSCL").hide();	// corporate single card load
			$("#CBLR").hide(); // bulk card load report
			$("#CPR").hide(); // corporate prefund request
			$("#NOTC").hide(); // push notification
			$("#CRD").hide(); //user report
			$("#corppart").hide();
			$("#GBCA").hide();

			var arrayList = response.details;
			if(response.details.length != 0) {
				for(var i in arrayList) {
					console.log("i::code = "+response.details[i].code);		
					if(response.details[i].code == 'CUR' || response.details[i].code == 'CPH' || response.details[i].code == 'CFR'
					|| response.details[i].code == 'CBLR' || response.details[i].code == 'CFLL' || response.details[i].code == 'CBCL') {
						$("#CRD").show();
					} 
					if(response.details[i].code == 'CAP' || response.details[i].code == 'CLP') {
						$("#corppart").show();
					}
					$("#"+response.details[i].code+"").show();
				}
			} else {
				$("#groupServiceId").hide();
				$("#corppart").hide();
				$("#CRD").hide();	
				$("#GSRL").hide();
				$("#GSAL").hide();
				$("#GSRJ").hide();
				$("#BLR").hide();
				$("#GBLR").hide();
				$("#BCL").hide();
				$("#BKY").hide();
				$("#BSMS").hide();					
				$("#CAP").hide();
				$("#CLP").hide();				
				$("#CBCL").hide();
				$("#CFLL").hide();
				$("#CBLR").hide();
				$("#CFR").hide();
				$("#CUR").hide();
				$("#CPH").hide();
				$("#CSCA").hide();
				$("#CSCL").hide();	
				$("#CPR").hide();
				$("#NOTC").hide();
				$("#GBCA").hide();
				console.log("empty list");				
			}
		}
	});
});