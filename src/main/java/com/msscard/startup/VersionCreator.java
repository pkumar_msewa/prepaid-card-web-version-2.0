package com.msscard.startup;

import com.msscard.entity.ApiVersion;
import com.msscard.entity.VersionLogs;
import com.msscard.repositories.ApiVersionRepository;
import com.msscard.repositories.VersionLogsRepository;
import com.msscard.util.SecurityUtil;

public class VersionCreator {

	private final ApiVersionRepository apiVersionRepository;
	private final VersionLogsRepository versionLogsRepository;
	
	
	public VersionCreator(ApiVersionRepository apiVersionRepository, VersionLogsRepository versionLogsRepository) {
		super();
		this.apiVersionRepository = apiVersionRepository;
		this.versionLogsRepository = versionLogsRepository;
	}


		public void create(){
			ApiVersion apiLogs=new ApiVersion();
			apiLogs.setName("Android");
			apiLogs.setApiStatus("Active");
			apiLogs.setApiVersion("v1");
			try {
				apiLogs.setApiKey(SecurityUtil.md5("7022620747"));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			apiVersionRepository.save(apiLogs);
			
			ApiVersion apiLogs1=new ApiVersion();
			apiLogs1.setName("IOS");
			apiLogs1.setApiStatus("Active");
			apiLogs1.setApiVersion("v1");
			try {
				apiLogs1.setApiKey(SecurityUtil.md5("8895093779"));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			apiVersionRepository.save(apiLogs1);
			
			VersionLogs version=new VersionLogs();
			version.setVersion("1.0");
			version.setStatus("Active");
			ApiVersion clientApilogs=apiVersionRepository.getByNameStatus("Android", "Active");
			version.setAndroidVersion(clientApilogs);
			versionLogsRepository.save(version);
	}
}
