package com.msscard.startup;

import java.io.FileOutputStream;
import java.io.StringReader;

import javax.ws.rs.core.MultivaluedMap;

import org.json.JSONException;
import org.json.JSONObject;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.msscard.app.model.response.PGStatus;
import com.msscard.app.model.response.PGStatusCheckDTO;
import com.razorpay.constants.RazorPayConstants;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class TestReceipt {

	public static void main(String[] args) {
		 new TestReceipt().checkPGStatus("1547561720590C","T001147066677");

	}
	public PGStatusCheckDTO checkPGStatus(String originalTxnNo,String retreivalRefNo){
		PGStatusCheckDTO statusDto=new PGStatusCheckDTO();
		String transactionType="STATUS";
		Client client=Client.create();
		client.addFilter(new LoggingFilter(System.out));
		MultivaluedMap<String,String> multiMap=new MultivaluedMapImpl();
		multiMap.add("merchantID",RazorPayConstants.MERCHANT_ID);
		multiMap.add("merchantTxnNo", originalTxnNo);
		multiMap.add("originalTxnNo", retreivalRefNo);
		multiMap.add("transactionType",transactionType);
		multiMap.add("secureHash",RazorPayConstants.composeMessageStatusCheck(RazorPayConstants.MERCHANT_ID, originalTxnNo,retreivalRefNo, transactionType));
		ClientResponse clientResponse=client.resource(RazorPayConstants.STATUS_CHECK).header("Content-Type", "application/x-www-form-urlencoded").
		post(ClientResponse.class,multiMap);
		String strResponse=clientResponse.getEntity(String.class);
		System.err.println(strResponse);
		try {
			JSONObject statusCheck=new JSONObject(strResponse);
			if(clientResponse.getStatus()==200){
			if(statusCheck!=null){
				String responseCode=statusCheck.getString("responseCode");
				String respDescription=statusCheck.getString("respDescription");
				String txnStatus=statusCheck.getString("txnStatus");
				String txnResponseCode=statusCheck.getString("txnResponseCode");
				String txnRespDescription=statusCheck.getString("txnID");
				String txnID=statusCheck.getString("txnID");
				String txnAuthID=statusCheck.getString("txnAuthID");
				String secureHash=statusCheck.getString("secureHash");
				String amount=statusCheck.getString("amount");
				statusDto.setResponseCode(txnResponseCode);
				statusDto.setRespDescription(respDescription);
				statusDto.setPgStatus(PGStatus.valueOf(txnStatus));
				statusDto.setSecureHash(secureHash);
				statusDto.setTxnAuthID(txnAuthID);
				statusDto.setRespDescription(txnRespDescription);
				statusDto.setTxnID(txnID);
				statusDto.setAmount(amount);
				return statusDto;
			}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			statusDto.setResponseCode("F00");
			return statusDto;
		}
		statusDto.setResponseCode("F00");
		return statusDto;
		
		
	}
	
}