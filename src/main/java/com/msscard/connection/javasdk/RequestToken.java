package com.msscard.connection.javasdk;


import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.Map;
import java.util.HashMap;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;

import org.json.JSONException;

import com.msscard.connection.javasdk.HttpRequest.HttpRequestException;


public class RequestToken extends Token {

	private String method = HttpRequest.METHOD_POST;
	private String api = "oauth/request/token";
	
	public RequestToken (String host, String key, String secret, String user, String password)
				throws
					NoSuchAlgorithmException,
					UnsupportedEncodingException,
					URISyntaxException,
					InvalidKeyException,
					NoSuchProviderException,
					NoSuchPaddingException,
					IllegalBlockSizeException,
					BadPaddingException,
					ShortBufferException,
					InvalidKeySpecException,
					InvalidAlgorithmParameterException,
					HttpRequestException,
					ResourceException,
					UnsupportedMethodException, JSONException {
			this.initialize(host, key, secret);
		
			String api = this.getHost() + "/" + this.api; 
		
			Map<String, String> data = new HashMap<String, String>();
			
			data.put("oauth_consumer_key", this.getKey());
			data.put("oauth_nonce", OAuth.nonce());
			data.put("oauth_signature_method", Signature.METHOD);
			data.put("oauth_timestamp", new Long(OAuth.timestamp()).toString());
			data.put("oauth_user_name", user);
			data.put("oauth_user_password", password);
			data.put("oauth_version", OAuth.VERSION);
			
			Signature signature = new Signature(this.getSecret(), this.method, api, data);
			
			data.put("oauth_signature", signature.getString());
			data.put("oauth_user_name", signature.encrypt(this.getSecret(), user));
			data.put("oauth_user_password", signature.encrypt(this.getSecret(), password));
			
			this.response = Connection.request(this.method, api, data);
	}
}