package com.msscard.connection.javasdk;

import org.json.JSONObject;

import com.msscard.util.MatchMoveUtil;


public class Token {
	private String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
	private String key = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
	private String secret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
	
	protected JSONObject response = null;
	
	protected void initialize (String host, String key, String secret) {
		this.host = host;
		this.key = key;
		this.secret = secret;
	}
	
	public JSONObject getResponse() {
		return this.response;
	}
	
	public String getHost() {
		return this.host;
	}
	
	public String getKey() {
		return this.key;
	}
	
	public String getSecret() {
		return this.secret;
	}
}
