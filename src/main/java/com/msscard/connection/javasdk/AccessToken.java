package com.msscard.connection.javasdk;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.json.JSONException;
import org.json.JSONObject;

import com.msscard.connection.javasdk.HttpRequest.HttpRequestException;



public class AccessToken extends Token {

	private String method = HttpRequest.METHOD_POST;
	private String api = "oauth/access/token";
	
	public AccessToken (String host, String key, String secret) {
		this.initialize(host, key, secret);
		this.response = null;
	}
	
	public AccessToken (AccessSession session, String user)
			throws UnsupportedEncodingException,
				NoSuchAlgorithmException, JSONException {
		this.initialize(session.getHost(), session.getKey(), session.getSecret());
		
		String cache = session.get(user);
		
		this.response = null == cache ? null: new JSONObject(cache);
	}
	
	public AccessToken (RequestToken requestToken)
			throws 
				NoSuchAlgorithmException,
				UnsupportedEncodingException,
				InvalidKeyException,
				NoSuchProviderException,
				HttpRequestException,
				ResourceException,
				UnsupportedMethodException, JSONException {
		this.initialize(requestToken.getHost(), requestToken.getKey(), requestToken.getSecret());
		
		String api = this.getHost() + "/" + this.api;
		Map<String, String> data = new HashMap<String, String>();
		
		JSONObject oauthToken = requestToken.getResponse();
		if (oauthToken!=null && oauthToken.has("oauth_token")) {
		data.put("oauth_consumer_key", this.getKey());
		data.put("oauth_nonce", OAuth.nonce());
		data.put("oauth_signature_method", Signature.METHOD);
		data.put("oauth_timestamp", new Long(OAuth.timestamp()).toString());
		data.put("oauth_token", oauthToken.getString("oauth_token"));
		data.put("oauth_version", OAuth.VERSION);
		Signature signature = new Signature(this.getSecret() + "&" + oauthToken.getString("oauth_token_secret"), this.method, api, data);
		data.put("oauth_signature", signature.getString());
		}
		this.response = Connection.request(this.method, api, data);
	}

	public JSONObject consume (String resource)
			throws InvalidKeyException,
				NoSuchAlgorithmException,
				UnsupportedEncodingException,
				NoSuchProviderException,
				JSONException,
				HttpRequestException,
				ResourceException,
				UnsupportedMethodException,
				NoSuchPaddingException,
				IllegalBlockSizeException,
				BadPaddingException {
		return this.consume(resource, HttpRequest.METHOD_GET, null);
	}
	
	public JSONObject consume (String resource, String method)
			throws InvalidKeyException,
				NoSuchAlgorithmException,
				UnsupportedEncodingException,
				NoSuchProviderException,
				JSONException,
				HttpRequestException,
				ResourceException,
				UnsupportedMethodException,
				NoSuchPaddingException,
				IllegalBlockSizeException,
				BadPaddingException {
		return this.consume(resource, method, null);
	}
	
	public JSONObject consume (String resource, String method, Map<String, String> query)
			throws NoSuchAlgorithmException,
				UnsupportedEncodingException,
				InvalidKeyException,
				NoSuchProviderException,
				JSONException,
				HttpRequestException,
				ResourceException,
				UnsupportedMethodException,
				NoSuchPaddingException,
				IllegalBlockSizeException,
				BadPaddingException {
		String signatureKey = this.getSecret();
		String api = this.getHost() + "/" + resource;
		Map<String, String> data = new HashMap<String, String>();
		
		data.put("oauth_consumer_key", this.getKey());
		data.put("oauth_nonce", OAuth.nonce());
		data.put("oauth_signature_method", Signature.METHOD);
		data.put("oauth_timestamp", new Long(OAuth.timestamp()).toString());
		data.put("oauth_version", OAuth.VERSION);
		
		JSONObject oauthToken = this.getResponse();
		System.out.println(oauthToken);
		String strQuery = null;
		
		if (oauthToken != null) {
			signatureKey = signatureKey + "&" + oauthToken.getString("oauth_token_secret");
			data.put("oauth_token", oauthToken.getString("oauth_token"));

			if (null != query)
			{
				data.putAll(query);
			}
		} else if (null != query) {
			strQuery = MapUtil.mapToString(query);
			data.put("_d",strQuery);
		}
		
		Signature signature = new Signature(signatureKey, method, api, data);
		
		data.put("oauth_signature", signature.getString());
		
		if (null != strQuery) {
			data.put("_d", signature.encrypt(this.getSecret(), strQuery));
		}
		
		return Connection.request(method, api, data);
	}
}
