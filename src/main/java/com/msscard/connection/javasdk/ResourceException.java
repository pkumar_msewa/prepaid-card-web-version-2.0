package com.msscard.connection.javasdk;


public class ResourceException extends Exception {

	private static final long serialVersionUID = 8015516139565523098L;

	private int code = 0;
	private String message = "";
	private HttpRequest http = null;
	
	public ResourceException (int code, String message, HttpRequest http) {
		this.code = code;
		this.message = message;
		this.http = http;
	}
	
	public String getMessage() {
		return this.message;
	}
	
	public int getCode() {
		return this.code;
	}
	
	public HttpRequest getHttpRequest() {
		return this.http;
	}
}
