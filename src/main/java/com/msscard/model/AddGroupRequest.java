package com.msscard.model;

import org.springframework.web.multipart.MultipartFile;

public class AddGroupRequest {
	
	private boolean bulkRegister;
	private boolean bulkcardload;
	private boolean bulkkyc;
	private boolean sincarload;
	private boolean singcarassign;
	private boolean userReport;
	private boolean prefundHistory;
	private boolean failedReg;
	private boolean failedLoad;
	private boolean blockedCard;
	private boolean bulkSms;
	private boolean noticenter;
	private boolean listPart;
	private boolean addPartner;
	private boolean changePass;
	private String groupName;
	private String displayname;
	private String mobile;
	private String email;
	private boolean corporate;
	private boolean groupRequestList;
	private boolean groupAcceptList;
	private boolean groupRejectList;
	private boolean bulkCardLoadList;
	private boolean groupBulkRegister;
	private boolean prefundRequest;
	private MultipartFile image;
	private boolean groupBulkRegisterFail;
	
	
	public boolean isGroupBulkRegisterFail() {
		return groupBulkRegisterFail;
	}
	public void setGroupBulkRegisterFail(boolean groupBulkRegisterFail) {
		this.groupBulkRegisterFail = groupBulkRegisterFail;
	}
	public boolean isPrefundRequest() {
		return prefundRequest;
	}
	public void setPrefundRequest(boolean prefundRequest) {
		this.prefundRequest = prefundRequest;
	}
	public boolean isGroupBulkRegister() {
		return groupBulkRegister;
	}
	public void setGroupBulkRegister(boolean groupBulkRegister) {
		this.groupBulkRegister = groupBulkRegister;
	}
	public boolean isBulkCardLoadList() {
		return bulkCardLoadList;
	}
	public void setBulkCardLoadList(boolean bulkCardLoadList) {
		this.bulkCardLoadList = bulkCardLoadList;
	}
	public boolean isGroupRequestList() {
		return groupRequestList;
	}
	public void setGroupRequestList(boolean groupRequestList) {
		this.groupRequestList = groupRequestList;
	}
	public boolean isGroupAcceptList() {
		return groupAcceptList;
	}
	public void setGroupAcceptList(boolean groupAcceptList) {
		this.groupAcceptList = groupAcceptList;
	}
	public boolean isGroupRejectList() {
		return groupRejectList;
	}
	public void setGroupRejectList(boolean groupRejectList) {
		this.groupRejectList = groupRejectList;
	}
	public boolean isCorporate() {
		return corporate;
	}
	public void setCorporate(boolean corporate) {
		this.corporate = corporate;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getDisplayname() {
		return displayname;
	}
	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public MultipartFile getImage() {
		return image;
	}
	public void setImage(MultipartFile image) {
		this.image = image;
	}
	public boolean isBulkRegister() {
		return bulkRegister;
	}
	public void setBulkRegister(boolean bulkRegister) {
		this.bulkRegister = bulkRegister;
	}
	public boolean isBulkcardload() {
		return bulkcardload;
	}
	public void setBulkcardload(boolean bulkcardload) {
		this.bulkcardload = bulkcardload;
	}
	public boolean isBulkkyc() {
		return bulkkyc;
	}
	public void setBulkkyc(boolean bulkkyc) {
		this.bulkkyc = bulkkyc;
	}
	public boolean isSincarload() {
		return sincarload;
	}
	public void setSincarload(boolean sincarload) {
		this.sincarload = sincarload;
	}
	public boolean isSingcarassign() {
		return singcarassign;
	}
	public void setSingcarassign(boolean singcarassign) {
		this.singcarassign = singcarassign;
	}
	public boolean isUserReport() {
		return userReport;
	}
	public void setUserReport(boolean userReport) {
		this.userReport = userReport;
	}
	public boolean isPrefundHistory() {
		return prefundHistory;
	}
	public void setPrefundHistory(boolean prefundHistory) {
		this.prefundHistory = prefundHistory;
	}
	public boolean isFailedReg() {
		return failedReg;
	}
	public void setFailedReg(boolean failedReg) {
		this.failedReg = failedReg;
	}
	public boolean isFailedLoad() {
		return failedLoad;
	}
	public void setFailedLoad(boolean failedLoad) {
		this.failedLoad = failedLoad;
	}
	public boolean isBlockedCard() {
		return blockedCard;
	}
	public void setBlockedCard(boolean blockedCard) {
		this.blockedCard = blockedCard;
	}
	public boolean isBulkSms() {
		return bulkSms;
	}
	public void setBulkSms(boolean bulkSms) {
		this.bulkSms = bulkSms;
	}
	public boolean isNoticenter() {
		return noticenter;
	}
	public void setNoticenter(boolean noticenter) {
		this.noticenter = noticenter;
	}
	public boolean isListPart() {
		return listPart;
	}
	public void setListPart(boolean listPart) {
		this.listPart = listPart;
	}
	public boolean isAddPartner() {
		return addPartner;
	}
	public void setAddPartner(boolean addPartner) {
		this.addPartner = addPartner;
	}
	public boolean isChangePass() {
		return changePass;
	}
	public void setChangePass(boolean changePass) {
		this.changePass = changePass;
	}
	
	
}
