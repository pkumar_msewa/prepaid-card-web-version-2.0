package com.msscard.model;

public class FlightResponseEmail {

	private String journeyTime;
	private String airlineName;
	private String arrivalDate;
	private String arrivalTime;
	private String cabin;
	private String baggageUnit;
	private String baggageWeight;
	private String departureDate;
	private String departureTime;
	private String duration;
	private String destination;
	private String origin;
	private String flightNumber;
	
	private String journeyTimereturn;
	private String airlineNamereturn;
	private String arrivalDatereturn;
	private String arrivalTimereturn;
	private String cabinreturn;
	private String baggageUnitreturn;
	private String baggageWeightreturn;
	private String departureDatereturn;
	private String departureTimereturn;
	private String durationreturn;
	private String destinationreturn;
	private String originreturn;
	private String flightNumberreturn;
	private FlightTicketdtoForname passengerDetails;
	
	public String getJourneyTime() {
		return journeyTime;
	}
	public void setJourneyTime(String journeyTime) {
		this.journeyTime = journeyTime;
	}
	public String getAirlineName() {
		return airlineName;
	}
	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}
	public String getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public String getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public String getCabin() {
		return cabin;
	}
	public void setCabin(String cabin) {
		this.cabin = cabin;
	}
	public String getBaggageUnit() {
		return baggageUnit;
	}
	public void setBaggageUnit(String baggageUnit) {
		this.baggageUnit = baggageUnit;
	}
	public String getBaggageWeight() {
		return baggageWeight;
	}
	public void setBaggageWeight(String baggageWeight) {
		this.baggageWeight = baggageWeight;
	}
	public String getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}
	public String getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	public String getJourneyTimereturn() {
		return journeyTimereturn;
	}
	public void setJourneyTimereturn(String journeyTimereturn) {
		this.journeyTimereturn = journeyTimereturn;
	}
	public String getAirlineNamereturn() {
		return airlineNamereturn;
	}
	public void setAirlineNamereturn(String airlineNamereturn) {
		this.airlineNamereturn = airlineNamereturn;
	}
	public String getArrivalDatereturn() {
		return arrivalDatereturn;
	}
	public void setArrivalDatereturn(String arrivalDatereturn) {
		this.arrivalDatereturn = arrivalDatereturn;
	}
	public String getArrivalTimereturn() {
		return arrivalTimereturn;
	}
	public void setArrivalTimereturn(String arrivalTimereturn) {
		this.arrivalTimereturn = arrivalTimereturn;
	}
	public String getCabinreturn() {
		return cabinreturn;
	}
	public void setCabinreturn(String cabinreturn) {
		this.cabinreturn = cabinreturn;
	}
	public String getBaggageUnitreturn() {
		return baggageUnitreturn;
	}
	public void setBaggageUnitreturn(String baggageUnitreturn) {
		this.baggageUnitreturn = baggageUnitreturn;
	}
	public String getBaggageWeightreturn() {
		return baggageWeightreturn;
	}
	public void setBaggageWeightreturn(String baggageWeightreturn) {
		this.baggageWeightreturn = baggageWeightreturn;
	}
	public String getDepartureDatereturn() {
		return departureDatereturn;
	}
	public void setDepartureDatereturn(String departureDatereturn) {
		this.departureDatereturn = departureDatereturn;
	}
	public String getDepartureTimereturn() {
		return departureTimereturn;
	}
	public void setDepartureTimereturn(String departureTimereturn) {
		this.departureTimereturn = departureTimereturn;
	}
	public String getDurationreturn() {
		return durationreturn;
	}
	public void setDurationreturn(String durationreturn) {
		this.durationreturn = durationreturn;
	}
	public String getDestinationreturn() {
		return destinationreturn;
	}
	public void setDestinationreturn(String destinationreturn) {
		this.destinationreturn = destinationreturn;
	}
	public String getOriginreturn() {
		return originreturn;
	}
	public void setOriginreturn(String originreturn) {
		this.originreturn = originreturn;
	}
	public String getFlightNumberreturn() {
		return flightNumberreturn;
	}
	public void setFlightNumberreturn(String flightNumberreturn) {
		this.flightNumberreturn = flightNumberreturn;
	}
	public FlightTicketdtoForname getPassengerDetails() {
		return passengerDetails;
	}
	public void setPassengerDetails(FlightTicketdtoForname passengerDetails) {
		this.passengerDetails = passengerDetails;
	}
	
	
}
