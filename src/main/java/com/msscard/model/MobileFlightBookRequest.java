package com.msscard.model;

import java.util.List;

public class MobileFlightBookRequest {

	private String boundType;
	private String baggageFare;
	private String ssrFare;
	private String itineraryKey;
	private String journeyTime;
	private String addOnDetail;

	private String aircraftCode;
	private String aircraftType;
	private String airlineName;
	private String amount;
	private String arrivalDate;
	private String arrivalTerminal;
	private String arrivalTime;
	private String availableSeat;
	private String baggageUnit;
	private String baggageWeight;
	private String boundTypes;
	private String cabin;

	private String capacity;
	private String carrierCode;
	private String currencyCode;
	private String departureDate;
	private String departureTerminal;
	private String departureTime;
	private String destination;
	private String duration;
	private String fareBasisCode;
	private String fareClassOfService;
	private String flightDesignator;
	private String flightDetailRefKey;
	private String flightName;
	private String flightNumber;
	private String group;
	private String connecting;
	private String numberOfStops;
	private String origin;
	private String providerCode;
	private String remarks;
	private String sold;
	private String status;

	private String deeplink;
	private String fareIndicator;
	private String fareRule;

	private String cache;
	private String holdBooking;
	private String international;
	private String roundTrip;
	private String special;
	private String specialId;

	private String journeyIndex;
	private String memoryCreationTime;
	private String nearByAirport;
	private String remark;
	private String searchId;
	private String engineID;

	private String basicFare;
	private String exchangeRate;
	private String totalFareWithOutMarkUp;
	private String totalTaxWithOutMarkUp;

	private String baseTransactionAmount;
	private String cancelPenalty;
	private String changePenalty;
	private String equivCurrencyCode;

	private String fareInfoKey;
	private String fareInfoValue;
	private String markUP;
	private String paxType;
	private String refundable;
	private String totalFare;
	private String totalTax;
	private String transactionAmount;

	private String beginDate;

	private String endDate;

	private String bookingAmount;
	private String bookingCurrencyCode;

	private String address1;
	private String address2;
	private String city;
	private String countryCode;
	private String cultureCode;
	private String dateofBirth;
	private String emailAddress;
	private String firstName;
	private String frequentFlierNumber;
	private String gender;
	private String homePhone;
	private String lastName;
	private String meal;
	private String middleName;
	private String mobileNumber;
	private String nationality;
	private String passportExpiryDate;
	private String passportNo;

	private String provisionState;
	private String residentCountry;
	private String title;

	private String visatype;
	private String traceId;
	private String transactionId;
	private String txnRefNo;
	private String androidBooking;
	private List<BookFare> bookFares;
	private String domestic;
	
	private String sessionId;
	private String grandtotal;
	private String adults;
	private String childs;
	private String infants;
	private String merchantRefNo;
	
	List<TravellerFlightDetails> travellerDetails;
	
	private String ticketDetails;
	private String firstNamechild;
	private String lastNamechild;
	
	private String firstNameinfant;
	private String lastNameinfant;
	public String getBoundType() {
		return boundType;
	}
	public void setBoundType(String boundType) {
		this.boundType = boundType;
	}
	public String getBaggageFare() {
		return baggageFare;
	}
	public void setBaggageFare(String baggageFare) {
		this.baggageFare = baggageFare;
	}
	public String getSsrFare() {
		return ssrFare;
	}
	public void setSsrFare(String ssrFare) {
		this.ssrFare = ssrFare;
	}
	public String getItineraryKey() {
		return itineraryKey;
	}
	public void setItineraryKey(String itineraryKey) {
		this.itineraryKey = itineraryKey;
	}
	public String getJourneyTime() {
		return journeyTime;
	}
	public void setJourneyTime(String journeyTime) {
		this.journeyTime = journeyTime;
	}
	public String getAddOnDetail() {
		return addOnDetail;
	}
	public void setAddOnDetail(String addOnDetail) {
		this.addOnDetail = addOnDetail;
	}
	public String getAircraftCode() {
		return aircraftCode;
	}
	public void setAircraftCode(String aircraftCode) {
		this.aircraftCode = aircraftCode;
	}
	public String getAircraftType() {
		return aircraftType;
	}
	public void setAircraftType(String aircraftType) {
		this.aircraftType = aircraftType;
	}
	public String getAirlineName() {
		return airlineName;
	}
	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public String getArrivalTerminal() {
		return arrivalTerminal;
	}
	public void setArrivalTerminal(String arrivalTerminal) {
		this.arrivalTerminal = arrivalTerminal;
	}
	public String getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public String getAvailableSeat() {
		return availableSeat;
	}
	public void setAvailableSeat(String availableSeat) {
		this.availableSeat = availableSeat;
	}
	public String getBaggageUnit() {
		return baggageUnit;
	}
	public void setBaggageUnit(String baggageUnit) {
		this.baggageUnit = baggageUnit;
	}
	public String getBaggageWeight() {
		return baggageWeight;
	}
	public void setBaggageWeight(String baggageWeight) {
		this.baggageWeight = baggageWeight;
	}
	public String getBoundTypes() {
		return boundTypes;
	}
	public void setBoundTypes(String boundTypes) {
		this.boundTypes = boundTypes;
	}
	public String getCabin() {
		return cabin;
	}
	public void setCabin(String cabin) {
		this.cabin = cabin;
	}
	public String getCapacity() {
		return capacity;
	}
	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}
	public String getCarrierCode() {
		return carrierCode;
	}
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}
	public String getDepartureTerminal() {
		return departureTerminal;
	}
	public void setDepartureTerminal(String departureTerminal) {
		this.departureTerminal = departureTerminal;
	}
	public String getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getFareBasisCode() {
		return fareBasisCode;
	}
	public void setFareBasisCode(String fareBasisCode) {
		this.fareBasisCode = fareBasisCode;
	}
	public String getFareClassOfService() {
		return fareClassOfService;
	}
	public void setFareClassOfService(String fareClassOfService) {
		this.fareClassOfService = fareClassOfService;
	}
	public String getFlightDesignator() {
		return flightDesignator;
	}
	public void setFlightDesignator(String flightDesignator) {
		this.flightDesignator = flightDesignator;
	}
	public String getFlightDetailRefKey() {
		return flightDetailRefKey;
	}
	public void setFlightDetailRefKey(String flightDetailRefKey) {
		this.flightDetailRefKey = flightDetailRefKey;
	}
	public String getFlightName() {
		return flightName;
	}
	public void setFlightName(String flightName) {
		this.flightName = flightName;
	}
	public String getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getConnecting() {
		return connecting;
	}
	public void setConnecting(String connecting) {
		this.connecting = connecting;
	}
	public String getNumberOfStops() {
		return numberOfStops;
	}
	public void setNumberOfStops(String numberOfStops) {
		this.numberOfStops = numberOfStops;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getProviderCode() {
		return providerCode;
	}
	public void setProviderCode(String providerCode) {
		this.providerCode = providerCode;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getSold() {
		return sold;
	}
	public void setSold(String sold) {
		this.sold = sold;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDeeplink() {
		return deeplink;
	}
	public void setDeeplink(String deeplink) {
		this.deeplink = deeplink;
	}
	public String getFareIndicator() {
		return fareIndicator;
	}
	public void setFareIndicator(String fareIndicator) {
		this.fareIndicator = fareIndicator;
	}
	public String getFareRule() {
		return fareRule;
	}
	public void setFareRule(String fareRule) {
		this.fareRule = fareRule;
	}
	public String getCache() {
		return cache;
	}
	public void setCache(String cache) {
		this.cache = cache;
	}
	public String getHoldBooking() {
		return holdBooking;
	}
	public void setHoldBooking(String holdBooking) {
		this.holdBooking = holdBooking;
	}
	public String getInternational() {
		return international;
	}
	public void setInternational(String international) {
		this.international = international;
	}
	public String getRoundTrip() {
		return roundTrip;
	}
	public void setRoundTrip(String roundTrip) {
		this.roundTrip = roundTrip;
	}
	public String getSpecial() {
		return special;
	}
	public void setSpecial(String special) {
		this.special = special;
	}
	public String getSpecialId() {
		return specialId;
	}
	public void setSpecialId(String specialId) {
		this.specialId = specialId;
	}
	public String getJourneyIndex() {
		return journeyIndex;
	}
	public void setJourneyIndex(String journeyIndex) {
		this.journeyIndex = journeyIndex;
	}
	public String getMemoryCreationTime() {
		return memoryCreationTime;
	}
	public void setMemoryCreationTime(String memoryCreationTime) {
		this.memoryCreationTime = memoryCreationTime;
	}
	public String getNearByAirport() {
		return nearByAirport;
	}
	public void setNearByAirport(String nearByAirport) {
		this.nearByAirport = nearByAirport;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getSearchId() {
		return searchId;
	}
	public void setSearchId(String searchId) {
		this.searchId = searchId;
	}
	public String getEngineID() {
		return engineID;
	}
	public void setEngineID(String engineID) {
		this.engineID = engineID;
	}
	public String getBasicFare() {
		return basicFare;
	}
	public void setBasicFare(String basicFare) {
		this.basicFare = basicFare;
	}
	public String getExchangeRate() {
		return exchangeRate;
	}
	public void setExchangeRate(String exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
	public String getTotalFareWithOutMarkUp() {
		return totalFareWithOutMarkUp;
	}
	public void setTotalFareWithOutMarkUp(String totalFareWithOutMarkUp) {
		this.totalFareWithOutMarkUp = totalFareWithOutMarkUp;
	}
	public String getTotalTaxWithOutMarkUp() {
		return totalTaxWithOutMarkUp;
	}
	public void setTotalTaxWithOutMarkUp(String totalTaxWithOutMarkUp) {
		this.totalTaxWithOutMarkUp = totalTaxWithOutMarkUp;
	}
	public String getBaseTransactionAmount() {
		return baseTransactionAmount;
	}
	public void setBaseTransactionAmount(String baseTransactionAmount) {
		this.baseTransactionAmount = baseTransactionAmount;
	}
	public String getCancelPenalty() {
		return cancelPenalty;
	}
	public void setCancelPenalty(String cancelPenalty) {
		this.cancelPenalty = cancelPenalty;
	}
	public String getChangePenalty() {
		return changePenalty;
	}
	public void setChangePenalty(String changePenalty) {
		this.changePenalty = changePenalty;
	}
	public String getEquivCurrencyCode() {
		return equivCurrencyCode;
	}
	public void setEquivCurrencyCode(String equivCurrencyCode) {
		this.equivCurrencyCode = equivCurrencyCode;
	}
	public String getFareInfoKey() {
		return fareInfoKey;
	}
	public void setFareInfoKey(String fareInfoKey) {
		this.fareInfoKey = fareInfoKey;
	}
	public String getFareInfoValue() {
		return fareInfoValue;
	}
	public void setFareInfoValue(String fareInfoValue) {
		this.fareInfoValue = fareInfoValue;
	}
	public String getMarkUP() {
		return markUP;
	}
	public void setMarkUP(String markUP) {
		this.markUP = markUP;
	}
	public String getPaxType() {
		return paxType;
	}
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}
	public String getRefundable() {
		return refundable;
	}
	public void setRefundable(String refundable) {
		this.refundable = refundable;
	}
	public String getTotalFare() {
		return totalFare;
	}
	public void setTotalFare(String totalFare) {
		this.totalFare = totalFare;
	}
	public String getTotalTax() {
		return totalTax;
	}
	public void setTotalTax(String totalTax) {
		this.totalTax = totalTax;
	}
	public String getTransactionAmount() {
		return transactionAmount;
	}
	public void setTransactionAmount(String transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	public String getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getBookingAmount() {
		return bookingAmount;
	}
	public void setBookingAmount(String bookingAmount) {
		this.bookingAmount = bookingAmount;
	}
	public String getBookingCurrencyCode() {
		return bookingCurrencyCode;
	}
	public void setBookingCurrencyCode(String bookingCurrencyCode) {
		this.bookingCurrencyCode = bookingCurrencyCode;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getCultureCode() {
		return cultureCode;
	}
	public void setCultureCode(String cultureCode) {
		this.cultureCode = cultureCode;
	}
	public String getDateofBirth() {
		return dateofBirth;
	}
	public void setDateofBirth(String dateofBirth) {
		this.dateofBirth = dateofBirth;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getFrequentFlierNumber() {
		return frequentFlierNumber;
	}
	public void setFrequentFlierNumber(String frequentFlierNumber) {
		this.frequentFlierNumber = frequentFlierNumber;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getHomePhone() {
		return homePhone;
	}
	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMeal() {
		return meal;
	}
	public void setMeal(String meal) {
		this.meal = meal;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getPassportExpiryDate() {
		return passportExpiryDate;
	}
	public void setPassportExpiryDate(String passportExpiryDate) {
		this.passportExpiryDate = passportExpiryDate;
	}
	public String getPassportNo() {
		return passportNo;
	}
	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}
	public String getProvisionState() {
		return provisionState;
	}
	public void setProvisionState(String provisionState) {
		this.provisionState = provisionState;
	}
	public String getResidentCountry() {
		return residentCountry;
	}
	public void setResidentCountry(String residentCountry) {
		this.residentCountry = residentCountry;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getVisatype() {
		return visatype;
	}
	public void setVisatype(String visatype) {
		this.visatype = visatype;
	}
	public String getTraceId() {
		return traceId;
	}
	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getTxnRefNo() {
		return txnRefNo;
	}
	public void setTxnRefNo(String txnRefNo) {
		this.txnRefNo = txnRefNo;
	}
	public String getAndroidBooking() {
		return androidBooking;
	}
	public void setAndroidBooking(String androidBooking) {
		this.androidBooking = androidBooking;
	}
	public List<BookFare> getBookFares() {
		return bookFares;
	}
	public void setBookFares(List<BookFare> bookFares) {
		this.bookFares = bookFares;
	}
	public String getDomestic() {
		return domestic;
	}
	public void setDomestic(String domestic) {
		this.domestic = domestic;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getGrandtotal() {
		return grandtotal;
	}
	public void setGrandtotal(String grandtotal) {
		this.grandtotal = grandtotal;
	}
	public String getAdults() {
		return adults;
	}
	public void setAdults(String adults) {
		this.adults = adults;
	}
	public String getChilds() {
		return childs;
	}
	public void setChilds(String childs) {
		this.childs = childs;
	}
	public String getInfants() {
		return infants;
	}
	public void setInfants(String infants) {
		this.infants = infants;
	}
	public String getMerchantRefNo() {
		return merchantRefNo;
	}
	public void setMerchantRefNo(String merchantRefNo) {
		this.merchantRefNo = merchantRefNo;
	}
	public List<TravellerFlightDetails> getTravellerDetails() {
		return travellerDetails;
	}
	public void setTravellerDetails(List<TravellerFlightDetails> travellerDetails) {
		this.travellerDetails = travellerDetails;
	}
	public String getTicketDetails() {
		return ticketDetails;
	}
	public void setTicketDetails(String ticketDetails) {
		this.ticketDetails = ticketDetails;
	}
	public String getFirstNamechild() {
		return firstNamechild;
	}
	public void setFirstNamechild(String firstNamechild) {
		this.firstNamechild = firstNamechild;
	}
	public String getLastNamechild() {
		return lastNamechild;
	}
	public void setLastNamechild(String lastNamechild) {
		this.lastNamechild = lastNamechild;
	}
	public String getFirstNameinfant() {
		return firstNameinfant;
	}
	public void setFirstNameinfant(String firstNameinfant) {
		this.firstNameinfant = firstNameinfant;
	}
	public String getLastNameinfant() {
		return lastNameinfant;
	}
	public void setLastNameinfant(String lastNameinfant) {
		this.lastNameinfant = lastNameinfant;
	}
	
	
}
