package com.msscard.model;

import java.util.List;

import com.msscard.entity.TravellerDetails;

public class FlightTicketMobileDTO {

	private String totalFare;
	private String transactionRefNo;
	private String flightStatus;
	private String mdexTxnRefNo;
	private List<TravellerDetailst> travellerDetails;
	private String bookingRefId;
	private String paymentStatus;
	private String ticketDetails;
	private String paymentMethod;
	
	private String source;
	private String destination;
	private String journeyDate;
	private String arrTime;
	private String deptTime;

	
	private String sourceReturn;
	private String destinationReturn;
	private String journeyDateReturn;
	private String arrTimeReturn;
	private String deptTimeReturn;
	
	private List<TicketObj> oneway;
	private List<TicketObj> roundway;
	public String getTotalFare() {
		return totalFare;
	}
	public void setTotalFare(String totalFare) {
		this.totalFare = totalFare;
	}
	public String getTransactionRefNo() {
		return transactionRefNo;
	}
	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}
	public String getFlightStatus() {
		return flightStatus;
	}
	public void setFlightStatus(String flightStatus) {
		this.flightStatus = flightStatus;
	}
	public String getMdexTxnRefNo() {
		return mdexTxnRefNo;
	}
	public void setMdexTxnRefNo(String mdexTxnRefNo) {
		this.mdexTxnRefNo = mdexTxnRefNo;
	}
	
	public List<TravellerDetailst> getTravellerDetails() {
		return travellerDetails;
	}
	public void setTravellerDetails(List<TravellerDetailst> travellerDetails) {
		this.travellerDetails = travellerDetails;
	}
	public String getBookingRefId() {
		return bookingRefId;
	}
	public void setBookingRefId(String bookingRefId) {
		this.bookingRefId = bookingRefId;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public String getTicketDetails() {
		return ticketDetails;
	}
	public void setTicketDetails(String ticketDetails) {
		this.ticketDetails = ticketDetails;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getJourneyDate() {
		return journeyDate;
	}
	public void setJourneyDate(String journeyDate) {
		this.journeyDate = journeyDate;
	}
	public String getArrTime() {
		return arrTime;
	}
	public void setArrTime(String arrTime) {
		this.arrTime = arrTime;
	}
	public String getDeptTime() {
		return deptTime;
	}
	public void setDeptTime(String deptTime) {
		this.deptTime = deptTime;
	}
	public String getSourceReturn() {
		return sourceReturn;
	}
	public void setSourceReturn(String sourceReturn) {
		this.sourceReturn = sourceReturn;
	}
	public String getDestinationReturn() {
		return destinationReturn;
	}
	public void setDestinationReturn(String destinationReturn) {
		this.destinationReturn = destinationReturn;
	}
	public String getJourneyDateReturn() {
		return journeyDateReturn;
	}
	public void setJourneyDateReturn(String journeyDateReturn) {
		this.journeyDateReturn = journeyDateReturn;
	}
	public String getArrTimeReturn() {
		return arrTimeReturn;
	}
	public void setArrTimeReturn(String arrTimeReturn) {
		this.arrTimeReturn = arrTimeReturn;
	}
	public String getDeptTimeReturn() {
		return deptTimeReturn;
	}
	public void setDeptTimeReturn(String deptTimeReturn) {
		this.deptTimeReturn = deptTimeReturn;
	}
	public List<TicketObj> getOneway() {
		return oneway;
	}
	public void setOneway(List<TicketObj> oneway) {
		this.oneway = oneway;
	}
	public List<TicketObj> getRoundway() {
		return roundway;
	}
	public void setRoundway(List<TicketObj> roundway) {
		this.roundway = roundway;
	}
	
	
}
