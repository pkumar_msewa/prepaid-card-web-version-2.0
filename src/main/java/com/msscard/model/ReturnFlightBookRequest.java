package com.msscard.model;

public class ReturnFlightBookRequest {

	private String  useradultsgender;
	private String  userchildsgender;
	private String  userinfantsgender;
	private String grandtotal;
	private String androidBooking;
	private String addOnDetail;
	private String baggageFare;
	private String boundType;
	private String journeyTime;
	private String aircraftCode;
	private String aircraftType;
	private String airlineName;
	private String amount;
	private String arrivalDate;
	private String arrivalTerminal;
	private String arrivalTime;
	private String availableSeat;
	private String boundTypes;
	private String cabin;
	private String cabinClasses;
	private String capacity;
	private String carrierCode;
	private String connecting;
	private String currencyCode;
	private String departureDate;
	private String departureTerminal;
	private String departureTime;
	private String duration;
	private String fareClassOfService;
	private String flightDesignator;
	private String flightDetailRefKey;
	private String flightName;
	private String flightNumber;
	private String group;
	private String numberOfStops;
	private String providerCode;
	private String remarks;
	private String sold;
	private String status;
	private String cache;
	private String deeplink;
	private String fareIndicator;
	private String fareRule;
	private String exchangeRate;
	private String baggageUnit;
	private String baggageWeight;
	private String baseTransactionAmount;
	private String basicFare;
	private String cancelPenalty;
	private String changePenalty;
	private String equivCurrencyCode;
	private String fareBasisCode;
	private String fareInfoKey;
	private String fareInfoValue;
	private String markUP;
	private String paxType;
	private String refundable;
	private String totalFare;
	private String totalTax;
	private String transactionAmount;
	private String totalFareWithOutMarkUp;
	private String totalTaxWithOutMarkUp;
	private String holdBooking;
	private String international;
	private String itineraryKey;
	private String journeyIndex;
	private String memoryCreationTime;
	private String nearByAirport;
	private String remark;
	private String roundTrip;
	private String searchId;
	private String special;
	private String domestic;
	private String engineID;
	private String beginDate;
	private String destination;
	private String endDate;
	private String origin;
	private String bookingAmount;
	private String bookingCurrencyCode;
	private String spKey;
	private String traceId;
	private String transactionId;
	private String address1;
	private String address2;
	private String city;
	private String countryCode;
	private String cultureCode;
	private String dateofBirth;
	private String emailAddress;
	private String firstName;
	private String frequentFlierNumber;
	private String gender;
	private String homePhone;
	private String lastName;
	private String meal;
	private String middleName;
	private String mobileNumber;
	private String nationality;
	private String passportExpiryDate;
	private String passportNo;
	private String provisionState;
	private String residentCountry;
	private String title;
	private String adults;
	private String childs;
	private String infants;

	private String androidBookingreturn;
	private String addOnDetailreturn;
	private String baggageFarereturn;
	private String boundTypereturn;
	private String journeyTimereturn;
	private String aircraftCodereturn;
	private String aircraftTypereturn;
	private String airlineNamereturn;
	private String amountreturn;
	private String arrivalDatereturn;
	private String arrivalTerminalreturn;
	private String arrivalTimereturn;
	private String availableSeatreturn;
	private String boundTypesreturn;
	private String cabinreturn;
	private String cabinClassesreturn;
	private String capacityreturn;
	private String carrierCodereturn;
	private String connectingreturn;
	private String currencyCodereturn;
	private String departureDatereturn;
	private String departureTerminalreturn;
	private String departureTimereturn;
	private String durationreturn;
	private String fareClassOfServicereturn;
	private String flightDesignatorreturn;
	private String flightDetailRefKeyreturn;
	private String flightNamereturn;
	private String flightNumberreturn;
	private String groupreturn;
	private String numberOfStopsreturn;
	private String providerCodereturn;
	private String remarksreturn;
	private String soldreturn;
	private String statusreturn;
	private String cachereturn;
	private String deeplinkreturn;
	private String fareIndicatorreturn;
	private String fareRulereturn;
	private String exchangeRatereturn;
	private String baggageUnitreturn;
	private String baggageWeightreturn;
	private String baseTransactionAmountreturn;
	private String basicFarereturn;
	private String cancelPenaltyreturn;
	private String changePenaltyreturn;
	private String equivCurrencyCodereturn;
	private String fareBasisCodereturn;
	private String fareInfoKeyreturn;
	private String fareInfoValuereturn;
	private String markUPreturn;
	private String paxTypereturn;
	private String refundablereturn;
	private String totalFarereturn;
	private String totalTaxreturn;
	private String transactionAmountreturn;
	private String totalFareWithOutMarkUpreturn;
	private String totalTaxWithOutMarkUpreturn;
	private String holdBookingreturn;
	private String internationalreturn;
	private String itineraryKeyreturn;
	private String journeyIndexreturn;
	private String memoryCreationTimereturn;
	private String nearByAirportreturn;
	private String remarkreturn;
	private String roundTripreturn;
	private String searchIdreturn;
	private String specialreturn;
	private String domesticreturn;
	private String engineIDreturn;
	private String beginDatereturn;
	private String destinationreturn;
	private String endDatereturn;
	private String originreturn;
	private String bookingAmountreturn;
	private String bookingCurrencyCodereturn;
	private String spKeyreturn;
	private String traceIdreturn;
	private String transactionIdreturn;

	private String firstNamechild;
	private String lastNamechild;

	private String firstNameinfant;
	private String lastNameinfant;

	private String adultspassNo;
	private String childspassNo;
	private String infantspassNo;
	
	private String adultDateOfBirth;
	private String childDateOfBirth;
	private String infantDateOfBirth;
	
	private String adultspassExp;
	private String childspassExp;
	private String infantspassExp;
	public String getUseradultsgender() {
		return useradultsgender;
	}
	public void setUseradultsgender(String useradultsgender) {
		this.useradultsgender = useradultsgender;
	}
	public String getUserchildsgender() {
		return userchildsgender;
	}
	public void setUserchildsgender(String userchildsgender) {
		this.userchildsgender = userchildsgender;
	}
	public String getUserinfantsgender() {
		return userinfantsgender;
	}
	public void setUserinfantsgender(String userinfantsgender) {
		this.userinfantsgender = userinfantsgender;
	}
	public String getGrandtotal() {
		return grandtotal;
	}
	public void setGrandtotal(String grandtotal) {
		this.grandtotal = grandtotal;
	}
	public String getAndroidBooking() {
		return androidBooking;
	}
	public void setAndroidBooking(String androidBooking) {
		this.androidBooking = androidBooking;
	}
	public String getAddOnDetail() {
		return addOnDetail;
	}
	public void setAddOnDetail(String addOnDetail) {
		this.addOnDetail = addOnDetail;
	}
	public String getBaggageFare() {
		return baggageFare;
	}
	public void setBaggageFare(String baggageFare) {
		this.baggageFare = baggageFare;
	}
	public String getBoundType() {
		return boundType;
	}
	public void setBoundType(String boundType) {
		this.boundType = boundType;
	}
	public String getJourneyTime() {
		return journeyTime;
	}
	public void setJourneyTime(String journeyTime) {
		this.journeyTime = journeyTime;
	}
	public String getAircraftCode() {
		return aircraftCode;
	}
	public void setAircraftCode(String aircraftCode) {
		this.aircraftCode = aircraftCode;
	}
	public String getAircraftType() {
		return aircraftType;
	}
	public void setAircraftType(String aircraftType) {
		this.aircraftType = aircraftType;
	}
	public String getAirlineName() {
		return airlineName;
	}
	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public String getArrivalTerminal() {
		return arrivalTerminal;
	}
	public void setArrivalTerminal(String arrivalTerminal) {
		this.arrivalTerminal = arrivalTerminal;
	}
	public String getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public String getAvailableSeat() {
		return availableSeat;
	}
	public void setAvailableSeat(String availableSeat) {
		this.availableSeat = availableSeat;
	}
	public String getBoundTypes() {
		return boundTypes;
	}
	public void setBoundTypes(String boundTypes) {
		this.boundTypes = boundTypes;
	}
	public String getCabin() {
		return cabin;
	}
	public void setCabin(String cabin) {
		this.cabin = cabin;
	}
	public String getCabinClasses() {
		return cabinClasses;
	}
	public void setCabinClasses(String cabinClasses) {
		this.cabinClasses = cabinClasses;
	}
	public String getCapacity() {
		return capacity;
	}
	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}
	public String getCarrierCode() {
		return carrierCode;
	}
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}
	public String getConnecting() {
		return connecting;
	}
	public void setConnecting(String connecting) {
		this.connecting = connecting;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}
	public String getDepartureTerminal() {
		return departureTerminal;
	}
	public void setDepartureTerminal(String departureTerminal) {
		this.departureTerminal = departureTerminal;
	}
	public String getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getFareClassOfService() {
		return fareClassOfService;
	}
	public void setFareClassOfService(String fareClassOfService) {
		this.fareClassOfService = fareClassOfService;
	}
	public String getFlightDesignator() {
		return flightDesignator;
	}
	public void setFlightDesignator(String flightDesignator) {
		this.flightDesignator = flightDesignator;
	}
	public String getFlightDetailRefKey() {
		return flightDetailRefKey;
	}
	public void setFlightDetailRefKey(String flightDetailRefKey) {
		this.flightDetailRefKey = flightDetailRefKey;
	}
	public String getFlightName() {
		return flightName;
	}
	public void setFlightName(String flightName) {
		this.flightName = flightName;
	}
	public String getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getNumberOfStops() {
		return numberOfStops;
	}
	public void setNumberOfStops(String numberOfStops) {
		this.numberOfStops = numberOfStops;
	}
	public String getProviderCode() {
		return providerCode;
	}
	public void setProviderCode(String providerCode) {
		this.providerCode = providerCode;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getSold() {
		return sold;
	}
	public void setSold(String sold) {
		this.sold = sold;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCache() {
		return cache;
	}
	public void setCache(String cache) {
		this.cache = cache;
	}
	public String getDeeplink() {
		return deeplink;
	}
	public void setDeeplink(String deeplink) {
		this.deeplink = deeplink;
	}
	public String getFareIndicator() {
		return fareIndicator;
	}
	public void setFareIndicator(String fareIndicator) {
		this.fareIndicator = fareIndicator;
	}
	public String getFareRule() {
		return fareRule;
	}
	public void setFareRule(String fareRule) {
		this.fareRule = fareRule;
	}
	public String getExchangeRate() {
		return exchangeRate;
	}
	public void setExchangeRate(String exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
	public String getBaggageUnit() {
		return baggageUnit;
	}
	public void setBaggageUnit(String baggageUnit) {
		this.baggageUnit = baggageUnit;
	}
	public String getBaggageWeight() {
		return baggageWeight;
	}
	public void setBaggageWeight(String baggageWeight) {
		this.baggageWeight = baggageWeight;
	}
	public String getBaseTransactionAmount() {
		return baseTransactionAmount;
	}
	public void setBaseTransactionAmount(String baseTransactionAmount) {
		this.baseTransactionAmount = baseTransactionAmount;
	}
	public String getBasicFare() {
		return basicFare;
	}
	public void setBasicFare(String basicFare) {
		this.basicFare = basicFare;
	}
	public String getCancelPenalty() {
		return cancelPenalty;
	}
	public void setCancelPenalty(String cancelPenalty) {
		this.cancelPenalty = cancelPenalty;
	}
	public String getChangePenalty() {
		return changePenalty;
	}
	public void setChangePenalty(String changePenalty) {
		this.changePenalty = changePenalty;
	}
	public String getEquivCurrencyCode() {
		return equivCurrencyCode;
	}
	public void setEquivCurrencyCode(String equivCurrencyCode) {
		this.equivCurrencyCode = equivCurrencyCode;
	}
	public String getFareBasisCode() {
		return fareBasisCode;
	}
	public void setFareBasisCode(String fareBasisCode) {
		this.fareBasisCode = fareBasisCode;
	}
	public String getFareInfoKey() {
		return fareInfoKey;
	}
	public void setFareInfoKey(String fareInfoKey) {
		this.fareInfoKey = fareInfoKey;
	}
	public String getFareInfoValue() {
		return fareInfoValue;
	}
	public void setFareInfoValue(String fareInfoValue) {
		this.fareInfoValue = fareInfoValue;
	}
	public String getMarkUP() {
		return markUP;
	}
	public void setMarkUP(String markUP) {
		this.markUP = markUP;
	}
	public String getPaxType() {
		return paxType;
	}
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}
	public String getRefundable() {
		return refundable;
	}
	public void setRefundable(String refundable) {
		this.refundable = refundable;
	}
	public String getTotalFare() {
		return totalFare;
	}
	public void setTotalFare(String totalFare) {
		this.totalFare = totalFare;
	}
	public String getTotalTax() {
		return totalTax;
	}
	public void setTotalTax(String totalTax) {
		this.totalTax = totalTax;
	}
	public String getTransactionAmount() {
		return transactionAmount;
	}
	public void setTransactionAmount(String transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	public String getTotalFareWithOutMarkUp() {
		return totalFareWithOutMarkUp;
	}
	public void setTotalFareWithOutMarkUp(String totalFareWithOutMarkUp) {
		this.totalFareWithOutMarkUp = totalFareWithOutMarkUp;
	}
	public String getTotalTaxWithOutMarkUp() {
		return totalTaxWithOutMarkUp;
	}
	public void setTotalTaxWithOutMarkUp(String totalTaxWithOutMarkUp) {
		this.totalTaxWithOutMarkUp = totalTaxWithOutMarkUp;
	}
	public String getHoldBooking() {
		return holdBooking;
	}
	public void setHoldBooking(String holdBooking) {
		this.holdBooking = holdBooking;
	}
	public String getInternational() {
		return international;
	}
	public void setInternational(String international) {
		this.international = international;
	}
	public String getItineraryKey() {
		return itineraryKey;
	}
	public void setItineraryKey(String itineraryKey) {
		this.itineraryKey = itineraryKey;
	}
	public String getJourneyIndex() {
		return journeyIndex;
	}
	public void setJourneyIndex(String journeyIndex) {
		this.journeyIndex = journeyIndex;
	}
	public String getMemoryCreationTime() {
		return memoryCreationTime;
	}
	public void setMemoryCreationTime(String memoryCreationTime) {
		this.memoryCreationTime = memoryCreationTime;
	}
	public String getNearByAirport() {
		return nearByAirport;
	}
	public void setNearByAirport(String nearByAirport) {
		this.nearByAirport = nearByAirport;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getRoundTrip() {
		return roundTrip;
	}
	public void setRoundTrip(String roundTrip) {
		this.roundTrip = roundTrip;
	}
	public String getSearchId() {
		return searchId;
	}
	public void setSearchId(String searchId) {
		this.searchId = searchId;
	}
	public String getSpecial() {
		return special;
	}
	public void setSpecial(String special) {
		this.special = special;
	}
	public String getDomestic() {
		return domestic;
	}
	public void setDomestic(String domestic) {
		this.domestic = domestic;
	}
	public String getEngineID() {
		return engineID;
	}
	public void setEngineID(String engineID) {
		this.engineID = engineID;
	}
	public String getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getBookingAmount() {
		return bookingAmount;
	}
	public void setBookingAmount(String bookingAmount) {
		this.bookingAmount = bookingAmount;
	}
	public String getBookingCurrencyCode() {
		return bookingCurrencyCode;
	}
	public void setBookingCurrencyCode(String bookingCurrencyCode) {
		this.bookingCurrencyCode = bookingCurrencyCode;
	}
	public String getSpKey() {
		return spKey;
	}
	public void setSpKey(String spKey) {
		this.spKey = spKey;
	}
	public String getTraceId() {
		return traceId;
	}
	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getCultureCode() {
		return cultureCode;
	}
	public void setCultureCode(String cultureCode) {
		this.cultureCode = cultureCode;
	}
	public String getDateofBirth() {
		return dateofBirth;
	}
	public void setDateofBirth(String dateofBirth) {
		this.dateofBirth = dateofBirth;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getFrequentFlierNumber() {
		return frequentFlierNumber;
	}
	public void setFrequentFlierNumber(String frequentFlierNumber) {
		this.frequentFlierNumber = frequentFlierNumber;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getHomePhone() {
		return homePhone;
	}
	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMeal() {
		return meal;
	}
	public void setMeal(String meal) {
		this.meal = meal;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getPassportExpiryDate() {
		return passportExpiryDate;
	}
	public void setPassportExpiryDate(String passportExpiryDate) {
		this.passportExpiryDate = passportExpiryDate;
	}
	public String getPassportNo() {
		return passportNo;
	}
	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}
	public String getProvisionState() {
		return provisionState;
	}
	public void setProvisionState(String provisionState) {
		this.provisionState = provisionState;
	}
	public String getResidentCountry() {
		return residentCountry;
	}
	public void setResidentCountry(String residentCountry) {
		this.residentCountry = residentCountry;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAdults() {
		return adults;
	}
	public void setAdults(String adults) {
		this.adults = adults;
	}
	public String getChilds() {
		return childs;
	}
	public void setChilds(String childs) {
		this.childs = childs;
	}
	public String getInfants() {
		return infants;
	}
	public void setInfants(String infants) {
		this.infants = infants;
	}
	public String getAndroidBookingreturn() {
		return androidBookingreturn;
	}
	public void setAndroidBookingreturn(String androidBookingreturn) {
		this.androidBookingreturn = androidBookingreturn;
	}
	public String getAddOnDetailreturn() {
		return addOnDetailreturn;
	}
	public void setAddOnDetailreturn(String addOnDetailreturn) {
		this.addOnDetailreturn = addOnDetailreturn;
	}
	public String getBaggageFarereturn() {
		return baggageFarereturn;
	}
	public void setBaggageFarereturn(String baggageFarereturn) {
		this.baggageFarereturn = baggageFarereturn;
	}
	public String getBoundTypereturn() {
		return boundTypereturn;
	}
	public void setBoundTypereturn(String boundTypereturn) {
		this.boundTypereturn = boundTypereturn;
	}
	public String getJourneyTimereturn() {
		return journeyTimereturn;
	}
	public void setJourneyTimereturn(String journeyTimereturn) {
		this.journeyTimereturn = journeyTimereturn;
	}
	public String getAircraftCodereturn() {
		return aircraftCodereturn;
	}
	public void setAircraftCodereturn(String aircraftCodereturn) {
		this.aircraftCodereturn = aircraftCodereturn;
	}
	public String getAircraftTypereturn() {
		return aircraftTypereturn;
	}
	public void setAircraftTypereturn(String aircraftTypereturn) {
		this.aircraftTypereturn = aircraftTypereturn;
	}
	public String getAirlineNamereturn() {
		return airlineNamereturn;
	}
	public void setAirlineNamereturn(String airlineNamereturn) {
		this.airlineNamereturn = airlineNamereturn;
	}
	public String getAmountreturn() {
		return amountreturn;
	}
	public void setAmountreturn(String amountreturn) {
		this.amountreturn = amountreturn;
	}
	public String getArrivalDatereturn() {
		return arrivalDatereturn;
	}
	public void setArrivalDatereturn(String arrivalDatereturn) {
		this.arrivalDatereturn = arrivalDatereturn;
	}
	public String getArrivalTerminalreturn() {
		return arrivalTerminalreturn;
	}
	public void setArrivalTerminalreturn(String arrivalTerminalreturn) {
		this.arrivalTerminalreturn = arrivalTerminalreturn;
	}
	public String getArrivalTimereturn() {
		return arrivalTimereturn;
	}
	public void setArrivalTimereturn(String arrivalTimereturn) {
		this.arrivalTimereturn = arrivalTimereturn;
	}
	public String getAvailableSeatreturn() {
		return availableSeatreturn;
	}
	public void setAvailableSeatreturn(String availableSeatreturn) {
		this.availableSeatreturn = availableSeatreturn;
	}
	public String getBoundTypesreturn() {
		return boundTypesreturn;
	}
	public void setBoundTypesreturn(String boundTypesreturn) {
		this.boundTypesreturn = boundTypesreturn;
	}
	public String getCabinreturn() {
		return cabinreturn;
	}
	public void setCabinreturn(String cabinreturn) {
		this.cabinreturn = cabinreturn;
	}
	public String getCabinClassesreturn() {
		return cabinClassesreturn;
	}
	public void setCabinClassesreturn(String cabinClassesreturn) {
		this.cabinClassesreturn = cabinClassesreturn;
	}
	public String getCapacityreturn() {
		return capacityreturn;
	}
	public void setCapacityreturn(String capacityreturn) {
		this.capacityreturn = capacityreturn;
	}
	public String getCarrierCodereturn() {
		return carrierCodereturn;
	}
	public void setCarrierCodereturn(String carrierCodereturn) {
		this.carrierCodereturn = carrierCodereturn;
	}
	public String getConnectingreturn() {
		return connectingreturn;
	}
	public void setConnectingreturn(String connectingreturn) {
		this.connectingreturn = connectingreturn;
	}
	public String getCurrencyCodereturn() {
		return currencyCodereturn;
	}
	public void setCurrencyCodereturn(String currencyCodereturn) {
		this.currencyCodereturn = currencyCodereturn;
	}
	public String getDepartureDatereturn() {
		return departureDatereturn;
	}
	public void setDepartureDatereturn(String departureDatereturn) {
		this.departureDatereturn = departureDatereturn;
	}
	public String getDepartureTerminalreturn() {
		return departureTerminalreturn;
	}
	public void setDepartureTerminalreturn(String departureTerminalreturn) {
		this.departureTerminalreturn = departureTerminalreturn;
	}
	public String getDepartureTimereturn() {
		return departureTimereturn;
	}
	public void setDepartureTimereturn(String departureTimereturn) {
		this.departureTimereturn = departureTimereturn;
	}
	public String getDurationreturn() {
		return durationreturn;
	}
	public void setDurationreturn(String durationreturn) {
		this.durationreturn = durationreturn;
	}
	public String getFareClassOfServicereturn() {
		return fareClassOfServicereturn;
	}
	public void setFareClassOfServicereturn(String fareClassOfServicereturn) {
		this.fareClassOfServicereturn = fareClassOfServicereturn;
	}
	public String getFlightDesignatorreturn() {
		return flightDesignatorreturn;
	}
	public void setFlightDesignatorreturn(String flightDesignatorreturn) {
		this.flightDesignatorreturn = flightDesignatorreturn;
	}
	public String getFlightDetailRefKeyreturn() {
		return flightDetailRefKeyreturn;
	}
	public void setFlightDetailRefKeyreturn(String flightDetailRefKeyreturn) {
		this.flightDetailRefKeyreturn = flightDetailRefKeyreturn;
	}
	public String getFlightNamereturn() {
		return flightNamereturn;
	}
	public void setFlightNamereturn(String flightNamereturn) {
		this.flightNamereturn = flightNamereturn;
	}
	public String getFlightNumberreturn() {
		return flightNumberreturn;
	}
	public void setFlightNumberreturn(String flightNumberreturn) {
		this.flightNumberreturn = flightNumberreturn;
	}
	public String getGroupreturn() {
		return groupreturn;
	}
	public void setGroupreturn(String groupreturn) {
		this.groupreturn = groupreturn;
	}
	public String getNumberOfStopsreturn() {
		return numberOfStopsreturn;
	}
	public void setNumberOfStopsreturn(String numberOfStopsreturn) {
		this.numberOfStopsreturn = numberOfStopsreturn;
	}
	public String getProviderCodereturn() {
		return providerCodereturn;
	}
	public void setProviderCodereturn(String providerCodereturn) {
		this.providerCodereturn = providerCodereturn;
	}
	public String getRemarksreturn() {
		return remarksreturn;
	}
	public void setRemarksreturn(String remarksreturn) {
		this.remarksreturn = remarksreturn;
	}
	public String getSoldreturn() {
		return soldreturn;
	}
	public void setSoldreturn(String soldreturn) {
		this.soldreturn = soldreturn;
	}
	public String getStatusreturn() {
		return statusreturn;
	}
	public void setStatusreturn(String statusreturn) {
		this.statusreturn = statusreturn;
	}
	public String getCachereturn() {
		return cachereturn;
	}
	public void setCachereturn(String cachereturn) {
		this.cachereturn = cachereturn;
	}
	public String getDeeplinkreturn() {
		return deeplinkreturn;
	}
	public void setDeeplinkreturn(String deeplinkreturn) {
		this.deeplinkreturn = deeplinkreturn;
	}
	public String getFareIndicatorreturn() {
		return fareIndicatorreturn;
	}
	public void setFareIndicatorreturn(String fareIndicatorreturn) {
		this.fareIndicatorreturn = fareIndicatorreturn;
	}
	public String getFareRulereturn() {
		return fareRulereturn;
	}
	public void setFareRulereturn(String fareRulereturn) {
		this.fareRulereturn = fareRulereturn;
	}
	public String getExchangeRatereturn() {
		return exchangeRatereturn;
	}
	public void setExchangeRatereturn(String exchangeRatereturn) {
		this.exchangeRatereturn = exchangeRatereturn;
	}
	public String getBaggageUnitreturn() {
		return baggageUnitreturn;
	}
	public void setBaggageUnitreturn(String baggageUnitreturn) {
		this.baggageUnitreturn = baggageUnitreturn;
	}
	public String getBaggageWeightreturn() {
		return baggageWeightreturn;
	}
	public void setBaggageWeightreturn(String baggageWeightreturn) {
		this.baggageWeightreturn = baggageWeightreturn;
	}
	public String getBaseTransactionAmountreturn() {
		return baseTransactionAmountreturn;
	}
	public void setBaseTransactionAmountreturn(String baseTransactionAmountreturn) {
		this.baseTransactionAmountreturn = baseTransactionAmountreturn;
	}
	public String getBasicFarereturn() {
		return basicFarereturn;
	}
	public void setBasicFarereturn(String basicFarereturn) {
		this.basicFarereturn = basicFarereturn;
	}
	public String getCancelPenaltyreturn() {
		return cancelPenaltyreturn;
	}
	public void setCancelPenaltyreturn(String cancelPenaltyreturn) {
		this.cancelPenaltyreturn = cancelPenaltyreturn;
	}
	public String getChangePenaltyreturn() {
		return changePenaltyreturn;
	}
	public void setChangePenaltyreturn(String changePenaltyreturn) {
		this.changePenaltyreturn = changePenaltyreturn;
	}
	public String getEquivCurrencyCodereturn() {
		return equivCurrencyCodereturn;
	}
	public void setEquivCurrencyCodereturn(String equivCurrencyCodereturn) {
		this.equivCurrencyCodereturn = equivCurrencyCodereturn;
	}
	public String getFareBasisCodereturn() {
		return fareBasisCodereturn;
	}
	public void setFareBasisCodereturn(String fareBasisCodereturn) {
		this.fareBasisCodereturn = fareBasisCodereturn;
	}
	public String getFareInfoKeyreturn() {
		return fareInfoKeyreturn;
	}
	public void setFareInfoKeyreturn(String fareInfoKeyreturn) {
		this.fareInfoKeyreturn = fareInfoKeyreturn;
	}
	public String getFareInfoValuereturn() {
		return fareInfoValuereturn;
	}
	public void setFareInfoValuereturn(String fareInfoValuereturn) {
		this.fareInfoValuereturn = fareInfoValuereturn;
	}
	public String getMarkUPreturn() {
		return markUPreturn;
	}
	public void setMarkUPreturn(String markUPreturn) {
		this.markUPreturn = markUPreturn;
	}
	public String getPaxTypereturn() {
		return paxTypereturn;
	}
	public void setPaxTypereturn(String paxTypereturn) {
		this.paxTypereturn = paxTypereturn;
	}
	public String getRefundablereturn() {
		return refundablereturn;
	}
	public void setRefundablereturn(String refundablereturn) {
		this.refundablereturn = refundablereturn;
	}
	public String getTotalFarereturn() {
		return totalFarereturn;
	}
	public void setTotalFarereturn(String totalFarereturn) {
		this.totalFarereturn = totalFarereturn;
	}
	public String getTotalTaxreturn() {
		return totalTaxreturn;
	}
	public void setTotalTaxreturn(String totalTaxreturn) {
		this.totalTaxreturn = totalTaxreturn;
	}
	public String getTransactionAmountreturn() {
		return transactionAmountreturn;
	}
	public void setTransactionAmountreturn(String transactionAmountreturn) {
		this.transactionAmountreturn = transactionAmountreturn;
	}
	public String getTotalFareWithOutMarkUpreturn() {
		return totalFareWithOutMarkUpreturn;
	}
	public void setTotalFareWithOutMarkUpreturn(String totalFareWithOutMarkUpreturn) {
		this.totalFareWithOutMarkUpreturn = totalFareWithOutMarkUpreturn;
	}
	public String getTotalTaxWithOutMarkUpreturn() {
		return totalTaxWithOutMarkUpreturn;
	}
	public void setTotalTaxWithOutMarkUpreturn(String totalTaxWithOutMarkUpreturn) {
		this.totalTaxWithOutMarkUpreturn = totalTaxWithOutMarkUpreturn;
	}
	public String getHoldBookingreturn() {
		return holdBookingreturn;
	}
	public void setHoldBookingreturn(String holdBookingreturn) {
		this.holdBookingreturn = holdBookingreturn;
	}
	public String getInternationalreturn() {
		return internationalreturn;
	}
	public void setInternationalreturn(String internationalreturn) {
		this.internationalreturn = internationalreturn;
	}
	public String getItineraryKeyreturn() {
		return itineraryKeyreturn;
	}
	public void setItineraryKeyreturn(String itineraryKeyreturn) {
		this.itineraryKeyreturn = itineraryKeyreturn;
	}
	public String getJourneyIndexreturn() {
		return journeyIndexreturn;
	}
	public void setJourneyIndexreturn(String journeyIndexreturn) {
		this.journeyIndexreturn = journeyIndexreturn;
	}
	public String getMemoryCreationTimereturn() {
		return memoryCreationTimereturn;
	}
	public void setMemoryCreationTimereturn(String memoryCreationTimereturn) {
		this.memoryCreationTimereturn = memoryCreationTimereturn;
	}
	public String getNearByAirportreturn() {
		return nearByAirportreturn;
	}
	public void setNearByAirportreturn(String nearByAirportreturn) {
		this.nearByAirportreturn = nearByAirportreturn;
	}
	public String getRemarkreturn() {
		return remarkreturn;
	}
	public void setRemarkreturn(String remarkreturn) {
		this.remarkreturn = remarkreturn;
	}
	public String getRoundTripreturn() {
		return roundTripreturn;
	}
	public void setRoundTripreturn(String roundTripreturn) {
		this.roundTripreturn = roundTripreturn;
	}
	public String getSearchIdreturn() {
		return searchIdreturn;
	}
	public void setSearchIdreturn(String searchIdreturn) {
		this.searchIdreturn = searchIdreturn;
	}
	public String getSpecialreturn() {
		return specialreturn;
	}
	public void setSpecialreturn(String specialreturn) {
		this.specialreturn = specialreturn;
	}
	public String getDomesticreturn() {
		return domesticreturn;
	}
	public void setDomesticreturn(String domesticreturn) {
		this.domesticreturn = domesticreturn;
	}
	public String getEngineIDreturn() {
		return engineIDreturn;
	}
	public void setEngineIDreturn(String engineIDreturn) {
		this.engineIDreturn = engineIDreturn;
	}
	public String getBeginDatereturn() {
		return beginDatereturn;
	}
	public void setBeginDatereturn(String beginDatereturn) {
		this.beginDatereturn = beginDatereturn;
	}
	public String getDestinationreturn() {
		return destinationreturn;
	}
	public void setDestinationreturn(String destinationreturn) {
		this.destinationreturn = destinationreturn;
	}
	public String getEndDatereturn() {
		return endDatereturn;
	}
	public void setEndDatereturn(String endDatereturn) {
		this.endDatereturn = endDatereturn;
	}
	public String getOriginreturn() {
		return originreturn;
	}
	public void setOriginreturn(String originreturn) {
		this.originreturn = originreturn;
	}
	public String getBookingAmountreturn() {
		return bookingAmountreturn;
	}
	public void setBookingAmountreturn(String bookingAmountreturn) {
		this.bookingAmountreturn = bookingAmountreturn;
	}
	public String getBookingCurrencyCodereturn() {
		return bookingCurrencyCodereturn;
	}
	public void setBookingCurrencyCodereturn(String bookingCurrencyCodereturn) {
		this.bookingCurrencyCodereturn = bookingCurrencyCodereturn;
	}
	public String getSpKeyreturn() {
		return spKeyreturn;
	}
	public void setSpKeyreturn(String spKeyreturn) {
		this.spKeyreturn = spKeyreturn;
	}
	public String getTraceIdreturn() {
		return traceIdreturn;
	}
	public void setTraceIdreturn(String traceIdreturn) {
		this.traceIdreturn = traceIdreturn;
	}
	public String getTransactionIdreturn() {
		return transactionIdreturn;
	}
	public void setTransactionIdreturn(String transactionIdreturn) {
		this.transactionIdreturn = transactionIdreturn;
	}
	public String getFirstNamechild() {
		return firstNamechild;
	}
	public void setFirstNamechild(String firstNamechild) {
		this.firstNamechild = firstNamechild;
	}
	public String getLastNamechild() {
		return lastNamechild;
	}
	public void setLastNamechild(String lastNamechild) {
		this.lastNamechild = lastNamechild;
	}
	public String getFirstNameinfant() {
		return firstNameinfant;
	}
	public void setFirstNameinfant(String firstNameinfant) {
		this.firstNameinfant = firstNameinfant;
	}
	public String getLastNameinfant() {
		return lastNameinfant;
	}
	public void setLastNameinfant(String lastNameinfant) {
		this.lastNameinfant = lastNameinfant;
	}
	public String getAdultspassNo() {
		return adultspassNo;
	}
	public void setAdultspassNo(String adultspassNo) {
		this.adultspassNo = adultspassNo;
	}
	public String getChildspassNo() {
		return childspassNo;
	}
	public void setChildspassNo(String childspassNo) {
		this.childspassNo = childspassNo;
	}
	public String getInfantspassNo() {
		return infantspassNo;
	}
	public void setInfantspassNo(String infantspassNo) {
		this.infantspassNo = infantspassNo;
	}
	public String getAdultDateOfBirth() {
		return adultDateOfBirth;
	}
	public void setAdultDateOfBirth(String adultDateOfBirth) {
		this.adultDateOfBirth = adultDateOfBirth;
	}
	public String getChildDateOfBirth() {
		return childDateOfBirth;
	}
	public void setChildDateOfBirth(String childDateOfBirth) {
		this.childDateOfBirth = childDateOfBirth;
	}
	public String getInfantDateOfBirth() {
		return infantDateOfBirth;
	}
	public void setInfantDateOfBirth(String infantDateOfBirth) {
		this.infantDateOfBirth = infantDateOfBirth;
	}
	public String getAdultspassExp() {
		return adultspassExp;
	}
	public void setAdultspassExp(String adultspassExp) {
		this.adultspassExp = adultspassExp;
	}
	public String getChildspassExp() {
		return childspassExp;
	}
	public void setChildspassExp(String childspassExp) {
		this.childspassExp = childspassExp;
	}
	public String getInfantspassExp() {
		return infantspassExp;
	}
	public void setInfantspassExp(String infantspassExp) {
		this.infantspassExp = infantspassExp;
	}
	
	
}
