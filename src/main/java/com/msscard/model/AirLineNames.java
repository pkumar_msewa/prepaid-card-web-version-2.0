package com.msscard.model;

public class AirLineNames {

	private String cityCode;
	
	private String cityName;
	
	private String airportName;
	
	private String country;
	
	public AirLineNames() {
		
	}
	public AirLineNames(String cityCode, String cityName, String airportName,String country) {
		super();
		this.cityCode = cityCode;
		this.cityName = cityName;
		this.airportName = airportName;
		this.country=country;
	}
	public String getCityCode() {
		return cityCode;
	}
	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getAirportName() {
		return airportName;
	}
	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	
}
