package com.msscard.model;

public class FligthcancelableReq {

	private String subUserId;
	private String transactionScreenId;
	
	public String getSubUserId() {
		return subUserId;
	}
	public void setSubUserId(String subUserId) {
		this.subUserId = subUserId;
	}
	public String getTransactionScreenId() {
		return transactionScreenId;
	}
	public void setTransactionScreenId(String transactionScreenId) {
		this.transactionScreenId = transactionScreenId;
	}
	
	
}
