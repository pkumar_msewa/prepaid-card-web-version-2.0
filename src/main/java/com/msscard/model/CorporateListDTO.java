package com.msscard.model;

public class CorporateListDTO {

	private String corporateName;
	private String contactNo;
	private String username;
	private String balance;
	private String corporateLogo1;
	private String corporateLogo2;
	private String status;
	private String accountNumber;
	private String ifscCode;
	private String id;
	
	
	@Override
	public String toString() {
		return "CorporateListDTO [corporateName=" + corporateName + ", contactNo=" + contactNo + ", username="
				+ username + ", balance=" + balance + ", corporateLogo1=" + corporateLogo1 + ", corporateLogo2="
				+ corporateLogo2 + ", status=" + status + ", accountNumber=" + accountNumber + ", ifscCode=" + ifscCode
				+ ", id=" + id + "]";
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getIfscCode() {
		return ifscCode;
	}
	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}
	public String getCorporateName() {
		return corporateName;
	}
	public void setCorporateName(String corporateName) {
		this.corporateName = corporateName;
	}
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}
	public String getCorporateLogo1() {
		return corporateLogo1;
	}
	public void setCorporateLogo1(String corporateLogo1) {
		this.corporateLogo1 = corporateLogo1;
	}
	public String getCorporateLogo2() {
		return corporateLogo2;
	}
	public void setCorporateLogo2(String corporateLogo2) {
		this.corporateLogo2 = corporateLogo2;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
