package com.msscard.model;

public enum PaymentType {

	Wallet("Wallet","WLT"),

	VNet("VNet","VNT"),

	EBS("EBS","EBS");

	private PaymentType() {

	}

	private String key;

	private String value;

	private PaymentType(String key,String value) {
		this.key=key;
		this.value = value;
	}

	public String getKey(){
		return key;
	}
	public String getValue() {
		return value;
	}

	public static PaymentType getEnumByValue(String value) {
		if (value == null)
			throw new IllegalArgumentException();
		for (PaymentType v : values())
			if (value.equalsIgnoreCase(v.getValue()))
				return v;
		throw new IllegalArgumentException();
	}

	public static PaymentType getEnumByKey(String key) {
		if (key == null)
			throw new IllegalArgumentException();
		for (PaymentType v : values())
			if (key.equalsIgnoreCase(v.getKey()))
				return v;
		throw new IllegalArgumentException();
	}
}
