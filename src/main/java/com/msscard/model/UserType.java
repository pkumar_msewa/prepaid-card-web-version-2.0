package com.msscard.model;

public enum UserType {

	Admin,User,Merchant,Locked,SuperAdmin,Agent,SuperAgent,Donatee,SpecialUser,Corporate,CorporatePartner,Group;
}
