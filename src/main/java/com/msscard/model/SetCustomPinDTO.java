package com.msscard.model;

import com.msscard.app.model.request.SessionDTO;

public class SetCustomPinDTO extends SessionDTO{

	private String pin;
	
	private String dob;
	
	private String customPinRequest;
	
	public String getCustomPinRequest() {
		return customPinRequest;
	}

	public void setCustomPinRequest(String customPinRequest) {
		this.customPinRequest = customPinRequest;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}
	
	
	
}
