package com.msscard.model;

import java.io.StringWriter;
import java.util.ArrayList;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;

import com.msscard.entity.ERCredentialsManager;
import com.msscard.repositories.ERCredentialManager;
import com.msscard.util.MatchMoveUtil;
import com.razorpay.constants.MdexConstants;


public class AirPriceCheckRequest implements JSONRequest{
	
	private String clientIp;
	private String cllientKey;
	private String clientToken;
	private String clientApiName;
	
	private FlightAvailability flightAvailability;
	private ArrayList<Segment> segments;
	public String getClientIp() {
		return clientIp;
	}
	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}
	public String getCllientKey() {
		return cllientKey;
	}
	public void setCllientKey(String cllientKey) {
		this.cllientKey = cllientKey;
	}
	public String getClientToken() {
		return clientToken;
	}
	public void setClientToken(String clientToken) {
		this.clientToken = clientToken;
	}
	public String getClientApiName() {
		return clientApiName;
	}
	public void setClientApiName(String clientApiName) {
		this.clientApiName = clientApiName;
	}
	public FlightAvailability getFlightAvailability() {
		return flightAvailability;
	}
	public void setFlightAvailability(FlightAvailability flightAvailability) {
		this.flightAvailability = flightAvailability;
	}
	public ArrayList<Segment> getSegments() {
		return segments;
	}
	public void setSegments(ArrayList<Segment> segments) {
		this.segments = segments;
	}

	@Override
	public String getJsonRequest() {
		
				
		JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
	    JsonObjectBuilder FlightAvailabilityRQ = Json.createObjectBuilder();
	 	FlightAvailabilityRQ.add("adults", getFlightAvailability().getAdults());
	 		JsonObjectBuilder Authentication = Json.createObjectBuilder();
	 		Authentication.add("ipAddress", "");
		
	 	FlightAvailabilityRQ.add("cabin", getFlightAvailability().getCabin());
	 	FlightAvailabilityRQ.add("childs", getFlightAvailability().getChilds());
	 	JsonArrayBuilder engineId = Json.createArrayBuilder();
	 	
		for (int i = 0; i < getFlightAvailability().getEngineIDs().size(); i++) {
			engineId.add(getFlightAvailability().getEngineIDs().get(i));
		}
		FlightAvailabilityRQ.add("engineIDs", engineId);
	 	FlightAvailabilityRQ.add("traceId", getFlightAvailability().getTraceId());
			JsonArrayBuilder fsDetails = Json.createArrayBuilder();
			JsonObjectBuilder fsDetailsList = Json.createObjectBuilder();
			for (int j = 0; j < getFlightAvailability().getFlightSearchDetails().size(); j++) {
				fsDetailsList.add("origin", getFlightAvailability().getFlightSearchDetails().get(j).getOrigin());
				fsDetailsList.add("destination", getFlightAvailability().getFlightSearchDetails().get(j).getDestination());
				fsDetailsList.add("beginDate", getFlightAvailability().getFlightSearchDetails().get(j).getBeginDate());
				fsDetails.add(fsDetailsList.build());
			}
	    FlightAvailabilityRQ.add("flightSearchDetails", fsDetails);
	 	FlightAvailabilityRQ.add("infants", getFlightAvailability().getInfants());
	 	FlightAvailabilityRQ.add("tripType", getFlightAvailability().getTripType());
		
		JsonArrayBuilder BookingSegment = Json.createArrayBuilder();
        JsonObjectBuilder segList = Json.createObjectBuilder();
        for (int i = 0; i < segments.size(); i++) {
//        	 segList.add("BondType",  (segments.get(i).getBondType() == null) ? "" : segments.get(i).getBondType());
         	JsonArrayBuilder Bonds = Json.createArrayBuilder();
         	JsonObjectBuilder bondsList = Json.createObjectBuilder();
         	for (int j = 0; j < segments.get(i).getBonds().size(); j++) {
         		bondsList.add("boundType", (segments.get(i).getBonds().get(j).getBoundType() == null) ? "" : segments.get(i).getBonds().get(j).getBoundType());
             	bondsList.add("baggageFare", segments.get(i).getBonds().get(j).isBaggageFare());
             	bondsList.add("ssrFare", segments.get(i).getBonds().get(j).isSsrFare());
             	bondsList.add("itineraryKey", segments.get(i).getBonds().get(j).getItineraryKey());
             	bondsList.add("journeyTime", segments.get(i).getBonds().get(j).getJourneyTime());
             		JsonArrayBuilder Legs = Json.createArrayBuilder();
             		JsonObjectBuilder LegsList = Json.createObjectBuilder();
             		for (int k = 0; k < segments.get(i).getBonds().get(j).getLegs().size(); k++) {
             			LegsList.add("aircraftCode", (segments.get(i).getBonds().get(j).getLegs().get(k).getAircraftCode() == null) ? "" : segments.get(i).getBonds().get(j).getLegs().get(k).getAircraftCode());
                 		LegsList.add("aircraftType", (segments.get(i).getBonds().get(j).getLegs().get(k).getAircraftType() == null) ? "" : segments.get(i).getBonds().get(j).getLegs().get(k).getAircraftType());
                 		LegsList.add("airlineName", (segments.get(i).getBonds().get(j).getLegs().get(k).getAirlineName() == null) ? "" : segments.get(i).getBonds().get(j).getLegs().get(k).getAirlineName());
//                 		LegsList.add("amount", segments.get(i).getBonds().get(j).getLegs().get(k).getAmount());
                 		LegsList.add("arrivalDate", segments.get(i).getBonds().get(j).getLegs().get(k).getArrivalDate());
                 		LegsList.add("arrivalTerminal", (segments.get(i).getBonds().get(j).getLegs().get(k).getArrivalTerminal() == null) ? "" : segments.get(i).getBonds().get(j).getLegs().get(k).getArrivalTerminal());
                 		LegsList.add("arrivalTime", segments.get(i).getBonds().get(j).getLegs().get(k).getArrivalTime());
                 		LegsList.add("availableSeat", (segments.get(i).getBonds().get(j).getLegs().get(k).getAvailableSeat() == null) ? "" : segments.get(i).getBonds().get(j).getLegs().get(k).getAvailableSeat());
                 		LegsList.add("baggageUnit", segments.get(i).getBonds().get(j).getLegs().get(k).getBaggageUnit());
                 		LegsList.add("baggageWeight", segments.get(i).getBonds().get(j).getLegs().get(k).getBaggageWeight());
                 		LegsList.add("boundTypes", (segments.get(i).getBonds().get(j).getLegs().get(k).getBoundTypes() == null) ? "" : segments.get(i).getBonds().get(j).getLegs().get(k).getBoundTypes());
                 		LegsList.add("cabin", segments.get(i).getBonds().get(j).getLegs().get(k).getCabin());
                 		LegsList.add("capacity", (segments.get(i).getBonds().get(j).getLegs().get(k).getCapacity() == null) ? "" : segments.get(i).getBonds().get(j).getLegs().get(k).getCapacity());
                 		LegsList.add("carrierCode", (segments.get(i).getBonds().get(j).getLegs().get(k).getCarrierCode() == null) ? "" : segments.get(i).getBonds().get(j).getLegs().get(k).getCarrierCode());
                 		LegsList.add("currencyCode", (segments.get(i).getBonds().get(j).getLegs().get(k).getCurrencyCode() == null) ? "" : segments.get(i).getBonds().get(j).getLegs().get(k).getCurrencyCode());
                 		LegsList.add("departureDate", (segments.get(i).getBonds().get(j).getLegs().get(k).getDepartureDate() == null) ? "" : segments.get(i).getBonds().get(j).getLegs().get(k).getDepartureDate());
                 		LegsList.add("departureTerminal", (segments.get(i).getBonds().get(j).getLegs().get(k).getDepartureTerminal() == null) ? "" : segments.get(i).getBonds().get(j).getLegs().get(k).getDepartureTerminal());
                 		LegsList.add("departureTime", (segments.get(i).getBonds().get(j).getLegs().get(k).getDepartureTime() == null) ? "" : segments.get(i).getBonds().get(j).getLegs().get(k).getDepartureTime());
                 		LegsList.add("destination", (segments.get(i).getBonds().get(j).getLegs().get(k).getDestination() == null) ? "" : segments.get(i).getBonds().get(j).getLegs().get(k).getDestination());
                 		LegsList.add("duration", (segments.get(i).getBonds().get(j).getLegs().get(k).getDuration() == null) ? "" : segments.get(i).getBonds().get(j).getLegs().get(k).getDuration());
                 		LegsList.add("fareBasisCode", (segments.get(i).getBonds().get(j).getLegs().get(k).getFareBasisCode() == null) ? "" : segments.get(i).getBonds().get(j).getLegs().get(k).getFareBasisCode());
                 		LegsList.add("fareClassOfService", (segments.get(i).getBonds().get(j).getLegs().get(k).getFareClassOfService() == null) ? "" : segments.get(i).getBonds().get(j).getLegs().get(k).getFareClassOfService());
                 		LegsList.add("flightDesignator", (segments.get(i).getBonds().get(j).getLegs().get(k).getFlightDesignator() == null) ? "" : segments.get(i).getBonds().get(j).getLegs().get(k).getFlightDesignator());
//                 		LegsList.add("flightName", (segments.get(i).getBonds().get(j).getLegs().get(k).getFlightName() == null ) ? "" : segments.get(i).getBonds().get(j).getLegs().get(k).getFlightName());
                 		LegsList.add("flightNumber", (segments.get(i).getBonds().get(j).getLegs().get(k).getFlightNumber() == null) ? "" : segments.get(i).getBonds().get(j).getLegs().get(k).getFlightNumber());
//                 		LegsList.add("isConnecting", segments.get(i).getBonds().get(j).getLegs().get(k).isConnecting());
                 		LegsList.add("origin", (segments.get(i).getBonds().get(j).getLegs().get(k).getOrigin() == null) ? "" : segments.get(i).getBonds().get(j).getLegs().get(k).getOrigin());
                        LegsList.add("sold", segments.get(i).getBonds().get(j).getLegs().get(k).getSold());
                 		LegsList.add("status", (segments.get(i).getBonds().get(j).getLegs().get(k).getStatus() == null) ? "" : segments.get(i).getBonds().get(j).getLegs().get(k).getStatus());
                 		LegsList.add("group", (segments.get(i).getBonds().get(j).getLegs().get(k).getGroup() == null ) ? "" : segments.get(i).getBonds().get(j).getLegs().get(k).getGroup());
                 		LegsList.add("providerCode", (segments.get(i).getBonds().get(j).getLegs().get(k).getProviderCode() == null ) ? "" : segments.get(i).getBonds().get(j).getLegs().get(k).getProviderCode());
                 		Legs.add(LegsList.build());
					}
                 bondsList.add("legs", Legs);
     		Bonds.add(bondsList.build());
			}
         segList.add("bonds", Bonds);
         segList.add("engineID", segments.get(i).getEngineID());
         	JsonArrayBuilder Fare = Json.createArrayBuilder();
         	JsonObjectBuilder FareList = Json.createObjectBuilder();
         	for (int l = 0; l < segments.get(i).getFares().size(); l++) {
         		FareList.add("basicFare", segments.get(i).getFares().get(l).getBasicFare());
    			FareList.add("exchangeRate", segments.get(i).getFares().get(l).getExchangeRate());
     			JsonArrayBuilder PaxFares = Json.createArrayBuilder();
    				JsonObjectBuilder PaxFaresList = Json.createObjectBuilder();
    				for (int m = 0; m < segments.get(i).getFares().get(l).getPaxFares().size(); m++) {
    					PaxFaresList.add("baggageUnit", (segments.get(i).getFares().get(l).getPaxFares().get(m).getBaggageUnit() == null) ? "" : segments.get(i).getFares().get(l).getPaxFares().get(m).getBaggageUnit());	
    	 				PaxFaresList.add("baggageWeight", (segments.get(i).getFares().get(l).getPaxFares().get(m).getBaggageWeight() == null)? "" : segments.get(i).getFares().get(l).getPaxFares().get(m).getBaggageWeight());
    	 				PaxFaresList.add("baseTransactionAmount", segments.get(i).getFares().get(l).getPaxFares().get(m).getBaseTransactionAmount());
    	 				PaxFaresList.add("basicFare", segments.get(i).getFares().get(l).getPaxFares().get(m).getBasicFare());
    	 				PaxFaresList.add("cancelPenalty", segments.get(i).getFares().get(l).getPaxFares().get(m).getCancelPenalty());
    	 				PaxFaresList.add("changePenalty", segments.get(i).getFares().get(l).getPaxFares().get(m).getChangePenalty());
    	 					JsonArrayBuilder Fares = Json.createArrayBuilder();
    	 					JsonObjectBuilder FaresList = Json.createObjectBuilder();
    	 					for (int n = 0; n < segments.get(i).getFares().get(l).getPaxFares().get(m).getBookFares().size(); n++) {
    	 						FaresList.add("amount", segments.get(i).getFares().get(l).getPaxFares().get(m).getBookFares().get(n).getAmount());
    	 	 					FaresList.add("chargeCode", segments.get(i).getFares().get(l).getPaxFares().get(m).getBookFares().get(n).getChargeCode());
    	 	 					FaresList.add("chargeType", segments.get(i).getFares().get(l).getPaxFares().get(m).getBookFares().get(n).getChargeType());
    	 	 					Fares.add(FaresList.build());
    					}
    	 				PaxFaresList.add("bookFares", Fares);
    	 				PaxFaresList.add("markUP", segments.get(i).getFares().get(l).getPaxFares().get(m).getMarkUP());
    	 				PaxFaresList.add("paxType", segments.get(i).getFares().get(l).getPaxFares().get(m).getPaxType());
    	 				PaxFaresList.add("refundable", segments.get(i).getFares().get(l).getPaxFares().get(m).isRefundable());
    	 				PaxFaresList.add("totalFare", segments.get(i).getFares().get(l).getPaxFares().get(m).getTotalFare());
    	 				PaxFaresList.add("totalTax", segments.get(i).getFares().get(l).getPaxFares().get(m).getTotalTax());
    	 				PaxFaresList.add("transactionAmount", segments.get(i).getFares().get(l).getPaxFares().get(m).getTransactionAmount());
    	 				PaxFares.add(PaxFaresList.build());
    			}
    				FareList.add("paxFares", PaxFares);
    				FareList.add("totalFareWithOutMarkUp", segments.get(i).getFares().get(l).getTotalFareWithOutMarkUp());
        			FareList.add("totalTaxWithOutMarkUp", segments.get(i).getFares().get(l).getTotalTaxWithOutMarkUp());
    			Fare.add(FareList.build());
			}
		 segList.add("fares", Fare);
     	 segList.add("fareRule", (segments.get(i).getFareRule() == null ) ? "" : segments.get(i).getFareRule());
         segList.add("baggageFare", segments.get(i).isBaggageFare());
         segList.add("cache", segments.get(i).isCache());
         segList.add("holdBooking", segments.get(i).isHoldBooking());
         segList.add("international", segments.get(i).isInternational());
         segList.add("roundTrip", segments.get(i).isRoundTrip());
         segList.add("special", segments.get(i).isSpecial());
         segList.add("specialId", segments.get(i).isSpecialId());
         segList.add("itineraryKey", (segments.get(i).getItineraryKey() == null) ? "" : segments.get(i).getItineraryKey());
         segList.add("journeyIndex", segments.get(i).getJourneyIndex());
         segList.add("nearByAirport", segments.get(i).isNearByAirport());
         segList.add("searchId", (segments.get(i).getSearchId() == null) ? "" : segments.get(i).getSearchId());
         BookingSegment.add(segList.build());
		}
        
        jsonBuilder.add("flightAvailability", FlightAvailabilityRQ);
        jsonBuilder.add("segments", BookingSegment);

        jsonBuilder.add("clientIp", MdexConstants.MDEX_CLIENTIP);
        jsonBuilder.add("cllientKey", MdexConstants.getMdexCredentials().getCode());
        jsonBuilder.add("clientToken", MdexConstants.getMdexCredentials().getStatus());
        jsonBuilder.add("clientApiName", "Flight Search");
		
        JsonObject empObj = jsonBuilder.build();
        StringWriter jsnReqStr = new StringWriter();
        JsonWriter jsonWtr = Json.createWriter(jsnReqStr);
        jsonWtr.writeObject(empObj);
        jsonWtr.close();
        System.out.println(jsnReqStr.toString());
		
		return jsnReqStr.toString();
	}
}
