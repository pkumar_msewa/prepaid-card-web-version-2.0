package com.msscard.model;

public class TravelFlightRequest {

	private String origin;
	private String destination;
	private String beginDate;
	private String engineIDs;
	private String tripType;
	private String cabin;
	private String adults;
	private String childs;
	private String infants;
	private String traceId;
	private String sessionId;
	private String endDate;
	private String srcFullName;
	private String destFullName;
	private String data;
	
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}
	public String getEngineIDs() {
		return engineIDs;
	}
	public void setEngineIDs(String engineIDs) {
		this.engineIDs = engineIDs;
	}
	public String getTripType() {
		return tripType;
	}
	public void setTripType(String tripType) {
		this.tripType = tripType;
	}
	public String getCabin() {
		return cabin;
	}
	public void setCabin(String cabin) {
		this.cabin = cabin;
	}
	public String getAdults() {
		return adults;
	}
	public void setAdults(String adults) {
		this.adults = adults;
	}
	public String getChilds() {
		return childs;
	}
	public void setChilds(String childs) {
		this.childs = childs;
	}
	public String getInfants() {
		return infants;
	}
	public void setInfants(String infants) {
		this.infants = infants;
	}
	public String getTraceId() {
		return traceId;
	}
	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getSrcFullName() {
		return srcFullName;
	}
	public void setSrcFullName(String srcFullName) {
		this.srcFullName = srcFullName;
	}
	public String getDestFullName() {
		return destFullName;
	}
	public void setDestFullName(String destFullName) {
		this.destFullName = destFullName;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
	
}
