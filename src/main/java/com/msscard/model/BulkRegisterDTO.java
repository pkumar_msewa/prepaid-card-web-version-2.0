package com.msscard.model;

import org.springframework.web.multipart.MultipartFile;

public class BulkRegisterDTO {
	
	private String sessionId;
	
	private boolean kycStatus;

	private String username;

	private String password;

	private String confirmPassword;
   
	private UserType userType;

	private String authority;

	private Status status;

	private String address;

	private String contactNo;

	private String firstName;

	private String middleName;

	private String lastName;

	private String locationCode;

	private String registrationDate;

	private String dateOfBirth;

	private String email;

	private boolean vbankCustomer;

	private String branchCode;

	private String accountNumber;
	
	private String panCardNo;
	
	private String aadharNo;
	
	private MultipartFile panCardImage;
	
	private MultipartFile aadharImage;
	
	private String panCardImagePath;
	
	private String aadharImagePath;
	
	private String idType;
	
	private String idNo;
	
	private String shopNo;
	
	private String bankName;
	
	private String accountName;
	
	private String bankAccountNo;
	
	private String branchName;
	
	private String ifscCode;
	
	private String settlementAccountName;
	
	private String settlementAccountNo;
	
	private String settlementBankName;
	
	private String settlementBranchName;
	
	private String settlementIfscCode;
	

	private String secQuestionCode;
	
	private String secAnswer;
	
	private boolean web;
	
	private String brand;
	
	private String model;
	
	private String imeiNo;
	
	private String name;
	private String referee;
	private boolean hasAadhar;
	private boolean hasMpin;
	private String androidDeviceID;
	
	private MultipartFile file;
	private MultipartFile otherFile;
	private String fileName;
	
	
	public String getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public MultipartFile getOtherFile() {
		return otherFile;
	}

	public void setOtherFile(MultipartFile otherFile) {
		this.otherFile = otherFile;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public boolean isKycStatus() {
		return kycStatus;
	}

	public void setKycStatus(boolean kycStatus) {
		this.kycStatus = kycStatus;
	}

	public boolean isVbankCustomer() {
		return vbankCustomer;
	}

	public void setVbankCustomer(boolean vbankCustomer) {
		this.vbankCustomer = vbankCustomer;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getPanCardNo() {
		return panCardNo;
	}

	public void setPanCardNo(String panCardNo) {
		this.panCardNo = panCardNo;
	}

	public String getAadharNo() {
		return aadharNo;
	}

	public void setAadharNo(String aadharNo) {
		this.aadharNo = aadharNo;
	}

	public MultipartFile getPanCardImage() {
		return panCardImage;
	}

	public void setPanCardImage(MultipartFile panCardImage) {
		this.panCardImage = panCardImage;
	}

	public MultipartFile getAadharImage() {
		return aadharImage;
	}

	public void setAadharImage(MultipartFile aadharImage) {
		this.aadharImage = aadharImage;
	}

	public String getPanCardImagePath() {
		return panCardImagePath;
	}

	public void setPanCardImagePath(String panCardImagePath) {
		this.panCardImagePath = panCardImagePath;
	}

	public String getAadharImagePath() {
		return aadharImagePath;
	}

	public void setAadharImagePath(String aadharImagePath) {
		this.aadharImagePath = aadharImagePath;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getIdNo() {
		return idNo;
	}

	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}

	public String getShopNo() {
		return shopNo;
	}

	public void setShopNo(String shopNo) {
		this.shopNo = shopNo;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getBankAccountNo() {
		return bankAccountNo;
	}

	public void setBankAccountNo(String bankAccountNo) {
		this.bankAccountNo = bankAccountNo;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getSettlementAccountName() {
		return settlementAccountName;
	}

	public void setSettlementAccountName(String settlementAccountName) {
		this.settlementAccountName = settlementAccountName;
	}

	public String getSettlementAccountNo() {
		return settlementAccountNo;
	}

	public void setSettlementAccountNo(String settlementAccountNo) {
		this.settlementAccountNo = settlementAccountNo;
	}

	public String getSettlementBankName() {
		return settlementBankName;
	}

	public void setSettlementBankName(String settlementBankName) {
		this.settlementBankName = settlementBankName;
	}

	public String getSettlementBranchName() {
		return settlementBranchName;
	}

	public void setSettlementBranchName(String settlementBranchName) {
		this.settlementBranchName = settlementBranchName;
	}

	public String getSettlementIfscCode() {
		return settlementIfscCode;
	}

	public void setSettlementIfscCode(String settlementIfscCode) {
		this.settlementIfscCode = settlementIfscCode;
	}

	public String getSecQuestionCode() {
		return secQuestionCode;
	}

	public void setSecQuestionCode(String secQuestionCode) {
		this.secQuestionCode = secQuestionCode;
	}

	public String getSecAnswer() {
		return secAnswer;
	}

	public void setSecAnswer(String secAnswer) {
		this.secAnswer = secAnswer;
	}

	public boolean isWeb() {
		return web;
	}

	public void setWeb(boolean web) {
		this.web = web;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getImeiNo() {
		return imeiNo;
	}

	public void setImeiNo(String imeiNo) {
		this.imeiNo = imeiNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getReferee() {
		return referee;
	}

	public void setReferee(String referee) {
		this.referee = referee;
	}

	public boolean isHasAadhar() {
		return hasAadhar;
	}

	public void setHasAadhar(boolean hasAadhar) {
		this.hasAadhar = hasAadhar;
	}

	public boolean isHasMpin() {
		return hasMpin;
	}

	public void setHasMpin(boolean hasMpin) {
		this.hasMpin = hasMpin;
	}

	public String getAndroidDeviceID() {
		return androidDeviceID;
	}

	public void setAndroidDeviceID(String androidDeviceID) {
		this.androidDeviceID = androidDeviceID;
	}
}
