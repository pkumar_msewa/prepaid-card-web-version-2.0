package com.msscard.model;

import java.util.ArrayList;

public class FlightAvailability {

	private ArrayList<FlightSearchDetails> flightSearchDetails;
	private String TripType;
	private String Adults;
	private String Childs;
	private String Infants;
	private String Cabin;
	private ArrayList<String> engineIDs;
	private String TraceId;
	public ArrayList<FlightSearchDetails> getFlightSearchDetails() {
		return flightSearchDetails;
	}
	public void setFlightSearchDetails(ArrayList<FlightSearchDetails> flightSearchDetails) {
		this.flightSearchDetails = flightSearchDetails;
	}
	public String getTripType() {
		return TripType;
	}
	public void setTripType(String tripType) {
		TripType = tripType;
	}
	public String getAdults() {
		return Adults;
	}
	public void setAdults(String adults) {
		Adults = adults;
	}
	public String getChilds() {
		return Childs;
	}
	public void setChilds(String childs) {
		Childs = childs;
	}
	public String getInfants() {
		return Infants;
	}
	public void setInfants(String infants) {
		Infants = infants;
	}
	public String getCabin() {
		return Cabin;
	}
	public void setCabin(String cabin) {
		Cabin = cabin;
	}
	public ArrayList<String> getEngineIDs() {
		return engineIDs;
	}
	public void setEngineIDs(ArrayList<String> engineIDs) {
		this.engineIDs = engineIDs;
	}
	public String getTraceId() {
		return TraceId;
	}
	public void setTraceId(String traceId) {
		TraceId = traceId;
	}
	
	
}
