package com.msscard.model;

public class MAgentDTO {
	
	private String name;
	private String email;
	private String contactNo;
	private String accountNo;
	private String ifscCode;
	private String aadharCardImageFront;
	private String aadharCardImageBack;
	private String panCardImage;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getIfscCode() {
		return ifscCode;
	}
	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}
	
	public String getPanCardImage() {
		return panCardImage;
	}
	public void setPanCardImage(String panCardImage) {
		this.panCardImage = panCardImage;
	}
	public String getAadharCardImageFront() {
		return aadharCardImageFront;
	}
	public void setAadharCardImageFront(String aadharCardImageFront) {
		this.aadharCardImageFront = aadharCardImageFront;
	}
	public String getAadharCardImageBack() {
		return aadharCardImageBack;
	}
	public void setAadharCardImageBack(String aadharCardImageBack) {
		this.aadharCardImageBack = aadharCardImageBack;
	}
	
	

	
}
