package com.msscard.model;

import java.util.ArrayList;

public class CabinClasses {

	private ArrayList<BookingClass> bookingClasses;
	private String CabinType;
	
	public ArrayList<BookingClass> getBookingClasses() {
		return bookingClasses;
	}
	public void setBookingClasses(ArrayList<BookingClass> bookingClasses) {
		this.bookingClasses = bookingClasses;
	}
	public String getCabinType() {
		return CabinType;
	}
	public void setCabinType(String cabinType) {
		CabinType = cabinType;
	}
	
	
}
