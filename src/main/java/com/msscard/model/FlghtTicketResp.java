package com.msscard.model;

import java.util.List;

import com.msscard.entity.FlightTicket;
import com.msscard.entity.FlightTravellers;

public class FlghtTicketResp {

	private List<FlightTravellers> travellerDetails;
	
	private FlightTicket flightTicket;

	public List<FlightTravellers> getTravellerDetails() {
		return travellerDetails;
	}

	public void setTravellerDetails(List<FlightTravellers> travellerDetails) {
		this.travellerDetails = travellerDetails;
	}

	public FlightTicket getFlightTicket() {
		return flightTicket;
	}

	public void setFlightTicket(FlightTicket flightTicket) {
		this.flightTicket = flightTicket;
	}
	
	
}
