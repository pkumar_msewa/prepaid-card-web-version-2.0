package com.msscard.model;

import java.util.Date;

import com.msscard.entity.MUser;

public class AgentTransactionListDTO {
	
	private double amount;
	
	private Date created;
	
	private MUser user;
	
	private boolean physicalCard;
	
	private MUser agent;

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public MUser getUser() {
		return user;
	}

	public void setUser(MUser user) {
		this.user = user;
	}

	public boolean isPhysicalCard() {
		return physicalCard;
	}

	public void setPhysicalCard(boolean physicalCard) {
		this.physicalCard = physicalCard;
	}

	public MUser getAgent() {
		return agent;
	}

	public void setAgent(MUser agent) {
		this.agent = agent;
	}
	
	
	

	
	
}
