package com.msscard.model.error;

import com.msscard.model.ResponseStatus;

public class TransactionValidationError {

	private boolean isValid;
	private String message;
	private String code;
	
	
	
	public String getCode() {
		return code;
	}
	public void setCode(ResponseStatus code) {
		this.code = code.getValue();
		this.message=code.getKey();
	}
	public boolean isValid() {
		return isValid;
	}
	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
