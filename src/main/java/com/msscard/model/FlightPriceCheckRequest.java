package com.msscard.model;

import java.util.ArrayList;

public class FlightPriceCheckRequest {

	private String sessionId;
	private String adults;
	private String cabin;
	private String childs;
	private String engineID;
	private String traceId;
	private String origin;
	private String destination;
	private String beginDate;
	private String infants;
	private String tripType;
	private String bondType;
	private String boundType;
	private String isBaggageFare;
	private boolean baggageFare;
	private String isSSR;
	private String itineraryKey;
	private String journeyTime;
	private String airlineName;
	private String amount;
	private String arrivalDate;
	private String arrivalTerminal;
	private String arrivalTime;
	private String baggageUnit;
	private String baggageWeight;
	private String capacity;
	private String carrierCode; 
	private String currencyCode;
	private String departureDate;
	private String departureTerminal;
	private String departureTime;
	private String duration;
	private String fareClassOfService;
	private String flightName;
	private String flightNumber;
	private String isConnecting;
	private String sold;
	private String status;
	private String basicFare;
	private String exchangeRate;
	private String baseTransactionAmount;
	private String cancelPenalty;
	private String changePenalty;
	private String chargeCode;
	private String chargeType;
	private String markUP;
	private String paxType;
	private String refundable;
	private String totalFare;
	private String totalTax;
	private String transactionAmount;
	private String totalFareWithOutMarkUp;
	private String totalTaxWithOutMarkUp;
	private String fareRule;
	private String isCache;
	private boolean cache;
	private String isHoldBooking;
	private String isInternational;
	private String isRoundTrip;
	private String isSpecial;
	private String isSpecialId;
	private boolean holdBooking;
	private boolean international;
	private boolean roundTrip;
	private boolean special;
	private boolean specialId;
	private String journeyIndex;
	private boolean nearByAirport;
	private String onewayflight;
	private String endDate;
	private ArrayList<Fare> fares;
	private String fareBasisCode;
	private ArrayList<Legs> legs;
	private ArrayList<Bonds> bonds;
	private String colNo;
	
	
	private String jsonbonds;
	private String jsonpaxfare;
	 
	private String data;
	private String searchId;
	
	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getAdults() {
		return adults;
	}

	public void setAdults(String adults) {
		this.adults = adults;
	}

	public String getCabin() {
		return cabin;
	}

	public void setCabin(String cabin) {
		this.cabin = cabin;
	}

	public String getChilds() {
		return childs;
	}

	public void setChilds(String childs) {
		this.childs = childs;
	}

	public String getEngineID() {
		return engineID;
	}

	public void setEngineID(String engineID) {
		this.engineID = engineID;
	}

	public String getTraceId() {
		return traceId;
	}

	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}

	public String getInfants() {
		return infants;
	}

	public void setInfants(String infants) {
		this.infants = infants;
	}

	public String getTripType() {
		return tripType;
	}

	public void setTripType(String tripType) {
		this.tripType = tripType;
	}

	public String getBondType() {
		return bondType;
	}

	public void setBondType(String bondType) {
		this.bondType = bondType;
	}

	public String getBoundType() {
		return boundType;
	}

	public void setBoundType(String boundType) {
		this.boundType = boundType;
	}

	public String getIsBaggageFare() {
		return isBaggageFare;
	}

	public void setIsBaggageFare(String isBaggageFare) {
		this.isBaggageFare = isBaggageFare;
	}

	public boolean isBaggageFare() {
		return baggageFare;
	}

	public void setBaggageFare(boolean baggageFare) {
		this.baggageFare = baggageFare;
	}

	public String getIsSSR() {
		return isSSR;
	}

	public void setIsSSR(String isSSR) {
		this.isSSR = isSSR;
	}

	public String getItineraryKey() {
		return itineraryKey;
	}

	public void setItineraryKey(String itineraryKey) {
		this.itineraryKey = itineraryKey;
	}

	public String getJourneyTime() {
		return journeyTime;
	}

	public void setJourneyTime(String journeyTime) {
		this.journeyTime = journeyTime;
	}

	public String getAirlineName() {
		return airlineName;
	}

	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public String getArrivalTerminal() {
		return arrivalTerminal;
	}

	public void setArrivalTerminal(String arrivalTerminal) {
		this.arrivalTerminal = arrivalTerminal;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getBaggageUnit() {
		return baggageUnit;
	}

	public void setBaggageUnit(String baggageUnit) {
		this.baggageUnit = baggageUnit;
	}

	public String getBaggageWeight() {
		return baggageWeight;
	}

	public void setBaggageWeight(String baggageWeight) {
		this.baggageWeight = baggageWeight;
	}

	public String getCapacity() {
		return capacity;
	}

	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public String getDepartureTerminal() {
		return departureTerminal;
	}

	public void setDepartureTerminal(String departureTerminal) {
		this.departureTerminal = departureTerminal;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getFareClassOfService() {
		return fareClassOfService;
	}

	public void setFareClassOfService(String fareClassOfService) {
		this.fareClassOfService = fareClassOfService;
	}

	public String getFlightName() {
		return flightName;
	}

	public void setFlightName(String flightName) {
		this.flightName = flightName;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getIsConnecting() {
		return isConnecting;
	}

	public void setIsConnecting(String isConnecting) {
		this.isConnecting = isConnecting;
	}

	public String getSold() {
		return sold;
	}

	public void setSold(String sold) {
		this.sold = sold;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBasicFare() {
		return basicFare;
	}

	public void setBasicFare(String basicFare) {
		this.basicFare = basicFare;
	}

	public String getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(String exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public String getBaseTransactionAmount() {
		return baseTransactionAmount;
	}

	public void setBaseTransactionAmount(String baseTransactionAmount) {
		this.baseTransactionAmount = baseTransactionAmount;
	}

	public String getCancelPenalty() {
		return cancelPenalty;
	}

	public void setCancelPenalty(String cancelPenalty) {
		this.cancelPenalty = cancelPenalty;
	}

	public String getChangePenalty() {
		return changePenalty;
	}

	public void setChangePenalty(String changePenalty) {
		this.changePenalty = changePenalty;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	public String getMarkUP() {
		return markUP;
	}

	public void setMarkUP(String markUP) {
		this.markUP = markUP;
	}

	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	public String getRefundable() {
		return refundable;
	}

	public void setRefundable(String refundable) {
		this.refundable = refundable;
	}

	public String getTotalFare() {
		return totalFare;
	}

	public void setTotalFare(String totalFare) {
		this.totalFare = totalFare;
	}

	public String getTotalTax() {
		return totalTax;
	}

	public void setTotalTax(String totalTax) {
		this.totalTax = totalTax;
	}

	public String getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(String transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public String getTotalFareWithOutMarkUp() {
		return totalFareWithOutMarkUp;
	}

	public void setTotalFareWithOutMarkUp(String totalFareWithOutMarkUp) {
		this.totalFareWithOutMarkUp = totalFareWithOutMarkUp;
	}

	public String getTotalTaxWithOutMarkUp() {
		return totalTaxWithOutMarkUp;
	}

	public void setTotalTaxWithOutMarkUp(String totalTaxWithOutMarkUp) {
		this.totalTaxWithOutMarkUp = totalTaxWithOutMarkUp;
	}

	public String getFareRule() {
		return fareRule;
	}

	public void setFareRule(String fareRule) {
		this.fareRule = fareRule;
	}

	public String getIsCache() {
		return isCache;
	}

	public void setIsCache(String isCache) {
		this.isCache = isCache;
	}

	public boolean isCache() {
		return cache;
	}

	public void setCache(boolean cache) {
		this.cache = cache;
	}

	public String getIsHoldBooking() {
		return isHoldBooking;
	}

	public void setIsHoldBooking(String isHoldBooking) {
		this.isHoldBooking = isHoldBooking;
	}

	public String getIsInternational() {
		return isInternational;
	}

	public void setIsInternational(String isInternational) {
		this.isInternational = isInternational;
	}

	public String getIsRoundTrip() {
		return isRoundTrip;
	}

	public void setIsRoundTrip(String isRoundTrip) {
		this.isRoundTrip = isRoundTrip;
	}

	public String getIsSpecial() {
		return isSpecial;
	}

	public void setIsSpecial(String isSpecial) {
		this.isSpecial = isSpecial;
	}

	public String getIsSpecialId() {
		return isSpecialId;
	}

	public void setIsSpecialId(String isSpecialId) {
		this.isSpecialId = isSpecialId;
	}

	public boolean isHoldBooking() {
		return holdBooking;
	}

	public void setHoldBooking(boolean holdBooking) {
		this.holdBooking = holdBooking;
	}

	public boolean isInternational() {
		return international;
	}

	public void setInternational(boolean international) {
		this.international = international;
	}

	public boolean isRoundTrip() {
		return roundTrip;
	}

	public void setRoundTrip(boolean roundTrip) {
		this.roundTrip = roundTrip;
	}

	public boolean isSpecial() {
		return special;
	}

	public void setSpecial(boolean special) {
		this.special = special;
	}

	public boolean isSpecialId() {
		return specialId;
	}

	public void setSpecialId(boolean specialId) {
		this.specialId = specialId;
	}

	public String getJourneyIndex() {
		return journeyIndex;
	}

	public void setJourneyIndex(String journeyIndex) {
		this.journeyIndex = journeyIndex;
	}

	public boolean isNearByAirport() {
		return nearByAirport;
	}

	public void setNearByAirport(boolean nearByAirport) {
		this.nearByAirport = nearByAirport;
	}

	public String getOnewayflight() {
		return onewayflight;
	}

	public void setOnewayflight(String onewayflight) {
		this.onewayflight = onewayflight;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public ArrayList<Fare> getFares() {
		return fares;
	}

	public void setFares(ArrayList<Fare> fares) {
		this.fares = fares;
	}

	public String getFareBasisCode() {
		return fareBasisCode;
	}

	public void setFareBasisCode(String fareBasisCode) {
		this.fareBasisCode = fareBasisCode;
	}

	public ArrayList<Legs> getLegs() {
		return legs;
	}

	public void setLegs(ArrayList<Legs> legs) {
		this.legs = legs;
	}

	public ArrayList<Bonds> getBonds() {
		return bonds;
	}

	public void setBonds(ArrayList<Bonds> bonds) {
		this.bonds = bonds;
	}

	public String getColNo() {
		return colNo;
	}

	public void setColNo(String colNo) {
		this.colNo = colNo;
	}

	public String getJsonbonds() {
		return jsonbonds;
	}

	public void setJsonbonds(String jsonbonds) {
		this.jsonbonds = jsonbonds;
	}

	public String getJsonpaxfare() {
		return jsonpaxfare;
	}

	public void setJsonpaxfare(String jsonpaxfare) {
		this.jsonpaxfare = jsonpaxfare;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getSearchId() {
		return searchId;
	}

	public void setSearchId(String searchId) {
		this.searchId = searchId;
	}
	
	
}
