package com.msscard.model;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.msscard.app.model.request.SessionDTO;

public class FlightPaymentRequest extends SessionDTO{

	private String ticketNumber;
	private String firstName;
	private String bookingRefId;
	private String transactionRefNomdex;
	private double paymentAmount;
	private String ticketDetails;
	@Enumerated(EnumType.STRING)
	private Status statusflight;
	@Enumerated(EnumType.STRING)
	private Status paymentstatus;
	private String paymentmethod;
	private String transactionRefno;
	private boolean success;
	private String email;
	private String mobile;
	public String getTicketNumber() {
		return ticketNumber;
	}
	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getBookingRefId() {
		return bookingRefId;
	}
	public void setBookingRefId(String bookingRefId) {
		this.bookingRefId = bookingRefId;
	}
	public String getTransactionRefNomdex() {
		return transactionRefNomdex;
	}
	public void setTransactionRefNomdex(String transactionRefNomdex) {
		this.transactionRefNomdex = transactionRefNomdex;
	}
	public double getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	public String getTicketDetails() {
		return ticketDetails;
	}
	public void setTicketDetails(String ticketDetails) {
		this.ticketDetails = ticketDetails;
	}
	public Status getStatusflight() {
		return statusflight;
	}
	public void setStatusflight(Status statusflight) {
		this.statusflight = statusflight;
	}
	public Status getPaymentstatus() {
		return paymentstatus;
	}
	public void setPaymentstatus(Status paymentstatus) {
		this.paymentstatus = paymentstatus;
	}
	public String getPaymentmethod() {
		return paymentmethod;
	}
	public void setPaymentmethod(String paymentmethod) {
		this.paymentmethod = paymentmethod;
	}
	public String getTransactionRefno() {
		return transactionRefno;
	}
	public void setTransactionRefno(String transactionRefno) {
		this.transactionRefno = transactionRefno;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	
}
