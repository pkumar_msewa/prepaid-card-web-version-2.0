package com.msscard.model;

import java.util.List;

import com.msscard.entity.MUserDetails;

public class FlightTicketAdminDTO {

	private String userDetails;
	private String bookingStatus;
	private String transactionStatus;
	private String bookingRefId;
	private double paymentAmount;
	private String userName;
	private String journeyDate;
	private String arrTime;
	private double totalFare;
	private String transactionRefNo;
	private String status;
	private MUserDetails userDetail;
	private String txnStatus;
	private String txnDate;
	private double commsissionAmount;
	private long accountNumber;
	private long flightTicketId;
	private List<TravellerFlightDetails> travellerFlightDetails;
	private TicketObj oneWay;
	private TicketObj roundWay;
	private String TicketDetails;
	private String flightNameOnward;
	private String flightNameReturn;
	private String tripType;
	public String getUserDetails() {
		return userDetails;
	}
	public void setUserDetails(String userDetails) {
		this.userDetails = userDetails;
	}
	public String getBookingStatus() {
		return bookingStatus;
	}
	public void setBookingStatus(String bookingStatus) {
		this.bookingStatus = bookingStatus;
	}
	public String getTransactionStatus() {
		return transactionStatus;
	}
	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
	public String getBookingRefId() {
		return bookingRefId;
	}
	public void setBookingRefId(String bookingRefId) {
		this.bookingRefId = bookingRefId;
	}
	public double getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getJourneyDate() {
		return journeyDate;
	}
	public void setJourneyDate(String journeyDate) {
		this.journeyDate = journeyDate;
	}
	public String getArrTime() {
		return arrTime;
	}
	public void setArrTime(String arrTime) {
		this.arrTime = arrTime;
	}
	public double getTotalFare() {
		return totalFare;
	}
	public void setTotalFare(double totalFare) {
		this.totalFare = totalFare;
	}
	public String getTransactionRefNo() {
		return transactionRefNo;
	}
	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public MUserDetails getUserDetail() {
		return userDetail;
	}
	public void setUserDetail(MUserDetails userDetail) {
		this.userDetail = userDetail;
	}
	public String getTxnStatus() {
		return txnStatus;
	}
	public void setTxnStatus(String txnStatus) {
		this.txnStatus = txnStatus;
	}
	public String getTxnDate() {
		return txnDate;
	}
	public void setTxnDate(String txnDate) {
		this.txnDate = txnDate;
	}
	public double getCommsissionAmount() {
		return commsissionAmount;
	}
	public void setCommsissionAmount(double commsissionAmount) {
		this.commsissionAmount = commsissionAmount;
	}
	public long getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}
	public long getFlightTicketId() {
		return flightTicketId;
	}
	public void setFlightTicketId(long flightTicketId) {
		this.flightTicketId = flightTicketId;
	}
	public List<TravellerFlightDetails> getTravellerFlightDetails() {
		return travellerFlightDetails;
	}
	public void setTravellerFlightDetails(List<TravellerFlightDetails> travellerFlightDetails) {
		this.travellerFlightDetails = travellerFlightDetails;
	}
	public TicketObj getOneWay() {
		return oneWay;
	}
	public void setOneWay(TicketObj oneWay) {
		this.oneWay = oneWay;
	}
	public TicketObj getRoundWay() {
		return roundWay;
	}
	public void setRoundWay(TicketObj roundWay) {
		this.roundWay = roundWay;
	}
	public String getTicketDetails() {
		return TicketDetails;
	}
	public void setTicketDetails(String ticketDetails) {
		TicketDetails = ticketDetails;
	}
	public String getFlightNameOnward() {
		return flightNameOnward;
	}
	public void setFlightNameOnward(String flightNameOnward) {
		this.flightNameOnward = flightNameOnward;
	}
	public String getFlightNameReturn() {
		return flightNameReturn;
	}
	public void setFlightNameReturn(String flightNameReturn) {
		this.flightNameReturn = flightNameReturn;
	}
	public String getTripType() {
		return tripType;
	}
	public void setTripType(String tripType) {
		this.tripType = tripType;
	}
	
	
}
