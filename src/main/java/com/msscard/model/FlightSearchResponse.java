package com.msscard.model;

import java.util.ArrayList;

public class FlightSearchResponse {

	private Errors errors;
	private ArrayList<Journeys> journeys;
	
	public Errors getErrors() {
		return errors;
	}
	public void setErrors(Errors errors) {
		this.errors = errors;
	}
	public ArrayList<Journeys> getJourneys() {
		return journeys;
	}
	public void setJourneys(ArrayList<Journeys> journeys) {
		this.journeys = journeys;
	}
	
	
}
