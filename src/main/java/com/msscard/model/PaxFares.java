package com.msscard.model;

import java.util.ArrayList;

public class PaxFares {

	private String baggageUnit;
	private String baggageWeight;
	private double baseTransactionAmount;
	private double basicFare;
	private double cancelPenalty;
	private double changePenalty;
	private String equivCurrencyCode;
	private ArrayList<BookFare> bookFares;
	private String fareBasisCode;
	private String fareInfoKey;
	private String fareInfoValue;
	private double markUP;
	private String paxType;
	private boolean refundable;
	private double totalFare;
	private double totalTax;
	private double transactionAmount;
	public String getBaggageUnit() {
		return baggageUnit;
	}
	public void setBaggageUnit(String baggageUnit) {
		this.baggageUnit = baggageUnit;
	}
	public String getBaggageWeight() {
		return baggageWeight;
	}
	public void setBaggageWeight(String baggageWeight) {
		this.baggageWeight = baggageWeight;
	}
	public double getBaseTransactionAmount() {
		return baseTransactionAmount;
	}
	public void setBaseTransactionAmount(double baseTransactionAmount) {
		this.baseTransactionAmount = baseTransactionAmount;
	}
	public double getBasicFare() {
		return basicFare;
	}
	public void setBasicFare(double basicFare) {
		this.basicFare = basicFare;
	}
	public double getCancelPenalty() {
		return cancelPenalty;
	}
	public void setCancelPenalty(double cancelPenalty) {
		this.cancelPenalty = cancelPenalty;
	}
	public double getChangePenalty() {
		return changePenalty;
	}
	public void setChangePenalty(double changePenalty) {
		this.changePenalty = changePenalty;
	}
	public String getEquivCurrencyCode() {
		return equivCurrencyCode;
	}
	public void setEquivCurrencyCode(String equivCurrencyCode) {
		this.equivCurrencyCode = equivCurrencyCode;
	}
	public ArrayList<BookFare> getBookFares() {
		return bookFares;
	}
	public void setBookFares(ArrayList<BookFare> bookFares) {
		this.bookFares = bookFares;
	}
	public String getFareBasisCode() {
		return fareBasisCode;
	}
	public void setFareBasisCode(String fareBasisCode) {
		this.fareBasisCode = fareBasisCode;
	}
	public String getFareInfoKey() {
		return fareInfoKey;
	}
	public void setFareInfoKey(String fareInfoKey) {
		this.fareInfoKey = fareInfoKey;
	}
	public String getFareInfoValue() {
		return fareInfoValue;
	}
	public void setFareInfoValue(String fareInfoValue) {
		this.fareInfoValue = fareInfoValue;
	}
	public double getMarkUP() {
		return markUP;
	}
	public void setMarkUP(double markUP) {
		this.markUP = markUP;
	}
	public String getPaxType() {
		return paxType;
	}
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}
	public boolean isRefundable() {
		return refundable;
	}
	public void setRefundable(boolean refundable) {
		this.refundable = refundable;
	}
	public double getTotalFare() {
		return totalFare;
	}
	public void setTotalFare(double totalFare) {
		this.totalFare = totalFare;
	}
	public double getTotalTax() {
		return totalTax;
	}
	public void setTotalTax(double totalTax) {
		this.totalTax = totalTax;
	}
	public double getTransactionAmount() {
		return transactionAmount;
	}
	public void setTransactionAmount(double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	
	
}
