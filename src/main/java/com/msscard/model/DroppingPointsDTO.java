package com.msscard.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class DroppingPointsDTO {
	
	@JsonProperty("dpId")
	private String dpId;
	
	@JsonProperty("dpName")
	private String dpName;
	
	@JsonProperty("location")
	private String location;
	
	@JsonProperty("prime")
	private String prime;
	
	@JsonProperty("dpTime")
	private String dpTime;
	
	@JsonProperty("contactNumber")
	private String contactNumber;
	
	public String getDpId() {
		return dpId;
	}
	public void setDpId(String dpId) {
		this.dpId = dpId;
	}
	public String getDpName() {
		return dpName;
	}
	public void setDpName(String dpName) {
		this.dpName = dpName;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getPrime() {
		return prime;
	}
	public void setPrime(String prime) {
		this.prime = prime;
	}
	public String getDpTime() {
		return dpTime;
	}
	public void setDpTime(String dpTime) {
		this.dpTime = dpTime;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	
	
}
