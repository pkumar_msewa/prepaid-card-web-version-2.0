package com.msscard.model;

import java.util.ArrayList;

public class Bonds {
	
	private String boundType;
	private String itineraryKey;
	private boolean isBaggageFare;
	private boolean isSsrFare;
	private String journeyTime;
	private ArrayList<Legs> legs;
	private String addOnDetail;
	
	public String getBoundType() {
		return boundType;
	}
	public void setBoundType(String boundType) {
		this.boundType = boundType;
	}
	public String getItineraryKey() {
		return itineraryKey;
	}
	public void setItineraryKey(String itineraryKey) {
		this.itineraryKey = itineraryKey;
	}
	public boolean isBaggageFare() {
		return isBaggageFare;
	}
	public void setBaggageFare(boolean isBaggageFare) {
		this.isBaggageFare = isBaggageFare;
	}
	public boolean isSsrFare() {
		return isSsrFare;
	}
	public void setSsrFare(boolean isSsrFare) {
		this.isSsrFare = isSsrFare;
	}
	public String getJourneyTime() {
		return journeyTime;
	}
	public void setJourneyTime(String journeyTime) {
		this.journeyTime = journeyTime;
	}
	public ArrayList<Legs> getLegs() {
		return legs;
	}
	public void setLegs(ArrayList<Legs> legs) {
		this.legs = legs;
	}
	public String getAddOnDetail() {
		return addOnDetail;
	}
	public void setAddOnDetail(String addOnDetail) {
		this.addOnDetail = addOnDetail;
	}
	
	
}
