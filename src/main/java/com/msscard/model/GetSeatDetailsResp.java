package com.msscard.model;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class GetSeatDetailsResp {
	
	@JsonProperty("status")
	private String status;
	
	@JsonProperty("code")
	private String code;
	
	@JsonProperty("message")
	private String message;

	@JsonProperty("upperShow")
	private boolean upperShow;
	
	@JsonProperty("lowerShow")
	private boolean lowerShow;

	@JsonProperty("lower")
	private SeatObject lower;
	
	@JsonProperty("upper")
	private SeatObject upper;
	
	@JsonProperty("listBoardingPoints")
	private List<BDPointDTO> boardingPoints;

	@JsonProperty("listDropingPoints")
	private List<DPPointsDTO> droppingPoints;
	
	@JsonProperty("maxcolumn")
	private int maxcolumn;
	
	@JsonProperty("maxrow")
	private int maxrow;

	@JsonProperty("seatAcFare")
	private double seatAcFare;

	@JsonProperty("seatNacFare")
	private double seatNacFare;

	@JsonProperty("sleepAcFare")
	private double sleepAcFare;
	
	@JsonProperty("sleepNacFare")
	private double sleepNacFare;
	
	@JsonProperty("minFare")
	private double minFare;
	
	@JsonProperty("maxFare")
	private double maxFare;
	
	@JsonProperty("cancelPolicyList")
	private Object cancelPolicyList;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isUpperShow() {
		return upperShow;
	}

	public void setUpperShow(boolean upperShow) {
		this.upperShow = upperShow;
	}

	public boolean isLowerShow() {
		return lowerShow;
	}

	public void setLowerShow(boolean lowerShow) {
		this.lowerShow = lowerShow;
	}

	public SeatObject getLower() {
		return lower;
	}

	public void setLower(SeatObject lower) {
		this.lower = lower;
	}

	public SeatObject getUpper() {
		return upper;
	}

	public void setUpper(SeatObject upper) {
		this.upper = upper;
	}

	public List<BDPointDTO> getBoardingPoints() {
		return boardingPoints;
	}

	public void setBoardingPoints(List<BDPointDTO> boardingPoints) {
		this.boardingPoints = boardingPoints;
	}

	public List<DPPointsDTO> getDroppingPoints() {
		return droppingPoints;
	}

	public void setDroppingPoints(List<DPPointsDTO> droppingPoints) {
		this.droppingPoints = droppingPoints;
	}

	public int getMaxcolumn() {
		return maxcolumn;
	}

	public void setMaxcolumn(int maxcolumn) {
		this.maxcolumn = maxcolumn;
	}

	public int getMaxrow() {
		return maxrow;
	}

	public void setMaxrow(int maxrow) {
		this.maxrow = maxrow;
	}

	public double getSeatAcFare() {
		return seatAcFare;
	}

	public void setSeatAcFare(double seatAcFare) {
		this.seatAcFare = seatAcFare;
	}

	public double getSeatNacFare() {
		return seatNacFare;
	}

	public void setSeatNacFare(double seatNacFare) {
		this.seatNacFare = seatNacFare;
	}

	public double getSleepAcFare() {
		return sleepAcFare;
	}

	public void setSleepAcFare(double sleepAcFare) {
		this.sleepAcFare = sleepAcFare;
	}

	public double getSleepNacFare() {
		return sleepNacFare;
	}

	public void setSleepNacFare(double sleepNacFare) {
		this.sleepNacFare = sleepNacFare;
	}

	public double getMinFare() {
		return minFare;
	}

	public void setMinFare(double minFare) {
		this.minFare = minFare;
	}

	public double getMaxFare() {
		return maxFare;
	}

	public void setMaxFare(double maxFare) {
		this.maxFare = maxFare;
	}

	public Object getCancelPolicyList() {
		return cancelPolicyList;
	}

	public void setCancelPolicyList(Object cancelPolicyList) {
		this.cancelPolicyList = cancelPolicyList;
	}
}