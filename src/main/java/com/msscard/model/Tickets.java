package com.msscard.model;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

public class Tickets {

	@JsonProperty("Oneway")
	private List<FlightTicketDeatilsDTO> oneway;
	
	@JsonProperty("Roundway")
	private List<FlightTicketDeatilsDTO> roundway;

	public List<FlightTicketDeatilsDTO> getOneway() {
		return oneway;
	}

	public void setOneway(List<FlightTicketDeatilsDTO> oneway) {
		this.oneway = oneway;
	}

	public List<FlightTicketDeatilsDTO> getRoundway() {
		return roundway;
	}

	public void setRoundway(List<FlightTicketDeatilsDTO> roundway) {
		this.roundway = roundway;
	}
	
	
	
}
