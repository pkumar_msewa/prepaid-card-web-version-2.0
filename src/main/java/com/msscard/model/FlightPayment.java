package com.msscard.model;

import java.util.List;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.msscard.app.model.request.SessionDTO;

public class FlightPayment extends SessionDTO{

	private String bookingRefId;
	private String mdexTxnRefNo;
	private double paymentAmount;
	private String ticketDetails;
	@Enumerated(EnumType.STRING)
	private Status flightStatus;
	@Enumerated(EnumType.STRING)
	private Status paymentStatus;
	private String paymentMethod;
	private String txnRefno;
	private String status;
	private String email;
	private String mobile;
	private String pnrNo;
	private boolean success;
	private String merchantRefNo;
	private String source;
	private String destination;
	private long flightId;
	private List<FlightTravellerDetailsDTO> travellerDetails;
	private String convenienceFee;
	private String baseFare;
	public String getBookingRefId() {
		return bookingRefId;
	}
	public void setBookingRefId(String bookingRefId) {
		this.bookingRefId = bookingRefId;
	}
	public String getMdexTxnRefNo() {
		return mdexTxnRefNo;
	}
	public void setMdexTxnRefNo(String mdexTxnRefNo) {
		this.mdexTxnRefNo = mdexTxnRefNo;
	}
	public double getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	public String getTicketDetails() {
		return ticketDetails;
	}
	public void setTicketDetails(String ticketDetails) {
		this.ticketDetails = ticketDetails;
	}
	public Status getFlightStatus() {
		return flightStatus;
	}
	public void setFlightStatus(Status flightStatus) {
		this.flightStatus = flightStatus;
	}
	public Status getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(Status paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getTxnRefno() {
		return txnRefno;
	}
	public void setTxnRefno(String txnRefno) {
		this.txnRefno = txnRefno;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPnrNo() {
		return pnrNo;
	}
	public void setPnrNo(String pnrNo) {
		this.pnrNo = pnrNo;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getMerchantRefNo() {
		return merchantRefNo;
	}
	public void setMerchantRefNo(String merchantRefNo) {
		this.merchantRefNo = merchantRefNo;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public long getFlightId() {
		return flightId;
	}
	public void setFlightId(long flightId) {
		this.flightId = flightId;
	}
	public List<FlightTravellerDetailsDTO> getTravellerDetails() {
		return travellerDetails;
	}
	public void setTravellerDetails(List<FlightTravellerDetailsDTO> travellerDetails) {
		this.travellerDetails = travellerDetails;
	}
	public String getConvenienceFee() {
		return convenienceFee;
	}
	public void setConvenienceFee(String convenienceFee) {
		this.convenienceFee = convenienceFee;
	}
	public String getBaseFare() {
		return baseFare;
	}
	public void setBaseFare(String baseFare) {
		this.baseFare = baseFare;
	}
	
	
}
