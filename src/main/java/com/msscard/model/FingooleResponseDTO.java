package com.msscard.model;

public class FingooleResponseDTO {
	
	private String coiurl;
	private String coino;
	private String mobileNo;
	private String customerName;
	private String refNo;
	public String getCoiurl() {
		return coiurl;
	}
	public void setCoiurl(String coiurl) {
		this.coiurl = coiurl;
	}
	public String getCoino() {
		return coino;
	}
	public void setCoino(String coino) {
		this.coino = coino;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	
	
}
