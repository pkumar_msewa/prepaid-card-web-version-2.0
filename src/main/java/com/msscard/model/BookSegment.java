package com.msscard.model;

import java.util.ArrayList;

public class BookSegment {

	private String bondType;
	private ArrayList<Bonds> bonds;
	private String deeplink;
	private EngineID engineID;
	private Fare fares;
	private String fareIndicator;
	private String fareRule;
	private boolean isBaggageFare;
	private boolean isCache;
	private boolean isHoldBooking;
	private boolean isInternational;
	private boolean isRoundTrip;
	private boolean isSpecial;
	private boolean isSpecialId;
	private String itineraryKey;
	private String memoryCreationTime;
	private int journeyIndex;
	private boolean nearByAirport;
	private String remark;
	private String searchId;
	public String getBondType() {
		return bondType;
	}
	public void setBondType(String bondType) {
		this.bondType = bondType;
	}
	public ArrayList<Bonds> getBonds() {
		return bonds;
	}
	public void setBonds(ArrayList<Bonds> bonds) {
		this.bonds = bonds;
	}
	public String getDeeplink() {
		return deeplink;
	}
	public void setDeeplink(String deeplink) {
		this.deeplink = deeplink;
	}
	public EngineID getEngineID() {
		return engineID;
	}
	public void setEngineID(EngineID engineID) {
		this.engineID = engineID;
	}
	public Fare getFares() {
		return fares;
	}
	public void setFares(Fare fares) {
		this.fares = fares;
	}
	public String getFareIndicator() {
		return fareIndicator;
	}
	public void setFareIndicator(String fareIndicator) {
		this.fareIndicator = fareIndicator;
	}
	public String getFareRule() {
		return fareRule;
	}
	public void setFareRule(String fareRule) {
		this.fareRule = fareRule;
	}
	public boolean isBaggageFare() {
		return isBaggageFare;
	}
	public void setBaggageFare(boolean isBaggageFare) {
		this.isBaggageFare = isBaggageFare;
	}
	public boolean isCache() {
		return isCache;
	}
	public void setCache(boolean isCache) {
		this.isCache = isCache;
	}
	public boolean isHoldBooking() {
		return isHoldBooking;
	}
	public void setHoldBooking(boolean isHoldBooking) {
		this.isHoldBooking = isHoldBooking;
	}
	public boolean isInternational() {
		return isInternational;
	}
	public void setInternational(boolean isInternational) {
		this.isInternational = isInternational;
	}
	public boolean isRoundTrip() {
		return isRoundTrip;
	}
	public void setRoundTrip(boolean isRoundTrip) {
		this.isRoundTrip = isRoundTrip;
	}
	public boolean isSpecial() {
		return isSpecial;
	}
	public void setSpecial(boolean isSpecial) {
		this.isSpecial = isSpecial;
	}
	public boolean isSpecialId() {
		return isSpecialId;
	}
	public void setSpecialId(boolean isSpecialId) {
		this.isSpecialId = isSpecialId;
	}
	public String getItineraryKey() {
		return itineraryKey;
	}
	public void setItineraryKey(String itineraryKey) {
		this.itineraryKey = itineraryKey;
	}
	public String getMemoryCreationTime() {
		return memoryCreationTime;
	}
	public void setMemoryCreationTime(String memoryCreationTime) {
		this.memoryCreationTime = memoryCreationTime;
	}
	public int getJourneyIndex() {
		return journeyIndex;
	}
	public void setJourneyIndex(int journeyIndex) {
		this.journeyIndex = journeyIndex;
	}
	public boolean isNearByAirport() {
		return nearByAirport;
	}
	public void setNearByAirport(boolean nearByAirport) {
		this.nearByAirport = nearByAirport;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getSearchId() {
		return searchId;
	}
	public void setSearchId(String searchId) {
		this.searchId = searchId;
	}
	
	
}
