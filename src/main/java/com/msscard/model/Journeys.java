package com.msscard.model;

import java.util.ArrayList;

public class Journeys {

	private JourneyDetail journeyDetail;
	private ArrayList<Segment> segments;
	
	public JourneyDetail getJourneyDetail() {
		return journeyDetail;
	}
	public void setJourneyDetail(JourneyDetail journeyDetail) {
		this.journeyDetail = journeyDetail;
	}
	public ArrayList<Segment> getSegments() {
		return segments;
	}
	public void setSegments(ArrayList<Segment> segments) {
		this.segments = segments;
	}
	
	
}
