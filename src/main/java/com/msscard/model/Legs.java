package com.msscard.model;

import java.util.ArrayList;

public class Legs {

	private String aircraftCode;
	private String aircraftType;
	private String airlineName;
	private double amount;
	private String arrivalDate;
	private String arrivalTerminal;
	private String arrivalTime;
	private String availableSeat;
	private String baggageUnit;
	private String baggageWeight;
	private String boundTypes;
	private String cabin;
	private ArrayList<CabinClasses> cabinClasses;
	private String capacity;
	private String carrierCode;
	private String currencyCode;
	private String departureDate;
	private String departureTerminal;
	private String departureTime;
	private String destination;
	private String duration;
	private String fareBasisCode;
	private String fareClassOfService;
	private String flightDesignator;
	private String flightDetailRefKey;
	private String flightNumber;
	private String flightName;
	private String group;
	private boolean isConnecting;
	private String numberOfStops;
	private String origin;
	private String providerCode;
	private String remarks;
	private ArrayList<SSRDetails> ssrDetails;
	private String sold;
	private String status;
	public String getAircraftCode() {
		return aircraftCode;
	}
	public void setAircraftCode(String aircraftCode) {
		this.aircraftCode = aircraftCode;
	}
	public String getAircraftType() {
		return aircraftType;
	}
	public void setAircraftType(String aircraftType) {
		this.aircraftType = aircraftType;
	}
	public String getAirlineName() {
		return airlineName;
	}
	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public String getArrivalTerminal() {
		return arrivalTerminal;
	}
	public void setArrivalTerminal(String arrivalTerminal) {
		this.arrivalTerminal = arrivalTerminal;
	}
	public String getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public String getAvailableSeat() {
		return availableSeat;
	}
	public void setAvailableSeat(String availableSeat) {
		this.availableSeat = availableSeat;
	}
	public String getBaggageUnit() {
		return baggageUnit;
	}
	public void setBaggageUnit(String baggageUnit) {
		this.baggageUnit = baggageUnit;
	}
	public String getBaggageWeight() {
		return baggageWeight;
	}
	public void setBaggageWeight(String baggageWeight) {
		this.baggageWeight = baggageWeight;
	}
	public String getBoundTypes() {
		return boundTypes;
	}
	public void setBoundTypes(String boundTypes) {
		this.boundTypes = boundTypes;
	}
	public String getCabin() {
		return cabin;
	}
	public void setCabin(String cabin) {
		this.cabin = cabin;
	}
	public ArrayList<CabinClasses> getCabinClasses() {
		return cabinClasses;
	}
	public void setCabinClasses(ArrayList<CabinClasses> cabinClasses) {
		this.cabinClasses = cabinClasses;
	}
	public String getCapacity() {
		return capacity;
	}
	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}
	public String getCarrierCode() {
		return carrierCode;
	}
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}
	public String getDepartureTerminal() {
		return departureTerminal;
	}
	public void setDepartureTerminal(String departureTerminal) {
		this.departureTerminal = departureTerminal;
	}
	public String getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getFareBasisCode() {
		return fareBasisCode;
	}
	public void setFareBasisCode(String fareBasisCode) {
		this.fareBasisCode = fareBasisCode;
	}
	public String getFareClassOfService() {
		return fareClassOfService;
	}
	public void setFareClassOfService(String fareClassOfService) {
		this.fareClassOfService = fareClassOfService;
	}
	public String getFlightDesignator() {
		return flightDesignator;
	}
	public void setFlightDesignator(String flightDesignator) {
		this.flightDesignator = flightDesignator;
	}
	public String getFlightDetailRefKey() {
		return flightDetailRefKey;
	}
	public void setFlightDetailRefKey(String flightDetailRefKey) {
		this.flightDetailRefKey = flightDetailRefKey;
	}
	public String getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	public String getFlightName() {
		return flightName;
	}
	public void setFlightName(String flightName) {
		this.flightName = flightName;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public boolean isConnecting() {
		return isConnecting;
	}
	public void setConnecting(boolean isConnecting) {
		this.isConnecting = isConnecting;
	}
	public String getNumberOfStops() {
		return numberOfStops;
	}
	public void setNumberOfStops(String numberOfStops) {
		this.numberOfStops = numberOfStops;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getProviderCode() {
		return providerCode;
	}
	public void setProviderCode(String providerCode) {
		this.providerCode = providerCode;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public ArrayList<SSRDetails> getSsrDetails() {
		return ssrDetails;
	}
	public void setSsrDetails(ArrayList<SSRDetails> ssrDetails) {
		this.ssrDetails = ssrDetails;
	}
	public String getSold() {
		return sold;
	}
	public void setSold(String sold) {
		this.sold = sold;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
