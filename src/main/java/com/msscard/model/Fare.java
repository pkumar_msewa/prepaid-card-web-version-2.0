package com.msscard.model;

import java.util.ArrayList;

public class Fare {
	
	private double basicFare;
	private double exchangeRate;
	private ArrayList<PaxFares> paxFares;
	private double totalFareWithOutMarkUp;
	private double totalTaxWithOutMarkUp;
	public double getBasicFare() {
		return basicFare;
	}
	public void setBasicFare(double basicFare) {
		this.basicFare = basicFare;
	}
	public double getExchangeRate() {
		return exchangeRate;
	}
	public void setExchangeRate(double exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
	public ArrayList<PaxFares> getPaxFares() {
		return paxFares;
	}
	public void setPaxFares(ArrayList<PaxFares> paxFares) {
		this.paxFares = paxFares;
	}
	public double getTotalFareWithOutMarkUp() {
		return totalFareWithOutMarkUp;
	}
	public void setTotalFareWithOutMarkUp(double totalFareWithOutMarkUp) {
		this.totalFareWithOutMarkUp = totalFareWithOutMarkUp;
	}
	public double getTotalTaxWithOutMarkUp() {
		return totalTaxWithOutMarkUp;
	}
	public void setTotalTaxWithOutMarkUp(double totalTaxWithOutMarkUp) {
		this.totalTaxWithOutMarkUp = totalTaxWithOutMarkUp;
	}

	
}
