package com.msscard.model;

public class DownloadCsv {

	private String status;
	private String dateRange;
	private String requestType;
	
	
	
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDateRange() {
		return dateRange;
	}
	public void setDateRange(String dateRange) {
		this.dateRange = dateRange;
	}
	@Override
	public String toString() {
		return "DownloadCsv [status=" + status + ", dateRange=" + dateRange + ", requestType=" + requestType + "]";
	}

	
}
