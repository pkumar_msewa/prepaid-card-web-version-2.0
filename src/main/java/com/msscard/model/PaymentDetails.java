package com.msscard.model;

public class PaymentDetails {

	private double BookingAmount;
	private String BookingCurrencyCode;
	public double getBookingAmount() {
		return BookingAmount;
	}
	public void setBookingAmount(double bookingAmount) {
		BookingAmount = bookingAmount;
	}
	public String getBookingCurrencyCode() {
		return BookingCurrencyCode;
	}
	public void setBookingCurrencyCode(String bookingCurrencyCode) {
		BookingCurrencyCode = bookingCurrencyCode;
	}
	
	
}
