package com.msscard.model;

import java.util.ArrayList;

public class Travellers {

	private ArrayList<AdultTraveller> adultTravellers;

	private ArrayList<ChildTraveller> childTravellers;

	private ArrayList<InfantTraveller> infantTravellers;

	public ArrayList<AdultTraveller> getAdultTravellers() {
		return adultTravellers;
	}

	public void setAdultTravellers(ArrayList<AdultTraveller> adultTravellers) {
		this.adultTravellers = adultTravellers;
	}

	public ArrayList<ChildTraveller> getChildTravellers() {
		return childTravellers;
	}

	public void setChildTravellers(ArrayList<ChildTraveller> childTravellers) {
		this.childTravellers = childTravellers;
	}

	public ArrayList<InfantTraveller> getInfantTravellers() {
		return infantTravellers;
	}

	public void setInfantTravellers(ArrayList<InfantTraveller> infantTravellers) {
		this.infantTravellers = infantTravellers;
	}

	
	
}
