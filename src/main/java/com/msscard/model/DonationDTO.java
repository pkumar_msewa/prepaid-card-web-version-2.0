package com.msscard.model;

import java.util.List;

import com.msscard.app.model.response.CommonResponse;
import com.msscard.entity.Donation;

public class DonationDTO extends CommonResponse {
	
	private String donateeName;
	
	private String organization;
	
	private String contactNo;
	
	private String message;
	
	private String groupName;
	
	private String created;
	
	private String email;
		
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	private List<Donation> donationList;

	public String getDonateeName() {
		return donateeName;
	}

	public void setDonateeName(String donateeName) {
		this.donateeName = donateeName;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<Donation> getDonationList() {
		return donationList;
	}

	public void setDonationList(List<Donation> donationList) {
		this.donationList = donationList;
	}
	
	
	
}
