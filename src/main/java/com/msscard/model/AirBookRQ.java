package com.msscard.model;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;

import com.razorpay.constants.MdexConstants;

public class AirBookRQ implements JSONRequest{

	private String clientIp;
	private String cllientKey;
	private String clientToken;
	private String clientApiName;
	private ArrayList<BookSegment> bookSegments;
	private String engineID;
	private ArrayList<EngineID> engineIDList;
	private boolean androidBooking;
	private boolean domestic;
	private PaymentDetails paymentDetails;
	private ArrayList<FlightSearchDetails> flightSearchDetails;
	private String visatype;
	private String traceId;
	private String transactionId;
	private Travellers travellers;
	private String emailAddress;
	private String mobileNumber;
	private String spKey;
	private String sessionId;
	List<TravellerFlightDetails> travellerDetails;
	private String ticketDetails;
	private String paymentMethod;
	private String data;
	public String getClientIp() {
		return clientIp;
	}
	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}
	public String getCllientKey() {
		return cllientKey;
	}
	public void setCllientKey(String cllientKey) {
		this.cllientKey = cllientKey;
	}
	public String getClientToken() {
		return clientToken;
	}
	public void setClientToken(String clientToken) {
		this.clientToken = clientToken;
	}
	public String getClientApiName() {
		return clientApiName;
	}
	public void setClientApiName(String clientApiName) {
		this.clientApiName = clientApiName;
	}
	public ArrayList<BookSegment> getBookSegments() {
		return bookSegments;
	}
	public void setBookSegments(ArrayList<BookSegment> bookSegments) {
		this.bookSegments = bookSegments;
	}
	public String getEngineID() {
		return engineID;
	}
	public void setEngineID(String engineID) {
		this.engineID = engineID;
	}
	public ArrayList<EngineID> getEngineIDList() {
		return engineIDList;
	}
	public void setEngineIDList(ArrayList<EngineID> engineIDList) {
		this.engineIDList = engineIDList;
	}
	public boolean isAndroidBooking() {
		return androidBooking;
	}
	public void setAndroidBooking(boolean androidBooking) {
		this.androidBooking = androidBooking;
	}
	public boolean isDomestic() {
		return domestic;
	}
	public void setDomestic(boolean domestic) {
		this.domestic = domestic;
	}
	public PaymentDetails getPaymentDetails() {
		return paymentDetails;
	}
	public void setPaymentDetails(PaymentDetails paymentDetails) {
		this.paymentDetails = paymentDetails;
	}
	public ArrayList<FlightSearchDetails> getFlightSearchDetails() {
		return flightSearchDetails;
	}
	public void setFlightSearchDetails(ArrayList<FlightSearchDetails> flightSearchDetails) {
		this.flightSearchDetails = flightSearchDetails;
	}
	public String getVisatype() {
		return visatype;
	}
	public void setVisatype(String visatype) {
		this.visatype = visatype;
	}
	public String getTraceId() {
		return traceId;
	}
	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public Travellers getTravellers() {
		return travellers;
	}
	public void setTravellers(Travellers travellers) {
		this.travellers = travellers;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getSpKey() {
		return spKey;
	}
	public void setSpKey(String spKey) {
		this.spKey = spKey;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public List<TravellerFlightDetails> getTravellerDetails() {
		return travellerDetails;
	}
	public void setTravellerDetails(List<TravellerFlightDetails> travellerDetails) {
		this.travellerDetails = travellerDetails;
	}
	public String getTicketDetails() {
		return ticketDetails;
	}
	public void setTicketDetails(String ticketDetails) {
		this.ticketDetails = ticketDetails;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
	@Override
	public String getJsonRequest() {
		JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();

	 	JsonArrayBuilder BookingSegment = Json.createArrayBuilder();
        JsonObjectBuilder segList = Json.createObjectBuilder();
        for (int i = 0; i < bookSegments.size(); i++) {
//        	 segList.add("bondType",  (bookSegments.get(i).getBondType() == null) ? "" : bookSegments.get(i).getBondType());
         	JsonArrayBuilder Bonds = Json.createArrayBuilder();
         	JsonObjectBuilder bondsList = Json.createObjectBuilder();
         	for (int j = 0; j < bookSegments.get(i).getBonds().size(); j++) {
         		bondsList.add("boundType", (bookSegments.get(i).getBonds().get(j).getBoundType() == null) ? "" : bookSegments.get(i).getBonds().get(j).getBoundType());
             	bondsList.add("baggageFare", bookSegments.get(i).getBonds().get(j).isBaggageFare());
             	bondsList.add("ssrFare", bookSegments.get(i).getBonds().get(j).isSsrFare());
             	bondsList.add("itineraryKey", bookSegments.get(i).getBonds().get(j).getItineraryKey());
             	bondsList.add("journeyTime", bookSegments.get(i).getBonds().get(j).getJourneyTime());
             		JsonArrayBuilder Legs = Json.createArrayBuilder();
             		JsonObjectBuilder LegsList = Json.createObjectBuilder();
				for (int k = 0; k < bookSegments.get(i).getBonds().get(j).getLegs().size(); k++) {
					LegsList.add("aircraftCode",
							(bookSegments.get(i).getBonds().get(j).getLegs().get(k).getAircraftCode() == null) ? ""
									: bookSegments.get(i).getBonds().get(j).getLegs().get(k).getAircraftCode());
					LegsList.add("aircraftType",
							(bookSegments.get(i).getBonds().get(j).getLegs().get(k).getAircraftType() == null) ? ""
									: bookSegments.get(i).getBonds().get(j).getLegs().get(k).getAircraftType());
					LegsList.add("airlineName",
							(bookSegments.get(i).getBonds().get(j).getLegs().get(k).getAirlineName() == null) ? ""
									: bookSegments.get(i).getBonds().get(j).getLegs().get(k).getAirlineName());
					// LegsList.add("amount",
					// bookSegments.get(i).getBonds().get(j).getLegs().get(k).getAmount());
					LegsList.add("arrivalDate",
							bookSegments.get(i).getBonds().get(j).getLegs().get(k).getArrivalDate());
					LegsList.add("arrivalTerminal",
							(bookSegments.get(i).getBonds().get(j).getLegs().get(k).getArrivalTerminal() == null) ? ""
									: bookSegments.get(i).getBonds().get(j).getLegs().get(k).getArrivalTerminal());
					LegsList.add("arrivalTime",
							bookSegments.get(i).getBonds().get(j).getLegs().get(k).getArrivalTime());
					LegsList.add("availableSeat",
							(bookSegments.get(i).getBonds().get(j).getLegs().get(k).getAvailableSeat() == null) ? ""
									: bookSegments.get(i).getBonds().get(j).getLegs().get(k).getAvailableSeat());
					LegsList.add("baggageUnit",
							bookSegments.get(i).getBonds().get(j).getLegs().get(k).getBaggageUnit());
					LegsList.add("baggageWeight",
							bookSegments.get(i).getBonds().get(j).getLegs().get(k).getBaggageWeight());
					LegsList.add("boundTypes",
							(bookSegments.get(i).getBonds().get(j).getLegs().get(k).getBoundTypes() == null) ? ""
									: bookSegments.get(i).getBonds().get(j).getLegs().get(k).getBoundTypes());
					LegsList.add("cabin", bookSegments.get(i).getBonds().get(j).getLegs().get(k).getCabin());

					LegsList.add("capacity",
							(bookSegments.get(i).getBonds().get(j).getLegs().get(k).getCapacity() == null) ? ""
									: bookSegments.get(i).getBonds().get(j).getLegs().get(k).getCapacity());
					LegsList.add("carrierCode",
							(bookSegments.get(i).getBonds().get(j).getLegs().get(k).getCarrierCode() == null) ? ""
									: bookSegments.get(i).getBonds().get(j).getLegs().get(k).getCarrierCode());
					LegsList.add("currencyCode",
							(bookSegments.get(i).getBonds().get(j).getLegs().get(k).getCurrencyCode() == null) ? ""
									: bookSegments.get(i).getBonds().get(j).getLegs().get(k).getCurrencyCode());
					LegsList.add("departureDate",
							(bookSegments.get(i).getBonds().get(j).getLegs().get(k).getDepartureDate() == null) ? ""
									: bookSegments.get(i).getBonds().get(j).getLegs().get(k).getDepartureDate());
					LegsList.add("departureTerminal",
							(bookSegments.get(i).getBonds().get(j).getLegs().get(k).getDepartureTerminal() == null) ? ""
									: bookSegments.get(i).getBonds().get(j).getLegs().get(k).getDepartureTerminal());
					LegsList.add("departureTime",
							(bookSegments.get(i).getBonds().get(j).getLegs().get(k).getDepartureTime() == null) ? ""
									: bookSegments.get(i).getBonds().get(j).getLegs().get(k).getDepartureTime());
					LegsList.add("destination",
							(bookSegments.get(i).getBonds().get(j).getLegs().get(k).getDestination() == null) ? ""
									: bookSegments.get(i).getBonds().get(j).getLegs().get(k).getDestination());
					LegsList.add("duration",
							(bookSegments.get(i).getBonds().get(j).getLegs().get(k).getDuration() == null) ? ""
									: bookSegments.get(i).getBonds().get(j).getLegs().get(k).getDuration());
					LegsList.add("fareBasisCode",
							(bookSegments.get(i).getBonds().get(j).getLegs().get(k).getFareBasisCode() == null) ? ""
									: bookSegments.get(i).getBonds().get(j).getLegs().get(k).getFareBasisCode());
					LegsList.add("fareClassOfService",
							(bookSegments.get(i).getBonds().get(j).getLegs().get(k).getFareClassOfService() == null)
									? ""
									: bookSegments.get(i).getBonds().get(j).getLegs().get(k).getFareClassOfService());
					LegsList.add("flightDesignator",
							(bookSegments.get(i).getBonds().get(j).getLegs().get(k).getFlightDesignator() == null) ? ""
									: bookSegments.get(i).getBonds().get(j).getLegs().get(k).getFlightDesignator());
					LegsList.add("flightDetailRefKey",
							(bookSegments.get(i).getBonds().get(j).getLegs().get(k).getFlightDetailRefKey() == null)
									? ""
									: bookSegments.get(i).getBonds().get(j).getLegs().get(k).getFlightDetailRefKey());
					// LegsList.add("flightName",
					// (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getFlightName()
					// == null ) ? "" :
					// bookSegments.get(i).getBonds().get(j).getLegs().get(k).getFlightName());
					LegsList.add("flightNumber",
							(bookSegments.get(i).getBonds().get(j).getLegs().get(k).getFlightNumber() == null) ? ""
									: bookSegments.get(i).getBonds().get(j).getLegs().get(k).getFlightNumber());
					LegsList.add("group", (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getGroup() == null)
							? "" : bookSegments.get(i).getBonds().get(j).getLegs().get(k).getGroup());
					// LegsList.add("isConnecting",
					// bookSegments.get(i).getBonds().get(j).getLegs().get(k).isConnecting());
					// LegsList.add("numberOfStops",
					// (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getNumberOfStops()
					// == null) ? "" :
					// bookSegments.get(i).getBonds().get(j).getLegs().get(k).getNumberOfStops());
					LegsList.add("origin", (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getOrigin() == null)
							? "" : bookSegments.get(i).getBonds().get(j).getLegs().get(k).getOrigin());
					LegsList.add("providerCode",
							(bookSegments.get(i).getBonds().get(j).getLegs().get(k).getProviderCode() == null) ? ""
									: bookSegments.get(i).getBonds().get(j).getLegs().get(k).getProviderCode());
					// LegsList.add("remarks",
					// (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getRemarks()
					// == null) ? "" :
					// bookSegments.get(i).getBonds().get(j).getLegs().get(k).getRemarks());

					LegsList.add("sold", bookSegments.get(i).getBonds().get(j).getLegs().get(k).getSold());
					LegsList.add("status", (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getStatus() == null)
							? "" : bookSegments.get(i).getBonds().get(j).getLegs().get(k).getStatus());
					Legs.add(LegsList.build());
				}
                 bondsList.add("legs", Legs);
     			bondsList.add("addOnDetail", (bookSegments.get(i).getBonds().get(j).getAddOnDetail() == null) ?	"" : bookSegments.get(i).getBonds().get(j).getAddOnDetail());
     		Bonds.add(bondsList.build());
			}
         segList.add("bonds", Bonds);
         segList.add("deeplink", (bookSegments.get(i).getDeeplink() == null) ? "" : bookSegments.get(i).getDeeplink());
         segList.add("engineID", getEngineID());
         	JsonObjectBuilder Fare = Json.createObjectBuilder();
         	Fare.add("basicFare", bookSegments.get(i).getFares().getBasicFare());
			Fare.add("exchangeRate", bookSegments.get(i).getFares().getExchangeRate());
 			JsonArrayBuilder PaxFares = Json.createArrayBuilder();
				JsonObjectBuilder PaxFaresList = Json.createObjectBuilder();
			for (int m = 0; m < bookSegments.get(i).getFares().getPaxFares().size(); m++) {
				PaxFaresList.add("baggageUnit",
						(bookSegments.get(i).getFares().getPaxFares().get(m).getBaggageUnit() == null) ? ""
								: bookSegments.get(i).getFares().getPaxFares().get(m).getBaggageUnit());
				PaxFaresList.add("baggageWeight",
						(bookSegments.get(i).getFares().getPaxFares().get(m).getBaggageWeight() == null) ? ""
								: bookSegments.get(i).getFares().getPaxFares().get(m).getBaggageWeight());
				PaxFaresList.add("baseTransactionAmount",
						bookSegments.get(i).getFares().getPaxFares().get(m).getBaseTransactionAmount());
				PaxFaresList.add("basicFare", bookSegments.get(i).getFares().getPaxFares().get(m).getBasicFare());
				PaxFaresList.add("cancelPenalty",
						bookSegments.get(i).getFares().getPaxFares().get(m).getCancelPenalty());
				PaxFaresList.add("changePenalty",
						bookSegments.get(i).getFares().getPaxFares().get(m).getChangePenalty());
				PaxFaresList.add("equivCurrencyCode",
						(bookSegments.get(i).getFares().getPaxFares().get(m).getEquivCurrencyCode() == null) ? ""
								: bookSegments.get(i).getFares().getPaxFares().get(m).getEquivCurrencyCode());
				JsonArrayBuilder Fares = Json.createArrayBuilder();
				JsonObjectBuilder FaresList = Json.createObjectBuilder();
				for (int n = 0; n < bookSegments.get(i).getFares().getPaxFares().get(m).getBookFares().size(); n++) {
					FaresList.add("amount",
							bookSegments.get(i).getFares().getPaxFares().get(m).getBookFares().get(n).getAmount());
					FaresList.add("chargeCode",
							bookSegments.get(i).getFares().getPaxFares().get(m).getBookFares().get(n).getChargeCode());
					FaresList.add("chargeType",
							bookSegments.get(i).getFares().getPaxFares().get(m).getBookFares().get(n).getChargeType());
					Fares.add(FaresList.build());
				}
				PaxFaresList.add("bookFares", Fares);
				PaxFaresList.add("fareBasisCode",
						(bookSegments.get(i).getFares().getPaxFares().get(m).getFareBasisCode() == null) ? ""
								: bookSegments.get(i).getFares().getPaxFares().get(m).getFareBasisCode());
				PaxFaresList.add("fareInfoKey",
						(bookSegments.get(i).getFares().getPaxFares().get(m).getFareInfoKey() == null) ? ""
								: bookSegments.get(i).getFares().getPaxFares().get(m).getFareInfoKey());
				PaxFaresList.add("fareInfoValue",
						(bookSegments.get(i).getFares().getPaxFares().get(m).getFareInfoValue() == null) ? ""
								: bookSegments.get(i).getFares().getPaxFares().get(m).getFareInfoValue());
				PaxFaresList.add("markUP", bookSegments.get(i).getFares().getPaxFares().get(m).getMarkUP());
				PaxFaresList.add("paxType", bookSegments.get(i).getFares().getPaxFares().get(m).getPaxType());
				PaxFaresList.add("refundable", bookSegments.get(i).getFares().getPaxFares().get(m).isRefundable());
				PaxFaresList.add("totalFare", bookSegments.get(i).getFares().getPaxFares().get(m).getTotalFare());
				PaxFaresList.add("totalTax", bookSegments.get(i).getFares().getPaxFares().get(m).getTotalTax());
				PaxFaresList.add("transactionAmount",
						bookSegments.get(i).getFares().getPaxFares().get(m).getTransactionAmount());
				PaxFares.add(PaxFaresList.build());
			}
			Fare.add("paxFares", PaxFares);
			Fare.add("totalFareWithOutMarkUp", bookSegments.get(i).getFares().getTotalFareWithOutMarkUp());
			Fare.add("totalTaxWithOutMarkUp", bookSegments.get(i).getFares().getTotalTaxWithOutMarkUp());

		 segList.add("fares", Fare);
		 bookSegments.get(i).setFareIndicator("0");
		 segList.add("fareIndicator", (bookSegments.get(i).getFareIndicator() == null ) ? "" : bookSegments.get(i).getFareIndicator());
     	 segList.add("fareRule", (bookSegments.get(i).getFareRule() == null ) ? "" : bookSegments.get(i).getFareRule());
         segList.add("baggageFare", bookSegments.get(i).isBaggageFare());
         segList.add("cache", bookSegments.get(i).isCache());
         segList.add("holdBooking", bookSegments.get(i).isHoldBooking());
         segList.add("international", bookSegments.get(i).isInternational());
         segList.add("roundTrip", bookSegments.get(i).isRoundTrip());
         segList.add("special", bookSegments.get(i).isSpecial());
         segList.add("specialId", bookSegments.get(i).isSpecialId());
         segList.add("itineraryKey", (bookSegments.get(i).getItineraryKey() == null) ? "" : bookSegments.get(i).getItineraryKey());
         segList.add("journeyIndex", bookSegments.get(i).getJourneyIndex());
         bookSegments.get(i).setMemoryCreationTime("/Date(1499158249483+0530)/");
         segList.add("memoryCreationTime", (bookSegments.get(i).getMemoryCreationTime() == null) ? "" : bookSegments.get(i).getMemoryCreationTime());
         segList.add("nearByAirport", bookSegments.get(i).isNearByAirport());
         segList.add("remark", (bookSegments.get(i).getRemark() == null) ? "" : bookSegments.get(i).getRemark());
         segList.add("searchId", (bookSegments.get(i).getSearchId() == null) ? "" : bookSegments.get(i).getSearchId());
         BookingSegment.add(segList.build());
		}
        
        jsonBuilder.add("clientIp", "192.170.1.116");
        jsonBuilder.add("cllientKey", MdexConstants.getMdexCredentials().getCode());
        jsonBuilder.add("clientToken", MdexConstants.getMdexCredentials().getStatus());
        jsonBuilder.add("clientApiName", "Flight Search");
        jsonBuilder.add("spKey", bookSegments.get(0).getBonds().get(0).getLegs().get(0).getAirlineName() == null ? "" : bookSegments.get(0).getBonds().get(0).getLegs().get(0).getAirlineName() + "IN");
        jsonBuilder.add("bookSegments", BookingSegment);
        jsonBuilder.add("engineID", getEngineID());
        JsonArrayBuilder engineId = Json.createArrayBuilder();
	    for (int i = 0; i < engineIDList.size(); i++) {
			engineId.add(engineIDList.get(i).ordinal());
		}
	    jsonBuilder.add("engineIDList", engineId);
        jsonBuilder.add("androidBooking", true);
        jsonBuilder.add("domestic", isDomestic());
        
        JsonObjectBuilder PaymentDetails = Json.createObjectBuilder();
        PaymentDetails.add("bookingAmount", paymentDetails.getBookingAmount());
        PaymentDetails.add("bookingCurrencyCode", (paymentDetails.getBookingCurrencyCode() == null) ? "" : paymentDetails.getBookingCurrencyCode());
        jsonBuilder.add("paymentDetails", PaymentDetails);
        
        JsonArrayBuilder FlightSearchDetails = Json.createArrayBuilder();
		JsonObjectBuilder SearchDetailList = Json.createObjectBuilder();
		for (int j = 0; j < getFlightSearchDetails().size(); j++) {
			SearchDetailList.add("beginDate", (getFlightSearchDetails().get(j).getBeginDate() == null ) ? "" : getFlightSearchDetails().get(j).getBeginDate());
			SearchDetailList.add("destination", (getFlightSearchDetails().get(j).getDestination() == null ) ? "" : getFlightSearchDetails().get(j).getDestination());
			if (getFlightSearchDetails().get(j).getEndDate()==null) {
				
				SearchDetailList.add("endDate", (getFlightSearchDetails().get(j).getBeginDate() == null) ? "" : getFlightSearchDetails().get(j).getBeginDate());
			}
			else if (getFlightSearchDetails().get(j).getEndDate().isEmpty()) {
				SearchDetailList.add("endDate", (getFlightSearchDetails().get(j).getBeginDate() == null) ? "" : getFlightSearchDetails().get(j).getBeginDate());
			}
			else {
				SearchDetailList.add("endDate", (getFlightSearchDetails().get(j).getEndDate() == null) ? "" : getFlightSearchDetails().get(j).getEndDate());
			}
			SearchDetailList.add("origin", (getFlightSearchDetails().get(j). getOrigin() == null) ? "" : getFlightSearchDetails().get(j). getOrigin());
			FlightSearchDetails.add(SearchDetailList.build());
		}
		
	    jsonBuilder.add("clientIp", "192.170.1.116");
        jsonBuilder.add("cllientKey", MdexConstants.getMdexCredentials().getCode());
        jsonBuilder.add("clientToken", MdexConstants.getMdexCredentials().getStatus());
        jsonBuilder.add("clientApiName", "Flight Book");
		
        jsonBuilder.add("flightSearchDetails", FlightSearchDetails);
        
        jsonBuilder.add("visatype", (getVisatype() == null ) ? "" : getVisatype());
        jsonBuilder.add("traceId", (getTraceId() == null ) ? "" : getTraceId());
        jsonBuilder.add("transactionId", (getTransactionId() == null ) ? "" : getTransactionId());
        
        
        
        JsonObjectBuilder travel=createMobilejsonAllraveler(getTravellerDetails(), getEmailAddress(), getMobileNumber());
        
		jsonBuilder.add("travellers", travel);
     
        JsonObject empObj = jsonBuilder.build();
        StringWriter jsnReqStr = new StringWriter();
        JsonWriter jsonWtr = Json.createWriter(jsnReqStr);
        jsonWtr.writeObject(empObj);
        jsonWtr.close();
         System.out.println("service id : " +  getSpKey());
        System.out.println(jsnReqStr.toString());
		
		return jsnReqStr.toString();
	}
	
	
	public static JsonObjectBuilder createMobilejsonAllraveler(List<TravellerFlightDetails> travellerDetails, String email,String mobile) {
		JsonObjectBuilder travellers = Json.createObjectBuilder();
		JsonArrayBuilder adultTraveller = Json.createArrayBuilder();
		JsonObjectBuilder adultTravellerList = Json.createObjectBuilder();
		JsonArrayBuilder infantTraveller = Json.createArrayBuilder();
		JsonObjectBuilder infantTravellerList = Json.createObjectBuilder();
		JsonArrayBuilder childTraveller = Json.createArrayBuilder();
		JsonObjectBuilder childTravellerList = Json.createObjectBuilder();
		
		try {
				for (int i = 0; i < travellerDetails.size(); i++) {
				
					if (travellerDetails.get(i).getType()!=null) {
						if (travellerDetails.get(i).getType().equalsIgnoreCase("Adult")) {
							
							adultTravellerList.add("address1", "");
							adultTravellerList.add("address2", "");
							adultTravellerList.add("city", "");
							adultTravellerList.add("countryCode", "");
							adultTravellerList.add("cultureCode", "String content");
							adultTravellerList.add("dateofBirth", (travellerDetails.get(i).getDob() == null ) ? "" : travellerDetails.get(i).getDob());
							adultTravellerList.add("emailAddress", email);
							adultTravellerList.add("firstName", travellerDetails.get(i).getfName());
							adultTravellerList.add("frequentFlierNumber", "");
							adultTravellerList.add("gender", travellerDetails.get(i).getGender());
							adultTravellerList.add("homePhone", "");
							adultTravellerList.add("lastName", travellerDetails.get(i).getlName());
							adultTravellerList.add("meal", "");
							adultTravellerList.add("middleName", "");
							adultTravellerList.add("mobileNumber", mobile );
							adultTravellerList.add("nationality", "");
							adultTravellerList.add("passportExpiryDate", (travellerDetails.get(i).getPassExpDate()== null ) ? "" : travellerDetails.get(i).getPassExpDate());
							adultTravellerList.add("passportNo", (travellerDetails.get(i).getPassNo()== null ) ? "" : travellerDetails.get(i).getPassNo());
							adultTravellerList.add("paxType", "ADT");
							adultTravellerList.add("provisionState", "");
							adultTravellerList.add("residentCountry", "");
							adultTravellerList.add("title", ""); 
							
							if (travellerDetails.get(i).getGender().equalsIgnoreCase("Male")) {
								adultTravellerList.add("title", "Mr");
							}
							else {
								adultTravellerList.add("title", "Ms");
							}
							
							adultTraveller.add(adultTravellerList);
							
						}
						travellers.add("adultTravellers", adultTraveller);
						if (travellerDetails.get(i).getType().equalsIgnoreCase("Child")) {
							
							childTravellerList.add("address1", "");
							childTravellerList.add("address2", "");
							childTravellerList.add("city", "");
							childTravellerList.add("countryCode", "");
							childTravellerList.add("cultureCode", "String content");
							childTravellerList.add("dateofBirth", (travellerDetails.get(i).getDob() == null ) ? "" : travellerDetails.get(i).getDob());
							childTravellerList.add("emailAddress", email);
							childTravellerList.add("firstName", travellerDetails.get(i).getfName());
							childTravellerList.add("frequentFlierNumber", "");
							childTravellerList.add("gender", travellerDetails.get(i).getGender());
							childTravellerList.add("homePhone", "");
							childTravellerList.add("lastName", travellerDetails.get(i).getlName());
							childTravellerList.add("meal", "");
							childTravellerList.add("middleName", "");
							childTravellerList.add("mobileNumber", mobile );
							childTravellerList.add("nationality", "");
							childTravellerList.add("passportExpiryDate", (travellerDetails.get(i).getPassExpDate()== null ) ? "" : travellerDetails.get(i).getPassExpDate());
							childTravellerList.add("passportNo", (travellerDetails.get(i).getPassNo()== null ) ? "" : travellerDetails.get(i).getPassNo());
							childTravellerList.add("paxType", "CHD");
							childTravellerList.add("provisionState", "");
							childTravellerList.add("residentCountry", "");
							childTravellerList.add("title", "");
							
							if (travellerDetails.get(i).getGender().equalsIgnoreCase("Male")) {
								childTravellerList.add("title", "Mr");
							}
							else {
								childTravellerList.add("title", "Ms");
							}
							childTraveller.add(childTravellerList);
						}
						travellers.add("childTravellers", childTraveller);
						
						if (travellerDetails.get(i).getType().equalsIgnoreCase("Infant")) {
							
							infantTravellerList.add("address1", "");
							infantTravellerList.add("address2", "");
							infantTravellerList.add("city", "");
							infantTravellerList.add("countryCode", "");
							infantTravellerList.add("cultureCode", "String content");
							infantTravellerList.add("dateofBirth", (travellerDetails.get(i).getDob() == null ) ? "" : travellerDetails.get(i).getDob());
							infantTravellerList.add("emailAddress", email);
							infantTravellerList.add("firstName", travellerDetails.get(i).getfName());
							infantTravellerList.add("frequentFlierNumber", "");
							infantTravellerList.add("gender", travellerDetails.get(i).getGender());
							infantTravellerList.add("homePhone", "");
							infantTravellerList.add("lastName", travellerDetails.get(i).getlName());
							infantTravellerList.add("meal", "");
							infantTravellerList.add("middleName", "");
							infantTravellerList.add("mobileNumber", mobile );
							infantTravellerList.add("nationality", "");
							infantTravellerList.add("passportExpiryDate",(travellerDetails.get(i).getPassExpDate()== null ) ? "" : travellerDetails.get(i).getPassExpDate());
							infantTravellerList.add("passportNo", (travellerDetails.get(i).getPassNo()== null ) ? "" : travellerDetails.get(i).getPassNo());
							infantTravellerList.add("paxType", "INF");
							infantTravellerList.add("provisionState", "");
							infantTravellerList.add("residentCountry", "");
							
							if (travellerDetails.get(i).getGender().equalsIgnoreCase("Male")) {
								infantTravellerList.add("title", "Mr");
							}
							else {
								infantTravellerList.add("title", "Ms");
							}
							infantTraveller.add(infantTravellerList);
						}
						travellers.add("infantTravellers", infantTraveller);
					}
				}
				
			
		} catch (Exception e) {

		}
		return travellers;
	}
}
