package com.msscard.model;

import java.util.List;

public class FlightTicketResp {

	private List<TravellerFlightDetails> travellerDetails;
	private TicketsResp ticketsResp;

	private String bookingRefNo;
	private String email;
	private String created;
	public List<TravellerFlightDetails> getTravellerDetails() {
		return travellerDetails;
	}
	public void setTravellerDetails(List<TravellerFlightDetails> travellerDetails) {
		this.travellerDetails = travellerDetails;
	}
	public TicketsResp getTicketsResp() {
		return ticketsResp;
	}
	public void setTicketsResp(TicketsResp ticketsResp) {
		this.ticketsResp = ticketsResp;
	}
	public String getBookingRefNo() {
		return bookingRefNo;
	}
	public void setBookingRefNo(String bookingRefNo) {
		this.bookingRefNo = bookingRefNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	
	
}
