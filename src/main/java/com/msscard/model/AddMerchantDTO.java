package com.msscard.model;

import org.springframework.web.multipart.MultipartFile;

public class AddMerchantDTO {

	private String merchantName;
	private String mcc;
	private String merchantCode;
	private MultipartFile image;
	private String businessCorrespondentName;
	private String businessCorrespondentNumber;
	private String businessCorrespondentEmail;
	
	
	
	public String getBusinessCorrespondentName() {
		return businessCorrespondentName;
	}
	public void setBusinessCorrespondentName(String businessCorrespondentName) {
		this.businessCorrespondentName = businessCorrespondentName;
	}
	public String getBusinessCorrespondentNumber() {
		return businessCorrespondentNumber;
	}
	public void setBusinessCorrespondentNumber(String businessCorrespondentNumber) {
		this.businessCorrespondentNumber = businessCorrespondentNumber;
	}
	public String getBusinessCorrespondentEmail() {
		return businessCorrespondentEmail;
	}
	public void setBusinessCorrespondentEmail(String businessCorrespondentEmail) {
		this.businessCorrespondentEmail = businessCorrespondentEmail;
	}
	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	public String getMcc() {
		return mcc;
	}
	public void setMcc(String mcc) {
		this.mcc = mcc;
	}
	public String getMerchantCode() {
		return merchantCode;
	}
	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}
	public MultipartFile getImage() {
		return image;
	}
	public void setImage(MultipartFile image) {
		this.image = image;
	}
	
	
}
