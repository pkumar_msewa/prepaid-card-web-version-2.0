package com.msscard.model;

import java.util.ArrayList;

public class FlightPriceReCheckResponse {

	private Errors errors;
	private ArrayList<Journeys> journeys;
	private String TraceId;
	public Errors getErrors() {
		return errors;
	}
	public void setErrors(Errors errors) {
		this.errors = errors;
	}
	public ArrayList<Journeys> getJourneys() {
		return journeys;
	}
	public void setJourneys(ArrayList<Journeys> journeys) {
		this.journeys = journeys;
	}
	public String getTraceId() {
		return TraceId;
	}
	public void setTraceId(String traceId) {
		TraceId = traceId;
	}

	
}
