package com.msscard.model;

import java.util.ArrayList;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import com.msscard.app.model.request.CancelPolicyListDTO;

@JsonIgnoreProperties(ignoreUnknown=true)
public class AvailableTripsDTO {
	
	@JsonProperty("ac")
	private boolean ac;
	
	@JsonProperty("arrivalTime")
	private String arrivalTime;
	
	@JsonProperty("availableSeats")
	private String availableSeats;
	
	@JsonProperty("busType")
	private String busType;
	
	@JsonProperty("bdlocation")
	private String bdlocation;
	
	@JsonProperty("busTypeId")
	private String busTypeId;
	
	@JsonProperty("cancellationPolicy")
	private String cancellationPolicy;
	
	@JsonProperty("departureTime")
	private String departureTime;
	
	@JsonProperty("doj")
	private String doj;
	
	@JsonProperty("duration")
	private String duration;
	
	@JsonProperty("bdPointDTO")
	private ArrayList<BoardingPointsDTO> bdPointDTO;  
	
	@JsonProperty("dpPointsDTO")
	private ArrayList<DroppingPointsDTO> dpPointsDTO;
	
	@JsonProperty("fareDetailDTOs")
	private ArrayList<FareDetailDTO> fareDetailDTOs;
	
	@JsonProperty("fare")
	private ArrayList<String> fare;
	
	@JsonProperty("travels")
	private String travels;
	
	@JsonProperty("routeId")
	private String routeId;
	
	@JsonProperty("price")
	private String price;
	
	@JsonProperty("seater")
	private boolean seater;
	
	@JsonProperty("sleeper")
	private boolean sleeper;
	
	@JsonProperty("idProofRequired")
	private String idProofRequired;
	
	@JsonProperty("liveTrackingAvailable")
	private String liveTrackingAvailable;
	
	@JsonProperty("nonAC")
	private boolean nonAC;
	
	@JsonProperty("operatorid")
	private String operatorid;
	
	@JsonProperty("partialCancellationAllowed")
	private String partialCancellationAllowed;
	
	@JsonProperty("tatkalTime")
	private String tatkalTime;
	
	@JsonProperty("vehicleType")
	private String vehicleType;
	
	@JsonProperty("zeroCancellationTime")
	private String zeroCancellationTime;
	
	@JsonProperty("mTicketEnabled")
	private String mTicketEnabled;
	
	@JsonProperty("sortDepTime")
	private int sortDepTime;
	
	@JsonProperty("engineId")
	private int engineId;
	
	@JsonProperty("isVolvo")
	private boolean isVolvoa;
	
	@JsonProperty("isCancellable")
	private boolean isCancellablea;
	
	@JsonProperty("status")
	private String status;
	
	@JsonProperty("totalSeat")
	private String totalSeat;
	
	@JsonProperty("departureDate")
	private String departureDate;
	
	@JsonProperty("arrivalDate")
	private String arrivalDate;
	
	@JsonProperty("discount")
	private int discount;
	
	@JsonProperty("commission")
	private int commission;
	
	@JsonProperty("markup")
	private int markup;
	
	@JsonProperty("tds")
	private int tds;
	
	@JsonProperty("stf")
	private int stf;
	
	@JsonProperty("bpId")
	private String bpId;
	
	@JsonProperty("dpId")
	private String dpId;
	
	@JsonProperty("bpDpLayout")
	private boolean bpDpLayout;
	
	@JsonProperty("volvo")
	private boolean volvo;
	
	@JsonProperty("cancellable")
	private boolean cancellable;
	
	@JsonProperty("id")
	private String id;
	
	@JsonProperty("cancelPolicyListDTO")
	private ArrayList<CancelPolicyListDTO> cancelPolicyListDTO;
		
	public boolean isVolvoa() {
		return isVolvoa;
	}
	public void setVolvoa(boolean isVolvoa) {
		this.isVolvoa = isVolvoa;
	}
	public boolean isCancellablea() {
		return isCancellablea;
	}
	public void setCancellablea(boolean isCancellablea) {
		this.isCancellablea = isCancellablea;
	}
	
	public boolean isAc() {
		return ac;
	}
	public void setAc(boolean ac) {
		this.ac = ac;
	}
	public String getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public String getAvailableSeats() {
		return availableSeats;
	}
	public void setAvailableSeats(String availableSeats) {
		this.availableSeats = availableSeats;
	}
	public String getBusType() {
		return busType;
	}
	public void setBusType(String busType) {
		this.busType = busType;
	}
	public String getBdlocation() {
		return bdlocation;
	}
	public void setBdlocation(String bdlocation) {
		this.bdlocation = bdlocation;
	}
	public String getBusTypeId() {
		return busTypeId;
	}
	public void setBusTypeId(String busTypeId) {
		this.busTypeId = busTypeId;
	}
	public String getCancellationPolicy() {
		return cancellationPolicy;
	}
	public void setCancellationPolicy(String cancellationPolicy) {
		this.cancellationPolicy = cancellationPolicy;
	}
	public String getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}
	public String getDoj() {
		return doj;
	}
	public void setDoj(String doj) {
		this.doj = doj;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public ArrayList<BoardingPointsDTO> getBdPointDTO() {
		return bdPointDTO;
	}
	public void setBdPointDTO(ArrayList<BoardingPointsDTO> bdPointDTO) {
		this.bdPointDTO = bdPointDTO;
	}
	public ArrayList<DroppingPointsDTO> getDpPointsDTO() {
		return dpPointsDTO;
	}
	public void setDpPointsDTO(ArrayList<DroppingPointsDTO> dpPointsDTO) {
		this.dpPointsDTO = dpPointsDTO;
	}
	public ArrayList<FareDetailDTO> getFareDetailDTOs() {
		return fareDetailDTOs;
	}
	public void setFareDetailDTOs(ArrayList<FareDetailDTO> fareDetailDTOs) {
		this.fareDetailDTOs = fareDetailDTOs;
	}
	public ArrayList<String> getFare() {
		return fare;
	}
	public void setFare(ArrayList<String> fare) {
		this.fare = fare;
	}
	public String getTravels() {
		return travels;
	}
	public void setTravels(String travels) {
		this.travels = travels;
	}
	public String getRouteId() {
		return routeId;
	}
	public void setRouteId(String routeId) {
		this.routeId = routeId;
	}
	
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public boolean isSeater() {
		return seater;
	}
	public void setSeater(boolean seater) {
		this.seater = seater;
	}
	public boolean isSleeper() {
		return sleeper;
	}
	public void setSleeper(boolean sleeper) {
		this.sleeper = sleeper;
	}
	public String getIdProofRequired() {
		return idProofRequired;
	}
	public void setIdProofRequired(String idProofRequired) {
		this.idProofRequired = idProofRequired;
	}
	public String getLiveTrackingAvailable() {
		return liveTrackingAvailable;
	}
	public void setLiveTrackingAvailable(String liveTrackingAvailable) {
		this.liveTrackingAvailable = liveTrackingAvailable;
	}
	public boolean isNonAC() {
		return nonAC;
	}
	public void setNonAC(boolean nonAC) {
		this.nonAC = nonAC;
	}
	public String getOperatorid() {
		return operatorid;
	}
	public void setOperatorid(String operatorid) {
		this.operatorid = operatorid;
	}
	public String getPartialCancellationAllowed() {
		return partialCancellationAllowed;
	}
	public void setPartialCancellationAllowed(String partialCancellationAllowed) {
		this.partialCancellationAllowed = partialCancellationAllowed;
	}
	public String getTatkalTime() {
		return tatkalTime;
	}
	public void setTatkalTime(String tatkalTime) {
		this.tatkalTime = tatkalTime;
	}
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getZeroCancellationTime() {
		return zeroCancellationTime;
	}
	public void setZeroCancellationTime(String zeroCancellationTime) {
		this.zeroCancellationTime = zeroCancellationTime;
	}
	public String getmTicketEnabled() {
		return mTicketEnabled;
	}
	public void setmTicketEnabled(String mTicketEnabled) {
		this.mTicketEnabled = mTicketEnabled;
	}
	public int getSortDepTime() {
		return sortDepTime;
	}
	public void setSortDepTime(int sortDepTime) {
		this.sortDepTime = sortDepTime;
	}
	public int getEngineId() {
		return engineId;
	}
	public void setEngineId(int engineId) {
		this.engineId = engineId;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTotalSeat() {
		return totalSeat;
	}
	public void setTotalSeat(String totalSeat) {
		this.totalSeat = totalSeat;
	}
	public String getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}
	public String getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public int getDiscount() {
		return discount;
	}
	public void setDiscount(int discount) {
		this.discount = discount;
	}
	public int getCommission() {
		return commission;
	}
	public void setCommission(int commission) {
		this.commission = commission;
	}
	public int getMarkup() {
		return markup;
	}
	public void setMarkup(int markup) {
		this.markup = markup;
	}
	public int getTds() {
		return tds;
	}
	public void setTds(int tds) {
		this.tds = tds;
	}
	public int getStf() {
		return stf;
	}
	public void setStf(int stf) {
		this.stf = stf;
	}
	public String getBpId() {
		return bpId;
	}
	public void setBpId(String bpId) {
		this.bpId = bpId;
	}
	public String getDpId() {
		return dpId;
	}
	public void setDpId(String dpId) {
		this.dpId = dpId;
	}
	public boolean isBpDpLayout() {
		return bpDpLayout;
	}
	public void setBpDpLayout(boolean bpDpLayout) {
		this.bpDpLayout = bpDpLayout;
	}
	public boolean isVolvo() {
		return volvo;
	}
	public void setVolvo(boolean volvo) {
		this.volvo = volvo;
	}
	public boolean isCancellable() {
		return cancellable;
	}
	public void setCancellable(boolean cancellable) {
		this.cancellable = cancellable;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public ArrayList<CancelPolicyListDTO> getCancelPolicyListDTO() {
		return cancelPolicyListDTO;
	}
	public void setCancelPolicyListDTO(ArrayList<CancelPolicyListDTO> cancelPolicyListDTO) {
		this.cancelPolicyListDTO = cancelPolicyListDTO;
	}
	
	
	
}
