package com.msscard.model;

public class SSRDetails {

	private double SSRAmount;
	private String SSRDetails;
	private String SSRCode;
	private String SSRType;
	
	public double getSSRAmount() {
		return SSRAmount;
	}
	public void setSSRAmount(double sSRAmount) {
		SSRAmount = sSRAmount;
	}
	public String getSSRDetails() {
		return SSRDetails;
	}
	public void setSSRDetails(String sSRDetails) {
		SSRDetails = sSRDetails;
	}
	public String getSSRCode() {
		return SSRCode;
	}
	public void setSSRCode(String sSRCode) {
		SSRCode = sSRCode;
	}
	public String getSSRType() {
		return SSRType;
	}
	public void setSSRType(String sSRType) {
		SSRType = sSRType;
	}
	
	
}
