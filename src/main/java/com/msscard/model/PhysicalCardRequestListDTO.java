package com.msscard.model;

import java.util.Date;

public class PhysicalCardRequestListDTO {
	
	private String contactNo;
	
	private String email;
	
	private double cardCost;
	
	private Date date;
	
	private String kitNo;
	
	private String name;
	
	private String userType;

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public double getCardCost() {
		return cardCost;
	}

	public void setCardCost(double cardCost) {
		this.cardCost = cardCost;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getKitNo() {
		return kitNo;
	}

	public void setKitNo(String kitNo) {
		this.kitNo = kitNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}
	
	

}
