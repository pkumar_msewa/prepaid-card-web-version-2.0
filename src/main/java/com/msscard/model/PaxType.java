package com.msscard.model;

public enum PaxType {

	ADT("ADT"), CHD("CHD"), INF("INF");

	private final String value;

	private PaxType(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public static PaxType getEnum(String value) {
		if (value == null)
			throw new IllegalArgumentException();
		for (PaxType v : values())
			if (value.equalsIgnoreCase(v.getValue()))
				return v;
		throw new IllegalArgumentException();
	}
}
