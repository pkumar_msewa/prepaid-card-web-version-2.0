package com.msscard.model;

import org.hibernate.validator.constraints.NotEmpty;

import com.msscard.app.model.request.SessionDTO;

public class BulkSMSRequest extends SessionDTO{
	
	 @NotEmpty(message="Enter message content")
	 private String content;
	 
	 private String data;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
	 
	 
}
