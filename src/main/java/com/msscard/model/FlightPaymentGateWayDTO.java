package com.msscard.model;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;

import com.razorpay.constants.MdexConstants;

public class FlightPaymentGateWayDTO implements JSONRequest{

private String flightType;
	
	private String boundTypereturn;
	private String baggageFarereturn;
	private String ssrFarereturn;
	private String itineraryKeyreturn;
	private String journeyTimereturn;
	private String addOnDetailreturn;

	private String aircraftCodereturn;
	private String aircraftTypereturn;
	private String airlineNamereturn;
	private String amountreturn;
	private String arrivalDatereturn;
	private String arrivalTerminalreturn;
	private String arrivalTimereturn;
	private String availableSeatreturn;
	private String baggageUnitreturn;
	private String baggageWeightreturn;
	private String boundTypesreturn;
	private String cabinreturn;

	private String capacityreturn;
	private String carrierCodereturn;
	private String currencyCodereturn;
	private String departureDatereturn;
	private String departureTerminalreturn;
	private String departureTimereturn;
	private String destinationreturn;
	private String durationreturn;
	private String fareBasisCodereturn;
	private String fareClassOfServicereturn;
	private String flightDesignatorreturn;
	private String flightDetailRefKeyreturn;
	private String flightNamereturn;
	private String flightNumberreturn;
	private String groupreturn;
	private String connectingreturn;
	private String numberOfStopsreturn;
	private String originreturn;
	private String providerCodereturn;
	private String remarksreturn;
	private String soldreturn;
	private String statusreturn;

	private String deeplinkreturn;
	private String fareIndicatorreturn;
	private String fareRulereturn;

	private String cachereturn;
	private String holdBookingreturn;
	private String internationalreturn;
	private String roundTripreturn;
	private String specialreturn;
	private String specialIdreturn;

	private String journeyIndexreturn;
	private String memoryCreationTimereturn;
	private String nearByAirportreturn;
	private String remarkreturn;
	private String searchIdreturn;
	private String engineIDreturn;

	private String basicFarereturn;
	private String exchangeRatereturn;
	private String totalFareWithOutMarkUpreturn;
	private String totalTaxWithOutMarkUpreturn;

	private String baseTransactionAmountreturn;
	private String cancelPenaltyreturn;
	private String changePenaltyreturn;
	private String equivCurrencyCodereturn;

	private String fareInfoKeyreturn;
	private String fareInfoValuereturn;
	private String markUPreturn;
	private String paxTypereturn;
	private String refundablereturn;
	private String totalFarereturn;
	private String totalTaxreturn;
	private String transactionAmountreturn;

	private String beginDatereturn;

	private String endDatereturn;

	private String bookingAmountreturn;
	private String bookingCurrencyCodereturn;

	private String visatypereturn;
	private String traceIdreturn;
	private String transactionIdreturn;
	private String androidBookingreturn;
	private List<BookFare> bookFaresreturn;
	private String domesticreturn;
	
	private String clientIp;
	private String cllientKey;
	private String clientToken;
	private String clientApiName;
	private ArrayList<BookSegment> bookSegments;
	private String engineID;
	private ArrayList<EngineID> engineIDList;
	private boolean androidBooking;
	private boolean domestic;
	private PaymentDetails paymentDetails;
	private ArrayList<FlightSearchDetails> flightSearchDetails;
	private String visatype;
	private String traceId;
	private String transactionId;
	private Travellers travellers;
	private String emailAddress;
	private String mobileNumber;
	private String spKey;
	private String sessionId;
	List<TravellerFlightDetails> travellerDetails;
	private String ticketDetails;
	
	
	private String boundType;
	private String baggageFare;
	private String ssrFare;
	private String itineraryKey;
	private String journeyTime;
	private String addOnDetail;

	private String aircraftCode;
	private String aircraftType;
	private String airlineName;
	private String amount;
	private String arrivalDate;
	private String arrivalTerminal;
	private String arrivalTime;
	private String availableSeat;
	private String baggageUnit;
	private String baggageWeight;
	private String boundTypes;
	private String cabin;

	private String capacity;
	private String carrierCode;
	private String currencyCode;
	private String departureDate;
	private String departureTerminal;
	private String departureTime;
	private String destination;
	private String duration;
	private String fareBasisCode;
	private String fareClassOfService;
	private String flightDesignator;
	private String flightDetailRefKey;
	private String flightName;
	private String flightNumber;
	private String group;
	private String connecting;
	private String numberOfStops;
	private String origin;
	private String providerCode;
	private String remarks;
	private String sold;
	private String status;

	private String deeplink;
	private String fareIndicator;
	private String fareRule;

	private String cache;
	private String holdBooking;
	private String international;
	private String roundTrip;
	private String special;
	private String specialId;

	private String journeyIndex;
	private String memoryCreationTime;
	private String nearByAirport;
	private String remark;
	private String searchId;

	private String basicFare;
	private String exchangeRate;
	private String totalFareWithOutMarkUp;
	private String totalTaxWithOutMarkUp;

	private String baseTransactionAmount;
	private String cancelPenalty;
	private String changePenalty;
	private String equivCurrencyCode;

	private String fareInfoKey;
	private String fareInfoValue;
	private String markUP;
	private String paxType;
	private String refundable;
	private String totalFare;
	private String totalTax;
	private String transactionAmount;

	private String beginDate;

	private String endDate;

	private String bookingAmount;
	private String bookingCurrencyCode;

	private String address1;
	private String address2;
	private String city;
	private String countryCode;
	private String cultureCode;
	private String dateofBirth;
	private String firstName;
	private String frequentFlierNumber;
	private String gender;
	private String homePhone;
	private String lastName;
	private String meal;
	private String middleName;
	private String nationality;
	private String passportExpiryDate;
	private String passportNo;

	private String provisionState;
	private String residentCountry;
	private String title;

	private String txnRefNo;
	private List<BookFare> bookFares;
	
	private String grandtotal;
	private String adults;
	private String childs;
	private String infants;
	private String merchantRefNo;
	
	private String firstNamechild;
	private String lastNamechild;
	
	private String firstNameinfant;
	private String lastNameinfant;
		
	public String getFlightType() {
		return flightType;
	}



	public void setFlightType(String flightType) {
		this.flightType = flightType;
	}



	public String getBoundTypereturn() {
		return boundTypereturn;
	}



	public void setBoundTypereturn(String boundTypereturn) {
		this.boundTypereturn = boundTypereturn;
	}



	public String getBaggageFarereturn() {
		return baggageFarereturn;
	}



	public void setBaggageFarereturn(String baggageFarereturn) {
		this.baggageFarereturn = baggageFarereturn;
	}



	public String getSsrFarereturn() {
		return ssrFarereturn;
	}



	public void setSsrFarereturn(String ssrFarereturn) {
		this.ssrFarereturn = ssrFarereturn;
	}



	public String getItineraryKeyreturn() {
		return itineraryKeyreturn;
	}



	public void setItineraryKeyreturn(String itineraryKeyreturn) {
		this.itineraryKeyreturn = itineraryKeyreturn;
	}



	public String getJourneyTimereturn() {
		return journeyTimereturn;
	}



	public void setJourneyTimereturn(String journeyTimereturn) {
		this.journeyTimereturn = journeyTimereturn;
	}



	public String getAddOnDetailreturn() {
		return addOnDetailreturn;
	}



	public void setAddOnDetailreturn(String addOnDetailreturn) {
		this.addOnDetailreturn = addOnDetailreturn;
	}



	public String getAircraftCodereturn() {
		return aircraftCodereturn;
	}



	public void setAircraftCodereturn(String aircraftCodereturn) {
		this.aircraftCodereturn = aircraftCodereturn;
	}



	public String getAircraftTypereturn() {
		return aircraftTypereturn;
	}



	public void setAircraftTypereturn(String aircraftTypereturn) {
		this.aircraftTypereturn = aircraftTypereturn;
	}



	public String getAirlineNamereturn() {
		return airlineNamereturn;
	}



	public void setAirlineNamereturn(String airlineNamereturn) {
		this.airlineNamereturn = airlineNamereturn;
	}



	public String getAmountreturn() {
		return amountreturn;
	}



	public void setAmountreturn(String amountreturn) {
		this.amountreturn = amountreturn;
	}



	public String getArrivalDatereturn() {
		return arrivalDatereturn;
	}



	public void setArrivalDatereturn(String arrivalDatereturn) {
		this.arrivalDatereturn = arrivalDatereturn;
	}



	public String getArrivalTerminalreturn() {
		return arrivalTerminalreturn;
	}



	public void setArrivalTerminalreturn(String arrivalTerminalreturn) {
		this.arrivalTerminalreturn = arrivalTerminalreturn;
	}



	public String getArrivalTimereturn() {
		return arrivalTimereturn;
	}



	public void setArrivalTimereturn(String arrivalTimereturn) {
		this.arrivalTimereturn = arrivalTimereturn;
	}



	public String getAvailableSeatreturn() {
		return availableSeatreturn;
	}



	public void setAvailableSeatreturn(String availableSeatreturn) {
		this.availableSeatreturn = availableSeatreturn;
	}



	public String getBaggageUnitreturn() {
		return baggageUnitreturn;
	}



	public void setBaggageUnitreturn(String baggageUnitreturn) {
		this.baggageUnitreturn = baggageUnitreturn;
	}



	public String getBaggageWeightreturn() {
		return baggageWeightreturn;
	}



	public void setBaggageWeightreturn(String baggageWeightreturn) {
		this.baggageWeightreturn = baggageWeightreturn;
	}



	public String getBoundTypesreturn() {
		return boundTypesreturn;
	}



	public void setBoundTypesreturn(String boundTypesreturn) {
		this.boundTypesreturn = boundTypesreturn;
	}



	public String getCabinreturn() {
		return cabinreturn;
	}



	public void setCabinreturn(String cabinreturn) {
		this.cabinreturn = cabinreturn;
	}



	public String getCapacityreturn() {
		return capacityreturn;
	}



	public void setCapacityreturn(String capacityreturn) {
		this.capacityreturn = capacityreturn;
	}



	public String getCarrierCodereturn() {
		return carrierCodereturn;
	}



	public void setCarrierCodereturn(String carrierCodereturn) {
		this.carrierCodereturn = carrierCodereturn;
	}



	public String getCurrencyCodereturn() {
		return currencyCodereturn;
	}



	public void setCurrencyCodereturn(String currencyCodereturn) {
		this.currencyCodereturn = currencyCodereturn;
	}



	public String getDepartureDatereturn() {
		return departureDatereturn;
	}



	public void setDepartureDatereturn(String departureDatereturn) {
		this.departureDatereturn = departureDatereturn;
	}



	public String getDepartureTerminalreturn() {
		return departureTerminalreturn;
	}



	public void setDepartureTerminalreturn(String departureTerminalreturn) {
		this.departureTerminalreturn = departureTerminalreturn;
	}



	public String getDepartureTimereturn() {
		return departureTimereturn;
	}



	public void setDepartureTimereturn(String departureTimereturn) {
		this.departureTimereturn = departureTimereturn;
	}



	public String getDestinationreturn() {
		return destinationreturn;
	}



	public void setDestinationreturn(String destinationreturn) {
		this.destinationreturn = destinationreturn;
	}



	public String getDurationreturn() {
		return durationreturn;
	}



	public void setDurationreturn(String durationreturn) {
		this.durationreturn = durationreturn;
	}



	public String getFareBasisCodereturn() {
		return fareBasisCodereturn;
	}



	public void setFareBasisCodereturn(String fareBasisCodereturn) {
		this.fareBasisCodereturn = fareBasisCodereturn;
	}



	public String getFareClassOfServicereturn() {
		return fareClassOfServicereturn;
	}



	public void setFareClassOfServicereturn(String fareClassOfServicereturn) {
		this.fareClassOfServicereturn = fareClassOfServicereturn;
	}



	public String getFlightDesignatorreturn() {
		return flightDesignatorreturn;
	}



	public void setFlightDesignatorreturn(String flightDesignatorreturn) {
		this.flightDesignatorreturn = flightDesignatorreturn;
	}



	public String getFlightDetailRefKeyreturn() {
		return flightDetailRefKeyreturn;
	}



	public void setFlightDetailRefKeyreturn(String flightDetailRefKeyreturn) {
		this.flightDetailRefKeyreturn = flightDetailRefKeyreturn;
	}



	public String getFlightNamereturn() {
		return flightNamereturn;
	}



	public void setFlightNamereturn(String flightNamereturn) {
		this.flightNamereturn = flightNamereturn;
	}



	public String getFlightNumberreturn() {
		return flightNumberreturn;
	}



	public void setFlightNumberreturn(String flightNumberreturn) {
		this.flightNumberreturn = flightNumberreturn;
	}



	public String getGroupreturn() {
		return groupreturn;
	}



	public void setGroupreturn(String groupreturn) {
		this.groupreturn = groupreturn;
	}



	public String getConnectingreturn() {
		return connectingreturn;
	}



	public void setConnectingreturn(String connectingreturn) {
		this.connectingreturn = connectingreturn;
	}



	public String getNumberOfStopsreturn() {
		return numberOfStopsreturn;
	}



	public void setNumberOfStopsreturn(String numberOfStopsreturn) {
		this.numberOfStopsreturn = numberOfStopsreturn;
	}



	public String getOriginreturn() {
		return originreturn;
	}



	public void setOriginreturn(String originreturn) {
		this.originreturn = originreturn;
	}



	public String getProviderCodereturn() {
		return providerCodereturn;
	}



	public void setProviderCodereturn(String providerCodereturn) {
		this.providerCodereturn = providerCodereturn;
	}



	public String getRemarksreturn() {
		return remarksreturn;
	}



	public void setRemarksreturn(String remarksreturn) {
		this.remarksreturn = remarksreturn;
	}



	public String getSoldreturn() {
		return soldreturn;
	}



	public void setSoldreturn(String soldreturn) {
		this.soldreturn = soldreturn;
	}



	public String getStatusreturn() {
		return statusreturn;
	}



	public void setStatusreturn(String statusreturn) {
		this.statusreturn = statusreturn;
	}



	public String getDeeplinkreturn() {
		return deeplinkreturn;
	}



	public void setDeeplinkreturn(String deeplinkreturn) {
		this.deeplinkreturn = deeplinkreturn;
	}



	public String getFareIndicatorreturn() {
		return fareIndicatorreturn;
	}



	public void setFareIndicatorreturn(String fareIndicatorreturn) {
		this.fareIndicatorreturn = fareIndicatorreturn;
	}



	public String getFareRulereturn() {
		return fareRulereturn;
	}



	public void setFareRulereturn(String fareRulereturn) {
		this.fareRulereturn = fareRulereturn;
	}



	public String getCachereturn() {
		return cachereturn;
	}



	public void setCachereturn(String cachereturn) {
		this.cachereturn = cachereturn;
	}



	public String getHoldBookingreturn() {
		return holdBookingreturn;
	}



	public void setHoldBookingreturn(String holdBookingreturn) {
		this.holdBookingreturn = holdBookingreturn;
	}



	public String getInternationalreturn() {
		return internationalreturn;
	}



	public void setInternationalreturn(String internationalreturn) {
		this.internationalreturn = internationalreturn;
	}



	public String getRoundTripreturn() {
		return roundTripreturn;
	}



	public void setRoundTripreturn(String roundTripreturn) {
		this.roundTripreturn = roundTripreturn;
	}



	public String getSpecialreturn() {
		return specialreturn;
	}



	public void setSpecialreturn(String specialreturn) {
		this.specialreturn = specialreturn;
	}



	public String getSpecialIdreturn() {
		return specialIdreturn;
	}



	public void setSpecialIdreturn(String specialIdreturn) {
		this.specialIdreturn = specialIdreturn;
	}



	public String getJourneyIndexreturn() {
		return journeyIndexreturn;
	}



	public void setJourneyIndexreturn(String journeyIndexreturn) {
		this.journeyIndexreturn = journeyIndexreturn;
	}



	public String getMemoryCreationTimereturn() {
		return memoryCreationTimereturn;
	}



	public void setMemoryCreationTimereturn(String memoryCreationTimereturn) {
		this.memoryCreationTimereturn = memoryCreationTimereturn;
	}



	public String getNearByAirportreturn() {
		return nearByAirportreturn;
	}



	public void setNearByAirportreturn(String nearByAirportreturn) {
		this.nearByAirportreturn = nearByAirportreturn;
	}



	public String getRemarkreturn() {
		return remarkreturn;
	}



	public void setRemarkreturn(String remarkreturn) {
		this.remarkreturn = remarkreturn;
	}



	public String getSearchIdreturn() {
		return searchIdreturn;
	}



	public void setSearchIdreturn(String searchIdreturn) {
		this.searchIdreturn = searchIdreturn;
	}



	public String getEngineIDreturn() {
		return engineIDreturn;
	}



	public void setEngineIDreturn(String engineIDreturn) {
		this.engineIDreturn = engineIDreturn;
	}



	public String getBasicFarereturn() {
		return basicFarereturn;
	}



	public void setBasicFarereturn(String basicFarereturn) {
		this.basicFarereturn = basicFarereturn;
	}



	public String getExchangeRatereturn() {
		return exchangeRatereturn;
	}



	public void setExchangeRatereturn(String exchangeRatereturn) {
		this.exchangeRatereturn = exchangeRatereturn;
	}



	public String getTotalFareWithOutMarkUpreturn() {
		return totalFareWithOutMarkUpreturn;
	}



	public void setTotalFareWithOutMarkUpreturn(String totalFareWithOutMarkUpreturn) {
		this.totalFareWithOutMarkUpreturn = totalFareWithOutMarkUpreturn;
	}



	public String getTotalTaxWithOutMarkUpreturn() {
		return totalTaxWithOutMarkUpreturn;
	}



	public void setTotalTaxWithOutMarkUpreturn(String totalTaxWithOutMarkUpreturn) {
		this.totalTaxWithOutMarkUpreturn = totalTaxWithOutMarkUpreturn;
	}



	public String getBaseTransactionAmountreturn() {
		return baseTransactionAmountreturn;
	}



	public void setBaseTransactionAmountreturn(String baseTransactionAmountreturn) {
		this.baseTransactionAmountreturn = baseTransactionAmountreturn;
	}



	public String getCancelPenaltyreturn() {
		return cancelPenaltyreturn;
	}



	public void setCancelPenaltyreturn(String cancelPenaltyreturn) {
		this.cancelPenaltyreturn = cancelPenaltyreturn;
	}



	public String getChangePenaltyreturn() {
		return changePenaltyreturn;
	}



	public void setChangePenaltyreturn(String changePenaltyreturn) {
		this.changePenaltyreturn = changePenaltyreturn;
	}



	public String getEquivCurrencyCodereturn() {
		return equivCurrencyCodereturn;
	}



	public void setEquivCurrencyCodereturn(String equivCurrencyCodereturn) {
		this.equivCurrencyCodereturn = equivCurrencyCodereturn;
	}



	public String getFareInfoKeyreturn() {
		return fareInfoKeyreturn;
	}



	public void setFareInfoKeyreturn(String fareInfoKeyreturn) {
		this.fareInfoKeyreturn = fareInfoKeyreturn;
	}



	public String getFareInfoValuereturn() {
		return fareInfoValuereturn;
	}



	public void setFareInfoValuereturn(String fareInfoValuereturn) {
		this.fareInfoValuereturn = fareInfoValuereturn;
	}



	public String getMarkUPreturn() {
		return markUPreturn;
	}



	public void setMarkUPreturn(String markUPreturn) {
		this.markUPreturn = markUPreturn;
	}



	public String getPaxTypereturn() {
		return paxTypereturn;
	}



	public void setPaxTypereturn(String paxTypereturn) {
		this.paxTypereturn = paxTypereturn;
	}



	public String getRefundablereturn() {
		return refundablereturn;
	}



	public void setRefundablereturn(String refundablereturn) {
		this.refundablereturn = refundablereturn;
	}



	public String getTotalFarereturn() {
		return totalFarereturn;
	}



	public void setTotalFarereturn(String totalFarereturn) {
		this.totalFarereturn = totalFarereturn;
	}



	public String getTotalTaxreturn() {
		return totalTaxreturn;
	}



	public void setTotalTaxreturn(String totalTaxreturn) {
		this.totalTaxreturn = totalTaxreturn;
	}



	public String getTransactionAmountreturn() {
		return transactionAmountreturn;
	}



	public void setTransactionAmountreturn(String transactionAmountreturn) {
		this.transactionAmountreturn = transactionAmountreturn;
	}



	public String getBeginDatereturn() {
		return beginDatereturn;
	}



	public void setBeginDatereturn(String beginDatereturn) {
		this.beginDatereturn = beginDatereturn;
	}



	public String getEndDatereturn() {
		return endDatereturn;
	}



	public void setEndDatereturn(String endDatereturn) {
		this.endDatereturn = endDatereturn;
	}



	public String getBookingAmountreturn() {
		return bookingAmountreturn;
	}



	public void setBookingAmountreturn(String bookingAmountreturn) {
		this.bookingAmountreturn = bookingAmountreturn;
	}



	public String getBookingCurrencyCodereturn() {
		return bookingCurrencyCodereturn;
	}



	public void setBookingCurrencyCodereturn(String bookingCurrencyCodereturn) {
		this.bookingCurrencyCodereturn = bookingCurrencyCodereturn;
	}



	public String getVisatypereturn() {
		return visatypereturn;
	}



	public void setVisatypereturn(String visatypereturn) {
		this.visatypereturn = visatypereturn;
	}



	public String getTraceIdreturn() {
		return traceIdreturn;
	}



	public void setTraceIdreturn(String traceIdreturn) {
		this.traceIdreturn = traceIdreturn;
	}



	public String getTransactionIdreturn() {
		return transactionIdreturn;
	}



	public void setTransactionIdreturn(String transactionIdreturn) {
		this.transactionIdreturn = transactionIdreturn;
	}



	public String getAndroidBookingreturn() {
		return androidBookingreturn;
	}



	public void setAndroidBookingreturn(String androidBookingreturn) {
		this.androidBookingreturn = androidBookingreturn;
	}



	public List<BookFare> getBookFaresreturn() {
		return bookFaresreturn;
	}



	public void setBookFaresreturn(List<BookFare> bookFaresreturn) {
		this.bookFaresreturn = bookFaresreturn;
	}



	public String getDomesticreturn() {
		return domesticreturn;
	}



	public void setDomesticreturn(String domesticreturn) {
		this.domesticreturn = domesticreturn;
	}



	public String getClientIp() {
		return clientIp;
	}



	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}



	public String getCllientKey() {
		return cllientKey;
	}



	public void setCllientKey(String cllientKey) {
		this.cllientKey = cllientKey;
	}



	public String getClientToken() {
		return clientToken;
	}



	public void setClientToken(String clientToken) {
		this.clientToken = clientToken;
	}



	public String getClientApiName() {
		return clientApiName;
	}



	public void setClientApiName(String clientApiName) {
		this.clientApiName = clientApiName;
	}



	public ArrayList<BookSegment> getBookSegments() {
		return bookSegments;
	}



	public void setBookSegments(ArrayList<BookSegment> bookSegments) {
		this.bookSegments = bookSegments;
	}



	public String getEngineID() {
		return engineID;
	}



	public void setEngineID(String engineID) {
		this.engineID = engineID;
	}



	public ArrayList<EngineID> getEngineIDList() {
		return engineIDList;
	}



	public void setEngineIDList(ArrayList<EngineID> engineIDList) {
		this.engineIDList = engineIDList;
	}



	public boolean isAndroidBooking() {
		return androidBooking;
	}



	public void setAndroidBooking(boolean androidBooking) {
		this.androidBooking = androidBooking;
	}



	public boolean isDomestic() {
		return domestic;
	}



	public void setDomestic(boolean domestic) {
		this.domestic = domestic;
	}



	public PaymentDetails getPaymentDetails() {
		return paymentDetails;
	}



	public void setPaymentDetails(PaymentDetails paymentDetails) {
		this.paymentDetails = paymentDetails;
	}



	public ArrayList<FlightSearchDetails> getFlightSearchDetails() {
		return flightSearchDetails;
	}



	public void setFlightSearchDetails(ArrayList<FlightSearchDetails> flightSearchDetails) {
		this.flightSearchDetails = flightSearchDetails;
	}



	public String getVisatype() {
		return visatype;
	}



	public void setVisatype(String visatype) {
		this.visatype = visatype;
	}



	public String getTraceId() {
		return traceId;
	}



	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}



	public String getTransactionId() {
		return transactionId;
	}



	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}



	public Travellers getTravellers() {
		return travellers;
	}



	public void setTravellers(Travellers travellers) {
		this.travellers = travellers;
	}



	public String getEmailAddress() {
		return emailAddress;
	}



	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}



	public String getMobileNumber() {
		return mobileNumber;
	}



	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}



	public String getSpKey() {
		return spKey;
	}



	public void setSpKey(String spKey) {
		this.spKey = spKey;
	}



	public String getSessionId() {
		return sessionId;
	}



	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}



	public List<TravellerFlightDetails> getTravellerDetails() {
		return travellerDetails;
	}



	public void setTravellerDetails(List<TravellerFlightDetails> travellerDetails) {
		this.travellerDetails = travellerDetails;
	}



	public String getTicketDetails() {
		return ticketDetails;
	}



	public void setTicketDetails(String ticketDetails) {
		this.ticketDetails = ticketDetails;
	}



	public String getBoundType() {
		return boundType;
	}



	public void setBoundType(String boundType) {
		this.boundType = boundType;
	}



	public String getBaggageFare() {
		return baggageFare;
	}



	public void setBaggageFare(String baggageFare) {
		this.baggageFare = baggageFare;
	}



	public String getSsrFare() {
		return ssrFare;
	}



	public void setSsrFare(String ssrFare) {
		this.ssrFare = ssrFare;
	}



	public String getItineraryKey() {
		return itineraryKey;
	}



	public void setItineraryKey(String itineraryKey) {
		this.itineraryKey = itineraryKey;
	}



	public String getJourneyTime() {
		return journeyTime;
	}



	public void setJourneyTime(String journeyTime) {
		this.journeyTime = journeyTime;
	}



	public String getAddOnDetail() {
		return addOnDetail;
	}



	public void setAddOnDetail(String addOnDetail) {
		this.addOnDetail = addOnDetail;
	}



	public String getAircraftCode() {
		return aircraftCode;
	}



	public void setAircraftCode(String aircraftCode) {
		this.aircraftCode = aircraftCode;
	}



	public String getAircraftType() {
		return aircraftType;
	}



	public void setAircraftType(String aircraftType) {
		this.aircraftType = aircraftType;
	}



	public String getAirlineName() {
		return airlineName;
	}



	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}



	public String getAmount() {
		return amount;
	}



	public void setAmount(String amount) {
		this.amount = amount;
	}



	public String getArrivalDate() {
		return arrivalDate;
	}



	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}



	public String getArrivalTerminal() {
		return arrivalTerminal;
	}



	public void setArrivalTerminal(String arrivalTerminal) {
		this.arrivalTerminal = arrivalTerminal;
	}



	public String getArrivalTime() {
		return arrivalTime;
	}



	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}



	public String getAvailableSeat() {
		return availableSeat;
	}



	public void setAvailableSeat(String availableSeat) {
		this.availableSeat = availableSeat;
	}



	public String getBaggageUnit() {
		return baggageUnit;
	}



	public void setBaggageUnit(String baggageUnit) {
		this.baggageUnit = baggageUnit;
	}



	public String getBaggageWeight() {
		return baggageWeight;
	}



	public void setBaggageWeight(String baggageWeight) {
		this.baggageWeight = baggageWeight;
	}



	public String getBoundTypes() {
		return boundTypes;
	}



	public void setBoundTypes(String boundTypes) {
		this.boundTypes = boundTypes;
	}



	public String getCabin() {
		return cabin;
	}



	public void setCabin(String cabin) {
		this.cabin = cabin;
	}



	public String getCapacity() {
		return capacity;
	}



	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}



	public String getCarrierCode() {
		return carrierCode;
	}



	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}



	public String getCurrencyCode() {
		return currencyCode;
	}



	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}



	public String getDepartureDate() {
		return departureDate;
	}



	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}



	public String getDepartureTerminal() {
		return departureTerminal;
	}



	public void setDepartureTerminal(String departureTerminal) {
		this.departureTerminal = departureTerminal;
	}



	public String getDepartureTime() {
		return departureTime;
	}



	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}



	public String getDestination() {
		return destination;
	}



	public void setDestination(String destination) {
		this.destination = destination;
	}



	public String getDuration() {
		return duration;
	}



	public void setDuration(String duration) {
		this.duration = duration;
	}



	public String getFareBasisCode() {
		return fareBasisCode;
	}



	public void setFareBasisCode(String fareBasisCode) {
		this.fareBasisCode = fareBasisCode;
	}



	public String getFareClassOfService() {
		return fareClassOfService;
	}



	public void setFareClassOfService(String fareClassOfService) {
		this.fareClassOfService = fareClassOfService;
	}



	public String getFlightDesignator() {
		return flightDesignator;
	}



	public void setFlightDesignator(String flightDesignator) {
		this.flightDesignator = flightDesignator;
	}



	public String getFlightDetailRefKey() {
		return flightDetailRefKey;
	}



	public void setFlightDetailRefKey(String flightDetailRefKey) {
		this.flightDetailRefKey = flightDetailRefKey;
	}



	public String getFlightName() {
		return flightName;
	}



	public void setFlightName(String flightName) {
		this.flightName = flightName;
	}



	public String getFlightNumber() {
		return flightNumber;
	}



	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}



	public String getGroup() {
		return group;
	}



	public void setGroup(String group) {
		this.group = group;
	}



	public String getConnecting() {
		return connecting;
	}



	public void setConnecting(String connecting) {
		this.connecting = connecting;
	}



	public String getNumberOfStops() {
		return numberOfStops;
	}



	public void setNumberOfStops(String numberOfStops) {
		this.numberOfStops = numberOfStops;
	}



	public String getOrigin() {
		return origin;
	}



	public void setOrigin(String origin) {
		this.origin = origin;
	}



	public String getProviderCode() {
		return providerCode;
	}



	public void setProviderCode(String providerCode) {
		this.providerCode = providerCode;
	}



	public String getRemarks() {
		return remarks;
	}



	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}



	public String getSold() {
		return sold;
	}



	public void setSold(String sold) {
		this.sold = sold;
	}



	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}



	public String getDeeplink() {
		return deeplink;
	}



	public void setDeeplink(String deeplink) {
		this.deeplink = deeplink;
	}



	public String getFareIndicator() {
		return fareIndicator;
	}



	public void setFareIndicator(String fareIndicator) {
		this.fareIndicator = fareIndicator;
	}



	public String getFareRule() {
		return fareRule;
	}



	public void setFareRule(String fareRule) {
		this.fareRule = fareRule;
	}



	public String getCache() {
		return cache;
	}



	public void setCache(String cache) {
		this.cache = cache;
	}



	public String getHoldBooking() {
		return holdBooking;
	}



	public void setHoldBooking(String holdBooking) {
		this.holdBooking = holdBooking;
	}



	public String getInternational() {
		return international;
	}



	public void setInternational(String international) {
		this.international = international;
	}



	public String getRoundTrip() {
		return roundTrip;
	}



	public void setRoundTrip(String roundTrip) {
		this.roundTrip = roundTrip;
	}



	public String getSpecial() {
		return special;
	}



	public void setSpecial(String special) {
		this.special = special;
	}



	public String getSpecialId() {
		return specialId;
	}



	public void setSpecialId(String specialId) {
		this.specialId = specialId;
	}



	public String getJourneyIndex() {
		return journeyIndex;
	}



	public void setJourneyIndex(String journeyIndex) {
		this.journeyIndex = journeyIndex;
	}



	public String getMemoryCreationTime() {
		return memoryCreationTime;
	}



	public void setMemoryCreationTime(String memoryCreationTime) {
		this.memoryCreationTime = memoryCreationTime;
	}



	public String getNearByAirport() {
		return nearByAirport;
	}



	public void setNearByAirport(String nearByAirport) {
		this.nearByAirport = nearByAirport;
	}



	public String getRemark() {
		return remark;
	}



	public void setRemark(String remark) {
		this.remark = remark;
	}



	public String getSearchId() {
		return searchId;
	}



	public void setSearchId(String searchId) {
		this.searchId = searchId;
	}



	public String getBasicFare() {
		return basicFare;
	}



	public void setBasicFare(String basicFare) {
		this.basicFare = basicFare;
	}



	public String getExchangeRate() {
		return exchangeRate;
	}



	public void setExchangeRate(String exchangeRate) {
		this.exchangeRate = exchangeRate;
	}



	public String getTotalFareWithOutMarkUp() {
		return totalFareWithOutMarkUp;
	}



	public void setTotalFareWithOutMarkUp(String totalFareWithOutMarkUp) {
		this.totalFareWithOutMarkUp = totalFareWithOutMarkUp;
	}



	public String getTotalTaxWithOutMarkUp() {
		return totalTaxWithOutMarkUp;
	}



	public void setTotalTaxWithOutMarkUp(String totalTaxWithOutMarkUp) {
		this.totalTaxWithOutMarkUp = totalTaxWithOutMarkUp;
	}



	public String getBaseTransactionAmount() {
		return baseTransactionAmount;
	}



	public void setBaseTransactionAmount(String baseTransactionAmount) {
		this.baseTransactionAmount = baseTransactionAmount;
	}



	public String getCancelPenalty() {
		return cancelPenalty;
	}



	public void setCancelPenalty(String cancelPenalty) {
		this.cancelPenalty = cancelPenalty;
	}



	public String getChangePenalty() {
		return changePenalty;
	}



	public void setChangePenalty(String changePenalty) {
		this.changePenalty = changePenalty;
	}



	public String getEquivCurrencyCode() {
		return equivCurrencyCode;
	}



	public void setEquivCurrencyCode(String equivCurrencyCode) {
		this.equivCurrencyCode = equivCurrencyCode;
	}



	public String getFareInfoKey() {
		return fareInfoKey;
	}



	public void setFareInfoKey(String fareInfoKey) {
		this.fareInfoKey = fareInfoKey;
	}



	public String getFareInfoValue() {
		return fareInfoValue;
	}



	public void setFareInfoValue(String fareInfoValue) {
		this.fareInfoValue = fareInfoValue;
	}



	public String getMarkUP() {
		return markUP;
	}



	public void setMarkUP(String markUP) {
		this.markUP = markUP;
	}



	public String getPaxType() {
		return paxType;
	}



	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}



	public String getRefundable() {
		return refundable;
	}



	public void setRefundable(String refundable) {
		this.refundable = refundable;
	}



	public String getTotalFare() {
		return totalFare;
	}



	public void setTotalFare(String totalFare) {
		this.totalFare = totalFare;
	}



	public String getTotalTax() {
		return totalTax;
	}



	public void setTotalTax(String totalTax) {
		this.totalTax = totalTax;
	}



	public String getTransactionAmount() {
		return transactionAmount;
	}



	public void setTransactionAmount(String transactionAmount) {
		this.transactionAmount = transactionAmount;
	}



	public String getBeginDate() {
		return beginDate;
	}



	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}



	public String getEndDate() {
		return endDate;
	}



	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}



	public String getBookingAmount() {
		return bookingAmount;
	}



	public void setBookingAmount(String bookingAmount) {
		this.bookingAmount = bookingAmount;
	}



	public String getBookingCurrencyCode() {
		return bookingCurrencyCode;
	}



	public void setBookingCurrencyCode(String bookingCurrencyCode) {
		this.bookingCurrencyCode = bookingCurrencyCode;
	}



	public String getAddress1() {
		return address1;
	}



	public void setAddress1(String address1) {
		this.address1 = address1;
	}



	public String getAddress2() {
		return address2;
	}



	public void setAddress2(String address2) {
		this.address2 = address2;
	}



	public String getCity() {
		return city;
	}



	public void setCity(String city) {
		this.city = city;
	}



	public String getCountryCode() {
		return countryCode;
	}



	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}



	public String getCultureCode() {
		return cultureCode;
	}



	public void setCultureCode(String cultureCode) {
		this.cultureCode = cultureCode;
	}



	public String getDateofBirth() {
		return dateofBirth;
	}



	public void setDateofBirth(String dateofBirth) {
		this.dateofBirth = dateofBirth;
	}



	public String getFirstName() {
		return firstName;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public String getFrequentFlierNumber() {
		return frequentFlierNumber;
	}



	public void setFrequentFlierNumber(String frequentFlierNumber) {
		this.frequentFlierNumber = frequentFlierNumber;
	}



	public String getGender() {
		return gender;
	}



	public void setGender(String gender) {
		this.gender = gender;
	}



	public String getHomePhone() {
		return homePhone;
	}



	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public String getMeal() {
		return meal;
	}



	public void setMeal(String meal) {
		this.meal = meal;
	}



	public String getMiddleName() {
		return middleName;
	}



	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}



	public String getNationality() {
		return nationality;
	}



	public void setNationality(String nationality) {
		this.nationality = nationality;
	}



	public String getPassportExpiryDate() {
		return passportExpiryDate;
	}



	public void setPassportExpiryDate(String passportExpiryDate) {
		this.passportExpiryDate = passportExpiryDate;
	}



	public String getPassportNo() {
		return passportNo;
	}



	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}



	public String getProvisionState() {
		return provisionState;
	}



	public void setProvisionState(String provisionState) {
		this.provisionState = provisionState;
	}



	public String getResidentCountry() {
		return residentCountry;
	}



	public void setResidentCountry(String residentCountry) {
		this.residentCountry = residentCountry;
	}



	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



	public String getTxnRefNo() {
		return txnRefNo;
	}



	public void setTxnRefNo(String txnRefNo) {
		this.txnRefNo = txnRefNo;
	}



	public List<BookFare> getBookFares() {
		return bookFares;
	}



	public void setBookFares(List<BookFare> bookFares) {
		this.bookFares = bookFares;
	}



	public String getGrandtotal() {
		return grandtotal;
	}



	public void setGrandtotal(String grandtotal) {
		this.grandtotal = grandtotal;
	}



	public String getAdults() {
		return adults;
	}



	public void setAdults(String adults) {
		this.adults = adults;
	}



	public String getChilds() {
		return childs;
	}



	public void setChilds(String childs) {
		this.childs = childs;
	}



	public String getInfants() {
		return infants;
	}



	public void setInfants(String infants) {
		this.infants = infants;
	}



	public String getMerchantRefNo() {
		return merchantRefNo;
	}



	public void setMerchantRefNo(String merchantRefNo) {
		this.merchantRefNo = merchantRefNo;
	}



	public String getFirstNamechild() {
		return firstNamechild;
	}



	public void setFirstNamechild(String firstNamechild) {
		this.firstNamechild = firstNamechild;
	}



	public String getLastNamechild() {
		return lastNamechild;
	}



	public void setLastNamechild(String lastNamechild) {
		this.lastNamechild = lastNamechild;
	}



	public String getFirstNameinfant() {
		return firstNameinfant;
	}



	public void setFirstNameinfant(String firstNameinfant) {
		this.firstNameinfant = firstNameinfant;
	}



	public String getLastNameinfant() {
		return lastNameinfant;
	}



	public void setLastNameinfant(String lastNameinfant) {
		this.lastNameinfant = lastNameinfant;
	}



	@Override
	public String getJsonRequest() {
		
		if (flightType.equalsIgnoreCase("DirectConnecting") || flightType.equalsIgnoreCase("RoundwayConnecting")) {
		
			JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();

	 	JsonArrayBuilder BookingSegment = Json.createArrayBuilder();
        JsonObjectBuilder segList = Json.createObjectBuilder();
        for (int i = 0; i < bookSegments.size(); i++) {
//        	 segList.add("bondType",  (bookSegments.get(i).getBondType() == null) ? "" : bookSegments.get(i).getBondType());
         	JsonArrayBuilder Bonds = Json.createArrayBuilder();
         	JsonObjectBuilder bondsList = Json.createObjectBuilder();
         	for (int j = 0; j < bookSegments.get(i).getBonds().size(); j++) {
         		bondsList.add("boundType", (bookSegments.get(i).getBonds().get(j).getBoundType() == null) ? "" : bookSegments.get(i).getBonds().get(j).getBoundType());
             	bondsList.add("baggageFare", bookSegments.get(i).getBonds().get(j).isBaggageFare());
             	bondsList.add("ssrFare", bookSegments.get(i).getBonds().get(j).isSsrFare());
             	bondsList.add("itineraryKey", bookSegments.get(i).getBonds().get(j).getItineraryKey());
             	bondsList.add("journeyTime", bookSegments.get(i).getBonds().get(j).getJourneyTime());
             		JsonArrayBuilder Legs = Json.createArrayBuilder();
             		JsonObjectBuilder LegsList = Json.createObjectBuilder();
             		for (int k = 0; k < bookSegments.get(i).getBonds().get(j).getLegs().size(); k++) {
             			LegsList.add("aircraftCode", (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getAircraftCode() == null) ? "" : bookSegments.get(i).getBonds().get(j).getLegs().get(k).getAircraftCode());
                 		LegsList.add("aircraftType", (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getAircraftType() == null) ? "" : bookSegments.get(i).getBonds().get(j).getLegs().get(k).getAircraftType());
                 		LegsList.add("airlineName", (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getAirlineName() == null) ? "" : bookSegments.get(i).getBonds().get(j).getLegs().get(k).getAirlineName());
//                 		LegsList.add("amount", bookSegments.get(i).getBonds().get(j).getLegs().get(k).getAmount());
                 		LegsList.add("arrivalDate", bookSegments.get(i).getBonds().get(j).getLegs().get(k).getArrivalDate());
                 		LegsList.add("arrivalTerminal", (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getArrivalTerminal() == null) ? "" : bookSegments.get(i).getBonds().get(j).getLegs().get(k).getArrivalTerminal());
                 		LegsList.add("arrivalTime", bookSegments.get(i).getBonds().get(j).getLegs().get(k).getArrivalTime());
                 		LegsList.add("availableSeat", (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getAvailableSeat() == null) ? "" : bookSegments.get(i).getBonds().get(j).getLegs().get(k).getAvailableSeat());
                 		LegsList.add("baggageUnit", bookSegments.get(i).getBonds().get(j).getLegs().get(k).getBaggageUnit());
                 		LegsList.add("baggageWeight", bookSegments.get(i).getBonds().get(j).getLegs().get(k).getBaggageWeight());
                 		LegsList.add("boundTypes", (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getBoundTypes() == null) ? "" : bookSegments.get(i).getBonds().get(j).getLegs().get(k).getBoundTypes());
                 		LegsList.add("cabin", bookSegments.get(i).getBonds().get(j).getLegs().get(k).getCabin());
                 		
                 		LegsList.add("capacity", (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getCapacity() == null) ? "" : bookSegments.get(i).getBonds().get(j).getLegs().get(k).getCapacity());
                 		LegsList.add("carrierCode", (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getCarrierCode() == null) ? "" : bookSegments.get(i).getBonds().get(j).getLegs().get(k).getCarrierCode());
                 		LegsList.add("currencyCode", (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getCurrencyCode() == null) ? "" : bookSegments.get(i).getBonds().get(j).getLegs().get(k).getCurrencyCode());
                 		LegsList.add("departureDate", (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getDepartureDate() == null) ? "" : bookSegments.get(i).getBonds().get(j).getLegs().get(k).getDepartureDate());
                 		LegsList.add("departureTerminal", (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getDepartureTerminal() == null) ? "" : bookSegments.get(i).getBonds().get(j).getLegs().get(k).getDepartureTerminal());
                 		LegsList.add("departureTime", (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getDepartureTime() == null) ? "" : bookSegments.get(i).getBonds().get(j).getLegs().get(k).getDepartureTime());
                 		LegsList.add("destination", (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getDestination() == null) ? "" : bookSegments.get(i).getBonds().get(j).getLegs().get(k).getDestination());
                 		LegsList.add("duration", (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getDuration() == null) ? "" : bookSegments.get(i).getBonds().get(j).getLegs().get(k).getDuration());
                 		LegsList.add("fareBasisCode", (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getFareBasisCode() == null) ? "" : bookSegments.get(i).getBonds().get(j).getLegs().get(k).getFareBasisCode());
                 		LegsList.add("fareClassOfService", (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getFareClassOfService() == null) ? "" : bookSegments.get(i).getBonds().get(j).getLegs().get(k).getFareClassOfService());
                 		LegsList.add("flightDesignator", (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getFlightDesignator() == null) ? "" : bookSegments.get(i).getBonds().get(j).getLegs().get(k).getFlightDesignator());
                 		LegsList.add("flightDetailRefKey", (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getFlightDetailRefKey() == null) ? "" : bookSegments.get(i).getBonds().get(j).getLegs().get(k).getFlightDetailRefKey());
//                 		LegsList.add("flightName", (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getFlightName() == null ) ? "" : bookSegments.get(i).getBonds().get(j).getLegs().get(k).getFlightName());
                 		LegsList.add("flightNumber", (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getFlightNumber() == null) ? "" : bookSegments.get(i).getBonds().get(j).getLegs().get(k).getFlightNumber());
//                 		LegsList.add("group", (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getGroup() == null) ? "" : bookSegments.get(i).getBonds().get(j).getLegs().get(k).getGroup());
//                 		LegsList.add("isConnecting", bookSegments.get(i).getBonds().get(j).getLegs().get(k).isConnecting());
//                 		LegsList.add("numberOfStops", (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getNumberOfStops() == null) ? "" : bookSegments.get(i).getBonds().get(j).getLegs().get(k).getNumberOfStops());
                 		LegsList.add("origin", (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getOrigin() == null) ? "" : bookSegments.get(i).getBonds().get(j).getLegs().get(k).getOrigin());
//                 		LegsList.add("providerCode", (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getProviderCode() == null) ?  "" : bookSegments.get(i).getBonds().get(j).getLegs().get(k).getProviderCode());
//                 		LegsList.add("remarks", (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getRemarks() == null) ? "" : bookSegments.get(i).getBonds().get(j).getLegs().get(k).getRemarks());

                        LegsList.add("sold", bookSegments.get(i).getBonds().get(j).getLegs().get(k).getSold());
                 		LegsList.add("status", (bookSegments.get(i).getBonds().get(j).getLegs().get(k).getStatus() == null) ? "" : bookSegments.get(i).getBonds().get(j).getLegs().get(k).getStatus());
                 		Legs.add(LegsList.build());
					}
                 bondsList.add("legs", Legs);
     			bondsList.add("addOnDetail", (bookSegments.get(i).getBonds().get(j).getAddOnDetail() == null) ?	"" : bookSegments.get(i).getBonds().get(j).getAddOnDetail());
     		Bonds.add(bondsList.build());
			}
         segList.add("bonds", Bonds);
         segList.add("deeplink", (bookSegments.get(i).getDeeplink() == null) ? "" : bookSegments.get(i).getDeeplink());
         segList.add("engineID", getEngineID());
         	JsonObjectBuilder Fare = Json.createObjectBuilder();
         	Fare.add("basicFare", bookSegments.get(i).getFares().getBasicFare());
			Fare.add("exchangeRate", bookSegments.get(i).getFares().getExchangeRate());
 			JsonArrayBuilder PaxFares = Json.createArrayBuilder();
				JsonObjectBuilder PaxFaresList = Json.createObjectBuilder();
				for (int m = 0; m < bookSegments.get(i).getFares().getPaxFares().size(); m++) {
					PaxFaresList.add("baggageUnit", (bookSegments.get(i).getFares().getPaxFares().get(m).getBaggageUnit() == null) ? "" : bookSegments.get(i).getFares().getPaxFares().get(m).getBaggageUnit());	
	 				PaxFaresList.add("baggageWeight", (bookSegments.get(i).getFares().getPaxFares().get(m).getBaggageWeight() == null)? "" : bookSegments.get(i).getFares().getPaxFares().get(m).getBaggageWeight());
	 				PaxFaresList.add("baseTransactionAmount", bookSegments.get(i).getFares().getPaxFares().get(m).getBaseTransactionAmount());
//	 				PaxFaresList.add("basicFare", bookSegments.get(i).getFares().getPaxFares().get(m).getBasicFare());
	 				PaxFaresList.add("cancelPenalty", bookSegments.get(i).getFares().getPaxFares().get(m).getCancelPenalty());
	 				PaxFaresList.add("changePenalty", bookSegments.get(i).getFares().getPaxFares().get(m).getChangePenalty());
	 				PaxFaresList.add("equivCurrencyCode", (bookSegments.get(i).getFares().getPaxFares().get(m).getEquivCurrencyCode() == null ) ? "" : bookSegments.get(i).getFares().getPaxFares().get(m).getEquivCurrencyCode());
	 					JsonArrayBuilder Fares = Json.createArrayBuilder();
	 					JsonObjectBuilder FaresList = Json.createObjectBuilder();
	 					for (int n = 0; n < bookSegments.get(i).getFares().getPaxFares().get(m).getBookFares().size(); n++) {
	 						FaresList.add("amount", bookSegments.get(i).getFares().getPaxFares().get(m).getBookFares().get(n).getAmount());
	 	 					FaresList.add("chargeCode", bookSegments.get(i).getFares().getPaxFares().get(m).getBookFares().get(n).getChargeCode());
	 	 					FaresList.add("chargeType", bookSegments.get(i).getFares().getPaxFares().get(m).getBookFares().get(n).getChargeType());
	 	 					Fares.add(FaresList.build());
					}
	 				PaxFaresList.add("bookFares", Fares);
	 				PaxFaresList.add("fareBasisCode", (bookSegments.get(i).getFares().getPaxFares().get(m).getFareBasisCode() == null) ? "" : bookSegments.get(i).getFares().getPaxFares().get(m).getFareBasisCode());
	 				PaxFaresList.add("fareInfoKey", (bookSegments.get(i).getFares().getPaxFares().get(m).getFareInfoKey() == null) ? "" : bookSegments.get(i).getFares().getPaxFares().get(m).getFareInfoKey());
	 				PaxFaresList.add("fareInfoValue", (bookSegments.get(i).getFares().getPaxFares().get(m).getFareInfoValue() == null ) ? "" : bookSegments.get(i).getFares().getPaxFares().get(m).getFareInfoValue());
	 				PaxFaresList.add("markUP", bookSegments.get(i).getFares().getPaxFares().get(m).getMarkUP());
	 				PaxFaresList.add("paxType", bookSegments.get(i).getFares().getPaxFares().get(m).getPaxType());
	 				PaxFaresList.add("refundable", bookSegments.get(i).getFares().getPaxFares().get(m).isRefundable());
	 				PaxFaresList.add("totalFare", bookSegments.get(i).getFares().getPaxFares().get(m).getTotalFare());
	 				PaxFaresList.add("totalTax", bookSegments.get(i).getFares().getPaxFares().get(m).getTotalTax());
	 				PaxFaresList.add("transactionAmount", bookSegments.get(i).getFares().getPaxFares().get(m).getTransactionAmount());
	 				PaxFares.add(PaxFaresList.build());
			}
			Fare.add("paxFares", PaxFares);
			Fare.add("totalFareWithOutMarkUp", bookSegments.get(i).getFares().getTotalFareWithOutMarkUp());
			Fare.add("totalTaxWithOutMarkUp", bookSegments.get(i).getFares().getTotalTaxWithOutMarkUp());

		 segList.add("fares", Fare);
		 bookSegments.get(i).setFareIndicator("0");
		 segList.add("fareIndicator", (bookSegments.get(i).getFareIndicator() == null ) ? "" : bookSegments.get(i).getFareIndicator());
     	 segList.add("fareRule", (bookSegments.get(i).getFareRule() == null ) ? "" : bookSegments.get(i).getFareRule());
         segList.add("baggageFare", bookSegments.get(i).isBaggageFare());
         segList.add("cache", bookSegments.get(i).isCache());
         segList.add("holdBooking", bookSegments.get(i).isHoldBooking());
         segList.add("international", bookSegments.get(i).isInternational());
         segList.add("roundTrip", bookSegments.get(i).isRoundTrip());
         segList.add("special", bookSegments.get(i).isSpecial());
         segList.add("specialId", bookSegments.get(i).isSpecialId());
         segList.add("itineraryKey", (bookSegments.get(i).getItineraryKey() == null) ? "" : bookSegments.get(i).getItineraryKey());
         segList.add("journeyIndex", bookSegments.get(i).getJourneyIndex());
         bookSegments.get(i).setMemoryCreationTime("/Date(1499158249483+0530)/");
         segList.add("memoryCreationTime", (bookSegments.get(i).getMemoryCreationTime() == null) ? "" : bookSegments.get(i).getMemoryCreationTime());
         segList.add("nearByAirport", bookSegments.get(i).isNearByAirport());
         segList.add("remark", (bookSegments.get(i).getRemark() == null) ? "" : bookSegments.get(i).getRemark());
         segList.add("searchId", (bookSegments.get(i).getSearchId() == null) ? "" : bookSegments.get(i).getSearchId());
         BookingSegment.add(segList.build());
		}
        
        jsonBuilder.add("clientIp", "192.170.1.116");
        jsonBuilder.add("cllientKey", MdexConstants.getMdexCredentials().getCode());
        jsonBuilder.add("clientToken", MdexConstants.getMdexCredentials().getStatus());
        jsonBuilder.add("clientApiName", "Flight Search");
        jsonBuilder.add("spKey", bookSegments.get(0).getBonds().get(0).getLegs().get(0).getAirlineName() == null ? "" : bookSegments.get(0).getBonds().get(0).getLegs().get(0).getAirlineName() + "IN");
        jsonBuilder.add("bookSegments", BookingSegment);
        jsonBuilder.add("engineID", getEngineID());
        JsonArrayBuilder engineId = Json.createArrayBuilder();
	    for (int i = 0; i < engineIDList.size(); i++) {
			engineId.add(engineIDList.get(i).ordinal());
		}
	    jsonBuilder.add("engineIDList", engineId);
        jsonBuilder.add("androidBooking", true);
        jsonBuilder.add("domestic", isDomestic());
        
        JsonObjectBuilder PaymentDetails = Json.createObjectBuilder();
        PaymentDetails.add("bookingAmount", paymentDetails.getBookingAmount());
        PaymentDetails.add("bookingCurrencyCode", (paymentDetails.getBookingCurrencyCode() == null) ? "" : paymentDetails.getBookingCurrencyCode());
        jsonBuilder.add("paymentDetails", PaymentDetails);
        
        JsonArrayBuilder FlightSearchDetails = Json.createArrayBuilder();
		JsonObjectBuilder SearchDetailList = Json.createObjectBuilder();
		for (int j = 0; j < getFlightSearchDetails().size(); j++) {
			SearchDetailList.add("beginDate", (getFlightSearchDetails().get(j).getBeginDate() == null ) ? "" : getFlightSearchDetails().get(j).getBeginDate());
			SearchDetailList.add("destination", (getFlightSearchDetails().get(j).getDestination() == null ) ? "" : getFlightSearchDetails().get(j).getDestination());
			if (getFlightSearchDetails().get(j).getEndDate()==null) {
				SearchDetailList.add("endDate", (getFlightSearchDetails().get(j).getBeginDate() == null) ? "" : getFlightSearchDetails().get(j).getBeginDate());
			}
			else {
				SearchDetailList.add("endDate", (getFlightSearchDetails().get(j).getEndDate() == null) ? "" : getFlightSearchDetails().get(j).getEndDate());
			}
//			SearchDetailList.add("endDate", (getFlightSearchDetails().get(j).getEndDate() == null) ? "" : getFlightSearchDetails().get(j).getEndDate());
			SearchDetailList.add("origin", (getFlightSearchDetails().get(j). getOrigin() == null) ? "" : getFlightSearchDetails().get(j). getOrigin());
			FlightSearchDetails.add(SearchDetailList.build());
		}
		
	    jsonBuilder.add("clientIp", "192.170.1.116");
        jsonBuilder.add("cllientKey", MdexConstants.getMdexCredentials().getCode());
        jsonBuilder.add("clientToken", MdexConstants.getMdexCredentials().getStatus());
        jsonBuilder.add("clientApiName", "Flight Book");
		
        jsonBuilder.add("flightSearchDetails", FlightSearchDetails);
        
        jsonBuilder.add("visatype", (getVisatype() == null ) ? "" : getVisatype());
        jsonBuilder.add("traceId", (getTraceId() == null ) ? "" : getTraceId());
        jsonBuilder.add("transactionId", (getTransactionId() == null ) ? "" : getTransactionId());
        
        
        
        JsonObjectBuilder travel=createMobilejsonAllraveler(getTravellerDetails(), getEmailAddress(), getMobileNumber());
        
		jsonBuilder.add("travellers", travel);
     
        JsonObject empObj = jsonBuilder.build();
        StringWriter jsnReqStr = new StringWriter();
        JsonWriter jsonWtr = Json.createWriter(jsnReqStr);
        jsonWtr.writeObject(empObj);
        jsonWtr.close();
         System.out.println("service id : " +  getSpKey());
        System.out.println(jsnReqStr.toString());
		
		return jsnReqStr.toString();
	}
		return null;
	}
	
	
	
	public static JsonObjectBuilder createMobilejsonAllraveler(List<TravellerFlightDetails> travellerDetails, String email,String mobile) {
		JsonObjectBuilder travellers = Json.createObjectBuilder();
		JsonArrayBuilder adultTraveller = Json.createArrayBuilder();
		JsonObjectBuilder adultTravellerList = Json.createObjectBuilder();
		JsonArrayBuilder infantTraveller = Json.createArrayBuilder();
		JsonObjectBuilder infantTravellerList = Json.createObjectBuilder();
		JsonArrayBuilder childTraveller = Json.createArrayBuilder();
		JsonObjectBuilder childTravellerList = Json.createObjectBuilder();
		
		try {
				for (int i = 0; i < travellerDetails.size(); i++) {
				
					if (travellerDetails.get(i).getType()!=null) {
						if (travellerDetails.get(i).getType().equalsIgnoreCase("Adult")) {
							
							adultTravellerList.add("address1", "BTM");
							adultTravellerList.add("address2", "BTM");
							adultTravellerList.add("city", "Bangalore");
							adultTravellerList.add("countryCode", "IN");
							adultTravellerList.add("cultureCode", "String content");
							adultTravellerList.add("dateofBirth", "");
							adultTravellerList.add("emailAddress", email);
							adultTravellerList.add("firstName", travellerDetails.get(i).getfName());
							adultTravellerList.add("frequentFlierNumber", "");
							adultTravellerList.add("gender", travellerDetails.get(i).getGender());
							adultTravellerList.add("homePhone", "");
							adultTravellerList.add("lastName", travellerDetails.get(i).getlName());
							adultTravellerList.add("meal", "VGML");
							adultTravellerList.add("middleName", "");
							adultTravellerList.add("mobileNumber", mobile );
							adultTravellerList.add("nationality", "India");
							adultTravellerList.add("passportExpiryDate", "");
							adultTravellerList.add("passportNo", "");
							adultTravellerList.add("paxType", "ADT");
							adultTravellerList.add("provisionState", "Karnataka");
							adultTravellerList.add("residentCountry", "IN");
							adultTravellerList.add("title", "Mr.");
							adultTraveller.add(adultTravellerList);
							
						}
						travellers.add("adultTravellers", adultTraveller);
						if (travellerDetails.get(i).getType().equalsIgnoreCase("Child")) {
							
							childTravellerList.add("address1", "BTM");
							childTravellerList.add("address2", "BTM");
							childTravellerList.add("city", "Bangalore");
							childTravellerList.add("countryCode", "IN");
							childTravellerList.add("cultureCode", "String content");
							childTravellerList.add("dateofBirth", "");
							childTravellerList.add("emailAddress", email);
							childTravellerList.add("firstName", travellerDetails.get(i).getfName());
							childTravellerList.add("frequentFlierNumber", "");
							childTravellerList.add("gender", travellerDetails.get(i).getGender());
							childTravellerList.add("homePhone", "");
							childTravellerList.add("lastName", travellerDetails.get(i).getlName());
							childTravellerList.add("meal", "VGML");
							childTravellerList.add("middleName", "");
							childTravellerList.add("mobileNumber", mobile );
							childTravellerList.add("nationality", "India");
							childTravellerList.add("passportExpiryDate", "");
							childTravellerList.add("passportNo", "");
							childTravellerList.add("paxType", "ADT");
							childTravellerList.add("provisionState", "Karnataka");
							childTravellerList.add("residentCountry", "IN");
							childTravellerList.add("title", "Mr.");
							childTraveller.add(childTravellerList);
						}
						travellers.add("childTravellers", childTraveller);
						
						if (travellerDetails.get(i).getType().equalsIgnoreCase("Infant")) {
							
							infantTravellerList.add("address1", "BTM");
							infantTravellerList.add("address2", "BTM");
							infantTravellerList.add("city", "Bangalore");
							infantTravellerList.add("countryCode", "IN");
							infantTravellerList.add("cultureCode", "String content");
							infantTravellerList.add("dateofBirth", "");
							infantTravellerList.add("emailAddress", email);
							infantTravellerList.add("firstName", travellerDetails.get(i).getfName());
							infantTravellerList.add("frequentFlierNumber", "");
							infantTravellerList.add("gender", travellerDetails.get(i).getGender());
							infantTravellerList.add("homePhone", "");
							infantTravellerList.add("lastName", travellerDetails.get(i).getlName());
							infantTravellerList.add("meal", "VGML");
							infantTravellerList.add("middleName", "");
							infantTravellerList.add("mobileNumber", mobile );
							infantTravellerList.add("nationality", "India");
							infantTravellerList.add("passportExpiryDate", "");
							infantTravellerList.add("passportNo", "");
							infantTravellerList.add("paxType", "ADT");
							infantTravellerList.add("provisionState", "Karnataka");
							infantTravellerList.add("residentCountry", "IN");
							infantTravellerList.add("title", "Mr.");
							infantTraveller.add(infantTravellerList);
						}
						travellers.add("infantTravellers", infantTraveller);
					}
				}
		}
			
		 catch (Exception e) {

		}
		return travellers;
	}
}
