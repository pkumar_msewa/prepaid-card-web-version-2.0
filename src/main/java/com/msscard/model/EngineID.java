package com.msscard.model;

public enum EngineID {

	Indigo("Indigo"), Spicjet("Spicjet"), Deleted("Deleted"), Success(
			"Success"), Failed("Failed"), GoAir("GoAir"), AirCosta("AirCosta"), TravelPort("TravelPort"), Initiated("Initiated"),
			Booked("Booked"), AirAsia("AirAsia"), Trujet("Trujet");

	private final String value;

	private EngineID(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public static EngineID getEnum(String value) {
		if (value == null)
			throw new IllegalArgumentException();
		for (EngineID v : values())
			if (value.equalsIgnoreCase(v.getValue()))
				return v;
		throw new IllegalArgumentException();
	}
}
