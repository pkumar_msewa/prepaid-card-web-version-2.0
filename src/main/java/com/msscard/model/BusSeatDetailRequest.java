package com.msscard.model;

public class BusSeatDetailRequest {
	
	private String busId;

	private boolean seater;

	private boolean sleeper;

	private int engineId;

	private String journeyDate;

	private String sessionId;

	private String bpId;

	private String dpId;

	private boolean bpdpLayout;
	
	private String routeid;
	
	private String searchReq;

	public String getBusId() {
		return busId;
	}

	public void setBusId(String busId) {
		this.busId = busId;
	}

	public boolean isSeater() {
		return seater;
	}

	public void setSeater(boolean seater) {
		this.seater = seater;
	}

	public boolean isSleeper() {
		return sleeper;
	}

	public void setSleeper(boolean sleeper) {
		this.sleeper = sleeper;
	}

	public int getEngineId() {
		return engineId;
	}

	public void setEngineId(int engineId) {
		this.engineId = engineId;
	}

	public String getJourneyDate() {
		return journeyDate;
	}

	public void setJourneyDate(String journeyDate) {
		this.journeyDate = journeyDate;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getBpId() {
		return bpId;
	}

	public void setBpId(String bpId) {
		this.bpId = bpId;
	}

	public String getDpId() {
		return dpId;
	}

	public void setDpId(String dpId) {
		this.dpId = dpId;
	}

	

	public boolean isBpdpLayout() {
		return bpdpLayout;
	}

	public void setBpdpLayout(boolean bpdpLayout) {
		this.bpdpLayout = bpdpLayout;
	}

	public String getRouteid() {
		return routeid;
	}

	public void setRouteid(String routeid) {
		this.routeid = routeid;
	}

	public String getSearchReq() {
		return searchReq;
	}

	public void setSearchReq(String searchReq) {
		this.searchReq = searchReq;
	}

	
}
