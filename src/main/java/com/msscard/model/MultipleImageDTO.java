package com.msscard.model;

import org.springframework.web.multipart.MultipartFile;

public class MultipleImageDTO {

	private MultipartFile image;

	public MultipartFile getImage() {
		return image;
	}

	public void setImage(MultipartFile image) {
		this.image = image;
	}
	
	
}
