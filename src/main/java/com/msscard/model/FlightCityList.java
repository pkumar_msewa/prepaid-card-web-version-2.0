package com.msscard.model;

import java.util.List;

import com.msscard.entity.FlightAirLineList;

public class FlightCityList {

	
	private String code;
	
	private String status;
	
	private String message;
	
	private List<FlightAirLineList> details;


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public List<FlightAirLineList> getDetails() {
		return details;
	}


	public void setDetails(List<FlightAirLineList> details) {
		this.details = details;
	}


	
	
	
}
