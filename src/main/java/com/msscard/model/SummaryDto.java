package com.msscard.model;

public class SummaryDto {
	
	private double upiLoadMoneySum;
	private double payementGatewayLoadMoneySum;
    private Long physicalCardRequested;
    private Long physicalCardActive;
    private Long totalVirtualCard;
    private Long newUser;
    private Long upiLoadMoney;
    private Long paymentGatewayLoadMoney;
    private double upiRefundSum;
    private double paymentByMdex;
    private double sendMoneyDetails;
    
	
	public double getSendMoneyDetails() {
		return sendMoneyDetails;
	}
	public void setSendMoneyDetails(double sendMoneyDetails) {
		this.sendMoneyDetails = sendMoneyDetails;
	}
	public double getPaymentByMdex() {
		return paymentByMdex;
	}
	public void setPaymentByMdex(double paymentByMdex) {
		this.paymentByMdex = paymentByMdex;
	}
	public double getUpiRefundSum() {
		return upiRefundSum;
	}
	public void setUpiRefundSum(double upiRefundSum) {
		this.upiRefundSum = upiRefundSum;
	}
	public double getPayementGatewayLoadMoneySum() {
		return payementGatewayLoadMoneySum;
	}
	public void setPayementGatewayLoadMoneySum(double payementGatewayLoadMoneySum) {
		this.payementGatewayLoadMoneySum = payementGatewayLoadMoneySum;
	}
	
	public double getUpiLoadMoneySum() {
		return upiLoadMoneySum;
	}
	public void setUpiLoadMoneySum(double upiLoadMoneySum) {
		this.upiLoadMoneySum = upiLoadMoneySum;
	}
	public Long getUpiLoadMoney() {
		return upiLoadMoney;
	}
	public void setUpiLoadMoney(Long upiLoadMoney) {
		this.upiLoadMoney = upiLoadMoney;
	}
	public Long getPaymentGatewayLoadMoney() {
		return paymentGatewayLoadMoney;
	}
	public void setPaymentGatewayLoadMoney(Long paymentGatewayLoadMoney) {
		this.paymentGatewayLoadMoney = paymentGatewayLoadMoney;
	}
	public Long getPhysicalCardRequested() {
		return physicalCardRequested;
	}
	public void setPhysicalCardRequested(Long physicalCardRequested) {
		this.physicalCardRequested = physicalCardRequested;
	}
	public Long getPhysicalCardActive() {
		return physicalCardActive;
	}
	public void setPhysicalCardActive(Long physicalCardActive) {
		this.physicalCardActive = physicalCardActive;
	}
	public Long getTotalVirtualCard() {
		return totalVirtualCard;
	}
	public void setTotalVirtualCard(Long totalVirtualCard) {
		this.totalVirtualCard = totalVirtualCard;
	}
	public Long getNewUser() {
		return newUser;
	}
	public void setNewUser(Long newUser) {
		this.newUser = newUser;
	}
	
	

}
