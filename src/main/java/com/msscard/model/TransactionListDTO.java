package com.msscard.model;

public class TransactionListDTO {

	private String username;
	private String contactNo;
	private String transactionRefNo;
	private	String authRefNo; 
	private String status;
	private String created;
	private String amount;
	private String error;
	private String retrivalRefNo;
	private String cardLoadStatus;
	
	private String commission;
	private String upiStatus;
	private String description;
	private String mdexTransactions;
	private String transactionType;
	private String merchantName;
	
	
	
	
	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	public String getMdexTransactions() {
		return mdexTransactions;
	}
	public void setMdexTransactions(String mdexTransactions) {
		this.mdexTransactions = mdexTransactions;
	}
	public String getUpiStatus() {
		return upiStatus;
	}
	public void setUpiStatus(String upiStatus) {
		this.upiStatus = upiStatus;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCommission() {
		return commission;
	}
	public void setCommission(String commission) {
		this.commission = commission;
	}
	public String getCardLoadStatus() {
		return cardLoadStatus;
	}
	public void setCardLoadStatus(String cardLoadStatus) {
		this.cardLoadStatus = cardLoadStatus;
	}
	public String getRetrivalRefNo() {
		return retrivalRefNo;
	}
	public void setRetrivalRefNo(String retrivalRefNo) {
		this.retrivalRefNo = retrivalRefNo;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getAuthRefNo() {
		return authRefNo;
	}
	public void setAuthRefNo(String authRefNo) {
		this.authRefNo = authRefNo;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	public String getTransactionRefNo() {
		return transactionRefNo;
	}
	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	
	
}
