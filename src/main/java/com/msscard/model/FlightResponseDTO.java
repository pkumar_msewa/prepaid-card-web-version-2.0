package com.msscard.model;

public class FlightResponseDTO {

	private String code;
	private String status;
	private String message;
	private Object details;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getDetails() {
		return details;
	}
	public void setDetails(Object details) {
		this.details = details;
	}
	@Override
	public String toString() {
		return "{code=" + code + ", status=" + status + ", message=" + message + ", details="
				+ details + "}";
	}
}
