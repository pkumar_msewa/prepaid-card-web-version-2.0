package com.msscard.model;

import java.io.StringWriter;
import java.net.Inet4Address;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.msscard.entity.ERCredentialsManager;
import com.msscard.repositories.ERCredentialManager;
import com.msscard.util.MatchMoveUtil;
import com.razorpay.constants.MdexConstants;

public class CreateJsonRequestFlight {
	

	public static JSONObject createjson(FlightBookRequest req, String source, HttpSession session) {
		try {
			
			
			JSONObject payload = new JSONObject();
			JSONObject obj = new JSONObject(source);
			JSONArray legsjsonarray = new JSONArray();
			JSONArray journeys = obj.getJSONObject("details").getJSONArray("journeys");
			List<FlightBookRequest>paxFareList= null;
			JSONArray segments = journeys.getJSONObject(0).getJSONArray("segments");
			for (int j = 0; j < segments.length(); j++) {

				JSONArray bonds = segments.getJSONObject(j).getJSONArray("bonds");
				for (int k = 0; k < bonds.length(); k++) {

					JSONArray legs = bonds.getJSONObject(k).getJSONArray("legs");
					legsjsonarray = bonds.getJSONObject(k).getJSONArray("legs");
					String flightnumber = legs.getJSONObject(k).getString("flightNumber");

					req.setAirlineName("" + legs.getJSONObject(k).getString("airlineName"));
					req.setArrivalTime("" + legs.getJSONObject(k).getString("arrivalTime"));
					req.setCabin("" + legs.getJSONObject(k).getString("cabin"));
					req.setDepartureTime("" + legs.getJSONObject(k).getString("departureTime"));
					req.setDestination("" + legs.getJSONObject(k).getString("destination"));
					req.setDuration("" + legs.getJSONObject(k).getString("duration"));
					req.setBoundType("" + legs.getJSONObject(k).getString("boundType"));
					req.setFlightNumber("" + legs.getJSONObject(k).getString("flightNumber"));
					req.setOrigin("" + legs.getJSONObject(k).getString("origin"));

					req.setFareBasisCode("" + legs.getJSONObject(k).getString("fareBasisCode"));// fareClassOfService//fareBasisCode
					req.setBeginDate("" + session.getAttribute("beginDate"));
					req.setFlightName("" + legs.getJSONObject(k).getString("airlineName"));
					req.setArrivalDate("" + legs.getJSONObject(k).getString("arrivalDate"));
					req.setDepartureDate("" + legs.getJSONObject(k).getString("departureDate"));
					req.setDepartureTerminal("" + legs.getJSONObject(k).getString("departureTerminal"));
					req.setArrivalTerminal("" + legs.getJSONObject(k).getString("arrivalTerminal"));
					req.setCapacity("" + legs.getJSONObject(k).getString("capacity"));
					req.setCarrierCode("" + legs.getJSONObject(k).getString("carrierCode"));
					req.setCurrencyCode("" + legs.getJSONObject(k).getString("currencyCode"));
					req.setBaggageUnit("" + legs.getJSONObject(k).getString("baggageUnit"));
					req.setBaggageWeight("" + legs.getJSONObject(k).getString("baggageWeight"));
					req.setFareClassOfService("" + legs.getJSONObject(k).getString("fareClassOfService"));
					req.setSold("" + legs.getJSONObject(k).getString("sold"));
					req.setStatus("" + legs.getJSONObject(k).getString("status"));
					req.setItineraryKey("" + segments.getJSONObject(j).getString("itineraryKey"));
					req.setSearchId("" + segments.getJSONObject(j).getString("searchId"));
					req.setEngineID("" + segments.getJSONObject(j).getString("engineID"));
					req.setFareRule("" + segments.getJSONObject(j).getString("fareRule"));
					req.setJourneyTime("" + legs.getJSONObject(k).getString("duration"));
					req.setJourneyIndex("" + segments.getJSONObject(j).getString("journeyIndex"));
					
					JSONArray fare = segments.getJSONObject(j).getJSONArray("fare");
					for (int l = 0; l < fare.length(); l++) {
						JSONArray paxFares = fare.getJSONObject(l).getJSONArray("paxFares");
						JSONArray fares = paxFares.getJSONObject(l).getJSONArray("fares");
						req.setTraceId("AYTM00011111111110002");
						req.setTotalFareWithOutMarkUp("" + fare.getJSONObject(l).getString("totalFareWithOutMarkUp"));
						req.setTotalTaxWithOutMarkUp("" + fare.getJSONObject(l).getString("totalTaxWithOutMarkUp"));
						req.setAmount("" + fares.getJSONObject(l).getString("amount"));
						req.setBasicFare("" + fare.getJSONObject(l).getString("basicFare"));
						req.setExchangeRate("" + fare.getJSONObject(l).getString("exchangeRate"));
						paxFareList= getPaxFareList(paxFares);
					}

				}

			}
			JSONArray fare = segments.getJSONObject(0).getJSONArray("fare");
			JSONArray paxFares = fare.getJSONObject(0).getJSONArray("paxFares");
//			JSONArray fares = paxFares.getJSONObject(0).getJSONArray("fares");

			JSONObject bondsjsonobjet = new JSONObject();
			JSONArray bondsjsonarray = new JSONArray();

			JSONObject bookSegmentsjsonobjet = new JSONObject();
			JSONArray bookSegmentsjsonarray = new JSONArray();

			JSONArray adultTravellers = CreateJsonRequestFlight.createjsonAdultraveler(req, source);
			JSONArray childTravellers = CreateJsonRequestFlight.createjsonChildraveler(req, source);
			JSONArray infantTravellers = CreateJsonRequestFlight.createjsonInfantrevaleer(req, source);
			JSONObject adultTravellersnew = new JSONObject();
			JSONArray flightSearchDetailsaray = new JSONArray();
			JSONObject flightSearchDetailsjsonobject = new JSONObject();
			JSONObject paymentDetails = new JSONObject();
			ArrayList<String> objhasmap = new ArrayList<>();
			System.err.println(adultTravellers);

			JSONObject faresjsonobjet = new JSONObject();
			JSONArray paxFaresjsonarray = null;
			paxFaresjsonarray= getPaxFaresJsonArray(paxFareList,paxFares);
			
			faresjsonobjet.put("basicFare", req.getBasicFare());
			faresjsonobjet.put("exchangeRate", 0);

			faresjsonobjet.put("totalFareWithOutMarkUp", req.getTotalFareWithOutMarkUp());
			faresjsonobjet.put("totalTaxWithOutMarkUp", req.getTotalTaxWithOutMarkUp());
			faresjsonobjet.put("paxFares", paxFaresjsonarray);

			objhasmap.add("" + req.getEngineID());
			payload.put("clientIp", Inet4Address.getLocalHost().getHostAddress());
			payload.put("cllientKey", MdexConstants.getMdexCredentials().getCode());
			payload.put("clientToken", MdexConstants.getMdexCredentials().getStatus());
			payload.put("clientApiName", "Flight Booking");
			payload.put("spKey", req.getAirlineName() + "IN");

			bondsjsonobjet.put("boundType", "OutBound");
			bondsjsonobjet.put("baggageFare", false);
			bondsjsonobjet.put("ssrFare", false);
			bondsjsonobjet.put("itineraryKey", req.getItineraryKey());
			bondsjsonobjet.put("journeyTime", req.getDuration());

			bondsjsonobjet.put("addOnDetail", "");

			JSONArray val = CreateJsonRequestFlight.createlegsarray(legsjsonarray);
			bondsjsonobjet.put("legs", val);
			// bondsjsonobjet.put("legs", legsjsonarray);

			bondsjsonarray.put(bondsjsonobjet);
			
			bookSegmentsjsonobjet.put("bondType","OutBound");
			bookSegmentsjsonobjet.put("bonds", bondsjsonarray);
			bookSegmentsjsonobjet.put("deeplink", "");
			bookSegmentsjsonobjet.put("fareIndicator", 0);
			bookSegmentsjsonobjet.put("fareRule", req.getFareRule());
			bookSegmentsjsonobjet.put("baggageFare", false);
			bookSegmentsjsonobjet.put("cache", false);
			bookSegmentsjsonobjet.put("holdBooking", false);
			bookSegmentsjsonobjet.put("international", false);
			bookSegmentsjsonobjet.put("roundTrip", false);
			bookSegmentsjsonobjet.put("special", false);
			bookSegmentsjsonobjet.put("specialId", false);
			bookSegmentsjsonobjet.put("itineraryKey", req.getItineraryKey());
			bookSegmentsjsonobjet.put("journeyIndex", 0);
			bookSegmentsjsonobjet.put("memoryCreationTime", "/Date(1499158249483+0530)/");
			bookSegmentsjsonobjet.put("nearByAirport", false);
			bookSegmentsjsonobjet.put("remark", "");
			bookSegmentsjsonobjet.put("searchId", req.getSearchId());

			bookSegmentsjsonobjet.put("engineID", req.getEngineID());
			bookSegmentsjsonobjet.put("fares", faresjsonobjet);

			bookSegmentsjsonarray.put(bookSegmentsjsonobjet);
			payload.put("bookSegments", bookSegmentsjsonarray);

			flightSearchDetailsjsonobject.put("beginDate", req.getBeginDate());
			flightSearchDetailsjsonobject.put("destination", "" + session.getAttribute("destination"));

			flightSearchDetailsjsonobject.put("endDate", req.getBeginDate());
			
			flightSearchDetailsjsonobject.put("origin", req.getOrigin());
			flightSearchDetailsaray.put(flightSearchDetailsjsonobject);
			payload.put("flightSearchDetails", flightSearchDetailsaray);

			paymentDetails.put("bookingAmount", req.getTotalFareWithOutMarkUp());
			paymentDetails.put("bookingCurrencyCode", "INR");
			payload.put("paymentDetails", paymentDetails);

			payload.put("engineIDList", objhasmap);

			adultTravellersnew.put("adultTravellers", adultTravellers);
			adultTravellersnew.put("childTravellers", childTravellers);
			adultTravellersnew.put("infantTravellers", infantTravellers);

			System.out.println(adultTravellersnew);
			payload.put("travellers", adultTravellersnew);
			payload.put("visatype", "Employee Visa");
			payload.put("traceId", "AYTM00011111111110001");
			payload.put("transactionId", req.getTransactionId());
			payload.put("androidBooking", false);
			payload.put("domestic", true);
			payload.put("engineID", req.getEngineID());
			System.out.println("booking request payload to EMT or MDEX--"+payload);
			return payload;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private static List<FlightBookRequest> getPaxFareList(JSONArray paxFares) {
		List<FlightBookRequest>paxFareList= new ArrayList<>();
		try {
			for(int fareVal=0;fareVal<paxFares.length(); fareVal++){
				FlightBookRequest paxFareReq= new FlightBookRequest();
				paxFareReq.setTotalFare("" + paxFares.getJSONObject(fareVal).getString("totalFare"));
				paxFareReq.setTotalTax("" + paxFares.getJSONObject(fareVal).getString("totalTax"));
				paxFareReq.setPaxBasicFare("" + paxFares.getJSONObject(fareVal).getString("basicFare"));
				paxFareReq.setCancelPenalty("" + paxFares.getJSONObject(fareVal).getString("cancelPenalty"));
				paxFareReq.setChangePenalty("" + paxFares.getJSONObject(fareVal).getString("changePenalty"));
				paxFareReq.setTransactionAmount("" + paxFares.getJSONObject(fareVal).getString("transactionAmount"));
				paxFareReq.setBaseTransactionAmount("" + paxFares.getJSONObject(fareVal).getString("baseTransactionAmount"));
				paxFareReq.setMarkUP("" + paxFares.getJSONObject(fareVal).getString("markUP"));
				paxFareReq.setPaxType("" + paxFares.getJSONObject(fareVal).getString("paxType"));
				paxFareList.add(paxFareReq);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return paxFareList;
	}

	private static JSONArray getPaxFaresJsonArray(List<FlightBookRequest> paxFareList, JSONArray paxFares) {
		JSONArray paxFaresjsonarray = new JSONArray();
		int index=0;
		try {
			for(FlightBookRequest paxReq: paxFareList){
				JSONObject paxFaresjsonobjet = new JSONObject();
				paxFaresjsonobjet.put("baggageUnit", paxReq.getBaggageUnit());
				paxFaresjsonobjet.put("baggageWeight", paxReq.getBaggageWeight());
				paxFaresjsonobjet.put("baseTransactionAmount", 0);
				paxFaresjsonobjet.put("basicFare", paxReq.getPaxBasicFare());
				paxFaresjsonobjet.put("cancelPenalty", paxReq.getCancelPenalty());
				paxFaresjsonobjet.put("changePenalty", paxReq.getChangePenalty());
				paxFaresjsonobjet.put("equivCurrencyCode", "");
				paxFaresjsonobjet.put("fareBasisCode", "");
				paxFaresjsonobjet.put("fareInfoKey", "");
				paxFaresjsonobjet.put("fareInfoValue", "");
				paxFaresjsonobjet.put("markUP", 0);
				paxFaresjsonobjet.put("paxType", paxReq.getPaxType());
				paxFaresjsonobjet.put("refundable", true);
				paxFaresjsonobjet.put("totalFare", paxReq.getTotalFare());
				paxFaresjsonobjet.put("totalTax", paxReq.getTotalTax());
				paxFaresjsonobjet.put("transactionAmount", 0);
				paxFaresjsonobjet.put("bookFares", paxFares.getJSONObject(index).getJSONArray("fares"));					
				paxFaresjsonarray.put(paxFaresjsonobjet);
				index++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return paxFaresjsonarray;
	}

	@SuppressWarnings("unchecked")
	public static JSONObject createjsonreturn(ReturnFlightBookRequest req, String source, String destination,
			HttpSession session) {
		try {
			JSONObject payload = new JSONObject();

			JSONArray bookSegmentsjsonarray = new JSONArray();

			org.json.simple.JSONArray flightSearchDetailsaray = new org.json.simple.JSONArray();
			JSONObject flightSearchDetailsjsonobject = new JSONObject();
			JSONObject paymentDetails = new JSONObject();
			ArrayList<String> objhasmap = new ArrayList<>();

			ArrayList<String> objhasmap1 = new ArrayList<>();

			String Firstfligt = "" + session.getAttribute("Firstfligt");
			String Secondflight = "" + session.getAttribute("Secondflight");
			if (Firstfligt.equals("Firstfligt")) {
				JSONArray legsjsonarray = new JSONArray();

				JSONObject bondsjsonobjet = new JSONObject();
				JSONArray bondsjsonarray = new JSONArray();

				JSONObject bookSegmentsjsonobjet = new JSONObject();
				JSONObject obj = new JSONObject(source);

				JSONArray journeys = obj.getJSONObject("details").getJSONArray("journeys");
				JSONObject faresjsonobjet = new JSONObject();
				JSONObject paxFaresjsonobjet = new JSONObject();
				JSONArray paxFaresjsonarray = new JSONArray();
				List<FlightBookRequest> paxFairList=null;
				JSONArray segments = journeys.getJSONObject(0).getJSONArray("segments");
				for (int j = 0; j < segments.length(); j++) {

					JSONArray bonds = segments.getJSONObject(j).getJSONArray("bonds");
					for (int k = 0; k < bonds.length(); k++) {

						JSONArray legs = bonds.getJSONObject(k).getJSONArray("legs");
						String flightnumber = legs.getJSONObject(k).getString("flightNumber");
						legsjsonarray = bonds.getJSONObject(k).getJSONArray("legs");
						System.out.println("#######################################");
						req.setAirlineName("" + legs.getJSONObject(k).getString("airlineName"));
						req.setArrivalTime("" + legs.getJSONObject(k).getString("arrivalTime"));
						req.setCabin("" + legs.getJSONObject(k).getString("cabin"));
						req.setDepartureTime("" + legs.getJSONObject(k).getString("departureTime"));
						req.setDestination("" + legs.getJSONObject(k).getString("destination"));
						req.setDuration("" + legs.getJSONObject(k).getString("duration"));
						req.setBoundType("" + legs.getJSONObject(k).getString("boundType"));
						req.setFlightNumber("" + legs.getJSONObject(k).getString("flightNumber"));
						req.setOrigin("" + legs.getJSONObject(k).getString("origin"));
						req.setAdults("" + session.getAttribute("adults"));
						req.setChilds("" + session.getAttribute("childs"));
						req.setInfants("" + session.getAttribute("infants"));
						req.setEndDate("" + session.getAttribute("endDate"));
						req.setFareBasisCode("" + legs.getJSONObject(k).getString("fareBasisCode"));// fareClassOfService//fareBasisCode
						req.setBeginDate("" + session.getAttribute("beginDate"));
						req.setFlightName("" + legs.getJSONObject(k).getString("airlineName"));
						req.setArrivalDate("" + legs.getJSONObject(k).getString("arrivalDate"));
						req.setDepartureDate("" + legs.getJSONObject(k).getString("departureDate"));
						req.setDepartureTerminal("" + legs.getJSONObject(k).getString("departureTerminal"));
						req.setArrivalTerminal("" + legs.getJSONObject(k).getString("arrivalTerminal"));
						req.setCapacity("" + legs.getJSONObject(k).getString("capacity"));
						req.setCarrierCode("" + legs.getJSONObject(k).getString("carrierCode"));
						req.setCurrencyCode("" + legs.getJSONObject(k).getString("currencyCode"));
						req.setBaggageUnit("" + legs.getJSONObject(k).getString("baggageUnit"));
						req.setBaggageWeight("" + legs.getJSONObject(k).getString("baggageWeight"));
						req.setFareClassOfService("" + legs.getJSONObject(k).getString("fareClassOfService"));
						req.setSold("" + legs.getJSONObject(k).getString("sold"));
						req.setStatus("" + legs.getJSONObject(k).getString("status"));
						req.setItineraryKey("" + segments.getJSONObject(j).getString("itineraryKey"));
						req.setSearchId("" + segments.getJSONObject(j).getString("searchId"));
						req.setEngineID("" + segments.getJSONObject(j).getString("engineID"));
						req.setFareRule("" + segments.getJSONObject(j).getString("fareRule"));
						req.setJourneyTime("" + legs.getJSONObject(k).getString("duration"));
						req.setJourneyIndex("" + segments.getJSONObject(j).getString("journeyIndex"));

						JSONArray fare = segments.getJSONObject(j).getJSONArray("fare");
						for (int l = 0; l < fare.length(); l++) {
							JSONArray paxFares = fare.getJSONObject(l).getJSONArray("paxFares");
							JSONArray fares = paxFares.getJSONObject(l).getJSONArray("fares");
							req.setExchangeRate("" + fare.getJSONObject(l).getString("exchangeRate"));
							req.setAmount("" + fares.getJSONObject(l).getString("amount"));
							req.setTotalFareWithOutMarkUp(
									"" + fare.getJSONObject(l).getString("totalFareWithOutMarkUp"));
							req.setTotalTaxWithOutMarkUp("" + fare.getJSONObject(l).getString("totalTaxWithOutMarkUp"));
							req.setTraceId("AYTM00011111111110002");
							paxFairList=getPaxFareList(paxFares);
						}

					}

				}

				JSONArray fare = segments.getJSONObject(0).getJSONArray("fare");
				JSONArray paxFares = fare.getJSONObject(0).getJSONArray("paxFares");
				
				paxFaresjsonarray= getPaxFaresJsonArray(paxFairList, paxFares);
				faresjsonobjet.put("basicFare", fare.getJSONObject(0).getString("basicFare"));
				faresjsonobjet.put("exchangeRate", 0);

				faresjsonobjet.put("totalFareWithOutMarkUp", req.getTotalFareWithOutMarkUp());
				faresjsonobjet.put("totalTaxWithOutMarkUp", req.getTotalTaxWithOutMarkUp());
				faresjsonobjet.put("paxFares", paxFaresjsonarray);

				objhasmap.add("" + req.getEngineID());
				payload.put("clientIp", Inet4Address.getLocalHost().getHostAddress());
				payload.put("cllientKey", MdexConstants.getMdexCredentials().getCode());
				payload.put("clientToken", MdexConstants.getMdexCredentials().getStatus());
				payload.put("clientApiName", "Flight Booking");
				payload.put("spKey", req.getAirlineName() + "IN");

				bondsjsonobjet.put("boundType", "OutBound");
				bondsjsonobjet.put("baggageFare", false);
				bondsjsonobjet.put("ssrFare", false);
				bondsjsonobjet.put("itineraryKey", req.getItineraryKey());
				bondsjsonobjet.put("journeyTime", req.getDuration());

				bondsjsonobjet.put("addOnDetail", "");

				JSONArray val = CreateJsonRequestFlight.createlegsarray(legsjsonarray);
				bondsjsonobjet.put("legs", val);

				bondsjsonarray.put(bondsjsonobjet);

				bookSegmentsjsonobjet.put("bondType", "OutBound");
				bookSegmentsjsonobjet.put("bonds", bondsjsonarray);
				bookSegmentsjsonobjet.put("deeplink", "");
				bookSegmentsjsonobjet.put("fareIndicator", 0);
				bookSegmentsjsonobjet.put("fareRule", req.getFareRule());
				bookSegmentsjsonobjet.put("baggageFare", false);
				bookSegmentsjsonobjet.put("cache", false);
				bookSegmentsjsonobjet.put("holdBooking", false);
				bookSegmentsjsonobjet.put("international", false);
				bookSegmentsjsonobjet.put("roundTrip", false);
				bookSegmentsjsonobjet.put("special", false);
				bookSegmentsjsonobjet.put("specialId", false);
				bookSegmentsjsonobjet.put("itineraryKey", req.getItineraryKey());
				bookSegmentsjsonobjet.put("journeyIndex", 0);
				bookSegmentsjsonobjet.put("memoryCreationTime", "/Date(1499158249483+0530)/");
				bookSegmentsjsonobjet.put("nearByAirport", false);
				bookSegmentsjsonobjet.put("remark", "");
				bookSegmentsjsonobjet.put("searchId", req.getSearchId());

				bookSegmentsjsonobjet.put("engineID", req.getEngineID());
				bookSegmentsjsonobjet.put("fares", faresjsonobjet);

				bookSegmentsjsonarray.put(bookSegmentsjsonobjet);
			}
			if (Secondflight.equals("Secondflight")) {
				JSONObject faresjsonobjet = new JSONObject();
				JSONObject paxFaresjsonobjet = new JSONObject();
				JSONArray paxFaresjsonarray = new JSONArray();
				JSONObject bookSegmentsjsonobjet = new JSONObject();
				JSONObject obj = new JSONObject(destination);
				JSONObject legsjsonobjet = new JSONObject();
				JSONArray legsjsonarray = new JSONArray();

				JSONObject bondsjsonobjet = new JSONObject();
				JSONArray bondsjsonarray = new JSONArray();

				JSONObject ssrDetailsjsonobjet = new JSONObject();
				JSONArray ssrDetailsjsonarray = new JSONArray();

				JSONArray engineIDList = new JSONArray();

				JSONArray journeys = obj.getJSONObject("details").getJSONArray("journeys");

				JSONArray segments = journeys.getJSONObject(0).getJSONArray("segments");
				
				List<FlightBookRequest> paxFairList2=null;
				for (int j = 0; j < segments.length(); j++) {

					JSONArray bonds = segments.getJSONObject(j).getJSONArray("bonds");
					for (int k = 0; k < bonds.length(); k++) {

						JSONArray legs = bonds.getJSONObject(k).getJSONArray("legs");
						String flightnumber = legs.getJSONObject(k).getString("flightNumber");
						legsjsonarray = bonds.getJSONObject(k).getJSONArray("legs");
						System.out.println("#######################################");
						req.setAirlineNamereturn("" + legs.getJSONObject(k).getString("airlineName"));
						req.setArrivalTimereturn("" + legs.getJSONObject(k).getString("arrivalTime"));
						req.setCabinreturn("" + legs.getJSONObject(k).getString("cabin"));
						req.setDepartureTimereturn("" + legs.getJSONObject(k).getString("departureTime"));
						req.setDestinationreturn("" + legs.getJSONObject(k).getString("destination"));
						req.setDurationreturn("" + legs.getJSONObject(k).getString("duration"));
						req.setBoundTypereturn("" + legs.getJSONObject(k).getString("boundType"));
						req.setFlightNumberreturn("" + legs.getJSONObject(k).getString("flightNumber"));
						// req.setOriginreturn("" +
						// legs.getJSONObject(k).getString("origin"));

						req.setFareBasisCodereturn("" + legs.getJSONObject(k).getString("fareBasisCode"));// fareClassOfService//fareBasisCode
						// req.setBeginDatereturn("" +
						// session.getAttribute("beginDate"));
						req.setFlightNamereturn("" + legs.getJSONObject(k).getString("airlineName"));
						req.setArrivalDatereturn("" + legs.getJSONObject(k).getString("arrivalDate"));
						req.setDepartureDatereturn("" + legs.getJSONObject(k).getString("departureDate"));
						req.setDepartureTerminalreturn("" + legs.getJSONObject(k).getString("departureTerminal"));
						req.setArrivalTerminalreturn("" + legs.getJSONObject(k).getString("arrivalTerminal"));
						req.setCapacityreturn("" + legs.getJSONObject(k).getString("capacity"));
						req.setCarrierCodereturn("" + legs.getJSONObject(k).getString("carrierCode"));
						req.setCurrencyCodereturn("" + legs.getJSONObject(k).getString("currencyCode"));
						req.setBaggageUnitreturn("" + legs.getJSONObject(k).getString("baggageUnit"));
						req.setBaggageWeightreturn("" + legs.getJSONObject(k).getString("baggageWeight"));
						req.setFareClassOfServicereturn("" + legs.getJSONObject(k).getString("fareClassOfService"));
						req.setSoldreturn("" + legs.getJSONObject(k).getString("sold"));
						req.setStatusreturn("" + legs.getJSONObject(k).getString("status"));
						req.setItineraryKeyreturn("" + segments.getJSONObject(j).getString("itineraryKey"));
						req.setSearchIdreturn("" + segments.getJSONObject(j).getString("searchId"));
						req.setEngineIDreturn("" + segments.getJSONObject(j).getString("engineID"));
						req.setFareRulereturn("" + segments.getJSONObject(j).getString("fareRule"));
						req.setJourneyTimereturn("" + legs.getJSONObject(k).getString("duration"));
						req.setJourneyIndexreturn("" + segments.getJSONObject(j).getString("journeyIndex"));

						JSONArray fare = segments.getJSONObject(j).getJSONArray("fare");
						for (int l = 0; l < fare.length(); l++) {
							JSONArray paxFares = fare.getJSONObject(l).getJSONArray("paxFares");
							JSONArray fares = paxFares.getJSONObject(l).getJSONArray("fares");

							req.setTotalFareWithOutMarkUpreturn(
									"" + fare.getJSONObject(l).getString("totalFareWithOutMarkUp"));
							req.setTotalTaxWithOutMarkUpreturn(
									"" + fare.getJSONObject(l).getString("totalTaxWithOutMarkUp"));
							req.setAmountreturn("" + fares.getJSONObject(l).getString("amount"));

							req.setBasicFarereturn("" + fare.getJSONObject(l).getString("basicFare"));
							req.setTraceIdreturn("AYTM00011111111110002");

							req.setExchangeRatereturn("" + fare.getJSONObject(l).getString("exchangeRate"));

							paxFairList2=getPaxFareList(paxFares);
						}

					}

				}
				JSONArray fare = segments.getJSONObject(0).getJSONArray("fare");
				JSONArray paxFares = fare.getJSONObject(0).getJSONArray("paxFares");
				paxFaresjsonarray= getPaxFaresJsonArray(paxFairList2, paxFares);
				faresjsonobjet.put("basicFare", fare.getJSONObject(0).getString("basicFare"));
				faresjsonobjet.put("exchangeRate", 0);

				faresjsonobjet.put("totalFareWithOutMarkUp", req.getTotalFareWithOutMarkUpreturn());
				faresjsonobjet.put("totalTaxWithOutMarkUp", req.getTotalTaxWithOutMarkUpreturn());
				faresjsonobjet.put("paxFares", paxFaresjsonarray);

				payload.put("spKey", req.getAirlineName() + "IN");

				bondsjsonobjet.put("boundType", "InBound");
				bondsjsonobjet.put("baggageFare", false);
				bondsjsonobjet.put("ssrFare", false);
				bondsjsonobjet.put("itineraryKey", req.getItineraryKeyreturn());
				bondsjsonobjet.put("journeyTime", req.getDurationreturn());

				bondsjsonobjet.put("addOnDetail", "");
				objhasmap1.add(req.getEngineIDreturn());

				JSONArray val = CreateJsonRequestFlight.createlegsarray(legsjsonarray);
				bondsjsonobjet.put("legs", val);

				bondsjsonarray.put(bondsjsonobjet);

				bookSegmentsjsonobjet.put("bondType", "InBound");
				bookSegmentsjsonobjet.put("bonds", bondsjsonarray);
				bookSegmentsjsonobjet.put("deeplink", "");
				bookSegmentsjsonobjet.put("fareIndicator", 0);
				bookSegmentsjsonobjet.put("fareRule", req.getFareRule());
				bookSegmentsjsonobjet.put("baggageFare", false);
				bookSegmentsjsonobjet.put("cache", false);
				bookSegmentsjsonobjet.put("holdBooking", false);
				bookSegmentsjsonobjet.put("international", false);
				bookSegmentsjsonobjet.put("roundTrip", false);
				bookSegmentsjsonobjet.put("special", false);
				bookSegmentsjsonobjet.put("specialId", false);

				bookSegmentsjsonobjet.put("itineraryKey", req.getItineraryKey());

				bookSegmentsjsonobjet.put("journeyIndex", 0);
				bookSegmentsjsonobjet.put("memoryCreationTime", "/Date(1499158249483+0530)/");
				bookSegmentsjsonobjet.put("nearByAirport", false);
				bookSegmentsjsonobjet.put("remark", "");
				bookSegmentsjsonobjet.put("searchId", req.getSearchIdreturn());

				bookSegmentsjsonobjet.put("engineID", req.getEngineID());
				bookSegmentsjsonobjet.put("fares", faresjsonobjet);

				bookSegmentsjsonarray.put(bookSegmentsjsonobjet);
			}

			payload.put("bookSegments", bookSegmentsjsonarray);

			flightSearchDetailsjsonobject.put("beginDate", req.getBeginDate());
			flightSearchDetailsjsonobject.put("destination", "" + session.getAttribute("destination"));

			flightSearchDetailsjsonobject.put("endDate", req.getBeginDate());
			
			flightSearchDetailsjsonobject.put("origin", req.getOrigin());
			flightSearchDetailsaray.add(flightSearchDetailsjsonobject);
			JSONObject roundtripDetailsobj = new JSONObject();

			roundtripDetailsobj.put("origin", "" + session.getAttribute("destination"));
			roundtripDetailsobj.put("destination", req.getOrigin());
			roundtripDetailsobj.put("beginDate", req.getEndDate());
			roundtripDetailsobj.put("endDate", req.getEndDate());
			
			flightSearchDetailsaray.add(roundtripDetailsobj);

			payload.put("flightSearchDetails", flightSearchDetailsaray);

			paymentDetails.put("bookingAmount", req.getGrandtotal());
			paymentDetails.put("bookingCurrencyCode", "INR");
			payload.put("paymentDetails", paymentDetails);

			payload.put("engineIDList", objhasmap1);

			JSONObject adultTravellersnew = new JSONObject();

			JSONArray adultTravellers = CreateJsonRequestFlight.createReturnjsonAdultraveler(req, source);
			JSONArray childTravellers =CreateJsonRequestFlight.createReturnjsonChildraveler(req, source); 
			JSONArray infantTravellers = CreateJsonRequestFlight.createReturnjsonInfantrevaleer(req, source);

			adultTravellersnew.put("adultTravellers", adultTravellers);
			adultTravellersnew.put("childTravellers", childTravellers);
			adultTravellersnew.put("infantTravellers", infantTravellers);

			payload.put("travellers", adultTravellersnew);
			payload.put("visatype", "Employee Visa");
			payload.put("traceId", "AYTM00011111111110001");
			payload.put("transactionId", req.getTransactionId());
			payload.put("androidBooking", false);
			payload.put("domestic", true);
			payload.put("engineID", req.getEngineIDreturn());
			return payload;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static JSONObject createjsonForMobile(MobileFlightBookRequest req) {
		try {

			JSONObject payload = new JSONObject();

			JSONObject legsjsonobjet = new JSONObject();
			JSONArray legsjsonarray = new JSONArray();
			org.json.simple.JSONArray bookfarejsonArray = CreateJsonRequestFlight.createBookFareJsonArray(req);

			JSONObject bondsjsonobjet = new JSONObject();
			JSONArray bondsjsonarray = new JSONArray();

			JSONObject ssrDetailsjsonobjet = new JSONObject();
			JSONArray ssrDetailsjsonarray = new JSONArray();

			JSONArray engineIDList = new JSONArray();

			JSONObject bookSegmentsjsonobjet = new JSONObject();
			JSONArray bookSegmentsjsonarray = new JSONArray();

			JSONObject adultTravellersnew = new JSONObject();
			JSONArray flightSearchDetailsaray = new JSONArray();
			JSONObject flightSearchDetailsjsonobject = new JSONObject();
			JSONObject paymentDetails = new JSONObject();
			ArrayList<String> objhasmap = new ArrayList<>();

			ArrayList<String> objhasmap1 = new ArrayList<>();

			JSONObject faresjsonobjet = new JSONObject();
			JSONObject paxFaresjsonobjet = new JSONObject();
			JSONArray paxFaresjsonarray = new JSONArray();

			paxFaresjsonobjet.put("baggageUnit", req.getBaggageUnit());
			paxFaresjsonobjet.put("baggageWeight", req.getBaggageWeight());
			paxFaresjsonobjet.put("baseTransactionAmount", 0);
			paxFaresjsonobjet.put("basicFare", req.getBasicFare());
			paxFaresjsonobjet.put("cancelPenalty", req.getCancelPenalty());
			paxFaresjsonobjet.put("changePenalty", req.getChangePenalty());
			paxFaresjsonobjet.put("equivCurrencyCode", "");

			paxFaresjsonobjet.put("fareBasisCode", "");
			paxFaresjsonobjet.put("fareInfoKey", "");
			paxFaresjsonobjet.put("fareInfoValue", "");
			paxFaresjsonobjet.put("markUP", 0);
			paxFaresjsonobjet.put("paxType", "ADT");
			paxFaresjsonobjet.put("refundable", true);
			paxFaresjsonobjet.put("totalFare", req.getTotalFare());
			paxFaresjsonobjet.put("totalTax", req.getTotalTax());
			paxFaresjsonobjet.put("transactionAmount", 0);
			paxFaresjsonobjet.put("bookFares", bookfarejsonArray);
			paxFaresjsonarray.put(paxFaresjsonobjet);

			faresjsonobjet.put("basicFare", req.getBasicFare());
			faresjsonobjet.put("exchangeRate", 0);

			faresjsonobjet.put("totalFareWithOutMarkUp", req.getTotalFareWithOutMarkUp());
			faresjsonobjet.put("totalTaxWithOutMarkUp", req.getTotalTaxWithOutMarkUp());
			faresjsonobjet.put("paxFares", paxFaresjsonarray);

			objhasmap.add("" + EngineID.Indigo);
			payload.put("clientIp", Inet4Address.getLocalHost().getHostAddress());
			payload.put("cllientKey", MdexConstants.getMdexCredentials().getCode());
			payload.put("clientToken", MdexConstants.getMdexCredentials().getStatus());
			payload.put("clientApiName", "Flight Booking");
			payload.put("spKey", req.getAirlineName() + "IN");

			bondsjsonobjet.put("boundType", "");
			bondsjsonobjet.put("baggageFare", false);
			bondsjsonobjet.put("ssrFare", false);
			bondsjsonobjet.put("itineraryKey", req.getItineraryKey());
			bondsjsonobjet.put("journeyTime", req.getDuration());

			bondsjsonobjet.put("addOnDetail", "");

			legsjsonobjet.put("aircraftCode", "");
			legsjsonobjet.put("aircraftType", "");
			legsjsonobjet.put("airlineName", req.getAirlineName());
			legsjsonobjet.put("amount", req.getAmount());
			legsjsonobjet.put("arrivalDate", req.getArrivalDate());
			legsjsonobjet.put("arrivalTerminal", "");
			legsjsonobjet.put("arrivalTime", req.getArrivalTime());
			legsjsonobjet.put("availableSeat", "");
			legsjsonobjet.put("baggageUnit", req.getBaggageUnit());
			legsjsonobjet.put("baggageWeight", req.getBaggageWeight());
			legsjsonobjet.put("boundTypes", "");
			legsjsonobjet.put("cabin", "Economy");
			legsjsonobjet.put("cabinClasses", objhasmap1);
			legsjsonobjet.put("capacity", "0");
			legsjsonobjet.put("carrierCode", req.getAirlineName());
			legsjsonobjet.put("currencyCode", "INR");
			legsjsonobjet.put("departureDate", req.getDepartureDate());
			legsjsonobjet.put("departureTerminal", req.getDepartureTerminal());
			legsjsonobjet.put("departureTime", req.getDepartureTime());
			legsjsonobjet.put("destination", req.getDestination());
			legsjsonobjet.put("duration", req.getDuration());
			legsjsonobjet.put("fareBasisCode", req.getFareBasisCode());
			legsjsonobjet.put("fareClassOfService", req.getFareClassOfService());
			legsjsonobjet.put("flightDesignator", "");
			legsjsonobjet.put("flightDetailRefKey", "");
			legsjsonobjet.put("flightName", "Indigo");
			legsjsonobjet.put("flightNumber", req.getFlightNumber());
			legsjsonobjet.put("group", "");
			legsjsonobjet.put("connecting", false);
			legsjsonobjet.put("numberOfStops", "");
			legsjsonobjet.put("origin", req.getOrigin());
			legsjsonobjet.put("providerCode", "");
			legsjsonobjet.put("remarks", "");
			ssrDetailsjsonarray.put(ssrDetailsjsonobjet);
			legsjsonobjet.put("sold", 0);
			legsjsonobjet.put("status", "Normal");
			legsjsonobjet.put("ssrDetails", ssrDetailsjsonarray);
			legsjsonarray.put(legsjsonobjet);

			bondsjsonobjet.put("legs", legsjsonarray);

			bondsjsonarray.put(bondsjsonobjet);

			bookSegmentsjsonobjet.put("bonds", bondsjsonarray);
			bookSegmentsjsonobjet.put("deeplink", "");
			bookSegmentsjsonobjet.put("fareIndicator", 0);
			bookSegmentsjsonobjet.put("fareRule", req.getFareRule());
			bookSegmentsjsonobjet.put("baggageFare", false);
			bookSegmentsjsonobjet.put("cache", false);
			bookSegmentsjsonobjet.put("holdBooking", false);
			bookSegmentsjsonobjet.put("international", false);
			bookSegmentsjsonobjet.put("roundTrip", false);
			bookSegmentsjsonobjet.put("special", false);
			bookSegmentsjsonobjet.put("specialId", false);
			bookSegmentsjsonobjet.put("itineraryKey", req.getItineraryKey());
			bookSegmentsjsonobjet.put("journeyIndex", 0);
			bookSegmentsjsonobjet.put("memoryCreationTime", "/Date(1499158249483+0530)/");
			bookSegmentsjsonobjet.put("nearByAirport", false);
			bookSegmentsjsonobjet.put("remark", "");
			bookSegmentsjsonobjet.put("searchId", req.getSearchId());

			bookSegmentsjsonobjet.put("engineID", "Indigo");
			bookSegmentsjsonobjet.put("fares", faresjsonobjet);

			bookSegmentsjsonarray.put(bookSegmentsjsonobjet);
			payload.put("bookSegments", bookSegmentsjsonarray);

			flightSearchDetailsjsonobject.put("beginDate", req.getBeginDate());
			flightSearchDetailsjsonobject.put("destination", req.getDestination());
			flightSearchDetailsjsonobject.put("endDate", req.getBeginDate());
			flightSearchDetailsjsonobject.put("origin", req.getOrigin());
			flightSearchDetailsaray.put(flightSearchDetailsjsonobject);
			payload.put("flightSearchDetails", flightSearchDetailsaray);

			paymentDetails.put("bookingAmount", req.getTotalFareWithOutMarkUp());
			paymentDetails.put("bookingCurrencyCode", "INR");
			payload.put("paymentDetails", paymentDetails);

			payload.put("engineIDList", objhasmap);
			JSONArray adultTravellers = CreateJsonRequestFlight.createMobilejsonAdultraveler(req, "");
			JSONArray childTravellers = CreateJsonRequestFlight.createMobilejsonChildraveler(req, "");
			JSONArray infantTravellers = CreateJsonRequestFlight.createMobilejsonInfantrevaleer(req, "");

			adultTravellersnew.put("adultTravellers", adultTravellers);
			adultTravellersnew.put("childTravellers", childTravellers);
			adultTravellersnew.put("infantTravellers", infantTravellers);

			payload.put("travellers", adultTravellersnew);
			payload.put("visatype", "Employee Visa");
			payload.put("traceId", "AYTM00011111111110001");
			payload.put("transactionId", req.getTransactionId());
			payload.put("androidBooking", true);
			payload.put("domestic", true);
			payload.put("engineID", objhasmap.get(0));
			return payload;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static org.json.simple.JSONArray createBookFareJsonArray(MobileFlightBookRequest req) {
		org.json.simple.JSONArray BookfaresArray = new org.json.simple.JSONArray();
		try {

			for (int i = 0; i < req.getBookFares().size(); i++) {
				JSONObject Bookfarejsonobejct = new JSONObject();
				Bookfarejsonobejct.put("chargeCode", req.getBookFares().get(i).getChargeCode());
				Bookfarejsonobejct.put("chargeType", req.getBookFares().get(i).getChargeType());
				Bookfarejsonobejct.put("amount", req.getBookFares().get(i).getAmount());
				BookfaresArray.add(Bookfarejsonobejct);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return BookfaresArray;

	}

	@SuppressWarnings("unchecked")
	public static org.json.simple.JSONArray createBookRoundFareJsonArray(MobileRoundwayFlightBookRequest req) {
		org.json.simple.JSONArray BookfaresArray = new org.json.simple.JSONArray();
		try {

			for (int i = 0; i < req.getBookFares().size(); i++) {
				JSONObject Bookfarejsonobejct = new JSONObject();

				Bookfarejsonobejct.put("chargeCode", req.getBookFares().get(i).getChargeCode());
				Bookfarejsonobejct.put("chargeType", req.getBookFares().get(i).getChargeType());
				Bookfarejsonobejct.put("amount", req.getBookFares().get(i).getAmount());
				BookfaresArray.add(Bookfarejsonobejct);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return BookfaresArray;

	}

	@SuppressWarnings("unchecked")
	public static org.json.simple.JSONArray createBookRoundFareJsonArray1(MobileRoundwayFlightBookRequest req) {
		org.json.simple.JSONArray BookfaresArray = new org.json.simple.JSONArray();
		try {

			for (int i = 0; i < req.getBookFaresreturn().size(); i++) {
				JSONObject Bookfarejsonobejct = new JSONObject();

				Bookfarejsonobejct.put("chargeCode", req.getBookFaresreturn().get(i).getChargeCode());
				Bookfarejsonobejct.put("chargeType", req.getBookFaresreturn().get(i).getChargeType());
				Bookfarejsonobejct.put("amount", req.getBookFaresreturn().get(i).getAmount());
				BookfaresArray.add(Bookfarejsonobejct);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return BookfaresArray;

	}

	public static JSONObject createjsonForReturnMobile(MobileRoundwayFlightBookRequest req) {
		try {

			JSONObject payload = new JSONObject();
			//
			JSONObject legsjsonobjet = new JSONObject();
			JSONArray legsjsonarray = new JSONArray();
			org.json.simple.JSONArray bookfarejsonArray = CreateJsonRequestFlight.createBookRoundFareJsonArray(req);

			JSONObject bondsjsonobjet = new JSONObject();
			JSONArray bondsjsonarray = new JSONArray();

			JSONObject ssrDetailsjsonobjet = new JSONObject();
			JSONArray ssrDetailsjsonarray = new JSONArray();

			JSONArray engineIDList = new JSONArray();
			org.json.simple.JSONArray bookfarejsonArrayreturn = CreateJsonRequestFlight
					.createBookRoundFareJsonArray1(req);
			JSONObject bookSegmentsjsonobjet = new JSONObject();
			JSONArray bookSegmentsjsonarray = new JSONArray();

			org.json.simple.JSONArray flightSearchDetailsaray = new org.json.simple.JSONArray();
			JSONObject flightSearchDetailsjsonobject = new JSONObject();
			JSONObject paymentDetails = new JSONObject();
			ArrayList<String> objhasmap = new ArrayList<>();
			//
			ArrayList<String> objhasmap1 = new ArrayList<>();
			//
			JSONObject faresjsonobjet = new JSONObject();
			JSONObject paxFaresjsonobjet = new JSONObject();
			JSONArray paxFaresjsonarray = new JSONArray();

			paxFaresjsonobjet.put("baggageUnit", req.getBaggageUnit());
			paxFaresjsonobjet.put("baggageWeight", req.getBaggageWeight());
			paxFaresjsonobjet.put("baseTransactionAmount", 0);
			paxFaresjsonobjet.put("basicFare", req.getBasicFare());
			paxFaresjsonobjet.put("cancelPenalty", req.getCancelPenalty());
			paxFaresjsonobjet.put("changePenalty", req.getChangePenalty());
			paxFaresjsonobjet.put("equivCurrencyCode", "");

			paxFaresjsonobjet.put("fareBasisCode", "");
			paxFaresjsonobjet.put("fareInfoKey", "");
			paxFaresjsonobjet.put("fareInfoValue", "");
			paxFaresjsonobjet.put("markUP", 0);
			paxFaresjsonobjet.put("paxType", "ADT");
			paxFaresjsonobjet.put("refundable", true);
			paxFaresjsonobjet.put("totalFare", req.getTotalFare());
			paxFaresjsonobjet.put("totalTax", req.getTotalTax());
			paxFaresjsonobjet.put("transactionAmount", 0);
			paxFaresjsonobjet.put("bookFares", bookfarejsonArray);
			paxFaresjsonarray.put(paxFaresjsonobjet);

			faresjsonobjet.put("basicFare", req.getBasicFare());
			faresjsonobjet.put("exchangeRate", 0);

			faresjsonobjet.put("totalFareWithOutMarkUp", req.getTotalFareWithOutMarkUp());
			faresjsonobjet.put("totalTaxWithOutMarkUp", req.getTotalTaxWithOutMarkUp());
			faresjsonobjet.put("paxFares", paxFaresjsonarray);

			objhasmap.add("" + EngineID.Indigo);
			payload.put("clientIp", Inet4Address.getLocalHost().getHostAddress());
			payload.put("cllientKey", MdexConstants.getMdexCredentials().getCode());
			payload.put("clientToken", MdexConstants.getMdexCredentials().getStatus());
			payload.put("clientApiName", "Flight Booking");
			payload.put("spKey", req.getAirlineName() + "IN");

			bondsjsonobjet.put("boundType", "");
			bondsjsonobjet.put("baggageFare", false);
			bondsjsonobjet.put("ssrFare", false);
			bondsjsonobjet.put("itineraryKey", req.getItineraryKey());
			bondsjsonobjet.put("journeyTime", req.getDuration());

			bondsjsonobjet.put("addOnDetail", "");

			legsjsonobjet.put("aircraftCode", "");
			legsjsonobjet.put("aircraftType", "");
			legsjsonobjet.put("airlineName", req.getAirlineName());
			legsjsonobjet.put("amount", req.getAmount());
			legsjsonobjet.put("arrivalDate", req.getArrivalDate());
			legsjsonobjet.put("arrivalTerminal", "");
			legsjsonobjet.put("arrivalTime", req.getArrivalTime());
			legsjsonobjet.put("availableSeat", "");
			legsjsonobjet.put("baggageUnit", req.getBaggageUnit());
			legsjsonobjet.put("baggageWeight", req.getBaggageWeight());
			legsjsonobjet.put("boundTypes", "");
			legsjsonobjet.put("cabin", "Economy");
			legsjsonobjet.put("cabinClasses", objhasmap1);
			legsjsonobjet.put("capacity", "0");
			legsjsonobjet.put("carrierCode", req.getAirlineName());
			legsjsonobjet.put("currencyCode", "INR");
			legsjsonobjet.put("departureDate", req.getDepartureDate());
			legsjsonobjet.put("departureTerminal", req.getDepartureTerminal());
			legsjsonobjet.put("departureTime", req.getDepartureTime());
			legsjsonobjet.put("destination", req.getDestination());
			legsjsonobjet.put("duration", req.getDuration());
			legsjsonobjet.put("fareBasisCode", req.getFareBasisCode());
			legsjsonobjet.put("fareClassOfService", req.getFareClassOfService());
			legsjsonobjet.put("flightDesignator", "");
			legsjsonobjet.put("flightDetailRefKey", "");
			legsjsonobjet.put("flightName", "Indigo");
			legsjsonobjet.put("flightNumber", req.getFlightNumber());
			legsjsonobjet.put("group", "");
			legsjsonobjet.put("connecting", false);
			legsjsonobjet.put("numberOfStops", "");
			legsjsonobjet.put("origin", req.getOrigin());
			legsjsonobjet.put("providerCode", "");
			legsjsonobjet.put("remarks", "");
			ssrDetailsjsonarray.put(ssrDetailsjsonobjet);
			legsjsonobjet.put("sold", 0);
			legsjsonobjet.put("status", "Normal");
			legsjsonobjet.put("ssrDetails", ssrDetailsjsonarray);
			legsjsonarray.put(legsjsonobjet);

			bondsjsonobjet.put("legs", legsjsonarray);

			bondsjsonarray.put(bondsjsonobjet);

			bookSegmentsjsonobjet.put("bonds", bondsjsonarray);
			bookSegmentsjsonobjet.put("deeplink", "");
			bookSegmentsjsonobjet.put("fareIndicator", 0);
			bookSegmentsjsonobjet.put("fareRule", req.getFareRule());
			bookSegmentsjsonobjet.put("baggageFare", false);
			bookSegmentsjsonobjet.put("cache", false);
			bookSegmentsjsonobjet.put("holdBooking", false);
			bookSegmentsjsonobjet.put("international", false);
			bookSegmentsjsonobjet.put("roundTrip", false);
			bookSegmentsjsonobjet.put("special", false);
			bookSegmentsjsonobjet.put("specialId", false);
			bookSegmentsjsonobjet.put("itineraryKey", req.getItineraryKey());
			bookSegmentsjsonobjet.put("journeyIndex", 0);
			bookSegmentsjsonobjet.put("memoryCreationTime", "/Date(1499158249483+0530)/");
			bookSegmentsjsonobjet.put("nearByAirport", false);
			bookSegmentsjsonobjet.put("remark", "");
			bookSegmentsjsonobjet.put("searchId", req.getSearchId());

			bookSegmentsjsonobjet.put("engineID", "Indigo");
			bookSegmentsjsonobjet.put("fares", faresjsonobjet);
			bookSegmentsjsonarray.put(bookSegmentsjsonobjet);

			JSONObject faresjsonobjetreturn = new JSONObject();
			JSONObject paxFaresjsonobjetreturn = new JSONObject();
			JSONArray paxFaresjsonarrayreturn = new JSONArray();
			JSONObject bookSegmentsjsonobjetreturn = new JSONObject();

			JSONObject legsjsonobjetreturn = new JSONObject();
			JSONArray legsjsonarrayreturn = new JSONArray();

			JSONObject bondsjsonobjetreturn = new JSONObject();
			JSONArray bondsjsonarrayreturn = new JSONArray();

			JSONObject ssrDetailsjsonobjetreturn = new JSONObject();
			JSONArray ssrDetailsjsonarrayreturn = new JSONArray();

			JSONArray engineIDListreturn = new JSONArray();

			paxFaresjsonobjetreturn.put("baggageUnit", req.getBaggageUnitreturn());
			paxFaresjsonobjetreturn.put("baggageWeight", req.getBaggageWeightreturn());
			paxFaresjsonobjetreturn.put("baseTransactionAmount", 0);
			paxFaresjsonobjetreturn.put("basicFare", req.getBaggageFarereturn());
			paxFaresjsonobjetreturn.put("cancelPenalty", req.getCancelPenaltyreturn());
			paxFaresjsonobjetreturn.put("changePenalty", req.getChangePenaltyreturn());
			paxFaresjsonobjetreturn.put("equivCurrencyCode", "");
			paxFaresjsonobjetreturn.put("basicFare", req.getBasicFare());
			paxFaresjsonobjetreturn.put("fareBasisCode", "");
			paxFaresjsonobjetreturn.put("fareInfoKey", "");
			paxFaresjsonobjetreturn.put("fareInfoValue", "");
			paxFaresjsonobjetreturn.put("markUP", 0);
			paxFaresjsonobjetreturn.put("paxType", "ADT");
			paxFaresjsonobjetreturn.put("refundable", true);
			paxFaresjsonobjetreturn.put("totalFare", req.getTotalFarereturn());
			paxFaresjsonobjetreturn.put("totalTax", req.getTotalTaxreturn());
			paxFaresjsonobjetreturn.put("transactionAmount", 0);
			paxFaresjsonobjetreturn.put("bookFares", bookfarejsonArrayreturn);
			paxFaresjsonarrayreturn.put(paxFaresjsonobjetreturn);

			faresjsonobjetreturn.put("basicFare", req.getBasicFare());
			faresjsonobjetreturn.put("exchangeRate", 0);

			faresjsonobjetreturn.put("totalFareWithOutMarkUp", req.getTotalFareWithOutMarkUpreturn());
			faresjsonobjetreturn.put("totalTaxWithOutMarkUp", req.getTotalTaxWithOutMarkUpreturn());
			faresjsonobjetreturn.put("paxFares", paxFaresjsonarrayreturn);

			payload.put("spKey", req.getAirlineNamereturn() + "IN");

			bondsjsonobjetreturn.put("boundType", "");
			bondsjsonobjetreturn.put("baggageFare", false);
			bondsjsonobjetreturn.put("ssrFare", false);
			bondsjsonobjetreturn.put("itineraryKey", req.getItineraryKeyreturn());
			bondsjsonobjetreturn.put("journeyTime", req.getDurationreturn());

			bondsjsonobjetreturn.put("addOnDetail", "");

			legsjsonobjetreturn.put("aircraftCode", "");
			legsjsonobjetreturn.put("aircraftType", "");
			legsjsonobjetreturn.put("airlineName", req.getAirlineNamereturn());
			legsjsonobjetreturn.put("amount", req.getAmountreturn());
			legsjsonobjetreturn.put("arrivalDate", req.getArrivalDatereturn());
			legsjsonobjetreturn.put("arrivalTerminal", "");
			legsjsonobjetreturn.put("arrivalTime", req.getArrivalTimereturn());
			legsjsonobjetreturn.put("availableSeat", "");
			legsjsonobjetreturn.put("baggageUnit", req.getBaggageUnitreturn());
			legsjsonobjetreturn.put("baggageWeight", req.getBaggageWeightreturn());
			legsjsonobjetreturn.put("boundTypes", "");
			legsjsonobjetreturn.put("cabin", "Economy");
			legsjsonobjetreturn.put("cabinClasses", objhasmap1);
			legsjsonobjetreturn.put("capacity", "0");
			legsjsonobjetreturn.put("carrierCode", req.getAirlineNamereturn());
			legsjsonobjetreturn.put("currencyCode", "INR");
			legsjsonobjetreturn.put("departureDate", req.getDepartureDatereturn());
			legsjsonobjetreturn.put("departureTerminal", req.getDepartureTerminalreturn());
			legsjsonobjetreturn.put("departureTime", req.getDepartureTimereturn());
			legsjsonobjetreturn.put("destination", req.getOrigin());
			legsjsonobjetreturn.put("duration", req.getDurationreturn());
			legsjsonobjetreturn.put("fareBasisCode", req.getFareBasisCodereturn());
			legsjsonobjetreturn.put("fareClassOfService", req.getFareClassOfServicereturn());
			legsjsonobjetreturn.put("flightDesignator", "");
			legsjsonobjetreturn.put("flightDetailRefKey", "");
			legsjsonobjetreturn.put("flightName", "Indigo");
			legsjsonobjetreturn.put("flightNumber", req.getFlightNumberreturn());
			legsjsonobjetreturn.put("group", "");
			legsjsonobjetreturn.put("connecting", false);
			legsjsonobjetreturn.put("numberOfStops", "");
			legsjsonobjetreturn.put("origin", req.getDestination());
			legsjsonobjetreturn.put("providerCode", "");
			legsjsonobjetreturn.put("remarks", "");
			ssrDetailsjsonarrayreturn.put(ssrDetailsjsonobjetreturn);
			legsjsonobjetreturn.put("sold", 0);
			legsjsonobjetreturn.put("status", "Normal");
			legsjsonobjetreturn.put("ssrDetails", ssrDetailsjsonarrayreturn);
			legsjsonarrayreturn.put(legsjsonobjetreturn);

			bondsjsonobjetreturn.put("legs", legsjsonarrayreturn);

			bondsjsonarrayreturn.put(bondsjsonobjetreturn);

			bookSegmentsjsonobjetreturn.put("bonds", bondsjsonarrayreturn);
			bookSegmentsjsonobjetreturn.put("deeplink", "");
			bookSegmentsjsonobjetreturn.put("fareIndicator", 0);
			bookSegmentsjsonobjetreturn.put("fareRule", req.getFareRule());
			bookSegmentsjsonobjetreturn.put("baggageFare", false);
			bookSegmentsjsonobjetreturn.put("cache", false);
			bookSegmentsjsonobjetreturn.put("holdBooking", false);
			bookSegmentsjsonobjetreturn.put("international", false);
			bookSegmentsjsonobjetreturn.put("roundTrip", false);
			bookSegmentsjsonobjetreturn.put("special", false);
			bookSegmentsjsonobjetreturn.put("specialId", false);

			bookSegmentsjsonobjetreturn.put("itineraryKey", req.getItineraryKey());

			bookSegmentsjsonobjetreturn.put("journeyIndex", 0);
			bookSegmentsjsonobjetreturn.put("memoryCreationTime", "/Date(1499158249483+0530)/");
			bookSegmentsjsonobjetreturn.put("nearByAirport", false);
			bookSegmentsjsonobjetreturn.put("remark", "");
			bookSegmentsjsonobjetreturn.put("searchId", req.getSearchIdreturn());

			bookSegmentsjsonobjetreturn.put("engineID", "Indigo");
			bookSegmentsjsonobjetreturn.put("fares", faresjsonobjetreturn);

			bookSegmentsjsonarray.put(bookSegmentsjsonobjetreturn);

			payload.put("bookSegments", bookSegmentsjsonarray);

			flightSearchDetailsjsonobject.put("beginDate", req.getBeginDate());
			flightSearchDetailsjsonobject.put("destination", req.getDestination());
			flightSearchDetailsjsonobject.put("endDate", req.getEndDate());
			flightSearchDetailsjsonobject.put("origin", req.getOrigin());
			flightSearchDetailsaray.add(flightSearchDetailsjsonobject);
			JSONObject roundtripDetailsobj = new JSONObject();

			roundtripDetailsobj.put("origin", req.getDestination());
			roundtripDetailsobj.put("destination", req.getOrigin());
			roundtripDetailsobj.put("endDate", req.getEndDate());
			roundtripDetailsobj.put("beginDate", req.getBeginDate());

			flightSearchDetailsaray.add(roundtripDetailsobj);
			payload.put("flightSearchDetails", flightSearchDetailsaray);

			paymentDetails.put("bookingAmount", req.getGrandtotal());
			paymentDetails.put("bookingCurrencyCode", "INR");
			payload.put("paymentDetails", paymentDetails);

			payload.put("engineIDList", objhasmap);

			JSONArray adultTravellers = CreateJsonRequestFlight.createMobileReturnjsonAdultraveler(req, "");
			JSONArray childTravellers = CreateJsonRequestFlight.createMobileReturnjsonChildraveler(req, "");
			JSONArray infantTravellers = CreateJsonRequestFlight.createMobileReturnjsonInfantrevaleer(req, "");
			JSONObject adultTravellersnew = new JSONObject();
			adultTravellersnew.put("adultTravellers", adultTravellers);
			adultTravellersnew.put("childTravellers", childTravellers);
			adultTravellersnew.put("infantTravellers", infantTravellers);
			payload.put("travellers", adultTravellersnew);
			payload.put("visatype", "Employee Visa");
			payload.put("traceId", "AYTM00011111111110001");
			payload.put("transactionId", req.getTransactionId());
			payload.put("androidBooking", true);
			payload.put("domestic", true);
			payload.put("engineID", objhasmap.get(0));
			return payload;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static JSONArray createjsonAdultraveler(FlightBookRequest req, String source) {
		JSONArray adultTravellers = new JSONArray();

		try {
			System.err.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@" + req.getAdults());
			if (req.getAdults().equals("1")) {
				System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@" + req.getAdults());
				JSONObject travellers = new JSONObject();
				travellers.put("address1", "");
				travellers.put("address2", "");
				travellers.put("city", "");
				travellers.put("countryCode", "");
				travellers.put("cultureCode", "String content");
				if (req.getAdultDateOfBirth()!=null) {
					travellers.put("dateofBirth",req.getAdultDateOfBirth().replace("~", ""));
				}
				else {
					travellers.put("dateofBirth", "");
				}
				
				travellers.put("emailAddress", req.getEmailAddress());
				travellers.put("firstName", req.getFirstName().replace("~", ""));
				travellers.put("frequentFlierNumber", "");
				
				if (req.getUseradultsgender().replace("#", "").equalsIgnoreCase("Mr")) {
					travellers.put("gender", "MALE");
				}
				else {
					travellers.put("gender", "FEMALE");
				}
				travellers.put("homePhone", "");
				travellers.put("lastName", req.getLastName().replace("@", ""));
				travellers.put("meal", "");
				travellers.put("middleName", "");
				travellers.put("mobileNumber", req.getMobileNumber());
				travellers.put("nationality", "");
				
				if (req.getAdultspassNo()!=null) {
					travellers.put("passportNo", req.getAdultspassNo().replace("~", ""));
				} else {
					travellers.put("passportNo", "");
				}
				if (req.getAdultspassExp()!=null) {
					travellers.put("passportExpiryDate", req.getAdultspassExp().replace("~", ""));
				} else {
					travellers.put("passportExpiryDate", "");
				}
				
				/*travellers.put("passportExpiryDate", "");
				travellers.put("passportNo", "");*/
				
				travellers.put("paxType", "ADT");
				travellers.put("provisionState", "");
				travellers.put("residentCountry", "");
				travellers.put("title", "" + req.getUseradultsgender().replace("#", ""));
				adultTravellers.put(travellers);
				System.out.println(travellers);
				System.out.println(adultTravellers);
			} else if (req.getAdults().equals("0")) {

			} else {
				String fanme[] = req.getFirstName().split("~");

				String lname[] = req.getLastName().split("@");

				String gender[] = req.getUseradultsgender().split("#");

				//String dob[] = req.getAdultDateOfBirth().split("~");

				String dob[]=new String[fanme.length];
				String passNo[]=new String[fanme.length];
				String passExp[]=new String[fanme.length];
				
				if (req.getAdultDateOfBirth()!=null) {
					dob=req.getAdultDateOfBirth().split("~");
				}
				if (req.getAdultspassNo()!=null) {
					passNo=req.getAdultspassNo().split("~");
				}
				if (req.getAdultspassExp()!=null) {
					passExp=req.getAdultspassExp().split("~");
				}
				

				for (int i = 1; i < fanme.length; i++) {

					JSONObject travellers = new JSONObject();
					travellers.put("address1", "");
					travellers.put("address2", "");
					travellers.put("city", "");
					travellers.put("countryCode", "");
					travellers.put("cultureCode", "String content");
					travellers.put("dateofBirth", dob[i]== null ? "" : dob[i] );
					travellers.put("emailAddress", req.getEmailAddress());
					travellers.put("firstName", fanme[i]);
					travellers.put("frequentFlierNumber", "");
					if (gender[i].equalsIgnoreCase("Mr")) {
						travellers.put("gender", "MALE");
					}
					else {
						travellers.put("gender", "FEMALE");
					}
					travellers.put("gender", "MALE");
					travellers.put("homePhone", "");
					travellers.put("lastName", lname[i]);
					travellers.put("meal", "VGML");
					travellers.put("middleName", "");
					travellers.put("mobileNumber", req.getMobileNumber());
					travellers.put("nationality", "");

					travellers.put("passportExpiryDate", passExp[i]== null ? "" : passExp[i]);
					travellers.put("passportNo", passNo[i] == null ? "" : passNo[i]);
					
					travellers.put("paxType", "ADT");
					travellers.put("provisionState", "");
					travellers.put("residentCountry", "");
					travellers.put("title", gender[i]);
					adultTravellers.put(travellers);

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}
		return adultTravellers;
	}

	public static JSONArray createjsonChildraveler(FlightBookRequest req, String source) {
		JSONArray childTravellers = new JSONArray();

		try {
			if (req.getChilds().equals("1")) {
				JSONObject travellers = new JSONObject();
				travellers.put("address1", "");
				travellers.put("address2", "");
				travellers.put("city", "");
				travellers.put("countryCode", "");
				travellers.put("cultureCode", "String content");
				
				if (req.getChildDateOfBirth()!=null) {
					travellers.put("dateofBirth",req.getChildDateOfBirth().replace("~", ""));
				}
				else {
					travellers.put("dateofBirth", "");
				}
				
				travellers.put("emailAddress", req.getEmailAddress());
				travellers.put("firstName", req.getFirstNamechild().replace("~", ""));
				travellers.put("frequentFlierNumber", "");
				
				if (req.getUserchildsgender().replace("#", "").equalsIgnoreCase("Mr")) {
					travellers.put("gender", "MALE");
				}
				else {
					travellers.put("gender", "FEMALE");
				}
				travellers.put("homePhone", "");
				travellers.put("lastName", req.getLastNamechild().replace("@", ""));
				travellers.put("meal", "");
				travellers.put("middleName", "");
				travellers.put("mobileNumber", req.getMobileNumber());
				travellers.put("nationality", "");

				if (req.getChildspassNo()!=null) {
					travellers.put("passportNo", req.getChildspassNo().replace("~", ""));
				} else {
					travellers.put("passportNo", "");
				}
				if (req.getChildspassExp()!=null) {
					travellers.put("passportExpiryDate", req.getChildspassExp().replace("~", ""));
				} else {
					travellers.put("passportExpiryDate", "");
				}

				travellers.put("paxType", "CHD");
				travellers.put("provisionState", "");
				travellers.put("residentCountry", "");
				travellers.put("title", req.getUserchildsgender().replace("#", ""));
				childTravellers.put(travellers);
			} else if (req.getChilds().equals("0")) {

			} else {

				String fanme[] = req.getFirstNamechild().split("~");

				String lname[] = req.getLastNamechild().split("@");
				String gender[] = req.getUserchildsgender().split("#");
				//String dob[] = req.getChildDateOfBirth().split("~");

				String dob[]=new String[fanme.length];
				String passNo[]=new String[fanme.length];
				String passExp[]=new String[fanme.length];
				
				if (req.getChildDateOfBirth()!=null) {
					dob=req.getChildDateOfBirth().split("~");
				}
				if (req.getChildspassNo()!=null) {
					passNo=req.getChildspassNo().split("~");
				}
				if (req.getChildspassExp()!=null) {
					passExp=req.getChildspassExp().split("~");
				}
				
				for (int i = 1; i < fanme.length; i++) {

					JSONObject travellers = new JSONObject();
					travellers.put("address1", "");
					travellers.put("address2", "");
					travellers.put("city", "");
					travellers.put("countryCode", "");
					travellers.put("cultureCode", "String content");
					travellers.put("dateofBirth", dob[i]== null ? "" : dob[i]);
					travellers.put("emailAddress", req.getEmailAddress());
					travellers.put("firstName", fanme[i]);
					travellers.put("frequentFlierNumber", "");
					
					if (gender[i].equalsIgnoreCase("Mr")) {
						travellers.put("gender", "MALE");
					}
					else {
						travellers.put("gender", "FEMALE");
					}
					
					travellers.put("homePhone", "");
					travellers.put("lastName", lname[i]);
					travellers.put("meal", "");
					travellers.put("middleName", "");
					travellers.put("mobileNumber", req.getMobileNumber());
					travellers.put("nationality", "");

					travellers.put("passportExpiryDate", passExp[i] == null ? "" : passExp[i]);
					travellers.put("passportNo", passNo[i] == null ? "" : passNo[i]);
					
					travellers.put("paxType", "ADT");
					travellers.put("provisionState", "");
					travellers.put("residentCountry", "");
					travellers.put("title", gender[i]);
					childTravellers.put(travellers);

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return childTravellers;
	}

	public static JSONArray createjsonInfantrevaleer(FlightBookRequest req, String source) {

		JSONArray infantTravellers = new JSONArray();
		try {

			if (req.getInfants().equals("1")) {
				JSONObject travellers = new JSONObject();
				travellers.put("address1", "");
				travellers.put("address2", "");
				travellers.put("city", "");
				travellers.put("countryCode", "");
				travellers.put("cultureCode", "String content");

				if (req.getInfantDateOfBirth()!=null) {
					travellers.put("dateofBirth",req.getInfantDateOfBirth().replace("~", ""));
				}
				else {
					travellers.put("dateofBirth", "");
				}
				
				travellers.put("emailAddress", req.getEmailAddress());
				travellers.put("firstName", req.getFirstNameinfant().replace("~", ""));
				travellers.put("frequentFlierNumber", "");
				if (req.getUserinfantsgender().replace("#", "").equalsIgnoreCase("Mr")) {
					travellers.put("gender", "MALE");
				}
				else {
					travellers.put("gender", "FEMALE");
				}
				
				travellers.put("homePhone", "");
				travellers.put("lastName", req.getLastNameinfant().replace("@", ""));
				travellers.put("meal", "");
				travellers.put("middleName", "");
				travellers.put("mobileNumber", req.getMobileNumber());
				travellers.put("nationality", "");

				if (req.getInfantspassNo()!=null) {
					travellers.put("passportNo", req.getInfantspassNo().replace("~", ""));
				} else {
					travellers.put("passportNo", "");
				}
				if (req.getInfantspassExp()!=null) {
					travellers.put("passportExpiryDate", req.getInfantspassExp().replace("~", ""));
				} else {
					travellers.put("passportExpiryDate", "");
				}
				
				travellers.put("paxType", "INF");
				travellers.put("provisionState", "");
				travellers.put("residentCountry", "");
				travellers.put("title", req.getUserinfantsgender().replace("#", ""));
				infantTravellers.put(travellers);
			} else if (req.getInfants().equals("0")) {

			} else {
				String fanme[] = req.getFirstNameinfant().split("~");

				String lname[] = req.getLastNameinfant().split("@");
				String gender[] = req.getUserinfantsgender().split("#");
				//String dob[] = req.getInfantDateOfBirth().split("~");

				String dob[]=new String[fanme.length];
				String passNo[]=new String[fanme.length];
				String passExp[]=new String[fanme.length];
				
				if (req.getInfantDateOfBirth()!=null) {
					dob=req.getInfantDateOfBirth().split("~");
				}
				if (req.getInfantspassNo()!=null) {
					passNo=req.getInfantspassNo().split("~");
				}
				if (req.getInfantspassExp()!=null) {
					passExp=req.getInfantspassExp().split("~");
				}
				
				for (int i = 1; i < fanme.length; i++) {

					JSONObject travellers = new JSONObject();
					travellers.put("address1", "");
					travellers.put("address2", "");
					travellers.put("city", "");
					travellers.put("countryCode", "");
					travellers.put("cultureCode", "String content");
					travellers.put("dateofBirth", dob[i]== null ? "" : dob[i]);
					travellers.put("emailAddress", req.getEmailAddress());
					travellers.put("firstName", fanme[i]);
					travellers.put("frequentFlierNumber", "");
					
					if (gender[i].equalsIgnoreCase("Mr")) {
						travellers.put("gender", "MALE");
					}
					else {
						travellers.put("gender", "FEMALE");
					}
					
					travellers.put("homePhone", "");
					travellers.put("lastName", lname[i]);
					travellers.put("meal", "");
					travellers.put("middleName", "");
					travellers.put("mobileNumber", req.getMobileNumber());
					travellers.put("nationality", "");
					
					travellers.put("passportExpiryDate", passExp[i]== null ? "" : passExp[i]);
					travellers.put("passportNo", passNo[i] == null ? "" : passNo[i]);
					
					travellers.put("paxType", "ADT");
					travellers.put("provisionState", "");
					travellers.put("residentCountry", "");
					travellers.put("title", gender[i]);
					infantTravellers.put(travellers);

				}
			}

			return infantTravellers;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	
	// Return json is create

	public static JSONArray createReturnjsonAdultraveler(ReturnFlightBookRequest req, String source1) {
		JSONArray adultTravellers = new JSONArray();

		try {
			if (req.getAdults().equals("1")) {
				JSONObject travellers = new JSONObject();
				travellers.put("address1", "");
				travellers.put("address2", "");
				travellers.put("city", "");
				travellers.put("countryCode", "");
				travellers.put("cultureCode", "String content");
				if (req.getAdultDateOfBirth()!=null) {
					travellers.put("dateofBirth",req.getAdultDateOfBirth().replace("~", ""));
				}
				else {
					travellers.put("dateofBirth", "");
				}
				travellers.put("emailAddress", req.getEmailAddress());
				travellers.put("firstName", req.getFirstName().replace("~", ""));
				travellers.put("frequentFlierNumber", "");
				if (req.getUseradultsgender().replace("#", "").equalsIgnoreCase("Mr")) {
					travellers.put("gender", "MALE");
				}
				else {
					travellers.put("gender", "FEMALE");
				}
				
				travellers.put("homePhone", "");
				travellers.put("lastName", req.getLastName().replace("@", ""));
				travellers.put("meal", "");
				travellers.put("middleName", "");
				travellers.put("mobileNumber", req.getMobileNumber());
				travellers.put("nationality", "");
				if (req.getAdultspassNo()!=null) {
					travellers.put("passportNo", req.getAdultspassNo().replace("~", ""));
				} else {
					travellers.put("passportNo", "");
				}
				if (req.getAdultspassExp()!=null) {
					travellers.put("passportExpiryDate", req.getAdultspassExp().replace("~", ""));
				} else {
					travellers.put("passportExpiryDate", "");
				}
				travellers.put("paxType", "ADT");
				travellers.put("provisionState", "");
				travellers.put("residentCountry", "");
				travellers.put("title", req.getUseradultsgender().replace("#", ""));
				
				adultTravellers.put(travellers);
			} else if (req.getAdults().equals("0")) {

			} else {
				String fanme[] = req.getFirstName().split("~");

				String lname[] = req.getLastName().split("@");
				String gender[] = req.getUseradultsgender().split("#");
				String dob[]=new String[fanme.length];
				String passNo[]=new String[fanme.length];
				String passExp[]=new String[fanme.length];
				
				if (req.getAdultDateOfBirth()!=null) {
					dob=req.getAdultDateOfBirth().split("~");
				}
				if (req.getAdultspassNo()!=null) {
					passNo=req.getAdultspassNo().split("~");
				}
				if (req.getAdultspassExp()!=null) {
					passExp=req.getAdultspassExp().split("~");
				}
				
				for (int i = 1; i < fanme.length; i++) {

					JSONObject travellers = new JSONObject();
					travellers.put("address1", "");
					travellers.put("address2", "");
					travellers.put("city", "");
					travellers.put("countryCode", "");
					travellers.put("cultureCode", "String content");
					travellers.put("dateofBirth", dob[i]== null ? "" : dob[i] );
					travellers.put("emailAddress", req.getEmailAddress());
					travellers.put("firstName", fanme[i]);
					travellers.put("frequentFlierNumber", "");
					
					if (gender[i].equalsIgnoreCase("Mr")) {
						travellers.put("gender", "MALE");
					}
					else {
						travellers.put("gender", "FEMALE");
					}
					
					travellers.put("homePhone", "");
					travellers.put("lastName", lname[i]);
					travellers.put("meal", "");
					travellers.put("middleName", "");
					travellers.put("mobileNumber", req.getMobileNumber());
					travellers.put("nationality", "");
					travellers.put("passportExpiryDate", passExp[i]== null ? "" : passExp[i]);
					travellers.put("passportNo", passNo[i] == null ? "" : passNo[i]);
					travellers.put("paxType", "ADT");
					travellers.put("provisionState", "");
					travellers.put("residentCountry", "");
					travellers.put("title", gender[i]);
					adultTravellers.put(travellers);

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return adultTravellers;
	}

	public static JSONArray createReturnjsonChildraveler(ReturnFlightBookRequest req, String source) {
		JSONArray childTravellers = new JSONArray();

		try {
			if (req.getChilds().equals("1")) {
				JSONObject travellers = new JSONObject();
				travellers.put("address1", "");
				travellers.put("address2", "");
				travellers.put("city", "");
				travellers.put("countryCode", "");
				travellers.put("cultureCode", "String content");
				if (req.getChildDateOfBirth()!=null) {
					travellers.put("dateofBirth",req.getChildDateOfBirth().replace("~", ""));
				}
				else {
					travellers.put("dateofBirth", "");
				}
				travellers.put("emailAddress", req.getEmailAddress());
				travellers.put("firstName", req.getFirstNamechild().replace("~", ""));
				travellers.put("frequentFlierNumber", "");
				
				if (req.getUseradultsgender().replace("#", "").equalsIgnoreCase("Mr")) {
					travellers.put("gender", "MALE");
				}
				else {
					travellers.put("gender", "FEMALE");
				}
				
				travellers.put("homePhone", "");
				travellers.put("lastName", req.getLastNamechild().replace("@", ""));
				travellers.put("meal", "");
				travellers.put("middleName", "");
				travellers.put("mobileNumber", req.getMobileNumber());
				travellers.put("nationality", "");
				
				if (req.getChildspassNo()!=null) {
					travellers.put("passportNo", req.getChildspassNo().replace("~", ""));
				} else {
					travellers.put("passportNo", "");
				}
				if (req.getChildspassExp()!=null) {
					travellers.put("passportExpiryDate", req.getChildspassExp().replace("~", ""));
				} else {
					travellers.put("passportExpiryDate", "");
				}
				
				/*travellers.put("passportExpiryDate", "");
				travellers.put("passportNo", "");*/
				
				travellers.put("paxType", "ADT");
				travellers.put("provisionState", "");
				travellers.put("residentCountry", "");
				travellers.put("title", req.getUserchildsgender().replace("#", ""));
				childTravellers.put(travellers);
			} else if (req.getChilds().equals("0")) {

			} else {

				String fanme[] = req.getFirstNamechild().split("~");

				String lname[] = req.getLastNamechild().split("@");
				String gender[] = req.getUserchildsgender().split("#");
				
				String dob[]=new String[fanme.length];
				String passNo[]=new String[fanme.length];
				String passExp[]=new String[fanme.length];
				
				if (req.getChildDateOfBirth()!=null) {
					dob=req.getChildDateOfBirth().split("~");
				}
				if (req.getChildspassNo()!=null) {
					passNo=req.getChildspassNo().split("~");
				}
				if (req.getChildspassExp()!=null) {
					passExp=req.getChildspassExp().split("~");
				}
				
				
				for (int i = 1; i < fanme.length; i++) {

					JSONObject travellers = new JSONObject();
					travellers.put("address1", "");
					travellers.put("address2", "");
					travellers.put("city", "");
					travellers.put("countryCode", "");
					travellers.put("cultureCode", "String content");
					travellers.put("dateofBirth", dob[i]== null ? "" : dob[i]);
					travellers.put("emailAddress", req.getEmailAddress());
					travellers.put("firstName", fanme[i]);
					travellers.put("frequentFlierNumber", "");

					if (gender[i].equalsIgnoreCase("Mr")) {
						travellers.put("gender", "MALE");
					}
					else {
						travellers.put("gender", "FEMALE");
					}
					
					travellers.put("homePhone", "");
					travellers.put("lastName", lname[i]);
					travellers.put("meal", "");
					travellers.put("middleName", "");
					travellers.put("mobileNumber", req.getMobileNumber());
					travellers.put("nationality", "");
					travellers.put("passportExpiryDate", passExp[i] == null ? "" : passExp[i]);
					travellers.put("passportNo", passNo[i] == null ? "" : passNo[i]);
					travellers.put("paxType", "ADT");
					travellers.put("provisionState", "");
					travellers.put("residentCountry", "");
					travellers.put("title", gender[i]);
					childTravellers.put(travellers);

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return childTravellers;
	}

	public static JSONArray createReturnjsonInfantrevaleer(ReturnFlightBookRequest req, String source) {

		JSONArray infantTravellers = new JSONArray();
		try {
			if (req.getInfants().equals("1")) {
				JSONObject travellers = new JSONObject();
				travellers.put("address1", "");
				travellers.put("address2", "");
				travellers.put("city", "");
				travellers.put("countryCode", "");
				travellers.put("cultureCode", "String content");
				
				if (req.getInfantDateOfBirth()!=null) {
					travellers.put("dateofBirth",req.getInfantDateOfBirth().replace("~", ""));
				}
				else {
					travellers.put("dateofBirth", "");
				}
				
//				travellers.put("dateofBirth", "");
				
				travellers.put("emailAddress", req.getEmailAddress());
				travellers.put("firstName", req.getFirstNameinfant().replace("~", ""));
				travellers.put("frequentFlierNumber", "");
				
				if (req.getUserinfantsgender().replace("#", "").equalsIgnoreCase("Mr")) {
					travellers.put("gender", "MALE");
				}
				else {
					travellers.put("gender", "FEMALE");
				}
				
				travellers.put("homePhone", "");
				travellers.put("lastName", req.getLastNameinfant().replace("@", ""));
				travellers.put("meal", "");
				travellers.put("middleName", "");
				travellers.put("mobileNumber", req.getMobileNumber());
				travellers.put("nationality", "");
				
				if (req.getInfantspassNo()!=null) {
					travellers.put("passportNo", req.getInfantspassNo().replace("~", ""));
				} else {
					travellers.put("passportNo", "");
				}
				if (req.getInfantspassExp()!=null) {
					travellers.put("passportExpiryDate", req.getInfantspassExp().replace("~", ""));
				} else {
					travellers.put("passportExpiryDate", "");
				}
				
				/*travellers.put("passportExpiryDate", "");
				travellers.put("passportNo", "");*/
				
				travellers.put("paxType", "ADT");
				travellers.put("provisionState", "");
				travellers.put("residentCountry", "");
				travellers.put("title", req.getUserinfantsgender().replace("#", ""));
				infantTravellers.put(travellers);
			} else if (req.getInfants().equals("0")) {

			} else {
				String fanme[] = req.getFirstNameinfant().split("~");

				String lname[] = req.getLastNameinfant().split("@");
				String gender[] = req.getUserinfantsgender().split("#");
				
				String dob[]=new String[fanme.length];
				String passNo[]=new String[fanme.length];
				String passExp[]=new String[fanme.length];
				
				if (req.getInfantDateOfBirth()!=null) {
					dob=req.getInfantDateOfBirth().split("~");
				}
				if (req.getInfantspassNo()!=null) {
					passNo=req.getInfantspassNo().split("~");
				}
				if (req.getInfantspassExp()!=null) {
					passExp=req.getInfantspassExp().split("~");
				}
				
				
				for (int i = 1; i < fanme.length; i++) {

					JSONObject travellers = new JSONObject();
					travellers.put("address1", "");
					travellers.put("address2", "");
					travellers.put("city", "");
					travellers.put("countryCode", "");
					travellers.put("cultureCode", "String content");
					travellers.put("dateofBirth", dob[i]== null ? "" : dob[i]);
					travellers.put("emailAddress", req.getEmailAddress());
					travellers.put("firstName", fanme[i]);
					travellers.put("frequentFlierNumber", "");
					
					if (gender[i].equalsIgnoreCase("Mr")) {
						travellers.put("gender", "MALE");
					}
					else {
						travellers.put("gender", "FEMALE");
					}
					
					travellers.put("homePhone", "");
					travellers.put("lastName", lname[i]);
					travellers.put("meal", "");
					travellers.put("middleName", "");
					travellers.put("mobileNumber", req.getMobileNumber());
					travellers.put("nationality", "");
					travellers.put("passportExpiryDate", passExp[i]== null ? "" : passExp[i]);
					travellers.put("passportNo", passNo[i] == null ? "" : passNo[i]);
					travellers.put("paxType", "ADT");
					travellers.put("provisionState", "");
					travellers.put("residentCountry", "");
					travellers.put("title",gender[i]);
					infantTravellers.put(travellers);

				}
			}

			return infantTravellers;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	/*-----------------------------------------------------------------------------------------*/

	public static JSONArray createMobilejsonAdultraveler(MobileFlightBookRequest req, String source) {
		JSONArray adultTravellers = new JSONArray();

		try {
			if (req.getAdults().equals("1")) {
				JSONObject travellers = new JSONObject();
				travellers.put("address1", "BTM");
				travellers.put("address2", "BTM");
				travellers.put("city", "Bangalore");
				travellers.put("countryCode", "IN");
				travellers.put("cultureCode", "String content");
				travellers.put("dateofBirth", "");
				travellers.put("emailAddress", req.getEmailAddress());
				travellers.put("firstName", req.getFirstName().replace("~", ""));
				travellers.put("frequentFlierNumber", "");
				travellers.put("gender", "MALE");
				travellers.put("homePhone", "");
				travellers.put("lastName", req.getLastName().replace("@", ""));
				travellers.put("meal", "VGML");
				travellers.put("middleName", "");
				travellers.put("mobileNumber", req.getMobileNumber());
				travellers.put("nationality", "India");
				travellers.put("passportExpiryDate", "");
				travellers.put("passportNo", "");
				travellers.put("paxType", "ADT");
				travellers.put("provisionState", "Karnataka");
				travellers.put("residentCountry", "IN");
				travellers.put("title", "Mr.");
				adultTravellers.put(travellers);
			} else if (req.getAdults().equals("0")) {

			} else {
				String fanme[] = req.getFirstName().split("~");

				String lname[] = req.getLastName().split("@");

				for (int i = 1; i < fanme.length; i++) {

					JSONObject travellers = new JSONObject();
					travellers.put("address1", "BTM");
					travellers.put("address2", "BTM");
					travellers.put("city", "Bangalore");
					travellers.put("countryCode", "IN");
					travellers.put("cultureCode", "String content");
					travellers.put("dateofBirth", "");
					travellers.put("emailAddress", req.getEmailAddress());
					travellers.put("firstName", fanme[i]);
					travellers.put("frequentFlierNumber", "");
					travellers.put("gender", "MALE");
					travellers.put("homePhone", "");
					travellers.put("lastName", lname[i]);
					travellers.put("meal", "VGML");
					travellers.put("middleName", "");
					travellers.put("mobileNumber", req.getMobileNumber());
					travellers.put("nationality", "India");
					travellers.put("passportExpiryDate", "");
					travellers.put("passportNo", "");
					travellers.put("paxType", "ADT");
					travellers.put("provisionState", "Karnataka");
					travellers.put("residentCountry", "IN");
					travellers.put("title", "Mr.");
					adultTravellers.put(travellers);

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return adultTravellers;
	}

	public static JSONArray createMobilejsonChildraveler(MobileFlightBookRequest req, String source) {
		JSONArray childTravellers = new JSONArray();

		try {
			if (req.getChilds().equals("1")) {
				JSONObject travellers = new JSONObject();
				travellers.put("address1", "BTM");
				travellers.put("address2", "BTM");
				travellers.put("city", "Bangalore");
				travellers.put("countryCode", "IN");
				travellers.put("cultureCode", "String content");
				travellers.put("dateofBirth", "");
				travellers.put("emailAddress", req.getEmailAddress());
				travellers.put("firstName", req.getFirstNamechild().replace("~", ""));
				travellers.put("frequentFlierNumber", "");
				travellers.put("gender", "MALE");
				travellers.put("homePhone", "");
				travellers.put("lastName", req.getLastNamechild().replace("@", ""));
				travellers.put("meal", "VGML");
				travellers.put("middleName", "");
				travellers.put("mobileNumber", req.getMobileNumber());
				travellers.put("nationality", "India");
				travellers.put("passportExpiryDate", "");
				travellers.put("passportNo", "");
				travellers.put("paxType", "ADT");
				travellers.put("provisionState", "Karnataka");
				travellers.put("residentCountry", "IN");
				travellers.put("title", "Mr.");
				childTravellers.put(travellers);
			} else if (req.getChilds().equals("0")) {

			} else {

				String fanme[] = req.getFirstNamechild().split("~");

				String lname[] = req.getLastNamechild().split("@");

				for (int i = 1; i < fanme.length; i++) {

					JSONObject travellers = new JSONObject();
					travellers.put("address1", "BTM");
					travellers.put("address2", "BTM");
					travellers.put("city", "Bangalore");
					travellers.put("countryCode", "IN");
					travellers.put("cultureCode", "String content");
					travellers.put("dateofBirth", "");
					travellers.put("emailAddress", req.getEmailAddress());
					travellers.put("firstName", fanme[i]);
					travellers.put("frequentFlierNumber", "");
					travellers.put("gender", "MALE");
					travellers.put("homePhone", "");
					travellers.put("lastName", lname[i]);
					travellers.put("meal", "VGML");
					travellers.put("middleName", "");
					travellers.put("mobileNumber", req.getMobileNumber());
					travellers.put("nationality", "India");
					travellers.put("passportExpiryDate", "");
					travellers.put("passportNo", "");
					travellers.put("paxType", "ADT");
					travellers.put("provisionState", "Karnataka");
					travellers.put("residentCountry", "IN");
					travellers.put("title", "Mr.");
					childTravellers.put(travellers);

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return childTravellers;
	}

	public static JSONArray createMobilejsonInfantrevaleer(MobileFlightBookRequest req, String source) {

		JSONArray infantTravellers = new JSONArray();
		try {
			if (req.getInfants().equals("1")) {
				JSONObject travellers = new JSONObject();
				travellers.put("address1", "BTM");
				travellers.put("address2", "BTM");
				travellers.put("city", "Bangalore");
				travellers.put("countryCode", "IN");
				travellers.put("cultureCode", "String content");
				travellers.put("dateofBirth", "");
				travellers.put("emailAddress", req.getEmailAddress());
				travellers.put("firstName", req.getFirstNameinfant().replace("~", ""));
				travellers.put("frequentFlierNumber", "");
				travellers.put("gender", "MALE");
				travellers.put("homePhone", "");
				travellers.put("lastName", req.getLastNameinfant().replace("@", ""));
				travellers.put("meal", "VGML");
				travellers.put("middleName", "");
				travellers.put("mobileNumber", req.getMobileNumber());
				travellers.put("nationality", "India");
				travellers.put("passportExpiryDate", "");
				travellers.put("passportNo", "");
				travellers.put("paxType", "ADT");
				travellers.put("provisionState", "Karnataka");
				travellers.put("residentCountry", "IN");
				travellers.put("title", "Mr.");
				infantTravellers.put(travellers);
			} else if (req.getInfants().equals("0")) {

			} else {
				String fanme[] = req.getFirstNameinfant().split("~");

				String lname[] = req.getLastNameinfant().split("@");

				for (int i = 1; i < fanme.length; i++) {

					JSONObject travellers = new JSONObject();
					travellers.put("address1", "BTM");
					travellers.put("address2", "BTM");
					travellers.put("city", "Bangalore");
					travellers.put("countryCode", "IN");
					travellers.put("cultureCode", "String content");
					travellers.put("dateofBirth", "");
					travellers.put("emailAddress", req.getEmailAddress());
					travellers.put("firstName", fanme[i]);
					travellers.put("frequentFlierNumber", "");
					travellers.put("gender", "MALE");
					travellers.put("homePhone", "");
					travellers.put("lastName", lname[i]);
					travellers.put("meal", "VGML");
					travellers.put("middleName", "");
					travellers.put("mobileNumber", req.getMobileNumber());
					travellers.put("nationality", "India");
					travellers.put("passportExpiryDate", "");
					travellers.put("passportNo", "");
					travellers.put("paxType", "ADT");
					travellers.put("provisionState", "Karnataka");
					travellers.put("residentCountry", "IN");
					travellers.put("title", "Mr.");
					infantTravellers.put(travellers);

				}
			}

			return infantTravellers;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	// Return json is create Mobile

	public static JSONArray createMobileReturnjsonAdultraveler(MobileRoundwayFlightBookRequest req, String source) {
		JSONArray adultTravellers = new JSONArray();

		try {
			if (req.getAdults().equals("1")) {
				JSONObject travellers = new JSONObject();
				travellers.put("address1", "BTM");
				travellers.put("address2", "BTM");
				travellers.put("city", "Bangalore");
				travellers.put("countryCode", "IN");
				travellers.put("cultureCode", "String content");
				travellers.put("dateofBirth", "");
				travellers.put("emailAddress", req.getEmailAddress());
				travellers.put("firstName", req.getFirstName().replace("~", ""));
				travellers.put("frequentFlierNumber", "");
				travellers.put("gender", "MALE");
				travellers.put("homePhone", "");
				travellers.put("lastName", req.getLastName().replace("@", ""));
				travellers.put("meal", "VGML");
				travellers.put("middleName", "");
				travellers.put("mobileNumber", req.getMobileNumber());
				travellers.put("nationality", "India");
				travellers.put("passportExpiryDate", "");
				travellers.put("passportNo", "");
				travellers.put("paxType", "ADT");
				travellers.put("provisionState", "Karnataka");
				travellers.put("residentCountry", "IN");
				travellers.put("title", "Mr.");
				adultTravellers.put(travellers);
			} else if (req.getAdults().equals("0")) {

			} else {
				String fanme[] = req.getFirstName().split("~");

				String lname[] = req.getLastName().split("@");

				for (int i = 1; i < fanme.length; i++) {

					JSONObject travellers = new JSONObject();
					travellers.put("address1", "BTM");
					travellers.put("address2", "BTM");
					travellers.put("city", "Bangalore");
					travellers.put("countryCode", "IN");
					travellers.put("cultureCode", "String content");
					travellers.put("dateofBirth", "");
					travellers.put("emailAddress", req.getEmailAddress());
					travellers.put("firstName", fanme[i]);
					travellers.put("frequentFlierNumber", "");
					travellers.put("gender", "MALE");
					travellers.put("homePhone", "");
					travellers.put("lastName", lname[i]);
					travellers.put("meal", "VGML");
					travellers.put("middleName", "");
					travellers.put("mobileNumber", req.getMobileNumber());
					travellers.put("nationality", "India");
					travellers.put("passportExpiryDate", "");
					travellers.put("passportNo", "");
					travellers.put("paxType", "ADT");
					travellers.put("provisionState", "Karnataka");
					travellers.put("residentCountry", "IN");
					travellers.put("title", "Mr.");
					adultTravellers.put(travellers);

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return adultTravellers;
	}

	public static JSONArray createMobileReturnjsonChildraveler(MobileRoundwayFlightBookRequest req, String source) {
		JSONArray childTravellers = new JSONArray();

		try {
			if (req.getChilds().equals("1")) {
				JSONObject travellers = new JSONObject();
				travellers.put("address1", "BTM");
				travellers.put("address2", "BTM");
				travellers.put("city", "Bangalore");
				travellers.put("countryCode", "IN");
				travellers.put("cultureCode", "String content");
				travellers.put("dateofBirth", "");
				travellers.put("emailAddress", req.getEmailAddress());
				travellers.put("firstName", req.getFirstNamechild().replace("~", ""));
				travellers.put("frequentFlierNumber", "");
				travellers.put("gender", "MALE");
				travellers.put("homePhone", "");
				travellers.put("lastName", req.getLastNamechild().replace("@", ""));
				travellers.put("meal", "VGML");
				travellers.put("middleName", "");
				travellers.put("mobileNumber", req.getMobileNumber());
				travellers.put("nationality", "India");
				travellers.put("passportExpiryDate", "");
				travellers.put("passportNo", "");
				travellers.put("paxType", "ADT");
				travellers.put("provisionState", "Karnataka");
				travellers.put("residentCountry", "IN");
				travellers.put("title", "Mr.");
				childTravellers.put(travellers);
			} else if (req.getChilds().equals("0")) {

			} else {

				String fanme[] = req.getFirstNamechild().split("~");

				String lname[] = req.getLastNamechild().split("@");

				for (int i = 1; i < fanme.length; i++) {

					JSONObject travellers = new JSONObject();
					travellers.put("address1", "BTM");
					travellers.put("address2", "BTM");
					travellers.put("city", "Bangalore");
					travellers.put("countryCode", "IN");
					travellers.put("cultureCode", "String content");
					travellers.put("dateofBirth", "");
					travellers.put("emailAddress", req.getEmailAddress());
					travellers.put("firstName", fanme[i]);
					travellers.put("frequentFlierNumber", "");
					travellers.put("gender", "MALE");
					travellers.put("homePhone", "");
					travellers.put("lastName", lname[i]);
					travellers.put("meal", "VGML");
					travellers.put("middleName", "");
					travellers.put("mobileNumber", req.getMobileNumber());
					travellers.put("nationality", "India");
					travellers.put("passportExpiryDate", "");
					travellers.put("passportNo", "");
					travellers.put("paxType", "ADT");
					travellers.put("provisionState", "Karnataka");
					travellers.put("residentCountry", "IN");
					travellers.put("title", "Mr.");
					childTravellers.put(travellers);

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return childTravellers;
	}

	public static JSONArray createMobileReturnjsonInfantrevaleer(MobileRoundwayFlightBookRequest req, String source) {

		JSONArray infantTravellers = new JSONArray();
		try {
			if (req.getInfants().equals("1")) {
				JSONObject travellers = new JSONObject();
				travellers.put("address1", "BTM");
				travellers.put("address2", "BTM");
				travellers.put("city", "Bangalore");
				travellers.put("countryCode", "IN");
				travellers.put("cultureCode", "String content");
				travellers.put("dateofBirth", "");
				travellers.put("emailAddress", req.getEmailAddress());
				travellers.put("firstName", req.getFirstNameinfant().replace("~", ""));
				travellers.put("frequentFlierNumber", "");
				travellers.put("gender", "MALE");
				travellers.put("homePhone", "");
				travellers.put("lastName", req.getLastNameinfant().replace("@", ""));
				travellers.put("meal", "VGML");
				travellers.put("middleName", "");
				travellers.put("mobileNumber", req.getMobileNumber());
				travellers.put("nationality", "India");
				travellers.put("passportExpiryDate", "");
				travellers.put("passportNo", "");
				travellers.put("paxType", "ADT");
				travellers.put("provisionState", "Karnataka");
				travellers.put("residentCountry", "IN");
				travellers.put("title", "Mr.");
				infantTravellers.put(travellers);
			} else if (req.getInfants().equals("0")) {

			} else {
				String fanme[] = req.getFirstNameinfant().split("~");

				String lname[] = req.getLastNameinfant().split("@");

				for (int i = 1; i < fanme.length; i++) {

					JSONObject travellers = new JSONObject();
					travellers.put("address1", "BTM");
					travellers.put("address2", "BTM");
					travellers.put("city", "Bangalore");
					travellers.put("countryCode", "IN");
					travellers.put("cultureCode", "String content");
					travellers.put("dateofBirth", "");
					travellers.put("emailAddress", req.getEmailAddress());
					travellers.put("firstName", fanme[i]);
					travellers.put("frequentFlierNumber", "");
					travellers.put("gender", "MALE");
					travellers.put("homePhone", "");
					travellers.put("lastName", lname[i]);
					travellers.put("meal", "VGML");
					travellers.put("middleName", "");
					travellers.put("mobileNumber", req.getMobileNumber());
					travellers.put("nationality", "India");
					travellers.put("passportExpiryDate", "");
					travellers.put("passportNo", "");
					travellers.put("paxType", "ADT");
					travellers.put("provisionState", "Karnataka");
					travellers.put("residentCountry", "IN");
					travellers.put("title", "Mr.");
					infantTravellers.put(travellers);

				}
			}

			return infantTravellers;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static JSONObject createjsonpricerecheckApi(FlightPriceCheckRequest req, String source, String destination,
			HttpSession session) {
		JSONObject payload = new JSONObject();
		try {

			JSONObject payload1 = new JSONObject();
			JSONObject obj = new JSONObject(source);
			JSONArray fare1 = null;
			JSONArray paxFares1 = null;
			JSONArray fares1 = null;
			JSONArray journeys = obj.getJSONObject("details").getJSONArray("journeys");
			//			journeys.get(req.getobjectNo);
			JSONArray legsjsonarray = new JSONArray();
			//			for (int x = 0; x < journeys.length(); x++) {

			JSONArray segments = journeys.getJSONObject(0).getJSONArray("segments");

			for (int j = 0; j < segments.length(); j++) {
				JSONObject segmentsObj=segments.getJSONObject(Integer.parseInt(req.getColNo()));

				JSONArray bonds = segmentsObj.getJSONArray("bonds");
				for (int k = 0; k < bonds.length(); k++) {

					JSONArray legs = bonds.getJSONObject(k).getJSONArray("legs");
					System.err.println("##############################" + req.getFlightNumber() + "ddd");
					String flightnumber = legs.getJSONObject(k).getString("flightNumber");
					//						if (flightnumber.equals(req.getFlightNumber())) {
					fare1 = segmentsObj.getJSONArray("fare");
					paxFares1 = fare1.getJSONObject(0).getJSONArray("paxFares");
					fares1 = paxFares1.getJSONObject(0).getJSONArray("fares");

					System.err.println(fare1);
					System.err.println(paxFares1);
					System.err.println(fares1);
					legsjsonarray = bonds.getJSONObject(k).getJSONArray("legs");
					req.setAirlineName("" + legs.getJSONObject(k).getString("airlineName"));
					req.setArrivalTime("" + legs.getJSONObject(k).getString("arrivalTime"));
					req.setCabin("" + legs.getJSONObject(k).getString("cabin"));
					req.setDepartureTime("" + legs.getJSONObject(k).getString("departureTime"));
					req.setDestination("" + legs.getJSONObject(k).getString("destination"));
					req.setDuration("" + legs.getJSONObject(k).getString("duration"));
					req.setBoundType("" + legs.getJSONObject(k).getString("boundType"));
					req.setFlightNumber("" + legs.getJSONObject(k).getString("flightNumber"));
					req.setOrigin("" + legs.getJSONObject(k).getString("origin"));
					req.setAdults("" + session.getAttribute("adults"));
					req.setChilds("" + session.getAttribute("childs"));
					req.setInfants("" + session.getAttribute("infants"));
					req.setBeginDate("" + session.getAttribute("beginDate"));
					req.setFlightName("" + legs.getJSONObject(k).getString("airlineName"));
					req.setArrivalDate("" + legs.getJSONObject(k).getString("arrivalDate"));
					req.setDepartureDate("" + legs.getJSONObject(k).getString("departureDate"));
					req.setDepartureTerminal("" + legs.getJSONObject(k).getString("departureTerminal"));
					req.setArrivalTerminal("" + legs.getJSONObject(k).getString("arrivalTerminal"));
					req.setCapacity("" + legs.getJSONObject(k).getString("capacity"));
					req.setCarrierCode("" + legs.getJSONObject(k).getString("carrierCode"));
					req.setCurrencyCode("" + legs.getJSONObject(k).getString("currencyCode"));
					req.setBaggageUnit("" + legs.getJSONObject(k).getString("baggageUnit"));
					req.setBaggageWeight("" + legs.getJSONObject(k).getString("baggageWeight"));
					req.setFareClassOfService("" + legs.getJSONObject(k).getString("fareClassOfService"));
					req.setSold("" + legs.getJSONObject(k).getString("sold"));
					req.setStatus("" + legs.getJSONObject(k).getString("status"));
					req.setItineraryKey("" + segmentsObj.getString("itineraryKey"));
					req.setSearchId("" + segmentsObj.getString("searchId"));
					req.setEngineID("" + segmentsObj.getString("engineID"));
					req.setFareRule("" + segmentsObj.getString("fareRule"));
					req.setJourneyTime("" + legs.getJSONObject(k).getString("duration"));
					req.setJourneyIndex("" + segmentsObj.getString("journeyIndex"));

					JSONArray fare = segmentsObj.getJSONArray("fare");
					for (int l = 0; l < fare.length(); l++) {
						JSONArray paxFares = fare.getJSONObject(l).getJSONArray("paxFares");
						JSONArray fares = paxFares.getJSONObject(l).getJSONArray("fares");

						req.setTotalFare("" + paxFares.getJSONObject(l).getString("totalFare"));
						req.setTotalTax("" + paxFares.getJSONObject(l).getString("totalTax"));
						req.setTotalFareWithOutMarkUp(
								"" + fare.getJSONObject(l).getString("totalFareWithOutMarkUp"));
						req.setTotalTaxWithOutMarkUp(
								"" + fare.getJSONObject(l).getString("totalTaxWithOutMarkUp"));
						req.setAmount("" + fares.getJSONObject(l).getString("amount"));

						req.setBasicFare("" + fare.getJSONObject(l).getString("basicFare"));
						req.setCancelPenalty("" + paxFares.getJSONObject(l).getString("cancelPenalty"));
						req.setChangePenalty("" + paxFares.getJSONObject(l).getString("changePenalty"));
						req.setTransactionAmount("" + paxFares.getJSONObject(l).getString("transactionAmount"));
						req.setBaseTransactionAmount(
								"" + paxFares.getJSONObject(l).getString("baseTransactionAmount"));
						req.setTraceId("AYTM00011111111110002");

						req.setExchangeRate("" + fare.getJSONObject(l).getString("exchangeRate"));

						req.setMarkUP("" + paxFares.getJSONObject(l).getString("markUP"));
						req.setPaxType("" + paxFares.getJSONObject(l).getString("paxType"));

					}

					//						}
				}

				//				}
			}

			JSONObject legsjsonobjet = new JSONObject();

			JSONObject bondsjsonobjet = new JSONObject();
			JSONArray bondsjsonarray = new JSONArray();

			JSONObject ssrDetailsjsonobjet = new JSONObject();
			JSONArray ssrDetailsjsonarray = new JSONArray();

			JSONArray engineIDList = new JSONArray();

			JSONObject bookSegmentsjsonobjet = new JSONObject();
			JSONArray bookSegmentsjsonarray = new JSONArray();

			JSONObject adultTravellersnew = new JSONObject();
			JSONArray flightSearchDetailsaray = new JSONArray();
			JSONObject flightSearchDetailsjsonobject = new JSONObject();
			JSONObject paymentDetails = new JSONObject();
			ArrayList<String> objhasmap = new ArrayList<>();

			ArrayList<String> objhasmap1 = new ArrayList<>();

			JSONObject faresjsonobjet = new JSONObject();
			JSONObject paxFaresjsonobjet = new JSONObject();
			JSONArray paxFaresjsonarray = new JSONArray();

			paxFaresjsonobjet.put("baggageUnit", req.getBaggageUnit());
			paxFaresjsonobjet.put("baggageWeight", req.getBaggageWeight());
			paxFaresjsonobjet.put("baseTransactionAmount", 0);
			paxFaresjsonobjet.put("basicFare", req.getBasicFare());
			paxFaresjsonobjet.put("cancelPenalty", req.getCancelPenalty());
			paxFaresjsonobjet.put("changePenalty", req.getChangePenalty());
			paxFaresjsonobjet.put("equivCurrencyCode", "");

			paxFaresjsonobjet.put("fareBasisCode", "");
			paxFaresjsonobjet.put("fareInfoKey", "");
			paxFaresjsonobjet.put("fareInfoValue", "");
			paxFaresjsonobjet.put("markUP", 0);
			paxFaresjsonobjet.put("paxType", "ADT");
			paxFaresjsonobjet.put("refundable", true);
			paxFaresjsonobjet.put("totalFare", req.getTotalFare());
			paxFaresjsonobjet.put("totalTax", req.getTotalTax());
			paxFaresjsonobjet.put("transactionAmount", 0);
			paxFaresjsonobjet.put("bookFares", fares1);

			paxFaresjsonarray.put(paxFaresjsonobjet);

			faresjsonobjet.put("basicFare", req.getBasicFare());
			faresjsonobjet.put("exchangeRate", 0);

			faresjsonobjet.put("totalFareWithOutMarkUp", req.getTotalFareWithOutMarkUp());
			faresjsonobjet.put("totalTaxWithOutMarkUp", req.getTotalTaxWithOutMarkUp());
			faresjsonobjet.put("paxFares", paxFaresjsonarray);

			objhasmap.add("" + req.getEngineID());
			payload.put("clientIp", Inet4Address.getLocalHost().getHostAddress());
			payload.put("cllientKey", MdexConstants.getMdexCredentials().getCode());
			payload.put("clientToken", MdexConstants.getMdexCredentials().getStatus());
			payload.put("clientApiName", "Flight Search");

			bondsjsonobjet.put("boundType", "");
			bondsjsonobjet.put("baggageFare", false);
			bondsjsonobjet.put("ssrFare", false);
			bondsjsonobjet.put("itineraryKey", req.getItineraryKey());
			bondsjsonobjet.put("journeyTime", req.getDuration());

			bondsjsonobjet.put("addOnDetail", "");

			JSONArray val = CreateJsonRequestFlight.createlegsarray(legsjsonarray);
			bondsjsonobjet.put("legs", val);

			bondsjsonarray.put(bondsjsonobjet);

			bookSegmentsjsonobjet.put("bonds", bondsjsonarray);

			bookSegmentsjsonobjet.put("fareRule", req.getFareRule());
			bookSegmentsjsonobjet.put("baggageFare", false);
			bookSegmentsjsonobjet.put("cache", false);
			bookSegmentsjsonobjet.put("holdBooking", false);
			bookSegmentsjsonobjet.put("international", false);
			bookSegmentsjsonobjet.put("roundTrip", false);
			bookSegmentsjsonobjet.put("special", false);
			bookSegmentsjsonobjet.put("specialId", false);
			bookSegmentsjsonobjet.put("itineraryKey", req.getItineraryKey());
			bookSegmentsjsonobjet.put("journeyIndex", 0);
			bookSegmentsjsonobjet.put("nearByAirport", false);

			bookSegmentsjsonobjet.put("searchId", req.getSearchId());

			bookSegmentsjsonobjet.put("engineID", req.getEngineID());
			String df = "" + fare1;

			String vv = df.replace("fares", "bookFares");
			JSONArray bn = new JSONArray(vv);
			bookSegmentsjsonobjet.put("fares", bn);

			bookSegmentsjsonarray.put(bookSegmentsjsonobjet);
			payload.put("segments", bookSegmentsjsonarray);

			if (req.getOnewayflight().equals("Firstfligt")) {

				flightSearchDetailsjsonobject.put("endDate", "" + session.getAttribute("endDate"));
				flightSearchDetailsjsonobject.put("beginDate", "" + session.getAttribute("beginDate"));
				flightSearchDetailsjsonobject.put("destination", "" + session.getAttribute("destination"));
				flightSearchDetailsjsonobject.put("origin", "" + session.getAttribute("source"));
			}
			if (req.getOnewayflight().equals("Secondflight")) {
				flightSearchDetailsjsonobject.put("beginDate", "" + session.getAttribute("beginDate"));
				flightSearchDetailsjsonobject.put("destination", "" + session.getAttribute("source"));
				flightSearchDetailsjsonobject.put("origin", "" + session.getAttribute("destination"));
				flightSearchDetailsjsonobject.put("endDate", "" + session.getAttribute("endDate"));

			}

			flightSearchDetailsaray.put(flightSearchDetailsjsonobject);
			payload1.put("flightSearchDetails", flightSearchDetailsaray);

			payload1.put("engineIDs", objhasmap);

			payload1.put("traceId", "AYTM00011111111110001");
			payload1.put("adults", req.getAdults());
			payload1.put("cabin", req.getCabin());
			payload1.put("childs", req.getChilds());
			payload1.put("infants", req.getInfants());
			payload1.put("tripType", "" + session.getAttribute("triptype"));

			payload.put("flightAvailability", payload1);

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}
		return payload;
	}

	public static JSONArray createlegsarray(JSONArray obj) {
		JSONArray obj1 = new JSONArray();
		for (int i = 0; i < obj.length(); i++) {
			try {

				System.err.println(obj);
				JSONObject payload1 = new JSONObject();
				payload1.put("aircraftCode", "null");
				payload1.put("aircraftType", "null");
				payload1.put("airlineName", obj.getJSONObject(i).getString("airlineName"));
				payload1.put("arrivalDate", obj.getJSONObject(i).getString("arrivalDate"));
				payload1.put("arrivalTerminal", "");
				payload1.put("arrivalTime", obj.getJSONObject(i).getString("arrivalTime"));
				payload1.put("availableSeat", "null");
				payload1.put("baggageUnit", obj.getJSONObject(i).getString("baggageUnit"));
				payload1.put("baggageWeight", obj.getJSONObject(i).getString("baggageWeight"));
				payload1.put("boundTypes", obj.getJSONObject(i).getString("boundType"));
				payload1.put("cabin", "Economy");

				payload1.put("capacity", obj.getJSONObject(i).getString("capacity"));
				payload1.put("carrierCode", obj.getJSONObject(i).getString("carrierCode"));

				payload1.put("currencyCode", obj.getJSONObject(i).getString("currencyCode"));
				payload1.put("departureDate", obj.getJSONObject(i).getString("departureDate"));
				payload1.put("departureTerminal", obj.getJSONObject(i).getString("departureTerminal"));
				payload1.put("departureTime", obj.getJSONObject(i).getString("departureTime"));
				payload1.put("destination", obj.getJSONObject(i).getString("destination"));
				payload1.put("duration", obj.getJSONObject(i).getString("duration"));
				payload1.put("fareBasisCode", obj.getJSONObject(i).getString("fareBasisCode"));
				payload1.put("fareClassOfService", obj.getJSONObject(i).getString("fareClassOfService"));
				payload1.put("flightDesignator", obj.getJSONObject(i).getString("flightDesignator"));
				payload1.put("flightDetailRefKey", obj.getJSONObject(i).getString("flightDetailRefKey"));

				payload1.put("flightNumber", obj.getJSONObject(i).getString("flightNumber"));
				payload1.put("origin", obj.getJSONObject(i).getString("origin"));
				payload1.put("sold", 0);
				payload1.put("status", obj.getJSONObject(i).getString("status"));
				payload1.put("providerCode", obj.getJSONObject(i).getString("providerCode"));
				payload1.put("group", obj.getJSONObject(i).getString("group"));

				obj1.put(payload1);
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(e);
			}
		}

		return obj1;
	}

	/* Create JSON for connecting flight price re-check */

	public static JSONObject abc(FlightPriceCheckRequest req) {

		JSONObject payload = new JSONObject();
		try {

			JSONObject payload1 = new JSONObject();
			JSONArray fare1 = null;
			JSONArray paxFares1 = null;
			JSONArray fares1 = null;

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}
		return payload;

	}

	// Json for All

	public static JSONObject createMobilejsonAllraveler(MobileFlightBookRequest req) {
		JSONObject adultTravellersnew = new JSONObject();
		JSONArray adultTravellers = new JSONArray();
		JSONArray childTravellers = new JSONArray();
		JSONArray infantTravellers = new JSONArray();
		try {

			List<TravellerFlightDetails> travellerDetails = req.getTravellerDetails();

			for (int i = 0; i < travellerDetails.size(); i++) {

				if (travellerDetails.get(i).getType() != null) {
					if (travellerDetails.get(i).getType().equalsIgnoreCase("Adult")) {

						JSONObject travellers = new JSONObject();
						travellers.put("address1", "BTM");
						travellers.put("address2", "BTM");
						travellers.put("city", "Bangalore");
						travellers.put("countryCode", "IN");
						travellers.put("cultureCode", "String content");
						travellers.put("dateofBirth", "");
						travellers.put("emailAddress", req.getEmailAddress());
						travellers.put("firstName", req.getFirstName().replace("~", ""));
						travellers.put("frequentFlierNumber", "");
						travellers.put("gender", "MALE");
						travellers.put("homePhone", "");
						travellers.put("lastName", req.getLastName().replace("@", ""));
						travellers.put("meal", "VGML");
						travellers.put("middleName", "");
						travellers.put("mobileNumber", req.getMobileNumber());
						travellers.put("nationality", "India");
						travellers.put("passportExpiryDate", "");
						travellers.put("passportNo", "");
						travellers.put("paxType", "ADT");
						travellers.put("provisionState", "Karnataka");
						travellers.put("residentCountry", "IN");
						travellers.put("title", "Mr.");
						adultTravellers.put(travellers);

					}

					if (travellerDetails.get(i).getType().equalsIgnoreCase("Child")) {

						JSONObject travellers = new JSONObject();
						travellers.put("address1", "BTM");
						travellers.put("address2", "BTM");
						travellers.put("city", "Bangalore");
						travellers.put("countryCode", "IN");
						travellers.put("cultureCode", "String content");
						travellers.put("dateofBirth", "");
						travellers.put("emailAddress", req.getEmailAddress());
						travellers.put("firstName", req.getFirstNamechild().replace("~", ""));
						travellers.put("frequentFlierNumber", "");
						travellers.put("gender", "MALE");
						travellers.put("homePhone", "");
						travellers.put("lastName", req.getLastNamechild().replace("@", ""));
						travellers.put("meal", "VGML");
						travellers.put("middleName", "");
						travellers.put("mobileNumber", req.getMobileNumber());
						travellers.put("nationality", "India");
						travellers.put("passportExpiryDate", "");
						travellers.put("passportNo", "");
						travellers.put("paxType", "ADT");
						travellers.put("provisionState", "Karnataka");
						travellers.put("residentCountry", "IN");
						travellers.put("title", "Mr.");
						childTravellers.put(travellers);
					}

					if (travellerDetails.get(i).getType().equalsIgnoreCase("Infant")) {

						JSONObject travellers = new JSONObject();
						travellers.put("address1", "BTM");
						travellers.put("address2", "BTM");
						travellers.put("city", "Bangalore");
						travellers.put("countryCode", "IN");
						travellers.put("cultureCode", "String content");
						travellers.put("dateofBirth", "");
						travellers.put("emailAddress", req.getEmailAddress());
						travellers.put("firstName", req.getFirstNameinfant().replace("~", ""));
						travellers.put("frequentFlierNumber", "");
						travellers.put("gender", "MALE");
						travellers.put("homePhone", "");
						travellers.put("lastName", req.getLastNameinfant().replace("@", ""));
						travellers.put("meal", "VGML");
						travellers.put("middleName", "");
						travellers.put("mobileNumber", req.getMobileNumber());
						travellers.put("nationality", "India");
						travellers.put("passportExpiryDate", "");
						travellers.put("passportNo", "");
						travellers.put("paxType", "ADT");
						travellers.put("provisionState", "Karnataka");
						travellers.put("residentCountry", "IN");
						travellers.put("title", "Mr.");
						infantTravellers.put(travellers);
					}
				}
			}

			adultTravellersnew.put("adultTravellers", adultTravellers);
			adultTravellersnew.put("childTravellers", childTravellers);
			adultTravellersnew.put("infantTravellers", infantTravellers);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return adultTravellersnew;
	}

	public static JSONObject createjsonForMobileConecting(MobileFlightBookRequest req) {

		try {

			JSONObject payload = new JSONObject();

			JSONObject legsjsonobjet = new JSONObject();
			JSONArray legsjsonarray = new JSONArray();
			org.json.simple.JSONArray bookfarejsonArray = CreateJsonRequestFlight.createBookFareJsonArray(req);

			JSONObject bondsjsonobjet = new JSONObject();
			JSONArray bondsjsonarray = new JSONArray();

			JSONObject ssrDetailsjsonobjet = new JSONObject();
			JSONArray ssrDetailsjsonarray = new JSONArray();

			JSONArray engineIDList = new JSONArray();

			JSONObject bookSegmentsjsonobjet = new JSONObject();
			JSONArray bookSegmentsjsonarray = new JSONArray();

			JSONObject adultTravellersnew = new JSONObject();
			JSONArray flightSearchDetailsaray = new JSONArray();
			JSONObject flightSearchDetailsjsonobject = new JSONObject();
			JSONObject paymentDetails = new JSONObject();
			ArrayList<String> objhasmap = new ArrayList<>();

			ArrayList<String> objhasmap1 = new ArrayList<>();

			JSONObject faresjsonobjet = new JSONObject();
			JSONObject paxFaresjsonobjet = new JSONObject();
			JSONArray paxFaresjsonarray = new JSONArray();

			paxFaresjsonobjet.put("baggageUnit", req.getBaggageUnit());
			paxFaresjsonobjet.put("baggageWeight", req.getBaggageWeight());
			paxFaresjsonobjet.put("baseTransactionAmount", 0);
			paxFaresjsonobjet.put("basicFare", req.getBasicFare());
			paxFaresjsonobjet.put("cancelPenalty", req.getCancelPenalty());
			paxFaresjsonobjet.put("changePenalty", req.getChangePenalty());
			paxFaresjsonobjet.put("equivCurrencyCode", "");

			paxFaresjsonobjet.put("fareBasisCode", "");
			paxFaresjsonobjet.put("fareInfoKey", "");
			paxFaresjsonobjet.put("fareInfoValue", "");
			paxFaresjsonobjet.put("markUP", 0);
			paxFaresjsonobjet.put("paxType", "ADT");
			paxFaresjsonobjet.put("refundable", true);
			paxFaresjsonobjet.put("totalFare", req.getTotalFare());
			paxFaresjsonobjet.put("totalTax", req.getTotalTax());
			paxFaresjsonobjet.put("transactionAmount", 0);
			paxFaresjsonobjet.put("bookFares", bookfarejsonArray);
			paxFaresjsonarray.put(paxFaresjsonobjet);

			faresjsonobjet.put("basicFare", req.getBasicFare());
			faresjsonobjet.put("exchangeRate", 0);

			faresjsonobjet.put("totalFareWithOutMarkUp", req.getTotalFareWithOutMarkUp());
			faresjsonobjet.put("totalTaxWithOutMarkUp", req.getTotalTaxWithOutMarkUp());
			faresjsonobjet.put("paxFares", paxFaresjsonarray);

			objhasmap.add("" + EngineID.Indigo);
			payload.put("clientIp", Inet4Address.getLocalHost().getHostAddress());
			payload.put("cllientKey", MdexConstants.getMdexCredentials().getCode());
			payload.put("clientToken", MdexConstants.getMdexCredentials().getStatus());
			payload.put("clientApiName", "Flight Booking");
			payload.put("spKey", req.getAirlineName() + "IN");

			bondsjsonobjet.put("boundType", "");
			bondsjsonobjet.put("baggageFare", false);
			bondsjsonobjet.put("ssrFare", false);
			bondsjsonobjet.put("itineraryKey", req.getItineraryKey());
			bondsjsonobjet.put("journeyTime", req.getDuration());

			bondsjsonobjet.put("addOnDetail", "");

			legsjsonobjet.put("aircraftCode", "");
			legsjsonobjet.put("aircraftType", "");
			legsjsonobjet.put("airlineName", req.getAirlineName());
			legsjsonobjet.put("amount", req.getAmount());
			legsjsonobjet.put("arrivalDate", req.getArrivalDate());
			legsjsonobjet.put("arrivalTerminal", "");
			legsjsonobjet.put("arrivalTime", req.getArrivalTime());
			legsjsonobjet.put("availableSeat", "");
			legsjsonobjet.put("baggageUnit", req.getBaggageUnit());
			legsjsonobjet.put("baggageWeight", req.getBaggageWeight());
			legsjsonobjet.put("boundTypes", "");
			legsjsonobjet.put("cabin", "Economy");
			legsjsonobjet.put("cabinClasses", objhasmap1);
			legsjsonobjet.put("capacity", "0");
			legsjsonobjet.put("carrierCode", req.getAirlineName());
			legsjsonobjet.put("currencyCode", "INR");
			legsjsonobjet.put("departureDate", req.getDepartureDate());
			legsjsonobjet.put("departureTerminal", req.getDepartureTerminal());
			legsjsonobjet.put("departureTime", req.getDepartureTime());
			legsjsonobjet.put("destination", req.getDestination());
			legsjsonobjet.put("duration", req.getDuration());
			legsjsonobjet.put("fareBasisCode", req.getFareBasisCode());
			legsjsonobjet.put("fareClassOfService", req.getFareClassOfService());
			legsjsonobjet.put("flightDesignator", "");
			legsjsonobjet.put("flightDetailRefKey", "");
			legsjsonobjet.put("flightName", "Indigo");
			legsjsonobjet.put("flightNumber", req.getFlightNumber());
			legsjsonobjet.put("group", "");
			legsjsonobjet.put("connecting", false);
			legsjsonobjet.put("numberOfStops", "");
			legsjsonobjet.put("origin", req.getOrigin());
			legsjsonobjet.put("providerCode", "");
			legsjsonobjet.put("remarks", "");
			ssrDetailsjsonarray.put(ssrDetailsjsonobjet);
			legsjsonobjet.put("sold", 0);
			legsjsonobjet.put("status", "Normal");
			legsjsonobjet.put("ssrDetails", ssrDetailsjsonarray);
			legsjsonarray.put(legsjsonobjet);

			bondsjsonobjet.put("legs", legsjsonarray);

			bondsjsonarray.put(bondsjsonobjet);

			bookSegmentsjsonobjet.put("bonds", bondsjsonarray);
			bookSegmentsjsonobjet.put("deeplink", "");
			bookSegmentsjsonobjet.put("fareIndicator", 0);
			bookSegmentsjsonobjet.put("fareRule", req.getFareRule());
			bookSegmentsjsonobjet.put("baggageFare", false);
			bookSegmentsjsonobjet.put("cache", false);
			bookSegmentsjsonobjet.put("holdBooking", false);
			bookSegmentsjsonobjet.put("international", false);
			bookSegmentsjsonobjet.put("roundTrip", false);
			bookSegmentsjsonobjet.put("special", false);
			bookSegmentsjsonobjet.put("specialId", false);
			bookSegmentsjsonobjet.put("itineraryKey", req.getItineraryKey());
			bookSegmentsjsonobjet.put("journeyIndex", 0);
			bookSegmentsjsonobjet.put("memoryCreationTime", "/Date(1499158249483+0530)/");
			bookSegmentsjsonobjet.put("nearByAirport", false);
			bookSegmentsjsonobjet.put("remark", "");
			bookSegmentsjsonobjet.put("searchId", req.getSearchId());

			bookSegmentsjsonobjet.put("engineID", "Indigo");
			bookSegmentsjsonobjet.put("fares", faresjsonobjet);

			bookSegmentsjsonarray.put(bookSegmentsjsonobjet);
			payload.put("bookSegments", bookSegmentsjsonarray);

			flightSearchDetailsjsonobject.put("beginDate", req.getBeginDate());
			flightSearchDetailsjsonobject.put("destination", req.getDestination());
			flightSearchDetailsjsonobject.put("endDate", req.getBeginDate());
			flightSearchDetailsjsonobject.put("origin", req.getOrigin());
			flightSearchDetailsaray.put(flightSearchDetailsjsonobject);
			payload.put("flightSearchDetails", flightSearchDetailsaray);

			paymentDetails.put("bookingAmount", req.getTotalFareWithOutMarkUp());
			paymentDetails.put("bookingCurrencyCode", "INR");
			payload.put("paymentDetails", paymentDetails);

			payload.put("engineIDList", objhasmap);

			adultTravellersnew = createMobilejsonAllraveler(req);

			payload.put("travellers", adultTravellersnew);
			payload.put("visatype", "Employee Visa");
			payload.put("traceId", "AYTM00011111111110001");
			payload.put("transactionId", req.getTransactionId());
			payload.put("androidBooking", true);
			payload.put("domestic", true);
			payload.put("engineID", objhasmap.get(0));
			return payload;
		} catch (Exception e) {
			e.printStackTrace();

			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public static org.json.JSONObject createbondslegsarray(org.json.JSONArray obj3, org.json.JSONArray obj12)
			throws Exception {
		org.json.JSONArray obj1 = new org.json.JSONArray();
		org.json.JSONObject objmain = new org.json.JSONObject();
		org.json.JSONArray obj2 = new org.json.JSONArray();
		org.json.JSONArray obj4 = new org.json.JSONArray();
		org.json.JSONObject legs = new org.json.JSONObject();
		org.json.JSONObject legs2 = new org.json.JSONObject();
		org.json.JSONArray legsroundwayinternational = obj3;

		for (int j = 0; j < legsroundwayinternational.length(); j++) {
			try {
				if (j == 0) {

					System.err.println(legsroundwayinternational.length());
					org.json.JSONArray obj = legsroundwayinternational.getJSONObject(j).getJSONArray("legs");
					for (int i = 0; i < obj.length(); i++) {

						legs.put("boundType", legsroundwayinternational.getJSONObject(j).getString("boundType"));
						legs.put("baggageFare", false);
						legs.put("ssrFare", false);
						legs.put("itineraryKey", legsroundwayinternational.getJSONObject(j).getString("itineraryKey"));
						legs.put("journeyTime", legsroundwayinternational.getJSONObject(j).getString("journeyTime"));

						org.json.simple.JSONObject payload1 = new org.json.simple.JSONObject();
						payload1.put("aircraftCode", "null");
						payload1.put("aircraftType", "null");
						payload1.put("airlineName", obj.getJSONObject(i).getString("airlineName"));
						payload1.put("arrivalDate", obj.getJSONObject(i).getString("arrivalDate"));
						payload1.put("arrivalTerminal", "");
						payload1.put("arrivalTime", obj.getJSONObject(i).getString("arrivalTime"));
						payload1.put("availableSeat", "null");
						payload1.put("baggageUnit", obj.getJSONObject(i).getString("baggageUnit"));
						payload1.put("baggageWeight", obj.getJSONObject(i).getString("baggageWeight"));
						payload1.put("boundTypes", obj.getJSONObject(i).getString("boundType"));
						payload1.put("cabin", "Economy");

						payload1.put("capacity", obj.getJSONObject(i).getString("capacity"));
						payload1.put("carrierCode", obj.getJSONObject(i).getString("carrierCode"));

						payload1.put("currencyCode", obj.getJSONObject(i).getString("currencyCode"));
						payload1.put("departureDate", obj.getJSONObject(i).getString("departureDate"));
						payload1.put("departureTerminal", obj.getJSONObject(i).getString("departureTerminal"));
						payload1.put("departureTime", obj.getJSONObject(i).getString("departureTime"));
						payload1.put("destination", obj.getJSONObject(i).getString("destination"));
						payload1.put("duration", obj.getJSONObject(i).getString("duration"));
						payload1.put("fareBasisCode", obj.getJSONObject(i).getString("fareBasisCode"));
						payload1.put("fareClassOfService", obj.getJSONObject(i).getString("fareClassOfService"));
						payload1.put("flightDesignator", obj.getJSONObject(i).getString("flightDesignator"));
						payload1.put("flightDetailRefKey", obj.getJSONObject(i).getString("flightDetailRefKey"));

						payload1.put("flightNumber", obj.getJSONObject(i).getString("flightNumber"));
						payload1.put("origin", obj.getJSONObject(i).getString("origin"));
						payload1.put("sold", 0);
						payload1.put("status", obj.getJSONObject(i).getString("status"));
						payload1.put("providerCode", obj.getJSONObject(i).getString("providerCode"));
						payload1.put("group", obj.getJSONObject(i).getString("group"));
						StringWriter v = new StringWriter();
						v.write(payload1.toString());
						payload1.writeJSONString(v);
						obj1.put(payload1);

					}
				} else {

					System.err.println(legsroundwayinternational.length());
					org.json.JSONArray obj = legsroundwayinternational.getJSONObject(j).getJSONArray("legs");
					for (int i = 0; i < obj.length(); i++) {
						System.err.println(obj.length());
						legs2.put("boundType", legsroundwayinternational.getJSONObject(j).getString("boundType"));
						legs2.put("baggageFare", false);
						legs2.put("ssrFare", false);
						legs2.put("itineraryKey", legsroundwayinternational.getJSONObject(j).getString("itineraryKey"));
						legs2.put("journeyTime", legsroundwayinternational.getJSONObject(j).getString("journeyTime"));

						org.json.simple.JSONObject payload1 = new org.json.simple.JSONObject();
						payload1.put("aircraftCode", "null");
						payload1.put("aircraftType", "null");
						payload1.put("airlineName", obj.getJSONObject(i).getString("airlineName"));
						payload1.put("arrivalDate", obj.getJSONObject(i).getString("arrivalDate"));
						payload1.put("arrivalTerminal", "");
						payload1.put("arrivalTime", obj.getJSONObject(i).getString("arrivalTime"));
						payload1.put("availableSeat", "null");
						payload1.put("baggageUnit", obj.getJSONObject(i).getString("baggageUnit"));
						payload1.put("baggageWeight", obj.getJSONObject(i).getString("baggageWeight"));
						payload1.put("boundTypes", obj.getJSONObject(i).getString("boundType"));
						payload1.put("cabin", "Economy");

						payload1.put("capacity", obj.getJSONObject(i).getString("capacity"));
						payload1.put("carrierCode", obj.getJSONObject(i).getString("carrierCode"));

						payload1.put("currencyCode", obj.getJSONObject(i).getString("currencyCode"));
						payload1.put("departureDate", obj.getJSONObject(i).getString("departureDate"));
						payload1.put("departureTerminal", obj.getJSONObject(i).getString("departureTerminal"));
						payload1.put("departureTime", obj.getJSONObject(i).getString("departureTime"));
						payload1.put("destination", obj.getJSONObject(i).getString("destination"));
						payload1.put("duration", obj.getJSONObject(i).getString("duration"));
						payload1.put("fareBasisCode", obj.getJSONObject(i).getString("fareBasisCode"));
						payload1.put("fareClassOfService", obj.getJSONObject(i).getString("fareClassOfService"));
						payload1.put("flightDesignator", obj.getJSONObject(i).getString("flightDesignator"));
						payload1.put("flightDetailRefKey", obj.getJSONObject(i).getString("flightDetailRefKey"));

						payload1.put("flightNumber", obj.getJSONObject(i).getString("flightNumber"));
						payload1.put("origin", obj.getJSONObject(i).getString("origin"));
						payload1.put("sold", 0);
						payload1.put("status", obj.getJSONObject(i).getString("status"));
						payload1.put("providerCode", obj.getJSONObject(i).getString("providerCode"));
						payload1.put("group", obj.getJSONObject(i).getString("group"));

						StringWriter v = new StringWriter();
						v.write(payload1.toString());
						payload1.writeJSONString(v);
						obj2.put(payload1);
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(e);
			}
		}
		legs.put("legs", obj1);
		legs2.put("legs", obj2);
		obj4.put(legs);
		obj4.put(legs2);
		objmain.put("bonds", obj4);
		objmain.put("fares", obj12);
		// System.err.println("###" + legs2);
		System.err.println("###" + obj4);
		return objmain;
	}

	@SuppressWarnings("unchecked")
	public static org.json.JSONObject createjsonBookApiinternational(org.json.JSONArray obj3, org.json.JSONArray obj12)
			throws Exception {
		org.json.JSONArray obj1 = new org.json.JSONArray();
		org.json.JSONObject objmain = new org.json.JSONObject();
		org.json.JSONArray obj2 = new org.json.JSONArray();
		org.json.JSONArray obj4 = new org.json.JSONArray();
		org.json.JSONObject legs = new org.json.JSONObject();
		org.json.JSONObject legs2 = new org.json.JSONObject();
		org.json.JSONArray legsroundwayinternational = obj3;

		for (int j = 0; j < legsroundwayinternational.length(); j++) {
			try {
				if (j == 0) {

					org.json.JSONArray obj = legsroundwayinternational.getJSONObject(j).getJSONArray("legs");
					for (int i = 0; i < obj.length(); i++) {

						legs.put("boundType", legsroundwayinternational.getJSONObject(j).getString("boundType"));
						legs.put("baggageFare", false);
						legs.put("ssrFare", false);
						legs.put("itineraryKey", legsroundwayinternational.getJSONObject(j).getString("itineraryKey"));
						legs.put("journeyTime", legsroundwayinternational.getJSONObject(j).getString("journeyTime"));

						org.json.simple.JSONObject payload1 = new org.json.simple.JSONObject();
						payload1.put("aircraftCode", "null");
						payload1.put("aircraftType", "null");
						payload1.put("airlineName", obj.getJSONObject(i).getString("airlineName"));
						payload1.put("arrivalDate", obj.getJSONObject(i).getString("arrivalDate"));
						payload1.put("arrivalTerminal", "");
						payload1.put("arrivalTime", obj.getJSONObject(i).getString("arrivalTime"));
						payload1.put("availableSeat", "null");
						payload1.put("baggageUnit", obj.getJSONObject(i).getString("baggageUnit"));
						payload1.put("baggageWeight", obj.getJSONObject(i).getString("baggageWeight"));
						payload1.put("boundTypes", obj.getJSONObject(i).getString("boundType"));
						payload1.put("cabin", "Economy");

						payload1.put("capacity", obj.getJSONObject(i).getString("capacity"));
						payload1.put("carrierCode", obj.getJSONObject(i).getString("carrierCode"));

						payload1.put("currencyCode", obj.getJSONObject(i).getString("currencyCode"));
						payload1.put("departureDate", obj.getJSONObject(i).getString("departureDate"));
						payload1.put("departureTerminal", obj.getJSONObject(i).getString("departureTerminal"));
						payload1.put("departureTime", obj.getJSONObject(i).getString("departureTime"));
						payload1.put("destination", obj.getJSONObject(i).getString("destination"));
						payload1.put("duration", obj.getJSONObject(i).getString("duration"));
						payload1.put("fareBasisCode", obj.getJSONObject(i).getString("fareBasisCode"));
						payload1.put("fareClassOfService", obj.getJSONObject(i).getString("fareClassOfService"));
						payload1.put("flightDesignator", obj.getJSONObject(i).getString("flightDesignator"));
						payload1.put("flightDetailRefKey", obj.getJSONObject(i).getString("flightDetailRefKey"));

						payload1.put("flightNumber", obj.getJSONObject(i).getString("flightNumber"));
						payload1.put("origin", obj.getJSONObject(i).getString("origin"));
						payload1.put("sold", 0);
						payload1.put("status", obj.getJSONObject(i).getString("status"));
						StringWriter v = new StringWriter();
						v.write(payload1.toString());
						payload1.writeJSONString(v);
						obj1.put(payload1);

					}
				} else {

					org.json.JSONArray obj = legsroundwayinternational.getJSONObject(j).getJSONArray("legs");
					for (int i = 0; i < obj.length(); i++) {
						System.err.println(obj.length());
						legs2.put("boundType", legsroundwayinternational.getJSONObject(j).getString("boundType"));
						legs2.put("baggageFare", false);
						legs2.put("ssrFare", false);
						legs2.put("itineraryKey", legsroundwayinternational.getJSONObject(j).getString("itineraryKey"));
						legs2.put("journeyTime", legsroundwayinternational.getJSONObject(j).getString("journeyTime"));

						org.json.simple.JSONObject payload1 = new org.json.simple.JSONObject();
						payload1.put("aircraftCode", "null");
						payload1.put("aircraftType", "null");
						payload1.put("airlineName", obj.getJSONObject(i).getString("airlineName"));
						payload1.put("arrivalDate", obj.getJSONObject(i).getString("arrivalDate"));
						payload1.put("arrivalTerminal", "");
						payload1.put("arrivalTime", obj.getJSONObject(i).getString("arrivalTime"));
						payload1.put("availableSeat", "null");
						payload1.put("baggageUnit", obj.getJSONObject(i).getString("baggageUnit"));
						payload1.put("baggageWeight", obj.getJSONObject(i).getString("baggageWeight"));
						payload1.put("boundTypes", obj.getJSONObject(i).getString("boundType"));
						payload1.put("cabin", "Economy");

						payload1.put("capacity", obj.getJSONObject(i).getString("capacity"));
						payload1.put("carrierCode", obj.getJSONObject(i).getString("carrierCode"));

						payload1.put("currencyCode", obj.getJSONObject(i).getString("currencyCode"));
						payload1.put("departureDate", obj.getJSONObject(i).getString("departureDate"));
						payload1.put("departureTerminal", obj.getJSONObject(i).getString("departureTerminal"));
						payload1.put("departureTime", obj.getJSONObject(i).getString("departureTime"));
						payload1.put("destination", obj.getJSONObject(i).getString("destination"));
						payload1.put("duration", obj.getJSONObject(i).getString("duration"));
						payload1.put("fareBasisCode", obj.getJSONObject(i).getString("fareBasisCode"));
						payload1.put("fareClassOfService", obj.getJSONObject(i).getString("fareClassOfService"));
						payload1.put("flightDesignator", obj.getJSONObject(i).getString("flightDesignator"));
						payload1.put("flightDetailRefKey", obj.getJSONObject(i).getString("flightDetailRefKey"));

						payload1.put("flightNumber", obj.getJSONObject(i).getString("flightNumber"));
						payload1.put("origin", obj.getJSONObject(i).getString("origin"));
						payload1.put("sold", 0);
						payload1.put("status", obj.getJSONObject(i).getString("status"));
						StringWriter v = new StringWriter();
						v.write(payload1.toString());
						payload1.writeJSONString(v);
						obj2.put(payload1);
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(e);
			}
		}
		legs.put("legs", obj1);
		legs2.put("legs", obj2);
		obj4.put(legs);
		obj4.put(legs2);
		objmain.put("bonds", obj4);

		org.json.JSONObject obj11 = obj12.getJSONObject(0);
		objmain.put("fares", obj11);

		return objmain;
	}

	public static org.json.JSONObject createjsonBookApiinternationalFlightBook(ReturnFlightBookRequest req,
			HttpSession session) throws Exception {
		String source = "" + session.getAttribute("airRePriceRQinternational");
		JSONObject parser = new JSONObject(source);

		JSONArray journeys = parser.getJSONObject("details").getJSONArray("journeys");

		JSONArray segments = journeys.getJSONObject(0).getJSONArray("segments");

		JSONArray bondsroundwayinternational = segments.getJSONObject(0).getJSONArray("bonds");
		JSONArray fare = segments.getJSONObject(0).getJSONArray("fare");

		org.json.JSONArray obj12 = new org.json.JSONArray(bondsroundwayinternational.toString());

		String df = "" + fare;

		String val = df.replaceAll("fares", "bookFares");
		org.json.JSONArray obj1 = new org.json.JSONArray(val);
		org.json.JSONObject obj22 = CreateJsonRequestFlight.createjsonBookApiinternational(obj12, obj1);
		JSONObject objmain = new JSONObject();

		org.json.JSONObject objmain1 = new org.json.JSONObject();

		org.json.JSONObject objmain2 = new org.json.JSONObject();
		org.json.simple.JSONArray objkbond = new org.json.simple.JSONArray();

		ArrayList<String> objhasmap = new ArrayList<>();

		org.json.simple.JSONArray objk = new org.json.simple.JSONArray();

		obj22.put("engineID", segments.getJSONObject(0).getString("engineID"));
		obj22.put("fareIndicator", 0);
		obj22.put("fareRule", segments.getJSONObject(0).getString("fareRule"));
		obj22.put("baggageFare", false);
		obj22.put("cache", false);
		obj22.put("holdBooking", false);
		obj22.put("international", true);
		obj22.put("roundTrip", false);
		obj22.put("special", false);
		obj22.put("specialId", false);
		obj22.put("itineraryKey", segments.getJSONObject(0).getString("itineraryKey"));
		obj22.put("journeyIndex", 0);
		obj22.put("memoryCreationTime", "/Date(1499158249483+0530)/");
		obj22.put("nearByAirport", false);
		obj22.put("remark", "");
		obj22.put("searchId", segments.getJSONObject(0).getString("searchId"));
		objk.add(obj22);

		objhasmap.add("" + segments.getJSONObject(0).getString("engineID"));
		objmain1.put("clientIp", Inet4Address.getLocalHost().getHostAddress());
		objmain1.put("cllientKey", MdexConstants.getMdexCredentials().getCode());
		objmain1.put("clientToken", MdexConstants.getMdexCredentials().getStatus());
		objmain1.put("clientApiName", "Flight Booking");

		org.json.JSONObject flightSearchDetailsjsonobject = new org.json.JSONObject();
		org.json.JSONObject flightSearchDetailsjsonobject1 = new org.json.JSONObject();
		org.json.JSONObject payload1 = new org.json.JSONObject();
		org.json.JSONObject paymentDetails = new org.json.JSONObject();
		org.json.JSONArray flightSearchDetailsaray = new org.json.JSONArray();
		flightSearchDetailsjsonobject.put("beginDate", "" + session.getAttribute("beginDate"));
		flightSearchDetailsjsonobject.put("destination", "" + session.getAttribute("destination"));
		flightSearchDetailsjsonobject.put("origin", "" + session.getAttribute("source"));
		flightSearchDetailsjsonobject.put("endDate", "" + session.getAttribute("beginDate"));
		flightSearchDetailsaray.put(flightSearchDetailsjsonobject);
		flightSearchDetailsjsonobject1.put("endDate", "" + session.getAttribute("endDate"));
		flightSearchDetailsjsonobject1.put("beginDate", "" + session.getAttribute("endDate"));
		flightSearchDetailsjsonobject1.put("destination", "" + session.getAttribute("source"));
		flightSearchDetailsjsonobject1.put("origin", "" + session.getAttribute("destination"));

		flightSearchDetailsaray.put(flightSearchDetailsjsonobject1);
		payload1.put("engineIDs", objhasmap);

		paymentDetails.put("bookingAmount", req.getGrandtotal());
		paymentDetails.put("bookingCurrencyCode", "INR");
		objmain1.put("paymentDetails", paymentDetails);
		objmain1.put("flightSearchDetails", flightSearchDetailsaray);
		objmain1.put("engineIDList", objhasmap);

		org.codehaus.jettison.json.JSONArray adultTravellers = CreateJsonRequestFlight.createReturnjsonAdultraveler(req,
				"");
		org.codehaus.jettison.json.JSONArray childTravellers = CreateJsonRequestFlight.createReturnjsonChildraveler(req,
				"");
		org.codehaus.jettison.json.JSONArray infantTravellers = CreateJsonRequestFlight
				.createReturnjsonInfantrevaleer(req, "");

		org.json.JSONArray adultTravellers1 = new org.json.JSONArray(adultTravellers.toString());
		org.json.JSONArray childTravellers1 = new org.json.JSONArray(childTravellers.toString());
		org.json.JSONArray infantTravellers1 = new org.json.JSONArray(infantTravellers.toString());
		org.json.JSONObject adultTravellersnew = new org.json.JSONObject();
		adultTravellersnew.put("adultTravellers", adultTravellers1);
		adultTravellersnew.put("childTravellers", childTravellers1);
		adultTravellersnew.put("infantTravellers", infantTravellers1);

		objmain1.put("spKey", "6EIN");
		objmain1.put("traceId", "AYTM00011111111110001");
		objmain1.put("transactionId", req.getTransactionId());
		objmain1.put("travellers", adultTravellersnew);
		objmain1.put("visatype", "Employee Visa");
		objmain1.put("traceId", "AYTM00011111111110001");

		objmain1.put("androidBooking", true);
		objmain1.put("domestic", true);
		objmain1.put("engineID", segments.getJSONObject(0).getString("engineID"));

		objmain1.put("bookSegments", objk);
		return objmain1;
	}

	@SuppressWarnings("unchecked")
	public static org.json.JSONObject createjsonForTicket(org.json.JSONArray obj3) throws Exception {
		org.json.JSONArray obj1 = new org.json.JSONArray();
		org.json.JSONArray obj7 = new org.json.JSONArray();
		org.json.JSONObject objmain = new org.json.JSONObject();
		org.json.JSONArray obj2 = new org.json.JSONArray();
		org.json.JSONArray obj4 = new org.json.JSONArray();
		org.json.JSONObject legs = new org.json.JSONObject();
		org.json.JSONObject legs2 = new org.json.JSONObject();
		org.json.JSONArray legsroundwayinternational = obj3;

		for (int j = 0; j < legsroundwayinternational.length(); j++) {
			try {
				String journeyTime = legsroundwayinternational.getJSONObject(j).getString("journeyTime");
				org.json.JSONArray obj = legsroundwayinternational.getJSONObject(j).getJSONArray("legs");

				if (j == 0) {

					for (int i = 0; i < obj.length(); i++) {
						
						
						
						org.json.simple.JSONObject payload1 = new org.json.simple.JSONObject();
						payload1.put("journeyTime", journeyTime);
						payload1.put("cabin", obj.getJSONObject(i).getString("cabin"));
						payload1.put("airlineName", obj.getJSONObject(i).getString("airlineName"));
						payload1.put("arrivalDate", obj.getJSONObject(i).getString("arrivalDate"));

						payload1.put("arrivalTime", obj.getJSONObject(i).getString("arrivalTime"));


						payload1.put("baggageUnit", obj.getJSONObject(i).getString("baggageUnit"));
						payload1.put("baggageWeight", obj.getJSONObject(i).getString("baggageWeight"));

						payload1.put("departureDate", obj.getJSONObject(i).getString("departureDate"));

						payload1.put("departureTime", obj.getJSONObject(i).getString("departureTime"));

						payload1.put("duration", obj.getJSONObject(i).getString("duration"));
						payload1.put("destination", obj.getJSONObject(i).getString("destination"));
						payload1.put("origin", obj.getJSONObject(i).getString("origin"));

						payload1.put("flightNumber", obj.getJSONObject(i).getString("flightNumber"));

						StringWriter v = new StringWriter();
						v.write(payload1.toString());
						payload1.writeJSONString(v);
						obj1.put(payload1);
						
					}
				} else {
					for (int i = 0; i < obj.length(); i++) {

						org.json.simple.JSONObject payload1 = new org.json.simple.JSONObject();
						payload1.put("journeyTime", journeyTime);
						payload1.put("cabin", obj.getJSONObject(i).getString("cabin"));
						payload1.put("airlineName", obj.getJSONObject(i).getString("airlineName"));
						payload1.put("arrivalDate", obj.getJSONObject(i).getString("arrivalDate"));

						payload1.put("arrivalTime", obj.getJSONObject(i).getString("arrivalTime"));

						payload1.put("baggageUnit", obj.getJSONObject(i).getString("baggageUnit"));
						payload1.put("baggageWeight", obj.getJSONObject(i).getString("baggageWeight"));

						payload1.put("departureDate", obj.getJSONObject(i).getString("departureDate"));

						payload1.put("departureTime", obj.getJSONObject(i).getString("departureTime"));

						payload1.put("duration", obj.getJSONObject(i).getString("duration"));
						payload1.put("destination", obj.getJSONObject(i).getString("destination"));
						payload1.put("origin", obj.getJSONObject(i).getString("origin"));

						payload1.put("flightNumber", obj.getJSONObject(i).getString("flightNumber"));

						StringWriter v = new StringWriter();
						v.write(payload1.toString());
						payload1.writeJSONString(v);
						obj7.put(payload1);

					}
				}

			} catch (Exception e) {

				e.printStackTrace();

				System.out.println(e);
			}
		}

		legs.put("Oneway", obj1);

		legs.put("Roundway", obj7);

		objmain.put("Tickets", legs);

		return objmain;
	}

	@SuppressWarnings("unchecked")
	public static org.json.JSONArray createjsonForTicketRoundwayfirstflight(org.json.JSONArray obj3) throws Exception {
		org.json.JSONArray obj1 = new org.json.JSONArray();
		org.json.JSONArray obj7 = new org.json.JSONArray();
		org.json.JSONObject objmain = new org.json.JSONObject();
		org.json.JSONArray obj2 = new org.json.JSONArray();
		org.json.JSONArray obj4 = new org.json.JSONArray();
		org.json.JSONObject legs = new org.json.JSONObject();
		org.json.JSONObject legs2 = new org.json.JSONObject();
		org.json.JSONArray legsroundwayinternational = obj3;

		for (int j = 0; j < legsroundwayinternational.length(); j++) {
			try {
				String journeyTime = legsroundwayinternational.getJSONObject(j).getString("journeyTime");
				org.json.JSONArray obj = legsroundwayinternational.getJSONObject(j).getJSONArray("legs");

				for (int i = 0; i < obj.length(); i++) {

					org.json.simple.JSONObject payload1 = new org.json.simple.JSONObject();
					payload1.put("journeyTime", journeyTime);
					payload1.put("airlineName", obj.getJSONObject(i).getString("airlineName"));
					payload1.put("arrivalDate", obj.getJSONObject(i).getString("arrivalDate"));


					payload1.put("arrivalTime", obj.getJSONObject(i).getString("arrivalTime"));

					payload1.put("cabin", obj.getJSONObject(i).getString("cabin"));
					payload1.put("baggageUnit", obj.getJSONObject(i).getString("baggageUnit"));
					payload1.put("baggageWeight", obj.getJSONObject(i).getString("baggageWeight"));

					payload1.put("departureDate", obj.getJSONObject(i).getString("departureDate"));

					payload1.put("departureTime", obj.getJSONObject(i).getString("departureTime"));

					payload1.put("duration", obj.getJSONObject(i).getString("duration"));
					payload1.put("destination", obj.getJSONObject(i).getString("destination"));
					payload1.put("origin", obj.getJSONObject(i).getString("origin"));

					payload1.put("flightNumber", obj.getJSONObject(i).getString("flightNumber"));

					StringWriter v = new StringWriter();
					v.write(payload1.toString());
					payload1.writeJSONString(v);
					obj1.put(payload1);

				}

			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(e);
			}
		}

		return obj1;
	}


	@SuppressWarnings("unchecked")
	public static org.json.JSONArray createjsonForTicketRoundwaySecondflight(org.json.JSONArray obj3) throws Exception {
		org.json.JSONArray obj1 = new org.json.JSONArray();
		org.json.JSONArray obj7 = new org.json.JSONArray();
		org.json.JSONObject objmain = new org.json.JSONObject();
		org.json.JSONArray obj2 = new org.json.JSONArray();
		org.json.JSONArray obj4 = new org.json.JSONArray();
		org.json.JSONObject legs = new org.json.JSONObject();
		org.json.JSONObject legs2 = new org.json.JSONObject();
		org.json.JSONArray legsroundwayinternational = obj3;

		for (int j = 0; j < legsroundwayinternational.length(); j++) {
			try {
				String journeyTime = legsroundwayinternational.getJSONObject(j).getString("journeyTime");
				org.json.JSONArray obj = legsroundwayinternational.getJSONObject(j).getJSONArray("legs");

				for (int i = 0; i < obj.length(); i++) {

					org.json.simple.JSONObject payload1 = new org.json.simple.JSONObject();
					payload1.put("journeyTime", journeyTime);
					payload1.put("airlineName", obj.getJSONObject(i).getString("airlineName"));
					payload1.put("arrivalDate", obj.getJSONObject(i).getString("arrivalDate"));

					payload1.put("arrivalTime", obj.getJSONObject(i).getString("arrivalTime"));

					payload1.put("cabin", obj.getJSONObject(i).getString("cabin"));
					payload1.put("baggageUnit", obj.getJSONObject(i).getString("baggageUnit"));
					payload1.put("baggageWeight", obj.getJSONObject(i).getString("baggageWeight"));

					payload1.put("departureDate", obj.getJSONObject(i).getString("departureDate"));

					payload1.put("departureTime", obj.getJSONObject(i).getString("departureTime"));

					payload1.put("duration", obj.getJSONObject(i).getString("duration"));
					payload1.put("destination", obj.getJSONObject(i).getString("destination"));
					payload1.put("origin", obj.getJSONObject(i).getString("origin"));

					payload1.put("flightNumber", obj.getJSONObject(i).getString("flightNumber"));

					StringWriter v = new StringWriter();
					v.write(payload1.toString());
					payload1.writeJSONString(v);
					obj1.put(payload1);

				}

			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(e);
			}
		}

		return obj1;
	}



	public static JSONObject createjsonForPaymentGateWayMobile(FlightPaymentGateWayDTO req) {
		try {

			JSONObject payload = new JSONObject();

			JSONObject legsjsonobjet = new JSONObject();
			JSONArray legsjsonarray = new JSONArray();
			MobileFlightBookRequest req1=new MobileFlightBookRequest();
			req1.setBookFares(req.getBookFares());
			org.json.simple.JSONArray bookfarejsonArray = CreateJsonRequestFlight.createBookFareJsonArray(req1);

			JSONObject bondsjsonobjet = new JSONObject();
			JSONArray bondsjsonarray = new JSONArray();

			JSONObject ssrDetailsjsonobjet = new JSONObject();
			JSONArray ssrDetailsjsonarray = new JSONArray();

			JSONArray engineIDList = new JSONArray();

			JSONObject bookSegmentsjsonobjet = new JSONObject();
			JSONArray bookSegmentsjsonarray = new JSONArray();

			JSONObject adultTravellersnew = new JSONObject();
			JSONArray flightSearchDetailsaray = new JSONArray();
			JSONObject flightSearchDetailsjsonobject = new JSONObject();
			JSONObject paymentDetails = new JSONObject();
			ArrayList<String> objhasmap = new ArrayList<>();

			ArrayList<String> objhasmap1 = new ArrayList<>();

			JSONObject faresjsonobjet = new JSONObject();
			JSONObject paxFaresjsonobjet = new JSONObject();
			JSONArray paxFaresjsonarray = new JSONArray();

			paxFaresjsonobjet.put("baggageUnit", req.getBaggageUnit());
			paxFaresjsonobjet.put("baggageWeight", req.getBaggageWeight());
			paxFaresjsonobjet.put("baseTransactionAmount", 0);
			paxFaresjsonobjet.put("basicFare", req.getBasicFare());
			paxFaresjsonobjet.put("cancelPenalty", req.getCancelPenalty());
			paxFaresjsonobjet.put("changePenalty", req.getChangePenalty());
			paxFaresjsonobjet.put("equivCurrencyCode", "");

			paxFaresjsonobjet.put("fareBasisCode", "");
			paxFaresjsonobjet.put("fareInfoKey", "");
			paxFaresjsonobjet.put("fareInfoValue", "");
			paxFaresjsonobjet.put("markUP", 0);
			paxFaresjsonobjet.put("paxType", "ADT");
			paxFaresjsonobjet.put("refundable", true);
			paxFaresjsonobjet.put("totalFare", req.getTotalFare());
			paxFaresjsonobjet.put("totalTax", req.getTotalTax());
			paxFaresjsonobjet.put("transactionAmount", 0);
			paxFaresjsonobjet.put("bookFares", bookfarejsonArray);
			paxFaresjsonarray.put(paxFaresjsonobjet);

			faresjsonobjet.put("basicFare", req.getBasicFare());
			faresjsonobjet.put("exchangeRate", 0);

			faresjsonobjet.put("totalFareWithOutMarkUp", req.getTotalFareWithOutMarkUp());
			faresjsonobjet.put("totalTaxWithOutMarkUp", req.getTotalTaxWithOutMarkUp());
			faresjsonobjet.put("paxFares", paxFaresjsonarray);

			objhasmap.add("" + EngineID.Indigo);
			payload.put("clientIp", Inet4Address.getLocalHost().getHostAddress());
			payload.put("cllientKey", MdexConstants.getMdexCredentials().getCode());
			payload.put("clientToken", MdexConstants.getMdexCredentials().getStatus());
			payload.put("clientApiName", "Flight Booking");
			payload.put("spKey", req.getAirlineName() + "IN");

			bondsjsonobjet.put("boundType", "");
			bondsjsonobjet.put("baggageFare", false);
			bondsjsonobjet.put("ssrFare", false);
			bondsjsonobjet.put("itineraryKey", req.getItineraryKey());
			bondsjsonobjet.put("journeyTime", req.getDuration());

			bondsjsonobjet.put("addOnDetail", "");

			legsjsonobjet.put("aircraftCode", "");
			legsjsonobjet.put("aircraftType", "");
			legsjsonobjet.put("airlineName", req.getAirlineName());
			legsjsonobjet.put("amount", req.getAmount());
			legsjsonobjet.put("arrivalDate", req.getArrivalDate());
			legsjsonobjet.put("arrivalTerminal", "");
			legsjsonobjet.put("arrivalTime", req.getArrivalTime());
			legsjsonobjet.put("availableSeat", "");
			legsjsonobjet.put("baggageUnit", req.getBaggageUnit());
			legsjsonobjet.put("baggageWeight", req.getBaggageWeight());
			legsjsonobjet.put("boundTypes", "");
			legsjsonobjet.put("cabin", "Economy");
			legsjsonobjet.put("cabinClasses", objhasmap1);
			legsjsonobjet.put("capacity", "0");
			legsjsonobjet.put("carrierCode", req.getAirlineName());
			legsjsonobjet.put("currencyCode", "INR");
			legsjsonobjet.put("departureDate", req.getDepartureDate());
			legsjsonobjet.put("departureTerminal", req.getDepartureTerminal());
			legsjsonobjet.put("departureTime", req.getDepartureTime());
			legsjsonobjet.put("destination", req.getDestination());
			legsjsonobjet.put("duration", req.getDuration());
			legsjsonobjet.put("fareBasisCode", req.getFareBasisCode());
			legsjsonobjet.put("fareClassOfService", req.getFareClassOfService());
			legsjsonobjet.put("flightDesignator", "");
			legsjsonobjet.put("flightDetailRefKey", "");
			legsjsonobjet.put("flightName", "Indigo");
			legsjsonobjet.put("flightNumber", req.getFlightNumber());
			legsjsonobjet.put("group", "");
			legsjsonobjet.put("connecting", false);
			legsjsonobjet.put("numberOfStops", "");
			legsjsonobjet.put("origin", req.getOrigin());
			legsjsonobjet.put("providerCode", "");
			legsjsonobjet.put("remarks", "");
			ssrDetailsjsonarray.put(ssrDetailsjsonobjet);
			legsjsonobjet.put("sold", 0);
			legsjsonobjet.put("status", "Normal");
			legsjsonobjet.put("ssrDetails", ssrDetailsjsonarray);
			legsjsonarray.put(legsjsonobjet);

			bondsjsonobjet.put("legs", legsjsonarray);

			bondsjsonarray.put(bondsjsonobjet);

			bookSegmentsjsonobjet.put("bonds", bondsjsonarray);
			bookSegmentsjsonobjet.put("deeplink", "");
			bookSegmentsjsonobjet.put("fareIndicator", 0);
			bookSegmentsjsonobjet.put("fareRule", req.getFareRule());
			bookSegmentsjsonobjet.put("baggageFare", false);
			bookSegmentsjsonobjet.put("cache", false);
			bookSegmentsjsonobjet.put("holdBooking", false);
			bookSegmentsjsonobjet.put("international", false);
			bookSegmentsjsonobjet.put("roundTrip", false);
			bookSegmentsjsonobjet.put("special", false);
			bookSegmentsjsonobjet.put("specialId", false);
			bookSegmentsjsonobjet.put("itineraryKey", req.getItineraryKey());
			bookSegmentsjsonobjet.put("journeyIndex", 0);
			bookSegmentsjsonobjet.put("memoryCreationTime", "/Date(1499158249483+0530)/");
			bookSegmentsjsonobjet.put("nearByAirport", false);
			bookSegmentsjsonobjet.put("remark", "");
			bookSegmentsjsonobjet.put("searchId", req.getSearchId());

			bookSegmentsjsonobjet.put("engineID", "Indigo");
			bookSegmentsjsonobjet.put("fares", faresjsonobjet);

			bookSegmentsjsonarray.put(bookSegmentsjsonobjet);
			payload.put("bookSegments", bookSegmentsjsonarray);

			flightSearchDetailsjsonobject.put("beginDate", req.getBeginDate());
			flightSearchDetailsjsonobject.put("destination", req.getDestination());
			flightSearchDetailsjsonobject.put("endDate", req.getBeginDate());
			flightSearchDetailsjsonobject.put("origin", req.getOrigin());
			flightSearchDetailsaray.put(flightSearchDetailsjsonobject);
			payload.put("flightSearchDetails", flightSearchDetailsaray);

			paymentDetails.put("bookingAmount", req.getTotalFareWithOutMarkUp());
			paymentDetails.put("bookingCurrencyCode", "INR");
			payload.put("paymentDetails", paymentDetails);

			payload.put("engineIDList", objhasmap);

			req1.setAdults(req.getAdults());
			req1.setEmailAddress(req.getEmailAddress());
			req1.setFirstName(req.getFirstName());
			req1.setLastName(req.getLastName());
			req1.setMobileNumber(req.getMobileNumber());

			req1.setChilds(req.getChilds());
			req1.setFirstNamechild(req.getFirstNamechild());
			req1.setLastNamechild(req.getLastNamechild());

			req1.setInfants(req.getInfants());
			req1.setFirstNameinfant(req.getFirstNameinfant());
			req1.setLastNameinfant(req.getLastNameinfant());

			JSONArray adultTravellers = CreateJsonRequestFlight.createMobilejsonAdultraveler(req1, "");
			JSONArray childTravellers = CreateJsonRequestFlight.createMobilejsonChildraveler(req1, "");
			JSONArray infantTravellers = CreateJsonRequestFlight.createMobilejsonInfantrevaleer(req1, "");

			adultTravellersnew.put("adultTravellers", adultTravellers);
			adultTravellersnew.put("childTravellers", childTravellers);
			adultTravellersnew.put("infantTravellers", infantTravellers);

			payload.put("travellers", adultTravellersnew);
			payload.put("visatype", "Employee Visa");
			payload.put("traceId", "AYTM00011111111110001");
			payload.put("transactionId", req.getTransactionId());
			payload.put("androidBooking", true);
			payload.put("domestic", true);
			payload.put("engineID", objhasmap.get(0));
			return payload;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}



	public static JSONObject createjsonForPaymentGateWayReturnMobile(FlightPaymentGateWayDTO req) {
		try {

			JSONObject payload = new JSONObject();
			//
			JSONObject legsjsonobjet = new JSONObject();
			JSONArray legsjsonarray = new JSONArray();
			MobileRoundwayFlightBookRequest req1=new MobileRoundwayFlightBookRequest();
			req1.setBookFares(req.getBookFares());
			req1.setBookFaresreturn(req.getBookFaresreturn());
			org.json.simple.JSONArray bookfarejsonArray = CreateJsonRequestFlight.createBookRoundFareJsonArray(req1);

			JSONObject bondsjsonobjet = new JSONObject();
			JSONArray bondsjsonarray = new JSONArray();

			JSONObject ssrDetailsjsonobjet = new JSONObject();
			JSONArray ssrDetailsjsonarray = new JSONArray();

			JSONArray engineIDList = new JSONArray();
			org.json.simple.JSONArray bookfarejsonArrayreturn = CreateJsonRequestFlight
					.createBookRoundFareJsonArray1(req1);
			JSONObject bookSegmentsjsonobjet = new JSONObject();
			JSONArray bookSegmentsjsonarray = new JSONArray();

			org.json.simple.JSONArray flightSearchDetailsaray = new org.json.simple.JSONArray();
			JSONObject flightSearchDetailsjsonobject = new JSONObject();
			JSONObject paymentDetails = new JSONObject();
			ArrayList<String> objhasmap = new ArrayList<>();
			//
			ArrayList<String> objhasmap1 = new ArrayList<>();
			//
			JSONObject faresjsonobjet = new JSONObject();
			JSONObject paxFaresjsonobjet = new JSONObject();
			JSONArray paxFaresjsonarray = new JSONArray();

			paxFaresjsonobjet.put("baggageUnit", req.getBaggageUnit());
			paxFaresjsonobjet.put("baggageWeight", req.getBaggageWeight());
			paxFaresjsonobjet.put("baseTransactionAmount", 0);
			paxFaresjsonobjet.put("basicFare", req.getBasicFare());
			paxFaresjsonobjet.put("cancelPenalty", req.getCancelPenalty());
			paxFaresjsonobjet.put("changePenalty", req.getChangePenalty());
			paxFaresjsonobjet.put("equivCurrencyCode", "");

			paxFaresjsonobjet.put("fareBasisCode", "");
			paxFaresjsonobjet.put("fareInfoKey", "");
			paxFaresjsonobjet.put("fareInfoValue", "");
			paxFaresjsonobjet.put("markUP", 0);
			paxFaresjsonobjet.put("paxType", "ADT");
			paxFaresjsonobjet.put("refundable", true);
			paxFaresjsonobjet.put("totalFare", req.getTotalFare());
			paxFaresjsonobjet.put("totalTax", req.getTotalTax());
			paxFaresjsonobjet.put("transactionAmount", 0);
			paxFaresjsonobjet.put("bookFares", bookfarejsonArray);
			paxFaresjsonarray.put(paxFaresjsonobjet);

			faresjsonobjet.put("basicFare", req.getBasicFare());
			faresjsonobjet.put("exchangeRate", 0);

			faresjsonobjet.put("totalFareWithOutMarkUp", req.getTotalFareWithOutMarkUp());
			faresjsonobjet.put("totalTaxWithOutMarkUp", req.getTotalTaxWithOutMarkUp());
			faresjsonobjet.put("paxFares", paxFaresjsonarray);

			objhasmap.add("" + EngineID.Indigo);
			payload.put("clientIp", Inet4Address.getLocalHost().getHostAddress());
			payload.put("cllientKey", MdexConstants.getMdexCredentials().getCode());
			payload.put("clientToken", MdexConstants.getMdexCredentials().getStatus());
			payload.put("clientApiName", "Flight Booking");
			payload.put("spKey", req.getAirlineName() + "IN");

			bondsjsonobjet.put("boundType", "");
			bondsjsonobjet.put("baggageFare", false);
			bondsjsonobjet.put("ssrFare", false);
			bondsjsonobjet.put("itineraryKey", req.getItineraryKey());
			bondsjsonobjet.put("journeyTime", req.getDuration());

			bondsjsonobjet.put("addOnDetail", "");

			legsjsonobjet.put("aircraftCode", "");
			legsjsonobjet.put("aircraftType", "");
			legsjsonobjet.put("airlineName", req.getAirlineName());
			legsjsonobjet.put("amount", req.getAmount());
			legsjsonobjet.put("arrivalDate", req.getArrivalDate());
			legsjsonobjet.put("arrivalTerminal", "");
			legsjsonobjet.put("arrivalTime", req.getArrivalTime());
			legsjsonobjet.put("availableSeat", "");
			legsjsonobjet.put("baggageUnit", req.getBaggageUnit());
			legsjsonobjet.put("baggageWeight", req.getBaggageWeight());
			legsjsonobjet.put("boundTypes", "");
			legsjsonobjet.put("cabin", "Economy");
			legsjsonobjet.put("cabinClasses", objhasmap1);
			legsjsonobjet.put("capacity", "0");
			legsjsonobjet.put("carrierCode", req.getAirlineName());
			legsjsonobjet.put("currencyCode", "INR");
			legsjsonobjet.put("departureDate", req.getDepartureDate());
			legsjsonobjet.put("departureTerminal", req.getDepartureTerminal());
			legsjsonobjet.put("departureTime", req.getDepartureTime());
			legsjsonobjet.put("destination", req.getDestination());
			legsjsonobjet.put("duration", req.getDuration());
			legsjsonobjet.put("fareBasisCode", req.getFareBasisCode());
			legsjsonobjet.put("fareClassOfService", req.getFareClassOfService());
			legsjsonobjet.put("flightDesignator", "");
			legsjsonobjet.put("flightDetailRefKey", "");
			legsjsonobjet.put("flightName", "Indigo");
			legsjsonobjet.put("flightNumber", req.getFlightNumber());
			legsjsonobjet.put("group", "");
			legsjsonobjet.put("connecting", false);
			legsjsonobjet.put("numberOfStops", "");
			legsjsonobjet.put("origin", req.getOrigin());
			legsjsonobjet.put("providerCode", "");
			legsjsonobjet.put("remarks", "");
			ssrDetailsjsonarray.put(ssrDetailsjsonobjet);
			legsjsonobjet.put("sold", 0);
			legsjsonobjet.put("status", "Normal");
			legsjsonobjet.put("ssrDetails", ssrDetailsjsonarray);
			legsjsonarray.put(legsjsonobjet);

			bondsjsonobjet.put("legs", legsjsonarray);

			bondsjsonarray.put(bondsjsonobjet);

			bookSegmentsjsonobjet.put("bonds", bondsjsonarray);
			bookSegmentsjsonobjet.put("deeplink", "");
			bookSegmentsjsonobjet.put("fareIndicator", 0);
			bookSegmentsjsonobjet.put("fareRule", req.getFareRule());
			bookSegmentsjsonobjet.put("baggageFare", false);
			bookSegmentsjsonobjet.put("cache", false);
			bookSegmentsjsonobjet.put("holdBooking", false);
			bookSegmentsjsonobjet.put("international", false);
			bookSegmentsjsonobjet.put("roundTrip", false);
			bookSegmentsjsonobjet.put("special", false);
			bookSegmentsjsonobjet.put("specialId", false);
			bookSegmentsjsonobjet.put("itineraryKey", req.getItineraryKey());
			bookSegmentsjsonobjet.put("journeyIndex", 0);
			bookSegmentsjsonobjet.put("memoryCreationTime", "/Date(1499158249483+0530)/");
			bookSegmentsjsonobjet.put("nearByAirport", false);
			bookSegmentsjsonobjet.put("remark", "");
			bookSegmentsjsonobjet.put("searchId", req.getSearchId());

			bookSegmentsjsonobjet.put("engineID", "Indigo");
			bookSegmentsjsonobjet.put("fares", faresjsonobjet);
			bookSegmentsjsonarray.put(bookSegmentsjsonobjet);

			JSONObject faresjsonobjetreturn = new JSONObject();
			JSONObject paxFaresjsonobjetreturn = new JSONObject();
			JSONArray paxFaresjsonarrayreturn = new JSONArray();
			JSONObject bookSegmentsjsonobjetreturn = new JSONObject();

			JSONObject legsjsonobjetreturn = new JSONObject();
			JSONArray legsjsonarrayreturn = new JSONArray();

			JSONObject bondsjsonobjetreturn = new JSONObject();
			JSONArray bondsjsonarrayreturn = new JSONArray();

			JSONObject ssrDetailsjsonobjetreturn = new JSONObject();
			JSONArray ssrDetailsjsonarrayreturn = new JSONArray();

			JSONArray engineIDListreturn = new JSONArray();

			paxFaresjsonobjetreturn.put("baggageUnit", req.getBaggageUnitreturn());
			paxFaresjsonobjetreturn.put("baggageWeight", req.getBaggageWeightreturn());
			paxFaresjsonobjetreturn.put("baseTransactionAmount", 0);
			paxFaresjsonobjetreturn.put("basicFare", req.getBaggageFarereturn());
			paxFaresjsonobjetreturn.put("cancelPenalty", req.getCancelPenaltyreturn());
			paxFaresjsonobjetreturn.put("changePenalty", req.getChangePenaltyreturn());
			paxFaresjsonobjetreturn.put("equivCurrencyCode", "");
			paxFaresjsonobjetreturn.put("basicFare", req.getBasicFare());
			paxFaresjsonobjetreturn.put("fareBasisCode", "");
			paxFaresjsonobjetreturn.put("fareInfoKey", "");
			paxFaresjsonobjetreturn.put("fareInfoValue", "");
			paxFaresjsonobjetreturn.put("markUP", 0);
			paxFaresjsonobjetreturn.put("paxType", "ADT");
			paxFaresjsonobjetreturn.put("refundable", true);
			paxFaresjsonobjetreturn.put("totalFare", req.getTotalFarereturn());
			paxFaresjsonobjetreturn.put("totalTax", req.getTotalTaxreturn());
			paxFaresjsonobjetreturn.put("transactionAmount", 0);
			paxFaresjsonobjetreturn.put("bookFares", bookfarejsonArrayreturn);
			paxFaresjsonarrayreturn.put(paxFaresjsonobjetreturn);

			faresjsonobjetreturn.put("basicFare", req.getBasicFare());
			faresjsonobjetreturn.put("exchangeRate", 0);

			faresjsonobjetreturn.put("totalFareWithOutMarkUp", req.getTotalFareWithOutMarkUpreturn());
			faresjsonobjetreturn.put("totalTaxWithOutMarkUp", req.getTotalTaxWithOutMarkUpreturn());
			faresjsonobjetreturn.put("paxFares", paxFaresjsonarrayreturn);

			payload.put("spKey", req.getAirlineNamereturn() + "IN");

			bondsjsonobjetreturn.put("boundType", "");
			bondsjsonobjetreturn.put("baggageFare", false);
			bondsjsonobjetreturn.put("ssrFare", false);
			bondsjsonobjetreturn.put("itineraryKey", req.getItineraryKeyreturn());
			bondsjsonobjetreturn.put("journeyTime", req.getDurationreturn());

			bondsjsonobjetreturn.put("addOnDetail", "");

			legsjsonobjetreturn.put("aircraftCode", "");
			legsjsonobjetreturn.put("aircraftType", "");
			legsjsonobjetreturn.put("airlineName", req.getAirlineNamereturn());
			legsjsonobjetreturn.put("amount", req.getAmountreturn());
			legsjsonobjetreturn.put("arrivalDate", req.getArrivalDatereturn());
			legsjsonobjetreturn.put("arrivalTerminal", "");
			legsjsonobjetreturn.put("arrivalTime", req.getArrivalTimereturn());
			legsjsonobjetreturn.put("availableSeat", "");
			legsjsonobjetreturn.put("baggageUnit", req.getBaggageUnitreturn());
			legsjsonobjetreturn.put("baggageWeight", req.getBaggageWeightreturn());
			legsjsonobjetreturn.put("boundTypes", "");
			legsjsonobjetreturn.put("cabin", "Economy");
			legsjsonobjetreturn.put("cabinClasses", objhasmap1);
			legsjsonobjetreturn.put("capacity", "0");
			legsjsonobjetreturn.put("carrierCode", req.getAirlineNamereturn());
			legsjsonobjetreturn.put("currencyCode", "INR");
			legsjsonobjetreturn.put("departureDate", req.getDepartureDatereturn());
			legsjsonobjetreturn.put("departureTerminal", req.getDepartureTerminalreturn());
			legsjsonobjetreturn.put("departureTime", req.getDepartureTimereturn());
			legsjsonobjetreturn.put("destination", req.getOrigin());
			legsjsonobjetreturn.put("duration", req.getDurationreturn());
			legsjsonobjetreturn.put("fareBasisCode", req.getFareBasisCodereturn());
			legsjsonobjetreturn.put("fareClassOfService", req.getFareClassOfServicereturn());
			legsjsonobjetreturn.put("flightDesignator", "");
			legsjsonobjetreturn.put("flightDetailRefKey", "");
			legsjsonobjetreturn.put("flightName", "Indigo");
			legsjsonobjetreturn.put("flightNumber", req.getFlightNumberreturn());
			legsjsonobjetreturn.put("group", "");
			legsjsonobjetreturn.put("connecting", false);
			legsjsonobjetreturn.put("numberOfStops", "");
			legsjsonobjetreturn.put("origin", req.getDestination());
			legsjsonobjetreturn.put("providerCode", "");
			legsjsonobjetreturn.put("remarks", "");
			ssrDetailsjsonarrayreturn.put(ssrDetailsjsonobjetreturn);
			legsjsonobjetreturn.put("sold", 0);
			legsjsonobjetreturn.put("status", "Normal");
			legsjsonobjetreturn.put("ssrDetails", ssrDetailsjsonarrayreturn);
			legsjsonarrayreturn.put(legsjsonobjetreturn);

			bondsjsonobjetreturn.put("legs", legsjsonarrayreturn);

			bondsjsonarrayreturn.put(bondsjsonobjetreturn);

			bookSegmentsjsonobjetreturn.put("bonds", bondsjsonarrayreturn);
			bookSegmentsjsonobjetreturn.put("deeplink", "");
			bookSegmentsjsonobjetreturn.put("fareIndicator", 0);
			bookSegmentsjsonobjetreturn.put("fareRule", req.getFareRule());
			bookSegmentsjsonobjetreturn.put("baggageFare", false);
			bookSegmentsjsonobjetreturn.put("cache", false);
			bookSegmentsjsonobjetreturn.put("holdBooking", false);
			bookSegmentsjsonobjetreturn.put("international", false);
			bookSegmentsjsonobjetreturn.put("roundTrip", false);
			bookSegmentsjsonobjetreturn.put("special", false);
			bookSegmentsjsonobjetreturn.put("specialId", false);

			bookSegmentsjsonobjetreturn.put("itineraryKey", req.getItineraryKey());

			bookSegmentsjsonobjetreturn.put("journeyIndex", 0);
			bookSegmentsjsonobjetreturn.put("memoryCreationTime", "/Date(1499158249483+0530)/");
			bookSegmentsjsonobjetreturn.put("nearByAirport", false);
			bookSegmentsjsonobjetreturn.put("remark", "");
			bookSegmentsjsonobjetreturn.put("searchId", req.getSearchIdreturn());

			bookSegmentsjsonobjetreturn.put("engineID", "Indigo");
			bookSegmentsjsonobjetreturn.put("fares", faresjsonobjetreturn);

			bookSegmentsjsonarray.put(bookSegmentsjsonobjetreturn);

			payload.put("bookSegments", bookSegmentsjsonarray);

			flightSearchDetailsjsonobject.put("beginDate", req.getBeginDate());
			flightSearchDetailsjsonobject.put("destination", req.getDestination());
			flightSearchDetailsjsonobject.put("endDate", req.getEndDate());
			flightSearchDetailsjsonobject.put("origin", req.getOrigin());
			flightSearchDetailsaray.add(flightSearchDetailsjsonobject);
			JSONObject roundtripDetailsobj = new JSONObject();

			roundtripDetailsobj.put("origin", req.getDestination());
			roundtripDetailsobj.put("destination", req.getOrigin());
			roundtripDetailsobj.put("endDate", req.getEndDate());
			roundtripDetailsobj.put("beginDate", req.getBeginDate());

			flightSearchDetailsaray.add(roundtripDetailsobj);
			payload.put("flightSearchDetails", flightSearchDetailsaray);

			paymentDetails.put("bookingAmount", req.getGrandtotal());
			paymentDetails.put("bookingCurrencyCode", "INR");
			payload.put("paymentDetails", paymentDetails);

			payload.put("engineIDList", objhasmap);

			req1.setAdults(req.getAdults());
			req1.setEmailAddress(req.getEmailAddress());
			req1.setFirstName(req.getFirstName());
			req1.setLastName(req.getLastName());
			req1.setMobileNumber(req.getMobileNumber());

			req1.setChilds(req.getChilds());
			req1.setFirstNamechild(req.getFirstNamechild());
			req1.setLastNamechild(req.getLastNamechild());

			req1.setInfants(req.getInfants());
			req1.setFirstNameinfant(req.getFirstNameinfant());
			req1.setLastNameinfant(req.getLastNameinfant());

			JSONArray adultTravellers = CreateJsonRequestFlight.createMobileReturnjsonAdultraveler(req1, "");
			JSONArray childTravellers = CreateJsonRequestFlight.createMobileReturnjsonChildraveler(req1, "");
			JSONArray infantTravellers = CreateJsonRequestFlight.createMobileReturnjsonInfantrevaleer(req1, "");

			JSONObject adultTravellersnew = new JSONObject();
			adultTravellersnew.put("adultTravellers", adultTravellers);
			adultTravellersnew.put("childTravellers", childTravellers);
			adultTravellersnew.put("infantTravellers", infantTravellers);
			payload.put("travellers", adultTravellersnew);
			payload.put("visatype", "Employee Visa");
			payload.put("traceId", "AYTM00011111111110001");
			payload.put("transactionId", req.getTransactionId());
			payload.put("androidBooking", true);
			payload.put("domestic", true);
			payload.put("engineID", objhasmap.get(0));
			return payload;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	//###########################  Test #######################//







	@SuppressWarnings("unchecked")
	public static JSONObject createjsonforTest(ReturnFlightBookRequest req, String source, String destination,
			HttpSession session) {

		try {
			JSONObject payload = new JSONObject();

			JSONArray bookSegmentsjsonarray = new JSONArray();

			org.json.simple.JSONArray flightSearchDetailsaray = new org.json.simple.JSONArray();
			JSONObject flightSearchDetailsjsonobject = new JSONObject();
			JSONObject paymentDetails = new JSONObject();
			ArrayList<String> objhasmap = new ArrayList<>();

			ArrayList<String> objhasmap1 = new ArrayList<>();
			ArrayList<String> objhasmap2 = new ArrayList<>();

			String Firstfligt = "" + session.getAttribute("Firstfligt");
			String Secondflight = "" + session.getAttribute("Secondflight");
			if (Firstfligt.equals("Firstfligt")) {
				JSONObject legsjsonobjet = new JSONObject();
				JSONArray legsjsonarray = new JSONArray();

				JSONObject bondsjsonobjet = new JSONObject();
				JSONArray bondsjsonarray = new JSONArray();

				JSONObject ssrDetailsjsonobjet = new JSONObject();
				JSONArray ssrDetailsjsonarray = new JSONArray();

				JSONArray engineIDList = new JSONArray();
				JSONObject bookSegmentsjsonobjet = new JSONObject();
				JSONObject obj = new JSONObject(source);

				JSONArray journeys = obj.getJSONObject("details").getJSONArray("journeys");
				JSONObject faresjsonobjet = new JSONObject();
				JSONObject paxFaresjsonobjet = new JSONObject();
				JSONArray paxFaresjsonarray = new JSONArray();
				JSONArray segments = journeys.getJSONObject(0).getJSONArray("segments");
				for (int j = 0; j < segments.length(); j++) {

					JSONArray bonds = segments.getJSONObject(j).getJSONArray("bonds");
					for (int k = 0; k < bonds.length(); k++) {

						JSONArray legs = bonds.getJSONObject(k).getJSONArray("legs");
						String flightnumber = legs.getJSONObject(k).getString("flightNumber");
						legsjsonarray = bonds.getJSONObject(k).getJSONArray("legs");
						System.out.println("#######################################");
						req.setAirlineName("" + legs.getJSONObject(k).getString("airlineName"));
						req.setArrivalTime("" + legs.getJSONObject(k).getString("arrivalTime"));
						req.setCabin("" + legs.getJSONObject(k).getString("cabin"));
						req.setDepartureTime("" + legs.getJSONObject(k).getString("departureTime"));
						req.setDestination("" + legs.getJSONObject(k).getString("destination"));
						req.setDuration("" + legs.getJSONObject(k).getString("duration"));
						req.setBoundType("" + legs.getJSONObject(k).getString("boundType"));
						req.setFlightNumber("" + legs.getJSONObject(k).getString("flightNumber"));
						req.setOrigin("" + legs.getJSONObject(k).getString("origin"));
						req.setAdults("" + session.getAttribute("adults"));
						req.setChilds("" + session.getAttribute("childs"));
						req.setInfants("" + session.getAttribute("infants"));
						req.setEndDate("" + session.getAttribute("endDate"));
						req.setFareBasisCode("" + legs.getJSONObject(k).getString("fareBasisCode"));// fareClassOfService//fareBasisCode
						req.setBeginDate("" + session.getAttribute("beginDate"));
						req.setFlightName("" + legs.getJSONObject(k).getString("airlineName"));
						req.setArrivalDate("" + legs.getJSONObject(k).getString("arrivalDate"));
						req.setDepartureDate("" + legs.getJSONObject(k).getString("departureDate"));
						req.setDepartureTerminal("" + legs.getJSONObject(k).getString("departureTerminal"));
						req.setArrivalTerminal("" + legs.getJSONObject(k).getString("arrivalTerminal"));
						req.setCapacity("" + legs.getJSONObject(k).getString("capacity"));
						req.setCarrierCode("" + legs.getJSONObject(k).getString("carrierCode"));
						req.setCurrencyCode("" + legs.getJSONObject(k).getString("currencyCode"));
						req.setBaggageUnit("" + legs.getJSONObject(k).getString("baggageUnit"));
						req.setBaggageWeight("" + legs.getJSONObject(k).getString("baggageWeight"));
						req.setFareClassOfService("" + legs.getJSONObject(k).getString("fareClassOfService"));
						req.setSold("" + legs.getJSONObject(k).getString("sold"));
						req.setStatus("" + legs.getJSONObject(k).getString("status"));
						req.setItineraryKey("" + segments.getJSONObject(j).getString("itineraryKey"));
						req.setSearchId("" + segments.getJSONObject(j).getString("searchId"));
						req.setEngineID("" + segments.getJSONObject(j).getString("engineID"));
						req.setFareRule("" + segments.getJSONObject(j).getString("fareRule"));
						req.setJourneyTime("" + legs.getJSONObject(k).getString("duration"));
						req.setJourneyIndex("" + segments.getJSONObject(j).getString("journeyIndex"));

						JSONArray fare = segments.getJSONObject(j).getJSONArray("fare");
						for (int l = 0; l < fare.length(); l++) {
							JSONArray paxFares = fare.getJSONObject(l).getJSONArray("paxFares");
							JSONArray fares = paxFares.getJSONObject(l).getJSONArray("fares");

							req.setTotalFare("" + paxFares.getJSONObject(l).getString("totalFare"));
							req.setTotalTax("" + paxFares.getJSONObject(l).getString("totalTax"));
							req.setTotalFareWithOutMarkUp(
									"" + fare.getJSONObject(l).getString("totalFareWithOutMarkUp"));
							req.setTotalTaxWithOutMarkUp("" + fare.getJSONObject(l).getString("totalTaxWithOutMarkUp"));
							req.setAmount("" + fares.getJSONObject(l).getString("amount"));

							req.setBasicFare("" + fare.getJSONObject(l).getString("basicFare"));
							req.setCancelPenalty("" + paxFares.getJSONObject(l).getString("cancelPenalty"));
							req.setChangePenalty("" + paxFares.getJSONObject(l).getString("changePenalty"));
							req.setTransactionAmount("" + paxFares.getJSONObject(l).getString("transactionAmount"));
							req.setBaseTransactionAmount(
									"" + paxFares.getJSONObject(l).getString("baseTransactionAmount"));
							req.setTraceId("AYTM00011111111110002");

							req.setExchangeRate("" + fare.getJSONObject(l).getString("exchangeRate"));

							req.setMarkUP("" + paxFares.getJSONObject(l).getString("markUP"));
							req.setPaxType("" + paxFares.getJSONObject(l).getString("paxType"));

						}

					}

				}

				JSONArray fare = segments.getJSONObject(0).getJSONArray("fare");
				JSONArray paxFares = fare.getJSONObject(0).getJSONArray("paxFares");
				JSONArray fares = paxFares.getJSONObject(0).getJSONArray("fares");
				paxFaresjsonobjet.put("baggageUnit", req.getBaggageUnit());
				paxFaresjsonobjet.put("baggageWeight", req.getBaggageWeight());
				paxFaresjsonobjet.put("baseTransactionAmount", 0);
				paxFaresjsonobjet.put("basicFare", req.getBaggageFare());
				paxFaresjsonobjet.put("cancelPenalty", req.getCancelPenalty());
				paxFaresjsonobjet.put("changePenalty", req.getChangePenalty());
				paxFaresjsonobjet.put("equivCurrencyCode", "");
				paxFaresjsonobjet.put("basicFare", req.getBasicFare());
				paxFaresjsonobjet.put("fareBasisCode", "");
				paxFaresjsonobjet.put("fareInfoKey", "");
				paxFaresjsonobjet.put("fareInfoValue", "");
				paxFaresjsonobjet.put("markUP", 0);
				paxFaresjsonobjet.put("paxType", "ADT");
				paxFaresjsonobjet.put("refundable", true);
				paxFaresjsonobjet.put("totalFare", req.getTotalFare());
				paxFaresjsonobjet.put("totalTax", req.getTotalTax());
				paxFaresjsonobjet.put("transactionAmount", 0);
				paxFaresjsonobjet.put("bookFares", fares);
				paxFaresjsonarray.put(paxFaresjsonobjet);

				faresjsonobjet.put("basicFare", req.getBasicFare());
				faresjsonobjet.put("exchangeRate", 0);

				faresjsonobjet.put("totalFareWithOutMarkUp", req.getTotalFareWithOutMarkUp());
				faresjsonobjet.put("totalTaxWithOutMarkUp", req.getTotalTaxWithOutMarkUp());
				faresjsonobjet.put("paxFares", paxFaresjsonarray);

				objhasmap.add("" + req.getEngineID());
				payload.put("clientIp", Inet4Address.getLocalHost().getHostAddress());
				payload.put("cllientKey", MdexConstants.getMdexCredentials().getCode());
				payload.put("clientToken", MdexConstants.getMdexCredentials().getStatus());
				payload.put("clientApiName", "Flight Booking");
				payload.put("spKey", req.getAirlineName() + "IN");

				bondsjsonobjet.put("boundType", "");
				bondsjsonobjet.put("baggageFare", false);
				bondsjsonobjet.put("ssrFare", false);
				bondsjsonobjet.put("itineraryKey", req.getItineraryKey());
				bondsjsonobjet.put("journeyTime", req.getDuration());

				bondsjsonobjet.put("addOnDetail", "");

				JSONArray val = CreateJsonRequestFlight.createlegsarray(legsjsonarray);
				bondsjsonobjet.put("legs", val);

				bondsjsonarray.put(bondsjsonobjet);

				bookSegmentsjsonobjet.put("bonds", bondsjsonarray);
				bookSegmentsjsonobjet.put("deeplink", "");
				bookSegmentsjsonobjet.put("fareIndicator", 0);
				bookSegmentsjsonobjet.put("fareRule", req.getFareRule());
				bookSegmentsjsonobjet.put("baggageFare", false);
				bookSegmentsjsonobjet.put("cache", false);
				bookSegmentsjsonobjet.put("holdBooking", false);
				bookSegmentsjsonobjet.put("international", false);
				bookSegmentsjsonobjet.put("roundTrip", false);
				bookSegmentsjsonobjet.put("special", false);
				bookSegmentsjsonobjet.put("specialId", false);
				bookSegmentsjsonobjet.put("itineraryKey", req.getItineraryKey());
				bookSegmentsjsonobjet.put("journeyIndex", 0);
				bookSegmentsjsonobjet.put("memoryCreationTime", "/Date(1499158249483+0530)/");
				bookSegmentsjsonobjet.put("nearByAirport", false);
				bookSegmentsjsonobjet.put("remark", "");
				bookSegmentsjsonobjet.put("searchId", req.getSearchId());

				bookSegmentsjsonobjet.put("engineID", req.getEngineID());
				bookSegmentsjsonobjet.put("fares", faresjsonobjet);

				bookSegmentsjsonarray.put(bookSegmentsjsonobjet);
			}
			if (Secondflight.equals("Secondflight")) {
				JSONObject faresjsonobjet = new JSONObject();
				JSONObject paxFaresjsonobjet = new JSONObject();
				JSONArray paxFaresjsonarray = new JSONArray();
				JSONObject bookSegmentsjsonobjet = new JSONObject();
				JSONObject obj = new JSONObject(destination);
				JSONObject legsjsonobjet = new JSONObject();
				JSONArray legsjsonarray = new JSONArray();

				JSONObject bondsjsonobjet = new JSONObject();
				JSONArray bondsjsonarray = new JSONArray();

				JSONObject ssrDetailsjsonobjet = new JSONObject();
				JSONArray ssrDetailsjsonarray = new JSONArray();

				JSONArray engineIDList = new JSONArray();

				JSONArray journeys = obj.getJSONObject("details").getJSONArray("journeys");

				JSONArray segments = journeys.getJSONObject(0).getJSONArray("segments");
				for (int j = 0; j < segments.length(); j++) {

					JSONArray bonds = segments.getJSONObject(j).getJSONArray("bonds");
					for (int k = 0; k < bonds.length(); k++) {

						JSONArray legs = bonds.getJSONObject(k).getJSONArray("legs");
						String flightnumber = legs.getJSONObject(k).getString("flightNumber");
						legsjsonarray = bonds.getJSONObject(k).getJSONArray("legs");
						System.out.println("#######################################");
						req.setAirlineNamereturn("" + legs.getJSONObject(k).getString("airlineName"));
						req.setArrivalTimereturn("" + legs.getJSONObject(k).getString("arrivalTime"));
						req.setCabinreturn("" + legs.getJSONObject(k).getString("cabin"));
						req.setDepartureTimereturn("" + legs.getJSONObject(k).getString("departureTime"));
						req.setDestinationreturn("" + legs.getJSONObject(k).getString("destination"));
						req.setDurationreturn("" + legs.getJSONObject(k).getString("duration"));
						req.setBoundTypereturn("" + legs.getJSONObject(k).getString("boundType"));
						req.setFlightNumberreturn("" + legs.getJSONObject(k).getString("flightNumber"));
						// req.setOriginreturn("" +
						// legs.getJSONObject(k).getString("origin"));

						req.setFareBasisCodereturn("" + legs.getJSONObject(k).getString("fareBasisCode"));// fareClassOfService//fareBasisCode
						// req.setBeginDatereturn("" +
						// session.getAttribute("beginDate"));
						req.setFlightNamereturn("" + legs.getJSONObject(k).getString("airlineName"));
						req.setArrivalDatereturn("" + legs.getJSONObject(k).getString("arrivalDate"));
						req.setDepartureDatereturn("" + legs.getJSONObject(k).getString("departureDate"));
						req.setDepartureTerminalreturn("" + legs.getJSONObject(k).getString("departureTerminal"));
						req.setArrivalTerminalreturn("" + legs.getJSONObject(k).getString("arrivalTerminal"));
						req.setCapacityreturn("" + legs.getJSONObject(k).getString("capacity"));
						req.setCarrierCodereturn("" + legs.getJSONObject(k).getString("carrierCode"));
						req.setCurrencyCodereturn("" + legs.getJSONObject(k).getString("currencyCode"));
						req.setBaggageUnitreturn("" + legs.getJSONObject(k).getString("baggageUnit"));
						req.setBaggageWeightreturn("" + legs.getJSONObject(k).getString("baggageWeight"));
						req.setFareClassOfServicereturn("" + legs.getJSONObject(k).getString("fareClassOfService"));
						req.setSoldreturn("" + legs.getJSONObject(k).getString("sold"));
						req.setStatusreturn("" + legs.getJSONObject(k).getString("status"));
						req.setItineraryKeyreturn("" + segments.getJSONObject(j).getString("itineraryKey"));
						req.setSearchIdreturn("" + segments.getJSONObject(j).getString("searchId"));
						req.setEngineIDreturn("" + segments.getJSONObject(j).getString("engineID"));
						req.setFareRulereturn("" + segments.getJSONObject(j).getString("fareRule"));
						req.setJourneyTimereturn("" + legs.getJSONObject(k).getString("duration"));
						req.setJourneyIndexreturn("" + segments.getJSONObject(j).getString("journeyIndex"));

						JSONArray fare = segments.getJSONObject(j).getJSONArray("fare");
						for (int l = 0; l < fare.length(); l++) {
							JSONArray paxFares = fare.getJSONObject(l).getJSONArray("paxFares");
							JSONArray fares = paxFares.getJSONObject(l).getJSONArray("fares");

							req.setTotalFarereturn("" + paxFares.getJSONObject(l).getString("totalFare"));
							req.setTotalTaxreturn("" + paxFares.getJSONObject(l).getString("totalTax"));
							req.setTotalFareWithOutMarkUpreturn(
									"" + fare.getJSONObject(l).getString("totalFareWithOutMarkUp"));
							req.setTotalTaxWithOutMarkUpreturn(
									"" + fare.getJSONObject(l).getString("totalTaxWithOutMarkUp"));
							req.setAmountreturn("" + fares.getJSONObject(l).getString("amount"));

							req.setBasicFarereturn("" + fare.getJSONObject(l).getString("basicFare"));
							req.setCancelPenaltyreturn("" + paxFares.getJSONObject(l).getString("cancelPenalty"));
							req.setChangePenaltyreturn("" + paxFares.getJSONObject(l).getString("changePenalty"));
							req.setTransactionAmountreturn(
									"" + paxFares.getJSONObject(l).getString("transactionAmount"));
							req.setBaseTransactionAmountreturn(
									"" + paxFares.getJSONObject(l).getString("baseTransactionAmount"));
							req.setTraceIdreturn("AYTM00011111111110002");

							req.setExchangeRatereturn("" + fare.getJSONObject(l).getString("exchangeRate"));

							req.setMarkUPreturn("" + paxFares.getJSONObject(l).getString("markUP"));
							req.setPaxTypereturn("" + paxFares.getJSONObject(l).getString("paxType"));

						}

					}

				}
				JSONArray fare = segments.getJSONObject(0).getJSONArray("fare");
				JSONArray paxFares = fare.getJSONObject(0).getJSONArray("paxFares");
				JSONArray fares = paxFares.getJSONObject(0).getJSONArray("fares");
				paxFaresjsonobjet.put("baggageUnit", req.getBaggageUnitreturn());
				paxFaresjsonobjet.put("baggageWeight", req.getBaggageWeightreturn());
				paxFaresjsonobjet.put("baseTransactionAmount", 0);
				paxFaresjsonobjet.put("basicFare", req.getBaggageFarereturn());
				paxFaresjsonobjet.put("cancelPenalty", req.getCancelPenaltyreturn());
				paxFaresjsonobjet.put("changePenalty", req.getChangePenaltyreturn());
				paxFaresjsonobjet.put("equivCurrencyCode", "");
				paxFaresjsonobjet.put("basicFare", req.getBasicFare());
				paxFaresjsonobjet.put("fareBasisCode", "");
				paxFaresjsonobjet.put("fareInfoKey", "");
				paxFaresjsonobjet.put("fareInfoValue", "");
				paxFaresjsonobjet.put("markUP", 0);
				paxFaresjsonobjet.put("paxType", "ADT");
				paxFaresjsonobjet.put("refundable", true);
				paxFaresjsonobjet.put("totalFare", req.getTotalFarereturn());
				paxFaresjsonobjet.put("totalTax", req.getTotalTaxreturn());
				paxFaresjsonobjet.put("transactionAmount", 0);
				paxFaresjsonobjet.put("bookFares", fares);
				paxFaresjsonarray.put(paxFaresjsonobjet);

				faresjsonobjet.put("basicFare", req.getBasicFare());
				faresjsonobjet.put("exchangeRate", 0);

				faresjsonobjet.put("totalFareWithOutMarkUp", req.getTotalFareWithOutMarkUpreturn());
				faresjsonobjet.put("totalTaxWithOutMarkUp", req.getTotalTaxWithOutMarkUpreturn());
				faresjsonobjet.put("paxFares", paxFaresjsonarray);

				payload.put("spKey", req.getAirlineName() + "IN");

				bondsjsonobjet.put("boundType", "");
				bondsjsonobjet.put("baggageFare", false);
				bondsjsonobjet.put("ssrFare", false);
				bondsjsonobjet.put("itineraryKey", req.getItineraryKeyreturn());
				bondsjsonobjet.put("journeyTime", req.getDurationreturn());

				bondsjsonobjet.put("addOnDetail", "");
				objhasmap1.add(req.getEngineIDreturn());

				JSONArray val = CreateJsonRequestFlight.createlegsarray(legsjsonarray);
				bondsjsonobjet.put("legs", val);

				bondsjsonarray.put(bondsjsonobjet);

				bookSegmentsjsonobjet.put("bonds", bondsjsonarray);
				bookSegmentsjsonobjet.put("deeplink", "");
				bookSegmentsjsonobjet.put("fareIndicator", 0);
				bookSegmentsjsonobjet.put("fareRule", req.getFareRule());
				bookSegmentsjsonobjet.put("baggageFare", false);
				bookSegmentsjsonobjet.put("cache", false);
				bookSegmentsjsonobjet.put("holdBooking", false);
				bookSegmentsjsonobjet.put("international", false);
				bookSegmentsjsonobjet.put("roundTrip", false);
				bookSegmentsjsonobjet.put("special", false);
				bookSegmentsjsonobjet.put("specialId", false);

				bookSegmentsjsonobjet.put("itineraryKey", req.getItineraryKey());

				bookSegmentsjsonobjet.put("journeyIndex", 0);
				bookSegmentsjsonobjet.put("memoryCreationTime", "/Date(1499158249483+0530)/");
				bookSegmentsjsonobjet.put("nearByAirport", false);
				bookSegmentsjsonobjet.put("remark", "");
				bookSegmentsjsonobjet.put("searchId", req.getSearchIdreturn());

				bookSegmentsjsonobjet.put("engineID", req.getEngineID());
				bookSegmentsjsonobjet.put("fares", faresjsonobjet);

				bookSegmentsjsonarray.put(bookSegmentsjsonobjet);
			}

			payload.put("bookSegments", bookSegmentsjsonarray);

			flightSearchDetailsjsonobject.put("beginDate", req.getBeginDate());
			flightSearchDetailsjsonobject.put("destination", "" + session.getAttribute("destination"));

			flightSearchDetailsjsonobject.put("origin", req.getOrigin());
			flightSearchDetailsaray.add(flightSearchDetailsjsonobject);
			JSONObject roundtripDetailsobj = new JSONObject();

			roundtripDetailsobj.put("origin", "" + session.getAttribute("destination"));
			roundtripDetailsobj.put("destination", req.getOrigin());
			roundtripDetailsobj.put("beginDate", req.getEndDate());

			flightSearchDetailsaray.add(roundtripDetailsobj);

			payload.put("flightSearchDetails", flightSearchDetailsaray);

			paymentDetails.put("bookingAmount", req.getGrandtotal());
			paymentDetails.put("bookingCurrencyCode", "INR");
			payload.put("paymentDetails", paymentDetails);

			payload.put("engineIDList", objhasmap1);

			JSONObject adultTravellersnew = new JSONObject();

			JSONArray adultTravellers = CreateJsonRequestFlight.createReturnjsonAdultraveler(req, source);
			JSONArray childTravellers = CreateJsonRequestFlight.createReturnjsonChildraveler(req, source);
			JSONArray infantTravellers = CreateJsonRequestFlight.createReturnjsonInfantrevaleer(req, source);

			adultTravellersnew.put("adultTravellers", adultTravellers);
			adultTravellersnew.put("childTravellers", childTravellers);
			adultTravellersnew.put("infantTravellers", infantTravellers);

			payload.put("travellers", adultTravellersnew);
			payload.put("visatype", "Employee Visa");
			payload.put("traceId", "AYTM00011111111110001");
			payload.put("transactionId", req.getTransactionId());
			payload.put("androidBooking", true);
			payload.put("domestic", true);
			payload.put("engineID", req.getEngineIDreturn());
			return payload;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public static org.json.JSONObject createMobilejsonForTicket(AirBookRQ req) throws Exception {
		org.json.JSONArray oneway = new org.json.JSONArray();
		org.json.JSONArray roundway = new org.json.JSONArray();
		org.json.JSONObject objmain = new org.json.JSONObject();
		org.json.JSONObject legs = new org.json.JSONObject();
		double basicFare = 0;
		try {
			if (req.getBookSegments() != null) {
				for (int i = 0; i < req.getBookSegments().size(); i++) {
					if (i == 0) {
						List<Bonds> bonds = req.getBookSegments().get(i).getBonds();
						for (int j = 0; j < bonds.size(); j++) {
							if (j == 0) {
								Bonds bondOne = bonds.get(j);
								List<Legs> legsArr = bondOne.getLegs();
								for (int k = 0; k < legsArr.size(); k++) {
									org.json.simple.JSONObject payload1 = new org.json.simple.JSONObject();
									payload1.put("journeyTime", bondOne.getJourneyTime());
									payload1.put("cabin", legsArr.get(k).getCabin());
									payload1.put("airlineName", legsArr.get(k).getAirlineName());
									payload1.put("arrivalDate", legsArr.get(k).getArrivalDate());
									payload1.put("arrivalTime", legsArr.get(k).getArrivalTime());
									payload1.put("baggageUnit", legsArr.get(k).getBaggageUnit());
									payload1.put("baggageWeight", legsArr.get(k).getBaggageWeight());
									payload1.put("departureDate", legsArr.get(k).getDepartureDate());
									payload1.put("departureTime", legsArr.get(k).getDepartureTime());
									payload1.put("duration", legsArr.get(k).getDuration());
									payload1.put("destination", legsArr.get(k).getDestination());
									payload1.put("origin", legsArr.get(k).getOrigin());
									payload1.put("flightNumber", legsArr.get(k).getFlightNumber());
									StringWriter v = new StringWriter();
									v.write(payload1.toString());
									payload1.writeJSONString(v);
									oneway.put(payload1);
								}
							} else {
								Bonds bondOne = bonds.get(j);
								List<Legs> legsArr = bondOne.getLegs();
								for (int k = 0; k < legsArr.size(); k++) {
									org.json.simple.JSONObject payload1 = new org.json.simple.JSONObject();
									payload1.put("journeyTime", bondOne.getJourneyTime());
									payload1.put("cabin", legsArr.get(k).getCabin());
									payload1.put("airlineName", legsArr.get(k).getAirlineName());
									payload1.put("arrivalDate", legsArr.get(k).getArrivalDate());
									payload1.put("arrivalTime", legsArr.get(k).getArrivalTime());
									payload1.put("baggageUnit", legsArr.get(k).getBaggageUnit());
									payload1.put("baggageWeight", legsArr.get(k).getBaggageWeight());
									payload1.put("departureDate", legsArr.get(k).getDepartureDate());
									payload1.put("departureTime", legsArr.get(k).getDepartureTime());
									payload1.put("duration", legsArr.get(k).getDuration());
									payload1.put("destination", legsArr.get(k).getDestination());
									payload1.put("origin", legsArr.get(k).getOrigin());
									payload1.put("flightNumber", legsArr.get(k).getFlightNumber());
									StringWriter v = new StringWriter();
									v.write(payload1.toString());
									payload1.writeJSONString(v);
									roundway.put(payload1);

								}

							}
						}
						basicFare = req.getBookSegments().get(0).getFares().getBasicFare();
						System.err.println("basic fare ::" + basicFare);
					} else {
						List<Bonds> bonds = req.getBookSegments().get(i).getBonds();
						for (int j = 0; j < bonds.size(); j++) {
							Bonds bondOne = bonds.get(j);
							List<Legs> legsArr = bondOne.getLegs();
							for (int k = 0; k < legsArr.size(); k++) {
								org.json.simple.JSONObject payload1 = new org.json.simple.JSONObject();
								payload1.put("journeyTime", bondOne.getJourneyTime());
								payload1.put("cabin", legsArr.get(k).getCabin());
								payload1.put("airlineName", legsArr.get(k).getAirlineName());
								payload1.put("arrivalDate", legsArr.get(k).getArrivalDate());
								payload1.put("arrivalTime", legsArr.get(k).getArrivalTime());
								payload1.put("baggageUnit", legsArr.get(k).getBaggageUnit());
								payload1.put("baggageWeight", legsArr.get(k).getBaggageWeight());
								payload1.put("departureDate", legsArr.get(k).getDepartureDate());
								payload1.put("departureTime", legsArr.get(k).getDepartureTime());
								payload1.put("duration", legsArr.get(k).getDuration());
								payload1.put("destination", legsArr.get(k).getDestination());
								payload1.put("origin", legsArr.get(k).getOrigin());
								payload1.put("flightNumber", legsArr.get(k).getFlightNumber());
								StringWriter v = new StringWriter();
								v.write(payload1.toString());
								payload1.writeJSONString(v);
								roundway.put(payload1);
							}
						}
						System.err.println("roundway fare ::" + req.getBookSegments().get(0).getFares().getBasicFare());
						basicFare = basicFare + req.getBookSegments().get(0).getFares().getBasicFare();
						System.err.println("round up figure ::" + basicFare);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}

		legs.put("Oneway", oneway);
		legs.put("Roundway", roundway);
		objmain.put("Tickets", legs);
		objmain.put("basicFare", basicFare);

		return objmain;

	}

}
