package com.msscard.model;

import java.util.List;

import com.msscard.entity.BusTicket;
import com.msscard.entity.TravellerDetails;

public class BusTicketResp {
	
	private List<TravellerDetails> travellerDetails;
	
	private BusTicket busTicket;

	public List<TravellerDetails> getTravellerDetails() {
		return travellerDetails;
	}

	public void setTravellerDetails(List<TravellerDetails> travellerDetails) {
		this.travellerDetails = travellerDetails;
	}

	public BusTicket getBusTicket() {
		return busTicket;
	}

	public void setBusTicket(BusTicket busTicket) {
		this.busTicket = busTicket;
	}

}
