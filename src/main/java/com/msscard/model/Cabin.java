package com.msscard.model;

public enum Cabin {

	Economy("Economy"), RoundTrip("RoundTrip"), Business("Business");

	 private final String value;

	 private Cabin(String value) {
	  this.value = value;
	 }

	 @Override
	 public String toString() {
	  return value;
	 }

	 public String getValue() {
	  return value;
	 }

	 public static Cabin getEnum(String value) {
	  if (value == null)
	   throw new IllegalArgumentException();
	  for (Cabin v : values())
	   if (value.equalsIgnoreCase(v.getValue()))
	    return v;
	  throw new IllegalArgumentException();
	 }

}
