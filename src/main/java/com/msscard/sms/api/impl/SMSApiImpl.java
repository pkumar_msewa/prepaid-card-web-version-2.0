package com.msscard.sms.api.impl;

import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.msscard.app.model.request.SMSRequest;
import com.msscard.app.model.response.SMSExecution;
import com.msscard.app.model.response.SMSResponse;
import com.msscard.sms.api.ISMSApi;
import com.msscard.sms.constant.SMSConstant;
import com.msscard.util.JSONParserUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class SMSApiImpl implements ISMSApi{

	@Override
	public SMSExecution sendSMS(SMSRequest dto) {
		// TODO Auto-generated method stub
		SMSExecution result = new SMSExecution();
		JSONObject payload = new JSONObject();
		try {
			payload.put("from", SMSConstant.FROM);
			payload.put("to", "91"+dto.getDestination());
			payload.put("text", dto.getMessage());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(SMSConstant.SMS_URL);
			ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON).type(MediaType.APPLICATION_JSON)
					.header("Authorization", SMSConstant.getAuthorizationHeader())
					.post(ClientResponse.class, payload.toString());
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() == 200) {
				result.setSuccess(true);
				JSONObject json = new JSONObject(strResponse);
				if (json != null) {
					JSONArray messages = JSONParserUtil.getArray(json, "messages");
					if (messages != null && messages.length() > 0) {
						for (int i = 0; i < messages.length(); i++) {
							JSONObject temp = messages.getJSONObject(i);
							JSONObject status = temp.getJSONObject("status");
							if (temp != null && temp.getString("to").equalsIgnoreCase(dto.getTo())) {
								result.setMessageId(JSONParserUtil.getString(temp, "messageId"));

							}
							if (status != null) {
								result.setStatus(JSONParserUtil.getString(status, "groupName"));
							}
						}
					}
				}
			}
			System.out.println("response--" + result.getMessageId());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	@Override
	public SMSResponse verifyDelivery(String messageId) {
		// TODO Auto-generated method stub
		SMSResponse result = new SMSResponse();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(SMSConstant.REPORT_URL).queryParam("messageId", messageId);
			ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON).type(MediaType.APPLICATION_JSON)
					.header("Authorization", SMSConstant.getAuthorizationHeader()).get(ClientResponse.class);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() == 200) {
				result.setSuccess(true);
				JSONObject json = new JSONObject(strResponse);
				if (json != null) {
					JSONArray results = JSONParserUtil.getArray(json, "results");
					if (results != null && results.length() > 0) {
						for (int i = 0; i < results.length(); i++) {
							JSONObject temp = results.getJSONObject(i);
							if (temp != null && temp.getString("messageId").equalsIgnoreCase(messageId)) {
								result.setMessageId(JSONParserUtil.getString(temp, "messageId"));
								result.setTo(JSONParserUtil.getString(temp, "to"));
								result.setStatus(JSONParserUtil.getObject(temp, "status").getString("groupName"));
							}
						}
					}
				}
			}
			System.out.println("response--" + result.getMessageId() + "-----" + result.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}


	
}
