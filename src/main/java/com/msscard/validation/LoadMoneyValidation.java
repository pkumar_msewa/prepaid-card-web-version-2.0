package com.msscard.validation;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.ewire.paramerterization.DataConfig;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.ITransactionApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.MatchMoveCreateCardRequest;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.entity.MCommission;
import com.msscard.entity.MMCards;
import com.msscard.entity.MPQAccountDetails;
import com.msscard.entity.MPQAccountType;
import com.msscard.entity.MService;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.entity.TwoMinBlock;
import com.msscard.entity.UserSession;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Status;
import com.msscard.model.error.CardValidationError;
import com.msscard.model.error.TransactionError;
import com.msscard.repositories.DataConfigRepository;
import com.msscard.repositories.MCommissionRepository;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MServiceRepository;
import com.msscard.repositories.MUserRespository;
import com.msscard.repositories.TwoMinBlockRepository;
import com.msscard.repositories.UserSessionRepository;
import com.msscard.util.CommonUtil;

public class LoadMoneyValidation {

	private final ITransactionApi transactionApi;
	private final IUserApi userApi;
	private final MCommissionRepository commissionRepository;
	private final MMCardRepository cardRepository;
	private final DataConfigRepository dataConfigRepository;	
	private final MUserRespository mUserRespository;
	private final TwoMinBlockRepository twoMinBlockRepository;
	private final MMCardRepository mMCardRepository;
	private final IMatchMoveApi matchMoveApi;
	private final UserSessionRepository userSessionRepository;
	private final MCommissionRepository mCommissionRepository;
	
	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	public LoadMoneyValidation(ITransactionApi transactionApi,IUserApi userApi,
			MCommissionRepository commissionRepository,MMCardRepository cardRepository,DataConfigRepository dataConfigRepository
			,MUserRespository mUserRespository, TwoMinBlockRepository twoMinBlockRepository, MMCardRepository mMCardRepository,
			IMatchMoveApi matchMoveApi, UserSessionRepository userSessionRepository, MCommissionRepository mCommissionRepository) {
		super();
		this.transactionApi = transactionApi;
		this.userApi=userApi;
		this.commissionRepository=commissionRepository;
		this.cardRepository=cardRepository;
		this.dataConfigRepository=dataConfigRepository;
		this.mUserRespository = mUserRespository;
		this.twoMinBlockRepository = twoMinBlockRepository;
		this.mMCardRepository = mMCardRepository;
		this.matchMoveApi = matchMoveApi;
		this.userSessionRepository = userSessionRepository;
		this.mCommissionRepository = mCommissionRepository;
	}
	
	public TransactionError validateLoadMoneyTransaction(String amount, String senderUsername, MService service) {
		TransactionError error = new TransactionError();
		DataConfig configDatas=dataConfigRepository.findDatas();
		boolean valid = true;
		MUser senderUser = userApi.findByUserName(senderUsername);
		MMCards virCard=cardRepository.getVirtualCardsByCardUser(senderUser);
		MMCards phyCard=cardRepository.getPhysicalCardByUser(senderUser);
		
		/*boolean accCheck=senderUser.getAccountDetail().getAccountType().getCode().equalsIgnoreCase("KYC");
		if(accCheck==false){
			valid=false;
			error.setMessage("Please upgrade your account to KYC");
			error.setValid(valid);
		}*/
		
		MCommission comm=commissionRepository.findCommissionByService(service);
		double finalTransactionAmount=Double.parseDouble(amount);

		if(comm!=null){
			double commis=CommonUtil.commissionEarned(comm,Double.parseDouble(amount));
			finalTransactionAmount=commis+Double.parseDouble(amount);
			BigDecimal a = new BigDecimal(String.valueOf(finalTransactionAmount));
			BigDecimal roundOff= a.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			finalTransactionAmount=roundOff.doubleValue();
		}
		
		if (senderUser.getMobileStatus().equals(Status.Inactive)) {
			error.setMessage("Please verify your mobile " + senderUser.getUserDetail().getContactNo()
					+ " before any transaction");
			error.setValid(false);
			return error;
		}
		
		if(!(senderUser.getAccountDetail().getAccountType().getCode().equals("KYC")) && Double.valueOf(amount) > service.getMaxAmount()) {
			error.setMessage("User needs to be Full KYC to load money of above "+service.getMaxAmount());
			error.setValid(false);
			return error;
		}
		
		if(service.getStatus().getValue().equalsIgnoreCase("Inactive")) {
			valid=false;
			error.setValid(false);
			error.setMessage("Service is temporarily down");
			return error;
		}
		double transactionAmount=finalTransactionAmount;
		double monthlyTransactionLimit = senderUser.getAccountDetail().getAccountType().getMonthlyLimit();
		double dailyTransactionLimit = senderUser.getAccountDetail().getAccountType().getDailyLimit();
		double totalCreditMonthly = transactionApi.getMonthlyCreditTransationTotalAmount(senderUser.getAccountDetail());
		double totalDailyCredit=transactionApi.getDailyCreditTransationTotalAmount(senderUser.getAccountDetail());
		double virtualCardMonthlyLimit=configDatas.getVirtualCardLoadMonthlyLimit();
		double virtualCardDailyLimit=configDatas.getVirtualCardLoadDailyLimit();
		
		System.err.println("monthlytransaction limit :: "+monthlyTransactionLimit);
		System.err.println("dailyTransactionLimit limit :: "+dailyTransactionLimit);
		System.err.println("totalCreditMonthly :: "+totalCreditMonthly);
		System.err.println("totalDailyCredit :: "+totalDailyCredit);
		System.err.println("virtualCardMonthlyLimit :: "+virtualCardMonthlyLimit);
		System.err.println("virtualCardDailyLimit :: "+virtualCardDailyLimit);
		
		 if (!CommonValidation.monthlyCreditLimitCheck(monthlyTransactionLimit, totalCreditMonthly,
				transactionAmount)) {
			error.setMessage("Monthly Credit Limit Exceeded. Your monthly limit is Rs." + monthlyTransactionLimit
					+ " and you've already credited total of Rs." + totalCreditMonthly);
			valid = false;
			error.setValid(valid);
			return error;
		}
		 
		 if (!CommonValidation.dailyCreditLimitCheck(dailyTransactionLimit, totalDailyCredit,
					transactionAmount)) {
				error.setMessage("Daily Credit Limit Exceeded. Your Daily limit is Rs." + dailyTransactionLimit
						+ " and you've already credited total of Rs." + totalDailyCredit);
				valid = false;
				error.setValid(valid);
				return error;
			}
		 if(virCard!=null && virCard.getStatus().equalsIgnoreCase("Active")){
			 
			 if (!CommonValidation.dailyCreditLimitCheck(virtualCardDailyLimit, totalDailyCredit,
						transactionAmount)) {
					error.setMessage("Daily Credit Limit Exceeded For Virtual Card. Your Daily limit is Rs." + virtualCardDailyLimit
							+ " and you've already credited total of Rs." + totalDailyCredit);
					valid = false;
					error.setValid(valid);
					return error;
				}
			 
			 if (!CommonValidation.monthlyCreditLimitCheck(virtualCardMonthlyLimit, totalCreditMonthly,
						transactionAmount)) {
					error.setMessage("Monthly Credit Limit Exceeded for your Virtual Card. Your monthly limit is Rs." + virtualCardMonthlyLimit
							+ " and you've already credited total of Rs." + totalCreditMonthly);
					valid = false;
					error.setValid(valid);
					return error;
				}
			 
		 }
		 double amt  = Double.parseDouble(amount);
		  if(!CommonValidation.isNumeric(amount)){
				error.setMessage("Please enter valid amount");
				valid = false;
			}

			if(amt < service.getMinAmount()|| amt >= service.getMaxAmount()) {
				error.setMessage("Amount must be between "+service.getMinAmount()+" to "+service.getMaxAmount());
				valid = false;
			}
			 if(phyCard!=null && phyCard.getStatus().equalsIgnoreCase("Inactive")){
			valid=false;
			error.setValid(valid);
			error.setMessage("Load Money for your account has been deactivated.Please contact customer care");
			return error;
		 }else if(virCard!=null && virCard.getStatus().equalsIgnoreCase("Inactive")){
			 if(phyCard==null){
			 valid=false;
			 error.setValid(valid);
			 error.setMessage("Load Money for your account has been deactivated.Please contact customer care");
			 return error;
			 }
		 }
		 
		 Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail(), false);
			if (date != null) {
				Date currentDate = new Date();
				String lastTransactionDate = sdf.format(date);
				String currentTransactionDate = sdf.format(currentDate);
				if (lastTransactionDate.equalsIgnoreCase(currentTransactionDate)) {
					error.setMessage("Please wait for current transaction to complete");
					// userApi.blockUser(senderUsername);
					error.setValid(false);
					return error;
				}
			}

			if (date != null) {
				long lastTransactionTimeStamp = date.getTime();
				long currentTimeInMillis = System.currentTimeMillis();
				if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 1) {
					error.setMessage("Please wait");
					error.setValid(false);
					return error;
				}
			}

			int count = 0;
			Date reverseTransactionDate = transactionApi.getLastTransactionTimeStampByStatus(senderUser.getAccountDetail(),
					Status.Success);
			if (reverseTransactionDate != null) {
				long timestamp = reverseTransactionDate.getTime();
				long currentTime = System.currentTimeMillis();
				if ((currentTime - timestamp) < 1000 * 60 * 2) {
					MTransaction tran = transactionApi.getLastTransactionDetails(reverseTransactionDate, senderUser.getAccountDetail(), false);
					double prevAmount = tran.getAmount();
					UserSession session = userSessionRepository.findByActiveUserSessions(senderUser);

					error.setMessage("Please try again after 2 mins");
					if(prevAmount == finalTransactionAmount) {
						++count;
					}
					
					if(count == 5) {
						senderUser.setAuthority("ROLE_USER,ROLE_BLOCKED");
						mUserRespository.save(senderUser);
							
						TwoMinBlock minBlock = new TwoMinBlock();
						minBlock.setBlock(true);
						minBlock.setUser(senderUser);
						minBlock.setStatus(Status.Blocked.getValue());
						twoMinBlockRepository.save(minBlock);
						
						String activeCardId=null;
						List<MMCards> cardList=mMCardRepository.getCardsListByUser(senderUser);
						if(cardList!=null && cardList.size()>0) {
						for (MMCards mmCards : cardList) {
							if(mmCards.isHasPhysicalCard()){
								if(mmCards.getStatus().equalsIgnoreCase("Active") || !mmCards.isBlocked()){
									activeCardId=mmCards.getCardId();
								}
							} else {
								if(mmCards.getStatus().equalsIgnoreCase("Active") || !mmCards.isBlocked()){
									activeCardId=mmCards.getCardId();
									mmCards.setStatus(Status.Inactive.getValue());
									mMCardRepository.save(mmCards);
								}
							}
						}
						MatchMoveCreateCardRequest request = new MatchMoveCreateCardRequest();
						request.setRequestType("suspend");
						request.setCardId(activeCardId);
						ResponseDTO res = matchMoveApi.deActivateCards(request);
						if(res.getCode().equalsIgnoreCase("S00")) {
							error.setMessage(res.getMessage());
							error.setValid(false);
							return error;
						}
						} else {
							error.setMessage("Failure");						
							error.setValid(false);
							return error;
						}
						
					}
					
					error.setValid(false);
					return error;
				}
			}
			
		 
	/*	 if(virCard!=null && virCard.getStatus().equalsIgnoreCase("Active")){
			 valid=false;
			 error.setValid(valid);
			 error.setMessage("Load money to virtual card has been disabled.Please Apply for physical card to continue");
			 return error;
		 }*/
		 
		 error.setValid(valid);
		return error;
	}
	
	public TransactionError validateLoadMoneyTransactionUpi(String amount, String senderUsername, MService service, String address) {
		TransactionError error = new TransactionError();
		DataConfig configDatas=dataConfigRepository.findDatas();
		boolean valid = true;
		MUser senderUser = userApi.findByUserName(senderUsername);
		MMCards virCard=cardRepository.getVirtualCardsByCardUser(senderUser);
		MMCards phyCard=cardRepository.getPhysicalCardByUser(senderUser);
		
		/*boolean accCheck=senderUser.getAccountDetail().getAccountType().getCode().equalsIgnoreCase("KYC");
		if(accCheck==false){
			valid=false;
			error.setMessage("Please upgrade your account to KYC");
			error.setValid(valid);
		}*/
		
		MCommission comm=commissionRepository.findCommissionByService(service);
		double finalTransactionAmount=Double.parseDouble(amount);

		if(comm!=null){
			double commis=CommonUtil.commissionEarned(comm,Double.parseDouble(amount));
			finalTransactionAmount=commis+Double.parseDouble(amount);
			BigDecimal a = new BigDecimal(String.valueOf(finalTransactionAmount));
			BigDecimal roundOff= a.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			finalTransactionAmount=roundOff.doubleValue();
		}
		
		if (senderUser.getMobileStatus().equals(Status.Inactive)) {
			error.setMessage("Please verify your mobile " + senderUser.getUserDetail().getContactNo()
					+ " before any transaction");
			error.setValid(false);
			return error;
		}
		
		if(!(senderUser.getAccountDetail().getAccountType().getCode().equals("KYC")) && Double.parseDouble(amount) > service.getMaxAmount()) {
			error.setMessage("User needs to be Full KYC to load money of above "+service.getMaxAmount());
			error.setValid(false);
			return error;
		}
		
		if(service.getStatus().getValue().equalsIgnoreCase("Inactive")) {
			valid=false;
			error.setValid(false);
			error.setMessage("Service is temporarily down");
			return error;
		}
		double transactionAmount=finalTransactionAmount;
		double monthlyTransactionLimit = senderUser.getAccountDetail().getAccountType().getMonthlyLimit();
		double dailyTransactionLimit = senderUser.getAccountDetail().getAccountType().getDailyLimit();
		double totalCreditMonthly = transactionApi.getMonthlyCreditTransationTotalAmount(senderUser.getAccountDetail());
		double totalDailyCredit=transactionApi.getDailyCreditTransationTotalAmount(senderUser.getAccountDetail());
		double virtualCardMonthlyLimit=configDatas.getVirtualCardLoadMonthlyLimit();
		double virtualCardDailyLimit=configDatas.getVirtualCardLoadDailyLimit();
		
		System.err.println("monthlytransaction limit :: "+monthlyTransactionLimit);
		System.err.println("dailyTransactionLimit limit :: "+dailyTransactionLimit);
		System.err.println("totalCreditMonthly :: "+totalCreditMonthly);
		System.err.println("totalDailyCredit :: "+totalDailyCredit);
		System.err.println("virtualCardMonthlyLimit :: "+virtualCardMonthlyLimit);
		System.err.println("virtualCardDailyLimit :: "+virtualCardDailyLimit);
		
		 if (!CommonValidation.monthlyCreditLimitCheck(monthlyTransactionLimit, totalCreditMonthly,
				transactionAmount)) {
			error.setMessage("Monthly Credit Limit Exceeded. Your monthly limit is Rs." + monthlyTransactionLimit
					+ " and you've already credited total of Rs." + totalCreditMonthly);
			valid = false;
			error.setValid(valid);
			return error;
		}
		 
		 if (!CommonValidation.dailyCreditLimitCheck(dailyTransactionLimit, totalDailyCredit,
					transactionAmount)) {
				error.setMessage("Daily Credit Limit Exceeded. Your Daily limit is Rs." + dailyTransactionLimit
						+ " and you've already credited total of Rs." + totalDailyCredit);
				valid = false;
				error.setValid(valid);
				return error;
			}
		 if(virCard!=null && virCard.getStatus().equalsIgnoreCase("Active")){
			 
			 if (!CommonValidation.dailyCreditLimitCheck(virtualCardDailyLimit, totalDailyCredit,
						transactionAmount)) {
					error.setMessage("Daily Credit Limit Exceeded For Virtual Card. Your Daily limit is Rs." + virtualCardDailyLimit
							+ " and you've already credited total of Rs." + totalDailyCredit);
					valid = false;
					error.setValid(valid);
					return error;
				}
			 
			 if (!CommonValidation.monthlyCreditLimitCheck(virtualCardMonthlyLimit, totalCreditMonthly,
						transactionAmount)) {
					error.setMessage("Monthly Credit Limit Exceeded for your Virtual Card. Your monthly limit is Rs." + virtualCardMonthlyLimit
							+ " and you've already credited total of Rs." + totalCreditMonthly);
					valid = false;
					error.setValid(valid);
					return error;
				}
			 
		 }
		 double amt  = Double.parseDouble(amount);
		  if(!CommonValidation.isNumeric(amount)){
				error.setMessage("Please enter valid amount");
				valid = false;
			}

			if(amt < service.getMinAmount()|| amt >= service.getMaxAmount()) {
				error.setMessage("Amount must be between "+service.getMinAmount()+" to "+service.getMaxAmount());
				valid = false;
			}
			/*if(amt <= Double.parseDouble(configDatas.getUpiLoadMin()) || amt >= service.getMaxAmount()) {
				error.setMessage("Amount must be between "+ configDatas.getUpiLoadMin()+" to "+service.getMaxAmount());
				valid  = false;
			}*/
		 if(phyCard!=null && phyCard.getStatus().equalsIgnoreCase("Inactive")){
			valid=false;
			error.setValid(valid);
			error.setMessage("Load Money for your account has been deactivated.Please contact customer care");
			return error;
		 }else if(virCard!=null && virCard.getStatus().equalsIgnoreCase("Inactive")){
			 if(phyCard==null){
			 valid=false;
			 error.setValid(valid);
			 error.setMessage("Load Money for your account has been deactivated.Please contact customer care");
			 return error;
			 }
		 }
		 
		 Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail(), false);
			if (date != null) {
				Date currentDate = new Date();
				String lastTransactionDate = sdf.format(date);
				String currentTransactionDate = sdf.format(currentDate);
				if (lastTransactionDate.equalsIgnoreCase(currentTransactionDate)) {
					error.setMessage("Please wait for current transaction to complete");
					// userApi.blockUser(senderUsername);
					error.setValid(false);
					return error;
				}
			}

			if (date != null) {
				long lastTransactionTimeStamp = date.getTime();
				long currentTimeInMillis = System.currentTimeMillis();
				if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 1) {
					error.setMessage("Please wait");
					error.setValid(false);
					return error;
				}
			}

			int count;
			Date reverseTransactionDate = transactionApi.getLastTransactionTimeStampByStatus(senderUser.getAccountDetail(),
					Status.Success);
			if (reverseTransactionDate != null) {
				long timestamp = reverseTransactionDate.getTime();
				long currentTime = System.currentTimeMillis();
				if ((currentTime - timestamp) < 1000 * 60 * 2) {
					MTransaction tran = transactionApi.getLastTransactionDetails(reverseTransactionDate, senderUser.getAccountDetail(), false);
					double prevAmount = tran.getAmount();
					UserSession session = userSessionRepository.findByActiveUserSessions(senderUser);

					error.setMessage("Please try again after 2 mins");
					
					
					TwoMinBlock minBlock = twoMinBlockRepository.getBlockedUser(senderUser);
					if(minBlock == null) {
						if(prevAmount == finalTransactionAmount && session.getIpAddress().equalsIgnoreCase(address)) {
							minBlock = new TwoMinBlock();
							minBlock.setCounting(1);
							minBlock.setUser(senderUser);
							twoMinBlockRepository.save(minBlock);
						}
					} else {
						if(prevAmount == finalTransactionAmount && session.getIpAddress().equalsIgnoreCase(address)) {
							count = minBlock.getCounting() + 1;
							minBlock.setCounting(count);
							twoMinBlockRepository.save(minBlock);
						}
					}						
					
					
					if(minBlock.getCounting() == 5) {
						senderUser.setAuthority("ROLE_USER,ROLE_BLOCKED");
						mUserRespository.save(senderUser);
							
						
						minBlock.setBlock(true);						
						//minBlock.setStatus(Status.Blocked.getValue());
						twoMinBlockRepository.save(minBlock);
						
						String activeCardId=null;
						List<MMCards> cardList=mMCardRepository.getCardsListByUser(senderUser);
						if(cardList!=null && cardList.size()>0) {
						for (MMCards mmCards : cardList) {
							if(mmCards.isHasPhysicalCard()){
								if(mmCards.getStatus().equalsIgnoreCase("Active") || !mmCards.isBlocked()){
									activeCardId=mmCards.getCardId();
								}
							} else {
								if(mmCards.getStatus().equalsIgnoreCase("Active") || !mmCards.isBlocked()){
									activeCardId=mmCards.getCardId();
									mmCards.setStatus(Status.Inactive.getValue());
									mMCardRepository.save(mmCards);
								}
							}
						}
						MatchMoveCreateCardRequest request = new MatchMoveCreateCardRequest();
						request.setRequestType("suspend");
						request.setCardId(activeCardId);
						ResponseDTO res = matchMoveApi.deActivateCards(request);
						if(res.getCode().equalsIgnoreCase("S00")) {
							error.setMessage(res.getMessage());
							error.setValid(false);
							return error;
						}
						} else {
							error.setMessage("Failure");						
							error.setValid(false);
							return error;
						}
						
					}
					
					error.setValid(false);
					return error;
				}
			}
			
		 
	/*	 if(virCard!=null && virCard.getStatus().equalsIgnoreCase("Active")){
			 valid=false;
			 error.setValid(valid);
			 error.setMessage("Load money to virtual card has been disabled.Please Apply for physical card to continue");
			 return error;
		 }*/
		 
		 error.setValid(valid);
		return error;
	}
	
	public TransactionError validateMdexTimestampTransaction(double amount, String senderUsername) {
		
		TransactionError error = new TransactionError();		
		boolean valid = true;
		MUser senderUser = userApi.findByUserName(senderUsername);
		
		Date reverseTransactionDate = transactionApi.getLastTransactionTimeStampByStatus(senderUser.getAccountDetail(),
				Status.Refunded);
		if (reverseTransactionDate != null) {
			long timestamp = reverseTransactionDate.getTime();
			long currentTime = System.currentTimeMillis();
			if ((currentTime - timestamp) < 1000 * 60 * 2) {
				error.setMessage("Your last transaction was reversed. Please try again after 1 minute");
				
				error.setValid(false);
				return error;
			}
		}
		error.setValid(valid);
		return error;
	}
	
	
	public TransactionError validateLoadCardTransaction(String amount, String senderUsername, MService service) {
		TransactionError error = new TransactionError();
		boolean valid = true;
		MUser senderUser = userApi.findByUserName(senderUsername);
		/*boolean accCheck=senderUser.getAccountDetail().getAccountType().getCode().equalsIgnoreCase("KYC");
		if(accCheck==false){
			valid=false;
			error.setMessage("Please upgrade your account to KYC");
			error.setValid(valid);
		}*/
		if (senderUser.getMobileStatus().equals(Status.Inactive)) {
			error.setMessage("Please verify User mobile " + senderUser.getUserDetail().getContactNo()
					+ " before any transaction");
			error.setValid(false);
			return error;
		}
		
		if(service.getStatus().getValue().equalsIgnoreCase("Inactive")){
			valid=false;
			error.setValid(false);
			error.setMessage("Service is temporarily down");
			return error;
		}
		double transactionAmount=Double.parseDouble(amount);
		double monthlyTransactionLimit = senderUser.getAccountDetail().getAccountType().getMonthlyLimit();
		double dailyTransactionLimit = senderUser.getAccountDetail().getAccountType().getDailyLimit();
		double totalCreditMonthly = transactionApi.getMonthlyCreditTransationTotalAmount(senderUser.getAccountDetail());
		double totalDailyCredit=transactionApi.getDailyCreditTransationTotalAmount(senderUser.getAccountDetail());
		

		 if (!CommonValidation.monthlyCreditLimitCheck(monthlyTransactionLimit, totalCreditMonthly,
				transactionAmount)) {
			error.setMessage("Monthly Credit Limit Exceeded. User monthly limit is Rs." + monthlyTransactionLimit
					+ " and you've already credited total of Rs." + totalCreditMonthly);
			valid = false;
			error.setValid(valid);
			return error;
		}
		 
		 if (!CommonValidation.dailyCreditLimitCheck(dailyTransactionLimit, totalDailyCredit,
					transactionAmount)) {
				error.setMessage("Daily Credit Limit Exceeded. User Daily limit is Rs." + dailyTransactionLimit
						+ " and you've already credited total of Rs." + totalDailyCredit);
				valid = false;
				error.setValid(valid);
				return error;
			}
		 
		 double amt  = Double.parseDouble(amount);
		  if(!CommonValidation.isNumeric(amount)){
				error.setMessage("Please enter valid amount");
				valid = false;
			}
			else if(amt <= 0 || amt >= service.getMaxAmount()) {
				error.setMessage("Amount must be between 1 to"+service.getMaxAmount());
				valid  = false;
			}
		 
		 error.setValid(valid);
		return error;
	}

	public TransactionError validateP2PFundTransaction(String amount, String senderUsername, MService service,
			MCommission commission, double netCommissionValue) {
		TransactionError error = new TransactionError();
		boolean valid = true;

		MUser senderUser = userApi.findByUserName(senderUsername);
		if (senderUser.getMobileStatus().equals(Status.Inactive)) {
			error.setMessage("Please verify your mobile " + senderUser.getUserDetail().getContactNo()
					+ " before any transaction");
			error.setValid(false);
			return error;
		}

		double transactionAmount = Double.parseDouble(amount);
		double monthlyTransactionLimit = senderUser.getAccountDetail().getAccountType().getMonthlyLimit();
		double totalDailyTransaction = senderUser.getAccountDetail().getAccountType().getDailyLimit();
		double totalMonthlyTransaction = transactionApi.getMonthlyCreditTransationTotalAmount(senderUser.getAccountDetail());
		double totalMonthlyDebit=transactionApi.getDailyCreditTransationTotalAmount(senderUser.getAccountDetail());
		
		if (totalDailyTransaction < 0 || totalMonthlyTransaction < 0) {
			error.setMessage("Opps!! Unable to process transaction.");
			error.setValid(false);
			return error;
		}
			transactionAmount = transactionAmount + netCommissionValue;
		

		if (!CommonValidation.balanceCheck(senderUser.getAccountDetail().getBalance(), transactionAmount)) {
			System.out.println(senderUser.getAccountDetail().getBalance() + "&&&&&&/n" + senderUsername);
				error.setMessage(
						"Insufficient Balance. Your current balance is Rs." + senderUser.getAccountDetail().getBalance()
								+ " and service charges is Rs." + netCommissionValue);
			
			valid = false;
			error.setValid(valid);
			return error;
		} else if (!CommonValidation.monthlyLimitCheck(monthlyTransactionLimit, totalMonthlyDebit, transactionAmount)) {
			error.setMessage("Transaction Limit Exceeded. Your monthly transaction limit is Rs."
					+ monthlyTransactionLimit + " and you already made total transaction of Rs." + totalMonthlyDebit);
			valid = false;
			error.setValid(valid);
			return error;
		}

		error.setValid(valid);
		return error;
	}

	public String CorporateLoadCardMaxLimit(String amount, double loadCardMaxLimit, MPQAccountDetails mpqAccountDetails) {
		double totalDailyCredit=transactionApi.getDailyDebitTransationTotalAmount(mpqAccountDetails);
		if(amount != null) {
			if(loadCardMaxLimit >= totalDailyCredit+ Double.parseDouble(amount)) {
				return null;
			}return "Max limit for load card has been exceed!";
		}
		return null;
	}

	
	/*public CardValidationError cardValidation(String amount,MUser user){
		CardValidationError cardError=new CardValidationError();
		DataConfig dataConfig=dataConfigRepository.findDatas();
		double dailyLimits=Double.parseDouble(dataConfig.getVirtualCardLoadDailyLimit());
		double monthlyLimits=Double.parseDouble(dataConfig.getVirtualCardLoadMonthlyLimit());
		double transactionAmount=Double.parseDouble(amount);
		MMCards cards=cardRepository.getVirtualCardsByCardUser(user);
		if(cards!=null){
			if(transactionAmount>)
		}
		return cardError;
	}
*/
	
	//flight validation
		public TransactionError validateEaseMyTrip(String amount, String senderUsername, MService service) {
			TransactionError error = new TransactionError();
			boolean valid = true;
			System.out.println("");
			MUser senderUser = userApi.findByUserName(senderUsername);
			if (senderUser.getMobileStatus().equals(Status.Inactive)) {
				error.setMessage(
						"Please verify your mobile " + senderUser.getUserDetail().getContactNo()+ " before any transaction");
				error.setValid(false);
				//error.setCode("F00");
				return error;
			}
			double transactionAmount = Double.parseDouble(amount);
			MUser user = userApi.findByUserName(senderUsername);
			double currentBalance = matchMoveApi.getBalanceApp(user);
			MCommission commission = findCommissionByService(service);
			double netCommissionValue = getCommissionValue(commission, transactionAmount);	
			
			transactionAmount = transactionAmount + netCommissionValue;
			
			System.err.println(currentBalance+"&&&&&&/n"+senderUsername);

			if (!CommonValidation.balanceCheck(currentBalance, transactionAmount))
			{
				System.out.println(currentBalance+"&&&&&&/n"+senderUsername);
				
					error.setMessage("Insufficient Balance. Your current balance is Rs."
							+ currentBalance);
				
				valid = false;
				error.setValid(valid);
				error.setCode("T01");			
				return error;
			}
			
			Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail(), true);
			System.err.println("DATE::::::"+date);
			Date currentDate = new Date();
			if(date!=null){
				String lastTransactionDate = sdf.format(date);
				String currentTransactionDate = sdf.format(currentDate);
				if(lastTransactionDate.equalsIgnoreCase(currentTransactionDate)) {
					error.setMessage("Please wait for current transaction to complete");
					error.setValid(false);
					error.setCode("F00");
					return error;
				}
			}


			if (date != null) {
				long lastTransactionTimeStamp = date.getTime();
				long currentTimeInMillis = System.currentTimeMillis();
				if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 1) {
					error.setMessage("Please wait for 1 min");
					error.setValid(false);
					error.setCode("F00");
					return error;
				}
			}
			
			error.setCode("S00");
			error.setValid(valid);
			return error;
		}
		
	public MCommission findCommissionByService(MService service) {
			
			MCommission commission = mCommissionRepository.findCommissionByService(service);
			if (commission == null) {
				commission = new MCommission();
				commission.setMinAmount(100);
				commission.setMaxAmount(2000000);			
				commission.setValue(0.5);
				commission.setFixed(false);
				commission.setService(service);
			}
			return commission;
		}
		
		public double getCommissionValue(MCommission senderCommission, double amount) {
			double netCommissionValue = 0;
			if (senderCommission.isFixed()) {
				netCommissionValue = senderCommission.getValue();
			} else {
				netCommissionValue = (senderCommission.getValue() * amount) / 100;
			}
			String v = String.format("%.2f", netCommissionValue);

			netCommissionValue = Double.parseDouble(v);
			return netCommissionValue;
		}
}
