package com.msscard.validation;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.ewire.paramerterization.DataConfig;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.ChangePasswordRequest;
import com.msscard.app.model.request.MatchMoveCreateCardRequest;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.response.ForgotPasswordError;
import com.msscard.entity.GroupBulkKyc;
import com.msscard.entity.GroupBulkRegister;
import com.msscard.entity.GroupFileCatalogue;
import com.msscard.entity.MLocationDetails;
import com.msscard.entity.MUser;
import com.msscard.entity.MUserDetails;
import com.msscard.entity.MerchantDetails;
import com.msscard.model.AddMerchantDTO;
import com.msscard.model.RegisterDTO;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Status;
import com.msscard.model.error.RegisterError;
import com.msscard.repositories.DataConfigRepository;
import com.msscard.repositories.GroupBulkKycRepository;
import com.msscard.repositories.GroupBulkRegisterRepository;
import com.msscard.repositories.MLocationDetailRepository;
import com.msscard.repositories.MUserDetailRepository;
import com.msscard.repositories.MUserRespository;
import com.msscard.repositories.MerchantDetailsRepository;
import com.msscard.util.CommonUtil;
import com.msscard.util.SecurityUtil;

import javassist.compiler.SyntaxError;

public class RegisterValidation {

	private final MUserRespository mUserRespository;
	private final PasswordEncoder passwordEncoder;
	private final MUserDetailRepository mUserDetailRepository;
	private final MLocationDetailRepository mLocationDetailRepository;
	private final IUserApi userApiImpl;
	private final IMatchMoveApi matchMoveApi;
	private final MerchantDetailsRepository merchantDetailsRepository;
	private final DataConfigRepository dataConfigRepository;
	private final GroupBulkRegisterRepository groupBulkRegisterRepository;
	private final GroupBulkKycRepository groupBulkKycRepository;
	

	public RegisterValidation(MUserRespository mUserRespository, PasswordEncoder passwordEncoder,
			MUserDetailRepository mUserDetailRepository, MLocationDetailRepository mLocationDetailRepository,
			IUserApi userApiImpl,IMatchMoveApi matchMoveApi,MerchantDetailsRepository merchantDetailsRepository,DataConfigRepository dataConfigRepository,
			GroupBulkRegisterRepository groupBulkRegisterRepository, GroupBulkKycRepository groupBulkKycRepository) {
		super();
		this.mUserRespository = mUserRespository;
		this.passwordEncoder = passwordEncoder;
		this.mUserDetailRepository = mUserDetailRepository;
		this.mLocationDetailRepository = mLocationDetailRepository;
		this.userApiImpl = userApiImpl;
		this.matchMoveApi=matchMoveApi;
		this.merchantDetailsRepository=merchantDetailsRepository;
		this.dataConfigRepository=dataConfigRepository;
		this.groupBulkRegisterRepository = groupBulkRegisterRepository;
		this.groupBulkKycRepository = groupBulkKycRepository;
	}

	public RegisterError validateNormalUser(RegisterDTO dto) {
		RegisterError error = new RegisterError();
		boolean valid = true;
		/*if(dto.getDateOfBirth().isEmpty()||dto.getDateOfBirth()==null){
			valid=false;
			error.setMessage("please enter date of birth");
		}*/
		if (dto.getUsername() == null || dto.getUsername().isEmpty()) {
			error.setUsername("Username Required");
			error.setMessage("Username Required");
			valid = false;
		} else {
			MUser user = mUserRespository.findByUsernameAndStatus((dto.getUsername().toLowerCase()), Status.Active);
			if (user != null) {
				error.setUsername("Username Already Exist");
				error.setContactNo("User Already Exist");
				error.setMessage("User Already Exist");
				valid = false;
			}
		}

		if(dto.isHasAadhar()){
			if(!CommonValidation.isNull(dto.getAadharNo())){
			}else{
				error.setMessage("aadhar no is mandatory");
				valid=false;
			}
		}
		/*if (CommonValidation.isNull(dto.getLocationCode())) {
			valid = false;
			error.setLocationCode("Pincode Required");
			error.setMessage("Pincode Required");
		}*/ /*else {
			MLocationDetails locationDetails = locationDetailsRepository.findLocationByPin(dto.getLocationCode());
			if (locationDetails == null) {
				System.err.println(dto.getLocationCode());
				valid = false;
				error.setLocationCode("Not a valid Pincode");
				error.setMessage("Not a valid Pincode");
			}
		}*/

		 if (dto.getPassword() == null || dto.getPassword().isEmpty()) {
		 error.setPassword("Password Required");
		 error.setMessage("Password Required");
		 valid = false;
		 }
		
		
//		 if (dto.getConfirmPassword() == null ||
//		 dto.getConfirmPassword().isEmpty()) {
//		 error.setConfirmPassword("Password Required");
//		 error.setMessage
//		 valid = false;
//		 } else {
//		 if (!dto.getPassword().equals(dto.getConfirmPassword())) {
//		 error.setConfirmPassword("Password Mis-Match");
//		 error.setMessage
//		 valid = false;
//		 }
//		 }

		 /*if (dto.getContactNo() == null || dto.getContactNo().isEmpty()) {
		 error.setContactNo("Contact No Required");
		 error.setMessage("Contact No Required");
		 valid = false;
		 } else if (CommonValidation.isValidNumber(dto.getContactNo())) {
		 if (!CommonValidation.checkLength10(dto.getContactNo())) {
		 error.setContactNo("Mobile number must be 10 digits long");
		 error.setMessage("Mobile number must be 10 digits long");
		 valid = false;
		 }
		 } else {
		 error.setContactNo("Please enter valid mobile number");
		 error.setMessage("Please enter valid mobile number");
		 valid = false;
		 }*/

//		 if (dto.getFirstName() == null || dto.getFirstName().isEmpty()) {
//		 error.setFirstName("First Name Required");
//		 error.setMessage("First Name Required");
//		 valid = false;
//		 }
//		
//		 if (dto.getLastName() == null || dto.getLastName().isEmpty()) {
//		 error.setLastName("Last Name Required");
//		 error.setMessage("Last Name Required");
//		 valid = false;
//		 }
		
		 if (dto.getEmail() == null || dto.getEmail().isEmpty()) {
		 error.setEmail("Email Required");
		 error.setMessage("Email Required");
		 valid = false;
		 } else if (!(CommonValidation.isValidMail(dto.getEmail()))) {
		 error.setEmail("Please enter valid mail address");
		 error.setMessage("Please enter valid mail address");
		 valid = false;
		 }else if(dto.getEmail().contains("@yopmail.com") || dto.getEmail().contains("@mailinator.com")){
			 error.setEmail("Please enter valid email address.");
			 error.setMessage("Please enter valid email address.");
			 valid = false; 
		 }else {
		if (!CommonValidation.isNull(dto.getEmail())) {
			List<MUserDetails> userDetail = mUserDetailRepository.checkMail(dto.getEmail());
			if (userDetail != null) {
				for (MUserDetails ud : userDetail) {
					MUser user = mUserRespository.findByUsernameAndMobileStatusAndEmailStatus(ud.getContactNo(),
							Status.Active, Status.Active);
					if (user != null) {
						valid = false;
						error.setEmail("Email already exists");
						error.setMessage("Email already exists");
					}
				}
				 }
			}
		
		MatchMoveCreateCardRequest cardRequest=new MatchMoveCreateCardRequest();
		cardRequest.setEmail(dto.getEmail());
		try {
			cardRequest.setPassword(SecurityUtil.md5(dto.getContactNo()));
		cardRequest.setUsername(dto.getContactNo());
		ResponseDTO responseDTO=matchMoveApi.fetchMMUser(cardRequest);
		if(responseDTO.getCode().equalsIgnoreCase("S00")){
			valid=false;
			error.setValid(valid);
			error.setMessage("Please use a different email id");
		}else if(responseDTO.getCode().equalsIgnoreCase("E01")){
			valid=false;
			error.setValid(valid);
			error.setMessage(responseDTO.getMessage());
		}else if(responseDTO.getCode().equalsIgnoreCase("E00")){
			valid=false;
			error.setValid(valid);
			error.setMessage(responseDTO.getMessage());
		}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			valid=false;
			error.setValid(valid);
			error.setMessage("Services are temporarily down..Please try after sometime");
			return error;
		}
		}
		 DataConfig dataConfig=dataConfigRepository.findDatas();
		 
		if(dataConfig!=null && !dataConfig.isAllowRegistration()){
			valid=false;
			error.setValid(valid);
			error.setMessage("Services are temporarily down..Inconvenience is regretted");
			return error;
		}
		error.setValid(valid);
		return error;
	}

	public ForgotPasswordError forgotPassword(String username) {
		ForgotPasswordError error = new ForgotPasswordError();
		if (!CommonValidation.checkLength10(username)) {
			error.setErrorLength("Mobile number must be 10 digits long");
			error.setValid(false);
			return error;
		}
		error.setValid(true);
		return error;

	}
	/*public RegisterError validateEditUser(RegisterDTO dto) {
		RegisterError error = new RegisterError();
		boolean valid = true;

		// if (dto.getAddress() == null || dto.getAddress().isEmpty()) {
		// error.setAddress("Address Required");
		// valid = false;
		// }
		//
		// if (dto.getFirstName() == null || dto.getFirstName().isEmpty()) {
		// error.setFirstName("First Name Required");
		// valid = false;
		// }
		//
		// if (dto.getLastName() == null || dto.getLastName().isEmpty()) {
		// error.setLastName("Last Name Required");
		// valid = false;
		// }
		//
		// if (dto.getEmail() == null) {
		// error.setLastName("Please Enter Valid Email ID");
		// valid = false;
		// }

		error.setValid(valid);

		return error;

	}

	public ChangePasswordError validateChangePassword(ChangePasswordDTO dto) {
		ChangePasswordError error = new ChangePasswordError();
		boolean valid = true;

		if (CommonValidation.isNull(dto.getNewPassword())) {
			error.setPassword("Insert Your New Password");
			valid = false;
		} else if (CommonValidation.isNull(dto.getConfirmPassword())) {
			error.setConfirmPassword("Please Re-Type Your Password");
			valid = false;
		} else if (dto.getPassword() != null && dto.getConfirmPassword() != null) {
			if (!dto.getPassword().equals(dto.getConfirmPassword())) {
				error.setConfirmPassword("Password Mis-match");
				valid = false;
			}
			if (CommonValidation.checkLength6(dto.getNewPassword())) {
				error.setNewPassword("Password must be atleast 6 characters long");
				valid = false;
			}
		}

		error.setValid(valid);
		return error;
	}

	public ChangePasswordError validateAdminPassword(ChangePasswordDTO dto) {
		ChangePasswordError error = new ChangePasswordError();
		boolean valid = true;
		User user = userRepository.findByUsername(dto.getUsername());
		if (CommonValidation.isNull(dto.getPassword())) {
			error.setPassword("Please Enter Current Password");
			valid = false;
		}
		if (CommonValidation.isNull(dto.getNewPassword())) {
			error.setNewPassword("Please Enter New Password");
			valid = false;
		} else if (!CommonValidation.validateAdminPassword(dto.getNewPassword())) {
			error.setNewPassword("Password must be 10 digits long");
			valid = false;
		}
		if (CommonValidation.isNull(dto.getConfirmPassword())) {
			error.setConfirmPassword("Please Re enter New Password");
			valid = false;
		} else if (!dto.getNewPassword().equals(dto.getConfirmPassword())) {
			valid = false;
			error.setConfirmPassword("Both Passwords must match");
		}
		if (user != null) {
			if (!passwordEncoder.matches(dto.getPassword(), user.getPassword())) {
				valid = false;
				error.setPassword("Current Password you entered doesn\'t match");
			}
		}
		if (!dto.isRequest()) {
			if (CommonValidation.isNull(dto.getKey())) {
				error.setKey("Please Enter OTP");
				valid = false;
			} else if (!dto.getKey().equals(user.getMobileToken())) {
				error.setKey("Enter valid OTP");
				valid = false;
			}
		}
		error.setValid(valid);
		return error;
	}

	public ChangePasswordError validateChangePasswordDTO(ChangePasswordDTO dto) {
		ChangePasswordError error = new ChangePasswordError();
		boolean valid = true;
		User u = userRepository.findByUsername(dto.getUsername());
		if (u != null) {
			System.err.println("password ::" + dto.getPassword());
			System.err.println(" backend password ::" + u.getPassword());
			if (!(passwordEncoder.matches(dto.getPassword(), u.getPassword()))) {
				error.setPassword("Enter your current password correctly");
				valid = false;
			}
		}
		error.setValid(valid);
		return error;
	}

	public ChangePasswordError renewPasswordValidation(ChangePasswordDTO dto) {
		System.err.println("change passowrd error");
		ChangePasswordError error = new ChangePasswordError();
		boolean valid = true;
		User user = userRepository.findByMobileTokenAndUsername(dto.getKey(), dto.getUsername());
		if (user == null) {
			valid = false;
			error.setKey("The OTP you have entered is INCORRECT.");
			error.setUsername("Invalid user");
		}

		if (!(dto.getConfirmPassword().equals(dto.getNewPassword()))) {
			valid = false;
			error.setKey("Password Mis-Match");
		}
		error.setValid(valid);
		return error;
	}

	public ForgotPasswordError forgotPassword(String username) {
		ForgotPasswordError error = new ForgotPasswordError();
		if (!CommonValidation.checkLength10(username)) {
			error.setErrorLength("Mobile number must be 10 digits long");
			error.setValid(false);
			return error;
		}
		error.setValid(true);
		return error;

	}

	public VerifyEmailError checkMailError(VerifyEmailDTO email) {
		VerifyEmailError error = new VerifyEmailError();
		boolean valid = true;
		if (CommonValidation.isNull(email.getKey())) {
			error.setEmail("Please enter email in the field");
			valid = false;
		}
		error.setValid(valid);
		return error;
	}

	public RegisterError validateMerchant(RegisterDTO dto) {
		RegisterError error = new RegisterError();
		boolean valid = true;
		if (dto.getUsername() == null || dto.getUsername().isEmpty()) {
			error.setUsername("Username Required");
			valid = false;
		} else {
			User user = userRepository.findByUsernameAndStatus((dto.getUsername().toLowerCase()), Status.Active);
			if (user != null) {
				error.setUsername("Username Already Exist");
				error.setContactNo("User Already Exist");
				valid = false;
			}
		}

		if (dto.getContactNo() != null) {
			UserDetail userDetail = userDetailRepository.findByContactNo(dto.getContactNo());
			if (userDetail != null) {
				error.setContactNo("Contact No. already exists");
				valid = false;
			}

		}

		if (CommonValidation.isNull(dto.getLocationCode())) {
			valid = false;
			error.setLocationCode("Pincode Required");
		} else {
			LocationDetails locationDetails = locationDetailsRepository.findLocationByPin(dto.getLocationCode());
			if (locationDetails == null) {
				valid = false;
				error.setLocationCode("Not a valid Pincode");
			}
		}

		if (dto.getPassword() == null || dto.getPassword().isEmpty()) {
			error.setPassword("Password Required");
			valid = false;
		}

		// if (dto.getConfirmPassword() == null ||
		// dto.getConfirmPassword().isEmpty()) {
		// error.setConfirmPassword("Password Required");
		// valid = false;
		// } else {
		// if (!dto.getPassword().equals(dto.getConfirmPassword())) {
		// error.setConfirmPassword("Password Mis-Match");
		// valid = false;
		// }
		// }

		if (dto.getContactNo() == null || dto.getContactNo().isEmpty()) {
			error.setContactNo("Contact No Required");
			valid = false;
		} else if (CommonValidation.isNumeric(dto.getContactNo())) {
			if (!CommonValidation.checkLength10(dto.getContactNo())) {
				error.setContactNo("Mobile number must be 10 digits long");
				valid = false;
			}
		} else {
			error.setContactNo("Please enter valid mobile number");
			valid = false;
		}

		if (dto.getFirstName() == null || dto.getFirstName().isEmpty()) {
			error.setFirstName("First Name Required");
			valid = false;
		}

		// if (dto.getLastName() == null || dto.getLastName().isEmpty()) {
		// error.setLastName("Last Name Required");
		// valid = false;
		// }
		//
		if (dto.getEmail() == null || dto.getEmail().isEmpty()) {
			error.setEmail("Email Required");
			valid = false;
		} else if (!(CommonValidation.isValidMail(dto.getEmail()))) {
			error.setEmail("Please enter valid mail address");
			valid = false;
		} else {
			if (!CommonValidation.isNull(dto.getEmail())) {
				List<UserDetail> userDetail = userDetailRepository.checkMail(dto.getEmail());
				if (userDetail != null) {
					for (UserDetail ud : userDetail) {
						User user = userRepository.findByUsernameAndMobileStatusAndEmailStatus(ud.getEmail(),
								Status.Active, Status.Active);
						if (user != null) {
							valid = false;
							error.setEmail("Email already exists");
						}
					}
					// }
				}
			}
		}
		error.setValid(valid);
		return error;

	}

	 Agent Validation 
	public RegisterError validateAgent(AgentRequest dto) {
		RegisterError error = new RegisterError();
		boolean valid = true;
		if (dto.getUserName() == null || dto.getUserName().isEmpty()) {
			error.setUsername("Username Required");

			valid = false;
		} else {
			User user = userRepository.findByUsernameAndStatus((dto.getUserName().toLowerCase()), Status.Active);
			if (user != null) {
				error.setMessage("Register Agent");
				error.setUsername("Username Already Exist");
				error.setContactNo("User Already Exist");
				System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@12");
				valid = false;
			}
		}

		if (dto.getMobileNo() != null) {
			UserDetail userDetail = userDetailRepository.findByContactNo(dto.getMobileNo());
			if (userDetail != null) {
				error.setContactNo("Contact No. already exists");
				error.setMessage("Contact No. already exists");

				valid = false;
			}

		}
		if (dto.getPanNo() != null) {
			AgentDetail userDetail = userRepository.findByAgentPannumber(dto.getPanNo());
			if (userDetail != null) {
				error.setMessage("PanCard No. already exists");
				error.setPanCardNo("PanCard No. already exists");
				valid = false;
			}

		}

		if (CommonValidation.isNull(dto.getPinCode())) {
			error.setLocationCode("Pincode Required");
			error.setMessage("Pincode Required");
			valid = false;

		} else {
			
			 * LocationDetails locationDetails =
			 * locationDetailsRepository.findLocationByPin(dto.getPinCode());
			 * if(locationDetails == null) {
			 * 
			 * valid = false; error.setMessage("Not a valid Pincode");
			 * error.setLocationCode("Not a valid Pincode"); }
			 
		}

		if (dto.getPassword() == null || dto.getPassword().isEmpty()) {
			error.setMessage("Password Required");
			error.setPassword("Password Required");

			valid = false;
		}

		// if (dto.getConfirmPassword() == null ||
		// dto.getConfirmPassword().isEmpty()) {
		// error.setConfirmPassword("Password Required");
		// valid = false;
		// } else {
		// if (!dto.getPassword().equals(dto.getConfirmPassword())) {
		// error.setConfirmPassword("Password Mis-Match");
		// valid = false;
		// }
		// }

		if (dto.getMobileNo() == null || dto.getMobileNo().isEmpty()) {
			error.setMessage("Contact No Required");
			error.setContactNo("Contact No Required");

			valid = false;
		} else if (CommonValidation.isNumeric(dto.getMobileNo())) {
			if (!CommonValidation.checkLength10(dto.getMobileNo())) {
				error.setMessage("Mobile number must be 10 digits long");
				error.setContactNo("Mobile number must be 10 digits long");

				valid = false;
			}
		} else {
			error.setMessage("Please enter valid mobile number");
			error.setContactNo("Please enter valid mobile number");

			valid = false;
		}

		if (dto.getFirstName() == null || dto.getFirstName().isEmpty()) {
			error.setMessage("First Name Required");
			error.setFirstName("First Name Required");
			valid = false;
		}

		// if (dto.getLastName() == null || dto.getLastName().isEmpty()) {
		// error.setLastName("Last Name Required");
		// valid = false;
		// }
		//
		if (dto.getEmailAddress() == null || dto.getEmailAddress().isEmpty()) {
			error.setEmail("Email Required");
			error.setMessage("Email Required");
			valid = false;
		} else if (!(CommonValidation.isValidMail(dto.getEmailAddress()))) {
			error.setEmail("Please enter valid mail address");
			error.setMessage("Please enter valid mail address");
			valid = false;
		} else {
			if (!CommonValidation.isNull(dto.getEmailAddress())) {

				List<UserDetail> userDetail1 = userDetailRepository.checkMail(dto.getEmailAddress());
				if (userDetail1 == null) {
					error.setMessage("Email already exists");

					valid = false;

				}
				// }
			}
		}

		error.setValid(valid);
		return error;

	}
	
	public RegisterError validateURegisterUser(RegisterDTO dto) {
		RegisterError error = new RegisterError();
		boolean valid = true;
		if (dto.getUsername() == null || dto.getUsername().isEmpty()) {
			error.setUsername("Username Required");
			valid = false;
		} else {
			User user = userRepository.findByUsername((dto.getUsername().toLowerCase()));
			if (user != null) {
				error.setUsername("Username Already Exist");
				error.setContactNo("User Already Exist");
				error.setMessage("User Already Exist");
				valid = false;
			}
		}

		if (CommonValidation.isNull(dto.getLocationCode())) {
			valid = false;
			error.setLocationCode("Pincode Required");
			error.setMessage("Pincode Required");
		} else {
			LocationDetails locationDetails = locationDetailsRepository.findLocationByPin(dto.getLocationCode());
			if (locationDetails == null) {
				valid = false;
				error.setLocationCode("Not a valid Pincode");
				error.setMessage("Not a valid Pincode");
			}
		}

		 if (dto.getPassword() == null || dto.getPassword().isEmpty()) {
		 error.setPassword("Password Required");
		 error.setMessage("Password Required");
		 valid = false;
		 }
		
		
//		 if (dto.getConfirmPassword() == null ||
//		 dto.getConfirmPassword().isEmpty()) {
//		 error.setConfirmPassword("Password Required");
//		 error.setMessage
//		 valid = false;
//		 } else {
//		 if (!dto.getPassword().equals(dto.getConfirmPassword())) {
//		 error.setConfirmPassword("Password Mis-Match");
//		 error.setMessage
//		 valid = false;
//		 }
//		 }

		 if (dto.getContactNo() == null || dto.getContactNo().isEmpty()) {
		 error.setContactNo("Contact No Required");
		 error.setMessage("Contact No Required");
		 valid = false;
		 } else if (CommonValidation.isValidNumber(dto.getContactNo())) {
		 if (!CommonValidation.checkLength10(dto.getContactNo())) {
		 error.setContactNo("Mobile number must be 10 digits long");
		 error.setMessage("Mobile number must be 10 digits long");
		 valid = false;
		 }
		 } else {
		 error.setContactNo("Please enter valid mobile number");
		 error.setMessage("Please enter valid mobile number");
		 valid = false;
		 }

//		 if (dto.getFirstName() == null || dto.getFirstName().isEmpty()) {
//		 error.setFirstName("First Name Required");
//		 error.setMessage("First Name Required");
//		 valid = false;
//		 }
//		
//		 if (dto.getLastName() == null || dto.getLastName().isEmpty()) {
//		 error.setLastName("Last Name Required");
//		 error.setMessage("Last Name Required");
//		 valid = false;
//		 }
		
		 if (dto.getEmail() == null || dto.getEmail().isEmpty()) {
		 error.setEmail("Email Required");
		 error.setMessage("Email Required");
		 valid = false;
		 } else if (!(CommonValidation.isValidMail(dto.getEmail()))) {
		 error.setEmail("Please enter valid mail address");
		 error.setMessage("Please enter valid mail address");
		 valid = false;
		 } else {

		if (!CommonValidation.isNull(dto.getEmail())) {
			List<UserDetail> userDetail = userDetailRepository.checkMail(dto.getEmail());
			if (userDetail != null) {
				for (UserDetail ud : userDetail) {
					User user = userRepository.findByUsernameAndMobileStatusAndEmailStatus(ud.getContactNo(),
							Status.Active, Status.Active);
					if (user != null) {
						valid = false;
						error.setEmail("Email already exists");
						error.setMessage("Email already exists");
					}
				}
				 }
			}
		}

		error.setValid(valid);
		return error;
	}
	
	public RegisterError validateRefernEarnUser(RegisterDTO dto) {
		RegisterError error = new RegisterError();
		boolean valid = true;
		if (dto.getUsername() == null || dto.getUsername().isEmpty()) {
			error.setUsername("Username Required");
			error.setMessage("Username Required");
			valid = false;
		} else {
			User user = userRepository.findByUsernameAndStatus((dto.getUsername().toLowerCase()), Status.Active);
			if (user != null) {
				error.setUsername("Username Already Exist");
				error.setContactNo("User Already Exist");
				error.setMessage("User Already Exist");
				valid = false;
			}
		}

		if (CommonValidation.isNull(dto.getLocationCode())) {
			valid = false;
			error.setLocationCode("Pincode Required");
			error.setMessage("Pincode Required");
		} else {
			LocationDetails locationDetails = locationDetailsRepository.findLocationByPin(dto.getLocationCode());
			if (locationDetails == null) {
				valid = false;
				error.setLocationCode("Not a valid Pincode");
				error.setMessage("Not a valid Pincode");
			}
		}

		 if (dto.getPassword() == null || dto.getPassword().isEmpty()) {
		 error.setPassword("Password Required");
		 error.setMessage("Password Required");
		 valid = false;
		 }
		
		
//		 if (dto.getConfirmPassword() == null ||
//		 dto.getConfirmPassword().isEmpty()) {
//		 error.setConfirmPassword("Password Required");
//		 error.setMessage
//		 valid = false;
//		 } else {
//		 if (!dto.getPassword().equals(dto.getConfirmPassword())) {
//		 error.setConfirmPassword("Password Mis-Match");
//		 error.setMessage
//		 valid = false;
//		 }
//		 }

		 if (dto.getContactNo() == null || dto.getContactNo().isEmpty()) {
		 error.setContactNo("Contact No Required");
		 error.setMessage("Contact No Required");
		 valid = false;
		 } else if (CommonValidation.isValidNumber(dto.getContactNo())) {
		 if (!CommonValidation.checkLength10(dto.getContactNo())) {
		 error.setContactNo("Mobile number must be 10 digits long");
		 error.setMessage("Mobile number must be 10 digits long");
		 valid = false;
		 }
		 } else {
		 error.setContactNo("Please enter valid mobile number");
		 error.setMessage("Please enter valid mobile number");
		 valid = false;
		 }

//		 if (dto.getFirstName() == null || dto.getFirstName().isEmpty()) {
//		 error.setFirstName("First Name Required");
//		 error.setMessage("First Name Required");
//		 valid = false;
//		 }
//		
//		 if (dto.getLastName() == null || dto.getLastName().isEmpty()) {
//		 error.setLastName("Last Name Required");
//		 error.setMessage("Last Name Required");
//		 valid = false;
//		 }
		
					User user = userRepository.findByUsername(dto.getUsername());
					if (user != null) {
						valid = false;
						error.setMessage("username already exists");
					}

		error.setValid(valid);
		return error;
	}

*/

	public ForgotPasswordError changePassword(ChangePasswordRequest dto) {
		
		ForgotPasswordError error = new ForgotPasswordError();
		
		MUser user=userApiImpl.findByUserName(dto.getUsername());
		String hashedPassword=user.getPassword();
		String pass = CommonUtil.base64Decode(dto.getPassword());
		String newPass = CommonUtil.base64Decode(dto.getNewPassword());
		String confirmPass = CommonUtil.base64Decode(dto.getConfirmPassword());
		
		System.err.println("password:: "+dto.getPassword());
		System.err.println("new pass:: "+dto.getNewPassword());
		System.err.println("confirmpass:: "+dto.getConfirmPassword());
		
		System.err.println("hashedPassword:: "+hashedPassword);
		System.err.println("pass:: "+pass);
		System.err.println("newPasss: "+newPass);
		System.err.println("confirmPass: "+confirmPass);
		if(!(passwordEncoder.matches(pass,hashedPassword))){
			error.setMessage("You have entered wrong current password");
			error.setValid(false);
			return error;
		}
		
		if (dto.getPassword()==null) {
			error.setMessage("Please enter the Password");
			error.setValid(false);
			return error;
		}
		if (pass.equalsIgnoreCase(newPass)) {
			error.setMessage("Please use a different password other than previous one ");
			error.setValid(false);
			return error;
		}
		if (dto.getNewPassword()==null) {
			error.setMessage("Please enter the New Password");
			error.setValid(false);
			return error;
		}if (dto.getConfirmPassword()==null) {
			error.setMessage("Please enter the Confirm Password");
			error.setValid(false);
			return error;
		}if (!newPass.equalsIgnoreCase(confirmPass)) {
			error.setMessage("Password Missmatch");
			error.setValid(false);
			return error;
		}
		error.setValid(true);
		return error;
	}

	public ForgotPasswordError forgotPassword(ChangePasswordRequest dto) {
		ForgotPasswordError error = new ForgotPasswordError();
		String pass = CommonUtil.base64Decode(dto.getPassword());
		String confirmPass = CommonUtil.base64Decode(dto.getConfirmPassword());
		if (dto.getPassword()==null) {
			error.setMessage("Please enter the Password");
			error.setValid(false);
			return error;
		}
		if (dto.getKey()==null) {
			error.setMessage("Please Enter the OTP");
			error.setValid(false);
			return error;
		}
		if (dto.getConfirmPassword()==null) {
			error.setMessage("Please enter the Confirm Password");
			error.setValid(false);
			return error;
		}if (!pass.equalsIgnoreCase(confirmPass)) {
			error.setMessage("Password Missmatch");
			error.setValid(false);
			return error;
		}
		error.setValid(true);
		return error;
	}
	
	public RegisterError customRegisterError(RegisterDTO dto){
		RegisterError error=new RegisterError();
		boolean valid=true;
		
		 if (dto.getContactNo() == null || dto.getContactNo().isEmpty()) {
			 error.setContactNo("Contact No Required");
			 error.setMessage("Contact No Required");
			 error.setHasError(true);
			 valid = false;
			 } else if (CommonValidation.isValidNumber(dto.getContactNo())) {
			 if (!CommonValidation.checkLength10(dto.getContactNo())) {
			 error.setContactNo("Mobile number must be 10 digits long");
			 error.setMessage("Mobile number must be 10 digits long");
			 error.setHasError(true);
			 valid = false;
			 }
			 } else {
			 error.setContactNo("Please enter valid mobile number");
			 error.setMessage("Please enter valid mobile number");
			 error.setHasError(true);
			 valid = false;
			 }
		 MUser user = mUserRespository.findByUsernameAndStatus(dto.getContactNo(), Status.Active);
			if (user != null) {
				error.setUsername("MobileNo Already Exist");
				error.setContactNo("MobileNo Already Exist");
				error.setMessage("MobileNo Already Exist");
				error.setFullyFilled(user.isFullyFilled());
				valid = false;
			}
			
			error.setValid(valid);
		 return error;
	}
	
	public RegisterError validateFullDetails(RegisterDTO dto){
			RegisterError error = new RegisterError();
			boolean valid = true;
			if(dto.getDateOfBirth().isEmpty()||dto.getDateOfBirth()==null){
				valid=false;
				error.setMessage("please enter date of birth");
			}
						
			 if (dto.getPassword() == null || dto.getPassword().isEmpty()) {
			 error.setPassword("Password Required");
			 error.setMessage("Password Required");
			 valid = false;
			 }
			

			 if (dto.getEmail() == null || dto.getEmail().isEmpty()) {
			 error.setEmail("Email Required");
			 error.setMessage("Email Required");
			 valid = false;
			 } else if (!(CommonValidation.isValidMail(dto.getEmail()))) {
			 error.setEmail("Please enter valid mail address");
			 error.setMessage("Please enter valid mail address");
			 valid = false;
			 }else if(dto.getEmail().contains("@yopmail.com") || dto.getEmail().contains("@mailinator.com")){
				 error.setEmail("Please enter valid email address.");
				 error.setMessage("Please enter valid email address.");
				 valid = false; 
			 }else {
			if (!CommonValidation.isNull(dto.getEmail())) {
				List<MUserDetails> userDetail = mUserDetailRepository.checkMail(dto.getEmail());
				if (!(userDetail == null||userDetail.isEmpty())) {
					valid=false;
					error.setMessage("Email id exists use a different one");
					error.setEmail("Email id exists use a different one");
				}
				}
			MatchMoveCreateCardRequest cardRequest=new MatchMoveCreateCardRequest();
			cardRequest.setEmail(dto.getEmail());
			try {
				cardRequest.setPassword(SecurityUtil.md5(dto.getContactNo()));
			
			/*ResponseDTO responseDTO=matchMoveApi.fetchMMUser(cardRequest);
			if(responseDTO.getCode().equalsIgnoreCase("S00")){
				valid=false;
				error.setValid(valid);
				error.setMessage("Please use a different email id");
			}else if(responseDTO.getCode().equalsIgnoreCase("E01")){
				valid=false;
				error.setValid(valid);
				error.setMessage(responseDTO.getMessage());
			}else if(responseDTO.getCode().equalsIgnoreCase("E00")){
				valid=false;
				error.setValid(valid);
				error.setMessage(responseDTO.getMessage());
			}*/
			
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				valid=false;
				error.setValid(valid);
				error.setMessage("Services are temporarily down..Please try after sometime");
				return error;
			}
			}

			error.setValid(valid);
			return error;
		}


	
	public RegisterError validateEmail(RegisterDTO dto){
		RegisterError error = new RegisterError();
		boolean valid = true;
		 if (dto.getEmail() == null || dto.getEmail().isEmpty()) {
		 error.setEmail("Email Required");
		 error.setMessage("Email Required");
		 valid = false;
		 } else if (!(CommonValidation.isValidMail(dto.getEmail()))) {
		 error.setEmail("Please enter valid mail address");
		 error.setMessage("Please enter valid mail address");
		 valid = false;
		 }else if(dto.getEmail().contains("@yopmail.com") || dto.getEmail().contains("@mailinator.com")){
			 error.setEmail("Please enter valid email address.");
			 error.setMessage("Please enter valid email address.");
			 valid = false; 
		 }else {
		if (!CommonValidation.isNull(dto.getEmail())) {
			List<MUserDetails> userDetail = mUserDetailRepository.checkMail(dto.getEmail());
			if (!(userDetail == null||userDetail.isEmpty())) {
				valid=false;
				error.setMessage("Email id exists use a different one");
				error.setEmail("Email id exists use a different one");
			}
			}
		MatchMoveCreateCardRequest cardRequest=new MatchMoveCreateCardRequest();
		cardRequest.setEmail(dto.getEmail());
		try {
			cardRequest.setPassword(SecurityUtil.md5(dto.getContactNo()));
		cardRequest.setUsername(dto.getContactNo());
		ResponseDTO responseDTO=matchMoveApi.fetchMMUser(cardRequest);
		if(responseDTO.getCode().equalsIgnoreCase("S00")){
			valid=false;
			error.setValid(valid);
			error.setMessage("Please use a different email id");
		}else if(responseDTO.getCode().equalsIgnoreCase("E01")){
			valid=false;
			error.setValid(valid);
			error.setMessage(responseDTO.getMessage());
		}else if(responseDTO.getCode().equalsIgnoreCase("E00")){
			valid=false;
			error.setValid(valid);
			error.setMessage(responseDTO.getMessage());
		} 
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			valid=false;
			error.setValid(valid);
			error.setMessage("Services are temporarily down..Please try after sometime");
			return error;
		}
		}

		error.setValid(valid);
		return error;
	}

	
	public static void main(String[] args) {
		String jsonString="{\r\n    \"status\": \"Success\",\r\n    \"code\": \"S00\",\r\n    \"message\": \"Login successful.\",\r\n    \"balance\": 0,\r\n    \"details\": {\r\n        \"accountDetail\": {\r\n            \"id\": 24,\r\n            \"created\": 1519452440000,\r\n            \"lastModified\": 1519452440000,\r\n            \"balance\": 0,\r\n            \"accountNumber\": 999900910000029,\r\n            \"points\": 0,\r\n            \"accountType\": {\r\n                \"id\": 3,\r\n                \"created\": 1519048495000,\r\n                \"lastModified\": 1519048495000,\r\n                \"name\": \"Non - KYC\",\r\n                \"description\": \"Unverified Account\",\r\n                \"code\": \"NONKYC\",\r\n                \"monthlyLimit\": 20000,\r\n                \"dailyLimit\": 20000,\r\n                \"balanceLimit\": 20000,\r\n                \"transactionLimit\": 5,\r\n                \"new\": false\r\n            },\r\n            \"branchCode\": null,\r\n            \"new\": false\r\n        },\r\n        \"userDetail\": {\r\n            \"sessionId\": null,\r\n            \"username\": \"8178003068\",\r\n            \"userId\": \"29\",\r\n            \"firstName\": \"kanchan\",\r\n            \"middleName\": \"\",\r\n            \"lastName\": \"bishst\",\r\n            \"address\": \"\",\r\n            \"contactNo\": \"8178003068\",\r\n            \"userType\": \"User\",\r\n            \"authority\": \"ROLE_USER,ROLE_AUTHENTICATED\",\r\n            \"emailStatus\": \"Active\",\r\n            \"mobileStatus\": \"Active\",\r\n            \"email\": \"kanchan.bishst@gmail.com\",\r\n            \"image\": \"\",\r\n            \"mpin\": \"\",\r\n            \"mpinPresent\": false,\r\n            \"dateOfBirth\": \"1992-02-02\",\r\n            \"gcmId\": \"\",\r\n            \"pinCode\": \"560095\",\r\n            \"circleName\": \"Karnataka\",\r\n            \"districtName\": \"Bangalore\",\r\n            \"locality\": \"Kormangala\",\r\n            \"encodedImage\": \"\",\r\n            \"hasMpin\": false,\r\n            \"deviceLocked\": false\r\n        },\r\n        \"sessionId\": \"4B8A548C34D06C8A54C78C3DF7509B6E\"\r\n    },\r\n    \"txnId\": null,\r\n    \"error\": null,\r\n    \"valid\": false,\r\n    \"info\": null,\r\n    \"hasRefer\": false,\r\n    \"userList\": null,\r\n    \"hasVirtualCard\": true,\r\n    \"hasPhysicalCard\": false,\r\n    \"cardDetails\": {\r\n        \"status\": null,\r\n        \"code\": \"S00\",\r\n        \"message\": \"card successfully assigned...\",\r\n        \"details\": null,\r\n        \"expiryDate\": \"2023-02\",\r\n        \"issueDate\": \"2018-02-24\",\r\n        \"walletNumber\": \"5386290707500161\",\r\n        \"walletId\": null,\r\n        \"withholdingAmt\": null,\r\n        \"availableAmt\": null,\r\n        \"card_status\": false,\r\n        \"mmUserId\": null,\r\n        \"cardId\": \"038c6a1f538cdb56d678e3c0be4c4f1b\",\r\n        \"cvv\": \"359\",\r\n        \"holderName\": \"kanchanbishst\",\r\n        \"cardCount\": 0,\r\n        \"countLimit\": false\r\n    }\r\n}";
		try {
			JSONObject jobject=new JSONObject(jsonString);
			System.err.println(jobject);
			String s=jobject.getString("message");
			System.err.println(s);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public RegisterError validateCorporateAgent(RegisterDTO dto) {
		RegisterError error = new RegisterError();
		boolean valid = true;
		if (dto.getCorporateName() == null || dto.getCorporateName().isEmpty()) {
			error.setUsername("Corporate name Required");
			error.setMessage("Corporate name Required");
			valid = false;
		} 
		MUser userC = mUserRespository.findByUsernameAndStatus((dto.getUsername().toLowerCase()), Status.Active);
		if (userC != null) {
			error.setUsername("Corporate Already Exist with this Email_id");
			error.setContactNo("User Already Exist");
			error.setMessage("Corporate Already Exist with this Email_id");
			valid = false;
		}

			if(!CommonValidation.isNullImage(dto.getCorporateLogo1())){
				}else{
				error.setMessage("Please upload Corporate Logo");
				valid=false;
			}
		
			if(!CommonValidation.isNullImage(dto.getCorporateLogo2())){
			
			}else{
			error.setMessage("Please upload Corporate Logo");
			valid=false;
		}
			
		 if (dto.getContactNo() == null || dto.getContactNo().isEmpty()) {
		 error.setContactNo("Contact No Required");
		 error.setMessage("Contact No Required");
		 valid = false;
		 } else if (CommonValidation.isValidNumber(dto.getContactNo())) {
		 if (!CommonValidation.checkLength10(dto.getContactNo())) {
		 error.setContactNo("Mobile number must be 10 digits long");
		 error.setMessage("Mobile number must be 10 digits long");
		 valid = false;
		 }
		 } else {
		 error.setContactNo("Please enter valid mobile number");
		 error.setMessage("Please enter valid mobile number");
		 valid = false;
		 }

		
		
		 if (dto.getEmail() == null || dto.getEmail().isEmpty()) {
		 error.setEmail("Email Required");
		 error.setMessage("Email Required");
		 valid = false;
		 } else if (!(CommonValidation.isValidMail(dto.getEmail()))) {
		 error.setEmail("Please enter valid mail address");
		 error.setMessage("Please enter valid mail address");
		 valid = false;
		 }else if(dto.getEmail().contains("@yopmail.com") || dto.getEmail().contains("@mailinator.com")){
			 error.setEmail("Please enter valid email address.");
			 error.setMessage("Please enter valid email address.");
			 valid = false; 
		 }
		else {
			if (!CommonValidation.isNull(dto.getEmail())) {
				List<MUserDetails> userDetail = mUserDetailRepository.checkMail(dto.getEmail());
				if (userDetail != null) {
					for (MUserDetails ud : userDetail) {
						MUser user = mUserRespository.findByUsernameAndMobileStatusAndEmailStatus(dto.getEmail(),
								Status.Active, Status.Active);
						if (user != null) {
							valid = false;
							error.setEmail("Email already exists");
							error.setMessage("Email already exists");
						}
					}
				}
			}
		}

		error.setValid(valid);
		return error;
	}
	
	public RegisterError validateCorporateUser(RegisterDTO dto) {
		RegisterError error = new RegisterError();
		boolean valid = true;
		/*if(dto.getDateOfBirth().isEmpty()||dto.getDateOfBirth()==null){
			valid=false;
			error.setMessage("please enter date of birth");
		}*/
		if (dto.getUsername() == null || dto.getUsername().isEmpty()) {
			error.setUsername("Username Required");
			error.setMessage("Username Required");
			valid = false;
		} else {
			MUser user = mUserRespository.findByUsernameAndStatus((dto.getUsername().toLowerCase()), Status.Active);
			if (user != null) {
				error.setUsername("Username Already Exist");
				error.setContactNo("User Already Exist");
				error.setMessage("User Already Exist");
				valid = false;
			}
		}

		/*if(dto.isHasAadhar()){
			if(!CommonValidation.isNull(dto.getAadharNo())){
			}else{
				error.setMessage("aadhar no is mandatory");
				valid=false;
			}
		}*/
		/*if (CommonValidation.isNull(dto.getLocationCode())) {
			valid = false;
			error.setLocationCode("Pincode Required");
			error.setMessage("Pincode Required");
		}*/ /*else {
			MLocationDetails locationDetails = locationDetailsRepository.findLocationByPin(dto.getLocationCode());
			if (locationDetails == null) {
				System.err.println(dto.getLocationCode());
				valid = false;
				error.setLocationCode("Not a valid Pincode");
				error.setMessage("Not a valid Pincode");
			}
		}*/

		 if (dto.getPassword() == null || dto.getPassword().isEmpty()) {
		 error.setPassword("Password Required");
		 error.setMessage("Password Required");
		 valid = false;
		 }
		
		
//		 if (dto.getConfirmPassword() == null ||
//		 dto.getConfirmPassword().isEmpty()) {
//		 error.setConfirmPassword("Password Required");
//		 error.setMessage
//		 valid = false;
//		 } else {
//		 if (!dto.getPassword().equals(dto.getConfirmPassword())) {
//		 error.setConfirmPassword("Password Mis-Match");
//		 error.setMessage
//		 valid = false;
//		 }
//		 }

		 if (dto.getContactNo() == null || dto.getContactNo().isEmpty()) {
		 error.setContactNo("Contact No Required");
		 error.setMessage("Contact No Required");
		 valid = false;
		 }/* else if (CommonValidation.isValidNumber(dto.getContactNo())) {
		 if (!CommonValidation.checkLength10(dto.getContactNo())) {
		 error.setContactNo("Mobile number must be 10 digits long");
		 error.setMessage("Mobile number must be 10 digits long");
		 valid = false;
		 }
		 }*/ /*else {
		 error.setContactNo("Please enter valid mobile number");
		 error.setMessage("Please enter valid mobile number");
		 valid = false;
		 }*/

//		 if (dto.getFirstName() == null || dto.getFirstName().isEmpty()) {
//		 error.setFirstName("First Name Required");
//		 error.setMessage("First Name Required");
//		 valid = false;
//		 }
//		
//		 if (dto.getLastName() == null || dto.getLastName().isEmpty()) {
//		 error.setLastName("Last Name Required");
//		 error.setMessage("Last Name Required");
//		 valid = false;
//		 }
		
		 if (dto.getEmail() == null || dto.getEmail().isEmpty()) {
		 error.setEmail("Email Required");
		 error.setMessage("Email Required");
		 valid = false;
		 } else if (!(CommonValidation.isValidMail(dto.getEmail()))) {
		 error.setEmail("Please enter valid mail address");
		 error.setMessage("Please enter valid mail address");
		 valid = false;
		 }else if(dto.getEmail().contains("@yopmail.com") || dto.getEmail().contains("@mailinator.com")){
			 error.setEmail("Please enter valid email address.");
			 error.setMessage("Please enter valid email address.");
			 valid = false; 
		 }else {
		if (!CommonValidation.isNull(dto.getEmail())) {
			List<MUserDetails> userDetail = mUserDetailRepository.checkMail(dto.getEmail());
			if (userDetail != null) {
				for (MUserDetails ud : userDetail) {
					MUser user = mUserRespository.findByUsernameAndMobileStatusAndEmailStatus(ud.getContactNo(),
							Status.Active, Status.Active);
					if (user != null) {
						valid = false;
						error.setEmail("Email already exists");
						error.setMessage("Email already exists");
					}
				}
				 }
			}
		
		MatchMoveCreateCardRequest cardRequest=new MatchMoveCreateCardRequest();
		cardRequest.setEmail(dto.getEmail());
		try {
			cardRequest.setPassword(SecurityUtil.md5(dto.getContactNo()));
		cardRequest.setUsername(dto.getContactNo());
		ResponseDTO responseDTO=matchMoveApi.fetchMMUser(cardRequest);
		if(responseDTO.getCode().equalsIgnoreCase("S00")){
			valid=false;
			error.setValid(valid);
			error.setMessage("Please use a different email id");
		}else if(responseDTO.getCode().equalsIgnoreCase("E01")){
			valid=false;
			error.setValid(valid);
			error.setMessage(responseDTO.getMessage());
		}else if(responseDTO.getCode().equalsIgnoreCase("E00")){
			valid=false;
			error.setValid(valid);
			error.setMessage(responseDTO.getMessage());
		}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			valid=false;
			error.setValid(valid);
			error.setMessage("Services are temporarily down..Please try after sometime");
			return error;
		}
		}

		error.setValid(valid);
		return error;
	}
	
	public ResponseDTO addAllUsers(RegisterDTO dto, GroupFileCatalogue catalogue) {
		ResponseDTO result = new ResponseDTO();
		try {
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			Date d1=sdf.parse(dto.getDateOfBirth());
			GroupBulkRegister gbr = groupBulkRegisterRepository.getUserRowByContact(dto.getContactNo(), catalogue, true, Status.Success);
			if(gbr != null) {
				gbr = new GroupBulkRegister();
				if(dto.getFirstName() != null || !(dto.getFirstName().isEmpty())) {
					gbr.setFirstName(dto.getFirstName());
				}
				if(dto.getLastName() != null || !(dto.getLastName().isEmpty())) {
					gbr.setLastName(dto.getLastName());
				}
				if(dto.getContactNo() !=null || !(dto.getContactNo().isEmpty())) {
					gbr.setMobile(dto.getContactNo());
				}
				if(dto.getEmail() != null || !(dto.getEmail().isEmpty())) {
					gbr.setEmail(dto.getEmail());
				}
				if(dto.getMiddleName() != null || !(dto.getMiddleName().isEmpty())) {
					gbr.setMiddlename(dto.getMiddleName());
				}
				if(dto.getDateOfBirth() != null || !(dto.getDateOfBirth().isEmpty())) {
					gbr.setDob(d1);
				}
				if(dto.getIdNo() != null || !(dto.getIdType().isEmpty())) {
					gbr.setIdType(dto.getIdType());
				}
				if(dto.getIdNo() != null || !(dto.getIdNo().isEmpty())) {
					gbr.setIdNumber(dto.getIdNo());
				}
				
				gbr.setCronStatus(true);
				gbr.setGroupDetail(catalogue.getUser().getGroupDetails());
				gbr.setGroupFileCatalogue(catalogue);
				gbr.setSuccessStatus(false);
				gbr.setFailReason("User already exist");
				gbr.setStatus(Status.Failed);
				groupBulkRegisterRepository.save(gbr);
				result.setCode(ResponseStatus.FAILURE.getValue());
			} else {
				gbr = new GroupBulkRegister();
				if(dto.getFirstName() != null || !(dto.getFirstName().isEmpty())) {
					gbr.setFirstName(dto.getFirstName());
				}
				if(dto.getLastName() != null || !(dto.getLastName().isEmpty())) {
					gbr.setLastName(dto.getLastName());
				}
				if(dto.getContactNo() !=null || !(dto.getContactNo().isEmpty())) {
					gbr.setMobile(dto.getContactNo());
				}
				if(dto.getEmail() != null || !(dto.getEmail().isEmpty())) {
					gbr.setEmail(dto.getEmail());
				}
				if(dto.getMiddleName() != null || !(dto.getMiddleName().isEmpty())) {
					gbr.setMiddlename(dto.getMiddleName());
				}
				if(dto.getDateOfBirth() != null || !(dto.getDateOfBirth().isEmpty())) {
					gbr.setDob(d1);
				}
				if(dto.getIdNo() != null || !(dto.getIdType().isEmpty())) {
					gbr.setIdType(dto.getIdType());
				}
				if(dto.getIdNo() != null || !(dto.getIdNo().isEmpty())) {
					gbr.setIdNumber(dto.getIdNo());
				}
				
				gbr.setCronStatus(true);
				gbr.setGroupDetail(catalogue.getUser().getGroupDetails());
				gbr.setGroupFileCatalogue(catalogue);
				gbr.setStatus(Status.Initiated);
				groupBulkRegisterRepository.save(gbr);
				result.setCode(ResponseStatus.SUCCESS.getValue());
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public ResponseDTO addKycData(RegisterDTO dto, GroupFileCatalogue catalogue) {
		ResponseDTO result = new ResponseDTO();
		try {
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			Date d1=sdf.parse(dto.getDateOfBirth());
			GroupBulkKyc gbr = groupBulkKycRepository.getUserRowByContact(dto.getContactNo(), catalogue, true, Status.Success);
			if(gbr != null) {
				gbr = new GroupBulkKyc();
				if(dto.getAddress() != null || dto.getAddress().isEmpty()) {
					gbr.setAddress1(dto.getAddress());
				}
				if(dto.getServices() != null || dto.getServices().isEmpty()) {
					gbr.setAddress2(dto.getServices());
				} 
				if(dto.getContactNo() != null || dto.getContactNo().isEmpty()) {
					gbr.setMobile(dto.getContactNo());
				}
				if(dto.getDateOfBirth() != null || dto.getDateOfBirth().isEmpty()) {
					gbr.setDob(d1);
				}
				if(dto.getEmail() != null || dto.getEmail().isEmpty()) {
					gbr.setEmail(dto.getEmail());
				}
				if(dto.getIdType() != null || dto.getIdType().isEmpty()) {
					gbr.setIdType(dto.getIdType());
				}
				if(dto.getIdNo() != null || dto.getIdNo().isEmpty()) {
					gbr.setIdNumber(dto.getIdNo());
				}
				if(dto.getFileName() != null || dto.getFileName().isEmpty()) {
					gbr.setPinCode(dto.getFileName());
				}
				if(dto.getAadharImagePath1() != null || dto.getAadharImagePath1().isEmpty()) {
					gbr.setDocumentUrl(dto.getAadharImagePath1());
				}
				if(dto.getAadharImagePath2() != null || dto.getAadharImagePath2().isEmpty()) {
					gbr.setDocumentUrl1(dto.getAadharImagePath2());
				}
				gbr.setCronStatus(true);
				gbr.setGroupDetail(catalogue.getUser().getGroupDetails());
				gbr.setGroupFileCatalogue(catalogue);
				gbr.setSuccessStatus(false);
				gbr.setFailReason("Duplicate KYC Data");
				gbr.setStatus(Status.Failed);
				groupBulkKycRepository.save(gbr);
				result.setCode(ResponseStatus.FAILURE.getValue());
			} else {
				gbr = new GroupBulkKyc();
				if(dto.getAddress() != null || dto.getAddress().isEmpty()) {
					gbr.setAddress1(dto.getAddress());
				}
				if(dto.getServices() != null || dto.getServices().isEmpty()) {
					gbr.setAddress2(dto.getServices());
				} 
				if(dto.getContactNo() != null || dto.getContactNo().isEmpty()) {
					gbr.setMobile(dto.getContactNo());
				}
				if(dto.getDateOfBirth() != null || dto.getDateOfBirth().isEmpty()) {
					gbr.setDob(d1);
				}
				if(dto.getEmail() != null || dto.getEmail().isEmpty()) {
					gbr.setEmail(dto.getEmail());
				}
				if(dto.getIdType() != null || dto.getIdType().isEmpty()) {
					gbr.setIdType(dto.getIdType());
				}
				if(dto.getIdNo() != null || dto.getIdNo().isEmpty()) {
					gbr.setIdNumber(dto.getIdNo());
				}
				if(dto.getFileName() != null || dto.getFileName().isEmpty()) {
					gbr.setPinCode(dto.getFileName());
				}
				if(dto.getAadharImagePath1() != null || dto.getAadharImagePath1().isEmpty()) {
					gbr.setDocumentUrl(dto.getAadharImagePath1());
				}
				if(dto.getAadharImagePath2() != null || dto.getAadharImagePath2().isEmpty()) {
					gbr.setDocumentUrl1(dto.getAadharImagePath2());
				}
				gbr.setCronStatus(true);
				gbr.setGroupDetail(catalogue.getUser().getGroupDetails());
				gbr.setGroupFileCatalogue(catalogue);
				gbr.setStatus(Status.Initiated);
				groupBulkKycRepository.save(gbr);
				result.setCode(ResponseStatus.SUCCESS.getValue());
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
/**
 * RECHECK VALIDATION
 * */

	public RegisterError validateCorporateUserRecheck(RegisterDTO dto) {
		RegisterError error = new RegisterError();
		boolean valid = true;
		
		
			if(dto.getDateOfBirth().isEmpty()||dto.getDateOfBirth()==null){
				valid=false;
				error.setMessage("please enter date of birth");
				
				
			}
			/*if (dto.getUsername() == null || dto.getUsername().isEmpty()) {
				error.setUsername("Username Required");
				error.setMessage("Username Required");
				valid = false;
			}*/
	
			/*if(dto.isHasAadhar()){
				if(!CommonValidation.isNull(dto.getAadharNo())){
				}else{
					error.setMessage("aadhar no is mandatory");
					valid=false;
				}
			}*/
			/*if (CommonValidation.isNull(dto.getLocationCode())) {
				valid = false;
				error.setLocationCode("Pincode Required");
				error.setMessage("Pincode Required");
			}*/ /*else {
				MLocationDetails locationDetails = locationDetailsRepository.findLocationByPin(dto.getLocationCode());
				if (locationDetails == null) {
					System.err.println(dto.getLocationCode());
					valid = false;
					error.setLocationCode("Not a valid Pincode");
					error.setMessage("Not a valid Pincode");
				}
			}*/
	
			/* if (dto.getPassword() == null || dto.getPassword().isEmpty()) {
			 error.setPassword("Password Required");
			 error.setMessage("Password Required");
			 valid = false;
			 }*/
			
			
	//		 if (dto.getConfirmPassword() == null ||
	//		 dto.getConfirmPassword().isEmpty()) {
	//		 error.setConfirmPassword("Password Required");
	//		 error.setMessage
	//		 valid = false;
	//		 } else {
	//		 if (!dto.getPassword().equals(dto.getConfirmPassword())) {
	//		 error.setConfirmPassword("Password Mis-Match");
	//		 error.setMessage
	//		 valid = false;
	//		 }
	//		 }
	
			 if (dto.getContactNo() == null || dto.getContactNo().isEmpty()) {
			 error.setContactNo("Contact No Required");
			 error.setMessage("Contact No Required");
			 valid = false;
			 
			 
			 } else if (CommonValidation.isValidNumber(dto.getContactNo())) {
			 if (!CommonValidation.checkLength10(dto.getContactNo())) {
				 error.setContactNo("Mobile number must be 10 digits long");
				 error.setMessage("Mobile number must be 10 digits long");
				 valid = false;
				 
				
			 }
			 } else {
			 error.setContactNo("Please enter valid mobile number");
			 error.setMessage("Please enter valid mobile number");
			 valid = false;
			 
			 
			 }
	
			 if (dto.getFirstName() == null || dto.getFirstName().isEmpty()) {
			 error.setFirstName("First Name Required");
			 error.setMessage("First Name Required");
			 valid = false;
			 
			
			 }
			
			 if (dto.getLastName() == null || dto.getLastName().isEmpty()) {
			 error.setLastName("Last Name Required");
			 error.setMessage("Last Name Required");
			 valid = false;
			 
			
			 }
			
			 if (dto.getEmail() == null || dto.getEmail().isEmpty()) {
				 error.setEmail("Email Required");
				 error.setMessage("Email Required");
				 valid = false;
				 
				
			 } else if (!(CommonValidation.isValidMail(dto.getEmail()))) {
				 error.setEmail("Please enter valid mail address");
				 error.setMessage("Please enter valid mail address");
				 valid = false;
				 
				 
			 }else if(dto.getEmail().contains("@yopmail.com") || dto.getEmail().contains("@mailinator.com")){
				 error.setEmail("Please enter valid email address.");
				 error.setMessage("Please enter valid email address.");
				 valid = false; 
				 
				
			 }else {
			
			
			MatchMoveCreateCardRequest cardRequest=new MatchMoveCreateCardRequest();
			cardRequest.setEmail(dto.getEmail());
			try {
				cardRequest.setPassword(SecurityUtil.md5(dto.getContactNo()));
			
			ResponseDTO responseDTO=matchMoveApi.fetchMMUser(cardRequest);
			if(responseDTO.getCode().equalsIgnoreCase("S00")){
				valid=false;
				error.setValid(valid);
				error.setMessage("Please use a different email id");
				
				
				
			}else if(responseDTO.getCode().equalsIgnoreCase("E01")){
				valid=false;
				error.setValid(valid);
				error.setMessage(responseDTO.getMessage());
				
			}else if(responseDTO.getCode().equalsIgnoreCase("E00")){
				valid=false;
				error.setValid(valid);
				error.setMessage(responseDTO.getMessage());
				
				
			}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				valid=false;
				error.setValid(valid);
				error.setMessage("Services are temporarily down..Please try after sometime");
				
			
				return error;
				
			}
			}
		

		error.setValid(valid);
		return error;
	}

	public RegisterError validateGroupUserRecheck(RegisterDTO dto, GroupFileCatalogue catalogue, boolean status) {
		RegisterError error = new RegisterError();
		boolean valid = true;
		GroupBulkRegister gbr = groupBulkRegisterRepository.getUserRowByContact(dto.getContactNo(), catalogue, status, Status.Initiated);
		
			if(dto.getDateOfBirth().isEmpty()||dto.getDateOfBirth()==null){
			
				valid=false;
				error.setMessage("please enter date of birth");
				
				gbr.setFailReason(error.getMessage());
				gbr.setStatus(Status.Failed);
				gbr.setSuccessStatus(false);
				groupBulkRegisterRepository.save(gbr);
			}
			/*if (dto.getUsername() == null || dto.getUsername().isEmpty()) {
				error.setUsername("Username Required");
				error.setMessage("Username Required");
				valid = false;
			}*/
	
			/*if(dto.isHasAadhar()){
				if(!CommonValidation.isNull(dto.getAadharNo())){
				}else{
					error.setMessage("aadhar no is mandatory");
					valid=false;
				}
			}*/
			/*if (CommonValidation.isNull(dto.getLocationCode())) {
				valid = false;
				error.setLocationCode("Pincode Required");
				error.setMessage("Pincode Required");
			}*/ /*else {
				MLocationDetails locationDetails = locationDetailsRepository.findLocationByPin(dto.getLocationCode());
				if (locationDetails == null) {
					System.err.println(dto.getLocationCode());
					valid = false;
					error.setLocationCode("Not a valid Pincode");
					error.setMessage("Not a valid Pincode");
				}
			}*/
	
			/* if (dto.getPassword() == null || dto.getPassword().isEmpty()) {
			 error.setPassword("Password Required");
			 error.setMessage("Password Required");
			 valid = false;
			 }*/
			
			
	//		 if (dto.getConfirmPassword() == null ||
	//		 dto.getConfirmPassword().isEmpty()) {
	//		 error.setConfirmPassword("Password Required");
	//		 error.setMessage
	//		 valid = false;
	//		 } else {
	//		 if (!dto.getPassword().equals(dto.getConfirmPassword())) {
	//		 error.setConfirmPassword("Password Mis-Match");
	//		 error.setMessage
	//		 valid = false;
	//		 }
	//		 }
	
			 if (dto.getContactNo() == null || dto.getContactNo().isEmpty()) {
			 error.setContactNo("Contact No Required");
			 error.setMessage("Contact No Required");
			 valid = false;
			 
			 	gbr.setFailReason(error.getMessage());
				gbr.setSuccessStatus(false);
				gbr.setStatus(Status.Failed);
				groupBulkRegisterRepository.save(gbr);
			 } else if (CommonValidation.isValidNumber(dto.getContactNo())) {
			 if (!CommonValidation.checkLength10(dto.getContactNo())) {
				 error.setContactNo("Mobile number must be 10 digits long");
				 error.setMessage("Mobile number must be 10 digits long");
				 valid = false;
				 
				 gbr.setFailReason(error.getMessage());
					gbr.setSuccessStatus(false);
					gbr.setStatus(Status.Failed);
					groupBulkRegisterRepository.save(gbr);
			 }
			 } else {
			 error.setContactNo("Please enter valid mobile number");
			 error.setMessage("Please enter valid mobile number");
			 valid = false;
			 
			 gbr.setFailReason(error.getMessage());
				gbr.setSuccessStatus(false);
				gbr.setStatus(Status.Failed);
				groupBulkRegisterRepository.save(gbr);
			 }
	
			 if (dto.getFirstName() == null || dto.getFirstName().isEmpty()) {
			 error.setFirstName("First Name Required");
			 error.setMessage("First Name Required");
			 valid = false;
			 
			 gbr.setFailReason(error.getMessage());
				gbr.setSuccessStatus(false);
				gbr.setStatus(Status.Failed);
				groupBulkRegisterRepository.save(gbr);
			 }
			
			 if (dto.getLastName() == null || dto.getLastName().isEmpty()) {
			 error.setLastName("Last Name Required");
			 error.setMessage("Last Name Required");
			 valid = false;
			 
			 	gbr.setFailReason(error.getMessage());
				gbr.setSuccessStatus(false);
				gbr.setStatus(Status.Failed);
				groupBulkRegisterRepository.save(gbr);
			 }
			
			 if (dto.getEmail() == null || dto.getEmail().isEmpty()) {
				 error.setEmail("Email Required");
				 error.setMessage("Email Required");
				 valid = false;
				 
				 gbr.setFailReason(error.getMessage());
					gbr.setSuccessStatus(false);
					gbr.setStatus(Status.Failed);
					groupBulkRegisterRepository.save(gbr);
			 } else if (!(CommonValidation.isValidMail(dto.getEmail()))) {
				 error.setEmail("Please enter valid mail address");
				 error.setMessage("Please enter valid mail address");
				 valid = false;
				 
				 gbr.setFailReason(error.getMessage());
					gbr.setSuccessStatus(false);
					gbr.setStatus(Status.Failed);
					groupBulkRegisterRepository.save(gbr);
			 }else if(dto.getEmail().contains("@yopmail.com") || dto.getEmail().contains("@mailinator.com")){
				 error.setEmail("Please enter valid email address.");
				 error.setMessage("Please enter valid email address.");
				 valid = false; 
				 
				 gbr.setFailReason(error.getMessage());
					gbr.setSuccessStatus(false);
					gbr.setStatus(Status.Failed);
					groupBulkRegisterRepository.save(gbr);
			 }else {
			
			
			MatchMoveCreateCardRequest cardRequest=new MatchMoveCreateCardRequest();
			cardRequest.setEmail(dto.getEmail());
			try {
				cardRequest.setPassword(SecurityUtil.md5(dto.getContactNo()));
			
			ResponseDTO responseDTO=matchMoveApi.fetchMMUser(cardRequest);
			if(responseDTO.getCode().equalsIgnoreCase("S00")){
				valid=false;
				error.setValid(valid);
				error.setMessage("Please use a different email id");
				
				gbr.setFailReason(error.getMessage());
				gbr.setSuccessStatus(false);
				gbr.setStatus(Status.Failed);
				groupBulkRegisterRepository.save(gbr);
				
			}else if(responseDTO.getCode().equalsIgnoreCase("E01")){
				valid=false;
				error.setValid(valid);
				error.setMessage(responseDTO.getMessage());
				
				gbr.setFailReason(responseDTO.getMessage());
				gbr.setSuccessStatus(false);
				gbr.setStatus(Status.Failed);
				groupBulkRegisterRepository.save(gbr);
				
			}else if(responseDTO.getCode().equalsIgnoreCase("E00")){
				valid=false;
				error.setValid(valid);
				error.setMessage(responseDTO.getMessage());
				
				gbr.setFailReason(responseDTO.getMessage());
				gbr.setSuccessStatus(false);
				gbr.setStatus(Status.Failed);
				groupBulkRegisterRepository.save(gbr);
			}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				valid=false;
				error.setValid(valid);
				error.setMessage("Services are temporarily down..Please try after sometime");
				
				gbr.setFailReason(error.getMessage());
				gbr.setSuccessStatus(false);
				gbr.setStatus(Status.Failed);
				groupBulkRegisterRepository.save(gbr);
				return error;
				
			}
			}
		/*} else {
			gbr.setFailReason("User already exists");
			gbr.setSuccessStatus(false);
			groupBulkRegisterRepository.save(gbr);
		}
*/
		error.setValid(valid);
		return error;
	}
	
	/**
	 * AGENT VALIDATION
	 * */
	
	public RegisterError validateAgent(RegisterDTO dto) {
		  RegisterError error = new RegisterError();
		  boolean valid = true;
		  if (dto.getUsername() == null || dto.getUsername().isEmpty()) {
		   error.setUsername("Username Required");
		   error.setMessage("Username Required");
		   valid = false;
		  } else {
		   MUser user = mUserRespository.findByUsernameAndStatus((dto.getUsername().toLowerCase()), Status.Active);
		   if (user != null) {
		    error.setUsername("Username Already Exist");
		    error.setContactNo("User Already Exist");
		    error.setMessage("User Already Exist");
		    valid = false;
		   }
		  }
		  
		   if(!CommonValidation.isNullImage(dto.getAadharImage1())){
		    }else{
		    error.setMessage("Please upload your Aadhar card front side");
		    valid=false;
		   }
		   
		   if(!CommonValidation.isNullImage(dto.getAadharImage2())){
		   }else{
		   error.setMessage("Please upload your Aadhar card back side");
		   valid=false;
		  }
		  
		   if(!CommonValidation.isNullImage(dto.getPanCardImage())){
		   
		   }else{
		   error.setMessage("Please upload your Pan card");
		   valid=false;
		  }
		   
		   if (dto.getContactNo() == null || dto.getContactNo().isEmpty()) {
		    error.setContactNo("Contact No Required");
		    error.setMessage("Contact No Required");
		    valid = false;
		   } else if (CommonValidation.isValidNumber(dto.getContactNo())) {
		    if (!CommonValidation.checkLength10(dto.getContactNo())) {
		     error.setContactNo("Mobile number must be 10 digits long");
		     error.setMessage("Mobile number must be 10 digits long");
		     valid = false;
		    }
		   } else {
		    error.setContactNo("Please enter valid mobile number");
		    error.setMessage("Please enter valid mobile number");
		    valid = false;
		   }

		   if (dto.getFirstName() == null || dto.getFirstName().isEmpty()) {
		    error.setFirstName("First Name Required");
		    error.setMessage("First Name Required");
		    valid = false;
		   }
		  
		   if (dto.getEmail() == null || dto.getEmail().isEmpty()) {
		    error.setEmail("Email Required");
		    error.setMessage("Email Required");
		    valid = false;
		   } else if (!(CommonValidation.isValid(dto.getEmail()))) {
		    error.setEmail("Please enter valid mail address");
		    error.setMessage("Please enter valid mail address");
		    valid = false;
		   } else if(dto.getEmail().contains("@yopmail.com") || dto.getEmail().contains("@mailinator.com")) {
		    error.setEmail("Please enter valid email address.");
		    error.setMessage("Please enter valid email address.");
		    valid = false; 
		   }
		  else {
		   if (!CommonValidation.isNull(dto.getEmail())) {
		    List<MUserDetails> userDetail = mUserDetailRepository.checkMail(dto.getEmail());
		    if (userDetail != null) {
		     for (MUserDetails ud : userDetail) {
		      MUser user = mUserRespository.findByUsernameAndMobileStatusAndEmailStatus(dto.getEmail(),
		        Status.Active, Status.Active);
		      if (user != null) {
		       valid = false;
		       error.setEmail("Email already exists");
		       error.setMessage("Email already exists");
		      }
		     }
		    }
		   }
		  }

		  error.setValid(valid);
		  return error;
		 }
	
	
		public RegisterError validateUser(RegisterDTO dto) {
		  RegisterError registerError = new RegisterError();
		  boolean valid = true;
		  if (dto.getFirstName() == null && dto.getFirstName().isEmpty()) {
			   registerError.setMessage("First name Required");
			   valid = false;
		  } else {
			   MUser user = mUserRespository.findByUsernameAndStatus((dto.getContactNo()), Status.Active);
			   if (user != null) {
			    registerError.setUsername("Username Already Exist");
			    registerError.setContactNo("User Already Exist");
			    registerError.setMessage("User Already Exist");
			    valid = false;
			   }
		  }
		  if (dto.getContactNo() == null || dto.getContactNo().isEmpty()) {
			   registerError.setContactNo("Contact No Required");
			   registerError.setMessage("Contact No Required");
			   valid = false;
		  } else if (CommonValidation.isValidNumber(dto.getContactNo())) {
			    if (!CommonValidation.checkLength10(dto.getContactNo())) {
			     registerError.setContactNo("Mobile number must be 10 digits long");
			     registerError.setMessage("Mobile number must be 10 digits long");
			     valid = false;
		   }
		  } else {
			   registerError.setContactNo("Please enter valid mobile number");
			   registerError.setMessage("Please enter valid mobile number");
			   valid = false;
		  }
		  
		  if (dto.getEmail() == null || dto.getEmail().isEmpty()) {
			   registerError.setEmail("Email Required");
			   registerError.setMessage("Email Required");
			   valid = false;
		  } else if (!(CommonValidation.isValidMail(dto.getEmail()))) {
			   registerError.setEmail("Please enter valid mail address");
			   registerError.setMessage("Please enter valid mail address");
			   valid = false;
		  } else if(dto.getEmail().contains("@yopmail.com") || dto.getEmail().contains("@mailinator.com")){
			   registerError.setEmail("Please enter valid email address.");
			   registerError.setMessage("Please enter valid email address.");
			   valid = false; 
		  } else {
			   if (!CommonValidation.isNull(dto.getEmail())) {
			    List<MUserDetails> userDetail = mUserDetailRepository.checkMail(dto.getEmail());
			    if (userDetail != null && !userDetail.isEmpty()) {
			     /*for (MUserDetails ud : userDetail) {
			      MUser user = userRepository.findByUsernameAndMobileStatusAndEmailStatus(dto.getEmail(),
			        Status.Active, Status.Active);
			      if (user != null) {
			       valid = false;
			       registerError.setEmail("Email already exists");
			       registerError.setMessage("Email already exists");
			      }
			     }*/
			    	valid = false;
				    registerError.setEmail("Email already exists");
				    registerError.setMessage("Email already exists");
			    }
			   }
		  }
		  /*if (dto.getPassword() == null && dto.getPassword().isEmpty()) {
		   registerError.setMessage("Password required.");
		  }
		  if (dto.getConfirmPassword() == null && dto.getConfirmPassword().isEmpty()) {
		   registerError.setMessage("Confirm Password required.");
		  }
		  if (!dto.getPassword().equalsIgnoreCase(dto.getConfirmPassword())) {
		   registerError.setMessage("Password and Confirm Password does not match.");
		  }*/
		  
		  registerError.setValid(valid);
		  return registerError;
		  
		 }
		
		
		/**
		 * ADD MERCHANT
		 * */
		
		public RegisterError validateMerchant(AddMerchantDTO dto) {
			RegisterError error = new RegisterError();
			boolean valid = true;
			if (dto.getBusinessCorrespondentName() == null || dto.getBusinessCorrespondentName().isEmpty()) {
				error.setUsername("BC name Required");
				error.setMessage("BC name Required");
				valid = false;
			} 
			
			if(dto.getMerchantName()!=null){
			MerchantDetails merchantDetails=merchantDetailsRepository.findByMerchantName(dto.getMerchantName().trim());
			if(merchantDetails!=null){
				valid=false;
				error.setMessage("Merchant with this name already exists");
				error.setValid(valid);
				return error;
			}
			
			}else{
				valid=false;
				error.setValid(valid);
				error.setMessage("Please enter Merchant Name");
			}
			 if (dto.getBusinessCorrespondentNumber() == null || dto.getBusinessCorrespondentNumber().isEmpty()) {
			 error.setContactNo("Contact No Required");
			 error.setMessage("Contact No Required");
			 valid = false;
			 } else if (CommonValidation.isValidNumber(dto.getBusinessCorrespondentNumber())) {
			 if (!CommonValidation.checkLength10(dto.getBusinessCorrespondentNumber())) {
			 error.setContactNo("Mobile number must be 10 digits long");
			 error.setMessage("Mobile number must be 10 digits long");
			 valid = false;
			 }
			 } else {
			 error.setContactNo("Please enter valid mobile number");
			 error.setMessage("Please enter valid mobile number");
			 valid = false;
			 }

			
			
			 if (dto.getBusinessCorrespondentEmail() == null || dto.getBusinessCorrespondentEmail().isEmpty()) {
			 error.setEmail("Email Required");
			 error.setMessage("Email Required");
			 valid = false;
			 } else if (!(CommonValidation.isValidMail(dto.getBusinessCorrespondentEmail()))) {
			 error.setEmail("Please enter valid mail address");
			 error.setMessage("Please enter valid mail address");
			 valid = false;
			 }else if(dto.getBusinessCorrespondentEmail().contains("@yopmail.com") || dto.getBusinessCorrespondentEmail().contains("@mailinator.com")){
				 error.setEmail("Please enter valid email address.");
				 error.setMessage("Please enter valid email address.");
				 valid = false; 
			 }
			else {
			}

			error.setValid(valid);
			return error;
		}
		
		public RegisterError validateKyc(RegisterDTO dto, GroupFileCatalogue fileCatalogue, boolean status) {
			RegisterError error = new RegisterError();
			boolean valid = true;
			
			GroupBulkKyc gbr = groupBulkKycRepository.getUserRowByContact(dto.getContactNo(), fileCatalogue, status, Status.Initiated);
			
			if(dto.getContactNo() == null || dto.getContactNo().isEmpty()) {
				valid = false;
				error.setMessage("Contact No Required");
				error.setValid(valid);	
				
				gbr.setFailReason(error.getMessage());
				gbr.setSuccessStatus(false);
				gbr.setStatus(Status.Failed);
				groupBulkKycRepository.save(gbr);
				
			} else if (CommonValidation.isValidNumber(dto.getContactNo())) {
				 if (!CommonValidation.checkLength10(dto.getContactNo())) {
					error.setContactNo("Mobile number must be 10 digits long");
					error.setMessage("Mobile number must be 10 digits long");
					valid = false;
					error.setValid(valid);	
					 
					gbr.setFailReason(error.getMessage());
					gbr.setSuccessStatus(false);
					gbr.setStatus(Status.Failed);
					groupBulkKycRepository.save(gbr);
				 }
			} else {
				 error.setContactNo("Please enter valid mobile number");
				 error.setMessage("Please enter valid mobile number");
				 valid = false;
				 error.setValid(valid);
				 
				gbr.setFailReason(error.getMessage());
				gbr.setSuccessStatus(false);
				gbr.setStatus(Status.Failed);
				groupBulkKycRepository.save(gbr);
			}
			
			if(dto.getEmail() == null || dto.getEmail().isEmpty()) {
				valid = false;
				error.setValid(valid);
				error.setEmail("email cannot be empty");
				error.setMessage("email cannot be empty");
				
				gbr.setFailReason(error.getMessage());
				gbr.setSuccessStatus(false);
				gbr.setStatus(Status.Failed);
				groupBulkKycRepository.save(gbr);
			} else if (!(CommonValidation.isValidMail(dto.getEmail()))) {
				   error.setEmail("Please enter valid mail address");
				   error.setMessage("Please enter valid mail address");
				   valid = false;
				   error.setValid(valid);
				   
				   gbr.setFailReason(error.getMessage());
				   gbr.setSuccessStatus(false);
				   gbr.setStatus(Status.Failed);
				   groupBulkKycRepository.save(gbr);
			  } else if(dto.getEmail().contains("@yopmail.com") || dto.getEmail().contains("@mailinator.com")){
				   error.setEmail("Please enter valid email address.");
				   error.setMessage("Please enter valid email address.");
				   valid = false; 
				   error.setValid(valid);
				   
				   gbr.setFailReason(error.getMessage());
				   gbr.setSuccessStatus(false);
				   gbr.setStatus(Status.Failed);
				   groupBulkKycRepository.save(gbr);
			  } /*else {
				   if (!CommonValidation.isNull(dto.getEmail())) {
				    List<MUserDetails> userDetail = mUserDetailRepository.checkMail(dto.getEmail());
				    if (userDetail != null && !userDetail.isEmpty()) {				   
				    	valid = false;
					    error.setEmail("Email already exists");
					    error.setMessage("Email already exists");
					    error.setValid(valid);
					    
					    gbr.setFailReason(error.getMessage());
						gbr.setSuccessStatus(false);
						gbr.setStatus(Status.Failed);
						groupBulkKycRepository.save(gbr);
				    }
				 }
			  }*/
			
			if(dto.getDateOfBirth() == null || dto.getDateOfBirth().isEmpty()) {
				valid = false;
				error.setMessage("date of birth cannot be empty");
				error.setValid(valid);
				
				gbr.setFailReason(error.getMessage());
				gbr.setSuccessStatus(false);
				gbr.setStatus(Status.Failed);
				groupBulkKycRepository.save(gbr);
			}
			
			System.err.println("address:: "+dto.getAddress());
			System.err.println("address2:: "+dto.getServices());
			
			if(dto.getAddress() == null || dto.getAddress().isEmpty()) {
				valid = false;
				error.setMessage("Address1 cannot be empty");
				error.setValid(valid);
				
				gbr.setFailReason(error.getMessage());
				gbr.setSuccessStatus(false);
				gbr.setStatus(Status.Failed);
				groupBulkKycRepository.save(gbr);
			}
			
			if(dto.getServices() == null || dto.getServices().isEmpty()) {
				valid = false;
				error.setMessage("Address2 cannot be empty");
				error.setValid(valid);
				
				gbr.setFailReason(error.getMessage());
				gbr.setSuccessStatus(false);
				gbr.setStatus(Status.Failed);
				groupBulkKycRepository.save(gbr);
			}
			
			if(dto.getIdType() == null || dto.getIdType().isEmpty()) {
				valid = false;
				error.setMessage("IdType cannot be empty");
				error.setValid(valid);
				
				gbr.setFailReason(error.getMessage());
				gbr.setSuccessStatus(false);
				gbr.setStatus(Status.Failed);
				groupBulkKycRepository.save(gbr);
			}
			
			if(dto.getIdNo() == null || dto.getIdNo().isEmpty()) {
				valid = false;
				error.setMessage("IdNo cannot be empty");
				error.setValid(valid);
				
				gbr.setFailReason(error.getMessage());
				gbr.setSuccessStatus(false);
				gbr.setStatus(Status.Failed);
				groupBulkKycRepository.save(gbr);
			}
			
			if(dto.getFileName() == null || dto.getFileName().isEmpty()) {
				valid = false;
				error.setMessage("Pincode cannot be empty");
				error.setValid(valid);
				
				gbr.setFailReason(error.getMessage());
				gbr.setSuccessStatus(false);
				gbr.setStatus(Status.Failed);
				groupBulkKycRepository.save(gbr);
			}
			
			if(dto.getAadharImagePath1() == null || dto.getAadharImagePath1().isEmpty()) {
				valid = false;
				error.setMessage("Id proof 1 should not be empty");
				error.setValid(valid);
				
				gbr.setFailReason(error.getMessage());
				gbr.setSuccessStatus(false);
				gbr.setStatus(Status.Failed);
				groupBulkKycRepository.save(gbr);
			}
			
			if(dto.getAadharImagePath2() == null || dto.getAadharImagePath2().isEmpty()) {
				valid = false;
				error.setMessage("Id proof 2 should not be empty");
				error.setValid(valid);
				
				gbr.setFailReason(error.getMessage());
				gbr.setSuccessStatus(false);
				gbr.setStatus(Status.Failed);
				groupBulkKycRepository.save(gbr);
			}
			error.setValid(valid);
			return error;
		}
		
		
			

	
	}
