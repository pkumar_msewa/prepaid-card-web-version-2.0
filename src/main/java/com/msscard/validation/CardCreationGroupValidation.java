package com.msscard.validation;

import com.msscard.entity.MMCards;
import com.msscard.entity.MUser;
import com.msscard.entity.MatchMoveWallet;
import com.msscard.model.error.GroupCardGenerationErrorDTO;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MUserRespository;
import com.msscard.repositories.MatchMoveWalletRepository;

public class CardCreationGroupValidation {

	private final MUserRespository mUserRespository;
	private final MatchMoveWalletRepository matchMoveWalletRepository;
	private final MMCardRepository mMCardRepository;
	
	public CardCreationGroupValidation(MUserRespository mUserRespository,
			MatchMoveWalletRepository matchMoveWalletRepository, MMCardRepository mMCardRepository) {
		super();
		this.mUserRespository = mUserRespository;
		this.matchMoveWalletRepository = matchMoveWalletRepository;
		this.mMCardRepository = mMCardRepository;
	}
	
	public GroupCardGenerationErrorDTO validateCardCreation(String mobile){
		boolean valid=true;
		GroupCardGenerationErrorDTO error=new GroupCardGenerationErrorDTO();
		MUser user=mUserRespository.findByUsername(mobile);
		if(user==null){
			valid=false;
			error.setValid(valid);
			error.setMessage("User doesn't exist");
			return error;
		}else{
			MatchMoveWallet wallet=matchMoveWalletRepository.findByUserName(mobile);
			if(wallet==null){
				valid=false;
				error.setValid(valid);
				error.setMessage("Wallet is not created for this user");
				return error;
			}else{
				MMCards card=mMCardRepository.getPhysicalCardByUserName(mobile);
				if(card!=null){
					valid=false;
					error.setValid(valid);
					error.setMessage("Physical already assigned to this user");
					return error;
				}
			}
		}
		error.setValid(valid);
		return error;
	}
}
