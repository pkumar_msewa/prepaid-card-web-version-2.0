package com.msscard.validation;

import java.util.List;

import com.ewire.paramerterization.DataConfig;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.model.request.SendMoneyRequestDTO;
import com.msscard.entity.MMCards;
import com.msscard.entity.MPQAccountDetails;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.entity.SendMoneyDetails;
import com.msscard.model.error.SendMoneyErrorDTO;
import com.msscard.repositories.DataConfigRepository;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MTransactionRepository;
import com.msscard.repositories.MUserRespository;
import com.msscard.repositories.SendMoneyDetailsRepository;

public class SendMoneyValidation {

	private final MUserRespository userRespository;
	private final DataConfigRepository dataConfigRepository;
	private final IMatchMoveApi matchMoveApi;
	private final MMCardRepository cardRepository;
	private final SendMoneyDetailsRepository sendMoneyDetailsRepository;
	public SendMoneyValidation(MUserRespository userRespository, DataConfigRepository dataConfigRepository,
			IMatchMoveApi matchMoveApi,MMCardRepository cardRepository,
			SendMoneyDetailsRepository sendMoneyDetailsRepository) {
		super();
		this.userRespository = userRespository;
		this.dataConfigRepository = dataConfigRepository;
		this.matchMoveApi = matchMoveApi;
		this.cardRepository=cardRepository;
		this.sendMoneyDetailsRepository=sendMoneyDetailsRepository;
	}
	
	public SendMoneyErrorDTO sendMoneyError(SendMoneyRequestDTO error,MUser sender){
		boolean valid=true;
		SendMoneyErrorDTO errorDTO=new SendMoneyErrorDTO();
		double reqAmt=Double.parseDouble(error.getAmount());
		MUser recipeintUser=userRespository.findByUsername(error.getRecipientNo());
		DataConfig datas=dataConfigRepository.findDatas();
		double minAmountLimit=Double.parseDouble(datas.getSendMoneyMinimum());
		double maxAmountLimit=Double.parseDouble(datas.getSendMoneyMaximum());
		double balance=matchMoveApi.getBalance(sender);
		MMCards recipientVCard=cardRepository.getVirtualCardsByCardUser(recipeintUser);
		MMCards recipientPCard=cardRepository.getPhysicalCardByUser(recipeintUser);
		MMCards senderVCard=cardRepository.getVirtualCardsByCardUser(sender);
		MMCards senderPCard=cardRepository.getPhysicalCardByUser(sender);
		MMCards activeCardSender=null;
		MMCards activeCardRecipient=null;
		
		if(sender.getAccountDetail().getAccountType().getCode().trim().equalsIgnoreCase("NONKYC")){
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("Please upgrade your account to KYC to avail this feature");
			return errorDTO;
		}
		if(sender.getUsername().equalsIgnoreCase(error.getRecipientNo())){
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("You cannot send money to yourself..Please Stop trying..");
			return errorDTO;
		}
		/*if(sender!=null){
			List<SendMoneyDetails> accTransaction=sendMoneyDetailsRepository.getSendMoneyDetails(sender);
			System.err.println("hiiiiiiiiiiiiiiiiii...m here");
			if(accTransaction!=null && !(accTransaction.isEmpty())){
				System.err.println("valid is false............");
				valid=false;
				errorDTO.setValid(valid);
				errorDTO.setMessage("Your previous send money status is pending.Please try after sometime");
				return errorDTO;
			}
			
		}*/
		
		if(reqAmt>maxAmountLimit){
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("The maximum limit for sending money is Rs "+maxAmountLimit);
			return errorDTO;

		}
		
		if(reqAmt<minAmountLimit){
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("The minimum limit for sending money is Rs "+minAmountLimit);
			return errorDTO;

		}

		
		if(recipeintUser==null){
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("Invalid Recipient.Transaction failed");
			return errorDTO;
		}else if(recipeintUser!=null){
			if(recipeintUser.getMobileStatus().getValue().equalsIgnoreCase("Inactive")){
				valid=false;
				errorDTO.setValid(valid);
				errorDTO.setMessage("Recipient is Inactive User.Transaction failed");
				return errorDTO;
			}else{
				if(recipeintUser.isFullyFilled()==false){
					valid=false;
					errorDTO.setValid(valid);
					errorDTO.setMessage("Recipient profile is not valid.Transaction failed");
					return errorDTO;

				}
			}
			if(recipeintUser.getAccountDetail().getAccountType().getCode().trim().equalsIgnoreCase("NONKYC")){
				valid=false;
				errorDTO.setValid(valid);
				errorDTO.setMessage("Recipient has to be KYC to Send Money");
				return errorDTO;
			}
		}
		
		if(recipientVCard==null){
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("Recipient has no Active card.Transaction failed...");
			return errorDTO;
		}
		
		if(senderVCard==null){
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("Sender has no Active card.Transaction failed");
			return errorDTO;
		}
		if(recipientPCard!=null){
			if(recipientPCard.getStatus().equalsIgnoreCase("Active")) {
				activeCardRecipient=recipientPCard;
			} else {
				activeCardRecipient=recipientVCard;
			}
		} else {
			activeCardRecipient=recipientVCard;
		}
		if(senderPCard!=null){
		if(senderPCard.getStatus().equalsIgnoreCase("Active")){
			activeCardSender=senderPCard;
		}else{
			activeCardSender=senderVCard;
		}
		}else{
			activeCardSender=senderVCard;

		}
				
		if(activeCardRecipient == null) {
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("Recipient has no Active card.Transaction failed");
			return errorDTO;
		}
		
		if(activeCardSender == null) {
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("Sender has no Active card.Transaction failed");
			return errorDTO;

		}
		if(reqAmt > balance){
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("You have insufficient funds.Transaction failed");
			return errorDTO;

		}
		/*if(senderVCard!=null && senderVCard.isBlocked()){
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("Your card is blocked.Transaction failed");
			return errorDTO;
		}
		
		if(recipientVCard!=null && recipientVCard.isBlocked()){
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("Recipient card is blocked.Transaction failed");
			return errorDTO;
		}*/
		
		/*if(senderPCard!=null && senderPCard.isBlocked()){
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("Your card is blocked.Transaction failed");
			return errorDTO;
		}
		
		if(recipientPCard!=null && recipientPCard.isBlocked()){
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("Recipient card is blocked.Transaction failed");
			return errorDTO;
		}*/
		/*if(senderVCard!=null && senderVCard.getStatus().equalsIgnoreCase("Active")){
			valid=false;
			errorDTO.setMessage("Send Money is only allowed via Physical Card");
			errorDTO.setValid(valid);
			return errorDTO;
		}
		if(recipientVCard!=null && recipientVCard.getStatus().equalsIgnoreCase("Active")){
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("Recipient must have a physical card to receive money");
			return errorDTO;
		}*/
		errorDTO.setValid(valid);
		return errorDTO;
	}
	
	public SendMoneyErrorDTO donationMoneyError(SendMoneyRequestDTO error,MUser sender){
		boolean valid=true;
		SendMoneyErrorDTO errorDTO=new SendMoneyErrorDTO();
		double reqAmt=Double.parseDouble(error.getAmount());		
		DataConfig datas=dataConfigRepository.findDatas();
		double minAmountLimit=Double.parseDouble(datas.getSendMoneyMinimum());
		double maxAmountLimit=Double.parseDouble(datas.getSendMoneyMaximum());
		double balance=matchMoveApi.getBalance(sender);
		
		MMCards senderVCard=cardRepository.getVirtualCardsByCardUser(sender);
		MMCards senderPCard=cardRepository.getPhysicalCardByUser(sender);
		MMCards activeCardSender=null;
				
		if(sender.getAccountDetail().getAccountType().getCode().trim().equalsIgnoreCase("NONKYC")){
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("Please upgrade your account to KYC to avail this feature");
			return errorDTO;
		}
		if(sender.getUsername().equalsIgnoreCase(error.getRecipientNo())){
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("You cannot send money to yourself..Please Stop trying..");
			return errorDTO;
		}
		/*if(sender!=null){
			List<SendMoneyDetails> accTransaction=sendMoneyDetailsRepository.getSendMoneyDetails(sender);
			System.err.println("hiiiiiiiiiiiiiiiiii...m here");
			if(accTransaction!=null && !(accTransaction.isEmpty())){
				System.err.println("valid is false............");
				valid=false;
				errorDTO.setValid(valid);
				errorDTO.setMessage("Your previous send money status is pending.Please try after sometime");
				return errorDTO;
			}
			
		}*/
		
		if(reqAmt>maxAmountLimit){
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("The maximum limit for sending money is Rs "+maxAmountLimit);
			return errorDTO;

		}
		
		if(reqAmt<minAmountLimit){
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("The minimum limit for sending money is Rs "+minAmountLimit);
			return errorDTO;

		}
		
		if(senderVCard==null){
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("Sender has no Active card.Transaction failed");
			return errorDTO;
		}
		
		if(senderPCard!=null){
		if(senderPCard.getStatus().equalsIgnoreCase("Active")){
			activeCardSender=senderPCard;
		}else{
			activeCardSender=senderVCard;
		}
		}else{
			activeCardSender=senderVCard;

		}
				
		
		
		if(activeCardSender==null){
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("Sender has no Active card.Transaction failed");
			return errorDTO;

		}
		if(reqAmt>balance){
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("You have insufficient funds.Transaction failed");
			return errorDTO;

		}
		/*if(senderVCard!=null && senderVCard.isBlocked()){
			if(senderPCard!=null && senderPCard.isBlocked()){
				valid=false;
				errorDTO.setValid(valid);
				errorDTO.setMessage("Your card is blocked.Transaction failed");
				return errorDTO;
			}
		}*/
		
	
		/*if(senderPCard!=null && senderPCard.isBlocked()){
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("Your card is blocked.Transaction failed");
			return errorDTO;
		}*/
		
	
		errorDTO.setValid(valid);
		return errorDTO;
	}
}
