package com.msscard.validation;

import com.msscard.entity.MUser;
import com.msscard.entity.MUserDetails;
import com.msscard.model.Status;
import com.msscard.model.error.VerifyMobileOTPError;
import com.msscard.repositories.MUserDetailRepository;
import com.msscard.repositories.MUserRespository;

public class MobileOTPValidation {

	private final MUserRespository userRepository;
	private final MUserDetailRepository userDetailRepository;

	public MobileOTPValidation(MUserRespository userRepository,MUserDetailRepository userDetailRepository) {
		this.userRepository = userRepository;
		this.userDetailRepository = userDetailRepository;
	}

	/**
	 * For OTP Validation
	 * 
	 * @param OTP
	 * @param phoneNO
	 * @return
	 */
	public VerifyMobileOTPError validateOTP(String OTP, String username) {
		VerifyMobileOTPError error = new VerifyMobileOTPError();
		boolean valid = true;

		if (OTP.length() == 0) {
			error.setOtp("OTP Required");
			valid = false;
		}

		if (OTP.length() != 6) {
			error.setOtp("OTP is 6 digit");
			valid = false;
		}

		MUser u = userRepository.findByUsername(username);
		if (u != null) {
			if (u.getMobileStatus().equals(Status.Active)) {
				error.setOtp(username + " Mobile Number is already Verified");
				valid = false;
			}
		}
		error.setValid(valid);
		return error;
	}

	public VerifyMobileOTPError validateResendOTP(String username) {
		VerifyMobileOTPError error = new VerifyMobileOTPError();
		boolean valid = true;

		/*MUser u = userRepository.findByUsernameAndStatus(username, Status.Active);
		if (u == null) {
			valid = true;
		} else {
			error.setOtp(username + " Mobile Number is already verified");
			valid = true;
		}*/
		error.setValid(valid);
		return error;
	}
	
	public VerifyMobileOTPError validateMerchantResendOTP(String username) {
		VerifyMobileOTPError error = new VerifyMobileOTPError();
		boolean valid = true;
		MUserDetails userDetail = userDetailRepository.findByContactNo(username);
		System.out.println(userDetail.getEmail());
		username=userDetail.getEmail();
		MUser u = userRepository
				.findByUsernameAndStatus(username, Status.Active);
		if (u == null) {
			valid = true;
		} else {
			error.setOtp(username + " Mobile Number is already verified");
			valid = false;
		}
		error.setValid(valid);
		return error;
	}
}
