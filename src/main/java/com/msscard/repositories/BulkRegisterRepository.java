package com.msscard.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.BulkRegister;
import com.msscard.entity.CorporateAgentDetails;
import com.msscard.entity.MUser;
import com.msscard.entity.PartnerDetails;

public interface BulkRegisterRepository extends CrudRepository<BulkRegister,Long>,JpaSpecificationExecutor<BulkRegister>,PagingAndSortingRepository<BulkRegister,Long>{

	@Query("select b from BulkRegister b where b.agentDetails=?1")
	List<BulkRegister> getRegisteredUsers(CorporateAgentDetails corporateDetails);

	@Query("select b from BulkRegister b where b.agentDetails=?3 and DATE(b.created) BETWEEN ?1 AND ?2")
	List<BulkRegister> getRegisteredUsersByDate(Date fromDate, Date toDate, CorporateAgentDetails agentDetails);
	
	@Query("select b from BulkRegister b where b.partnerDetails=?3 and DATE(b.created) BETWEEN ?1 AND ?2")
	List<BulkRegister> getRegisteredUserPartner(Date fromDate, Date toDate, PartnerDetails agentDetails);
	
	@Query("select b from BulkRegister b where b.user=?1")
	BulkRegister getByUser(MUser user);
	
	@Query("select b from BulkRegister b where b.cardCreationStatus=0 and b.agentDetails=?1")
	List<BulkRegister> getFailedUserList(CorporateAgentDetails details);
	
	@Query("select b from BulkRegister b where b.user=?1")
	BulkRegister getUser(MUser user);
	
	
	@Query("select b from BulkRegister b where b.partnerDetails=?1")
	List<BulkRegister> getPartnerUsers(PartnerDetails details);
	
	@Query("select b from BulkRegister b where b.agentDetails=6")
	List<BulkRegister> getRegisteredUsers();


}
