package com.msscard.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;


import com.msscard.entity.UpiPay;

public interface UpiPayRepository extends CrudRepository<UpiPay, Long>, PagingAndSortingRepository<UpiPay, Long>, JpaSpecificationExecutor<UpiPay>{
	
	@Query("select c from UpiPay c where c.username=?1")
	UpiPay getDataUsingUsername(String username);
	
	@Query("select c from UpiPay c where c.upikeys=?1")
	UpiPay getDataUsingKeys(String key);
}
