package com.msscard.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import com.msscard.entity.ERCredentialsManager;

public interface ERCredentialManager extends CrudRepository<ERCredentialsManager, Long>, PagingAndSortingRepository<ERCredentialsManager, Long>,
JpaSpecificationExecutor<ERCredentialsManager>{

	@Query("select e from ERCredentialsManager e where e.mode=?1")
	ERCredentialsManager getCredentials(String mode);
}
