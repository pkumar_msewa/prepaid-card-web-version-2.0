package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.CorporateAgentDetails;
import com.msscard.entity.MService;
import com.msscard.entity.MUser;
import com.msscard.entity.PartnerDetails;
import com.msscard.model.Status;

public interface PartnerDetailsRepository extends CrudRepository<PartnerDetails, Long>, PagingAndSortingRepository<PartnerDetails, Long>, JpaSpecificationExecutor<PartnerDetails>{

	@Query("select p from PartnerDetails p where p.corporate=?1 and p.status=?2")
	List<PartnerDetails> getPartnerDetails(CorporateAgentDetails corporate, Status active);
	
	@Query("update PartnerDetails p set p.partnerServices=?1 where p.id=?2")
	void updatePartnerDetails(List<MService> services,long id);
	
	@Query("select p from PartnerDetails p where p.partnerUser=?1")
	PartnerDetails getPartnerDetails(MUser partnerUser);
	
	@Query("select p from PartnerDetails p where p.id=?1 and p.status=?2")
	PartnerDetails getPartnerById(long id, Status status);
	
}
