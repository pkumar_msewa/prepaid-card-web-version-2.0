package com.msscard.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.BulkCardAssignmentGroup;
import com.msscard.model.Status;


public interface BulkCardAssignmentGroupRepository extends CrudRepository<BulkCardAssignmentGroup,Long>,JpaSpecificationExecutor<BulkCardAssignmentGroup>,PagingAndSortingRepository<BulkCardAssignmentGroup,Long>{
	
	@Query("select g from BulkCardAssignmentGroup g where g.creationStatus=?1 and g.groupDetails.groupDetails.email=?2")
	Page<BulkCardAssignmentGroup> getBulkCardFailList(Pageable pageable, Status fail, String email);	
	
	@Query("select g from BulkCardAssignmentGroup g where DATE(g.created) BETWEEN ?1 and ?2 and g.creationStatus=?3 and g.groupDetails.groupDetails.email=?4")
	Page<BulkCardAssignmentGroup> getBulkCardFailListPost(Pageable pageable, Date from, Date to, Status fail, String email);	

	@Query("select b from BulkCardAssignmentGroup b where b.activationStatus='Inactive' and b.creationStatus='Success'")
	List<BulkCardAssignmentGroup> getBulkCardAssignmentWithStatusInactive();
}
