package com.msscard.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import com.msscard.entity.EKYCDetails;

public interface KYCDetailsRepository extends CrudRepository<EKYCDetails, Long>,
JpaSpecificationExecutor<EKYCDetails>{

	
}
