package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.msscard.entity.MerchantDetails;

public interface MerchantDetailsRepository extends CrudRepository<MerchantDetails, Long>, JpaSpecificationExecutor<MerchantDetails>{

	@Query("select k from MerchantDetails k")
	List<MerchantDetails> getAllMerchants();
	
	@Query("select m from MerchantDetails m where merchantName=?1")
	MerchantDetails findByMerchantName(String merchantName);
	
	
}
