package com.msscard.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.msscard.entity.FlightDetails;
import com.msscard.entity.MTransaction;

public interface FlightDetailsRepository extends CrudRepository<FlightDetails,Long>,JpaSpecificationExecutor<FlightDetails>{

	@Query("select f from FlightDetails f where f.transaction=?1 order by f.created")
	FlightDetails getByTransaction(MTransaction transaction);
	
	@Query("select f from FlightDetails f where f.transaction=?1 order by f.created desc")
	FlightDetails getByMerchantRefNo(String merchantRefNo);
}
