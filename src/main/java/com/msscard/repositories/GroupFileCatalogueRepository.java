package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.msscard.entity.GroupFileCatalogue;

public interface GroupFileCatalogueRepository extends CrudRepository<GroupFileCatalogue, Long>,JpaSpecificationExecutor<GroupFileCatalogue>{
	
	@Query("select c from GroupFileCatalogue c")
	List<GroupFileCatalogue> getUnapprovedList();
	
	@Query("select c from GroupFileCatalogue c where c.categoryType='BLKREGISTER' or c.categoryType='BLKCARDASSIGNGROUP' or c.categoryType='BLKKYCUPLOAD' order by c.id desc")
	List<GroupFileCatalogue> getUnapprovedListGroup();
	
	@Query("select c from GroupFileCatalogue c where c.categoryType='BLKSMS'")
	List<GroupFileCatalogue> getUnapprovedListSms();
	
	@Query("select c from GroupFileCatalogue c where c.reviewStatus=1 and c.fileRejectionStatus=0 and c.categoryType='BLKREGISTER' and c.schedulerStatus=0")
	List<GroupFileCatalogue> getListOfBulkRegister();
	
	@Query("select c from GroupFileCatalogue c where c.reviewStatus=1 and c.fileRejectionStatus=0 and c.categoryType='BLKSMS' and c.schedulerStatus=0")
	List<GroupFileCatalogue> getListOfBulkSms();
	
	@Query("select c from GroupFileCatalogue c where c.reviewStatus=1 and c.fileRejectionStatus=0 and c.categoryType='BLKCARDLOAD' and c.schedulerStatus=0")
	List<GroupFileCatalogue> getListOfBulkFundTransfer();
	
	@Query("select c from GroupFileCatalogue c where c.reviewStatus=1 and c.fileRejectionStatus=0 and c.categoryType='BLKCARDASSIGNGROUP' and c.schedulerStatus=0")
	List<GroupFileCatalogue> getBulkCardAssignmentGroup();
	
	@Query("select COUNT(c) from GroupFileCatalogue c where c.categoryType='BLKREGISTER' AND c.user.groupDetails.email=?1")
	long getBulkRegCount(String email);
	
	@Query("select COUNT(c) from GroupFileCatalogue c where c.categoryType='BLKSMS' AND c.user.groupDetails.email=?1")
	long getBulkSmsCount(String email);
	
	@Query("select COUNT(c) from GroupFileCatalogue c where c.categoryType='BLKCARDASSIGNGROUP' AND c.user.groupDetails.email=?1")
	long getBulkCardLoadCount(String email);
	
	@Query("select c from GroupFileCatalogue c where c.reviewStatus=1 and c.fileRejectionStatus=0 and c.categoryType='BLKKYCUPLOAD' and c.schedulerStatus=0")
	List<GroupFileCatalogue> getListOfBulkKyc();
}
