package com.msscard.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.msscard.entity.CorporateRoles;

public interface CorporateRolesRepository extends CrudRepository<CorporateRoles, Long>,JpaSpecificationExecutor<CorporateRoles>{

	@Query("select c from CorporateRoles where c.code=?1")
	CorporateRoles getByCode(String code);
}
