package com.msscard.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.msscard.entity.MLocationDetails;

public interface MLocationDetailRepository extends CrudRepository<MLocationDetails,Long>,JpaSpecificationExecutor<MLocationDetails> {

    @Query("SELECT l from MLocationDetails l where l.pinCode = ?1")
    MLocationDetails findLocationByPin(String pinCode);


}
