package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.msscard.entity.GroupSms;



public interface GroupSmsRepository extends CrudRepository<GroupSms, Long>, JpaSpecificationExecutor<GroupSms>{
	
	@Query("select c from GroupSms c where c.cronStatus=?1 and c.isSingle=?2 and c.status=?3")
	List<GroupSms> getAllSingleNumbers(boolean cron, boolean single, String status);
	
	@Query("select c from GroupSms c where c.cronStatus=?1 and c.isSingle=?2 and c.status=?3")
	List<GroupSms> getAllGroupNumbers(boolean cron, boolean single, String status);
}
