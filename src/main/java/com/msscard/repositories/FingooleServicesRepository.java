package com.msscard.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.msscard.entity.FingooleServices;

public interface FingooleServicesRepository extends CrudRepository<FingooleServices, Long>, JpaSpecificationExecutor<FingooleServices>{
	
	@Query("select c from FingooleServices c where c.code=?1")
	FingooleServices getServiceBasedOnCode(String code);
}
