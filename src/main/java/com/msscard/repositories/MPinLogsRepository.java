package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.msscard.entity.MUser;
import com.msscard.entity.MpinLogs;
import com.msscard.model.Status;


public interface MPinLogsRepository extends CrudRepository<MpinLogs, Long>, JpaSpecificationExecutor<MpinLogs>{

	@Query("select c from MpinLogs c where c.created > CURRENT_DATE and c.user=?1 and c.status=?2")
	List<MpinLogs> findTodayEntryForUserWithStatus(MUser user, Status status);
}
