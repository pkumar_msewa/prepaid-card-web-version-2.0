package com.msscard.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.imps.entity.ImpsP2ADetail;
import com.msscard.entity.MTransaction;
public interface ImpsP2ADetailRepository extends CrudRepository<ImpsP2ADetail, Long>, PagingAndSortingRepository<ImpsP2ADetail, Long>, JpaSpecificationExecutor<ImpsP2ADetail> {

	
	
	@Query("select i from ImpsP2ADetail i where i.transactionId=?1")
	ImpsP2ADetail findByTransaction(MTransaction senderTransaction);

}

