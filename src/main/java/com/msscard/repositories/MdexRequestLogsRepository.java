package com.msscard.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.MTransaction;
import com.msscard.entity.MdexRequestLogs;

public interface MdexRequestLogsRepository extends CrudRepository<MdexRequestLogs,Long>,JpaSpecificationExecutor<MdexRequestLogs>,PagingAndSortingRepository<MdexRequestLogs,Long>{

	@Query("select u from MdexRequestLogs u where u.transaction=?1")
	MdexRequestLogs findByTransaction(MTransaction transaction);
    
	

}
