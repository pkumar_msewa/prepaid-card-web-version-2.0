package com.msscard.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.MUser;
import com.msscard.entity.MUserAgent;

public interface MUserAgentRepository extends CrudRepository<MUserAgent,Long>, JpaSpecificationExecutor<MUserAgent> ,
	PagingAndSortingRepository<MUserAgent,Long> {
	
	@Query("select u from MUserAgent u where u.agent=?1 ORDER BY u.created DESC")
	List<MUserAgent> getAllUserBasedOnAgent(MUser agent);
	
	
	@Query("select count(u) from MUserAgent u where u.agent=?1")
	long getCountOfRegisterUserBasedOnAgent(MUser agent);
	
	@Query("select u from MUserAgent u where u.user=?1")
	MUserAgent getUserBasedOnUser(MUser user);
	
	/*Graph*/
	
	@Query("select count(u) from MUserAgent u inner join u.user ud where ud.userType='1' and ud.authority='ROLE_USER,ROLE_AUTHENTICATED' and ud.mobileStatus='Active'and u.created BETWEEN ?1 AND ?2 and u.agent=?3")
	long findUserCountByMonth(Date parse, Date parse2,MUser agent);
	
	@Query("select u from MUserAgent u where u.agent=?1 AND u.created BETWEEN ?2 AND ?3")
	List<MUserAgent> getUserBasedOnDate(MUser agent,Date startDate,Date endDate);

}
