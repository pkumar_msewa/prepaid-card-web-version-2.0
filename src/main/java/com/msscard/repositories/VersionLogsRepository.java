package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.ApiVersion;
import com.msscard.entity.VersionLogs;


public interface VersionLogsRepository extends CrudRepository<VersionLogs,Long>,JpaSpecificationExecutor<VersionLogs>,PagingAndSortingRepository<VersionLogs,Long>{

	@Query("select v from VersionLogs v where v.androidVersion=?1")
	VersionLogs getVersion(ApiVersion logs);
	
	@Query("select v from VersionLogs v where v.version=?1")
	VersionLogs getVersionByName(String version);

	@Query("select v from VersionLogs v order by v.created  desc")
	List<VersionLogs> findAllVersions();

	@Query("select v from VersionLogs v where v.id=?1")
	VersionLogs findById(long id);

	@Query("select v from VersionLogs v where v.isNewVersion=1 and v.status='Active' and v.androidVersion=?1")
	VersionLogs findPreviouLog(ApiVersion apiVersion);

	@Query("select v from VersionLogs v where v.version=?1 and v.androidVersion=?2")
	VersionLogs findLogByVersion(String version, ApiVersion apiVersion);
	
	@Query("select v from VersionLogs v where v.version=?1 and v.androidVersion=1")
	VersionLogs findLogByVersionAndroid(String version);
	
	@Query("select v from VersionLogs v where v.version=?1 and v.androidVersion=2")
	VersionLogs findLogByVersionIos(String version);
}
