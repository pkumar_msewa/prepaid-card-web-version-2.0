package com.msscard.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import com.msscard.entity.DonationAccount;

public interface DonationAccountRepository extends CrudRepository<DonationAccount, Long>, JpaSpecificationExecutor<DonationAccount> {
	
}
