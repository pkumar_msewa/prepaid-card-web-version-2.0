package com.msscard.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.msscard.entity.BusTicket;
import com.msscard.entity.MPQAccountDetails;
import com.msscard.entity.MTransaction;
import com.msscard.model.Status;

public interface BusTicketRepository extends CrudRepository<BusTicket, Long>, JpaSpecificationExecutor<BusTicket> {
	
	@Query("select b from BusTicket b where b.transaction=?1")
	BusTicket getByTransaction(MTransaction transaction);

	@Query("select b from BusTicket b order by b.created desc")
	List<BusTicket> getByStatus();

	@Query("select b from BusTicket b where b.status!=?1 and b.transaction.account=?2  order by b.created desc")
	List<BusTicket> getByStatusAndAccount(Status status, MPQAccountDetails accout);

	@Query("select b from BusTicket b where b.emtTxnId=?1 order by b.created desc")
	BusTicket getTicketByTxnId(String emtTxnId);

	@Query("select t from BusTicket t where t.created BETWEEN ?1 AND ?2 order by t.created desc")
	Page<BusTicket> getBusTicketByDate(Pageable pageable, Date from, Date to);

	@Query("select b from BusTicket b where b.tripId=?1")
	BusTicket getTicketBytripId(String tripId);

	@Query("select b from BusTicket b where b.seatHoldId=?1")
	BusTicket getTicketBySeatHoldId(String seatHoldId);

	/*@Query("select ud.firstName,ud.lastName,u.username,pq.transactionRefNo,pq.created,t.ticketPnr,t.operatorPnr,t.status,pq.status,pq.amount,pq.profit,t.emtTxnId from BusTicket t left join t.transaction.user u left join t.transaction pq inner join u.userDetail ud where t.created BETWEEN ?1 AND ?2 order by t.created desc")
	List<BusTicket> getBusTicketByDateNew(Date from, Date to);*/
	
	

}
