package com.msscard.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.MUser;
import com.msscard.entity.SendMoneyDetails;

public interface SendMoneyDetailsRepository extends CrudRepository<SendMoneyDetails, Long>, PagingAndSortingRepository<SendMoneyDetails, Long>, JpaSpecificationExecutor<SendMoneyDetails>{

	@Query("select s from SendMoneyDetails s where s.isChecked=0 and status='Pending'")
	List<SendMoneyDetails> getTransactionWithPendingStatus();
	
	@Query("select s from SendMoneyDetails s where s.transactionId=?1")
	SendMoneyDetails getByTransactionRefNo(String refNo);
	
	@Query("select s from SendMoneyDetails s where s.status='Pending' and s.isChecked=1")
	List<SendMoneyDetails> getTransactionWithFailedStatus();
	
	@Query("select s from SendMoneyDetails s order by created desc")
	List<SendMoneyDetails> getSendMoneyTransactions();
	
	@Query("select s from SendMoneyDetails s where s.user=?1 AND s.status='Pending'")
	List<SendMoneyDetails> getSendMoneyDetails(MUser user);
	
	@Query("SELECT sum(s.amount) FROM SendMoneyDetails s where s.status='Success' AND Date(s.created) BETWEEN ?1 AND ?2")
	Double getAllSendMoneyDetails(Date from ,Date to);
	
	
}
