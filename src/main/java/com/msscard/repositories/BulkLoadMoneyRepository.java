package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import com.msscard.entity.BulkLoadMoney;
import com.msscard.entity.MUser;
import com.msscard.entity.PartnerDetails;

public interface BulkLoadMoneyRepository extends CrudRepository<BulkLoadMoney,Long>,JpaSpecificationExecutor<BulkLoadMoney>,PagingAndSortingRepository<BulkLoadMoney,Long>{

	@Query("select b from BulkLoadMoney b where b.corporate=?1")
	List<BulkLoadMoney> getByCorporate(MUser corporate);
	
	@Query("select b from BulkLoadMoney b where b.corporate=?1 and b.transactionStatus=0")
	List<BulkLoadMoney> getLoadMoneyFailed(MUser corporate);
	
	@Query("select b from BulkLoadMoney b where b.corporate=?1 and b.transactionStatus=1")
	List<BulkLoadMoney> getLoadMoneySuccess(MUser corporate);

	@Query("select b from BulkLoadMoney b where b.partnerDetails=?1")
	List<BulkLoadMoney> getBulkLoadMoney(PartnerDetails partnerDetails);

}
