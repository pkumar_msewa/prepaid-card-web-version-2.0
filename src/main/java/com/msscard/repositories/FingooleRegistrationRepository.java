package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.msscard.entity.FingooleRegistration;
import com.msscard.entity.MUser;

public interface FingooleRegistrationRepository extends CrudRepository<FingooleRegistration, Long>, JpaSpecificationExecutor<FingooleRegistration> {
	
	@Query("select c from FingooleRegistration c where c.user=?1 and c.batchNumber=?2")
	List<FingooleRegistration> getRegistrationByUserAndBatch(MUser user, String batchNUmber);
	
	@Query("select c from FingooleRegistration c where c.mobileNumber=?1 and c.batchNumber=?2")
	List<FingooleRegistration> getUsersByMobileAndBatch(String mobile, String batch);
	
	@Query("select c from FingooleRegistration c where c.coino=?1 and c.batchNumber=?2")
	FingooleRegistration getUserByNameAndBatch(String name, String batch); 
	
	@Query("select c from FingooleRegistration c where c.name=?1 and c.batchNumber=?2")
	FingooleRegistration getUserByBatch(String name, String batch); 
	
	@Query("select c from FingooleRegistration c where c.user=?1 and c.batchNumber=?2 and c.coino!=''")
	List<FingooleRegistration> getListByUsers(MUser user, String batch);
	
	@Query("select c from FingooleRegistration c where c.user=?1 and c.coino!='' ORDER BY c.created DESC")
	List<FingooleRegistration> getListByUsersNoBatch(MUser user);
}
