package com.msscard.repositories;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.msscard.entity.FCMDetails;

public interface FCMDetailsRepository extends CrudRepository<FCMDetails, Long>,
JpaSpecificationExecutor<FCMDetails>{

	@Query("select f from FCMDetails f where f.cronStatus=0")
	List<FCMDetails> getFcmDetails();
}
