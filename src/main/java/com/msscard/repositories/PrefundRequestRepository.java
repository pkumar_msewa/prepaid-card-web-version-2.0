package com.msscard.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.PrefundRequest;

public interface PrefundRequestRepository extends CrudRepository<PrefundRequest, Long>, 
	PagingAndSortingRepository<PrefundRequest, Long>, JpaSpecificationExecutor<PrefundRequest> {
	
	@Query("select p from PrefundRequest p where p.senderReferenceNo=?1")
	PrefundRequest getPrefundRequestBasedOnReferenceNo(String referenceNo);

}
