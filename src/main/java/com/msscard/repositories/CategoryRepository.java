package com.msscard.repositories;


import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.msscard.entity.Category;

public interface CategoryRepository extends CrudRepository<Category, Long>, JpaSpecificationExecutor<Category>{

	@Query("SELECT c From Category c WHERE c.categoryName= ?1")
	Category findByName(String name);

}
