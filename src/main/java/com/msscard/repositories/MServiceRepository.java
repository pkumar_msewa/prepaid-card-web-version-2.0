package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.msscard.entity.MOperator;
import com.msscard.entity.MService;
import com.msscard.entity.MServiceType;

public interface MServiceRepository extends CrudRepository<MService, Long>, JpaSpecificationExecutor<MService> {

	@Query("select u from MService u where u.code=?1")
	MService findServiceByCode(String name);
	
	@Query("select u from MService u where u.operatorCode=?1")
	MService findServiceByOperatorCode(String name);

	@Query("select u from MService u where u.serviceType.id=?1")
	List<MService> findServiceByServiceTypeID(long id);

	@Query("select s from MService s where s.serviceType=?1")
	List<MService> findByServiceType(MServiceType serviceType);
	
	@Query("select s from MService s where s.serviceType=?1")
	MService findServiceType(MServiceType serviceType);
	
	@Query("select s from MService s where s.name=?1")
	MService findByName(String opCode);

	@Query("select u from MService u where u.serviceType=?1")
	List<MService> findServiceByType(MServiceType serviceType);

	@Query("select s from MService s")
	List<MService> findService();


	@Query("select u from MService u where u.code=?1 and u.serviceImage is not null")
	MService findServiceByCodeImage(String name);
	
	@Query("select u from MService u where u.operator=?1")
	List<MService> getServicesByOperator(MOperator operator);
	

	@Query("select u from MService u where u not IN(?1) and u.operator=?2")
	List<MService> getServicesExcluded(List<MService> ids,MOperator operator);
	
	

}