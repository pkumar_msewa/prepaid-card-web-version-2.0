package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.msscard.entity.FlightTicket;
import com.msscard.entity.FlightTravellers;

public interface FlightTravellerDetailsRepository extends CrudRepository<FlightTravellers,Long>,JpaSpecificationExecutor<FlightTravellers>{

	@Query("select t from FlightTravellers t where t.flightTicket=?1")
	List<FlightTravellers> getTravellersByTicket(FlightTicket flightTicket);
}
