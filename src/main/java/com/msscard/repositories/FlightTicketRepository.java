package com.msscard.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.msscard.entity.FlightAirLineList;
import com.msscard.entity.FlightTicket;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.model.Status;

public interface FlightTicketRepository extends CrudRepository<FlightTicket,Long>,JpaSpecificationExecutor<FlightTicket>{

	@Query("select f from FlightTicket f where f.transaction=?1")
	FlightTicket getByTransaction(MTransaction transaction);

	@Query("select t from FlightTicket t where t.flightStatus!=?1 and t.user=?2  order by created desc")
	List<FlightTicket> getByStatusAndUser(Status status,MUser user);
	
	@Query("select t from FlightTicket t order by created desc")
	List<FlightTicket> getAllTicket();
	
	@Query("select t from FlightTicket t where t.created BETWEEN ?1 AND ?2")
	List<FlightTicket> getAllTicketByDate(Date from,Date to);
	
	@Query("select f from FlightAirLineList f where f.cityCode=?1")
	FlightAirLineList getCityByCode(String code);
	
	@Query("select t from FlightTicket t order by created desc")
	List<FlightTicket> getAllAgentTicket();
}
