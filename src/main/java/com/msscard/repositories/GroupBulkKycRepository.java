package com.msscard.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.GroupBulkKyc;
import com.msscard.entity.GroupFileCatalogue;
import com.msscard.model.Status;

public interface GroupBulkKycRepository extends CrudRepository<GroupBulkKyc, Long>, PagingAndSortingRepository<GroupBulkKyc, Long>, JpaSpecificationExecutor<GroupBulkKyc> {

	@Query("select g from GroupBulkKyc g where g.mobile=?1 and g.groupFileCatalogue=?2 and g.successStatus=?3 and g.status=?4")
	GroupBulkKyc getUserRowByContact(String contact, GroupFileCatalogue groupFiletCatalogue, boolean status, Status state);

	@Query("select g from GroupBulkKyc g where g.successStatus=?1 and g.groupDetail.email=?2")
	Page<GroupBulkKyc> getBulkKycFailList(Pageable pageable, boolean fail, String email);	
	
	@Query("select g from GroupBulkKyc g where DATE(g.created) BETWEEN ?1 and ?2 and g.successStatus=?3 and g.groupDetail.email=?4")
	Page<GroupBulkKyc> getBulkKycFailListPost(Pageable pageable, Date from, Date to, boolean fail, String email);
	
	@Query("select g from GroupBulkKyc g where g.successStatus=?1 and g.mobile=?2 and g.groupDetail.email=?3")
	List<GroupBulkKyc> getsingleuserfail(boolean fail, String contact, String email);
}
