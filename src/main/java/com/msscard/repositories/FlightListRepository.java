package com.msscard.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.msscard.entity.FlightAirLineList;

public interface FlightListRepository extends CrudRepository<FlightAirLineList,Long>,JpaSpecificationExecutor<FlightAirLineList>{

	@Query("select f from FlightAirLineList f where f.cityCode=?1")
	FlightAirLineList getCityByCode(String code);
	
	@Query("select f.country from FlightAirLineList f where f.cityCode=?1")
	String getCountryByCode(String code);
}
