package com.msscard.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.BulkRegisterGroup;
import com.msscard.entity.MUser;

public interface BulkRegisterGroupRepository extends CrudRepository<BulkRegisterGroup, Long>, PagingAndSortingRepository<BulkRegisterGroup, Long>, JpaSpecificationExecutor<BulkRegisterGroup>{

	@Query("select b from BulkRegisterGroup b where b.user=?1")
	BulkRegisterGroup getByUser(MUser user);
		
}
