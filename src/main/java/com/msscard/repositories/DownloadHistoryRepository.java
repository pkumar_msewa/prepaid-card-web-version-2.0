package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.DownloadHistory;


public interface DownloadHistoryRepository extends CrudRepository<DownloadHistory, Long>,
PagingAndSortingRepository<DownloadHistory, Long>, JpaSpecificationExecutor<DownloadHistory> {
	
	@Query("SELECT dh.fileExtension FROM DownloadHistory dh")
	String getFileName();
	
	
	@Query("SELECT dh FROM DownloadHistory dh order by dh.created desc limit 5")
	List<DownloadHistory> getFileNameByOrder();
	
	@Query("SELECT count(dh) FROM DownloadHistory dh where status='Pending' order by dh.created desc limit 5")
	Long getPendingCount();
	

}
