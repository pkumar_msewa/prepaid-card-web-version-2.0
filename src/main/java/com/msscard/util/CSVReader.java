package com.msscard.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.csvreader.CsvReader;

public class CSVReader {

	public static final String DESCRIPTION_BULKREGISTER="File for Bulk Register";
	public static final String DESCRIPTION_BULKSMS = "File for Bulk SMS";
	public static final String DESCRIPTION_BULKCARDLOAD="File for Bulk Card Load";
	public static final String DESCRIPTION_BULKKYC="File for Bulk KYC Load";
	public static final String BULK_CARD_ASSIGN="File for Bulk Card Assign in group";

	
	
	public static JSONObject readCsvBulkUploadRegister(String absPath) throws Exception {
		JSONObject userList=new JSONObject();

			
			CsvReader users = new CsvReader(absPath);
			JSONArray jsonArray=new JSONArray();
			if(users.readHeaders()){
			while (users.readRecord())
			{
				System.err.println("m reading");
				String fname = users.get("firstName(Mandatory)");
				String lname=users.get("lastName(Mandatory)");
				String mobile = users.get("mobile");
				String email=users.get("email");
				String dob=users.get("dob(yyyy-MM-dd)");
				String idType=users.get("idType(aadhaar|drivers_id|voters_id|passport)");
				String idNumber=users.get("idNumber");
				String proxyNo=users.get("proxyNo");
				System.err.println(fname);
				if(!lname.isEmpty() && !fname.isEmpty()
						&&!mobile.isEmpty()&&!email.isEmpty()&&!proxyNo.isEmpty()){
				JSONObject jsonObject=new JSONObject();
					
				jsonObject.put("firstName", fname);
				jsonObject.put("lastName", lname);
				jsonObject.put("mobile", mobile);
				jsonObject.put("email",email);
				jsonObject.put("dob", dob);
				jsonObject.put("idType", idType);
				jsonObject.put("idNumber",idNumber);
				jsonObject.put("proxyNo", proxyNo);
				
				jsonArray.put(jsonObject);
				}	
				// perform program logic here
			}
			userList.put("UserList", jsonArray);
			}
			users.close();
		
		return userList;
		
	}
	
	public static JSONObject readCsvBulkUploadRegisterGroup(String absPath) throws Exception {
		JSONObject userList=new JSONObject();

			
			CsvReader users = new CsvReader(absPath);
			JSONArray jsonArray=new JSONArray();
			if(users.readHeaders()){
			while (users.readRecord())
			{
				System.err.println("m reading");
				String fname = users.get("firstName(Mandatory)");
				String mname = users.get("middleName(Optional)");
				String lname=users.get("lastName(Mandatory)");
				String mobile = users.get("mobile");
				String email=users.get("email");
				String dob=users.get("dob(yyyy-MM-dd)");
				String idType=users.get("idType(aadhaar|drivers_id|voters_id|passport)");
				String idNumber=users.get("idNumber");
				
				System.err.println(fname);
				if(!lname.isEmpty() && !fname.isEmpty()
						&&!mobile.isEmpty()&&!email.isEmpty()){
				JSONObject jsonObject=new JSONObject();
					
				jsonObject.put("firstName", fname);
				jsonObject.put("middleName", mname);
				jsonObject.put("lastName", lname);
				jsonObject.put("mobile", mobile);
				jsonObject.put("email",email);
				jsonObject.put("dob", dob);
				jsonObject.put("idType", idType);
				jsonObject.put("idNumber",idNumber);
				
				
				jsonArray.put(jsonObject);
				}	
				// perform program logic here
			}
			userList.put("UserList", jsonArray);
			}
			users.close();
		
		return userList;
		
	}
	
	public static JSONObject readCsvBulkUploadSms(String absPath) throws Exception{
		JSONObject userList=new JSONObject();
		
		CsvReader users = new CsvReader(absPath);
		JSONArray jsonArray=new JSONArray();
		if(users.readHeaders()){
			while (users.readRecord()) {
				System.err.println("m reading");
				String mobile = users.get("mobile");
				String message = users.get("message");
				if(!mobile.isEmpty()){
					JSONObject jsonObject=new JSONObject();
					jsonObject.put("mobile", mobile);
					jsonObject.put("message", message);
					jsonArray.put(jsonObject);
				}
			}
			userList.put("UserList", jsonArray);
		}
		users.close();		
		return userList;
	}
	
		public static JSONObject readCsvBulkUploadLoadBalance(String absPath){
			JSONObject userList=new JSONObject();

			try{
			CsvReader users = new CsvReader(absPath);
			JSONArray jsonArray=new JSONArray();
			if(users.readHeaders()){
			while (users.readRecord())
			{
				System.err.println("m reading");
				String amount = users.get("amount");
				String mobile=users.get("mobile");
				String email=users.get("email");
				String dateOfTransaction=users.get("dateOfTransaction");
				if(!amount.isEmpty() && !email.isEmpty()
						&&!mobile.isEmpty()&&!email.isEmpty()&&!mobile.isEmpty()&&!dateOfTransaction.isEmpty()){
				JSONObject jsonObject=new JSONObject();
					
				jsonObject.put("dateOfTransaction", dateOfTransaction);
				jsonObject.put("mobile", mobile);
				jsonObject.put("email",email);
				jsonObject.put("amount", amount);
				jsonArray.put(jsonObject);
				}	
				// perform program logic here
			}
			userList.put("LoadMoneyList", jsonArray);
			}
			users.close();
			}catch(Exception e){
				e.printStackTrace();
			}
		return userList;

		}
		
		
			public static void readCsvBulkUploadGenerateCard(String absPath){
				try {
					
					CsvReader cards = new CsvReader(absPath);
				
					if(cards.readHeaders()){
					
					
					while (cards.readRecord())
					{
						String first_name = cards.get("first_name");
						String last_name = cards.get("last_name");
						String mobile=cards.get("mobile");
						String name_on_card=cards.get("name_on_card");
						String dob=cards.get("dob");
						String email=cards.get("email");
						
						// perform program logic here
					}
					}
					cards.close();
					
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

		
	}
	
			
			public static JSONObject readCsvBulkKycUpload(String absPath) throws Exception{
				JSONObject userList=new JSONObject();

					
				
					CsvReader users = new CsvReader(absPath);
					JSONArray jsonArray=new JSONArray();
					if(users.readHeaders()){
					while (users.readRecord())
					{
						System.err.println("m reading");
						String mobile = users.get("mobile");
						String email=users.get("email");
						String address1 = users.get("address1");
						String address2=users.get("address2");
						String dob=users.get("dob(yyyy-MM-dd)");
						String pincode=users.get("pinCode");
						String documentUrl=users.get("documentUrl");
						String documentUrl1=users.get("documentUrl1");
						String idType=users.get("idType(aadhaar|pan|drivers_id|voters_id|passport)");
						String idNumber=users.get("idNumber");
						if(!mobile.isEmpty() && !email.isEmpty()
								&&!address1.isEmpty()&&!address2.isEmpty()&&!dob.isEmpty()&&!pincode.isEmpty()&&!documentUrl.isEmpty()&&
								!idType.isEmpty() && !idNumber.isEmpty() && !documentUrl1.isEmpty()){
						JSONObject jsonObject=new JSONObject();
							
						jsonObject.put("mobile", mobile);
						jsonObject.put("email",email);
						jsonObject.put("dob", dob);
						jsonObject.put("address1", address1);
						jsonObject.put("address2", address2);
						jsonObject.put("idType", idType);
						jsonObject.put("idNumber",idNumber);
						jsonObject.put("pinCode", pincode);
						jsonObject.put("documentUrl", documentUrl);
						jsonObject.put("documentUrl1",documentUrl1);
						jsonArray.put(jsonObject);
						}	
						// perform program logic here
					}
					userList.put("UserList", jsonArray);
					}
					users.close();
				
				return userList;
				
			}

			public static JSONObject readCsvBulkCardAssignmentGroup(String absPath) throws Exception {
				JSONObject userList=new JSONObject();

					
					CsvReader users = new CsvReader(absPath);
					JSONArray jsonArray=new JSONArray();
					if(users.readHeaders()){
					while (users.readRecord())
					{
						System.err.println("m reading");
						String mobile = users.get("mobile");
						String proxNo = users.get("proxyNumber");
						
						if(!mobile.isEmpty() && !proxNo.isEmpty()){
						JSONObject jsonObject=new JSONObject();
							
						jsonObject.put("mobile", mobile);
						jsonObject.put("proxyNumber",proxNo);
						
						
						jsonArray.put(jsonObject);
						}	
						// perform program logic here
					}
					userList.put("CardList", jsonArray);
					}
					users.close();
				
				return userList;
				
			}
			

	
	public static void main(String[] args) {

		String absPath="‪‪D:\\PayMe\\bulkregister.csv";
		try {
			System.err.println(readCsvBulkUploadRegister(absPath));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
	
	