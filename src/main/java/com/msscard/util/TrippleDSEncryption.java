package com.msscard.util;


import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class TrippleDSEncryption {
	private static final String STR_PAD_RIGHT = "RIGHT";
	private static final String STR_PAD_LEFT = "LEFT";
	
	// Test key
//	private static final  String key = "";
	
	//Live Key
	private static final  String key = "C873E69870B0C74C51CEB38332AE64D3";  // need to enter live ZPK Key
	public static void main(String[] args) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException {
		TrippleDSEncryption test = new TrippleDSEncryption();
		test.generatePinblock("1234", "183778891121");

	}
	
	// pin is pin no and pan is mobile no
	
	public static String get3desPinBlock(String pin, String pan) {
		TrippleDSEncryption test = new TrippleDSEncryption();
		return test.generatePinblock(pin, pan);
	}
	

	public String generatePinblock(String pin, String pan) {
		String pin_b = pin;
		int length = pin.length();

		if (length < 14) {
			pin_b = str_pad(pin, 14, "f", STR_PAD_RIGHT);
		}

		pin_b = "0" + String.valueOf(pin.length()) + pin_b;

		String pan_b = pan;
		int length1 = pan.length();

		if (length1 < 16) {
			pan_b = str_pad(pan_b, 16, "0", STR_PAD_LEFT);
		}

		// Xor
		String pinblock = xorHex(pin_b.toUpperCase(), pan_b.toUpperCase());
		System.out.println(pinblock);

		pinblock = pinblock.toUpperCase();

		// encryption _ base64 convert.

		String finalRes = pinBlockEncryption(pinblock);

		return finalRes;
	}

	public String xorHex(String a, String b) {
		char[] chars = new char[a.length()];
		for (int i = 0; i < chars.length; i++) {
			chars[i] = toHex(fromHex(a.charAt(i)) ^ fromHex(b.charAt(i)));
		}
		return new String(chars);
	}

	private static int fromHex(char c) {
		if (c >= '0' && c <= '9') {
			return c - '0';
		} else if (c >= 'A' && c <= 'F') {
			return c - 'A' + 10;
		} else if (c >= 'a' && c <= 'f') {
			return c - 'a' + 10;
		}
		throw new IllegalArgumentException();
	}

	private char toHex(int nybble) {
		if (nybble < 0 || nybble > 15) {
			throw new IllegalArgumentException();
		}
		return "0123456789ABCDEF".charAt(nybble);
	}

	private String pinBlockEncryption(String pinblock) {
		String res = "";
		try {
			byte[] keyBytes = getEncryptionKey(key, 168);
			byte[] pinBlock = getHexByteArray(pinblock);

			SecretKey secretKey = new SecretKeySpec(keyBytes, "tripledes");

			Cipher cipher = Cipher.getInstance("tripledes/ECB/nopadding");
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			byte[] encryptedPinBlock = cipher.doFinal(pinBlock);

			res = getHexString(encryptedPinBlock).toUpperCase();
			System.out.println(res);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	private String str_pad(String pin, int length, String character, String strPadRight) {
		String extraStr = "";
		String res = "";

		for (int i = 0; i < length - pin.length(); i++) {
			extraStr = extraStr + character;
		}

		if (STR_PAD_LEFT.equalsIgnoreCase(strPadRight)) {
			res = extraStr + pin;
		} else {
			res = pin + extraStr;
		}
		return res;
	}

	private static int[] getHexIntArray(String input) throws IllegalBlockSizeException {
		if (input.length() % 2 != 0) {
			throw new IllegalBlockSizeException("Invalid Hex String, Hex representation length is not a multiple of 2");
		}
		int[] resultHex = new int[input.length() / 2];
		for (int iCnt1 = 0; iCnt1 < input.length(); iCnt1++) {
			String byteString = input.substring(iCnt1, ++iCnt1 + 1);
			int hexOut = Integer.parseInt(byteString, 16);
			resultHex[iCnt1 / 2] = (hexOut & 0x000000ff);
		}
		return resultHex;
	}

	private static byte[] getEncryptionKey(String keyString, int keySize)
			throws IllegalBlockSizeException, InvalidKeyException {
		int keyLength = keyString.length();
		switch (keySize) {
		case 56:
			if (keyLength != 16)
				throw new InvalidKeyException(
						"Hex Key length should be 16 for a 56 Bit Encryption, found [" + keyLength + "]");
			break;
		case 112:
			if (keyLength != 32)
				throw new InvalidKeyException(
						"Hex Key length should be 32 for a 112 Bit Encryption, found[" + keyLength + "]");
			break;
		case 168:
			if (keyLength != 32 && keyLength != 48)
				throw new InvalidKeyException(
						"Hex Key length should be 32 or 48 for a 168 Bit Encryption, found[" + keyLength + "]");
			if (keyLength == 32) {
				keyString = keyString + keyString.substring(0, 16);
			}
			break;
		default:
			throw new InvalidKeyException("Invalid Key Size, expected one of [56, 112, 168], found[" + keySize + "]");
		}

		byte[] keyBytes = getHexByteArray(keyString);
		return keyBytes;

	}

	private static String getHexString(byte[] input) {
		StringBuilder strBuilder = new StringBuilder();
		for (byte hexByte : input) {
			int res = 0xFF & hexByte;
			String hexString = Integer.toHexString(res);
			if (hexString.length() == 1) {
				strBuilder.append(0);
			}
			strBuilder.append(hexString);

		}

		return strBuilder.toString();
	}

	private static byte[] getHexByteArray(String input) throws IllegalBlockSizeException {

		int[] resultHex = getHexIntArray(input);
		byte[] returnBytes = new byte[resultHex.length];
		for (int cnt = 0; cnt < resultHex.length; cnt++) {
			returnBytes[cnt] = (byte) resultHex[cnt];
		}
		return returnBytes;
	}
}