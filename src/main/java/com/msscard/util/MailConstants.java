package com.msscard.util;

import java.util.Properties;

public class MailConstants {
	
	public static final String CASHIERUPIURL = "https://mycashier.in/resources/Home/img/logo1.png/";
	
	public static final String Path = "https://cashiercards.in";
	//public static final String Path = "http://13.127.101.55/IplCards";

	
	//public static final String Path ="http://localhost:8030";
	public static final String CC_MAIL_LIVE = "info@msewa.com";
	public static final String CC_MAIL = "info@msewa.com";
	public static final String DEBUG = "false";
	public static final String SENDER_EMAIL_NO_REPLY = "care@madmoveglobal.com";
	public static final String USER_ID_NO_REPLY = "noreplycashiercards";
	//public static final String SENDER_EMAIL = "cashierpromotions@madmoveglobal.com";
	public static final String SENDER_EMAIL = "info@liveewire.com";
		
	//public static final String PASSWORD = "Msewa@321";
	public static final String PASSWORD = "LiveEwire@321";
	//public static final String USER_ID = "cashierpromotions@madmoveglobal.com";
	public static final String USER_ID = "info@liveewire.com";
	
	// FOR TRANSACTIONS MONTHLY AND LAST DATE CRON
	
	public static final String SENDER_EMAIL_CC = "cashierpromotions@madmoveglobal.com";
	public static final String PASSWORD_CC = "12345678";
	public static final String USER_ID_CC = "cashierpromotions@madmoveglobal.com";
	
	/*public static final String SENDER_EMAIL_CC = "kbisht@msewa.com";
	public static final String PASSWORD_CC = "";
	public static final String USER_ID_CC = "kbisht@msewa.com";*/

	public static final String SMTP_AUTH = "true";
	public static final boolean SMTP_AUTH_PLAIN_DISABLE = true;
	public static final String SMTP_HOST_BULK_EMAIL = "172.16.7.58";
	 public static final String SMTP_STARTTLS_ENABLE = "true";
	 public static final String SMTP_HOST = "smtp.gmail.com";;
	 public static final String SMTP_SSL_TRUST = "smtp.office365.com";
	 public static final String SMTP_PORT = "587";
	 
	 public static final String LIVE_URL = "https://liveewire.com";
	 public static final String MAIL_TEMPLATE = "com/msscard/mail/template/";

	public static Properties getEmailProperties() {
		Properties props = new Properties();
		props.put("mail.smtp.auth", MailConstants.SMTP_AUTH);
		props.put("mail.smtp.starttls.enable", MailConstants.SMTP_STARTTLS_ENABLE);
		props.put("mail.smtp.host", MailConstants.SMTP_HOST);
		props.put("mail.smtp.port", MailConstants.SMTP_PORT);
		props.put("mail.smtp.ssl.trust", MailConstants.SMTP_HOST);

		return props;
	}

	public static Properties getBulkEmailProperties() {
		Properties props = new Properties();
		props.put("mail.smtp.auth", MailConstants.SMTP_AUTH);
		props.put("mail.smtp.starttls.enable", MailConstants.SMTP_STARTTLS_ENABLE);
		props.put("mail.smtp.auth.plain.disable", MailConstants.SMTP_AUTH_PLAIN_DISABLE);
		props.put("mail.smtp.host", MailConstants.SMTP_HOST_BULK_EMAIL);
		props.put("mail.smtp.ssl.trust", MailConstants.SMTP_SSL_TRUST);
		props.put("mail.smtp.port", MailConstants.SMTP_PORT);
		props.put("mail.debug", MailConstants.DEBUG);
		return props;
	}

}
