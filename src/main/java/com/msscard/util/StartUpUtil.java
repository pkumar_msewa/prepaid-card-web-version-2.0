package com.msscard.util;

public class StartUpUtil {

	// For fgmtest server
	// public static final String CSV_FILE =
	// "/home/vibhanshu/Documents/PayQwikCentralSystem/PayQwikApp/WebContent/WEB-INF/startup/";
	//

	// public static final String CSV_FILE =
	// "C:\\Users\\vibha_000\\Documents\\PayQwikCentralSystem\\PayQwikApp\\WebContent\\WEB-INF\\startup\\";

	// local tomcat deployment
	// public static final String CSV_FILE =
	// "C:\\tomcat7\\webapps\\ROOT\\WEB-INF\\startup\\";

	// public static final String CSV_FILE =
	// "/usr/local/tomcat7/webapps/PaulPayApp/WEB-INF/startup/";

	// public static final String CSV_FILE =
	// "/usr/local/tomcat7/webapps/PayQwikApp/WebContent/WEB-INF/startup/";

	// public static final String CSV_FILE =
	// "/usr/local/tomcat7/webapps/PayQwikApp/WEB-INF/startup/";

	// LIVE EWIRE (ANkit)
	public static final String CSV_FILE = "/usr/local/tomcat8/webapps/ROOT/resources/register/";

	// TEST EWIRE
	// public static final String CSV_FILE =
	// "/usr/local/tomcat/webapps/Ewire-Rupay/resources/register/";

	//

	// local
	// public static final String CSV_FILE =
	// "/home/ankit/workspace/ewire_rupay/WebContent/resources/register/";
	// TEST
	// public static final String CSV_FILE =
	// "/usr/local/tomcat/webapps/CashierCards/resources/register/";
	// WebContent/resources/register/bulkregister.csv

	// public static final String CSV_FILE ="D:\\MSS
	// Cards\\madmovecards-web\\WebContent\\resources\\register\\";

	// public static final String CSV_FILE
	// ="E://madmovecards//madmovecards-web//WebContent//resources//register//";

	// public static final String CSV_FILE = " D:\\OFFICE PROJECTS\\office
	// project\\APP\\APP\\WebContent\\WEB-INF\\startup\\" ;

	public static final String CSV_FILE_LOADMONEY = "/usr/local/tomcat8/webapps/ROOT/WEB-INF/startup/ebsRefundMoney/";

	public static final String TRAVEL = "travel@msewa.com";
	public static final String KYC = "KYC";
	public static final String SUPER_KYC = "SUPERKYC";
	public static final String NON_KYC = "NONKYC";
	public static final String ADMIN = "admin@cashier.com";
	public static final String SETTLEMENT = "settlement@vpayqwik.com";
	public static final String COMMISSION = "commission@vpayqwik.com";
	public static final String BANK = "bank@vpayqwik.com";
	public static final String PROMO_CODE = "promoCode@gmail.com";
	public static final String PRDEICT_AND_WIN = "predictAndWin@msewa.com";
	public static final String REFUND = "refund@msewa.com";
	public static final String RISK = "risk@vpayqwik.com";
	public static final String VAT = "vat@msewa.com";
	public static final String CGST = "cgst@msewa.com";
	public static final String SGST = "sgst@msewa.com";
	public static final String KRISHI_KALYAN = "kkcess@msewa.com";
	public static final String SWACCH_BHARAT = "sbcess@msewa.com";
	public static final String MBANK = "mbank@vpayqwik.com";
	public static final String MERCHANT = "merchant@vpayqwik.com";
	public static final String MVISA = "mVisa@msewa.com";
	// mera Event
	public static final String MERA_EVENTS = "meraevents@msewa.com";
	public static final String MeraEventCode = "EVNT";
	public static final String MOBILE_BANK = "mbank@msewa.com";
	public static final String SUPER_ADMIN = "superadmin@vpayqwik.com";
	public static final String Giftcart = "giftcart@msewa.com";
	public static final String SAVAARI = "savaari@msewa.com";
	public static final String GiftcartCode = "GCART";
	public static final String NIKKI_CHAT = "nikkichat@gmail.com";
	public static final String SUPER_AGENT = "superagent@vpayqwik.com";
	public static final String PAYLO_MERCHANT = "paylomerchant_1@gmail.com";
	// for adlabs
	public static final String Adlabs = "adlabs@msewa.com";
	public static final String AdlabsCode = "ADLABS";
	public static final String IMAGICA = "imagica@vpayqwik.com";
	public static final String IMAGICA_SERVICE = "IMGCA";
	public static final String QWIKR_PAY = "qwikrpay@msewa.com";
	public static final String QWIK_PAY_CODE = "QWKP";

	// for YuppTv

	public static final String yuppTv = "yuppTv@msewa.com";
	public static final String yuppTvCode = "YUPTV";
	public static final String YUPPTV = "yuppTV@vpayqwik.com";
	public static final String YuppTv_SERVICE = "YUPPTV";

	public static final String STATE_GST = "stateservicetax@msewa.com";
	public static final String CENTRAL_GST = "centralservicetax@msewa.com";

	// for refer and earn

	public static final String REFER_EARN = "refernearn@msewa.com";
	// for flight booking

	public static final String FLIGHT_BOOKING = "easeMyTrip_flight@msewa.com";
	public static final String FLIGHT_CODE = "EMTF";

	// For Bus Booking
	public static final String BUS_BOOKING = "easeMyTripBus@msewa.com";
	public static final String BUS_SERVICECODE = "EMTB";

	// RedeemPoints
	public static final String REDEEM_POINT_CASH = "REDEEM";

	// Travelkhana
	public static final String TRAVELKHANA = "tavelkhana@msewa.com";
	public static final String TRAVELKAHANA_SERVICECODE = "TKHANA";

	// woohoo
	// woohoo
	public static final String WOOHOO = "woohoocards@paulpay.com";
	public static final String WOOHOO_SERVICECODE = "WOOHO";
	public static final String INSTANTPAY = "instantpay@tripay.in";

	// MMCards

	public static final String MMCARDS = "PMMC";
	public static final String MMCARD_USER = "matchmove@msspayments.com";

	// IMPS

	public static final String IMPS_SERVICECODE = "IMPS";
	public static final String IMPS = "imps@cashier.com";

	public static final String LMS_SERVICECODE = "LMS";

	public static final String UPI_SERVICECODE = "UPS";

}
