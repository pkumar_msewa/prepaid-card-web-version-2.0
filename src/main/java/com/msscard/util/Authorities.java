package com.msscard.util;

public interface Authorities {

	public static final String AUTHENTICATED = "ROLE_AUTHENTICATED";
	
	public static final String GROUP = "ROLE_GROUP";
	
	public static final String BLOCKED = "ROLE_BLOCKED";
	
	public static final String LOCKED = "ROLE_LOCKED";
	
	public static final String ADMINISTRATOR = "ROLE_ADMINISTRATOR";
	
	public static final String CORPORATE = "ROLE_CORPORATE";

	public static final String MERCHANT = "ROLE_MERCHANT";
	
	public static final String USER = "ROLE_USER";
	
	public static final String AGENT = "ROLE_AGENT";

	public static final String SUPER_AGENT = "ROLE_SUPER_AGENT";
	
	public static final String SUPER_ADMIN = "ROLE_SUPER_ADMIN";
	
	public static final String DONATEE = "ROLE_DONATEE";
	
	public static final String SPECIAL_USER = "ROLE_SPECIAL_USER";
	
	public static final String CORPORATE_PARTNER="ROLE_PARTNER";
	
}
