package com.msscard.util;
import java.util.*;

import org.apache.log4j.BasicConfigurator;
import org.json.JSONException;

import javapns.*;
import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;
import javapns.notification.PushNotificationPayload;
import javapns.notification.PushedNotification;
import javapns.notification.ResponsePacket;

public class TestNotiIos {

	    public static void main(String[] args) {
	        BasicConfigurator.configure();
	        try {
	            PushNotificationPayload payload = PushNotificationPayload.complex();
	            payload.addAlert("hello this is abhijit");
	            payload.addBadge(1);
	            payload.addSound("default");
	            payload.addCustomDictionary("imageUrl", "https://bit.ly/2NEL5cJ");
	            payload.addCustomDictionary("body", "https://itunes.apple.com/app/id1449471334");
	            System.out.println(payload.toString());
	            System.err.println(payload.getPayloadSize());
	            List < PushedNotification > NOTIFICATIONS = Push.payload(payload, "D:\\Project Matrix\\Ewire_rupay\\ewire_rupay\\WebContent\\resources\\extra\\ewireRuPayPushCert.p12", "",true,"6d022e1e3f8049298c850028c159914957303a00a2136f4aabf73526a5478ae4");
	            for (PushedNotification NOTIFICATION: NOTIFICATIONS) {
	                if (NOTIFICATION.isSuccessful()) {
	                    /* APPLE ACCEPTED THE NOTIFICATION AND SHOULD DELIVER IT */
	                    System.out.println("PUSH NOTIFICATION SENT SUCCESSFULLY TO: " +
	                        NOTIFICATION.getDevice().getToken());
	                    /* STILL NEED TO QUERY THE FEEDBACK SERVICE REGULARLY */
	                } else {
	                    String INVALIDTOKEN = NOTIFICATION.getDevice().getToken();
	                    /* ADD CODE HERE TO REMOVE INVALIDTOKEN FROM YOUR DATABASE */
	                    /* FIND OUT MORE ABOUT WHAT THE PROBLEM WAS */
	                    Exception THEPROBLEM = NOTIFICATION.getException();
	                    THEPROBLEM.printStackTrace();
	                    /* IF THE PROBLEM WAS AN ERROR-RESPONSE PACKET RETURNED BY APPLE, GET IT */
	                    ResponsePacket THEERRORRESPONSE = NOTIFICATION.getResponse();
	                    if (THEERRORRESPONSE != null) {
	                        System.out.println(THEERRORRESPONSE.getMessage());
	                    }
	                }
	            }
	        } catch (CommunicationException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        } catch (KeystoreException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        } catch (JSONException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        } catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    
	    }
}
