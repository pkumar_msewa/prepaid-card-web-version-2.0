package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name="otpmobile")
@Entity
public class OtpMobile extends AbstractEntity<Long>{
	
	private static final long serialVersionUID = 8453654076725018243L;

	@Column
	private String mobile;
	
	@Column
	private String name;

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
