package com.msscard.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Table(name = "groupdetails")
@Entity
public class GroupDetails extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;

	@Column
	private String groupName;

	@Column
	private String email;

	@Column
	private String contactNo;

	@Column
	private String status;

	@Column
	private String dname;

	@Column
	private String groupImage;

	@Column
	private boolean corporate = false;

	@Column
	private String address;

	@Column
	private String city;

	@Column
	private String country;

	@Column
	private String state;
	@Column
	private String image;

	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column
	private byte[] imageContent;

	@OneToMany
	private List<MService> service = new ArrayList<MService>();

	public boolean isCorporate() {
		return corporate;
	}

	public void setCorporate(boolean corporate) {
		this.corporate = corporate;
	}

	public List<MService> getService() {
		return service;
	}

	public void setService(List<MService> service) {
		this.service = service;
	}

	public String getDname() {
		return dname;
	}

	public void setDname(String dname) {
		this.dname = dname;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getGroupImage() {
		return groupImage;
	}

	public void setGroupImage(String groupImage) {
		this.groupImage = groupImage;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public byte[] getImageContent() {
		return imageContent;
	}

	public void setImageContent(byte[] imageContent) {
		this.imageContent = imageContent;
	}

}