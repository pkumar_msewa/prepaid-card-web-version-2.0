package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class FlightAirLineList extends AbstractEntity<Long>{

	private static final long serialVersionUID = 1L;
	
	@Column(unique=true)
	private String cityCode;
	@Column
	private String cityName;
	@Column
	private String airportName;
	@Column
	private String country;
	
	public String getCityCode() {
		return cityCode;
	}
	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getAirportName() {
		return airportName;
	}
	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	
}
