package com.msscard.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class BulkLoadMoney extends AbstractEntity<Long>{

	private String amount;
	private String mobile;
	private String email;
	private String dateOfTransaction;
	@ManyToOne
	private MUser corporate;
	private String transactionRefNo;
	private String cardLoadReferenceNo;
	private String corporateDebitTransactionRefNo;
	private boolean transactionStatus=false;
	private boolean schedulerStatus=false;
	private boolean validationError=false;
	private boolean walletLoadError=false;
	private boolean cardLoadError=false;
	private String validationErrorDesc;
	private String walletLoadErrorDesc;
	private String cardLoadErrorDesc;
	private String driver_id;
	@ManyToOne
	private PartnerDetails partnerDetails;
	
	
	
	
	
	
	
	
	
	public PartnerDetails getPartnerDetails() {
		return partnerDetails;
	}
	public void setPartnerDetails(PartnerDetails partnerDetails) {
		this.partnerDetails = partnerDetails;
	}
	public String getDriver_id() {
		return driver_id;
	}
	public void setDriver_id(String driver_id) {
		this.driver_id = driver_id;
	}
	public String getCorporateDebitTransactionRefNo() {
		return corporateDebitTransactionRefNo;
	}
	public void setCorporateDebitTransactionRefNo(String corporateDebitTransactionRefNo) {
		this.corporateDebitTransactionRefNo = corporateDebitTransactionRefNo;
	}
	public String getTransactionRefNo() {
		return transactionRefNo;
	}
	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}
	public String getCardLoadReferenceNo() {
		return cardLoadReferenceNo;
	}
	public void setCardLoadReferenceNo(String cardLoadReferenceNo) {
		this.cardLoadReferenceNo = cardLoadReferenceNo;
	}
	public String getValidationErrorDesc() {
		return validationErrorDesc;
	}
	public void setValidationErrorDesc(String validationErrorDesc) {
		this.validationErrorDesc = validationErrorDesc;
	}
	public String getWalletLoadErrorDesc() {
		return walletLoadErrorDesc;
	}
	public void setWalletLoadErrorDesc(String walletLoadErrorDesc) {
		this.walletLoadErrorDesc = walletLoadErrorDesc;
	}
	public String getCardLoadErrorDesc() {
		return cardLoadErrorDesc;
	}
	public void setCardLoadErrorDesc(String cardLoadErrorDesc) {
		this.cardLoadErrorDesc = cardLoadErrorDesc;
	}
	public boolean isValidationError() {
		return validationError;
	}
	public void setValidationError(boolean validationError) {
		this.validationError = validationError;
	}
	public boolean isWalletLoadError() {
		return walletLoadError;
	}
	public void setWalletLoadError(boolean walletLoadError) {
		this.walletLoadError = walletLoadError;
	}
	public boolean isCardLoadError() {
		return cardLoadError;
	}
	public void setCardLoadError(boolean cardLoadError) {
		this.cardLoadError = cardLoadError;
	}
	public boolean isTransactionStatus() {
		return transactionStatus;
	}
	public void setTransactionStatus(boolean transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
	public boolean isSchedulerStatus() {
		return schedulerStatus;
	}
	public void setSchedulerStatus(boolean schedulerStatus) {
		this.schedulerStatus = schedulerStatus;
	}
	public MUser getCorporate() {
		return corporate;
	}
	public void setCorporate(MUser corporate) {
		this.corporate = corporate;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDateOfTransaction() {
		return dateOfTransaction;
	}
	public void setDateOfTransaction(String dateOfTransaction) {
		this.dateOfTransaction = dateOfTransaction;
	}
	
	
}
