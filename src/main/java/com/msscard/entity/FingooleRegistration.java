package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Table(name="fingooleregistration")
@Entity
public class FingooleRegistration extends AbstractEntity<Long>{
	
	private static final long serialVersionUID = 8453654076725018243L;
	
	@Column
	private String name;
	
	@Column
	private String mobileNumber;
	
	@Column
	private String noOfDays;
	
	@Column
	private String batchNumber;
	
	@Column
	private String serviceProvider;
	
	@Column
	private String amount;
	
	@Column
	private String coino;
	
	@Column
	private String coiurl;
	
	@Column
	private String transactionRefNo;
	
	@OneToOne(fetch = FetchType.EAGER)
	private MUser user;
		
	public String getTransactionRefNo() {
		return transactionRefNo;
	}
	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getNoOfDays() {
		return noOfDays;
	}
	public void setNoOfDays(String noOfDays) {
		this.noOfDays = noOfDays;
	}
	public String getBatchNumber() {
		return batchNumber;
	}
	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}
	public String getServiceProvider() {
		return serviceProvider;
	}
	public void setServiceProvider(String serviceProvider) {
		this.serviceProvider = serviceProvider;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}	
	public String getCoino() {
		return coino;
	}
	public void setCoino(String coino) {
		this.coino = coino;
	}
	public String getCoiurl() {
		return coiurl;
	}
	public void setCoiurl(String coiurl) {
		this.coiurl = coiurl;
	}
	public MUser getUser() {
		return user;
	}
	public void setUser(MUser user) {
		this.user = user;
	}
	
	
}
