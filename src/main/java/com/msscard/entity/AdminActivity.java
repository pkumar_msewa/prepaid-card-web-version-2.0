package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name="adminactivity")
@Entity
public class AdminActivity extends AbstractEntity<Long>{

	private static final long serialVersionUID = 8453654076725018243L;
	
	@Column
	private String activityName;

	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}	
	
}
