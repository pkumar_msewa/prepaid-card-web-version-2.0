package com.msscard.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.msscard.model.Status;

@Entity
public class FlightDetails extends AbstractEntity<Long>{

private static final long serialVersionUID = 1L;

	
	private String ticketNumber;
	private String firstName;
	private String bookingRefId;
	private String transactionRefNomdex;
	private double paymentAmount;
	@Lob
	private String ticketDetails;
	@Enumerated(EnumType.STRING)
	private Status flightStatus;
	@Enumerated(EnumType.STRING)
	private Status paymentstatus;
	private String paymentmethod;
	@OneToOne(fetch = FetchType.EAGER)
	private MTransaction transaction;
	@ManyToOne(fetch = FetchType.EAGER)
	private MUser user;
	public String getTicketNumber() {
		return ticketNumber;
	}
	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getBookingRefId() {
		return bookingRefId;
	}
	public void setBookingRefId(String bookingRefId) {
		this.bookingRefId = bookingRefId;
	}
	public String getTransactionRefNomdex() {
		return transactionRefNomdex;
	}
	public void setTransactionRefNomdex(String transactionRefNomdex) {
		this.transactionRefNomdex = transactionRefNomdex;
	}
	public double getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	public String getTicketDetails() {
		return ticketDetails;
	}
	public void setTicketDetails(String ticketDetails) {
		this.ticketDetails = ticketDetails;
	}
	public Status getFlightStatus() {
		return flightStatus;
	}
	public void setFlightStatus(Status flightStatus) {
		this.flightStatus = flightStatus;
	}
	public Status getPaymentstatus() {
		return paymentstatus;
	}
	public void setPaymentstatus(Status paymentstatus) {
		this.paymentstatus = paymentstatus;
	}
	public String getPaymentmethod() {
		return paymentmethod;
	}
	public void setPaymentmethod(String paymentmethod) {
		this.paymentmethod = paymentmethod;
	}
	public MTransaction getTransaction() {
		return transaction;
	}
	public void setTransaction(MTransaction transaction) {
		this.transaction = transaction;
	}
	public MUser getUser() {
		return user;
	}
	public void setUser(MUser user) {
		this.user = user;
	}
	
	
}
