package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name="groupsms")
@Entity
public class GroupSms extends AbstractEntity<Long>{
	
	private static final long serialVersionUID = 1L;
	
	@Column
	private String mobile;
	
	@Column
	private String text;
	
	@Column
	private boolean cronStatus;
	
	@Column
	private String groupName;
	
	@Column
	private boolean isSingle;
	
	@Column
	private String status;
	
	

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public boolean isSingle() {
		return isSingle;
	}

	public void setSingle(boolean isSingle) {
		this.isSingle = isSingle;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isCronStatus() {
		return cronStatus;
	}

	public void setCronStatus(boolean cronStatus) {
		this.cronStatus = cronStatus;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
}
