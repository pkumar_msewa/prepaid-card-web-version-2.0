/**
 * Corporate Module
 */
package com.msscard.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * @author Suchita
 * Stores all the card proxy nos and corporate assigned to it
 *
 */
@Entity
public class CardProccessorInventory extends AbstractEntity<Long>{

	private String cardNo;
	
	private String proxyNumber;
	
	@ManyToOne
	private MUser corporateAgent_id;

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getProxyNumber() {
		return proxyNumber;
	}

	public void setProxyNumber(String proxyNumber) {
		this.proxyNumber = proxyNumber;
	}

	public MUser getCorporateAgent_id() {
		return corporateAgent_id;
	}

	public void setCorporateAgent_id(MUser corporateAgent_id) {
		this.corporateAgent_id = corporateAgent_id;
	}
	
	
	
}
