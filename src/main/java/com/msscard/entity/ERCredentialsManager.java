package com.msscard.entity;

import javax.persistence.Entity;

@Entity
public class ERCredentialsManager extends AbstractEntity<Long> {
	private String mdex_base_url;
	private String mdex_keys;
	private String mdex_token;
	private String mode;
	private String basic_auth_header;
	
	public String getBasic_auth_header() {
		return basic_auth_header;
	}
	public void setBasic_auth_header(String basic_auth_header) {
		this.basic_auth_header = basic_auth_header;
	}
	public String getMdex_base_url() {
		return mdex_base_url;
	}
	public void setMdex_base_url(String mdex_base_url) {
		this.mdex_base_url = mdex_base_url;
	}
	public String getMdex_keys() {
		return mdex_keys;
	}
	public void setMdex_keys(String mdex_keys) {
		this.mdex_keys = mdex_keys;
	}
	public String getMdex_token() {
		return mdex_token;
	}
	public void setMdex_token(String mdex_token) {
		this.mdex_token = mdex_token;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	
	
	
	
}
