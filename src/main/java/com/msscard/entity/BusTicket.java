package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.msscard.model.Status;

@Table(name = "busticket")
@Entity
public class BusTicket extends AbstractEntity<Long> {
	
	private static final long serialVersionUID = 1L;
	
	@Column
	private String userMobile;
	@Column
	private String userEmail;
	@Column
	private String ticketPnr;
	@Column
	private String operatorPnr;
	@Column
	private String emtTxnId;
	@Column
	private String emtTransactionScreenId;
	@Column
	private String busId;
	@Column
	private double totalFare;

	@Column
	private String priceRecheckTotalFare;

	@Column
	@Enumerated(EnumType.STRING)
	private Status status;

	@Column
	private String journeyDate;

	@Column(nullable = false)
	private String source;

	@Column(nullable = false)
	private String destination;

	@Column
	private String boardingId;

	@Column
	private String boardingAddress;

	@Column
	private String busOperator;

	@Column
	private String arrTime;

	@Column
	private String deptTime;

	@Column
	private String boardTime;

	@Column
	private String seatHoldId;

	@Column
	private String tripId;

	@OneToOne
	@JsonIgnore
	private MTransaction transaction;

	public String getUserMobile() {
		return userMobile;
	}

	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getTicketPnr() {
		return ticketPnr;
	}

	public void setTicketPnr(String ticketPnr) {
		this.ticketPnr = ticketPnr;
	}

	public String getOperatorPnr() {
		return operatorPnr;
	}

	public void setOperatorPnr(String operatorPnr) {
		this.operatorPnr = operatorPnr;
	}

	public String getEmtTxnId() {
		return emtTxnId;
	}

	public void setEmtTxnId(String emtTxnId) {
		this.emtTxnId = emtTxnId;
	}

	public String getEmtTransactionScreenId() {
		return emtTransactionScreenId;
	}

	public void setEmtTransactionScreenId(String emtTransactionScreenId) {
		this.emtTransactionScreenId = emtTransactionScreenId;
	}

	public String getBusId() {
		return busId;
	}

	public void setBusId(String busId) {
		this.busId = busId;
	}

	public double getTotalFare() {
		return totalFare;
	}

	public void setTotalFare(double totalFare) {
		this.totalFare = totalFare;
	}

	public String getPriceRecheckTotalFare() {
		return priceRecheckTotalFare;
	}

	public void setPriceRecheckTotalFare(String priceRecheckTotalFare) {
		this.priceRecheckTotalFare = priceRecheckTotalFare;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getJourneyDate() {
		return journeyDate;
	}

	public void setJourneyDate(String journeyDate) {
		this.journeyDate = journeyDate;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getBoardingId() {
		return boardingId;
	}

	public void setBoardingId(String boardingId) {
		this.boardingId = boardingId;
	}

	public String getBoardingAddress() {
		return boardingAddress;
	}

	public void setBoardingAddress(String boardingAddress) {
		this.boardingAddress = boardingAddress;
	}

	public String getBusOperator() {
		return busOperator;
	}

	public void setBusOperator(String busOperator) {
		this.busOperator = busOperator;
	}

	public String getArrTime() {
		return arrTime;
	}

	public void setArrTime(String arrTime) {
		this.arrTime = arrTime;
	}

	public String getDeptTime() {
		return deptTime;
	}

	public void setDeptTime(String deptTime) {
		this.deptTime = deptTime;
	}

	public String getBoardTime() {
		return boardTime;
	}

	public void setBoardTime(String boardTime) {
		this.boardTime = boardTime;
	}

	public String getSeatHoldId() {
		return seatHoldId;
	}

	public void setSeatHoldId(String seatHoldId) {
		this.seatHoldId = seatHoldId;
	}

	public String getTripId() {
		return tripId;
	}

	public void setTripId(String tripId) {
		this.tripId = tripId;
	}

	public MTransaction getTransaction() {
		return transaction;
	}

	public void setTransaction(MTransaction transaction) {
		this.transaction = transaction;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	

	
}
