package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Table(name="adminaccessidentifier")
@Entity
public class AdminAccessIdentifier extends AbstractEntity<Long>{

	private static final long serialVersionUID = 8453654076725018243L;
	
	@Column
	private boolean addUser;
	
	@Column
	private boolean loadCard;
	
	@Column
	private boolean addCorporate;
	
	@Column
	private boolean addMerchant;
	
	@Column
	private boolean addAgent;
	
	@Column
	private boolean addDonation;
	
	@Column
	private boolean addGroup;
	
	@Column
	private boolean sendNotification;
	
	@Column
	private boolean assignPhysicalCard;
	
	@Column
	private boolean assignVirtualCard;
	
	@Column
	private boolean androidVersion;
	
	@Column
	private boolean promoService;
	
	@Column
	private boolean cardBlock;
	
	@Column
	private boolean myUnblock;
	
	@OneToOne(fetch = FetchType.EAGER)
	private MUser user;
	
	public boolean isMyUnblock() {
		return myUnblock;
	}

	public void setMyUnblock(boolean myUnblock) {
		this.myUnblock = myUnblock;
	}

	public boolean isCardBlock() {
		return cardBlock;
	}

	public void setCardBlock(boolean cardBlock) {
		this.cardBlock = cardBlock;
	}

	public boolean isPromoService() {
		return promoService;
	}

	public void setPromoService(boolean promoService) {
		this.promoService = promoService;
	}

	public MUser getUser() {
		return user;
	}

	public void setUser(MUser user) {
		this.user = user;
	}

	public boolean isAddUser() {
		return addUser;
	}

	public void setAddUser(boolean addUser) {
		this.addUser = addUser;
	}

	public boolean isLoadCard() {
		return loadCard;
	}

	public void setLoadCard(boolean loadCard) {
		this.loadCard = loadCard;
	}

	public boolean isAddCorporate() {
		return addCorporate;
	}

	public void setAddCorporate(boolean addCorporate) {
		this.addCorporate = addCorporate;
	}

	public boolean isAddMerchant() {
		return addMerchant;
	}

	public void setAddMerchant(boolean addMerchant) {
		this.addMerchant = addMerchant;
	}

	public boolean isAddAgent() {
		return addAgent;
	}

	public void setAddAgent(boolean addAgent) {
		this.addAgent = addAgent;
	}

	public boolean isAddDonation() {
		return addDonation;
	}

	public void setAddDonation(boolean addDonation) {
		this.addDonation = addDonation;
	}

	public boolean isAddGroup() {
		return addGroup;
	}

	public void setAddGroup(boolean addGroup) {
		this.addGroup = addGroup;
	}

	public boolean isSendNotification() {
		return sendNotification;
	}

	public void setSendNotification(boolean sendNotification) {
		this.sendNotification = sendNotification;
	}

	public boolean isAssignPhysicalCard() {
		return assignPhysicalCard;
	}

	public void setAssignPhysicalCard(boolean assignPhysicalCard) {
		this.assignPhysicalCard = assignPhysicalCard;
	}

	public boolean isAssignVirtualCard() {
		return assignVirtualCard;
	}

	public void setAssignVirtualCard(boolean assignVirtualCard) {
		this.assignVirtualCard = assignVirtualCard;
	}

	public boolean isAndroidVersion() {
		return androidVersion;
	}

	public void setAndroidVersion(boolean androidVersion) {
		this.androidVersion = androidVersion;
	}
	
	
	
}
