package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Table(name = "busresponseofmdex")
@Entity
public class BusResponseOfMdex extends AbstractEntity<Long> {
	
	private static final long serialVersionUID = 7184799860853214548L;

	@Lob
	@Column
	private String bookingResp;

	@Lob
	@Column
	private String getTxnIdResp;

	@Column
	private String emtTxnId;

	@Column
	private String seatHoldId;

	@OneToOne
	private MTransaction mTransaction;

	public String getBookingResp() {
		return bookingResp;
	}

	public void setBookingResp(String bookingResp) {
		this.bookingResp = bookingResp;
	}

	public String getGetTxnIdResp() {
		return getTxnIdResp;
	}

	public void setGetTxnIdResp(String getTxnIdResp) {
		this.getTxnIdResp = getTxnIdResp;
	}

	public String getEmtTxnId() {
		return emtTxnId;
	}

	public void setEmtTxnId(String emtTxnId) {
		this.emtTxnId = emtTxnId;
	}

	public String getSeatHoldId() {
		return seatHoldId;
	}

	public void setSeatHoldId(String seatHoldId) {
		this.seatHoldId = seatHoldId;
	}

	public MTransaction getmTransaction() {
		return mTransaction;
	}

	public void setmTransaction(MTransaction mTransaction) {
		this.mTransaction = mTransaction;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
