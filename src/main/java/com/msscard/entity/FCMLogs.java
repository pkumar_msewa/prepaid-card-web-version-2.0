package com.msscard.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
@Entity
public class FCMLogs extends AbstractEntity<Long>{
	
	@ManyToOne
	private FCMDetails fcmDetail;
	@ManyToOne
	private MUser user;
	public FCMDetails getFcmDetail() {
		return fcmDetail;
	}
	public void setFcmDetail(FCMDetails fcmDetail) {
		this.fcmDetail = fcmDetail;
	}
	public MUser getUser() {
		return user;
	}
	public void setUser(MUser user) {
		this.user = user;
	}
	
	
	
	
}
