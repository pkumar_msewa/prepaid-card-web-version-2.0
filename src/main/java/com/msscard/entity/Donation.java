package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name="donation")
@Entity
public class Donation extends AbstractEntity<Long>{

private static final long serialVersionUID = 1L;
	
	@Column
	private String donateeName;
	
	@Column
	private String organization;
	
	@Column
	private String contactNo;
	
	@Column
	private String message;
	
	@Column
	private String email;
	
	@Column
	private String account;
	
	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getDonateeName() {
		return donateeName;
	}

	public void setDonateeName(String donateeName) {
		this.donateeName = donateeName;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
