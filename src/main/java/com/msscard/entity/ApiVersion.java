package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity(name="ApiVersion")
public class ApiVersion extends AbstractEntity<Long>{

	
	private static final long serialVersionUID = 1L;
	
	@Column(nullable = true)
	private String name;
	@Column(nullable = true)
	private String apiKey;
	@Column(nullable = true)
	private String apiStatus;
	@Column(nullable = true)
	private String apiVersion;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getApiKey() {
		return apiKey;
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	public String getApiStatus() {
		return apiStatus;
	}
	public void setApiStatus(String apiStatus) {
		this.apiStatus = apiStatus;
	}
	public String getApiVersion() {
		return apiVersion;
	}
	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}
	
	
	
	
}
