package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class FlightTravellers extends AbstractEntity<Long>{

	private static final long serialVersionUID=1L;

	@Column
	private String fName;
	@Column
	private String lName;
	@Column
	private String age;
	@Column
	private String gender;
	@Column
	private String type;
	@Column
	private String fare;
	@Column
	private String ticketNo;

	@ManyToOne
	private FlightTicket flightTicket;

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFare() {
		return fare;
	}

	public void setFare(String fare) {
		this.fare = fare;
	}

	public String getTicketNo() {
		return ticketNo;
	}

	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}

	public FlightTicket getFlightTicket() {
		return flightTicket;
	}

	public void setFlightTicket(FlightTicket flightTicket) {
		this.flightTicket = flightTicket;
	}
	
	
}
