package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.msscard.model.Status;

@Entity
public class BulkCardAssignmentGroup extends AbstractEntity<Long> {

	@Column(nullable=false,unique=true)
	private String proxyNumber;
	@Column(nullable=false)
	private String mobileNumber;
	@Column(nullable=true)
	private String remarks;
	@ManyToOne(optional=false)
	private MUser groupDetails;

	@Column(nullable=true)
	@Enumerated(EnumType.STRING)
	private Status creationStatus;
	@Column(nullable=true)
	@Enumerated(EnumType.STRING)
	private Status activationStatus;
	@Column(nullable=true)
	private String fileName;
		
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getProxyNumber() {
		return proxyNumber;
	}
	public void setProxyNumber(String proxyNumber) {
		this.proxyNumber = proxyNumber;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public MUser getGroupDetails() {
		return groupDetails;
	}
	public void setGroupDetails(MUser groupDetails) {
		this.groupDetails = groupDetails;
	}
	public Status getCreationStatus() {
		return creationStatus;
	}
	public void setCreationStatus(Status creationStatus) {
		this.creationStatus = creationStatus;
	}
	public Status getActivationStatus() {
		return activationStatus;
	}
	public void setActivationStatus(Status activationStatus) {
		this.activationStatus = activationStatus;
	}
		
	
}
