package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class BulkRegister extends AbstractEntity<Long>{

	@Column
	private String name;
	@Column(unique=true,nullable=false)
	private String mobile;
	@Column(unique=true,nullable=false)
	private String email;
	private String kycStatus;
	private String dob;
	private String proxyNo;
	private String cardNo;
	private boolean userCreationStatus=false;
	private boolean walletCreationStatus=false;
	private boolean cardCreationStatus=false;
	private boolean physicalCardCreationStatus=false;
	private boolean phyCardActivationStatus=false;
	private String userCreationError;
	private String walletCreationError;
	private String cardCreationError;
	private String phyCardCreationStatus;
	private String cardActivationStatus;
	private String driver_id;
	
	
	public String getDriver_id() {
		return driver_id;
	}
	public void setDriver_id(String driver_id) {
		this.driver_id = driver_id;
	}
	@OneToOne(fetch=FetchType.EAGER)
	private MUser user;
	@ManyToOne
	private CorporateAgentDetails agentDetails;
	
	@ManyToOne
	private PartnerDetails partnerDetails;
	
	public PartnerDetails getPartnerDetails() {
		return partnerDetails;
	}
	public void setPartnerDetails(PartnerDetails partnerDetails) {
		this.partnerDetails = partnerDetails;
	}
	public boolean isPhysicalCardCreationStatus() {
		return physicalCardCreationStatus;
	}
	public void setPhysicalCardCreationStatus(boolean physicalCardCreationStatus) {
		this.physicalCardCreationStatus = physicalCardCreationStatus;
	}
	public boolean isPhyCardActivationStatus() {
		return phyCardActivationStatus;
	}
	public void setPhyCardActivationStatus(boolean phyCardActivationStatus) {
		this.phyCardActivationStatus = phyCardActivationStatus;
	}
	public String getPhyCardCreationStatus() {
		return phyCardCreationStatus;
	}
	public void setPhyCardCreationStatus(String phyCardCreationStatus) {
		this.phyCardCreationStatus = phyCardCreationStatus;
	}
	public String getCardActivationStatus() {
		return cardActivationStatus;
	}
	public void setCardActivationStatus(String cardActivationStatus) {
		this.cardActivationStatus = cardActivationStatus;
	}
	public MUser getUser() {
		return user;
	}
	public void setUser(MUser user) {
		this.user = user;
	}
	public CorporateAgentDetails getAgentDetails() {
		return agentDetails;
	}
	public void setAgentDetails(CorporateAgentDetails agentDetails) {
		this.agentDetails = agentDetails;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getKycStatus() {
		return kycStatus;
	}
	public void setKycStatus(String kycStatus) {
		this.kycStatus = kycStatus;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getProxyNo() {
		return proxyNo;
	}
	public void setProxyNo(String proxyNo) {
		this.proxyNo = proxyNo;
	}
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	public boolean isUserCreationStatus() {
		return userCreationStatus;
	}
	public void setUserCreationStatus(boolean userCreationStatus) {
		this.userCreationStatus = userCreationStatus;
	}
	public boolean isWalletCreationStatus() {
		return walletCreationStatus;
	}
	public void setWalletCreationStatus(boolean walletCreationStatus) {
		this.walletCreationStatus = walletCreationStatus;
	}
	public boolean isCardCreationStatus() {
		return cardCreationStatus;
	}
	public void setCardCreationStatus(boolean cardCreationStatus) {
		this.cardCreationStatus = cardCreationStatus;
	}
	public String getUserCreationError() {
		return userCreationError;
	}
	public void setUserCreationError(String userCreationError) {
		this.userCreationError = userCreationError;
	}
	public String getWalletCreationError() {
		return walletCreationError;
	}
	public void setWalletCreationError(String walletCreationError) {
		this.walletCreationError = walletCreationError;
	}
	public String getCardCreationError() {
		return cardCreationError;
	}
	public void setCardCreationError(String cardCreationError) {
		this.cardCreationError = cardCreationError;
	}
	
	
	
}
