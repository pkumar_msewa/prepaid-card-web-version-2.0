package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name="upipay")
@Entity
public class UpiPay extends AbstractEntity<Long>{

	private static final long serialVersionUID = 8453654076725018243L;
	
	@Column
	private String username;
	
	@Column	
	private String password;
	
	@Column
	private String upikeys;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUpikeys() {
		return upikeys;
	}

	public void setUpikeys(String upikeys) {
		this.upikeys = upikeys;
	}

	
	
}
