package com.msscard.controller.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.msscard.app.api.IMCardApi;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.request.VerifyMobileDTO;
import com.msscard.app.model.response.MMCardFetchResponse;
import com.msscard.app.model.response.RegisterResponseDTO;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.entity.MUser;
import com.msscard.model.RegisterDTO;
import com.msscard.model.ResponseStatus;
import com.msscard.model.UserType;
import com.msscard.model.error.RegisterError;
import com.msscard.repositories.MMCardRepository;
import com.msscard.util.SecurityUtil;
import com.msscard.validation.MobileOTPValidation;
import com.msscard.validation.RegisterValidation;

@Controller
@RequestMapping("/User")
public class RegistrationController {

	/*private RegisterValidation registerValidation;
	private IUserApi userApi;
	private MobileOTPValidation mobileOTPValidation;
	private MMCardRepository mMCardRepository;
	private IMCardApi iMCardApi;
	private final IMatchMoveApi matchMoveApi;
	public RegistrationController(RegisterValidation registerValidation, IUserApi userApi,
			MobileOTPValidation mobileOTPValidation, MMCardRepository mMCardRepository, IMCardApi iMCardApi,IMatchMoveApi matchMoveApi) {
		super();
		this.registerValidation = registerValidation;
		this.userApi = userApi;
		this.mobileOTPValidation = mobileOTPValidation;
		this.mMCardRepository = mMCardRepository;
		this.iMCardApi = iMCardApi;
		this.matchMoveApi=matchMoveApi;
	}*/

	
	/*@RequestMapping(value="/Registration",method=RequestMethod.POST)
	public String registerNormalUser(@ModelAttribute RegisterDTO dto,HttpServletRequest request,
			HttpServletResponse response,Model model,RedirectAttributes ra,HttpSession session){
		
		RegisterResponseDTO responseDTO=new RegisterResponseDTO();
		dto.setUsername(dto.getContactNo());
		RegisterError registerError=registerValidation.validateNormalUser(dto);
		if(registerError.isValid()){
			System.err.println(dto);
			dto.setUserType(UserType.User);
			try {
				userApi.saveUser(dto);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				//model.addAttribute("Errormsg", registerError.getMessage());
				//model.addAttribute("username", dto.getContactNo());
				ra.addFlashAttribute("Errormsg", registerError.getMessage());
				return "redirect:/User/SignUp";
			}
			responseDTO.setStatus(ResponseStatus.SUCCESS);
			responseDTO.setMessage("User Registration Successful and OTP sent to ::" + dto.getContactNo()
								+ " and  verification mail sent to ::" + dto.getEmail() + " .");
						responseDTO.setDetails("User Registration Successful and OTP sent to ::" + dto.getContactNo()
								+ " and  verification mail sent to ::" + dto.getEmail() + " .");
						model.addAttribute("username", dto.getContactNo());

						model.addAttribute("successMsg", "Please Enter the OTP sent to Registered No");
						session.setAttribute("username", dto.getContactNo());
						return "User/VerifyMobile";
		}else{
			System.err.println(registerError.getMessage());
			//model.addAttribute("Errormsg", registerError.getMessage());
			//model.addAttribute("username", dto.getContactNo());
			ra.addFlashAttribute("Errormsg", registerError.getMessage());

			return "redirect:/User/SignUp";
		}
	
}

	@RequestMapping(value="/Registration",method=RequestMethod.GET)
	public String registerNormalUser(HttpServletRequest request,
			HttpServletResponse response,Model model){
		System.err.println("hitting here...");
						return "User/SignUp";
				}
	

	@RequestMapping(value = "/Activate/Mobile", method = RequestMethod.POST)
 public	String verifyUserMobile(@ModelAttribute VerifyMobileDTO dto,Model model,
			HttpServletRequest request, HttpServletResponse response) {
		RegisterResponseDTO result = new RegisterResponseDTO();
		System.err.println(dto.getMobileNumber());
					if (verifyUserMobileToken(dto.getKey(), dto.getMobileNumber())) {
					MUser user=userApi.findByUserName(dto.getMobileNumber());
					ResponseDTO resp=matchMoveApi.createUserOnMM(user);
					if(resp.getCode().equalsIgnoreCase("S00")){
					WalletResponse walletResponse=matchMoveApi.assignVirtualCard(user);
					matchMoveApi.tempKycUserMM(user.getUserDetail().getEmail(),user.getUsername());
					matchMoveApi.tempSetIdDetails(user.getUserDetail().getEmail(),user.getUsername());
					matchMoveApi.tempSetImagesForKyc(user.getUserDetail().getEmail(),user.getUsername());
					matchMoveApi.tempConfirmKyc(user.getUserDetail().getEmail(),user.getUsername());
					matchMoveApi.tempKycStatusApproval(user.getUserDetail().getEmail(), user.getUsername());
					if(walletResponse.getCode().equalsIgnoreCase("S00")){
						result.setMessage("Activate Mobile||Card Generated");
					}
					}else{
						result.setMessage("Activate Mobile");
					}
				   	result.setStatus(ResponseStatus.SUCCESS);
					result.setDetails("Your Mobile is Successfully Verified");
					model.addAttribute("successMsg", "Mobile Verified Sucessfully Please Login to Continue");
					return "User/Login";
					
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage("Activate Mobile");
					result.setDetails("Invalid Activation Key");
					model.addAttribute("errorMsg", "Error verifying mobile");
					return "User/Home";
				}
			

	}

	private boolean verifyUserMobileToken(String key, String mobileNumber) {
		if (userApi.checkMobileToken(key, mobileNumber)) {
			return true;
		} else {
			return false;
		}
		
	}*/
}
