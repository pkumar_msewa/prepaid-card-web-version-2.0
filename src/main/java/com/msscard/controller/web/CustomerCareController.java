package com.msscard.controller.web;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;

import com.msscard.app.api.IAuthenticationApi;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.ISessionApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.LoginDTO;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.entity.AdminAccessIdentifier;
import com.msscard.entity.GroupDetails;
import com.msscard.entity.MService;
import com.msscard.entity.MUser;
import com.msscard.entity.UserSession;
import com.msscard.model.ResponseStatus;
import com.msscard.model.UserDTO;
import com.msscard.model.UserDataRequestDTO;
import com.msscard.model.error.AuthenticationError;
import com.msscard.model.error.LoginError;
import com.msscard.repositories.AdminAccessIdentifierRepository;
import com.msscard.repositories.MServiceRepository;
import com.msscard.repositories.MTransactionRepository;
import com.msscard.repositories.MUserRespository;
import com.msscard.repositories.PhysicalCardDetailRepository;
import com.msscard.repositories.UserSessionRepository;
import com.msscard.util.Authorities;
import com.msscard.util.CommonUtil;
import com.msscard.util.ModelMapKey;
import com.msscard.util.SecurityUtil;
import com.msscard.validation.LoginValidation;
import com.msscards.session.SessionLoggingStrategy;

@Controller
@RequestMapping("/Customer")
public class CustomerCareController {
	
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private final SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private final LoginValidation loginValidation;
	private final IAuthenticationApi authenticationApi;
	private final IUserApi userApi;
	private final AuthenticationManager authenticationManager;
	private final UserSessionRepository userSessionRepository;
	private final SessionLoggingStrategy sessionLoggingStrategy;
	private final ISessionApi sessionApi;
	private final AdminAccessIdentifierRepository adminAccessIdentifierRepository;
	private final IMatchMoveApi matchMoveApi;
	private final PhysicalCardDetailRepository physicalCardDetailRepository;
	private final MTransactionRepository mTransactionRepository;
	private final MServiceRepository mServiceRepository;
	private final MUserRespository mUserRespository;
	
	public CustomerCareController(IUserApi userApi, AuthenticationManager authenticationManager,
			UserSessionRepository userSessionRepository, SessionLoggingStrategy sessionLoggingStrategy,
			ISessionApi sessionApi, IAuthenticationApi authenticationApi, AdminAccessIdentifierRepository adminAccessIdentifierRepository,
			LoginValidation loginValidation, IMatchMoveApi matchMoveApi, PhysicalCardDetailRepository physicalCardDetailRepository,
			MTransactionRepository mTransactionRepository, MServiceRepository mServiceRepository, MUserRespository mUserRespository) {
		this.userApi = userApi;
		this.authenticationManager = authenticationManager;
		this.userSessionRepository = userSessionRepository;
		this.sessionLoggingStrategy = sessionLoggingStrategy;
		this.sessionApi = sessionApi;
		this.authenticationApi = authenticationApi;
		this.adminAccessIdentifierRepository= adminAccessIdentifierRepository;
		this.loginValidation = loginValidation;
		this.matchMoveApi = matchMoveApi;
		this.physicalCardDetailRepository = physicalCardDetailRepository;
		this.mTransactionRepository = mTransactionRepository;
		this.mServiceRepository= mServiceRepository;
		this.mUserRespository = mUserRespository;
	}

	@RequestMapping(value="/Login/Process", method = RequestMethod.GET)
	public String getLoginPage(HttpSession session, HttpServletRequest request, ModelMap model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		if(sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if(authority != null) {
				if(authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					MUser userInfo = userSession.getUser();
					AdminAccessIdentifier access = adminAccessIdentifierRepository.getallaccess(userInfo);
					model.addAttribute("addUser", access.isAddUser());
					model.addAttribute("loadCard", access.isLoadCard());
					model.addAttribute("addCorporate", access.isAddCorporate());
					model.addAttribute("addMerchant", access.isAddMerchant());
					model.addAttribute("addAgent", access.isAddAgent());
					model.addAttribute("addDonation", access.isAddDonation());
					model.addAttribute("addGroup", access.isAddGroup());
					model.addAttribute("sendNotification", access.isSendNotification());
					model.addAttribute("assignPhysicalCard", access.isAssignPhysicalCard());
					model.addAttribute("assignVirtualCard", access.isAssignVirtualCard());
					model.addAttribute("androidVersion", access.isAndroidVersion());
					model.addAttribute("cardBlock", access.isCardBlock());

					return "redirect:Admin/Login/Process";		
				}
			}
		}
		return "Admin/CustomerCare";
	}
	
	/*@RequestMapping(value="/Login/Process", method=RequestMethod.POST)
	public String getLoginPost(@ModelAttribute LoginDTO login, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request,HttpSession session, HttpServletResponse response,ModelMap map,Model model) {		
		ResponseDTO result = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		if(sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			UserSession adminSession = userSessionRepository.findByActiveSessionId(sessionId);
			MUser user = adminSession.getUser();
		boolean isValidHash = SecurityUtil.isHashMatches(login, hash);
		login.setIpAddress("192.2.122.2");
		LoginError error = loginValidation.checkLoginValidation(login);
		if (error.isSuccess()) {	
		if (user.getAuthority().contains(Authorities.ADMINISTRATOR)) {
				AuthenticationError auth = authentication(login, request);
				if (auth.isSuccess()) {
					Map<String, Object> detail = new HashMap<String, Object>();
					Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
					sessionLoggingStrategy.onAuthentication(authentication, request, response);
					UserSession userSession = userSessionRepository.findByActiveSessionId(
							RequestContextHolder.currentRequestAttributes().getSessionId());
					try {
					UserDTO activeUser = userApi.getUserById(userSession.getUser().getId());
					MUser u = userApi.findByUserName(userSession.getUser().getUsername());
					u.setMobileToken(null);
					mUserRespository.save(u);
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage("Login successful.");
					detail.put("sessionId", userSession.getSessionId());
					request.getSession().setAttribute("adminSessionId", RequestContextHolder.currentRequestAttributes().getSessionId());
					detail.put("userDetail", activeUser);
					detail.put("accountDetail", user.getAccountDetail());
					Double totalTxnAmount=transactionApi.getTransactionAmount();
					map.put("totalTxnAmount", totalTxnAmount);
					//map.put("totalCredit", totalTxnAmount);
					DecimalFormat f = new DecimalFormat("##.00");
					// System.err.println(String.format("%.2f",
					// walletResponse.getPrefundingBalance()));
					if(totalTxnAmount==null || totalTxnAmount<=0){
					map.addAttribute("totalCredit", "0.0");

					}else{
						map.addAttribute("totalCredit", f.format(Double.parseDouble(totalTxnAmount + "")));
					}
					} catch(Exception e) {
						e.printStackTrace();
						return "Admin/Home";
					}
					try {
					long totalCards=userApi.getMCardCount();
					map.put("totalCards", totalCards);}
					catch(Exception e) {
						e.printStackTrace();
						map.put("totalCards", "Unable to fetch");
					}
					try {	
					long totalPhysicalCard=userApi.gettotalPhysicalCard();
					map.put("totalPhysicalCards", totalPhysicalCard);
					} catch(Exception e){
						map.put("totalPhysicalCards", "Unable to fetch");
						e.printStackTrace();
					}
					try {
					long totalActiveUser=userApi.getTotalActiveUser();
					map.put("totalActiveUser", totalActiveUser);
					} catch(Exception e){
						map.put("totalActiveUser", "Unable to fetch");
						e.printStackTrace();
					}
					UserKycResponse walletResponse=new UserKycResponse(); 
					walletResponse=matchMoveApi.getConsumers();
					try {
						if(walletResponse!=null) {
							DecimalFormat f1 = new DecimalFormat("##.00");
							map.put("totalPrefundAmount", f1.format(Double.parseDouble(walletResponse.getPrefundingBalance())));
						} 
					} catch(Exception e) {
						return "Admin/Home";
					}
					result.setDetails(detail);
					Long phyCount=mMCardRepository.getCountPhysicalCards();
					if(phyCount!=null){
						map.put("phycount", phyCount.longValue());
					} else{
						map.put("phycount", 0);
					}
					Long virtualCard=mMCardRepository.getCountVirtualCards();
					if(virtualCard!=null){
						map.put("virCount", virtualCard.longValue());
					}
					else {
						map.put("virCount", 0);
					}
				Long pendingCardReq=physicalCardDetailRepository.getCountPhysicalCardRequestPending();
				if(pendingCardReq!=null){
					map.put("pendingReq", pendingCardReq.longValue());
				}else{
					map.put("pendingReq",0);
				}
				
				MService razorpay= mServiceRepository.findServiceByCode("LMS");
				MService upiPay=mServiceRepository.findServiceByCode("UPS");
				
				List<String> monthsListRazorPay = mTransactionRepository.getMonthsForBarGraph(Status.Success, razorpay);
				List<String> monthsListUpi = mTransactionRepository.getMonthsForBarGraph(Status.Success, upiPay);
				List<Double> upiAmtList = mTransactionRepository.getTransactionAmountMonthWise(Status.Success, upiPay);
				List<Double> razorAmtList = mTransactionRepository.getTransactionAmountMonthWise(Status.Success, razorpay);
				System.err.println("size monthsListRazorPay:"+ monthsListRazorPay.size());
				
				System.err.println("size razorAmtList:"+razorAmtList.size());

				if(upiAmtList==null||upiAmtList.isEmpty()){
					upiAmtList.add(0.0);
				}
				if(monthsListUpi==null||monthsListUpi.isEmpty()){
					monthsListUpi.add("june");
				}
				System.err.println("size monthsListUpi:"+monthsListUpi.size());
				System.err.println("size upiAmtList:"+upiAmtList.size());
				if(monthsListRazorPay.size()>=monthsListUpi.size()){
					List<String> stringList=new ArrayList<>();
					for (String string : monthsListRazorPay) {
						stringList.add("'"+string+"'");
					}
					model.addAttribute("monthsList", stringList);
					//model.addAttribute("monthsAmountUPI", upiAmtList);

					int sizeDiff=monthsListRazorPay.size()-monthsListUpi.size();
					System.err.println(sizeDiff);
					if(sizeDiff!=0){
						List<Double> doubleList=new ArrayList<>();
						
						for (int i = 0; i < sizeDiff; i++) {
							doubleList.add(0.0);
						}
						for (Double double1 : upiAmtList) {
							doubleList.add(double1);
						}
						model.addAttribute("monthsAmountUPI", doubleList);
					}
					//model.addAttribute("monthsList", monthsListUpi);
				

				}else{
					int sizeDiff=monthsListRazorPay.size()-monthsListUpi.size();
					System.err.println(sizeDiff);
					if(sizeDiff!=0){
						List<Double> doubleList=new ArrayList<>();
						
						for (int i = 0; i < sizeDiff; i++) {
							doubleList.add(0.0);
						}
						for (Double double1 : upiAmtList) {
							doubleList.add(double1);
						}
						model.addAttribute("monthsAmountUPI", doubleList);
					}
					//model.addAttribute("monthsList", monthsListUpi);
				}
				model.addAttribute("amountListRazorPay", razorAmtList);	

				return"Admin/Home";
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage(auth.getMessage());
					result.setDetails(null);
					map.put(ModelMapKey.ERROR, auth.getMessage());
					return "Admin/Login";
				}
			}
		}
	}
		
	}*/
	
	private AuthenticationError authentication(LoginDTO dto, HttpServletRequest request)
			throws ServletException, IOException, Exception {
		AuthenticationError error = new AuthenticationError();
		Authentication auth = null;
		UsernamePasswordAuthenticationToken token = null;
		try {
			token = new UsernamePasswordAuthenticationToken(dto.getUsername(), dto.getPassword());
			System.err.println("password ::" + dto.getPassword());
			auth = authenticationManager.authenticate(token);
			SecurityContext securityContext = SecurityContextHolder.getContext();
			if (auth.isAuthenticated()) {
				securityContext.setAuthentication(auth);
				SecurityContextHolder.getContext().setAuthentication(auth);
				HttpSession session = request.getSession(true);
				session.setMaxInactiveInterval(24*60*60);
				session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
				error.setSuccess(true);
				error.setMessage("Login successful.");
				userApi.handleLoginSuccess(request, null, auth, String.valueOf(token.getPrincipal()),
						dto.getIpAddress());
				return error;
			} else {
				error.setSuccess(false);
				error.setMessage(userApi.handleLoginFailure(request, null, auth, String.valueOf(token.getPrincipal()),
						dto.getIpAddress()));
				SecurityContextHolder.getContext().setAuthentication(null);
				return error;
			}
		} catch (Exception e) {
			e.printStackTrace();
			SecurityContextHolder.getContext().setAuthentication(null);
			error.setSuccess(false);
			error.setMessage(userApi.handleLoginFailure(request, null, auth, String.valueOf(token.getPrincipal()),
					dto.getIpAddress()));
			return error;
		}
	}
}
