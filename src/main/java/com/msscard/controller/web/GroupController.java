package com.msscard.controller.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ewire.errormessages.ErrorMessage;
import com.msscard.app.api.IAuthenticationApi;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.ISessionApi;
import com.msscard.app.api.ITransactionApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.DeleteCorporateDTO;
import com.msscard.app.model.request.LoginDTO;
import com.msscard.app.model.request.LoginResponse;
import com.msscard.app.model.request.MatchMoveCreateCardRequest;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.response.CommonResponse;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.entity.BulkCardAssignmentGroup;
import com.msscard.entity.BulkRegister;
import com.msscard.entity.CorporateAgentDetails;
import com.msscard.entity.CorporateFileCatalogue;
import com.msscard.entity.CorporatePrefundHistory;
import com.msscard.entity.DownloadHistory;
import com.msscard.entity.FCMDetails;
import com.msscard.entity.GroupBulkKyc;
import com.msscard.entity.GroupBulkRegister;
import com.msscard.entity.GroupDetails;
import com.msscard.entity.GroupFileCatalogue;
import com.msscard.entity.GroupSms;
import com.msscard.entity.MBulkRegister;
import com.msscard.entity.MKycDetail;
import com.msscard.entity.MMCards;
import com.msscard.entity.MOperator;
import com.msscard.entity.MService;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.entity.MUserDetails;
import com.msscard.entity.MatchMoveWallet;
import com.msscard.entity.PartnerDetails;
import com.msscard.entity.PhysicalCardDetails;
import com.msscard.entity.UserSession;
import com.msscard.model.AddPartnerDTO;
import com.msscard.model.BulkRegisterDTO;
import com.msscard.model.CorporateBulkUsers;
import com.msscard.model.DonationDTO;
import com.msscard.model.DownloadCsv;
import com.msscard.model.GroupDetailDTO;
import com.msscard.model.MMCardsDTO;
import com.msscard.model.PagingDTO;
import com.msscard.model.PrefundHistoryDTO;
import com.msscard.model.RegisterDTO;
import com.msscard.model.RequestDTO;
import com.msscard.model.ResponseStatus;
import com.msscard.model.SendNotificationDTO;
import com.msscard.model.Status;
import com.msscard.model.TransactionListDTO;
import com.msscard.model.UpgradeAccountDTO;
import com.msscard.model.UserDTO;
import com.msscard.model.UserDataRequestDTO;
import com.msscard.model.UserType;
import com.msscard.model.error.RegisterError;
import com.msscard.repositories.BulkRegisterRepository;
import com.msscard.repositories.CorporateAgentDetailsRepository;
import com.msscard.repositories.CorporateFileCatalogueRepository;
import com.msscard.repositories.CorporatePrefundHistoryRepository;
import com.msscard.repositories.DownloadHistoryRepository;
import com.msscard.repositories.FCMDetailsRepository;
import com.msscard.repositories.GroupDetailsRepository;
import com.msscard.repositories.GroupFileCatalogueRepository;
import com.msscard.repositories.GroupSmsRepository;
import com.msscard.repositories.MKycRepository;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MOperatorRepository;
import com.msscard.repositories.MServiceRepository;
import com.msscard.repositories.MTransactionRepository;
import com.msscard.repositories.MUserDetailRepository;
import com.msscard.repositories.MUserRespository;
import com.msscard.repositories.MatchMoveWalletRepository;
import com.msscard.repositories.PartnerDetailsRepository;
import com.msscard.repositories.PhysicalCardDetailRepository;
import com.msscard.repositories.UserSessionRepository;
import com.msscard.util.Authorities;
import com.msscard.util.CSVReader;
import com.msscard.util.CommonUtil;
import com.msscard.util.ExcelWriter;
import com.msscard.util.ModelMapKey;
import com.msscard.util.SecurityUtil;
import com.msscard.util.StartUpUtil;
import com.msscard.validation.RegisterValidation;

@Controller
@RequestMapping("/Group")
public class GroupController {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private final IAuthenticationApi authenticationApi;
	private final UserSessionRepository userSessionRepository;
	private final IUserApi userApiImpl;
	private final MUserRespository mUserRespository;
	private final ISessionApi sessionApi;
	private final GroupSmsRepository groupSmsRepository;
	private final PartnerDetailsRepository partnerDetailsRepository;
	private final GroupDetailsRepository groupDetailsRepository;
	private final GroupFileCatalogueRepository groupFileCatalogueRepository;
	private final CorporateFileCatalogueRepository corporateFileCatalogueRepository;
	private final RegisterValidation registerValidation;
	private final IMatchMoveApi matchMoveApi;
	private final CorporateAgentDetailsRepository corporateAgentDetailsRepository;
	private final BulkRegisterRepository bulkRegisterRepository;
	private final MatchMoveWalletRepository matchMoveWalletRepository;
	private final PhysicalCardDetailRepository physicalCardDetailRepository;
	private final MMCardRepository mMCardRepository;
	private final DownloadHistoryRepository downloadHistoryRepository;
	private final CorporatePrefundHistoryRepository corporatePrefundHistoryRepository;
	private final MKycRepository mKycRepository;
	private final MUserDetailRepository mUserDetailRepository;
	private final MTransactionRepository mTransactionRepository;
	private final ITransactionApi transactionApi;
	private final MOperatorRepository mOperatorRepository;
	private final MServiceRepository mServiceRepository;
	private final FCMDetailsRepository fCMDetailsRepository;

	public GroupController(IAuthenticationApi authenticationApi, UserSessionRepository userSessionRepository,
			IUserApi userApiImpl, MUserRespository mUserRespository, ISessionApi sessionApi,
			GroupSmsRepository groupSmsRepository, PartnerDetailsRepository partnerDetailsRepository,
			GroupDetailsRepository groupDetailsRepository, GroupFileCatalogueRepository groupFileCatalogueRepository,
			CorporateFileCatalogueRepository corporateFileCatalogueRepository, RegisterValidation registerValidation,
			IMatchMoveApi matchMoveApi, CorporateAgentDetailsRepository corporateAgentDetailsRepository,
			BulkRegisterRepository bulkRegisterRepository, MatchMoveWalletRepository matchMoveWalletRepository,
			PhysicalCardDetailRepository physicalCardDetailRepository, MMCardRepository mMCardRepository,
			DownloadHistoryRepository downloadHistoryRepository,
			CorporatePrefundHistoryRepository corporatePrefundHistoryRepository, MKycRepository mKycRepository,
			MUserDetailRepository mUserDetailRepository, MTransactionRepository mTransactionRepository,
			ITransactionApi transactionApi, MOperatorRepository mOperatorRepository,
			MServiceRepository mServiceRepository, FCMDetailsRepository fCMDetailsRepository) {
		this.authenticationApi = authenticationApi;
		this.userSessionRepository = userSessionRepository;
		this.userApiImpl = userApiImpl;
		this.mUserRespository = mUserRespository;
		this.sessionApi = sessionApi;
		this.groupSmsRepository = groupSmsRepository;
		this.partnerDetailsRepository = partnerDetailsRepository;
		this.groupDetailsRepository = groupDetailsRepository;
		this.groupFileCatalogueRepository = groupFileCatalogueRepository;
		this.corporateFileCatalogueRepository = corporateFileCatalogueRepository;
		this.registerValidation = registerValidation;
		this.matchMoveApi = matchMoveApi;
		this.corporateAgentDetailsRepository = corporateAgentDetailsRepository;
		this.bulkRegisterRepository = bulkRegisterRepository;
		this.matchMoveWalletRepository = matchMoveWalletRepository;
		this.physicalCardDetailRepository = physicalCardDetailRepository;
		this.mMCardRepository = mMCardRepository;
		this.downloadHistoryRepository = downloadHistoryRepository;
		this.corporatePrefundHistoryRepository = corporatePrefundHistoryRepository;
		this.mKycRepository = mKycRepository;
		this.mUserDetailRepository = mUserDetailRepository;
		this.mTransactionRepository = mTransactionRepository;
		this.transactionApi = transactionApi;
		this.mOperatorRepository = mOperatorRepository;
		this.mServiceRepository = mServiceRepository;
		this.fCMDetailsRepository = fCMDetailsRepository;
	}

	@RequestMapping(value = "/Home", method = RequestMethod.GET)
	public String getLoginPage(HttpSession session, HttpServletRequest request, ModelMap modelMap) {
		String sessionId = (String) session.getAttribute("groupSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if (authority != null) {
				if (authority.contains(Authorities.GROUP) && authority.contains(Authorities.AUTHENTICATED)) {
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					MUser userInfo = userSession.getUser();

					UserDataRequestDTO lUserData = new UserDataRequestDTO();

					lUserData.setSessionId(sessionId);
					try {

						Calendar cal = Calendar.getInstance();
						cal.add(Calendar.MONTH, -1);
						Date oneMonthBack = cal.getTime();
						Calendar calPresent = Calendar.getInstance();
						Date present = calPresent.getTime();
						lUserData.setFrom(oneMonthBack);
						lUserData.setTo(present);

					} catch (Exception e) {
						e.printStackTrace();
					}
					lUserData.setEmail(userInfo.getUserDetail().getEmail());
					lUserData.setContactNo(userInfo.getUserDetail().getContactNo());

					List<GroupDetails> details = userApiImpl.getGroupDetails();
					List<MUser> lUserDataDto = userApiImpl.groupAddRequestByContact(lUserData);

					long count = userApiImpl.getCountGroupRequest(userInfo.getUserDetail().getEmail());
					long acceptedcount = userApiImpl.getCountGroupAccept(userInfo.getUserDetail().getEmail());
					long rejectedCount = userApiImpl.getCountGroupReject(userInfo.getUserDetail().getContactNo());
					long bulkRegCount = userApiImpl.getBulkRegCount(userInfo.getUserDetail().getEmail());
					long bulkSmsCount = userApiImpl.getBulkSmsCount(userInfo.getUserDetail().getEmail());
					long bulkCardLoadCount = userApiImpl.getBulkCardLoadCount(userInfo.getUserDetail().getEmail());
					long totalUsersGroup = userApiImpl.getTotalUsersOfGroup(userInfo.getUserDetail().getEmail());

					modelMap.addAttribute("userData", lUserDataDto);

					modelMap.addAttribute("groupList", details);
					modelMap.put("userInfo", userInfo);

					String name = userInfo.getUserDetail().getFirstName();

					modelMap.put(ModelMapKey.MESSAGE, session.getAttribute(ModelMapKey.MESSAGE));
					modelMap.put(ModelMapKey.ERROR, session.getAttribute(ModelMapKey.ERROR));
					modelMap.addAttribute("fromDate", lUserData.getFromDate());
					modelMap.addAttribute("toDate", lUserData.getToDate());
					modelMap.addAttribute("name", name);
					session.setAttribute("name", name);

					request.getSession().setAttribute(ModelMapKey.MESSAGE, "");
					request.getSession().setAttribute(ModelMapKey.ERROR, "");
					session.setAttribute("mobile", userInfo.getUserDetail().getContactNo());

					modelMap.addAttribute("requestCount", count);
					modelMap.addAttribute("acceptCount", acceptedcount);
					modelMap.addAttribute("rejectedCount", rejectedCount);
					modelMap.addAttribute("bulkRegCount", bulkRegCount);
					modelMap.addAttribute("bulkSmsCount", bulkSmsCount);
					modelMap.addAttribute("bulkCardLoadCount", bulkCardLoadCount);
					modelMap.addAttribute("totalUsersGroup", totalUsersGroup);

					return "Group/Home";
				}
			}
		}
		return "Group/Login";
	}

	@RequestMapping(value = "/Home", method = RequestMethod.POST)
	public String getLoginHomePage(HttpServletRequest request, ModelMap modelMap, HttpServletResponse response,
			HttpSession session, @ModelAttribute LoginDTO dto) {
		try {
			dto.setIpAddress(request.getRemoteAddr());

			MUser user = mUserRespository.findByUsername(dto.getUsername());
			if (user.getMobileToken().equalsIgnoreCase(dto.getMobileToken())) {
				LoginResponse loginResponse = userApiImpl.loginGroup(dto, request, response);
				String sessionId = loginResponse.getSessionId();
				request.getSession().setAttribute(CommonUtil.GROUP_SESSION_ID, sessionId);

				if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(loginResponse.getCode())) {

					MUser userInfo = mUserRespository.findByUsername(loginResponse.getContactNo());
					userInfo.setMobileToken(null);
					mUserRespository.save(userInfo);
					UserDataRequestDTO lUserData = new UserDataRequestDTO();
					lUserData.setSessionId(sessionId);
					SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
					String daterange[] = null;
					try {
						if (dto.getDaterange() != null) {
							System.err.println(dto.getDaterange());
							String aaaaa = dto.getDaterange().substring(0, 10);
							String baaaa = dto.getDaterange().substring(13);
							Date from = sdformat.parse(aaaaa + " 00:00:00");
							Date to = sdformat.parse(baaaa + " 00:00:00");
							lUserData.setFrom(from);
							lUserData.setTo(to);

						} else {
							daterange = CommonUtil.getDefaultDateRange();
							Date fromDate = df2.parse(daterange[0]);
							Date toDate = df2.parse(daterange[1]);
							lUserData.setFrom(fromDate);
							lUserData.setTo(toDate);
						}

					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					logger.info("contact in post-- " + userInfo.getUserDetail().getContactNo());

					lUserData.setFromDate(daterange[0]);
					lUserData.setToDate(daterange[1]);
					lUserData.setContactNo(userInfo.getUserDetail().getContactNo());
					lUserData.setEmail(userInfo.getUserDetail().getEmail());

					List<GroupDetails> details = userApiImpl.getGroupDetails();
					List<MUser> lUserDataDto = userApiImpl.groupAddRequestByContact(lUserData);
					long count = userApiImpl.getCountGroupRequest(userInfo.getUserDetail().getEmail());
					long acceptedcount = userApiImpl.getCountGroupAccept(userInfo.getUserDetail().getEmail());
					long rejectedCount = userApiImpl.getCountGroupReject(userInfo.getUserDetail().getContactNo());
					long bulkRegCount = userApiImpl.getBulkRegCount(userInfo.getUserDetail().getEmail());
					long bulkSmsCount = userApiImpl.getBulkSmsCount(userInfo.getUserDetail().getEmail());
					long bulkCardLoadCount = userApiImpl.getBulkCardLoadCount(userInfo.getUserDetail().getEmail());
					long totalUsersGroup = userApiImpl.getTotalUsersOfGroup(userInfo.getUserDetail().getEmail());

					modelMap.addAttribute("userData", lUserDataDto);
					modelMap.addAttribute("groupList", details);
					modelMap.put("userInfo", userInfo);
					modelMap.addAttribute("fromDate", lUserData.getFromDate());
					modelMap.addAttribute("toDate", lUserData.getToDate());
					modelMap.addAttribute("name", userInfo.getUserDetail().getFirstName());

					modelMap.addAttribute("requestCount", count);
					modelMap.addAttribute("acceptCount", acceptedcount);
					modelMap.addAttribute("rejectedCount", rejectedCount);
					modelMap.addAttribute("bulkRegCount", bulkRegCount);
					modelMap.addAttribute("bulkSmsCount", bulkSmsCount);
					modelMap.addAttribute("bulkCardLoadCount", bulkCardLoadCount);
					modelMap.addAttribute("totalUsersGroup", totalUsersGroup);

					return "Group/Home";
				} else {
					modelMap.put(ModelMapKey.ERROR, loginResponse.getMessage());
					return "Group/Login";
				}
			} else {
				modelMap.addAttribute("error", "Incorrect OTP");
				return "Group/VerifyMobile";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Group/Login";
	}

	@RequestMapping(value = "/Logout", method = RequestMethod.GET)
	String logoutUserApi(HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws Exception {
		ResponseDTO result = new ResponseDTO();

		try {
			UserSession userSession = userSessionRepository.findBySessionId(session.getId());
			if (userSession != null) {
				sessionApi.expireSession(session.getId());
				result.setStatus(ResponseStatus.SUCCESS);
				result.setMessage("User logout successful");
				result.setDetails("Session Out");
				session.invalidate();
				return "Group/Login";
			} else {
				result.setStatus(ResponseStatus.INVALID_SESSION);
				result.setMessage("Please, login and try again.");
				result.setDetails("Please, login and try again.");
				session.invalidate();
				return "Group/Login";
			}
		} catch (Exception e) {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Failed, invalid request.");
			result.setDetails("Failed, invalid request.");
			return "Group/Login";
		}

	}

	@RequestMapping(value = "/GroupRequestList", method = RequestMethod.GET)
	public String getGroupRequestList(@ModelAttribute PagingDTO dto, @ModelAttribute("fromDate") String fromDate,
			@ModelAttribute("toDate") String toDate, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		try {
			String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
			logger.info("session:: " + sessionId);

			if (sessionId != null && !sessionId.isEmpty()) {
				String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
				if (authority != null
						&& (((authority.contains(Authorities.GROUP) || authority.contains(Authorities.SUPER_ADMIN))
								&& authority.contains(Authorities.AUTHENTICATED)))) {

					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					MUser userInfo = userSession.getUser();

					UserDataRequestDTO lUserData = new UserDataRequestDTO();
					lUserData.setSessionId(sessionId);
					try {
						Calendar cal = Calendar.getInstance();
						cal.add(Calendar.MONTH, -1);
						Date oneMonthBack = cal.getTime();
						Calendar calPresent = Calendar.getInstance();
						Date present = calPresent.getTime();
						lUserData.setFrom(oneMonthBack);
						lUserData.setTo(present);
					} catch (Exception e) {
						e.printStackTrace();
					}

					/* lUserData.setContactNo(userInfo.getUserDetail().getContactNo()); */
					lUserData.setEmail(userInfo.getUserDetail().getEmail());

					List<MUser> lUserDataDto = userApiImpl.groupAddRequestByContactGet(lUserData);
					List<GroupDetails> details = userApiImpl.getGroupDetails();

					model.addAttribute("groupList", details);
					model.addAttribute("userData", lUserDataDto);
					model.addAttribute("fromDate", lUserData.getFromDate());
					model.addAttribute("toDate", lUserData.getToDate());
					model.addAttribute("name", userInfo.getUserDetail().getFirstName());

					model.addAttribute("userInfo", userInfo);

					return "/Group/AddGroupRequest";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/Group/Home";
	}

	@RequestMapping(value = "/GroupRequestList", method = RequestMethod.POST)
	public String getGroupRequestListPost(@ModelAttribute PagingDTO dto, @ModelAttribute("fromDate") String fromDate,
			@ModelAttribute("toDate") String toDate, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		try {
			String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
			logger.info("session:: " + sessionId);

			if (sessionId != null && !sessionId.isEmpty()) {
				String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
				if (authority != null
						&& (((authority.contains(Authorities.GROUP) || authority.contains(Authorities.SUPER_ADMIN))
								&& authority.contains(Authorities.AUTHENTICATED)))) {
					UserDataRequestDTO lUserData = new UserDataRequestDTO();
					lUserData.setSessionId(sessionId);
					String daterange[] = null;
					SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
					try {
						if (dto.getReportrange() != null) {
							System.err.println(dto.getReportrange());
							String aaaaa = dto.getReportrange().substring(0, 10);
							String baaaa = dto.getReportrange().substring(13);
							Date from = sdformat.parse(aaaaa + " 00:00:00");
							Date to = sdformat.parse(baaaa + " 00:00:00");
							lUserData.setFrom(from);
							lUserData.setTo(to);
						} else {
							daterange = CommonUtil.getDefaultDateRange();
							Date f = df2.parse(daterange[0]);
							Date t = df2.parse(daterange[1]);
							lUserData.setFrom(f);
							lUserData.setTo(t);
						}

					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					MUser userInfo = userSession.getUser();

					/* lUserData.setContactNo(userInfo.getUserDetail().getContactNo()); */
					lUserData.setEmail(userInfo.getUserDetail().getEmail());

					List<MUser> lUserDataDto = userApiImpl.groupAddRequestByContact(lUserData);
					List<GroupDetails> details = userApiImpl.getGroupDetails();
					model.addAttribute("groupList", details);
					model.addAttribute("userData", lUserDataDto);
					model.addAttribute("fromDate", lUserData.getFromDate());
					model.addAttribute("toDate", lUserData.getToDate());
					model.addAttribute("name", userInfo.getUserDetail().getFirstName());

					model.addAttribute("userInfo", userInfo);
					return "/Group/AddGroupRequest";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/Group/Home";
	}

	@RequestMapping(value = "/ChangeRequestList", method = RequestMethod.GET)
	public String getGroupChangeList(@ModelAttribute PagingDTO dto, @ModelAttribute("fromDate") String fromDate,
			@ModelAttribute("toDate") String toDate, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		try {
			String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
			logger.info("session:: " + sessionId);

			if (sessionId != null && !sessionId.isEmpty()) {
				String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
				if (authority != null
						&& (((authority.contains(Authorities.GROUP) || authority.contains(Authorities.SUPER_ADMIN))
								&& authority.contains(Authorities.AUTHENTICATED)))) {

					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					MUser userInfo = userSession.getUser();

					UserDataRequestDTO lUserData = new UserDataRequestDTO();
					lUserData.setSessionId(sessionId);

					lUserData.setContactNo(userInfo.getUserDetail().getContactNo());
					lUserData.setEmail(userInfo.getUserDetail().getEmail());

					List<MUser> lUserDataDto = userApiImpl.changeRequestListGet(lUserData.getEmail());
					List<GroupDetails> details = userApiImpl.getGroupDetails();

					model.addAttribute("groupList", details);
					model.addAttribute("userData", lUserDataDto);
					model.addAttribute("fromDate", lUserData.getFromDate());
					model.addAttribute("toDate", lUserData.getToDate());
					model.addAttribute("name", userInfo.getUserDetail().getFirstName());

					model.addAttribute("userInfo", userInfo);

					return "/Group/ChangeGroupRequests";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/Group/Home";
	}

	@RequestMapping(value = "/ChangeRequestList", method = RequestMethod.POST)
	public String getGroupChangeRequestListPost(@ModelAttribute PagingDTO dto,
			@ModelAttribute("fromDate") String fromDate, @ModelAttribute("toDate") String toDate,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) {
		try {
			String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
			logger.info("session:: " + sessionId);

			if (sessionId != null && !sessionId.isEmpty()) {
				String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
				if (authority != null
						&& (((authority.contains(Authorities.GROUP) || authority.contains(Authorities.SUPER_ADMIN))
								&& authority.contains(Authorities.AUTHENTICATED)))) {

					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					MUser userInfo = userSession.getUser();

					UserDataRequestDTO lUserData = new UserDataRequestDTO();
					lUserData.setSessionId(sessionId);

					String daterange[] = null;
					SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
					try {
						if (dto.getReportrange() != null) {
							System.err.println(dto.getReportrange());
							String aaaaa = dto.getReportrange().substring(0, 10);
							String baaaa = dto.getReportrange().substring(13);
							Date from = sdformat.parse(aaaaa + " 00:00:00");
							Date to = sdformat.parse(baaaa + " 00:00:00");
							lUserData.setFrom(from);
							lUserData.setTo(to);
						} else {
							daterange = CommonUtil.getDefaultDateRange();
							Date f = df2.parse(daterange[0]);
							Date t = df2.parse(daterange[1]);
							lUserData.setFrom(f);
							lUserData.setTo(t);
						}

					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					lUserData.setContactNo(userInfo.getUserDetail().getContactNo());
					lUserData.setEmail(userInfo.getUserDetail().getEmail());

					List<MUser> lUserDataDto = userApiImpl.changeRequestListPost(lUserData);
					List<GroupDetails> details = userApiImpl.getGroupDetails();

					model.addAttribute("groupList", details);
					model.addAttribute("userData", lUserDataDto);
					model.addAttribute("fromDate", lUserData.getFromDate());
					model.addAttribute("toDate", lUserData.getToDate());
					model.addAttribute("name", userInfo.getUserDetail().getFirstName());

					model.addAttribute("userInfo", userInfo);

					return "/Group/ChangeGroupRequests";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/Group/Home";
	}

	@RequestMapping(value = "/AcceptGroup/{username}", method = RequestMethod.POST)
	public ResponseEntity<CommonResponse> acceptGroup(@PathVariable(value = "username") String contactNo,
			@ModelAttribute("fromDate") String fromDate, @ModelAttribute("toDate") String toDate,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) {
		CommonResponse result = new CommonResponse();
		try {
			String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);

			if (sessionId != null && !sessionId.isEmpty()) {
				String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
				if (authority != null
						&& (((authority.contains(Authorities.GROUP) || authority.contains(Authorities.SUPER_ADMIN))
								&& authority.contains(Authorities.AUTHENTICATED)))) {
					logger.info("contact: " + contactNo);
					result = userApiImpl.acceptGroupAddRequest(contactNo, result);
				} else {
					result.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
					result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE.getKey());
					result.setSuccess(false);
					result.setMessage("Unauthorized Role");
				}
			} else {
				result.setCode(ResponseStatus.INVALID_SESSION.getValue());
				result.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				result.setSuccess(false);
				result.setMessage("Session Expired");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<CommonResponse>(result, HttpStatus.OK);
	}

	/*
	 * @RequestMapping(value="/RejectGroup/{username}", method = RequestMethod.POST)
	 * public ResponseEntity<CommonResponse> rejectGroup(@PathVariable(value =
	 * "username") String contactNo, @ModelAttribute("fromDate") String fromDate,
	 * 
	 * @ModelAttribute("toDate") String toDate, HttpServletRequest request,
	 * HttpServletResponse response, HttpSession session, ModelMap model) {
	 * CommonResponse result = new CommonResponse(); try { String sessionId =
	 * (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
	 * 
	 * if (sessionId != null && !sessionId.isEmpty()) { String authority =
	 * authenticationApi.getAuthorityFromSessionRequest(sessionId, request); if
	 * (authority != null && (((authority.contains(Authorities.GROUP) ||
	 * authority.contains(Authorities.SUPER_ADMIN)) &&
	 * authority.contains(Authorities.AUTHENTICATED)))) {
	 * logger.info("contact: "+contactNo); result =
	 * userApiImpl.rejectGroupRequest(contactNo, result); } else {
	 * result.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
	 * result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE.getKey());
	 * result.setSuccess(false); result.setMessage("Unauthorized Role"); } } else {
	 * result.setCode(ResponseStatus.INVALID_SESSION.getValue());
	 * result.setStatus(ResponseStatus.INVALID_SESSION.getKey());
	 * result.setSuccess(false); result.setMessage("Session Expired"); }
	 * 
	 * } catch(Exception e) { e.printStackTrace(); } return new
	 * ResponseEntity<CommonResponse>(result, HttpStatus.OK); }
	 */

	@RequestMapping(value = "/RejectGroup", method = RequestMethod.POST)
	public ResponseEntity<CommonResponse> rejectGroupReason(@RequestBody PagingDTO page,
			@ModelAttribute("fromDate") String fromDate, @ModelAttribute("toDate") String toDate,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) {
		CommonResponse result = new CommonResponse();

		try {
			String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);

			if (sessionId != null && !sessionId.isEmpty()) {
				String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
				if (authority != null
						&& (((authority.contains(Authorities.GROUP) || authority.contains(Authorities.SUPER_ADMIN))
								&& authority.contains(Authorities.AUTHENTICATED)))) {
					logger.info("contact: " + page.getContact());
					result = userApiImpl.rejectGroupRequest(page, result);
				} else {
					result.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
					result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE.getKey());
					result.setSuccess(false);
					result.setMessage("Unauthorized Role");
				}
			} else {
				result.setCode(ResponseStatus.INVALID_SESSION.getValue());
				result.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				result.setSuccess(false);
				result.setMessage("Session Expired");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<CommonResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/RejectedUserGroup", method = RequestMethod.GET)
	public String acceptedUserListGet(@ModelAttribute PagingDTO dto, @ModelAttribute("fromDate") String fromDate,
			@ModelAttribute("toDate") String toDate, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		ResponseDTO result = new ResponseDTO();

		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && !sessionId.isEmpty()) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if (authority != null
					&& ((authority.contains(Authorities.GROUP) && authority.contains(Authorities.AUTHENTICATED)))) {

				UserDataRequestDTO lUserData = new UserDataRequestDTO();
				lUserData.setSessionId(sessionId);
				try {
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.MONTH, -1);
					Date oneMonthBack = cal.getTime();
					Calendar calPresent = Calendar.getInstance();
					Date present = calPresent.getTime();
					lUserData.setFrom(oneMonthBack);
					lUserData.setTo(present);
				} catch (Exception e) {
					e.printStackTrace();
				}
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				MUser userInfo = userSession.getUser();

				lUserData.setContactNo(userInfo.getUserDetail().getContactNo());

				List<MUser> lUserDataDto = userApiImpl.groupRejectedUserByContact(lUserData);

				model.addAttribute("userData", lUserDataDto);
				model.addAttribute("fromDate", lUserData.getFromDate());
				model.addAttribute("toDate", lUserData.getToDate());

				model.addAttribute("userInfo", userInfo);

				model.addAttribute("name", userInfo.getUserDetail().getFirstName());

			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.AUTHORITY_MSG);
			}
		} else {
			return "redirect:/Group/Home";
		}
		return "Group/RejectedUserGroup";
	}

	@RequestMapping(value = "/RejectedUserGroup", method = RequestMethod.POST)
	public String rejectedUserList(@ModelAttribute PagingDTO dto, @ModelAttribute("fromDate") String fromDate,
			@ModelAttribute("toDate") String toDate, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		try {
			String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);

			if (sessionId != null && !sessionId.isEmpty()) {
				String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
				if (authority != null
						&& (((authority.contains(Authorities.GROUP) || authority.contains(Authorities.SUPER_ADMIN))
								&& authority.contains(Authorities.AUTHENTICATED)))) {
					UserDataRequestDTO lUserData = new UserDataRequestDTO();
					lUserData.setSessionId(sessionId);
					String daterange[] = null;
					SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
					try {
						if (dto.getReportrange() != null) {
							System.err.println(dto.getReportrange());
							String aaaaa = dto.getReportrange().substring(0, 10);
							String baaaa = dto.getReportrange().substring(13);
							Date from = sdformat.parse(aaaaa + " 00:00:00");
							Date to = sdformat.parse(baaaa + " 00:00:00");
							lUserData.setFrom(from);
							lUserData.setTo(to);
						} else {
							daterange = CommonUtil.getDefaultDateRange();
							Date f = df2.parse(daterange[0]);
							Date t = df2.parse(daterange[1]);
							lUserData.setFrom(f);
							lUserData.setTo(t);
						}

					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					MUser userInfo = userSession.getUser();

					lUserData.setContactNo(userInfo.getUserDetail().getContactNo());

					List<MUser> lUserDataDto = userApiImpl.groupRejectedUserByContactPost(lUserData);
					model.addAttribute("userData", lUserDataDto);
					model.addAttribute("fromDate", lUserData.getFromDate());
					model.addAttribute("toDate", lUserData.getToDate());

					model.addAttribute("userInfo", userInfo);
					model.addAttribute("name", userInfo.getUserDetail().getFirstName());

					return "/Group/RejectedUserGroup";
				}
			} else {
				return "redirect:/Group/Home";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Group/RejectedUserGroup";
	}

	@RequestMapping(value = "/AcceptedUserGroup", method = RequestMethod.GET)
	public String acceptedUserList(@ModelAttribute PagingDTO dto, @ModelAttribute("fromDate") String fromDate,
			@ModelAttribute("toDate") String toDate, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		ResponseDTO result = new ResponseDTO();

		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && !sessionId.isEmpty()) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if (authority != null
					&& ((authority.contains(Authorities.GROUP) && authority.contains(Authorities.AUTHENTICATED)))) {

				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				MUser userInfo = userSession.getUser();

				UserDataRequestDTO lUserData = new UserDataRequestDTO();
				lUserData.setSessionId(sessionId);
				try {
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.MONTH, -1);
					Date oneMonthBack = cal.getTime();
					Calendar calPresent = Calendar.getInstance();
					Date present = calPresent.getTime();
					lUserData.setFrom(oneMonthBack);
					lUserData.setTo(present);
				} catch (Exception e) {
					e.printStackTrace();
				}
				lUserData.setContactNo(userInfo.getUserDetail().getContactNo());
				lUserData.setEmail(userInfo.getUserDetail().getEmail());

				List<MUser> lUserDataDto = userApiImpl.groupAcceptedUserByContact(lUserData);

				model.addAttribute("userData", lUserDataDto);
				model.addAttribute("fromDate", lUserData.getFromDate());
				model.addAttribute("toDate", lUserData.getToDate());
				model.addAttribute("name", userInfo.getUserDetail().getFirstName());

				model.addAttribute("userInfo", userInfo);

			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.AUTHORITY_MSG);
			}
		} else {
			return "redirect:/Group/Home";
		}
		return "Group/AcceptedUserGroup";
	}

	@RequestMapping(value = "/AcceptedUserGroup", method = RequestMethod.POST)
	public String acceptedUserListPost(@ModelAttribute PagingDTO dto, @ModelAttribute("fromDate") String fromDate,
			@ModelAttribute("toDate") String toDate, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		ResponseDTO result = new ResponseDTO();

		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && !sessionId.isEmpty()) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if (authority != null
					&& ((authority.contains(Authorities.GROUP) && authority.contains(Authorities.AUTHENTICATED)))) {

				UserDataRequestDTO lUserData = new UserDataRequestDTO();
				lUserData.setSessionId(sessionId);
				String daterange[] = null;
				SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
				try {
					if (dto.getReportrange() != null) {
						System.err.println(dto.getReportrange());
						String aaaaa = dto.getReportrange().substring(0, 10);
						String baaaa = dto.getReportrange().substring(13);
						Date from = sdformat.parse(aaaaa + " 00:00:00");
						Date to = sdformat.parse(baaaa + " 00:00:00");
						lUserData.setFrom(from);
						lUserData.setTo(to);
					} else {
						daterange = CommonUtil.getDefaultDateRange();
						Date f = df2.parse(daterange[0]);
						Date t = df2.parse(daterange[1]);
						lUserData.setFrom(f);
						lUserData.setTo(t);
					}

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				MUser userInfo = userSession.getUser();

				lUserData.setContactNo(userInfo.getUserDetail().getContactNo());
				lUserData.setEmail(userInfo.getUserDetail().getEmail());
				List<MUser> lUserDataDto = userApiImpl.acceptedUserByContactPostGroup(lUserData);
				model.addAttribute("userData", lUserDataDto);
				model.addAttribute("fromDate", lUserData.getFromDate());
				model.addAttribute("toDate", lUserData.getToDate());

				model.addAttribute("userInfo", userInfo);
				model.addAttribute("name", userInfo.getUserDetail().getFirstName());

			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.AUTHORITY_MSG);
			}
		} else {
			return "redirect:/Group/Home";
		}
		return "Group/AcceptedUserGroup";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/SingleGroupRequest")
	public String getSingleUserGroupRequest(@ModelAttribute GroupDetailDTO dto, HttpServletRequest request,
			HttpSession session, ModelMap model) {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (authority != null) {
				if ((authority.contains(Authorities.GROUP) && authority.contains(Authorities.AUTHENTICATED))) {
					try {

						if (dto.getRemark().equalsIgnoreCase("request")) {
							List<MUser> user = userApiImpl.getRequestUser(dto.getMobileNoId(),
									userSession.getUser().getUserDetail().getEmail());
							if (user != null) {
								model.addAttribute("userData", user);
							}
							return "Group/AddGroupRequest";
						} else if (dto.getRemark().equalsIgnoreCase("rejected")) {
							List<MUser> user = userApiImpl.getRejectedUser(dto.getMobileNoId(),
									userSession.getUser().getUserDetail().getContactNo());
							if (user != null) {
								model.addAttribute("userData", user);
							}
							return "Group/RejectedUserGroup";
						} else if (dto.getRemark().equalsIgnoreCase("accepted")) {
							List<MUser> user = userApiImpl.getAcceptedUser(dto.getMobileNoId(),
									userSession.getUser().getUserDetail().getEmail());
							if (user != null) {
								model.addAttribute("userData", user);
							}
							return "Group/AcceptedUserGroup";
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					return "Group/Login";
				}
			} else {
				return "Group/Login";
			}
		} else {
			return "Group/Login";
		}
		return "Group/Login";
	}

	@RequestMapping(value = "/DonationList", method = RequestMethod.GET)
	public String getDonationBasedOnGroup(@ModelAttribute PagingDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		ResponseDTO result = new ResponseDTO();
		try {
			String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
			if (sessionId != null && !sessionId.isEmpty()) {
				String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
				if (authority != null
						&& ((authority.contains(Authorities.GROUP) && authority.contains(Authorities.AUTHENTICATED)))) {
					UserDataRequestDTO lUserData = new UserDataRequestDTO();
					lUserData.setSessionId(sessionId);
					SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
					Date f = df2.parse(CommonUtil.getDefaultDateRange()[0]);
					Date t = df2.parse(CommonUtil.getDefaultDateRange()[1]);
					lUserData.setFrom(f);
					lUserData.setTo(t);

					List<DonationDTO> lUserDataDto = userApiImpl.donationListBasedOnGroup(lUserData);
				} else {
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage(ErrorMessage.AUTHORITY_MSG);
				}
			} else {
				return "redirect:/Group/Home";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Group/DonationList";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Login/VerifyMobile")
	public String getVerifyMobile(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute LoginDTO dto, HttpSession session, ModelMap map) {
		ResponseDTO result = new ResponseDTO();
		try {
			logger.info("mobilenumber:: " + dto.getUsername());
			dto.setIpAddress(request.getRemoteAddr());
			LoginResponse loginResponse = userApiImpl.loginGroup(dto, request, response);
			// String sessionId = loginResponse.getSessionId();
			// request.getSession().setAttribute(CommonUtil.GROUP_SESSION_ID, sessionId);

			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(loginResponse.getCode())) {
				boolean verified = userApiImpl.resendMobileTokenDeviceBindingGroup(dto.getUsername());
				if (verified) {
					result.setStatus(ResponseStatus.SUCCESS.getKey());
					result.setCode(ResponseStatus.SUCCESS.getValue());
					result.setSuccess(true);
					result.setMessage("OTP sent");
					result.setDetails("OTP sent");
					map.addAttribute("msg", result.getMessage());
					map.addAttribute("username", dto.getUsername());
					map.addAttribute("password", dto.getPassword());
				} else {
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setSuccess(false);
					result.setMessage("Enter valid Details");
					result.setDetails("Enter valid Details");
					map.addAttribute("msg", result.getMessage());
				}
			} else {
				map.put(ModelMapKey.ERROR, loginResponse.getMessage());
				return "Group/Login";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Group/VerifyMobile";
	}

	@RequestMapping(value = "/ResendDeviceBindingOTP", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> resendDeviceBindingOTP(@RequestBody LoginDTO dto, HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {
		ResponseDTO result = new ResponseDTO();
		try {
			logger.info("mobilenumber:: " + dto.getUsername());
			boolean verified = userApiImpl.resendMobileTokenDeviceBindingGroup(dto.getUsername());
			if (verified) {
				result.setStatus(ResponseStatus.SUCCESS.getKey());
				result.setCode(ResponseStatus.SUCCESS.getValue());
				result.setSuccess(true);
				result.setMessage("New OTP sent");
				result.setDetails("New OTP sent");
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setSuccess(false);
				result.setMessage("Enter valid Details");
				result.setDetails("Enter valid Details");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/SendGroupSms", method = RequestMethod.GET)
	public String sendGroupSmsGet(HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws JSONException {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && !sessionId.isEmpty()) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if (authority != null
					&& ((authority.contains(Authorities.GROUP) && authority.contains(Authorities.AUTHENTICATED)))) {
				logger.info("in send sms get");
				return "Group/SendGroupSms";
			}
		} else {
			return "Group/SendGroupSms";
		}
		return "Group/SendGroupSms";
	}

	@RequestMapping(value = "/SendGroupSms", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> sendNotificationToGroupUser(@RequestBody SendNotificationDTO dto,
			HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && !sessionId.isEmpty()) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if (authority != null
					&& ((authority.contains(Authorities.GROUP) && authority.contains(Authorities.AUTHENTICATED)))) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				MUser userInfo = userSession.getUser();
				try {
					logger.info("radio button value:: " + dto.getAllUser());
					logger.info("notify mobile no:: " + dto.getUserMobile());
					if (dto.getSingleUser() == null) {
						dto.setAllUser("YoAll");
					} else {
						dto.setAllUser("YoSingle");
					}
					if (dto.getAllUser().equals("YoSingle")) {
						MUser use = userApiImpl.findByUserName(dto.getUserMobile());
						if (use != null) {
							if (use.getGroupDetails().equals(userInfo.getGroupDetails())) {
								GroupSms single = new GroupSms();
								single.setCronStatus(false);
								single.setMobile(dto.getUserMobile());
								single.setText(dto.getMessage());
								single.setSingle(true);
								single.setStatus(Status.Inactive.getValue());
								single.setGroupName(userInfo.getGroupDetails().getGroupName());
								groupSmsRepository.save(single);

								// userApiImpl.sendSingleGroupSMS(dto.getUserMobile(), dto.getMessage());
								result.setCode(ResponseStatus.SUCCESS.getValue());
								result.setStatus(ResponseStatus.SUCCESS);
								result.setSuccess(true);
								result.setMessage("Message Sent Successfully");
							}
						}
					} else if (dto.getAllUser().equals("YoAll")) {
						userApiImpl.sendGroupSMS(userInfo.getGroupDetails().getGroupName(), dto.getMessage());
						result.setStatus(ResponseStatus.SUCCESS);
						result.setCode(ResponseStatus.SUCCESS.getValue());
						result.setSuccess(true);
						result.setMessage("Message Sent Successfully");
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GrBulkCarFailList", method = RequestMethod.GET)
	public String getBulkCardFailList(HttpServletRequest request, HttpSession session, ModelMap model) {

		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && !sessionId.isEmpty()) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if (authority != null
					&& ((authority.contains(Authorities.GROUP) && authority.contains(Authorities.AUTHENTICATED)))) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				try {
					List<BulkCardAssignmentGroup> list = userApiImpl
							.getgroupBulkCarFailList(userSession.getUser().getUserDetail().getEmail());
					model.addAttribute("userData", list);
				} catch (Exception e) {
					e.printStackTrace();
				}
				return "Group/GBulkCarFailList";
			}
		}
		return "Group/Home";
	}

	@RequestMapping(value = "/GrBulkCarFailList", method = RequestMethod.POST)
	public String getBulkCardFailListPost(@ModelAttribute PagingDTO dto, HttpServletRequest request,
			HttpSession session, ModelMap model) {

		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && !sessionId.isEmpty()) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if (authority != null
					&& ((authority.contains(Authorities.GROUP) && authority.contains(Authorities.AUTHENTICATED)))) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				try {
					String daterange[] = null;
					UserDataRequestDTO lUserData = new UserDataRequestDTO();
					SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");

					if (dto.getReportrange() != null) {
						System.err.println(dto.getReportrange());
						String aaaaa = dto.getReportrange().substring(0, 10);
						String baaaa = dto.getReportrange().substring(13);
						Date from = sdformat.parse(aaaaa + " 00:00:00");
						Date to = sdformat.parse(baaaa + " 00:00:00");
						lUserData.setFrom(from);
						lUserData.setTo(to);
					} else {
						daterange = CommonUtil.getDefaultDateRange();
						Date f = df2.parse(daterange[0]);
						Date t = df2.parse(daterange[1]);
						lUserData.setFrom(f);
						lUserData.setTo(t);
					}
					lUserData.setContactNo(userSession.getUser().getUserDetail().getContactNo());
					lUserData.setEmail(userSession.getUser().getUserDetail().getEmail());
					List<BulkCardAssignmentGroup> list = userApiImpl.getgroupBulkCarFailListPost(lUserData);
					model.addAttribute("userData", list);
				} catch (Exception e) {
					e.printStackTrace();
				}
				return "Group/GBulkCarFailList";
			}
		}
		return "Group/Home";
	}

	@RequestMapping(value = "/GrBulkRegFailList", method = RequestMethod.GET)
	public String getBulkRegisterFailList(HttpServletRequest request, HttpSession session, ModelMap model) {

		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && !sessionId.isEmpty()) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if (authority != null
					&& ((authority.contains(Authorities.GROUP) && authority.contains(Authorities.AUTHENTICATED)))) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				try {
					List<GroupBulkRegister> list = userApiImpl
							.getgroupBulkRegisterFailList(userSession.getUser().getUserDetail().getEmail());
					model.addAttribute("userData", list);
				} catch (Exception e) {
					e.printStackTrace();
				}
				return "Group/GBulkRegFailList";
			}
		}
		return "Group/Home";
	}

	@RequestMapping(value = "/GrBulkRegFailList", method = RequestMethod.POST)
	public String getBulkRegisterFailListPost(@ModelAttribute PagingDTO dto, HttpServletRequest request,
			HttpSession session, ModelMap model) {

		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && !sessionId.isEmpty()) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if (authority != null
					&& ((authority.contains(Authorities.GROUP) && authority.contains(Authorities.AUTHENTICATED)))) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				try {
					String daterange[] = null;
					UserDataRequestDTO lUserData = new UserDataRequestDTO();
					SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");

					if (dto.getReportrange() != null) {
						System.err.println(dto.getReportrange());
						String aaaaa = dto.getReportrange().substring(0, 10);
						String baaaa = dto.getReportrange().substring(13);
						Date from = sdformat.parse(aaaaa + " 00:00:00");
						Date to = sdformat.parse(baaaa + " 00:00:00");
						lUserData.setFrom(from);
						lUserData.setTo(to);
					} else {
						daterange = CommonUtil.getDefaultDateRange();
						Date f = df2.parse(daterange[0]);
						Date t = df2.parse(daterange[1]);
						lUserData.setFrom(f);
						lUserData.setTo(t);
					}
					lUserData.setContactNo(userSession.getUser().getUserDetail().getContactNo());
					lUserData.setEmail(userSession.getUser().getUserDetail().getEmail());
					List<GroupBulkRegister> list = userApiImpl.getgroupBulkRegisterFailListPost(lUserData);
					model.addAttribute("userData", list);
				} catch (Exception e) {
					e.printStackTrace();
				}
				return "Group/GBulkRegFailList";
			}
		}
		return "Group/Home";
	}

	@RequestMapping(value = "/SingleBulkRegFail", method = RequestMethod.POST)
	public String getSingleFailedUser(@ModelAttribute GroupDetailDTO dto, HttpServletRequest request,
			HttpSession session, ModelMap model) {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (authority != null) {
				if ((authority.contains(Authorities.GROUP) && authority.contains(Authorities.AUTHENTICATED))) {
					try {
						List<GroupBulkRegister> user = userApiImpl.getsingleuserfailbulkreg(dto.getMobileNoId(),
								userSession.getUser().getUserDetail().getEmail());
						model.addAttribute("userData", user);
					} catch (Exception e) {
						e.printStackTrace();
					}
					return "Group/GBulkRegFailList";
				}
			}
		}
		return "Group/Home";
	}

	@RequestMapping(value = "/GrBulkKycFailList", method = RequestMethod.GET)
	public String getBulkKycFailList(HttpServletRequest request, HttpSession session, ModelMap model) {

		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && !sessionId.isEmpty()) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if (authority != null
					&& ((authority.contains(Authorities.GROUP) && authority.contains(Authorities.AUTHENTICATED)))) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				try {
					List<GroupBulkKyc> list = userApiImpl
							.getgroupBulkKycFailList(userSession.getUser().getUserDetail().getEmail());
					model.addAttribute("userData", list);
				} catch (Exception e) {
					e.printStackTrace();
				}
				return "Group/BulkKycFailList";
			}
		}
		return "Group/Home";
	}

	@RequestMapping(value = "/GrBulkKycFailList", method = RequestMethod.POST)
	public String getBulkKycFailListPost(@ModelAttribute PagingDTO dto, HttpServletRequest request, HttpSession session,
			ModelMap model) {

		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && !sessionId.isEmpty()) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if (authority != null
					&& ((authority.contains(Authorities.GROUP) && authority.contains(Authorities.AUTHENTICATED)))) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				try {
					String daterange[] = null;
					UserDataRequestDTO lUserData = new UserDataRequestDTO();
					SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");

					if (dto.getReportrange() != null) {
						System.err.println(dto.getReportrange());
						String aaaaa = dto.getReportrange().substring(0, 10);
						String baaaa = dto.getReportrange().substring(13);
						Date from = sdformat.parse(aaaaa + " 00:00:00");
						Date to = sdformat.parse(baaaa + " 00:00:00");
						lUserData.setFrom(from);
						lUserData.setTo(to);
					} else {
						daterange = CommonUtil.getDefaultDateRange();
						Date f = df2.parse(daterange[0]);
						Date t = df2.parse(daterange[1]);
						lUserData.setFrom(f);
						lUserData.setTo(t);
					}
					lUserData.setContactNo(userSession.getUser().getUserDetail().getContactNo());
					lUserData.setEmail(userSession.getUser().getUserDetail().getEmail());
					List<GroupBulkKyc> list = userApiImpl.getgroupBulkLKycFailListPost(lUserData);
					model.addAttribute("userData", list);
				} catch (Exception e) {
					e.printStackTrace();
				}
				return "Group/BulkKycFailList";
			}
		}
		return "Group/Home";
	}

	@RequestMapping(value = "/SingleBulkKycFail", method = RequestMethod.POST)
	public String getSingleFailedKycUser(@ModelAttribute GroupDetailDTO dto, HttpServletRequest request,
			HttpSession session, ModelMap model) {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (authority != null) {
				if ((authority.contains(Authorities.GROUP) && authority.contains(Authorities.AUTHENTICATED))) {
					try {
						List<GroupBulkKyc> user = userApiImpl.getsingleuserfailbulkKyc(dto.getMobileNoId(),
								userSession.getUser().getUserDetail().getEmail());
						model.addAttribute("userData", user);
					} catch (Exception e) {
						e.printStackTrace();
					}
					return "Group/BulkKycFailList";
				}
			}
		}
		return "Group/Home";
	}

	@RequestMapping(value = "/GetAssignService", method = RequestMethod.GET)
	public ResponseEntity<ResponseDTO> getGroupServices(HttpServletRequest request, HttpSession session,
			ModelMap model) {
		ResponseDTO result = new ResponseDTO();
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && !sessionId.isEmpty()) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if (authority != null
					&& ((authority.contains(Authorities.GROUP) && authority.contains(Authorities.AUTHENTICATED)))) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				MUser userInfo = userSession.getUser();
				try {
					result = userApiImpl.getGroupServiceList(userInfo);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	// corporate module

	@RequestMapping(value = "/BulkRegister", method = RequestMethod.GET)
	public String getBulkRegister(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model) {
		List<MBulkRegister> lis = userApiImpl.findLast10Register();
		List<BulkRegisterDTO> cards = new ArrayList<>();
		for (int i = 0; i < lis.size(); i++) {
			BulkRegisterDTO car = new BulkRegisterDTO();
			car.setEmail(lis.get(i).getEmail());
			car.setName(lis.get(i).getName());
			car.setDateOfBirth(lis.get(i).getDateOfBirth() + "");
			car.setContactNo(lis.get(i).getContactNo());
			car.setRegistrationDate(lis.get(i).getCreated() + "");
			cards.add(car);
		}
		model.addAttribute("cardTransList", cards);
		return "Group/BulkRegister";
	}

	@RequestMapping(value = "/download/bulkregister", method = RequestMethod.GET)
	public void getDownloadBulkRegistationFile(HttpServletRequest request, HttpServletResponse res, HttpSession session)
			throws IOException {
		String contextPath = request.getRealPath("/");
		System.out.println(contextPath);
		PrintWriter out = res.getWriter();
		String fileName = "bulkregister.csv";
		String filePath = contextPath + "/WEB-INF/bulkuploadformat/";
		res.setContentType("APPLICATION/OCTET-STREAM");
		res.setHeader("Content-Disposition", "attachment;fileName=\"" + fileName + "\"");
		int i;
		FileInputStream file = new FileInputStream(filePath + fileName);
		while ((i = file.read()) != -1) {
			out.write(i);
		}
		file.close();
		out.close();
	}

	@RequestMapping(value = "/download/bulkcardassign", method = RequestMethod.GET)
	public void getDownloadBulkCardAssign(HttpServletRequest request, HttpServletResponse res, HttpSession session)
			throws IOException {
		String contextPath = request.getRealPath("/");
		System.out.println(contextPath);
		PrintWriter out = res.getWriter();
		String fileName = "bulkcardassign.csv";
		String filePath = contextPath + "/WEB-INF/bulkuploadformat/";
		res.setContentType("APPLICATION/OCTET-STREAM");
		res.setHeader("Content-Disposition", "attachment;fileName=\"" + fileName + "\"");
		int i;
		FileInputStream file = new FileInputStream(filePath + fileName);
		while ((i = file.read()) != -1) {
			out.write(i);
		}
		file.close();
		out.close();
	}

	@RequestMapping(method = RequestMethod.POST, value = "/BulkRegister")
	public String submitRequestRefund(@ModelAttribute RegisterDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) throws ParseException {

		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.GROUP)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionId);

					String rootDirectory = request.getSession().getServletContext().getRealPath("/");
					String path = saveRefundReport(rootDirectory, dto.getFile(), dto.getFileName());
					String file = path.substring(19);
					System.err.println(file);
					String[] splitted = file.split("#");
					System.err.println(splitted[1]);
					String fileName = StartUpUtil.CSV_FILE + file;
					if (fileName != null) {
						try {
							String s3Path = CommonUtil.uploadCsv(dto.getFile(), user.getUserId(), "BLKREGISTER");
							System.err.println("the s3 path::" + s3Path);
							System.err.println("the username::" + userSession.getUser().getUsername());
							GroupFileCatalogue fileCatalogue = new GroupFileCatalogue();
							fileCatalogue.setAbsPath(splitted[1]);
							fileCatalogue.setUser(userSession.getUser());
							fileCatalogue.setFileDescription(CSVReader.DESCRIPTION_BULKREGISTER);
							fileCatalogue.setS3Path(s3Path);
							fileCatalogue.setCategoryType("BLKREGISTER");
							groupFileCatalogueRepository.save(fileCatalogue);
							model.addAttribute("UserType", true);
							model.addAttribute("bulkRegistration", true);
							model.addAttribute("prefundC", true);
							model.addAttribute("BulkCL", true);
							model.addAttribute("BulkCardIssuance", true);
							model.addAttribute("SingleCardLoad", true);
							model.addAttribute("CardBlockUnblock", true);
							model.addAttribute("singleCardAssignment", true);

							model.addAttribute("username", userSession.getUser().getUsername());
							model.addAttribute("name", userSession.getUser().getUserDetail().getFirstName());

							model.addAttribute("sucessMSG",
									"Upload successful.Your Request for Bulk Registration has been taken.Please contact your admin for approval");
						} catch (Exception e) {
							e.printStackTrace();
						}
						return "Group/BulkRegister";
					}
				}
			}
		}
		return "redirect:/Group/Home";
	}

	@RequestMapping(value = "/BulkSMS", method = RequestMethod.GET)
	public String getBulkSms(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model) {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.GROUP)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					model.addAttribute("UserType", true);
					model.addAttribute("bulkRegistration", true);
					model.addAttribute("bulkSms", true);
					model.addAttribute("prefundC", true);
					model.addAttribute("BulkCL", true);
					model.addAttribute("BulkCardIssuance", true);
					model.addAttribute("SingleCardLoad", true);
					model.addAttribute("CardBlockUnblock", true);
					model.addAttribute("singleCardAssignment", true);

					model.addAttribute("username", userSession.getUser().getUsername());
					model.addAttribute("name", userSession.getUser().getUserDetail().getFirstName());
				}
			}
		}
		return "Group/BulkSms";
	}

	@RequestMapping(value = "/BulkSMS", method = RequestMethod.POST)
	public String sendBulkSMSRequest(@ModelAttribute RegisterDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) throws ParseException {
		try {
			String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
			if (sessionId != null && sessionId.length() != 0) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.GROUP)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						dto.setSessionId(sessionId);

						String rootDirectory = request.getSession().getServletContext().getRealPath("/");
						String path = saveRefundReport(rootDirectory, dto.getFile(), dto.getFileName());
						String file = path.substring(19);
						System.err.println(file);
						String[] splitted = file.split("#");
						System.err.println(splitted[1]);
						String fileName = StartUpUtil.CSV_FILE + file;
						if (fileName != null) {
							String s3Path = CommonUtil.uploadCsv(dto.getFile(), user.getUserId(), "BLKSMS");
							System.err.println("the s3 path::" + s3Path);
							System.err.println("the username::" + userSession.getUser().getUsername());
							GroupFileCatalogue fileCatalogue = new GroupFileCatalogue();
							fileCatalogue.setAbsPath(splitted[1]);
							fileCatalogue.setUser(userSession.getUser());
							fileCatalogue.setFileDescription(CSVReader.DESCRIPTION_BULKSMS);
							fileCatalogue.setS3Path(s3Path);
							fileCatalogue.setCategoryType("BLKSMS");
							groupFileCatalogueRepository.save(fileCatalogue);
							model.addAttribute("UserType", true);
							model.addAttribute("bulkRegistration", true);
							model.addAttribute("bulkSms", true);
							model.addAttribute("prefundC", true);
							model.addAttribute("BulkCL", true);
							model.addAttribute("BulkCardIssuance", true);
							model.addAttribute("SingleCardLoad", true);
							model.addAttribute("CardBlockUnblock", true);
							model.addAttribute("singleCardAssignment", true);

							model.addAttribute("username", userSession.getUser().getUsername());
							model.addAttribute("name", userSession.getUser().getUserDetail().getFirstName());

							model.addAttribute("sucessMSG",
									"Upload successful.Your Request for Bulk SMS has been taken.Please contact your admin for approval");
							return "Group/BulkSms";
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/Group/Home";
	}

	@RequestMapping(value = "/download/bulkSms", method = RequestMethod.GET)
	public void getDownloadBulkSmsFile(HttpServletRequest request, HttpServletResponse res, HttpSession session)
			throws IOException {
		String contextPath = request.getRealPath("/");
		System.out.println(contextPath);
		PrintWriter out = res.getWriter();
		String fileName = "bulksms.csv";
		String filePath = contextPath + "/WEB-INF/bulkuploadformat/";
		res.setContentType("APPLICATION/OCTET-STREAM");
		res.setHeader("Content-Disposition", "attachment;fileName=\"" + fileName + "\"");
		int i;
		FileInputStream file = new FileInputStream(filePath + fileName);
		while ((i = file.read()) != -1) {
			out.write(i);
		}
		file.close();
		out.close();
	}

	@RequestMapping(value = "/BulkUploadSmsList", method = RequestMethod.GET)
	public String fetchBulkUploadListSms(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.GROUP)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {

				List<GroupFileCatalogue> fileCatalogue = groupFileCatalogueRepository.getUnapprovedListSms();
				model.addAttribute("CatalogueList", fileCatalogue);

				return "Group/GroupSmsBulkList";
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "Group/Login";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "Group/Login";
		}
	}

	private String saveRefundReport(String rootDirectory, MultipartFile file, String code) {
		String contentType = file.getContentType();
		String[] fileExtension = contentType.split("/");
		String filePath = null;
		String fileName = String.valueOf(System.currentTimeMillis());
		File dirs = new File(rootDirectory + "/resources/register/" + fileName + "_" + file.getOriginalFilename());
		dirs.mkdirs();
		try {
			file.transferTo(dirs);
			filePath = "/resources/register/" + fileName + "_" + file.getOriginalFilename();
			return filePath + "#" + fileName + "_" + file.getOriginalFilename();
		} catch (IOException e) {
			e.printStackTrace();

		}
		return filePath;
	}

	// corporate bulk register
	@RequestMapping(method = RequestMethod.GET, value = "/CorporateBulkRegister")
	public String bulkRegster(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model, ModelMap map) throws ParseException {

		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.CORPORATE)
						|| user.getAuthority().contains(Authorities.GROUP)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					model.addAttribute("UserType", true);
					model.addAttribute("bulkRegistration", true);
					model.addAttribute("prefundC", true);
					model.addAttribute("BulkCL", true);
					model.addAttribute("BulkCardIssuance", true);
					model.addAttribute("SingleCardLoad", true);
					model.addAttribute("singleCardAssignment", true);
					model.addAttribute("CardBlockUnblock", true);
					model.addAttribute("username", userSession.getUser().getUsername());

					return "Group/CorporateBulkRegister";
				} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)
						|| user.getAuthority().contains(Authorities.GROUP)
								&& user.getAuthority().contains(Authorities.USER)) {
					System.err.println("i am in partner");
					PartnerDetails partnerDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
					if (partnerDetails != null) {
						List<MService> partnerServices = partnerDetails.getPartnerServices();
						if (partnerServices != null && !partnerServices.isEmpty()) {
							for (MService mService : partnerServices) {
								if (mService.getCode().equalsIgnoreCase("BRC")) {
									model.addAttribute("bulkRegistration", true);
								} else if (mService.getCode().equalsIgnoreCase("PREFC")) {
									model.addAttribute("prefundC", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCL")) {
									model.addAttribute("BulkCL", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCS")) {
									model.addAttribute("BulkCardIssuance", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCSL")) {
									model.addAttribute("SingleCardLoad", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCBL")) {
									model.addAttribute("CardBlockUnblock", true);
								} else if (mService.getCode().equalsIgnoreCase("CORSA")) {
									model.addAttribute("singleCardAssignment", true);

								}
							}
						}
					}
					model.addAttribute("UserType", false);
					model.addAttribute("username", userSession.getUser().getUsername());

					return "Group/CorporateBulkRegister";
				}
			}
		}
		return "redirect:/Group/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/CorporateBulkRegister")
	public String submitRequestRefundCorp(@ModelAttribute RegisterDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) throws ParseException {

		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.CORPORATE)
						|| user.getAuthority().contains(Authorities.GROUP)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionId);

					String rootDirectory = request.getSession().getServletContext().getRealPath("/");
					String path = saveRefundReport(rootDirectory, dto.getFile(), dto.getFileName());
					String file = path.substring(19);
					System.err.println(file);
					String[] splitted = file.split("#");
					System.err.println(splitted[1]);
					String fileName = StartUpUtil.CSV_FILE + file;
					if (fileName != null) {
						try {
							String s3Path = CommonUtil.uploadCsv(dto.getFile(), user.getUserId(), "BLKREGISTER");
							System.err.println("the s3 path::" + s3Path);
							CorporateFileCatalogue fileCatalogue = new CorporateFileCatalogue();
							fileCatalogue.setAbsPath(splitted[1]);
							fileCatalogue.setCorporate(userSession.getUser());
							fileCatalogue.setFileDescription(CSVReader.DESCRIPTION_BULKREGISTER);
							fileCatalogue.setS3Path(s3Path);
							fileCatalogue.setCategoryType("BLKREGISTER");
							corporateFileCatalogueRepository.save(fileCatalogue);
							model.addAttribute("UserType", true);
							model.addAttribute("bulkRegistration", true);
							model.addAttribute("prefundC", true);
							model.addAttribute("BulkCL", true);
							model.addAttribute("BulkCardIssuance", true);
							model.addAttribute("SingleCardLoad", true);
							model.addAttribute("CardBlockUnblock", true);
							model.addAttribute("singleCardAssignment", true);
							model.addAttribute("username", userSession.getUser().getUsername());
							model.addAttribute("sucessMSG",
									"Upload successful.Your Request for Bulk Registration has been taken.Please contact your admin for approval");
						} catch (Exception e) {
							model.addAttribute("errorMSG", "Upload failed due to an error while processing.");
							e.printStackTrace();
						}
						return "Group/CorporateBulkRegister";
					}
				} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)
						&& user.getAuthority().contains(Authorities.USER)) {
					dto.setSessionId(sessionId);
					String rootDirectory = request.getSession().getServletContext().getRealPath("/");
					String path = saveRefundReport(rootDirectory, dto.getFile(), dto.getFileName());
					String file = path.substring(19);
					System.err.println(file);
					String[] splitted = file.split("#");
					System.err.println(splitted[1]);
					String fileName = StartUpUtil.CSV_FILE + file;
					if (fileName != null) {
						PartnerDetails partnerDetails = partnerDetailsRepository
								.getPartnerDetails(userSession.getUser());
						String s3Path = CommonUtil.uploadCsv(dto.getFile(), user.getUserId(), "BLKREGISTER");
						CorporateFileCatalogue fileCatalogue = new CorporateFileCatalogue();
						fileCatalogue.setAbsPath(splitted[1]);
						fileCatalogue.setFileDescription(CSVReader.DESCRIPTION_BULKREGISTER);
						fileCatalogue.setS3Path(s3Path);
						if (partnerDetails != null) {
							fileCatalogue.setPartnerDetails(partnerDetails);
							fileCatalogue.setCorporate(partnerDetails.getCorporate().getCorporate());
						}
						fileCatalogue.setCategoryType("BLKREGISTER");
						corporateFileCatalogueRepository.save(fileCatalogue);
						model.addAttribute("sucessMSG",
								"Upload successful.Your Request for Bulk Registration has been taken.Please contact your admin for approval");
					}

					PartnerDetails partnerDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
					if (partnerDetails != null) {
						List<MService> partnerServices = partnerDetails.getPartnerServices();
						if (partnerServices != null && !partnerServices.isEmpty()) {
							for (MService mService : partnerServices) {
								if (mService.getCode().equalsIgnoreCase("BRC")) {
									model.addAttribute("bulkRegistration", true);
								} else if (mService.getCode().equalsIgnoreCase("PREFC")) {
									model.addAttribute("prefundC", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCL")) {
									model.addAttribute("BulkCL", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCS")) {
									model.addAttribute("BulkCardIssuance", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCSL")) {
									model.addAttribute("SingleCardLoad", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCBL")) {
									model.addAttribute("CardBlockUnblock", true);
								} else if (mService.getCode().equalsIgnoreCase("CORSA")) {
									model.addAttribute("singleCardAssignment", true);

								}
							}
						}
					}
					model.addAttribute("UserType", false);
					model.addAttribute("username", userSession.getUser().getUsername());
					return "Group/CorporateBulkRegister";

				}
			}
		}
		return "redirect:/Group/Home";

	}

	@RequestMapping(value = "/download/corpbulkregister", method = RequestMethod.GET)
	public void getDownloadBulkRegistationFileCorp(HttpServletRequest request, HttpServletResponse res,
			HttpSession session) throws IOException {
		String contextPath = request.getRealPath("/");
		System.out.println(contextPath);
		PrintWriter out = res.getWriter();
		String fileName = "bulkregisterCorp.csv";
		String filePath = contextPath + "/WEB-INF/bulkuploadformat/";
		res.setContentType("APPLICATION/OCTET-STREAM");
		res.setHeader("Content-Disposition", "attachment;fileName=\"" + fileName + "\"");
		int i;
		FileInputStream file = new FileInputStream(filePath + fileName);
		while ((i = file.read()) != -1) {
			out.write(i);
		}
		file.close();
		out.close();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/CorporateBulkCardLoad")
	public String bulkCardLoad(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model, ModelMap map) throws ParseException {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.CORPORATE)
						|| user.getAuthority().contains(Authorities.GROUP)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					model.addAttribute("UserType", true);
					model.addAttribute("bulkRegistration", true);
					model.addAttribute("prefundC", true);
					model.addAttribute("BulkCL", true);
					model.addAttribute("BulkCardIssuance", true);
					model.addAttribute("SingleCardLoad", true);
					model.addAttribute("CardBlockUnblock", true);
					model.addAttribute("singleCardAssignment", true);

					model.addAttribute("username", userSession.getUser().getUsername());

					return "Group/CorporateBulkCardCreation";
				} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)
						|| user.getAuthority().contains(Authorities.GROUP)
								&& user.getAuthority().contains(Authorities.USER)) {
					PartnerDetails partnerDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
					if (partnerDetails != null) {
						List<MService> partnerServices = partnerDetails.getPartnerServices();
						if (partnerServices != null && !partnerServices.isEmpty()) {
							for (MService mService : partnerServices) {
								if (mService.getCode().equalsIgnoreCase("BRC")) {
									model.addAttribute("bulkRegistration", true);
								} else if (mService.getCode().equalsIgnoreCase("PREFC")) {
									model.addAttribute("prefundC", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCL")) {
									model.addAttribute("BulkCL", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCS")) {
									model.addAttribute("BulkCardIssuance", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCSL")) {
									model.addAttribute("SingleCardLoad", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCBL")) {
									model.addAttribute("CardBlockUnblock", true);
								} else if (mService.getCode().equalsIgnoreCase("CORSA")) {
									model.addAttribute("singleCardAssignment", true);

								}
							}
						}
					}
					model.addAttribute("UserType", false);
					model.addAttribute("username", userSession.getUser().getUsername());

					return "Group/CorporateBulkCardCreation";

				}
			}
		}
		return "redirect:/Group/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/CorporateBulkCardLoad")
	public String bulkCardLoad(@ModelAttribute RegisterDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) throws ParseException {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.CORPORATE)
						|| user.getAuthority().contains(Authorities.GROUP)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {

					dto.setSessionId(sessionId);

					String rootDirectory = request.getSession().getServletContext().getRealPath("/");
					String path = saveRefundReport(rootDirectory, dto.getFile(), dto.getFileName());
					String file = path.substring(19);

					// String fileName = StartUpUtil.CSV_FILE+file;
					System.err.println(file);
					String[] splitted = file.split("#");
					System.err.println(splitted[1]);
					String fileName = StartUpUtil.CSV_FILE + file;
					if (fileName != null) {
						String s3Path = CommonUtil.uploadCsv(dto.getFile(), user.getUserId(), "BLKCARDLOAD");
						CorporateFileCatalogue fileCatalogue = new CorporateFileCatalogue();
						fileCatalogue.setAbsPath(splitted[1]);
						fileCatalogue.setCorporate(userSession.getUser());
						fileCatalogue.setFileDescription(CSVReader.DESCRIPTION_BULKCARDLOAD);
						fileCatalogue.setS3Path(s3Path);
						fileCatalogue.setCategoryType("BLKCARDLOAD");

						corporateFileCatalogueRepository.save(fileCatalogue);
						model.addAttribute("UserType", true);
						model.addAttribute("bulkRegistration", true);
						model.addAttribute("prefundC", true);
						model.addAttribute("BulkCL", true);
						model.addAttribute("BulkCardIssuance", true);
						model.addAttribute("SingleCardLoad", true);
						model.addAttribute("singleCardAssignment", true);

						model.addAttribute("CardBlockUnblock", true);
						model.addAttribute("username", userSession.getUser().getUsername());

						model.addAttribute("sucessMSG",
								"Upload successful.Your Request for Bulk Registration has been taken.Please contact your admin for approval");
						return "Group/CorporateBulkCardCreation";
					}
				} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)
						&& user.getAuthority().contains(Authorities.USER)) {

					dto.setSessionId(sessionId);

					String rootDirectory = request.getSession().getServletContext().getRealPath("/");
					String path = saveRefundReport(rootDirectory, dto.getFile(), dto.getFileName());
					String file = path.substring(19);

					// String fileName = StartUpUtil.CSV_FILE+file;
					System.err.println(file);
					String[] splitted = file.split("#");
					System.err.println(splitted[1]);
					String fileName = StartUpUtil.CSV_FILE + file;
					if (fileName != null) {
						String s3Path = CommonUtil.uploadCsv(dto.getFile(), user.getUserId(), "BLKCARDLOAD");
						CorporateFileCatalogue fileCatalogue = new CorporateFileCatalogue();
						fileCatalogue.setAbsPath(splitted[1]);
						fileCatalogue.setCorporate(userSession.getUser());
						fileCatalogue.setFileDescription(CSVReader.DESCRIPTION_BULKCARDLOAD);
						fileCatalogue.setS3Path(s3Path);
						fileCatalogue.setCategoryType("BLKCARDLOAD");

						corporateFileCatalogueRepository.save(fileCatalogue);
						model.addAttribute("sucessMSG",
								"Upload successful.Your Request for Bulk Registration has been taken.Please contact your admin for approval");
					}

					PartnerDetails partnerDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
					if (partnerDetails != null) {
						List<MService> partnerServices = partnerDetails.getPartnerServices();
						if (partnerServices != null && !partnerServices.isEmpty()) {
							for (MService mService : partnerServices) {
								if (mService.getCode().equalsIgnoreCase("BRC")) {
									model.addAttribute("bulkRegistration", true);
								} else if (mService.getCode().equalsIgnoreCase("PREFC")) {
									model.addAttribute("prefundC", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCL")) {
									model.addAttribute("BulkCL", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCS")) {
									model.addAttribute("BulkCardIssuance", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCSL")) {
									model.addAttribute("SingleCardLoad", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCBL")) {
									model.addAttribute("CardBlockUnblock", true);
								} else if (mService.getCode().equalsIgnoreCase("CORSA")) {
									model.addAttribute("singleCardAssignment", true);

								}
							}
						}
					}

					model.addAttribute("UserType", false);
					model.addAttribute("username", userSession.getUser().getUsername());

					return "Group/CorporateBulkCardCreation";

				}
			}
		}
		return "redirect:/Group/Home";

	}

	@RequestMapping(value = "/download/bulktransfer", method = RequestMethod.GET)
	public void getDownloadBulkTransferFile(HttpServletRequest request, HttpServletResponse res, HttpSession session)
			throws IOException {
		String contextPath = request.getRealPath("/");
		System.out.println(contextPath);
		PrintWriter out = res.getWriter();
		String fileName = "bulktransfer.csv";
		String filePath = contextPath + "/WEB-INF/bulkuploadformat/";
		res.setContentType("APPLICATION/OCTET-STREAM");
		res.setHeader("Content-Disposition", "attachment;fileName=\"" + fileName + "\"");
		int i;
		FileInputStream file = new FileInputStream(filePath + fileName);
		while ((i = file.read()) != -1) {
			out.write(i);
		}
		file.close();
		out.close();
	}

	@RequestMapping(value = "/download/bulkKyc", method = RequestMethod.GET)
	public void getDownloadBulkKycFile(HttpServletRequest request, HttpServletResponse res, HttpSession session)
			throws IOException {
		String contextPath = request.getRealPath("/");
		System.out.println(contextPath);
		PrintWriter out = res.getWriter();
		String fileName = "bulkkyc.csv";
		String filePath = contextPath + "/WEB-INF/bulkuploadformat/";
		res.setContentType("APPLICATION/OCTET-STREAM");
		res.setHeader("Content-Disposition", "attachment;fileName=\"" + fileName + "\"");
		int i;
		FileInputStream file = new FileInputStream(filePath + fileName);
		while ((i = file.read()) != -1) {
			out.write(i);
		}
		file.close();
		out.close();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/GroupBulkKYCApproval")
	public String bulkKycApproval(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model, ModelMap map) throws ParseException {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.CORPORATE)
						|| user.getAuthority().contains(Authorities.GROUP)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					model.addAttribute("UserType", true);
					model.addAttribute("bulkRegistration", true);
					model.addAttribute("prefundC", true);
					model.addAttribute("BulkCL", true);
					model.addAttribute("BulkCardIssuance", true);
					model.addAttribute("SingleCardLoad", true);
					model.addAttribute("CardBlockUnblock", true);
					model.addAttribute("singleCardAssignment", true);

					model.addAttribute("username", userSession.getUser().getUsername());

					return "Group/CorporateBulkKYCApproval";
				} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)
						&& user.getAuthority().contains(Authorities.USER)) {
					PartnerDetails partnerDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
					if (partnerDetails != null) {
						List<MService> partnerServices = partnerDetails.getPartnerServices();
						if (partnerServices != null && !partnerServices.isEmpty()) {
							for (MService mService : partnerServices) {
								if (mService.getCode().equalsIgnoreCase("BRC")) {
									model.addAttribute("bulkRegistration", true);
								} else if (mService.getCode().equalsIgnoreCase("PREFC")) {
									model.addAttribute("prefundC", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCL")) {
									model.addAttribute("BulkCL", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCS")) {
									model.addAttribute("BulkCardIssuance", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCSL")) {
									model.addAttribute("SingleCardLoad", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCBL")) {
									model.addAttribute("CardBlockUnblock", true);
								} else if (mService.getCode().equalsIgnoreCase("CORSA")) {
									model.addAttribute("singleCardAssignment", true);

								}
							}
						}
					}
					model.addAttribute("UserType", false);
					model.addAttribute("username", userSession.getUser().getUsername());

					return "Group/CorporateBulkKYCApproval";

				}
			}
		}
		return "redirect:/Group/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/GroupBulkKYCApproval")
	public String bulkKYCUpload(@ModelAttribute RegisterDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) throws ParseException {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.CORPORATE)
						|| user.getAuthority().contains(Authorities.GROUP)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {

					dto.setSessionId(sessionId);

					String rootDirectory = request.getSession().getServletContext().getRealPath("/");
					String path = saveRefundReport(rootDirectory, dto.getFile(), dto.getFileName());
					String file = path.substring(19);

					// String fileName = StartUpUtil.CSV_FILE+file;
					System.err.println(file);
					String[] splitted = file.split("#");
					System.err.println(splitted[1]);
					String fileName = StartUpUtil.CSV_FILE + file;
					if (fileName != null) {
						String s3Path = CommonUtil.uploadCsv(dto.getFile(), user.getUserId(), "BLKKYCUPLOAD");
						GroupFileCatalogue fileCatalogue = new GroupFileCatalogue();
						fileCatalogue.setAbsPath(splitted[1]);
						fileCatalogue.setUser(userSession.getUser());
						fileCatalogue.setFileDescription(CSVReader.DESCRIPTION_BULKKYC);
						fileCatalogue.setS3Path(s3Path);
						fileCatalogue.setCategoryType("BLKKYCUPLOAD");

						groupFileCatalogueRepository.save(fileCatalogue);
						model.addAttribute("UserType", true);
						model.addAttribute("bulkRegistration", true);
						model.addAttribute("prefundC", true);
						model.addAttribute("BulkCL", true);
						model.addAttribute("BulkCardIssuance", true);
						model.addAttribute("SingleCardLoad", true);
						model.addAttribute("singleCardAssignment", true);

						model.addAttribute("CardBlockUnblock", true);
						model.addAttribute("username", userSession.getUser().getUsername());

						model.addAttribute("sucessMSG",
								"Upload successful.Your Request for Bulk KYC has been taken.Please contact your admin for approval");
						return "Group/CorporateBulkKYCApproval";
					}
				} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)
						&& user.getAuthority().contains(Authorities.USER)) {

					dto.setSessionId(sessionId);

					String rootDirectory = request.getSession().getServletContext().getRealPath("/");
					String path = saveRefundReport(rootDirectory, dto.getFile(), dto.getFileName());
					String file = path.substring(19);

					// String fileName = StartUpUtil.CSV_FILE+file;
					System.err.println(file);
					String[] splitted = file.split("#");
					System.err.println(splitted[1]);
					String fileName = StartUpUtil.CSV_FILE + file;
					if (fileName != null) {
						String s3Path = CommonUtil.uploadCsv(dto.getFile(), user.getUserId(), "BLKKYCUPLOAD");
						GroupFileCatalogue fileCatalogue = new GroupFileCatalogue();
						fileCatalogue.setAbsPath(splitted[1]);
						fileCatalogue.setUser(userSession.getUser());
						fileCatalogue.setFileDescription(CSVReader.DESCRIPTION_BULKKYC);
						fileCatalogue.setS3Path(s3Path);
						fileCatalogue.setCategoryType("BLKKYCUPLOAD");

						groupFileCatalogueRepository.save(fileCatalogue);
						model.addAttribute("sucessMSG",
								"Upload successful.Your Request for Bulk Kyc has been taken.Please contact your admin for approval");
					}

					model.addAttribute("UserType", false);
					model.addAttribute("username", userSession.getUser().getUsername());

					return "Group/CorporateBulkKYCApproval";

				}
			}
		}
		return "redirect:/Group/Home";

	}

	@RequestMapping(method = RequestMethod.GET, value = "/CorporateSinleCardLoad")
	public String getCorporateSinleCardLoad(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE) || user.getAuthority().contains(Authorities.GROUP)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("singleCardAssignment", true);

				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("username", userSession.getUser().getUsername());

				return "Group/CorporateSinleCardLoad";
			} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)
					&& user.getAuthority().contains(Authorities.USER)) {
				PartnerDetails partnerDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
				if (partnerDetails != null) {
					List<MService> partnerServices = partnerDetails.getPartnerServices();
					if (partnerServices != null && !partnerServices.isEmpty()) {
						for (MService mService : partnerServices) {
							if (mService.getCode().equalsIgnoreCase("BRC")) {
								model.addAttribute("bulkRegistration", true);
							} else if (mService.getCode().equalsIgnoreCase("PREFC")) {
								model.addAttribute("prefundC", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCL")) {
								model.addAttribute("BulkCL", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCS")) {
								model.addAttribute("BulkCardIssuance", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCSL")) {
								model.addAttribute("SingleCardLoad", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCBL")) {
								model.addAttribute("CardBlockUnblock", true);
							} else if (mService.getCode().equalsIgnoreCase("CORSA")) {
								model.addAttribute("singleCardAssignment", true);

							}
						}
					}
				}
				model.addAttribute("UserType", false);
				model.addAttribute("username", userSession.getUser().getUsername());

				return "Group/CorporateSinleCardLoad";
			}
		}
		return "redirect:/Group/Home";
	}

	/*
	 * @RequestMapping(value="/LoadCard",method=RequestMethod.POST) public String
	 * LoadCard(@ModelAttribute RequestDTO cardRequest, HttpServletRequest
	 * request,HttpServletResponse response,HttpSession session,ModelMap map,Model
	 * model) { try{ String sessionId = (String)
	 * session.getAttribute("corporateSessionId"); UserSession userSession =
	 * userSessionRepository.findByActiveSessionId(sessionId); if (userSession !=
	 * null) { UserDTO user = userApi.getUserById(userSession.getUser().getId()); if
	 * (user.getAuthority().contains(Authorities.CORPORATE) &&
	 * user.getAuthority().contains(Authorities.AUTHENTICATED)) {
	 * model.addAttribute("UserType", true);
	 * model.addAttribute("bulkRegistration",true); model.addAttribute("prefundC",
	 * true); model.addAttribute("BulkCL", true);
	 * model.addAttribute("BulkCardIssuance", true);
	 * model.addAttribute("SingleCardLoad", true);
	 * model.addAttribute("CardBlockUnblock", true);
	 * model.addAttribute("singleCardAssignment", true);
	 * 
	 * model.addAttribute("username", userSession.getUser().getUsername());
	 * 
	 * 
	 * MUser agent=userApi.findByUserName(user.getUsername());
	 * if(agent.getAccountDetail().getBalance()>Long.valueOf(cardRequest.getAmount()
	 * )){ // persistingSessionRegistry.refreshLastRequest(sessionId); MUser
	 * cardUser=userApi.findByUserName(cardRequest.getContactNo());
	 * if(cardUser!=null){ MMCards Physical=null; MMCards virtual=null;
	 * WalletResponse cardTransferResposne=null; ResponseDTO resp=new ResponseDTO();
	 * MService service=mServiceRepository.findServiceByCode("BRCSL");
	 * TransactionError
	 * transactionError=loadMoneyValidation.validateLoadCardTransaction(cardRequest.
	 * getAmount(), cardRequest.getContactNo(), service);
	 * if(transactionError.isValid()){ resp=
	 * transactionApi.initiateLoadCardTransactionCorporate(cardRequest,userSession.
	 * getUser(),service); if(resp.getCode().equalsIgnoreCase("S00")){
	 * WalletResponse
	 * walletRespon=matchMoveApi.initiateLoadFundsToMMWallet(cardUser,
	 * cardRequest.getAmount()); if(walletRespon.getCode().equalsIgnoreCase("S00")){
	 * Physical=cardRepository.getPhysicalCardByUser(cardUser); if(Physical!=null &&
	 * Physical.getStatus().equalsIgnoreCase("Active")){
	 * cardTransferResposne=matchMoveApi.transferFundsToMMCard(cardUser,cardRequest.
	 * getAmount(),Physical.getCardId());
	 * if(cardTransferResposne.getCode().equalsIgnoreCase("S00")){
	 * System.err.println("In S00"); BulkLoadMoney card=new BulkLoadMoney();
	 * card.setAmount(cardRequest.getAmount()); card.setCardLoadError(false);
	 * card.setCorporate(agent); card.setDriver_id(cardRequest.getDriver_id());
	 * card.setDateOfTransaction(resp.getDate());
	 * card.setEmail(cardUser.getUserDetail().getEmail());
	 * card.setMobile(cardUser.getUsername()); card.setSchedulerStatus(true);
	 * card.setTransactionStatus(true);
	 * transactionApi.updateLoadCardByCorporate(resp.getTxnId(),Status.Success.
	 * getValue());
	 * System.err.println("Transaction Updates:::"+cardTransferResposne.getCode());
	 * //MTransaction succTrx=transactionApi.getTransactionByRefNo(resp.getTxnId());
	 * bulkLoadMoneyRepository.save(card); map.put("succMsg",
	 * "Transaction succesful, Amount loaded into the card.");
	 * 
	 * }else{
	 * transactionApi.updateLoadCardByCorporate(resp.getTxnId(),Status.Failed.
	 * getValue()); map.put("errorMsg",
	 * "Transaction failed,Please try again later.Transfer to card Failed"); }
	 * }else{
	 * transactionApi.updateLoadCardByCorporate(resp.getTxnId(),Status.Failed.
	 * getValue()); map.put("errorMsg",
	 * "Transaction failed,Please try again later.Card is not Active"); }
	 * 
	 * } else {
	 * transactionApi.updateLoadCardByCorporate(resp.getTxnId(),Status.Failed.
	 * getValue()); map.put("errorMsg",
	 * "Transaction failed,Please try again later.Wallet Transfer Failed"); } }else{
	 * transactionApi.updateLoadCardByCorporate(resp.getTxnId(),Status.Failed.
	 * getValue());
	 * 
	 * map.put("errorMsg",
	 * "Transaction failed,Please try again later.Fund initiation failed"); } }else{
	 * map.put("errorMsg", transactionError.getMessage() ); } }else{
	 * map.put("errorMsg", "User does not exist."); } }else{ map.put("errorMsg",
	 * "Insufficient balance available balance is Rs "+agent.getAccountDetail().
	 * getBalance()); } return "Group/CorporateSinleCardLoad"; }else
	 * if(user.getAuthority().contains(Authorities.CORPORATE_PARTNER) &&
	 * user.getAuthority().contains(Authorities.USER)) { PartnerDetails
	 * partnerDetails=partnerDetailsRepository.getPartnerDetails(userSession.getUser
	 * ()); if(partnerDetails!=null){ List<MService>
	 * partnerServices=partnerDetails.getPartnerServices(); if(partnerServices!=null
	 * && !partnerServices.isEmpty()){ for (MService mService : partnerServices) {
	 * if(mService.getCode().equalsIgnoreCase("BRC")){
	 * model.addAttribute("bulkRegistration",true); } else
	 * if(mService.getCode().equalsIgnoreCase("PREFC")){
	 * model.addAttribute("prefundC", true); } else
	 * if(mService.getCode().equalsIgnoreCase("BRCL")){ model.addAttribute("BulkCL",
	 * true); } else if(mService.getCode().equalsIgnoreCase("BRCS")){
	 * model.addAttribute("BulkCardIssuance", true); } else
	 * if(mService.getCode().equalsIgnoreCase("BRCSL")){
	 * model.addAttribute("SingleCardLoad", true); } else
	 * if(mService.getCode().equalsIgnoreCase("BRCBL")){
	 * model.addAttribute("CardBlockUnblock", true); } else
	 * if(mService.getCode().equalsIgnoreCase("CORSA")){
	 * model.addAttribute("singleCardAssignment", true);
	 * 
	 * } } } } model.addAttribute("UserType", false); model.addAttribute("username",
	 * userSession.getUser().getUsername());
	 * 
	 * 
	 * MUser agent=userApi.findByUserName(user.getUsername()); if
	 * (agent.getAccountDetail().getBalance() >
	 * Long.valueOf(cardRequest.getAmount())) { //
	 * persistingSessionRegistry.refreshLastRequest(sessionId); MUser cardUser =
	 * userApi.findByUserName(cardRequest.getContactNo()); if (cardUser != null) {
	 * MMCards Physical = null; WalletResponse cardTransferResposne = null;
	 * ResponseDTO resp = new ResponseDTO(); MService service =
	 * mServiceRepository.findServiceByCode("BRCSL"); TransactionError
	 * transactionError = loadMoneyValidation.validateLoadCardTransaction(
	 * cardRequest.getAmount(), cardRequest.getContactNo(), service);
	 * transactionError.setMessage(loadMoneyValidation.CorporateLoadCardMaxLimit(
	 * cardRequest.getAmount(), partnerDetails.getLoadCardMaxLimit(),
	 * userSession.getUser().getAccountDetail())); if (transactionError.isValid() &&
	 * transactionError.getMessage() == null ) { resp =
	 * transactionApi.initiateLoadCardTransactionCorporate(cardRequest,
	 * userSession.getUser(), service); if (resp.getCode().equalsIgnoreCase("S00"))
	 * { WalletResponse walletRespon =
	 * matchMoveApi.initiateLoadFundsToMMWallet(cardUser, cardRequest.getAmount());
	 * if (walletRespon.getCode().equalsIgnoreCase("S00")) { Physical =
	 * cardRepository.getPhysicalCardByUser(cardUser); if (Physical != null &&
	 * Physical.getStatus().equalsIgnoreCase("Active")) { cardTransferResposne =
	 * matchMoveApi.transferFundsToMMCard(cardUser, cardRequest.getAmount(),
	 * Physical.getCardId()); if
	 * (cardTransferResposne.getCode().equalsIgnoreCase("S00")) { BulkLoadMoney card
	 * = new BulkLoadMoney(); card.setAmount(cardRequest.getAmount());
	 * card.setCardLoadError(false);
	 * card.setCorporate(partnerDetails.getCorporate().getCorporate());
	 * card.setPartnerDetails(partnerDetails);
	 * card.setDriver_id(cardRequest.getDriver_id());
	 * card.setDateOfTransaction(resp.getDate());
	 * card.setEmail(cardUser.getUserDetail().getEmail());
	 * card.setMobile(cardUser.getUsername()); card.setSchedulerStatus(true);
	 * card.setTransactionStatus(true); bulkLoadMoneyRepository.save(card);
	 * 
	 * transactionApi.updateLoadCardByCorporate(resp.getTxnId(),
	 * Status.Success.getValue()); // MTransaction //
	 * succTrx=transactionApi.getTransactionByRefNo(resp.getTxnId());
	 * 
	 * map.put("succMsg", "Transaction succesful, Amount loaded into the card.");
	 * 
	 * } else { transactionApi.updateLoadCardByCorporate(resp.getTxnId(),
	 * Status.Failed.getValue()); map.put("errorMsg",
	 * "Transaction failed,Please try again later.Transfer to card Failed"); } }
	 * else { transactionApi.updateLoadCardByCorporate(resp.getTxnId(),
	 * Status.Failed.getValue()); map.put("errorMsg",
	 * "Transaction failed,Please try again later.Card is not Active"); }
	 * 
	 * } else { transactionApi.updateLoadCardByCorporate(resp.getTxnId(),
	 * Status.Failed.getValue()); map.put("errorMsg",
	 * "Transaction failed,Please try again later.Wallet Transfer Failed"); } } else
	 * { transactionApi.updateLoadCardByCorporate(resp.getTxnId(),
	 * Status.Failed.getValue());
	 * 
	 * map.put("errorMsg",
	 * "Transaction failed,Please try again later.Fund initiation failed"); } } else
	 * { map.put("errorMsg", transactionError.getMessage()); } } else {
	 * map.put("errorMsg", "User does not exist."); } }else{ map.put("errorMsg",
	 * "Insufficient balance available balance is Rs "+agent.getAccountDetail().
	 * getBalance()); }
	 * 
	 * }
	 * 
	 * 
	 * else{ return "redirect:/Group/Home"; } }else{ return "redirect:/Group/Home";
	 * } }catch(Exception e){ e.printStackTrace(); map.put("errorMsg",
	 * "Transaction failed,Please try again later."); return
	 * "Corporate/CorporateSinleCardLoad"; } return "Group/CorporateSinleCardLoad";
	 * }
	 */

	@RequestMapping(method = RequestMethod.GET, value = "/CorporateSingleCardAssignment")
	public String getCorporateSingleCardAssignment(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE) || user.getAuthority().contains(Authorities.GROUP)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);

				model.addAttribute("username", userSession.getUser().getUsername());

				return "Group/CorporateSingleCardAssignment";
			} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)
					&& user.getAuthority().contains(Authorities.USER)) {
				PartnerDetails partnerDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
				if (partnerDetails != null) {
					List<MService> partnerServices = partnerDetails.getPartnerServices();
					if (partnerServices != null && !partnerServices.isEmpty()) {
						for (MService mService : partnerServices) {
							if (mService.getCode().equalsIgnoreCase("BRC")) {
								model.addAttribute("bulkRegistration", true);
							} else if (mService.getCode().equalsIgnoreCase("PREFC")) {
								model.addAttribute("prefundC", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCL")) {
								model.addAttribute("BulkCL", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCS")) {
								model.addAttribute("BulkCardIssuance", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCSL")) {
								model.addAttribute("SingleCardLoad", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCBL")) {
								model.addAttribute("CardBlockUnblock", true);
							} else if (mService.getCode().equalsIgnoreCase("CORSA")) {
								model.addAttribute("singleCardAssignment", true);

							}
						}
					}
				}
				model.addAttribute("UserType", false);
				model.addAttribute("username", userSession.getUser().getUsername());

				return "Group/CorporateSingleCardAssignment";
			}

		}
		return "redirect:/Group/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/AddSingleUser")
	public String addUser(ModelMap modelMap, @ModelAttribute RegisterDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap map, Model model) {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
			MUser agent = userApiImpl.findByUserName(user.getUsername());
			if (user.getAuthority().contains(Authorities.CORPORATE) || user.getAuthority().contains(Authorities.GROUP)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("singleCardAssignment", true);

				model.addAttribute("username", userSession.getUser().getUsername());

				model.addAttribute("CardBlockUnblock", true);

				dto.setUsername(dto.getContactNo());
				RegisterError registerError = registerValidation.validateNormalUser(dto);
				if (registerError.isValid()) {
					System.err.println(dto);
					dto.setUserType(UserType.User);
					try {
						boolean val = userApiImpl.saveUserFromCorporate(dto);
						if (val) {
							BulkRegister bulkRegister = new BulkRegister();
							if (!dto.isPhysicalCard()) {
								MUser user1 = userApiImpl.findByUserName(dto.getContactNo());
								ResponseDTO resp = matchMoveApi.createUserOnMM(user1);
								if (resp.getCode().equalsIgnoreCase("S00")) {
									bulkRegister.setUserCreationStatus(true);
									bulkRegister.setWalletCreationStatus(true);
									WalletResponse walletResponse = matchMoveApi.assignVirtualCard(user1);
									// matchMoveApi.tempKycUserMM(user1.getUserDetail().getEmail(),user1.getUsername());
									// matchMoveApi.tempSetIdDetails(user1.getUserDetail().getEmail(),user1.getUsername());
									// matchMoveApi.tempSetImagesForKyc(user1.getUserDetail().getEmail(),user1.getUsername());
									// matchMoveApi.tempConfirmKyc(user1.getUserDetail().getEmail(),user1.getUsername());
									// matchMoveApi.tempKycStatusApproval(user1.getUserDetail().getEmail(),
									// user1.getUsername());
									if (walletResponse.getCode().equalsIgnoreCase("S00")) {
										map.put("regmessage", "User added Sucessfully and virtual card assigned. ");
										CorporateAgentDetails corpDetails = corporateAgentDetailsRepository
												.getByCorporateId(agent);
										MUser uuu = userApiImpl.findByUserName(dto.getContactNo());
										bulkRegister.setCardNo("");
										bulkRegister.setDob(dto.getDateOfBirth());
										bulkRegister.setEmail(dto.getEmail());
										bulkRegister.setMobile(dto.getContactNo());
										bulkRegister.setProxyNo("");
										bulkRegister.setUser(uuu);
										bulkRegister.setDriver_id(dto.getDriver_id());

										bulkRegister.setAgentDetails(corpDetails);
										bulkRegister.setName(dto.getFirstName() + " " + dto.getLastName());
										bulkRegister.setCardCreationStatus(true);
										bulkRegister.setPhyCardActivationStatus(false);
										bulkRegister.setPhyCardActivationStatus(false);
										bulkRegisterRepository.save(bulkRegister);
									} else {
										map.put("regmessage", "User added Sucessfully but virtual card not assigned. ");
										CorporateAgentDetails corpDetails = corporateAgentDetailsRepository
												.getByCorporateId(agent);
										MUser uuu = userApiImpl.findByUserName(dto.getContactNo());
										bulkRegister.setCardNo("");
										bulkRegister.setDob(dto.getDateOfBirth());
										bulkRegister.setEmail(dto.getEmail());
										bulkRegister.setMobile(dto.getContactNo());
										bulkRegister.setProxyNo("");
										bulkRegister.setUser(uuu);
										bulkRegister.setDriver_id(dto.getDriver_id());

										bulkRegister.setAgentDetails(corpDetails);
										bulkRegister.setName(dto.getFirstName() + " " + dto.getLastName());
										bulkRegister.setCardCreationStatus(false);
										bulkRegister.setPhyCardActivationStatus(false);
										bulkRegister.setPhyCardActivationStatus(false);
										bulkRegisterRepository.save(bulkRegister);
									}
								} else {
									map.put("regmessage",
											"User added Sucessfully but account not created in matchmove global.");
								}
							} else {

								MUser user1 = userApiImpl.findByUserName(dto.getContactNo());
								ResponseDTO resp = matchMoveApi.createUserOnMM(user1);
								if (resp.getCode().equalsIgnoreCase("S00")) {
									bulkRegister.setUserCreationStatus(true);
									bulkRegister.setWalletCreationStatus(true);

									WalletResponse walletResponse = new WalletResponse();
									MatchMoveWallet wallet = matchMoveWalletRepository.findByUser(user1);
									if (wallet != null) {
										PhysicalCardDetails physicalCard = new PhysicalCardDetails();
										physicalCard.setFromAdmin(true);
										physicalCard.setWallet(wallet);
										physicalCardDetailRepository.save(physicalCard);
										walletResponse = matchMoveApi.assignPhysicalCard(user1, dto.getProxyNumber());
										if ("S00".equalsIgnoreCase(walletResponse.getCode())) {
											PhysicalCardDetails card = physicalCardDetailRepository.findByUser(user1);
											card.setStatus(Status.Received);
											physicalCardDetailRepository.save(card);
											ResponseDTO respo = matchMoveApi
													.activationPhysicalCard(card.getActivationCode(), user1);
											if (respo.getCode().equalsIgnoreCase("S00")) {
												map.put("sucMsg", "Physical card assigned.");
												PhysicalCardDetails card1 = physicalCardDetailRepository
														.findByUser(user1);
												card.setStatus(Status.Active);
												physicalCardDetailRepository.save(card1);
												CorporateAgentDetails corpDetails = corporateAgentDetailsRepository
														.getByCorporateId(agent);
												MUser uuu = userApiImpl.findByUserName(dto.getContactNo());
												bulkRegister.setCardNo("");
												bulkRegister.setDob(dto.getDateOfBirth());
												bulkRegister.setEmail(dto.getEmail());
												bulkRegister.setMobile(dto.getContactNo());
												bulkRegister.setProxyNo(dto.getProxyNumber());
												bulkRegister.setDriver_id(dto.getDriver_id());
												bulkRegister.setUser(uuu);
												bulkRegister.setAgentDetails(corpDetails);
												bulkRegister.setName(dto.getFirstName() + " " + dto.getLastName());
												bulkRegister.setCardCreationStatus(true);
												bulkRegister.setPhyCardActivationStatus(true);
												bulkRegister.setPhyCardActivationStatus(true);
												bulkRegisterRepository.save(bulkRegister);
												map.put("regmessage",
														"User added Sucessfully and physical card assigned. ");
											} else {
												map.put("regmessage",
														"User added Sucessfully but physical card not assigned. ");
												CorporateAgentDetails corpDetails = corporateAgentDetailsRepository
														.getByCorporateId(agent);
												MUser uuu = userApiImpl.findByUserName(dto.getContactNo());
												bulkRegister.setCardNo("");
												bulkRegister.setDob(dto.getDateOfBirth());
												bulkRegister.setEmail(dto.getEmail());
												bulkRegister.setMobile(dto.getContactNo());
												bulkRegister.setProxyNo(dto.getProxyNumber());
												bulkRegister.setUser(uuu);
												bulkRegister.setDriver_id(dto.getDriver_id());

												bulkRegister.setAgentDetails(corpDetails);
												bulkRegister.setName(dto.getFirstName() + " " + dto.getLastName());
												bulkRegister.setCardCreationStatus(false);
												bulkRegister.setPhyCardActivationStatus(false);
												bulkRegister.setPhyCardActivationStatus(false);
												bulkRegisterRepository.save(bulkRegister);
											}
										} else {
											map.put("regmessage",
													"User added Sucessfully but physical card not assigned. ");
											CorporateAgentDetails corpDetails = corporateAgentDetailsRepository
													.getByCorporateId(agent);
											MUser uuu = userApiImpl.findByUserName(dto.getContactNo());
											bulkRegister.setCardNo("");
											bulkRegister.setDob(dto.getDateOfBirth());
											bulkRegister.setEmail(dto.getEmail());
											bulkRegister.setMobile(dto.getContactNo());
											bulkRegister.setProxyNo(dto.getProxyNumber());
											bulkRegister.setUser(uuu);
											bulkRegister.setDriver_id(dto.getDriver_id());

											bulkRegister.setAgentDetails(corpDetails);
											bulkRegister.setName(dto.getFirstName() + " " + dto.getLastName());
											bulkRegister.setCardCreationStatus(false);
											bulkRegister.setPhyCardActivationStatus(false);
											bulkRegister.setPhyCardActivationStatus(false);
											bulkRegisterRepository.save(bulkRegister);
										}

									} else {
										map.put("regmessage", "failed, Please try again later.");
									}
								} else {
									map.put("regmessage",
											"User added Sucessfully but account not created in matchmove global.");
								}
							}
						} else {
							map.put("regmessage", "User not added.");
						}
					} catch (Exception e) {
						e.printStackTrace();
						map.put("regmessage", "User not added please try again later");
					}
				} else {
					map.put("regmessage", registerError.getMessage());
				}
				return "Group/CorporateSingleCardAssignment";
			} else if (user.getAuthority().contains(Authorities.CORPORATE)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {

				dto.setUsername(dto.getContactNo());
				RegisterError registerError = registerValidation.validateNormalUser(dto);
				if (registerError.isValid()) {
					System.err.println(dto);
					dto.setUserType(UserType.User);
					try {
						boolean val = userApiImpl.saveUserFromCorporate(dto);
						if (val) {
							BulkRegister bulkRegister = new BulkRegister();
							if (!dto.isPhysicalCard()) {
								MUser user1 = userApiImpl.findByUserName(dto.getContactNo());
								ResponseDTO resp = matchMoveApi.createUserOnMM(user1);
								if (resp.getCode().equalsIgnoreCase("S00")) {
									bulkRegister.setUserCreationStatus(true);
									bulkRegister.setWalletCreationStatus(true);
									WalletResponse walletResponse = new WalletResponse();
									MatchMoveWallet wallet = matchMoveWalletRepository.findByUser(user1);
									if (wallet != null) {
										PhysicalCardDetails physicalCard = new PhysicalCardDetails();
										physicalCard.setFromAdmin(true);
										physicalCard.setWallet(wallet);
										physicalCardDetailRepository.save(physicalCard);
										walletResponse = matchMoveApi.assignPhysicalCard(user1, dto.getProxyNumber());
										if ("S00".equalsIgnoreCase(walletResponse.getCode())) {
											PhysicalCardDetails card = physicalCardDetailRepository.findByUser(user1);
											card.setStatus(Status.Received);
											physicalCardDetailRepository.save(card);
											ResponseDTO respo = matchMoveApi
													.activationPhysicalCard(card.getActivationCode(), user1);
											if (respo.getCode().equalsIgnoreCase("S00")) {
												map.put("sucMsg", "Physical card assigned.");
												PhysicalCardDetails card1 = physicalCardDetailRepository
														.findByUser(user1);
												card.setStatus(Status.Active);
												physicalCardDetailRepository.save(card1);
												PartnerDetails partnerDetails = partnerDetailsRepository
														.getPartnerDetails(agent);
												MUser uuu = userApiImpl.findByUserName(dto.getContactNo());
												bulkRegister.setCardNo("");
												bulkRegister.setDob(dto.getDateOfBirth());
												bulkRegister.setEmail(dto.getEmail());
												bulkRegister.setMobile(dto.getContactNo());
												bulkRegister.setProxyNo(dto.getProxyNumber());
												bulkRegister.setDriver_id(dto.getDriver_id());
												bulkRegister.setUser(uuu);
												if (partnerDetails != null) {

													bulkRegister.setAgentDetails(partnerDetails.getCorporate());
													bulkRegister.setPartnerDetails(partnerDetails);
												}
												bulkRegister.setName(dto.getFirstName() + " " + dto.getLastName());
												bulkRegister.setCardCreationStatus(true);
												bulkRegister.setPhyCardActivationStatus(true);
												bulkRegister.setPhyCardActivationStatus(true);
												bulkRegisterRepository.save(bulkRegister);
												map.put("regmessage",
														"User added Sucessfully and physical card assigned. ");
											} else {
												map.put("regmessage",
														"User added Sucessfully but physical card not assigned. ");
												PartnerDetails partnerDetails = partnerDetailsRepository
														.getPartnerDetails(agent);
												MUser uuu = userApiImpl.findByUserName(dto.getContactNo());
												bulkRegister.setCardNo("");
												bulkRegister.setDob(dto.getDateOfBirth());
												bulkRegister.setEmail(dto.getEmail());
												bulkRegister.setMobile(dto.getContactNo());
												bulkRegister.setProxyNo(dto.getProxyNumber());
												bulkRegister.setUser(uuu);
												bulkRegister.setDriver_id(dto.getDriver_id());
												if (partnerDetails != null) {

													bulkRegister.setAgentDetails(partnerDetails.getCorporate());
													bulkRegister.setPartnerDetails(partnerDetails);
												}
												bulkRegister.setName(dto.getFirstName() + " " + dto.getLastName());
												bulkRegister.setCardCreationStatus(false);
												bulkRegister.setPhyCardActivationStatus(false);
												bulkRegister.setPhyCardActivationStatus(false);
												bulkRegisterRepository.save(bulkRegister);
											}
										} else {
											map.put("regmessage",
													"User added Sucessfully but physical card not assigned. ");
											PartnerDetails partnerDetails = partnerDetailsRepository
													.getPartnerDetails(agent);

											MUser uuu = userApiImpl.findByUserName(dto.getContactNo());
											bulkRegister.setCardNo("");
											bulkRegister.setDob(dto.getDateOfBirth());
											bulkRegister.setEmail(dto.getEmail());
											bulkRegister.setMobile(dto.getContactNo());
											bulkRegister.setProxyNo(dto.getProxyNumber());
											bulkRegister.setUser(uuu);
											bulkRegister.setDriver_id(dto.getDriver_id());
											if (partnerDetails != null) {

												bulkRegister.setAgentDetails(partnerDetails.getCorporate());
												bulkRegister.setPartnerDetails(partnerDetails);
											}
											bulkRegister.setName(dto.getFirstName() + " " + dto.getLastName());
											bulkRegister.setCardCreationStatus(false);
											bulkRegister.setPhyCardActivationStatus(false);
											bulkRegister.setPhyCardActivationStatus(false);
											bulkRegisterRepository.save(bulkRegister);
										}

									} else {
										map.put("regmessage", "failed, Please try again later.");
									}
								} else {
									map.put("regmessage",
											"User added Sucessfully but account not created in matchmove global.");
								}
							}
						} else {
							map.put("regmessage", "User not added.");
						}
					} catch (Exception e) {
						e.printStackTrace();
						map.put("regmessage", "User not added please try again later");
					}
				} else {
					map.put("regmessage", registerError.getMessage());
				}

				PartnerDetails partnerDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
				if (partnerDetails != null) {
					List<MService> partnerServices = partnerDetails.getPartnerServices();
					if (partnerServices != null && !partnerServices.isEmpty()) {
						for (MService mService : partnerServices) {
							if (mService.getCode().equalsIgnoreCase("BRC")) {
								model.addAttribute("bulkRegistration", true);
							} else if (mService.getCode().equalsIgnoreCase("PREFC")) {
								model.addAttribute("prefundC", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCL")) {
								model.addAttribute("BulkCL", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCS")) {
								model.addAttribute("BulkCardIssuance", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCSL")) {
								model.addAttribute("SingleCardLoad", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCBL")) {
								model.addAttribute("CardBlockUnblock", true);
							} else if (mService.getCode().equalsIgnoreCase("CORSA")) {
								model.addAttribute("singleCardAssignment", true);

							}
						}
					}
				}
				model.addAttribute("UserType", false);
				model.addAttribute("username", userSession.getUser().getUsername());
				return "Group/CorporateSingleCardAssignment";

			}
			return "redirect:/Group/Home";
		}
		return "redirect:/Group/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/UserReport")
	public String userReport(@ModelAttribute PagingDTO page, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model, ModelMap map) throws ParseException {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				MUser user = userSession.getUser();
				if (user.getAuthority().contains(Authorities.CORPORATE)
						|| user.getAuthority().contains(Authorities.GROUP)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					model.addAttribute("UserType", true);
					model.addAttribute("bulkRegistration", true);
					model.addAttribute("prefundC", true);
					model.addAttribute("BulkCL", true);
					model.addAttribute("BulkCardIssuance", true);
					model.addAttribute("SingleCardLoad", true);
					model.addAttribute("CardBlockUnblock", true);
					model.addAttribute("username", userSession.getUser().getUsername());
					model.addAttribute("singleCardAssignment", true);
					String[] daterange = null;
					try {
						daterange = CommonUtil.getDefaultDateRange();
						Date fromDate = CommonUtil.formatter.parse(daterange[0]);
						Date toDate = CommonUtil.formatter.parse(daterange[1]);
						String pardF = CommonUtil.formatter.format(fromDate);
						String pardT = CommonUtil.formatter.format(toDate);

						page.setFromDate(fromDate);
						page.setToDate(toDate);
						model.addAttribute("dateRange", pardF + "-" + pardT);

						CorporateAgentDetails agentDetails = corporateAgentDetailsRepository
								.getByCorporateId(userSession.getUser());
						List<BulkRegister> corpDetails = bulkRegisterRepository
								.getRegisteredUsersByDate(page.getFromDate(), page.getToDate(), agentDetails);
						List<CorporateBulkUsers> bulkUsers = new ArrayList<>();
						if (corpDetails != null) {
							for (BulkRegister bulkRegister : corpDetails) {
								if (bulkRegister.getUser() != null) {
									CorporateBulkUsers blkUser = new CorporateBulkUsers();
									MMCards phyCard1 = mMCardRepository.getPhysicalCardByUser(bulkRegister.getUser());
									if (phyCard1 != null) {
										blkUser.setMobile(bulkRegister.getMobile());
										blkUser.setDob(bulkRegister.getDob());
										blkUser.setEmail(bulkRegister.getEmail());
										blkUser.setUsername(bulkRegister.getName());
										blkUser.setAuthority(bulkRegister.getUser().getAuthority());
										blkUser.setUser(bulkRegister.getUser());
										blkUser.setDriver_id(bulkRegister.getDriver_id());
										blkUser.setKycStatus(
												bulkRegister.getUser().getAccountDetail().getAccountType().getCode());
										blkUser.setPhysicalCardCreationStatus(true);
										blkUser.setCardNumber(phyCard1.getCardId());
										blkUser.setCardStatus(phyCard1.getStatus());
									}

									bulkUsers.add(blkUser);
								}
							}
							model.addAttribute("userList", bulkUsers);
							return "Group/CorporateUserReport";
						}
						model.addAttribute("userList", bulkUsers);
					} catch (Exception e) {
						e.printStackTrace();
					}
					return "Group/CorporateUserReport";
				} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)
						|| user.getAuthority().contains(Authorities.GROUP)
								&& user.getAuthority().contains(Authorities.USER)) {

					String[] daterange = null;
					try {
						daterange = CommonUtil.getDefaultDateRange();
						Date fromDate = CommonUtil.formatter.parse(daterange[0]);
						Date toDate = CommonUtil.formatter.parse(daterange[1]);
						String pardF = CommonUtil.formatter.format(fromDate);
						String pardT = CommonUtil.formatter.format(toDate);
						page.setFromDate(fromDate);
						page.setToDate(toDate);
						System.err.println(fromDate + "-" + toDate);
						model.addAttribute("dateRange", pardF + "-" + pardT);
						// }
					} catch (Exception e) {
						e.printStackTrace();
					}

					PartnerDetails agentDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
					List<BulkRegister> corpDetails = bulkRegisterRepository.getRegisteredUserPartner(page.getFromDate(),
							page.getToDate(), agentDetails);
					List<CorporateBulkUsers> bulkUsers = new ArrayList<>();
					if (corpDetails != null && !corpDetails.isEmpty()) {
						for (BulkRegister bulkRegister : corpDetails) {
							if (bulkRegister.getUser() != null) {
								CorporateBulkUsers blkUser = new CorporateBulkUsers();

								if (true) {
									MMCards phyCard = mMCardRepository.getPhysicalCardByUser(bulkRegister.getUser());
									if (phyCard != null) {

										blkUser.setMobile(bulkRegister.getMobile());
										blkUser.setDob(bulkRegister.getDob());
										blkUser.setEmail(bulkRegister.getEmail());
										blkUser.setUsername(bulkRegister.getName());
										blkUser.setAuthority(bulkRegister.getUser().getAuthority());
										blkUser.setUser(bulkRegister.getUser());
										blkUser.setDriver_id(bulkRegister.getDriver_id());
										blkUser.setKycStatus(
												bulkRegister.getUser().getAccountDetail().getAccountType().getCode());
										System.err.println("hiiii..");
										blkUser.setPhysicalCardCreationStatus(true);

										System.err.println(phyCard.getCardId());
										blkUser.setCardNumber(phyCard.getCardId());
										blkUser.setCardStatus(phyCard.getStatus());
									}

								}
								bulkUsers.add(blkUser);

							}
						}
						System.err.println(bulkUsers);
						model.addAttribute("userList", bulkUsers);
						return "Group/CorporateUserReport";
					}
					System.err.println(bulkUsers);

					model.addAttribute("userList", bulkUsers);

					PartnerDetails partnerDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
					if (partnerDetails != null) {
						List<MService> partnerServices = partnerDetails.getPartnerServices();
						if (partnerServices != null && !partnerServices.isEmpty()) {
							for (MService mService : partnerServices) {
								if (mService.getCode().equalsIgnoreCase("BRC")) {
									model.addAttribute("bulkRegistration", true);
								} else if (mService.getCode().equalsIgnoreCase("PREFC")) {
									model.addAttribute("prefundC", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCL")) {
									model.addAttribute("BulkCL", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCS")) {
									model.addAttribute("BulkCardIssuance", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCSL")) {
									model.addAttribute("SingleCardLoad", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCBL")) {
									model.addAttribute("CardBlockUnblock", true);
								} else if (mService.getCode().equalsIgnoreCase("CORSA")) {
									model.addAttribute("singleCardAssignment", true);

								}
							}
						}
					}
					model.addAttribute("UserType", false);
					model.addAttribute("username", userSession.getUser().getUsername());

					return "Group/CorporateUserReport";
				}

			}
		}
		return "redirect:/Group/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/UserReport")
	public String userReportPost(@ModelAttribute PagingDTO page, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model, ModelMap map) throws ParseException {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		try {
			if (sessionId != null && sessionId.length() != 0) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.CORPORATE)
							|| user.getAuthority().contains(Authorities.GROUP)
									&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						model.addAttribute("UserType", true);
						model.addAttribute("bulkRegistration", true);
						model.addAttribute("prefundC", true);
						model.addAttribute("BulkCL", true);
						model.addAttribute("BulkCardIssuance", true);
						model.addAttribute("SingleCardLoad", true);
						model.addAttribute("CardBlockUnblock", true);
						model.addAttribute("singleCardAssignment", true);

						model.addAttribute("username", userSession.getUser().getUsername());

						SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
						System.err.println("the date reange is :::::" + page.getDaterange());
						System.err.println("the status is :::::::::" + page.getCardStatus());
						String[] daterange = null;
						try {
							if (page.getDaterange().length() != 0) {
								System.err.println(page.getDaterange().length());
								String aaaaa = page.getDaterange().substring(0, 10);
								String baaaa = page.getDaterange().substring(13);
								Date from = sdformat.parse(aaaaa + " 00:00:00");
								Date to = sdformat.parse(baaaa + " 00:00:00");
								page.setFromDate(from);
								page.setToDate(to);

							} else {

								daterange = CommonUtil.getDefaultDateRange();
								Date fromDate = df2.parse(daterange[0]);
								Date toDate = df2.parse(daterange[1]);
								String pardF = df2.format(fromDate);
								String pardT = df2.format(toDate);

								page.setFromDate(fromDate);
								page.setToDate(toDate);
								System.err.println(fromDate + "-" + toDate);
								model.addAttribute("dateRange", pardF + "-" + pardT);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}

						CorporateAgentDetails agentDetails = corporateAgentDetailsRepository
								.getByCorporateId(userSession.getUser());
						// List<BulkRegister>
						// corpDetails=bulkRegisterRepository.getRegisteredUsers(agentDetails);
						List<BulkRegister> corpDetails = bulkRegisterRepository
								.getRegisteredUsersByDate(page.getFromDate(), page.getToDate(), agentDetails);
						List<CorporateBulkUsers> bulkUsers = new ArrayList<>();
						if (corpDetails != null) {
							for (BulkRegister bulkRegister : corpDetails) {
								if (bulkRegister.getUser() != null) {
									CorporateBulkUsers blkUser = new CorporateBulkUsers();

									if (true) {

										MMCards phyCard1 = mMCardRepository
												.getPhysicalCardByUser(bulkRegister.getUser());
										if (phyCard1 != null) {
											blkUser.setMobile(bulkRegister.getMobile());
											blkUser.setDob(bulkRegister.getDob());
											blkUser.setEmail(bulkRegister.getEmail());
											blkUser.setUsername(bulkRegister.getName());
											blkUser.setAuthority(bulkRegister.getUser().getAuthority());
											blkUser.setUser(bulkRegister.getUser());
											blkUser.setDriver_id(bulkRegister.getDriver_id());
											blkUser.setKycStatus(bulkRegister.getUser().getAccountDetail()
													.getAccountType().getCode());
											System.err.println("hiiii..");

											blkUser.setPhysicalCardCreationStatus(true);

											System.err.println(phyCard1.getCardId());
											blkUser.setCardNumber(phyCard1.getCardId());
											blkUser.setCardStatus(phyCard1.getStatus());
										}

									}
									bulkUsers.add(blkUser);

								}
							}
							System.err.println(bulkUsers);
							model.addAttribute("userList", bulkUsers);

							return "Group/CorporateUserReport";
						}
						System.err.println(bulkUsers);

						model.addAttribute("userList", bulkUsers);
						return "Group/CorporateUserReport";
					} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)) {

					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/Group/Home";
	}

	/**
	 * DOWLOAD AS CSV
	 */

	@RequestMapping(value = "/download/csv", method = RequestMethod.GET)
	public ResponseEntity<ResponseDTO> getDownloadBulkTransferFile(@ModelAttribute DownloadCsv downloadCsv,
			HttpServletRequest request, HttpServletResponse res, HttpSession session) throws IOException {

		ResponseDTO response = new ResponseDTO();
		Date from = null;
		Date to = null;
		String filename = ("Card_Records" + System.currentTimeMillis() + ".xls");
		DownloadHistory download = new DownloadHistory();
		download.setStatus(Status.Pending);
		download.setCorporate(true);
		download.setFileExtension(filename);
		DownloadHistory dwHis = downloadHistoryRepository.save(download);
		System.err.println("the req::" + downloadCsv);
		SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");

		String[] daterange = null;
		// String dateRange="2018-05-01 - 2018-06-01";
		try {
			if (downloadCsv.getDateRange().length() != 0) {
				String aaaaa = downloadCsv.getDateRange().substring(0, 10);
				String baaaa = downloadCsv.getDateRange().substring(13);
				from = sdformat.parse(aaaaa + " 00:00:00");
				to = sdformat.parse(baaaa + " 00:00:00");
			} else {
				daterange = CommonUtil.getDefaultDateRange();
				from = df2.parse(daterange[0]);
				to = df2.parse(daterange[1]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		List<MMCardsDTO> dto = new ArrayList<MMCardsDTO>();

		switch (downloadCsv.getRequestType()) {
		case "physicalCards":

			List<MMCards> cardsdto1 = mMCardRepository.getAllCards(false, from, to);

			for (MMCards cards : cardsdto1) {
				BulkRegister crpUser = bulkRegisterRepository.getByUser(cards.getWallet().getUser());
				if (crpUser != null) {
					MatchMoveCreateCardRequest cardRequest = new MatchMoveCreateCardRequest();
					cardRequest.setEmail(cards.getWallet().getUser().getUserDetail().getEmail());
					cardRequest.setUsername(cards.getWallet().getUser().getUsername());
					try {
						cardRequest.setPassword(SecurityUtil.md5(cards.getWallet().getUser().getUsername()));
					} catch (Exception e) {
						e.printStackTrace();
					}
					cardRequest.setCardId(cards.getCardId());
					WalletResponse walletResp = matchMoveApi.inquireCard(cardRequest);

					double balance = matchMoveApi.getBalance(cards.getWallet().getUser());
					MMCardsDTO card = new MMCardsDTO();
					card.setFirstName(cards.getWallet().getUser().getUserDetail().getFirstName());
					card.setLastName(cards.getWallet().getUser().getUserDetail().getLastName());
					// card.setCardNumber(cards.getWallet().getUser().getUserDetail().get);
					card.setBalance(String.valueOf(balance));
					card.setContactNO(cards.getWallet().getUser().getUserDetail().getContactNo());
					card.setEmail(cards.getWallet().getUser().getUserDetail().getEmail());
					card.setCardNumber(walletResp.getWalletNumber());
					card.setDate(sdformat.format(cards.getCreated()));

					dto.add(card);
				}
			}
			break;
		}

		dwHis.setStatus(Status.Success);
		downloadHistoryRepository.save(dwHis);
		String fileName = ExcelWriter.makeExcelSheet(dto, filename);
		System.err.println(fileName);
		String contextPath = request.getRealPath("/");
		// ServletContext contextPath= request.getSession().getServletContext();
		System.out.println(contextPath);
		String filePath = contextPath + "/resources/excelSheets/";
		response.setCode("\\resources\\excelSheets\\" + fileName);

		return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/Status/block/unblock", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> userStatus(@RequestBody RequestDTO cardRequest, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap map) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE) || user.getAuthority().contains(Authorities.GROUP)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {

				// persistingSessionRegistry.refreshLastRequest(sessionId);
				MUser use = userApiImpl.findByUserName(cardRequest.getUserName());
				System.err.println("auth" + cardRequest.getAuthority());
				userApiImpl.updateUserAuthority(cardRequest.getAuthority(), use.getId());
				return new ResponseEntity<>(walletResponse, HttpStatus.OK);
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return new ResponseEntity<>(walletResponse, HttpStatus.OK);
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return new ResponseEntity<>(walletResponse, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UserTransaction/{username}/{cardId}", method = RequestMethod.GET)
	public String fetchUserTransaction(@ModelAttribute RegisterDTO dto, @PathVariable String username,
			@PathVariable String cardId, HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap map, Model model) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE) || user.getAuthority().contains(Authorities.GROUP)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);

				model.addAttribute("username", userSession.getUser().getUsername());

				MUser use = userApiImpl.findByUserName(username);
				try {
					UserKycResponse walletRespon = new UserKycResponse();
					walletRespon = matchMoveApi.getTransactions(use);
					List<ResponseDTO> resul = new ArrayList<>();
					MMCards whichCard = matchMoveApi.findCardByCardId(cardId);
					if (!whichCard.isHasPhysicalCard()) {
						if (walletRespon.getVirtualTransactions() != null) {
							JSONObject obj = new JSONObject(walletRespon.getVirtualTransactions().toString());
							JSONArray aa = obj.getJSONArray("transactions");
							for (int i = 0; i < aa.length(); i++) {
								ResponseDTO resp = new ResponseDTO();
								resp.setAmount(aa.getJSONObject(i).getString("amount"));
								resp.setDate(aa.getJSONObject(i).getString("date"));
								if (aa.getJSONObject(i).getJSONObject("details") != null) {
									JSONObject ojj = aa.getJSONObject(i).getJSONObject("details");
									if (ojj.has("merchantname")) {
										resp.setDescription(
												aa.getJSONObject(i).getJSONObject("details").getString("merchantname"));
									} else {
										resp.setDescription(aa.getJSONObject(i).getString("description"));
									}
								}
								resp.setStatus(aa.getJSONObject(i).getString("status"));
								resp.setTransactionType(aa.getJSONObject(i).getString("indicator"));
								resul.add(resp);
							}
						}
					} else {
						if (walletRespon.getPhysicalCardTransactions() != null) {
							JSONObject obj = new JSONObject(walletRespon.getPhysicalCardTransactions().toString());
							JSONArray aa = obj.getJSONArray("transactions");
							for (int i = 0; i < aa.length(); i++) {
								ResponseDTO resp = new ResponseDTO();
								resp.setAmount(aa.getJSONObject(i).getString("amount"));
								resp.setDate(aa.getJSONObject(i).getString("date"));
								if (aa.getJSONObject(i).getJSONObject("details") != null) {
									JSONObject ojj = aa.getJSONObject(i).getJSONObject("details");
									if (ojj.has("merchantname")) {
										resp.setDescription(
												aa.getJSONObject(i).getJSONObject("details").getString("merchantname"));
									} else {
										resp.setDescription(aa.getJSONObject(i).getString("description"));
									}
								}
								resp.setStatus(aa.getJSONObject(i).getString("status"));
								resp.setTransactionType(aa.getJSONObject(i).getString("indicator"));
								resul.add(resp);
							}
						}
					}
					map.put("transactions", resul);

					return "Group/CorporateCardTransaction";
				} catch (Exception e) {
					e.printStackTrace();
					walletResponse.setMessage("Service Unavailable");
					walletResponse.setCode(ResponseStatus.FAILURE.getValue());
					return "Group/CorporateCardTransaction";
				}

			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "Group/Home";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "Group/Home";
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/SingleCard/{email}/{cardId}/{contactNo}")
	public String getSingleCard(@PathVariable(value = "email") String email,
			@PathVariable(value = "cardId") String cardId, @PathVariable(value = "contactNo") String contactNo,
			ModelMap modelMap, HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model) {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE) || user.getAuthority().contains(Authorities.GROUP)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);

				model.addAttribute("username", userSession.getUser().getUsername());
				MUser use = userApiImpl.findByUserName(contactNo);

				MatchMoveCreateCardRequest cardFetch = new MatchMoveCreateCardRequest();
				cardFetch.setCardId(cardId);
				cardFetch.setEmail(use.getUserDetail().getEmail());
				try {
					cardFetch.setPassword(SecurityUtil.md5(contactNo));
					cardFetch.setUsername(contactNo);
					WalletResponse fetchResponse = matchMoveApi.inquireCard(cardFetch);
					if (fetchResponse.getWalletNumber() != null && fetchResponse != null) {
						String nor = fetchResponse.getWalletNumber();
						System.err.println(nor);
						String aaa = nor.substring(0, 4) + " " + nor.substring(4, 8) + " " + nor.substring(8, 12) + " "
								+ nor.substring(12, 16);
						fetchResponse.setWalletNumber(aaa);
						UserKycResponse walletRespon = new UserKycResponse();
						walletRespon = matchMoveApi.getTransactions(use);
						MMCards whichCard = matchMoveApi.findCardByCardId(cardId);
						List<ResponseDTO> resul = new ArrayList<>();
						if (!whichCard.isHasPhysicalCard()) {
							if (walletRespon.getVirtualTransactions() != null) {
								JSONObject obj = new JSONObject(walletRespon.getVirtualTransactions().toString());
								JSONArray aa = obj.getJSONArray("transactions");
								for (int i = 0; i < aa.length(); i++) {
									if (i < 5) {
										ResponseDTO resp = new ResponseDTO();
										resp.setAmount(aa.getJSONObject(i).getString("amount"));
										resp.setDate(aa.getJSONObject(i).getString("date"));
										if (aa.getJSONObject(i).getJSONObject("details") != null) {
											JSONObject ojj = aa.getJSONObject(i).getJSONObject("details");
											if (ojj.has("merchantname")) {
												resp.setDescription(aa.getJSONObject(i).getJSONObject("details")
														.getString("merchantname"));
											} else {
												resp.setDescription(aa.getJSONObject(i).getString("description"));
											}
										}
										resp.setStatus(aa.getJSONObject(i).getString("status"));
										resp.setTransactionType(aa.getJSONObject(i).getString("indicator"));
										resul.add(resp);
									}
								}
							}
						} else {
							if (walletRespon.getPhysicalCardTransactions() != null) {
								JSONObject obj = new JSONObject(walletRespon.getPhysicalCardTransactions().toString());
								JSONArray aa = obj.getJSONArray("transactions");
								for (int i = 0; i < aa.length(); i++) {
									if (i < 5) {
										ResponseDTO resp = new ResponseDTO();
										resp.setAmount(aa.getJSONObject(i).getString("amount"));
										resp.setDate(aa.getJSONObject(i).getString("date"));
										if (aa.getJSONObject(i).getJSONObject("details") != null) {
											JSONObject ojj = aa.getJSONObject(i).getJSONObject("details");
											if (ojj.has("merchantname")) {
												resp.setDescription(aa.getJSONObject(i).getJSONObject("details")
														.getString("merchantname"));
											} else {
												resp.setDescription(aa.getJSONObject(i).getString("description"));
											}
										}
										resp.setStatus(aa.getJSONObject(i).getString("status"));
										resp.setTransactionType(aa.getJSONObject(i).getString("indicator"));
										resul.add(resp);
									}
								}
							}
						}
						session.setAttribute("transactions", resul);

					}

					double balance = matchMoveApi.getBalance(use);
					model.addAttribute("Ubalance", balance);
					MMCards card = matchMoveApi.findCardByCardId(cardId);
					model.addAttribute("cardstatus", card.isBlocked());
					session.setAttribute("cardDetail", fetchResponse);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return "Group/CorporateCardDetails";
			}
		}
		return "Group/Home";
	}

	/**
	 * UNBLOCK CARD
	 */

	@RequestMapping(value = "/UnblockCard", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> unblockCard(@RequestBody MatchMoveCreateCardRequest cardRequest,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE) || user.getAuthority().contains(Authorities.GROUP)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {

				walletResponse = matchMoveApi.reActivateCard(cardRequest);
				if ("S00".equalsIgnoreCase(walletResponse.getCode())) {
					MMCards card = matchMoveApi.findCardByCardId(cardRequest.getCardId());
					card.setBlocked(false);
					card.setStatus("Active");
					mMCardRepository.save(card);
				}
				MMCards card = matchMoveApi.findCardByCardId(cardRequest.getCardId());
				model.addAttribute("cardstatus", card.isBlocked());
				return new ResponseEntity<>(walletResponse, HttpStatus.OK);
			} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)) {
				walletResponse = matchMoveApi.reActivateCard(cardRequest);
				if ("S00".equalsIgnoreCase(walletResponse.getCode())) {
					MMCards card = matchMoveApi.findCardByCardId(cardRequest.getCardId());
					card.setBlocked(false);
					card.setStatus("Active");
					mMCardRepository.save(card);
				}
				MMCards card = matchMoveApi.findCardByCardId(cardRequest.getCardId());
				model.addAttribute("cardstatus", card.isBlocked());
				return new ResponseEntity<>(walletResponse, HttpStatus.OK);
			}

		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return new ResponseEntity<>(walletResponse, HttpStatus.OK);
		}
		return new ResponseEntity<>(walletResponse, HttpStatus.OK);
	}

	/**
	 * BLOCK CARD
	 */

	@RequestMapping(value = "/BlockCard", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> blockCardCorporate(@RequestBody MatchMoveCreateCardRequest cardRequest,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE) || user.getAuthority().contains(Authorities.GROUP)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {

				System.err.println(cardRequest.getRequestType());
				System.err.println(cardRequest.getCardId());
				walletResponse = matchMoveApi.deActivateCards(cardRequest);
				if ("S00".equalsIgnoreCase(walletResponse.getCode())) {
					MMCards card = matchMoveApi.findCardByCardId(cardRequest.getCardId());
					card.setBlocked(true);
					mMCardRepository.save(card);

				}
				MMCards card = matchMoveApi.findCardByCardId(cardRequest.getCardId());
				model.addAttribute("cardstatus", card.isBlocked());

				return new ResponseEntity<>(walletResponse, HttpStatus.OK);
			} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)) {

				System.err.println(cardRequest.getRequestType());
				System.err.println(cardRequest.getCardId());
				walletResponse = matchMoveApi.deActivateCards(cardRequest);
				if ("S00".equalsIgnoreCase(walletResponse.getCode())) {
					MMCards card = matchMoveApi.findCardByCardId(cardRequest.getCardId());
					card.setBlocked(true);
					mMCardRepository.save(card);

				}
				MMCards card = matchMoveApi.findCardByCardId(cardRequest.getCardId());
				model.addAttribute("cardstatus", card.isBlocked());

			}

			else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return new ResponseEntity<>(walletResponse, HttpStatus.OK);
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return new ResponseEntity<>(walletResponse, HttpStatus.OK);
		}
		return new ResponseEntity<>(walletResponse, HttpStatus.OK);

	}

	@RequestMapping(method = RequestMethod.GET, value = "{userc}/UpgradeAccount")
	public String updateAcco(@PathVariable("userc") String userc, ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE) || user.getAuthority().contains(Authorities.GROUP)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);

				model.addAttribute("username", userSession.getUser().getUsername());

				model.addAttribute("userc", userc);
				return "Group/CorporateUpgradeaccount";
			} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)) {
				PartnerDetails partnerDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
				if (partnerDetails != null) {
					List<MService> partnerServices = partnerDetails.getPartnerServices();
					if (partnerServices != null && !partnerServices.isEmpty()) {
						for (MService mService : partnerServices) {
							if (mService.getCode().equalsIgnoreCase("BRC")) {
								model.addAttribute("bulkRegistration", true);
							} else if (mService.getCode().equalsIgnoreCase("PREFC")) {
								model.addAttribute("prefundC", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCL")) {
								model.addAttribute("BulkCL", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCS")) {
								model.addAttribute("BulkCardIssuance", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCSL")) {
								model.addAttribute("SingleCardLoad", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCBL")) {
								model.addAttribute("CardBlockUnblock", true);
							} else if (mService.getCode().equalsIgnoreCase("CORSA")) {
								model.addAttribute("singleCardAssignment", true);

							}
						}
					}
				}
				model.addAttribute("UserType", false);
				model.addAttribute("username", userSession.getUser().getUsername());

				return "Group/CorporateUpgradeaccount";

			}
		}
		return "redirect:/Group/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/UpgradeAccount")
	public String updateKYC(@ModelAttribute UpgradeAccountDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		// TODO Auto-generated method stub
		try {
			System.err.println("hello");
			System.err.println("this is username:::" + dto.getUsername());
			MUser user = userApiImpl.findByUserName(dto.getUsername().trim());
			if (user != null) {
				MKycDetail detail = mKycRepository.findByUser(user);
				System.err.println("hey I am here");
				if (detail == null) {
					MUserDetails userDetails = user.getUserDetail();
					if (userDetails != null) {
						if (userDetails.getDateOfBirth() == null) {
							try {
								userDetails.setDateOfBirth(CommonUtil.formatter.parse(dto.getDob()));
								mUserDetailRepository.save(userDetails);

							} catch (java.text.ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}

					dto.setUserType(UserType.User);
					dto.setEmail(user.getUserDetail().getEmail());
					dto.setContactNo(user.getUserDetail().getContactNo());
					dto.setName(user.getUserDetail().getFirstName());
					if (dto.getImage() != null) {
						String aadharCardImgPath = CommonUtil.uploadImage(dto.getImage(), dto.getContactNo(),
								dto.getIdType());
						dto.setImagePath(aadharCardImgPath);
					}
					if (dto.getImage2() != null) {
						String aadharCardImgPath = CommonUtil.uploadImage(dto.getImage2(), dto.getContactNo(),
								dto.getIdType());
						dto.setImagePath2(aadharCardImgPath);
					}

					ResponseDTO upg = userApiImpl.upgradeAccountCorp(dto);
					if (upg.getCode().equalsIgnoreCase("S00")) {
						MKycDetail check = mKycRepository.findByUser(user);
						dto.setId(String.valueOf(check.getId()));
						ResponseDTO upg1 = userApiImpl.updateKycRequestCorp(dto);
						if (upg1.getCode().equalsIgnoreCase("S00")) {
							model.addAttribute("message", "Account has been upgraded to KYC");
							return "Group/CorporateUpgradeaccount";

						} else {
							model.addAttribute("message",
									"Account upgradation Failed.Please try again or contact Admin");
							return "Group/CorporateUpgradeaccount";

						}
					} else {
						model.addAttribute("message",
								"Account upgradation Failed due to invalid data.Please try again or contact Admin");
						return "Group/CorporateUpgradeaccount";

					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("message", "Upgrade Account Failed,Please try again or contact Admin");
			return "Group/CorporateUpgradeaccount";
		}
		return "Group/CorporateUpgradeaccount";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/PrefundHistory")
	public String getPrefundHistory(@ModelAttribute PagingDTO page, ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE) || user.getAuthority().contains(Authorities.GROUP)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);

				model.addAttribute("username", userSession.getUser().getUsername());

				SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
				String[] daterange = null;

				try {
					daterange = CommonUtil.getDefaultDateRange();
					Date fromDate = df2.parse(daterange[0]);
					Date toDate = df2.parse(daterange[1]);
					String pardF = df2.format(fromDate);
					String pardT = df2.format(toDate);

					page.setFromDate(fromDate);
					page.setToDate(toDate);
					System.err.println(fromDate + "-" + toDate);
					model.addAttribute("dateRange", pardF + "-" + pardT);
					// }
				} catch (Exception e) {
					e.printStackTrace();
				}
				// List<CorporatePrefundHistory>
				// prefundHistory=corporatePrefundHistoryRepository.getByCorporate(userSession.getUser());
				List<CorporatePrefundHistory> prefundHistory = corporatePrefundHistoryRepository
						.getByCorporateByDate(page.getFromDate(), page.getToDate(), userSession.getUser());
				model.addAttribute("prefund", prefundHistory);
				return "Group/CorporatePrefundHistory";
			}
		}
		return "redirect:/Group/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/PrefundHistory")
	public String getPrefundHistoryPost(@ModelAttribute PagingDTO page, ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE) || user.getAuthority().contains(Authorities.GROUP)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);

				model.addAttribute("username", userSession.getUser().getUsername());

				SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
				String[] daterange = null;
				try {
					if (page.getDaterange().length() != 0) {
						System.err.println(page.getDaterange().length());
						String aaaaa = page.getDaterange().substring(0, 10);
						String baaaa = page.getDaterange().substring(13);
						Date from = sdformat.parse(aaaaa + " 00:00:00");
						Date to = sdformat.parse(baaaa + " 00:00:00");
						page.setFromDate(from);
						page.setToDate(to);

					} else {
						daterange = CommonUtil.getDefaultDateRange();
						Date fromDate = df2.parse(daterange[0]);
						Date toDate = df2.parse(daterange[1]);
						String pardF = df2.format(fromDate);
						String pardT = df2.format(toDate);

						page.setFromDate(fromDate);
						page.setToDate(toDate);
						System.err.println(fromDate + "-" + toDate);
						model.addAttribute("dateRange", pardF + "-" + pardT);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				// List<CorporatePrefundHistory>
				// prefundHistory=corporatePrefundHistoryRepository.getByCorporate(userSession.getUser());
				List<CorporatePrefundHistory> prefundHistory = corporatePrefundHistoryRepository
						.getByCorporateByDate(page.getFromDate(), page.getToDate(), userSession.getUser());
				model.addAttribute("prefund", prefundHistory);
				return "Group/CorporatePrefundHistory";
			}
		}
		return "redirect:/Group/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/FailedRegistration")
	public String getFailedReport(@ModelAttribute PagingDTO page, ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE) || user.getAuthority().contains(Authorities.GROUP)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);

				model.addAttribute("username", userSession.getUser().getUsername());
				CorporateAgentDetails corpAgent = corporateAgentDetailsRepository
						.getByCorporateId(userSession.getUser());

				List<BulkRegister> bulkRegisters = bulkRegisterRepository.getFailedUserList(corpAgent);
				model.addAttribute("failed", bulkRegisters);
				return "Group/CorporateFailedRegistration";
			}
		}
		return "redirect:/Group/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/BulkLoadReport")
	public String getBulkLoadReport(@ModelAttribute PagingDTO page, ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE) || user.getAuthority().contains(Authorities.GROUP)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);

				model.addAttribute("username", userSession.getUser().getUsername());
				CorporateAgentDetails corpAgent = corporateAgentDetailsRepository
						.getByCorporateId(userSession.getUser());

				List<MTransaction> transactions = mTransactionRepository
						.getDriveUTransaction(userSession.getUser().getAccountDetail());
				model.addAttribute("failed", transactions);
				return "Group/CorporateBulkLoadReport";
			}
		}
		return "redirect:/Group/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/FailedBulkLoad")
	public String getBulkLoad(@ModelAttribute PagingDTO page, ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE) || user.getAuthority().contains(Authorities.GROUP)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);

				model.addAttribute("username", userSession.getUser().getUsername());
				CorporateAgentDetails corpAgent = corporateAgentDetailsRepository
						.getByCorporateId(userSession.getUser());

				List<MTransaction> transactions = mTransactionRepository.getDriveUTransactionFailed();
				model.addAttribute("failed", transactions);
				return "Group/CorporateFailedLoad";
			}
		}
		return "redirect:/Group/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/BlockedCards")
	public String getBlockedCards(@ModelAttribute PagingDTO page, ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE) || user.getAuthority().contains(Authorities.GROUP)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);

				model.addAttribute("username", userSession.getUser().getUsername());
				CorporateAgentDetails corpAgent = corporateAgentDetailsRepository
						.getByCorporateId(userSession.getUser());
				List<BulkRegister> corpUsers = bulkRegisterRepository.getRegisteredUsers(corpAgent);
				List<BulkRegister> blockedCardUser = new ArrayList<>();
				for (BulkRegister bulkRegister : corpUsers) {
					MMCards blkCard = mMCardRepository.getCardDetailsByUserAndStatus(bulkRegister.getUser(), "Inactive",
							true);
					if (blkCard != null) {
						blockedCardUser.add(bulkRegister);
					}
				}
				model.addAttribute("failed", blockedCardUser);
				return "Group/CorporateBlockedUser";
			} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)) {
				PartnerDetails partnerDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
				if (partnerDetails != null) {
					List<MService> partnerServices = partnerDetails.getPartnerServices();
					if (partnerServices != null && !partnerServices.isEmpty()) {
						for (MService mService : partnerServices) {
							if (mService.getCode().equalsIgnoreCase("BRC")) {
								model.addAttribute("bulkRegistration", true);
							} else if (mService.getCode().equalsIgnoreCase("PREFC")) {
								model.addAttribute("prefundC", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCL")) {
								model.addAttribute("BulkCL", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCS")) {
								model.addAttribute("BulkCardIssuance", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCSL")) {
								model.addAttribute("SingleCardLoad", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCBL")) {
								model.addAttribute("CardBlockUnblock", true);
							} else if (mService.getCode().equalsIgnoreCase("CORSA")) {
								model.addAttribute("singleCardAssignment", true);

							}
						}
					}
				}
				CorporateAgentDetails corpAgent = corporateAgentDetailsRepository
						.getByCorporateId(userSession.getUser());
				List<BulkRegister> corpUsers = bulkRegisterRepository.getRegisteredUsers(corpAgent);
				List<BulkRegister> blockedCardUser = new ArrayList<>();
				for (BulkRegister bulkRegister : corpUsers) {
					MMCards blkCard = mMCardRepository.getCardDetailsByUserAndStatus(bulkRegister.getUser(), "Inactive",
							true);
					if (blkCard != null) {
						blockedCardUser.add(bulkRegister);
					}
				}
				model.addAttribute("failed", blockedCardUser);
				model.addAttribute("UserType", false);
				model.addAttribute("username", userSession.getUser().getUsername());
				return "Group/CorporateBlockedUser";

			}
		}
		return "redirect:/Group/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Prefund")
	public String getcorporatePrefund(@ModelAttribute PrefundHistoryDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) throws ParseException {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.CORPORATE)
						|| user.getAuthority().contains(Authorities.GROUP)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					model.addAttribute("UserType", true);
					model.addAttribute("bulkRegistration", true);
					model.addAttribute("prefundC", true);
					model.addAttribute("BulkCL", true);
					model.addAttribute("BulkCardIssuance", true);
					model.addAttribute("SingleCardLoad", true);
					model.addAttribute("singleCardAssignment", true);

					model.addAttribute("CardBlockUnblock", true);
					model.addAttribute("username", userSession.getUser().getUsername());

					return "Group/CorporatePrefund";
				}
			}

		}
		return "redirect:/Group/Home";

	}

	@RequestMapping(method = RequestMethod.POST, value = "/CorporatePrefund")
	public String corporatePrefund(@ModelAttribute PrefundHistoryDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) throws ParseException {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.CORPORATE)
						|| user.getAuthority().contains(Authorities.GROUP)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					model.addAttribute("UserType", true);
					model.addAttribute("bulkRegistration", true);
					model.addAttribute("prefundC", true);
					model.addAttribute("BulkCL", true);
					model.addAttribute("BulkCardIssuance", true);
					model.addAttribute("SingleCardLoad", true);
					model.addAttribute("CardBlockUnblock", true);
					model.addAttribute("singleCardAssignment", true);

					model.addAttribute("username", userSession.getUser().getUsername());

					String s3Path = CommonUtil.uploadCorporateLogo(dto.getFile(), user.getUserId(), "PREFUNDREQ");
					CorporatePrefundHistory prefund = new CorporatePrefundHistory();
					prefund.setAmount(dto.getAmount());
					prefund.setCorporate(userSession.getUser());
					prefund.setCorporateName(dto.getClientName());
					prefund.setFilePath(s3Path);
					prefund.setTransactionRefNo(dto.getTransactionRefNo());
					corporatePrefundHistoryRepository.save(prefund);
					model.addAttribute("successMsg",
							"Your Request has been submitted.Amount will be reflected shortly in your account upon confirmation.Contact your admin for other queries.");
					return "Group/CorporatePrefund";
				}
			}

		}
		return "redirect:/Group/Home";

	}

	@RequestMapping(method = RequestMethod.GET, value = "/inactivePartner")
	public String inactiveCorporate(@ModelAttribute("partner") String partner, ModelMap modelMap,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model,
			RedirectAttributes redirect) {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE) || user.getAuthority().contains(Authorities.GROUP)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);
				model.addAttribute("username", userSession.getUser().getUsername());

				if (partner != null) {
					String success = userApiImpl.inactivePartener(userApiImpl.getPartnerById(Long.parseLong(partner)));
					if (success != null) {
						redirect.addFlashAttribute("mesg", "Partner has been removed");
					} else {
						redirect.addFlashAttribute("mesg", "Error! while removing partner");
					}
				}
				return "redirect:/Group/ListCorporatePartner";
			}
		}
		return "redirect:/Group/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/getPartnerTransaction")
	public String getPartnerTransaction(@ModelAttribute("partner") String partner, ModelMap modelMap,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model,
			RedirectAttributes redirect) {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE) || user.getAuthority().contains(Authorities.GROUP)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);
				model.addAttribute("username", userSession.getUser().getUsername());
				List<TransactionListDTO> txnList = null;
				if (partner != null) {
					PartnerDetails details = userApiImpl.getPartnerDetails(Long.parseLong(partner));
					if (details != null && details.getPartnerUser() != null) {
						List<MTransaction> transactions = mTransactionRepository
								.getTotalTxnByAccount(details.getPartnerUser().getAccountDetail());
						txnList = transactionApi.getTransactionByAccounts(transactions, details.getPartnerUser());
					}
				}
				modelMap.addAttribute("transactions", txnList);
				modelMap.put("Lstatus", "All");
				return "Group/CorporateLoadTransactions";
			}
		}
		return "redirect:/Group/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/AddCorporatePartner")
	public String getcorporatePrefundGet(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model) throws ParseException {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.CORPORATE)
						|| user.getAuthority().contains(Authorities.GROUP)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					model.addAttribute("UserType", true);
					model.addAttribute("bulkRegistration", true);
					model.addAttribute("prefundC", true);
					model.addAttribute("BulkCL", true);
					model.addAttribute("BulkCardIssuance", true);
					model.addAttribute("SingleCardLoad", true);
					model.addAttribute("CardBlockUnblock", true);
					model.addAttribute("singleCardAssignment", true);

					model.addAttribute("username", userSession.getUser().getUsername());

					MOperator operator = mOperatorRepository.findOperatorByName("CORP_AGENT");
					if (operator != null) {
						List<MService> listOperators = mServiceRepository.getServicesByOperator(operator);
						model.addAttribute("serviceList", listOperators);
						return "Group/CorporateAddPartner";
					}
				} else {
					return "redirect:/Group/Home";
				}
			} else {
				return "redirect:/Group/Home";
			}

		}
		return "redirect:/Group/Home";

	}

	@RequestMapping(method = RequestMethod.POST, value = "/AddCorporatePartner")
	public String getcorporatePrefund(@ModelAttribute AddPartnerDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) throws ParseException {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.CORPORATE)
						|| user.getAuthority().contains(Authorities.GROUP)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					model.addAttribute("UserType", true);
					model.addAttribute("bulkRegistration", true);
					model.addAttribute("prefundC", true);
					model.addAttribute("BulkCL", true);
					model.addAttribute("BulkCardIssuance", true);
					model.addAttribute("SingleCardLoad", true);
					model.addAttribute("CardBlockUnblock", true);
					model.addAttribute("singleCardAssignment", true);

					model.addAttribute("username", userSession.getUser().getUsername());

					if (dto.getServices() != null && !dto.getServices().isEmpty()) {
						List<MService> serviceList = new ArrayList<>();
						for (String serviceCode : dto.getServices()) {
							MService service = mServiceRepository.findServiceByCode(serviceCode);
							if (service != null) {
								serviceList.add(service);
							}
						}
						RegisterDTO registerPartner = new RegisterDTO();
						registerPartner.setEmail(dto.getPartnerEmail());
						registerPartner.setContactNo(dto.getPartnerMobile());
						registerPartner.setPartnerServices(serviceList);
						registerPartner.setFirstName(dto.getPartnerName());
						registerPartner.setMaxLoadCardLimit(dto.getLoadCardMaxLimit());
						CorporateAgentDetails agent = corporateAgentDetailsRepository
								.getByCorporateId(userSession.getUser());
						registerPartner.setAgentDetails(agent);
						boolean success = userApiImpl.saveCorporatePartner(registerPartner);
						if (success) {
							MOperator operator = mOperatorRepository.findOperatorByName("CORP_AGENT");
							if (operator != null) {
								List<MService> listOperators = mServiceRepository.getServicesByOperator(operator);
								model.addAttribute("serviceList", listOperators);
								model.addAttribute("message", "Partner Registered Successfully");
								return "Group/CorporateAddPartner";
							}
						} else {
							MOperator operator = mOperatorRepository.findOperatorByName("CORP_AGENT");
							if (operator != null) {
								List<MService> listOperators = mServiceRepository.getServicesByOperator(operator);

								model.addAttribute("serviceList", listOperators);

								model.addAttribute("message", "Partner Registration Failed");
								return "Group/CorporateAddPartner";

							}
						}
					}
					model.addAttribute("message", "Partner Registration Failed");

					return "Group/CorporateAddPartner";
				}
			}

		}
		return "redirect:/Group/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/ListCorporatePartner")
	public String listCorporatePartner(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) throws ParseException {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.CORPORATE)
						|| user.getAuthority().contains(Authorities.GROUP)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					model.addAttribute("UserType", true);
					model.addAttribute("bulkRegistration", true);
					model.addAttribute("prefundC", true);
					model.addAttribute("BulkCL", true);
					model.addAttribute("BulkCardIssuance", true);
					model.addAttribute("SingleCardLoad", true);
					model.addAttribute("CardBlockUnblock", true);
					model.addAttribute("singleCardAssignment", true);

					model.addAttribute("username", userSession.getUser().getUsername());

					CorporateAgentDetails agentDetails = corporateAgentDetailsRepository
							.getByCorporateId(userSession.getUser());
					if (agentDetails != null) {
						List<String> serviceList = new ArrayList<>();
						List<MService> pseudoServices = new ArrayList<>();
						List<MService> listOperators = null;
						MOperator operator = mOperatorRepository.findOperatorByName("CORP_AGENT");
						if (operator != null) {
							listOperators = mServiceRepository.getServicesByOperator(operator);
						}
						List<PartnerDetails> listPartnerDetails = partnerDetailsRepository
								.getPartnerDetails(agentDetails, Status.Active);
						if (listPartnerDetails != null) {
							for (PartnerDetails partnerDetails : listPartnerDetails) {
								for (MService servicesList : partnerDetails.getPartnerServices()) {

									serviceList.add(servicesList.getDescription());
									pseudoServices.add(servicesList);
									System.err.println(servicesList.getCode());
								}
							}
							// System.err.println(someService.size());
							model.addAttribute("pseudoServices", pseudoServices);
							model.addAttribute("serviceList", serviceList);
							model.addAttribute("partnerDetailsList", listPartnerDetails);
							model.addAttribute("serviceExcluded", listOperators);
							model.addAttribute("mesg", model.get("mesg"));
							return "Group/CorporateListPartner";
						}
					} else {
						return "Group/CorporateListPartner";
					}
				}

			} else {
				return "redirect:/Group/Home";
			}

		}
		return "redirect:/Group/Home";

	}

	/**
	 * DELETE PARTNER
	 */

	@RequestMapping(method = RequestMethod.POST, value = "/DeletePartner")
	public ResponseEntity<ResponseDTO> deletePartner(@RequestBody DeleteCorporateDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		ResponseDTO resp = new ResponseDTO();
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE) || user.getAuthority().contains(Authorities.GROUP)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				PartnerDetails partner = partnerDetailsRepository.findOne(Long.parseLong(dto.getId()));
				if (partner != null) {
					try {
						partnerDetailsRepository.delete(partner);
						resp.setCode("S00");
						resp.setMessage("Partner Deleted SuccessFully");
						return new ResponseEntity<>(resp, HttpStatus.OK);
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
						resp.setCode("F00");
						resp.setMessage("Exception Occurred");
						return new ResponseEntity<>(resp, HttpStatus.OK);
					}
				}
			}
		}
		return new ResponseEntity<>(resp, HttpStatus.OK);

	}

	/**
	 * EDIT SERVICES
	 */

	@RequestMapping(method = RequestMethod.POST, value = "/EditPartnerServices")
	public ResponseEntity<ResponseDTO> editPartnerServices(@ModelAttribute DeleteCorporateDTO dto,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		ResponseDTO resp = new ResponseDTO();
		System.err.println("agentId:" + dto.getId());
		// System.err.println(dto.getServices().toString());
		System.err.println(dto.getServices());
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE) || user.getAuthority().contains(Authorities.GROUP)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				PartnerDetails partner = partnerDetailsRepository.findOne(Long.parseLong(dto.getId()));
				if (partner != null) {
					try {
						List<String> serviceList = dto.getServices();
						List<MService> serviceTobeReassigned = new ArrayList<>();
						if (serviceList != null && !serviceList.isEmpty()) {
							partner.getPartnerServices().clear();
							for (String string : serviceList) {
								System.err.println("This is the service" + string);
								MService assignedService = mServiceRepository.findServiceByCode(string);
								System.err.println("Service to be assigned::" + assignedService.getCode());
								serviceTobeReassigned.add(assignedService);
							}
							partner.setPartnerServices(serviceTobeReassigned);
							partnerDetailsRepository.save(partner);
							resp.setCode("S00");
							resp.setMessage("Partner Deleted SuccessFully");
							return new ResponseEntity<>(resp, HttpStatus.OK);
						}
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
						resp.setCode("F00");
						resp.setMessage("Exception Occurred");
						return new ResponseEntity<>(resp, HttpStatus.OK);
					}
				}
			}
		}
		return new ResponseEntity<>(resp, HttpStatus.OK);

	}

	/**
	 * FCM INTEGRATION
	 */

	@RequestMapping(value = "/SendNotification", method = RequestMethod.GET)
	public String sendNotification(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.GROUP)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {

				try {

					return "Group/SendNotifiGroup";
				} catch (Exception e) {
					e.printStackTrace();
					walletResponse.setMessage("Service Unavailable");
					walletResponse.setCode(ResponseStatus.FAILURE.getValue());
					return "Group/SendNotifiGroup";
				}

			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "Group/Login";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "Group/Login";
		}
	}

	@RequestMapping(value = "/SendNotification", method = RequestMethod.POST)
	public String sendNotificationPost(@ModelAttribute SendNotificationDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.GROUP)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {

				try {
					// if(!dto.getForIos().equalsIgnoreCase("ios")){
					if (dto.getForIos() != null) {
						if (dto.getWithImage() != null && dto.getWithImage().equalsIgnoreCase("Yo")) {
							if (dto.getAllUser() != null && dto.getAllUser().equalsIgnoreCase("YoAll")) {
								String imagePath = CommonUtil.uploadFcmImage(dto.getImage(),
										userSession.getUser().getId() + "", "FCMIMAGES");
								FCMDetails fcmDetails = new FCMDetails();
								fcmDetails.setImagePath(imagePath);
								fcmDetails.setMessage(dto.getMessage());
								fcmDetails.setTitle(dto.getTitle());
								fcmDetails.setStatus("Active");
								fcmDetails.setHasImage(true);
								fcmDetails.setAllUsers(true);
								fCMDetailsRepository.save(fcmDetails);
								model.addAttribute("message", "Your FCM content is set on Scheduler");
								return "Group/SendNotifiGroup";
							} else if (dto.getAllUser() != null && dto.getAllUser().equalsIgnoreCase("YoSingle")) {
								String imagePath = CommonUtil.uploadFcmImage(dto.getImage(),
										userSession.getUser().getId() + "", "FCMIMAGES");
								FCMDetails fcmDetails = new FCMDetails();
								fcmDetails.setImagePath(imagePath);
								fcmDetails.setMessage(dto.getMessage());
								fcmDetails.setTitle(dto.getTitle());
								fcmDetails.setStatus("Active");
								fcmDetails.setHasImage(true);
								fcmDetails.setSingleUser(true);
								fcmDetails.setUsername(dto.getUserMobile());
								fCMDetailsRepository.save(fcmDetails);
								model.addAttribute("message", "Your FCM content is set on Scheduler");
								return "Group/SendNotifiGroup";
							}
						} else if (dto.getAllUser() != null && dto.getAllUser().equalsIgnoreCase("YoAll")) {

							FCMDetails fcmDetails = new FCMDetails();
							fcmDetails.setMessage(dto.getMessage());
							fcmDetails.setTitle(dto.getTitle());
							fcmDetails.setStatus("Active");
							fcmDetails.setHasImage(false);
							fcmDetails.setAllUsers(true);
							fCMDetailsRepository.save(fcmDetails);
							model.addAttribute("message", "Your FCM content is set on Scheduler");
							return "Group/SendNotifiGroup";
						} else {
							FCMDetails fcmDetails = new FCMDetails();
							fcmDetails.setMessage(dto.getMessage());
							fcmDetails.setTitle(dto.getTitle());
							fcmDetails.setStatus("Active");
							fcmDetails.setHasImage(false);
							fcmDetails.setSingleUser(true);
							fcmDetails.setUsername(dto.getUserMobile());
							fCMDetailsRepository.save(fcmDetails);
							model.addAttribute("message", "Your FCM content is set on Scheduler");
							return "Group/SendNotifiGroup";
						}
					} else {
						if (CommonUtil.calculatePayload(dto)) {
							if (dto.getWithImage() != null && dto.getWithImage().equalsIgnoreCase("Yo")) {
								if (dto.getAllUser() != null && dto.getAllUser().equalsIgnoreCase("YoAll")) {
									FCMDetails fcmDetails = new FCMDetails();
									fcmDetails.setImagePath(dto.getShortenedimageUrl());
									fcmDetails.setMessage(dto.getMessage());
									fcmDetails.setTitle(dto.getTitle());
									fcmDetails.setStatus("Active");
									fcmDetails.setHasImage(true);
									fcmDetails.setAllUsers(true);
									fcmDetails.setForIos(false);
									fCMDetailsRepository.save(fcmDetails);
									model.addAttribute("message", "Your content is set on Scheduler");
									return "Group/SendNotifiGroup";
								} else if (dto.getAllUser() != null && dto.getAllUser().equalsIgnoreCase("YoSingle")) {
									FCMDetails fcmDetails = new FCMDetails();
									// fcmDetails.setImagePath(dto.getShortenedimageUrl());
									String imagePath = CommonUtil.uploadFcmImage(dto.getImage(),
											userSession.getUser().getId() + "", "FCMIMAGES");
									fcmDetails.setImagePath(imagePath);
									fcmDetails.setMessage(dto.getMessage());
									fcmDetails.setTitle(dto.getTitle());
									fcmDetails.setStatus("Active");
									fcmDetails.setHasImage(true);
									fcmDetails.setSingleUser(true);
									fcmDetails.setForIos(false);
									fcmDetails.setUsername(dto.getUserMobile());
									fCMDetailsRepository.save(fcmDetails);
									model.addAttribute("message", "Your content is set on Scheduler");
									return "Group/SendNotifiGroup";
								}
							} else if (dto.getAllUser() != null && dto.getAllUser().equalsIgnoreCase("YoAll")) {
								FCMDetails fcmDetails = new FCMDetails();
								fcmDetails.setMessage(dto.getMessage());
								fcmDetails.setTitle(dto.getTitle());
								fcmDetails.setStatus("Active");
								fcmDetails.setHasImage(false);
								fcmDetails.setAllUsers(true);
								fcmDetails.setForIos(false);
								fCMDetailsRepository.save(fcmDetails);
								model.addAttribute("message", "Your content is set on Scheduler");
								return "Group/SendNotifiGroup";
							} else {
								FCMDetails fcmDetails = new FCMDetails();
								fcmDetails.setMessage(dto.getMessage());
								fcmDetails.setTitle(dto.getTitle());
								fcmDetails.setStatus("Active");
								fcmDetails.setHasImage(false);
								fcmDetails.setSingleUser(true);
								fcmDetails.setForIos(false);
								fcmDetails.setUsername(dto.getUserMobile());
								fCMDetailsRepository.save(fcmDetails);
								model.addAttribute("message", "Your content is set on Scheduler");
								return "Group/SendNotifiGroup";

							}
						} else {
							model.addAttribute("message",
									"Payload size limit exceeded.The Size should be less than 256 bytes");
							return "Group/SendNotifiGroup";
						}
					}
				} catch (Exception e) {

					e.printStackTrace();
					walletResponse.setMessage("Service Unavailable");
					walletResponse.setCode(ResponseStatus.FAILURE.getValue());
					model.addAttribute("message", "Exception occurred");
					return "Group/SendNotifiGroup";
				}

			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "Group/Login";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "Group/Login";
		}
		return "Group/Login";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/ChangePassword")
	public String getPasswordChange(@ModelAttribute PagingDTO page, ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE) || user.getAuthority().contains(Authorities.GROUP)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);

				model.addAttribute("username", userSession.getUser().getUsername());

				return "Group/CorporateChangePassword";
			} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)) {
				PartnerDetails partnerDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
				if (partnerDetails != null) {
					List<MService> partnerServices = partnerDetails.getPartnerServices();
					if (partnerServices != null && !partnerServices.isEmpty()) {
						for (MService mService : partnerServices) {
							if (mService.getCode().equalsIgnoreCase("BRC")) {
								model.addAttribute("bulkRegistration", true);
							} else if (mService.getCode().equalsIgnoreCase("PREFC")) {
								model.addAttribute("prefundC", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCL")) {
								model.addAttribute("BulkCL", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCS")) {
								model.addAttribute("BulkCardIssuance", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCSL")) {
								model.addAttribute("SingleCardLoad", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCBL")) {
								model.addAttribute("CardBlockUnblock", true);
							} else if (mService.getCode().equalsIgnoreCase("CORSA")) {
								model.addAttribute("singleCardAssignment", true);

							}
						}
					}
				}
				model.addAttribute("UserType", false);
				model.addAttribute("username", userSession.getUser().getUsername());

				return "Group/CorporateChangePassword";

			}
		}
		return "redirect:/Group/Home";
	}

	/**
	 * bulk card assignment
	 * 
	 * @param RegisterDTO
	 * @param request
	 * @param response
	 * @param session
	 * @param model
	 * @return String
	 * @throws ParseException
	 */

	@RequestMapping(method = RequestMethod.POST, value = "/BulkCardAssignment")
	public String bulkCardAssignment(@ModelAttribute RegisterDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) throws ParseException {

		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.GROUP)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionId);

					String rootDirectory = request.getSession().getServletContext().getRealPath("/");
					String path = saveRefundReport(rootDirectory, dto.getFile(), dto.getFileName());
					String file = path.substring(19);
					System.err.println(file);
					String[] splitted = file.split("#");
					System.err.println(splitted[1]);
					String fileName = StartUpUtil.CSV_FILE + file;
					if (fileName != null) {
						try {
							String s3Path = CommonUtil.uploadCsv(dto.getFile(), user.getUserId(), "BLKCARDASSIGNGROUP");
							System.err.println("the s3 path::" + s3Path);
							System.err.println("the username::" + userSession.getUser().getUsername());
							GroupFileCatalogue fileCatalogue = new GroupFileCatalogue();
							fileCatalogue.setAbsPath(splitted[1]);
							fileCatalogue.setUser(userSession.getUser());
							fileCatalogue.setFileDescription(CSVReader.BULK_CARD_ASSIGN);
							fileCatalogue.setS3Path(s3Path);
							fileCatalogue.setCategoryType("BLKCARDASSIGNGROUP");
							groupFileCatalogueRepository.save(fileCatalogue);
							model.addAttribute("sucessMSG",
									"Upload successful.Your Request for Bulk Card Assignment has been taken.Please contact your admin for approval");
						} catch (Exception e) {
							e.printStackTrace();
						}
						return "Group/BulkCardAssign";
					}
				}
			}
		}
		return "redirect:/Group/Home";
	}

	@RequestMapping(value = "/BulkCardAssignment", method = RequestMethod.GET)
	public String getBulkCardAssign(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model) {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.GROUP)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					return "Group/BulkCardAssign";

				}
			}
		}
		return "redirect:/Group/Home";

	}

}
