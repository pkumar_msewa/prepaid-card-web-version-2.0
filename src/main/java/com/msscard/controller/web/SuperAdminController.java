package com.msscard.controller.web;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.amazonaws.Response;
import com.msscard.app.api.IAuthenticationApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.response.CommonResponse;
import com.msscard.entity.MUser;
import com.msscard.model.PagingDTO;
import com.msscard.model.ResponseStatus;
import com.msscard.model.SuperAdminDTO;
import com.msscard.util.Authorities;
import com.msscard.util.CommonUtil;

@Controller
@RequestMapping("/SuperAdmin")
public class SuperAdminController {
	
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	private final IUserApi userApi;
	private final IAuthenticationApi authenticationApi;
	
	public SuperAdminController(IUserApi userApi, IAuthenticationApi authenticationApi) {
		this.userApi = userApi;
		this.authenticationApi = authenticationApi;
	}
	
	@RequestMapping(value="/AddMultipleAdmin", method=RequestMethod.POST)
	public ResponseEntity<CommonResponse> addMultipleAdminFromSuperAdmin(@RequestBody SuperAdminDTO dto, HttpServletRequest request, 
			HttpSession session, ModelMap model) {
		CommonResponse response = new CommonResponse();
		try {
			String sessionId = (String) session.getAttribute(CommonUtil.ADMIN_SESSION_ID);
			if(sessionId != null) {
				String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
				if(authority != null) {
					if((authority.contains(Authorities.ADMINISTRATOR) || authority.contains(Authorities.SUPER_ADMIN) 
							&& authority.contains(Authorities.AUTHENTICATED))) {
							response = userApi.setMultipleAdmin(dto);
					} else {
						response.setCode(ResponseStatus.FAILURE.getValue());
						response.setMessage(ResponseStatus.UNAUTHORIZED_ROLE.getKey());
					}
				} else {
					response.setCode(ResponseStatus.FAILURE.getValue());
					response.setMessage(ResponseStatus.UNAUTHORIZED_USER.getKey());
				}
			} else {
				response.setCode(ResponseStatus.FAILURE.getValue());
				response.setMessage(ResponseStatus.INVALID_SESSION.getKey());
			}
		} catch(Exception e) {
			e.printStackTrace();
			response.setCode(ResponseStatus.FAILURE.getValue());
			response.setMessage("Service Down");
		}
		return new ResponseEntity<CommonResponse>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value="/MultipleAdminList", method= RequestMethod.GET)
	public String getMultipleList(@ModelAttribute PagingDTO dto, @ModelAttribute("fromDate") String fromDate,
			@ModelAttribute("toDate") String toDate,  HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		CommonResponse result = new CommonResponse();
		try {
			String sessionId = (String) session.getAttribute(CommonUtil.ADMIN_SESSION_ID);
			if(sessionId != null) {
				String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
				if(authority != null) {
					if((authority.contains(Authorities.ADMINISTRATOR) || authority.contains(Authorities.SUPER_ADMIN) 
							&& authority.contains(Authorities.AUTHENTICATED))) {
						List<MUser> list = userApi.getAddedAdminList(dto);
						model.addAttribute("adminList", list);
					} else {
						result.setCode(ResponseStatus.FAILURE.getValue());
						result.setMessage(ResponseStatus.UNAUTHORIZED_ROLE.getKey());
					}
				} else {
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage(ResponseStatus.UNAUTHORIZED_USER.getKey());
				}
			} else {
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ResponseStatus.INVALID_SESSION.getKey());
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return "Admin/";
	}
}
