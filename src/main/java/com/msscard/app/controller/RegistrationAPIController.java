package com.msscard.app.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;

import com.ewire.enums.Device;
import com.ewire.errormessages.ErrorMessage;
import com.msscard.app.api.IMCardApi;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.IMdexApi;
import com.msscard.app.api.ITransactionApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.ChangePasswordRequest;
import com.msscard.app.model.request.ForgotPasswordDTO;
import com.msscard.app.model.request.MatchMoveCreateCardRequest;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.request.TransactionalRequest;
import com.msscard.app.model.request.UserKycRequest;
import com.msscard.app.model.request.VerifyMobileDTO;
import com.msscard.app.model.response.FingooleResponse;
import com.msscard.app.model.response.FingooleTransactionListDTO;
import com.msscard.app.model.response.ForgotPasswordError;
import com.msscard.app.model.response.RegisterResponseDTO;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.entity.Donation;
import com.msscard.entity.FingooleRegistration;
import com.msscard.entity.FingooleServices;
import com.msscard.entity.GroupDetails;
import com.msscard.entity.MCommission;
import com.msscard.entity.MMCards;
import com.msscard.entity.MPQAccountDetails;
import com.msscard.entity.MService;
import com.msscard.entity.MServiceType;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.entity.MUserDetails;
import com.msscard.entity.UserSession;
import com.msscard.model.DonationDTO;
import com.msscard.model.FingooleRegisterDTO;
import com.msscard.model.GroupDetailDTO;
import com.msscard.model.GroupDetailsListDTO;
import com.msscard.model.RegisterDTO;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Role;
import com.msscard.model.Status;
import com.msscard.model.UserType;
import com.msscard.model.error.RegisterError;
import com.msscard.model.error.VerifyMobileOTPError;
import com.msscard.repositories.FingooleRegistrationRepository;
import com.msscard.repositories.FingooleServicesRepository;
import com.msscard.repositories.MCommissionRepository;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MPQAccountDetailRepository;
import com.msscard.repositories.MServiceRepository;
import com.msscard.repositories.MTransactionRepository;
import com.msscard.repositories.MUserDetailRepository;
import com.msscard.repositories.UserSessionRepository;
import com.msscard.util.Authorities;
import com.msscard.util.SecurityUtil;
import com.msscard.validation.MobileOTPValidation;
import com.msscard.validation.RegisterValidation;
import com.msscard.validation.TransactionTypeValidation;
import com.msscards.session.PersistingSessionRegistry;
import com.razorpay.constants.RazorPayConstants;

@Controller
@RequestMapping("/Api/{role}/{device}/{language}")
public class RegistrationAPIController {

	private RegisterValidation registerValidation;
	private IUserApi userApi;
	private MobileOTPValidation mobileOTPValidation;
	private MMCardRepository mMCardRepository;
	private IMCardApi iMCardApi;
	private final UserSessionRepository userSessionRepository;
	private final IMatchMoveApi matchMoveApi;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final IMdexApi iMdexApi;
	private final MUserDetailRepository mUserDetailRepository;
	private final ITransactionApi transactionApi;
	private final MCommissionRepository mCommissionRepository;
	private final MServiceRepository mServiceRepository;
	private final MTransactionRepository mTransactionRepository;
	private final MPQAccountDetailRepository mPQAccountDetailRepository;
	private final FingooleServicesRepository fingooleServicesRepository;
	private final FingooleRegistrationRepository fingooleRegistrationRepository;
	private final TransactionTypeValidation transactionTypeValidation;
	private final MServiceRepository serviceRepository;
	
	public RegistrationAPIController(RegisterValidation registerValidation, IUserApi userApi,
			MobileOTPValidation mobileOTPValidation,MMCardRepository mMCardRepository,IMCardApi iMCardApi,
			UserSessionRepository userSessionRepository, MUserDetailRepository mUserDetailRepository,
			IMatchMoveApi matchMoveApi,PersistingSessionRegistry persistingSessionRegistry, IMdexApi iMdexApi,
			ITransactionApi transactionApi, MCommissionRepository mCommissionRepository, MServiceRepository mServiceRepository,
			MTransactionRepository mTransactionRepository, MPQAccountDetailRepository mPQAccountDetailRepository,
			FingooleServicesRepository fingooleServicesRepository, FingooleRegistrationRepository fingooleRegistrationRepository,
			TransactionTypeValidation transactionTypeValidation, MServiceRepository serviceRepository) {
		super();
		this.registerValidation = registerValidation;
		this.userApi = userApi;
		this.mobileOTPValidation = mobileOTPValidation;
		this.mMCardRepository=mMCardRepository;
		this.iMCardApi=iMCardApi;
		this.userSessionRepository=userSessionRepository;
		this.matchMoveApi=matchMoveApi;
		this.persistingSessionRegistry=persistingSessionRegistry;
		this.iMdexApi = iMdexApi;
		this.mUserDetailRepository = mUserDetailRepository;
		this.transactionApi = transactionApi;
		this.mCommissionRepository = mCommissionRepository;
		this.mServiceRepository = mServiceRepository;
		this.mTransactionRepository = mTransactionRepository;
		this.mPQAccountDetailRepository = mPQAccountDetailRepository;
		this.fingooleServicesRepository = fingooleServicesRepository;
		this.fingooleRegistrationRepository = fingooleRegistrationRepository;
		this.transactionTypeValidation = transactionTypeValidation;
		this.serviceRepository = serviceRepository;
	}


	@RequestMapping(value="/Register",method=RequestMethod.POST)
	public ResponseEntity<RegisterResponseDTO> registerNormalUser(@RequestBody RegisterDTO dto,HttpServletRequest request,
			HttpServletResponse response,Model model,@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = true) String hash){
		
		RegisterResponseDTO responseDTO=new RegisterResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
		RegisterError registerError=registerValidation.validateNormalUser(dto);
		if(registerError.isValid()){
			dto.setUserType(UserType.User);
			try {
						userApi.saveUser(dto);
						responseDTO.setStatus(ResponseStatus.SUCCESS);
						responseDTO.setMessage("User Registration Successful and OTP sent to ::" + dto.getContactNo()
											+ " and  verification mail sent to ::" + dto.getEmail() + " .");
									responseDTO.setDetails("User Registration Successful and OTP sent to ::" + dto.getContactNo()
											+ " and  verification mail sent to ::" + dto.getEmail() + " .");

				
				
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				responseDTO.setStatus(ResponseStatus.FAILURE);
				responseDTO.setMessage("Opps!something is wrong please try again....");
				return new ResponseEntity<>(responseDTO,HttpStatus.OK);
			}
			responseDTO.setStatus(ResponseStatus.SUCCESS);
			responseDTO.setMessage("User Registration Successful and OTP sent to ::" + dto.getContactNo()
								+ " and  verification mail sent to ::" + dto.getEmail() + " .");
						responseDTO.setDetails("User Registration Successful and OTP sent to ::" + dto.getContactNo()
								+ " and  verification mail sent to ::" + dto.getEmail() + " .");
		}else{
			responseDTO.setStatus(ResponseStatus.FAILURE);
			responseDTO.setMessage(registerError.getMessage());
		}
	}else{
		responseDTO.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
		responseDTO.setMessage("Unauthorized Role..");
	}
}else{
	responseDTO.setStatus(ResponseStatus.INVALID_HASH);
	responseDTO.setMessage("Invalid Hash..");
}
		return new ResponseEntity<RegisterResponseDTO>(responseDTO,HttpStatus.OK);
}
	
	
	@RequestMapping(value = "/ForgotPassword", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RegisterResponseDTO> forgotPassword(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ForgotPasswordDTO forgot, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		RegisterResponseDTO result = new RegisterResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(forgot, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String username = forgot.getUsername();
				ForgotPasswordError error = registerValidation.forgotPassword(username);
				if (error.isValid() == true) {
					MUser u = userApi.findByUserName(username);
					if (u != null) {
						if (u.getAuthority().contains(Authorities.AUTHENTICATED)) {
							if (u.getMobileStatus() == Status.Active) {
								
								userApi.changePasswordRequest(u);
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Forgot Password");
								result.setDetails("Please, Check your SMS for OTP Code to change your password.");
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Please try again later.");
								result.setDetails("Please try again later.");
							}
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Your account is blocked! Please try after 24 hrs.");
							result.setDetails("Your account is blocked! Please try after 24 hrs.");
						}
					} else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("User doesn't Exist,Please Register.");
						result.setDetails("User doesn't Exist,Please Register.");

					}
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage("Failed, invalid request.");
					result.setDetails(error.getErrorLength());
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<RegisterResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/Activate/Mobile", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RegisterResponseDTO> verifyUserMobile(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VerifyMobileDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		RegisterResponseDTO result = new RegisterResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);

		if (isValidHash) {
			if (role.equalsIgnoreCase("User")||role.equalsIgnoreCase("Merchant")) {
				if (verifyUserMobileToken(dto.getKey(), dto.getMobileNumber())) {
					MUser user=userApi.findByUserName(dto.getMobileNumber());
					//MMCardFetchResponse resp=iMCardApi.generateCard(user);
					MatchMoveCreateCardRequest cardRequest=new MatchMoveCreateCardRequest();
					cardRequest.setEmail(user.getUserDetail().getEmail());
					try {
						cardRequest.setPassword(SecurityUtil.md5(user.getUsername()));
						cardRequest.setUsername(user.getUsername());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					ResponseDTO res=matchMoveApi.fetchMMUser(cardRequest);
					if(res.getCode().equalsIgnoreCase("F00")){
					ResponseDTO resp=matchMoveApi.createUserOnMM(user);
					}
					WalletResponse responseDTO=matchMoveApi.fetchWalletMM(cardRequest);
					if(responseDTO.getCode().equalsIgnoreCase("F00"))
					{
						matchMoveApi.createWallet(cardRequest);
					}else{
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("Failed to activate.Please contact customer care");
						return new ResponseEntity<>(result,HttpStatus.OK);
						
					}
					WalletResponse walletResponse=matchMoveApi.assignVirtualCard(user);
					System.err.println("wallet response:::::::::::___---"+walletResponse);
					if(walletResponse.getCode().equalsIgnoreCase("S00")){
						result.setMessage("Activate Mobile||Card Generated");
					
					}else{
						result.setMessage("Activate Mobile");
					}
				   	result.setStatus(ResponseStatus.SUCCESS);
					result.setDetails("Your Mobile is Successfully Verified");
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage("Activate Mobile");
					result.setDetails("Invalid Activation Key");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid User");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}

		return new ResponseEntity<RegisterResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Resend/Mobile/OTP", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RegisterResponseDTO> generateNewOtp(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VerifyMobileDTO dto, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
		RegisterResponseDTO result = new RegisterResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String username = dto.getMobileNumber();
				VerifyMobileOTPError error = new VerifyMobileOTPError();
				error = mobileOTPValidation.validateResendOTP(username);
				if (error.isValid()) {
					userApi.resendMobileTokenNew(username);
					result.setStatus(ResponseStatus.SUCCESS); 
					result.setMessage("Resend OTP");
					result.setDetails("New OTP sent on " + username);
				} else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("Mobile Number Already Verified.");
					result.setDetails(error.getOtp());
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid user");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<RegisterResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "ChangePasswordOTP", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RegisterResponseDTO> changePassword(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ChangePasswordRequest dto, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		RegisterResponseDTO result = new RegisterResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")||role.equalsIgnoreCase("Merchant")) {
				ForgotPasswordError error = registerValidation.forgotPassword(dto);
				if(error.isValid()){
				if (userApi.changePassword(dto)) {
				   	result.setStatus(ResponseStatus.SUCCESS);
					result.setDetails("Password successfully Updated");
				} else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("Not a valid OTP");
					result.setDetails("Not a valid OTP");
				}}else{
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage(error.getMessage());
					result.setDetails(error.getMessage());
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid User");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}

		return new ResponseEntity<RegisterResponseDTO>(result, HttpStatus.OK);
	}


	@RequestMapping(value = "/ChangePassword", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RegisterResponseDTO> changePasswrd(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ChangePasswordRequest dto, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response,HttpSession session) {
		RegisterResponseDTO result = new RegisterResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		String sessionId=dto.getSessionId();
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		System.err.println(sessionId);
		if(userSession!=null){
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")||role.equalsIgnoreCase("Merchant")) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				ForgotPasswordError error = registerValidation.changePassword(dto);
				if(error.isValid()){
					System.err.println("hi i am herer");
					dto.setUsername(userSession.getUser().getUsername());
				if (userApi.changeProfilePassword(dto)) {
				   	result.setStatus(ResponseStatus.SUCCESS);
					result.setDetails("Password successfully Updated");
				} else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("Error occured while operation");
					result.setDetails("Error occured while operation");
				}}else{
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage(error.getMessage());
					result.setDetails(error.getMessage());
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid User");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
	}else{
		result.setStatus(ResponseStatus.INVALID_SESSION);
		result.setMessage("Invalid Session, Please login and try again");
	}

		return new ResponseEntity<RegisterResponseDTO>(result, HttpStatus.OK);
	}

	
	@RequestMapping(value = "/ChangePasswordWeb", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RegisterResponseDTO> changePasswrdWeb(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ChangePasswordRequest dto, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response,HttpSession session) {
		RegisterResponseDTO result = new RegisterResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		String sessionId=session.getId();
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		System.err.println(sessionId);
		if(userSession!=null){
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")||role.equalsIgnoreCase("Merchant")) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				ForgotPasswordError error = registerValidation.changePassword(dto);
				if(error.isValid()){
					System.err.println("hi i am herer");
					dto.setUsername(userSession.getUser().getUsername());
				if (userApi.changeProfilePassword(dto)) {
				   	result.setStatus(ResponseStatus.SUCCESS);
					result.setDetails("Password successfully Updated");
				} else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("Error occured while operation");
					result.setDetails("Error occured while operation");
				}}else{
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage(error.getMessage());
					result.setDetails(error.getMessage());
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid User");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
	}else{
		result.setStatus(ResponseStatus.INVALID_SESSION);
		result.setMessage("Invalid Session, Please login and try again");
	}

		return new ResponseEntity<RegisterResponseDTO>(result, HttpStatus.OK);
	}

	
	

	private boolean verifyUserMobileToken(String key, String mobileNumber) {
		if (userApi.checkMobileToken(key, mobileNumber)) {
			return true;
		} else {
			return false;
		}
		
	}
	
	@RequestMapping(value="/CustomRegister",method=RequestMethod.POST)
	public ResponseEntity<RegisterResponseDTO> customRegisterNormalUser(@RequestBody RegisterDTO dto,HttpServletRequest request,
			HttpServletResponse response,Model model,@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = true) String hash){
		
		RegisterResponseDTO responseDTO=new RegisterResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				System.err.println("before validation");
		RegisterError registerError=registerValidation.customRegisterError(dto);
		System.err.println(registerError);
		if(registerError.isValid()){
			dto.setUserType(UserType.User);
			try {
						dto.setLastLoginDevice(device);
						userApi.saveCustomUser(dto);
						responseDTO.setStatus(ResponseStatus.SUCCESS);
						responseDTO.setMessage("User Registration Successful and OTP sent to ::" + dto.getContactNo());
						responseDTO.setDetails("User Registration Successful and OTP sent to ::" + dto.getContactNo());
						
				
				
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				responseDTO.setStatus(ResponseStatus.FAILURE);
				responseDTO.setMessage("Opps!something is wrong please try again....");
				return new ResponseEntity<>(responseDTO,HttpStatus.OK);
			}
			
		}else{
			responseDTO.setStatus(ResponseStatus.FAILURE);
			responseDTO.setMessage(registerError.getMessage());
			responseDTO.setFullyFilled(registerError.isFullyFilled());
			responseDTO.setHasError(registerError.isHasError());
		}
	}else{
		responseDTO.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
		responseDTO.setMessage("Unauthorized Role..");
	}
}else{
	responseDTO.setStatus(ResponseStatus.INVALID_HASH);
	responseDTO.setMessage("Invalid Hash..");
}
		return new ResponseEntity<RegisterResponseDTO>(responseDTO,HttpStatus.OK);
}

	@RequestMapping(value = "/CustomActivate/Mobile", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RegisterResponseDTO> customVerifyUserMobile(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VerifyMobileDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		RegisterResponseDTO result = new RegisterResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);

		if (isValidHash) {
			if (role.equalsIgnoreCase("User")||role.equalsIgnoreCase("Merchant")) {
				if (verifyUserMobileToken(dto.getKey(), dto.getMobileNumber())) {
						result.setMessage("Activate Mobile");
						result.setStatus(ResponseStatus.SUCCESS);
						result.setDetails("Activate Mobile");
						MUser user=userApi.findByUserName(dto.getMobileNumber());
						result.setFullyFilled(user.isFullyFilled());
					}else{
						result.setStatus(ResponseStatus.BAD_REQUEST);
						result.setMessage("Activate Mobile");
						result.setDetails("Invalid Activation Key");
					}
				   	
				 }else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Unauthorized User");
					result.setDetails("Invalid Activation Key");
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_HASH);
				result.setMessage("Not a valid Hash");
			}

		return new ResponseEntity<RegisterResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value="/RegisterFullDetails",method=RequestMethod.POST)
	public ResponseEntity<RegisterResponseDTO> customRegisterFullDetails(@RequestBody RegisterDTO dto,HttpServletRequest request,
			HttpServletResponse response,Model model,@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = true) String hash) {
		
		RegisterResponseDTO responseDTO=new RegisterResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				System.err.println("before validation");
		RegisterError registerError=registerValidation.validateFullDetails(dto);
		System.err.println(registerError);
		if(registerError.isValid()) {
			try {
					ResponseDTO resp = new ResponseDTO();
						userApi.saveFullDetails(dto);
						//userApi.saveUser(dto);
						MUser user=userApi.findByUserName(dto.getContactNo());
						resp = matchMoveApi.createUserOnMM(user);
						if(resp.getCode().equals(ResponseStatus.SUCCESS.getValue())) {	
							MService service=serviceRepository.findServiceByCode(RazorPayConstants.SERVICE_CODE);
							responseDTO.setStatus(ResponseStatus.SUCCESS.getKey());
							responseDTO.setCode(ResponseStatus.SUCCESS.getValue());
							responseDTO.setMessage("You have completed minimum KYC for doing transactions upto INR "+service.getMaxAmount()+". Please upgrade your KYC with OSV to get maximum benefit of the application");
							responseDTO.setDetails("You have completed minimum KYC for doing transactions upto INR "+service.getMaxAmount()+". Please upgrade your KYC with OSV to get maximum benefit of the application");
							responseDTO.setFullyFilled(true);
						} else {
							responseDTO.setCode(ResponseStatus.SUCCESS.getValue());
							responseDTO.setStatus(ResponseStatus.FAILURE.getKey());
							responseDTO.setMessage("Unable to create Profile");
							responseDTO.setDetails("Unable to create Profile");
							responseDTO.setFullyFilled(true);
						}
					//	matchMoveApi.tempKycUserMM(user.getUserDetail().getEmail(),user.getUsername());
						//matchMoveApi.tempSetIdDetails(user.getUserDetail().getEmail(),user.getUsername());
						
					//	System.err.println("attempting 2nd time::::::::::::::::::::::::::::::::");
						//matchMoveApi.tempSetIdDetails(user.getUserDetail().getEmail(),user.getUsername());

					/*	
					 *  matchMoveApi.tempSetImagesForKyc(user.getUserDetail().getEmail(),user.getUsername());
						matchMoveApi.tempConfirmKyc(user.getUserDetail().getEmail(),user.getUsername());
						matchMoveApi.tempKycStatusApproval(user.getUserDetail().getEmail(), user.getUsername());
						
						*/
						
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				responseDTO.setStatus(ResponseStatus.FAILURE);
				responseDTO.setMessage("Opps!something is wrong please try again....");
				return new ResponseEntity<>(responseDTO,HttpStatus.OK);
			}
			
		}else{
			responseDTO.setStatus(ResponseStatus.FAILURE);
			responseDTO.setMessage(registerError.getMessage());
		}
	}else{
		responseDTO.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
		responseDTO.setMessage("Unauthorized Role..");
	}
}else{
	responseDTO.setStatus(ResponseStatus.INVALID_HASH);
	responseDTO.setMessage("Invalid Hash..");
}
		return new ResponseEntity<RegisterResponseDTO>(responseDTO,HttpStatus.OK);
}
	
	@RequestMapping(value = "/ValidateEmail", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RegisterResponseDTO> validateEmail(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody RegisterDTO dto, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		RegisterResponseDTO result = new RegisterResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);

		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				RegisterError registerError=registerValidation.validateEmail(dto);
				if (registerError.isValid()) {
					result.setCode("S00");
					result.setMessage("proceed Ahead");
					}else{
						result.setCode("F00");
						result.setMessage(registerError.getMessage());
					}	
				 } else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Unauthorized User");
					result.setDetails("Invalid Activation Key");
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_HASH);
				result.setMessage("Not a valid Hash");
			}

		return new ResponseEntity<RegisterResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value="/Group", method= RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<GroupDetailDTO> getGroupDetails(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language) {
		GroupDetailDTO result = new GroupDetailDTO();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					List<GroupDetailsListDTO> details = userApi.getGroupDetailsmobile();
					
					result.setGroupName(details);
					result.setCode(ResponseStatus.SUCCESS.getValue());
					result.setStatus(ResponseStatus.SUCCESS.getKey());
					result.setSuccess(true);
					result.setMessage("Success");				
				} else {
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage(ErrorMessage.DEVICE_MSG);
					result.setSuccess(false);
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.AUTHORITY_MSG);
				result.setSuccess(false);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<GroupDetailDTO>(result, HttpStatus.OK);		
	}

	@RequestMapping(value="/DonationList", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<DonationDTO> getDonationDetails(@PathVariable(value = "role") String role, 
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language) {
		DonationDTO result = new DonationDTO();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				List<Donation> donationList = userApi.getDonationDetails();
				result.setDonationList(donationList);
				result.setCode(ResponseStatus.SUCCESS.getValue());
				result.setStatus(ResponseStatus.SUCCESS.getKey());
				result.setSuccess(true);
				result.setMessage("Success");	
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.DEVICE_MSG);
				result.setSuccess(false);
			}
		} else {
			result.setStatus(ResponseStatus.FAILURE.getKey());
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage(ErrorMessage.AUTHORITY_MSG);
			result.setSuccess(false);
		}
		return new ResponseEntity<DonationDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value="/Fingoole/Services", method=RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<FingooleResponse> fingooleServices(@PathVariable(value = "role") String role, 
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody FingooleRegisterDTO dto) {
		FingooleResponse result = new FingooleResponse();
		try {
			if(role.equalsIgnoreCase(Role.USER.getValue())) {
				if(device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					String sessionId = dto.getSessionId();	
					if(sessionId !=null) {
						result = iMdexApi.getServices();
						result.setCode(ResponseStatus.SUCCESS.getValue());
						result.setStatus(ResponseStatus.SUCCESS.getKey());
						result.setSuccess(true);
					} else {
						result.setStatus(ResponseStatus.FAILURE.getKey());
						result.setCode(ResponseStatus.FAILURE.getValue());
						result.setMessage(ResponseStatus.INVALID_SESSION.getKey());
						result.setSuccess(false);
					}					
				} else {
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage(ErrorMessage.DEVICE_MSG);
					result.setSuccess(false);
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.AUTHORITY_MSG);
				result.setSuccess(false);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<FingooleResponse>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value="/Fingoole/Amount", method=RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<FingooleResponse> getFingooleAmount(@PathVariable(value = "role") String role, 
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody FingooleRegisterDTO dto) {
		FingooleResponse result = new FingooleResponse();
		try {
			if(role.equalsIgnoreCase(Role.USER.getValue())) {
				if(device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					String sessionId = dto.getSessionId();	
					if(sessionId != null) {
						UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
						MUser user = userSession.getUser();
						if(user != null) {
							FingooleServices service = fingooleServicesRepository.getServiceBasedOnCode(dto.getBatchNumber());
							if(service != null) {
								if(dto.getBatchNumber().equalsIgnoreCase(service.getCode())) {
									result = iMdexApi.batchNumber(dto);
									result.setAmount(service.getAmount());
									result.setBatch(result.getBatch());
									result.setSuccess(true);
									result.setCode(ResponseStatus.SUCCESS.getValue());
									result.setStatus(ResponseStatus.SUCCESS.getKey());
								} else {
									result.setStatus(ResponseStatus.FAILURE.getKey());
									result.setCode(ResponseStatus.FAILURE.getValue());
									result.setMessage("Invalid batch number");
									result.setSuccess(false);
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE.getKey());
								result.setCode(ResponseStatus.FAILURE.getValue());
								result.setMessage("Service does not exist");
								result.setSuccess(false);
							}						
						} else {
							result.setStatus(ResponseStatus.FAILURE.getKey());
							result.setCode(ResponseStatus.FAILURE.getValue());
							result.setMessage("User does not Exist");
							result.setSuccess(false);
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION.getKey());
						result.setCode(ResponseStatus.INVALID_SESSION.getValue());
						result.setMessage(ErrorMessage.SESSION_LOGOUT_ERROR);
						result.setSuccess(false);
					}
				} else {
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage(ErrorMessage.DEVICE_MSG);
					result.setSuccess(false);
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.AUTHORITY_MSG);
				result.setSuccess(false);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<FingooleResponse>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value="/Fingoole/Register", method=RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE }) 
	public ResponseEntity<FingooleResponse> fingooleRegister(@PathVariable(value = "role") String role, 
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody FingooleRegisterDTO dto) {
		FingooleResponse result = new FingooleResponse();
		try {
			if(role.equalsIgnoreCase(Role.USER.getValue())) {
				if(device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					String sessionId = dto.getSessionId();	
					if(sessionId != null) {
						UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
						MUser user = userSession.getUser();
						dto.setBatchNumber(dto.getBatchNumber());
						dto.setEmail(user.getUserDetail().getEmail());							
						result = iMdexApi.fingooleRegister(dto);
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION.getKey());
						result.setCode(ResponseStatus.INVALID_SESSION.getValue());
						result.setMessage(ResponseStatus.INVALID_SESSION.getKey());
						result.setSuccess(false);
					}					
				} else {
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage(ErrorMessage.DEVICE_MSG);
					result.setSuccess(false);
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.AUTHORITY_MSG);
				result.setSuccess(false);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<FingooleResponse>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value="/Fingoole/Process", method=RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<FingooleResponse> processTransaction(@PathVariable(value = "role") String role, 
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody FingooleRegisterDTO dto) {
		FingooleResponse result = new FingooleResponse();
		try {
			if(role.equalsIgnoreCase(Role.USER.getValue())) {
				if(device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					String sessionId = dto.getSessionId();	
					if(sessionId != null) {		
						UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
						MUser user = userSession.getUser();
						MUserDetails detail = user.getUserDetail();
						System.err.println("registration list:: "+dto.getRegistrationData());
						System.err.println("batch:: "+dto.getBatchNumber());
						for(int i = 0; i < dto.getRegistrationData().size(); i++) {
							FingooleRegistration fr = new FingooleRegistration();
							fr.setName(dto.getRegistrationData().get(i).getName());
							fr.setAmount(dto.getRegistrationData().get(i).getAmount());
							fr.setBatchNumber(dto.getBatchNumber());
							fr.setMobileNumber(dto.getRegistrationData().get(i).getMobileNumber());
							fr.setNoOfDays(dto.getRegistrationData().get(i).getNoOfDays());
							fr.setServiceProvider(dto.getRegistrationData().get(i).getServiceProvider());
							fr.setUser(user);
							fingooleRegistrationRepository.save(fr);
						}						
						MService service = mServiceRepository.findServiceByCode("FINGL");
						MCommission commission = mCommissionRepository.findCommissionByService(service);						
						List<FingooleRegistration> reg = fingooleRegistrationRepository.getRegistrationByUserAndBatch(user, dto.getBatchNumber());
						Double amount = 0.0;
						for(int i = 0; i < reg.size(); i++) {
							Double d1 = new Double(reg.get(i).getAmount());
							amount += d1.doubleValue();
						}						
						TransactionalRequest request = new TransactionalRequest();						
						request.setAmount(amount);
						request.setAndroid(true);
						request.setDescription("Fingoole Insurance Transaction");
						request.setCommission(commission);
						request.setDescription("Fingoole Insurance");
						request.setService(service);
						request.setUser(user);
						request.setBatch(dto.getBatchNumber());
						//if(user.getAccountDetail().getAccountType().getCode().equals("KYC")) {
							double balance = matchMoveApi.getBalanceApp(user);	
							if(amount < balance) {
								MTransaction transaction = transactionApi.initiateFingooleTransaction(request);						
								dto.setBatchNumber(dto.getBatchNumber());
								dto.setTransactionRefNo(transaction.getTransactionRefNo());
								result = iMdexApi.fingooleService(dto);								
								if(result.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
									for(int i = 0; i <result.getCustomerTransactionList().size(); i++) {
										FingooleRegistration ref = fingooleRegistrationRepository.getUserByBatch(result.getCustomerTransactionList().get(i).getCustomerName(), result.getBatch());
										if(ref != null) {
											ref.setCoino(result.getCustomerTransactionList().get(i).getCoino());
											ref.setCoiurl(result.getCustomerTransactionList().get(i).getCoiurl());
											ref.setTransactionRefNo(transaction.getTransactionRefNo());
											fingooleRegistrationRepository.save(ref);
										}
									}
									MTransaction transactionf = transactionApi.getTransactionByRefNo(transaction.getTransactionRefNo());		
									if(transactionf != null) {								
										MMCards cards1 = mMCardRepository.getPhysicalCardByUser(user);
										if(cards1 != null) {
											if(!cards1.isBlocked()) {
												UserKycResponse response = matchMoveApi.debitFromCardFingoole(user.getUserDetail().getEmail(), user.getUsername(), cards1.getCardId(), String.valueOf(transactionf.getAmount()));
												if(response.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
													double balance1 = matchMoveApi.getBalanceApp(user);	
													transactionf.setStatus(Status.Success);
													transactionf.setCardLoadStatus(Status.Success.getValue());
													transactionf.setSuspicious(false);
													transactionf.setAuthReferenceNo(response.getIndicator());
													transactionf.setCurrentBalance(balance1);
													transactionf.setMdexTransactionStatus(Status.Success.getValue());
													mTransactionRepository.save(transactionf);
													MPQAccountDetails account = user.getAccountDetail();
													account.setBalance(balance1);
													mPQAccountDetailRepository.save(account);
													result.setStatus(ResponseStatus.SUCCESS.getKey());
													result.setCode(ResponseStatus.SUCCESS.getValue());
													result.setMessage("Transaction Successfull");
													result.setSuccess(true);	
													return new ResponseEntity<FingooleResponse>(result, HttpStatus.OK);
												} else {
													result.setStatus(ResponseStatus.FAILURE.getKey());
													result.setCode(ResponseStatus.FAILURE.getValue());
													result.setMessage(response.getMessage());
													result.setSuccess(false);
												}
											} else {
												MMCards cards2 = mMCardRepository.getVirtualCardsByCardUser(user);
												if(cards2!= null) {
													if(!cards2.isBlocked()) {
														UserKycResponse response = matchMoveApi.debitFromCardFingoole(user.getUserDetail().getEmail(), user.getUsername(), cards2.getCardId(), String.valueOf(transactionf.getAmount()));
														if(response.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
															double balance2 = matchMoveApi.getBalanceApp(user);	
															transactionf.setStatus(Status.Success);
															transactionf.setSuspicious(false);
															transactionf.setAuthReferenceNo(response.getIndicator());
															transactionf.setMdexTransactionStatus(Status.Success.getValue());
															transactionf.setCardLoadStatus(Status.Success.getValue());
															transactionf.setCurrentBalance(balance2);
															mTransactionRepository.save(transactionf);
															MPQAccountDetails account = user.getAccountDetail();
															account.setBalance(balance2);
															mPQAccountDetailRepository.save(account);
															result.setStatus(ResponseStatus.SUCCESS.getKey());
															result.setCode(ResponseStatus.SUCCESS.getValue());
															result.setMessage("Transaction Successfull");
															result.setSuccess(true);	
															return new ResponseEntity<FingooleResponse>(result, HttpStatus.OK);
														} else {
															result.setStatus(ResponseStatus.FAILURE.getKey());
															result.setCode(ResponseStatus.FAILURE.getValue());
															result.setMessage(response.getMessage());
															result.setSuccess(false);
														}
													} else {
														result.setStatus(ResponseStatus.FAILURE.getKey());
														result.setCode(ResponseStatus.FAILURE.getValue());
														result.setMessage("Card is Blocked");
														result.setSuccess(false);
													}
												} else {
													result.setStatus(ResponseStatus.FAILURE.getKey());
													result.setCode(ResponseStatus.FAILURE.getValue());
													result.setMessage("card does not exist");
													result.setSuccess(false);
												}
											}
										} else {
											System.err.println("in virtual card fingoole insurance");
											MMCards card3 = mMCardRepository.getVirtualCardsByCardUser(user);
											if(card3 != null) {
												if(!card3.isBlocked()) {
													UserKycResponse response = matchMoveApi.debitFromCardFingoole(user.getUserDetail().getEmail(), user.getUsername(), card3.getCardId(), String.valueOf(transactionf.getAmount()));
													if(response.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
														double balance3 = matchMoveApi.getBalanceApp(user);	
														transactionf.setStatus(Status.Success);
														transactionf.setSuspicious(false);
														transactionf.setAuthReferenceNo(response.getIndicator());
														transactionf.setCardLoadStatus(Status.Success.getValue());
														transactionf.setMdexTransactionStatus(Status.Success.getValue());
														transactionf.setCurrentBalance(balance3);
														mTransactionRepository.save(transactionf);
														MPQAccountDetails account = user.getAccountDetail();
														account.setBalance(balance3);
														mPQAccountDetailRepository.save(account);
														result.setStatus(ResponseStatus.SUCCESS.getKey());
														result.setCode(ResponseStatus.SUCCESS.getValue());
														result.setMessage("Transaction Successfull");
														result.setSuccess(true);	
														return new ResponseEntity<FingooleResponse>(result, HttpStatus.OK);
													} else {
														result.setStatus(ResponseStatus.FAILURE.getKey());
														result.setCode(ResponseStatus.FAILURE.getValue());
														result.setMessage(response.getMessage());
														result.setSuccess(false);
													}
												} else {
													result.setStatus(ResponseStatus.FAILURE.getKey());
													result.setCode(ResponseStatus.FAILURE.getValue());
													result.setMessage("Card is Blocked");
													result.setSuccess(false);
												}
											} else {
												result.setStatus(ResponseStatus.FAILURE.getKey());
												result.setCode(ResponseStatus.FAILURE.getValue());
												result.setMessage("card does not exist");
												result.setSuccess(false);
											}
										}
									} else {
										result.setStatus(ResponseStatus.FAILURE.getKey());
										result.setCode(ResponseStatus.FAILURE.getValue());
										result.setMessage("Transaction Does not exist");
										result.setSuccess(false);
									}
								} else {
									result.setStatus(ResponseStatus.FAILURE.getKey());
									result.setCode(ResponseStatus.FAILURE.getValue());
									result.setMessage("Transaction Failed");
									result.setSuccess(false);
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE.getKey());
								result.setCode(ResponseStatus.FAILURE.getValue());
								result.setMessage("Low Balance");
								result.setSuccess(false);
							}
					/*	} else {
							result.setStatus(ResponseStatus.FAILURE.getKey());
							result.setCode(ResponseStatus.FAILURE.getValue());
							result.setMessage("Non KYC user");
							result.setSuccess(false);
						}*/
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION.getKey());
						result.setCode(ResponseStatus.INVALID_SESSION.getValue());
						result.setMessage(ResponseStatus.INVALID_SESSION.getKey());
						result.setSuccess(false);
					}					
				} else {
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage(ErrorMessage.DEVICE_MSG);
					result.setSuccess(false);
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.AUTHORITY_MSG);
				result.setSuccess(false);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<FingooleResponse>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value="/Fingoole/TransactionList", method=RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<FingooleResponse> getTransactionList(@PathVariable(value = "role") String role, 
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody FingooleRegisterDTO dto) {
		FingooleResponse result = new FingooleResponse();
		try {
			String sessionId = dto.getSessionId();	
			if(sessionId != null) {		
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				MUser user = userSession.getUser();				
				ArrayList<FingooleTransactionListDTO> tran = new ArrayList<FingooleTransactionListDTO>();
				List<FingooleRegistration> reference = fingooleRegistrationRepository.getListByUsersNoBatch(user);
				for(int i = 0; i < reference.size(); i++) {
					FingooleTransactionListDTO list = new FingooleTransactionListDTO();
					list.setAmount(reference.get(i).getAmount());				
					list.setCoi(reference.get(i).getCoino());
					list.setDate(reference.get(i).getCreated().toLocaleString());
					list.setMobile(reference.get(i).getMobileNumber());
					list.setName(reference.get(i).getName());
					list.setTransactionNo(reference.get(i).getTransactionRefNo());
					list.setDescription("Fingoole Insurance");
					tran.add(list);
				}				
				result.setCode(ResponseStatus.SUCCESS.getValue());
				result.setStatus(ResponseStatus.SUCCESS.getKey());
				result.setSuccess(true);
				result.setTransactions(tran);
				result.setMessage("Transaction List");
			} else {
				result.setCode(ResponseStatus.INVALID_SESSION.getValue());
				result.setMessage(ResponseStatus.INVALID_SESSION.getKey());
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<FingooleResponse>(result, HttpStatus.OK);
	}
	
}