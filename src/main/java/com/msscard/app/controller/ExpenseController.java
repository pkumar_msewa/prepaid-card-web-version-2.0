package com.msscard.app.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.CreateRoleDTO;
import com.msscard.entity.CorporateRoles;
import com.msscard.entity.UserSession;
import com.msscard.model.UserDTO;
import com.msscard.repositories.CorporateRolesRepository;
import com.msscard.repositories.MServiceRepository;
import com.msscard.repositories.UserSessionRepository;
import com.msscard.util.Authorities;

/**
 * EXPENSE MANAGEMENT CONTROLLER
 * */
@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}/ExpM")
public class ExpenseController {

	private final CorporateRolesRepository corporateRolesRepository;
	private final MServiceRepository mServiceRepository;
	private final UserSessionRepository userSessionRepository;
	private final IUserApi userApi;
	
	
	
	public ExpenseController(CorporateRolesRepository corporateRolesRepository, MServiceRepository mServiceRepository,
			UserSessionRepository userSessionRepository, IUserApi userApi) {
		super();
		this.corporateRolesRepository = corporateRolesRepository;
		this.mServiceRepository = mServiceRepository;
		this.userSessionRepository = userSessionRepository;
		this.userApi = userApi;
	}



	/*@RequestMapping(value="/CreateRole",method=RequestMethod.POST)
	public ResponseEntity<Object> createRole(@RequestBody CreateRoleDTO dto,@PathVariable(value="role") String role,@PathVariable(value="device") String device,@PathVariable(value="language") String language,
			HttpServletRequest httpServletRequest,HttpServletResponse httpServletResponse,HttpSession session){
		
		String sessionId = (String) session.getAttribute("corporateSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				String roleName=dto.getName().trim();
				CorporateRoles roles=corporateRolesRepository.getByCode(roleName);
				if(roles==null){
					
				}
		
	}
		}
	
	}
*/			
}
