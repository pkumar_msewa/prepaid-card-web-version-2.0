package com.msscard.app.controller;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ewire.enums.Device;
import com.ewire.errormessages.ErrorMessage;
import com.msscard.app.api.ITravelFlightApi;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.request.SessionDTO;
import com.msscard.app.model.response.CommonResponse;
import com.msscard.entity.MService;
import com.msscard.entity.MUser;
import com.msscard.entity.UserSession;
import com.msscard.model.AirBookRQ;
import com.msscard.model.CreateJsonRequestFlight;
import com.msscard.model.FlightBookReq;
import com.msscard.model.FlightCityList;
import com.msscard.model.FlightPriceCheckRequest;
import com.msscard.model.FlightResponseDTO;
import com.msscard.model.MobileFlightBookRequest;
import com.msscard.model.PaymentType;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Role;
import com.msscard.model.Status;
import com.msscard.model.TravelFlightRequest;
import com.msscard.model.TravellerFlightDetails;
import com.msscard.repositories.UserSessionRepository;
import com.msscard.util.CommonUtil;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/Travel/Flight")
public class FlightController {	
	
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private final ITravelFlightApi travelflightapi;
	private final UserSessionRepository userSessionRepository;
	
	public FlightController(ITravelFlightApi travelflightapi, UserSessionRepository userSessionRepository) {
		this.travelflightapi = travelflightapi;
		this.userSessionRepository = userSessionRepository;
	}
	
	@RequestMapping(value="/SetFlightService", method=RequestMethod.GET)
	ResponseEntity<CommonResponse> setFlightService(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language) {
		CommonResponse result = new CommonResponse();
		result = travelflightapi.setFlightService();
		return new ResponseEntity<CommonResponse>(result, HttpStatus.OK);
	}
	 
	@RequestMapping(value = "/Source", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<String> processFlightSource(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody TravelFlightRequest req) {
		String source = travelflightapi.getAirlineNames();
		return new ResponseEntity<String>(source, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/OneWay", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<String> processFlightOneWay(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody TravelFlightRequest req) {

		System.err.println("Flight Search");
		String source = travelflightapi.getFlightSearchAPI(req);
		return new ResponseEntity<String>(source, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/ShowPriceDetail", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<String> processFlightShowPriceDetail(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody FlightPriceCheckRequest req) {
		String airRePriceRQ = travelflightapi.AirRePriceRQ(req);
		return new ResponseEntity<String>(airRePriceRQ, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/ConnectingShowPriceDetail", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<String> processConectingFlightShowPriceDetail(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody FlightPriceCheckRequest req) {

		String airRePriceRQ = travelflightapi.mobileAirRePriceRQConnecting(req);
		return new ResponseEntity<String>(airRePriceRQ, HttpStatus.OK);
	}
	

	/*@RequestMapping(method = RequestMethod.POST, value = "/Flight/CheckOut", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> getFlightCheckOutPostMethod(@RequestBody MobileFlightBookRequest req) {
		try {

			List<TravellerFlightDetails> travellerFlightDetails=new ArrayList<>();

			String flightBookingInitiate = travelflightapi.FlightBookingInitiate(req.getSessionId(), req.getFirstName(),
					req.getGrandtotal(), req.getTicketDetails(), "Wallet");

			JSONObject objflightBookingInitiate = new JSONObject(flightBookingInitiate);
			String codeflightBookingInitiate = objflightBookingInitiate.getString("code");
			if (codeflightBookingInitiate.equalsIgnoreCase("S00")) {
				String transactionRefNo = objflightBookingInitiate.getJSONObject("details")
						.getString("transactionRefNo");
				req.setTransactionId(transactionRefNo);

				String airRePriceRQ = travelflightapi.AirBookRQForMobile(req);
				JSONObject obj = new JSONObject(airRePriceRQ);
				String code = obj.getString("code");
				if (code.equalsIgnoreCase("S00")) {
					String bookingRefId = obj.getJSONObject("details").getString("bookingRefId");
					String transactionRefNomdex = obj.getJSONObject("details").getString("transactionRefNo");
					JSONArray objcc = obj.getJSONObject("details").getJSONObject("bookingDetail")
							.getJSONObject("pnrDetail").getJSONArray("tickets");
					String ticketNumber = "";
					String firstName = "";

					for (int i = 0; i < objcc.length(); i++) {
						ticketNumber += "~" + objcc.getJSONObject(i).getString("ticketNumber");
						firstName += "~" + objcc.getJSONObject(i).getString("firstName");
					}
					JSONArray objccpnrs = obj.getJSONObject("details").getJSONObject("bookingDetail")
							.getJSONObject("pnrDetail").getJSONArray("pnrs");
					String pnrNo = objccpnrs.getJSONObject(0).getString("pnr");

					String flightBookingSucess = travelflightapi.FlightBookingSucess(req.getSessionId(), ticketNumber,
							firstName, bookingRefId, transactionRefNomdex, req.getGrandtotal(), req.getTicketDetails(),
							"Booked", "Success", "Wallet", transactionRefNo, true,req.getEmailAddress(),req.getMobileNumber());

					JSONObject flightBookingSucessobj = new JSONObject(flightBookingSucess);
					String flightBookingSucesscode = flightBookingSucessobj.getString("code");
					if (flightBookingSucesscode.equalsIgnoreCase("S00")) {
						return new ResponseEntity<String>("" + flightBookingSucess, HttpStatus.OK);
					} else {
						travelflightapi.FlightBookingSucess(req.getSessionId(), ticketNumber,
								firstName, bookingRefId, transactionRefNomdex, req.getGrandtotal(), req.getTicketDetails(),
								"Booked", "Success", "Wallet", transactionRefNo, false,req.getEmailAddress(),req.getMobileNumber());
						return new ResponseEntity<String>(airRePriceRQ, HttpStatus.OK);
					}
				} else {
					travelflightapi.FlightBookingSucess(req.getSessionId(), "Flight Not booked", req.getFirstName(),
							"Flight Not booked", "Flight Not booked", req.getGrandtotal(), req.getTicketDetails(),
							"Processing", "Initiated", "Wallet", transactionRefNo, false,req.getEmailAddress(),req.getMobileNumber());	
					return new ResponseEntity<String>(airRePriceRQ, HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<String>(flightBookingInitiate, HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();

			return new ResponseEntity<String>("" + APIUtils.getFailedJSON().toString(), HttpStatus.OK);
		}

	}*/
	
	
	//new code
	
	
	@RequestMapping(value ="/AirLineNames", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<FlightCityList> source(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language,@RequestHeader(value = "hash", required = false) String hash,
			@RequestBody TravelFlightRequest req) {

		FlightCityList resp=new FlightCityList();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				try {
					resp= travelflightapi.getAirLineNames(req.getSessionId());				
				} catch (Exception e) {
					System.out.println(e);
					resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
					resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
					resp.setMessage("Service Unavailable");
				}
			}
			else {
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("FAILED");
				//resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			//resp.setDetails(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<FlightCityList>(resp, HttpStatus.OK);
	}
	
	@RequestMapping(value ="/AirLineList", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<FlightResponseDTO> flightList(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language,@RequestHeader(value = "hash", required = false) String hash,
			@RequestBody TravelFlightRequest req) {

		FlightResponseDTO resp=new FlightResponseDTO();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(req.getSessionId());
				if (userSession != null) {
					try {
						logger.info("Flight Search");						
						MService service = travelflightapi.getServiceStatus();
						if (Status.Active.getValue().equalsIgnoreCase(service.getStatus().getValue())) {
							resp= travelflightapi.searchFlight(req);
						}
						else {
							resp.setStatus(ResponseStatus.FAILURE.getKey());
							resp.setCode(ResponseStatus.FAILURE.getValue());
							resp.setMessage("Service is under maintenance");
						}
					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				} else {
					resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage(ErrorMessage.SESSION_LOGOUT_ERROR);
					
				}
			
			} else {
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("FAILED");	
				resp.setDetails(CommonUtil.getFailedJSON().toString());
			}
		} else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(CommonUtil.getFailedJSON().toString());
		}

		return new ResponseEntity<FlightResponseDTO>(resp, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/AirPriceRecheck", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<FlightResponseDTO> airPriceRecheck(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody FlightPriceCheckRequest req) {

		FlightResponseDTO resp=new FlightResponseDTO();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(req.getSessionId());
				if (userSession != null) {				
					try {
						resp= travelflightapi.airRePriceRQ(req);
					} catch (Exception e) {
						e.printStackTrace();
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				}else {
					resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage("Session expired");
				}
			}
			else {
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("FAILED");	
				resp.setDetails(CommonUtil.getFailedJSON().toString());
			}
		}else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(CommonUtil.getFailedJSON().toString());
		}

		return new ResponseEntity<FlightResponseDTO>(resp, HttpStatus.OK);

	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/BookTicket", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> flightCheckOut(HttpSession session, @RequestBody AirBookRQ req) {
		try {
			UserSession userSession = userSessionRepository.findByActiveSessionId(req.getSessionId());
			if(userSession != null) {
			MUser user = userSession.getUser();
			JSONObject ticketdetails=CreateJsonRequestFlight.createMobilejsonForTicket(req);
			FlightBookReq flightBookReq=new FlightBookReq();
			flightBookReq.setSessionId(req.getSessionId());
			flightBookReq.setPaymentAmount(req.getPaymentDetails().getBookingAmount()+"");
			flightBookReq.setTicketDetails(ticketdetails.toString());
			flightBookReq.setPaymentMethod(req.getPaymentMethod());
			flightBookReq.setEmail(req.getEmailAddress());
			flightBookReq.setMobile(req.getMobileNumber());
			flightBookReq.setSource(req.getFlightSearchDetails().get(0).getOrigin());
			flightBookReq.setDestination(req.getFlightSearchDetails().get(0).getDestination());
            flightBookReq.setBaseFare(ticketdetails.getString("basicFare"));
			String paymentInitcode = null;
			ResponseDTO flightBookingInitiate= new ResponseDTO();

			List<TravellerFlightDetails> travellerFlightDetails=req.getTravellerDetails();
			List<TravellerFlightDetails> travellerFlightDetailsReturn=new ArrayList<>();
			if (req.getBookSegments().size()==2) {
				travellerFlightDetailsReturn.addAll(travellerFlightDetails);
			}
			travellerFlightDetailsReturn.addAll(travellerFlightDetails);


			if (req.getPaymentMethod().equalsIgnoreCase(PaymentType.Wallet.getKey())) {
				flightBookingInitiate = travelflightapi.flightBookingInitiate(flightBookReq,travellerFlightDetailsReturn, user);
				//JSONObject objflightBookingInitiate = new JSONObject(flightBookingInitiate);
				String codeflightBookingInitiate=flightBookingInitiate.getCode();
				if (codeflightBookingInitiate.equalsIgnoreCase("S00")) {
					/*String transactionRefNo = objflightBookingInitiate.getJSONObject("details")
							.getString("transactionRefNo");*/
					req.setTransactionId(flightBookingInitiate.getTransactionRefNo());
				}
				paymentInitcode=codeflightBookingInitiate;
			}
			else if (req.getPaymentMethod().equalsIgnoreCase(PaymentType.EBS.getKey())) {
				if (req.getTransactionId()!=null ||!req.getTransactionId().isEmpty()) {
					paymentInitcode=ResponseStatus.SUCCESS.getValue();
				}

			}
			else if (req.getPaymentMethod().equalsIgnoreCase(PaymentType.VNet.getKey())) {

				if (req.getTransactionId()!=null ||!req.getTransactionId().isEmpty()) {
					paymentInitcode=ResponseStatus.SUCCESS.getValue();
				}
			}

			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(paymentInitcode)) {
				req.setTravellerDetails(travellerFlightDetails);
				String airRePriceRQ = travelflightapi.AirBookRQForMobileConecting(req);
				JSONObject obj = new JSONObject(airRePriceRQ);
				String code = obj.getString("code");
				if (code.equalsIgnoreCase("S00")) {
					String transactionRefNomdex = obj.getJSONObject("details").getString("transactionRefNo");
					JSONArray tickets = obj.getJSONObject("details").getJSONObject("bookingDetail")
							.getJSONObject("pnrDetail").getJSONArray("tickets");
					String ticketNumber = "";
					for (int i = 0; i < tickets.length(); i++) {
						ticketNumber= tickets.getJSONObject(i).getString("ticketNumber");
						travellerFlightDetailsReturn.get(i).setTicketNo(ticketNumber);
					}
					String bookingRefId = obj.getJSONObject("details").getString("bookingRefId");
					JSONArray objccpnrs = obj.getJSONObject("details").getJSONObject("bookingDetail")
							.getJSONObject("pnrDetail").getJSONArray("pnrs");
					String pnrNo = objccpnrs.getJSONObject(0).getString("pnr");
					flightBookReq.setBookingRefId(bookingRefId);
					flightBookReq.setMdexTxnRefNo(transactionRefNomdex);
					flightBookReq.setStatus(Status.Booked.getValue());
					flightBookReq.setTxnRefno(req.getTransactionId());
					flightBookReq.setSuccess(true);
					flightBookReq.setFlightStatus(Status.Booked.getValue());
					flightBookReq.setPnrNo(pnrNo);
					ResponseDTO flightBookingSucess= new ResponseDTO();
					if (req.getPaymentMethod().equalsIgnoreCase(PaymentType.Wallet.getKey())) {
						flightBookingSucess = travelflightapi.flightBookingSucess(flightBookReq,travellerFlightDetailsReturn, user);
					}
					else {
						flightBookingSucess = travelflightapi.flightPaymentGatewaySucess(flightBookReq,req.getTravellerDetails(), user);
					}
					JSONObject flightBookingSucessobj = new JSONObject(flightBookingSucess);
					String flightBookingSucesscode = flightBookingSucessobj.getString("code");
					if (flightBookingSucesscode.equalsIgnoreCase("S00")) {
						return new ResponseEntity<String>("" + flightBookingSucess, HttpStatus.OK);
					} else {
						flightBookReq.setSuccess(false);
						travelflightapi.flightBookingSucess(flightBookReq,req.getTravellerDetails(), user);
						return new ResponseEntity<String>("" + flightBookingSucess, HttpStatus.OK);
					}
				} else {
					flightBookReq.setTxnRefno(req.getTransactionId());
					flightBookReq.setSuccess(false);
					if (req.getPaymentMethod().equalsIgnoreCase(PaymentType.Wallet.getKey())) {
						travelflightapi.flightBookingSucess(flightBookReq,req.getTravellerDetails(), user);
					}
					else {
						travelflightapi.flightPaymentGatewaySucess(flightBookReq,req.getTravellerDetails(), user);
					}
					return new ResponseEntity<String>(airRePriceRQ, HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<String>(""+flightBookingInitiate.toString(), HttpStatus.OK);
			}
			} else {
				return new ResponseEntity<String>(""+CommonUtil.getFailedJSON().toString(), HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();

			return new ResponseEntity<String>("" + CommonUtil.getFailedJSON().toString(), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/MyTickets", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<FlightResponseDTO> getAllTicketsByUser(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SessionDTO dto,
			@RequestHeader(value = "hash", required = false) String hash,HttpSession session) {

		FlightResponseDTO resp=new FlightResponseDTO();

		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if(userSession != null) {
					try {
						MUser user =  userSession.getUser();
						resp=travelflightapi.getAllBookTicketsForMobile(user);
						session.setAttribute("flightMsg", "");
						session.setAttribute("flighterrMsg", "");
						/*System.err.println("Working");*/
					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				}else {
					resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage("Session expired");
				}
			}
			else {
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("FAILED");
				resp.setDetails(CommonUtil.getFailedJSON().toString());
			}
		}else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(CommonUtil.getFailedJSON().toString());
		}
		return new ResponseEntity<FlightResponseDTO>(resp, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/SaveAirLineInDbByCron", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<FlightResponseDTO> getAllCityListCron(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SessionDTO dto,
			@RequestHeader(value = "hash", required = false) String hash) {

		FlightResponseDTO resp=new FlightResponseDTO();

		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				try {
					resp=travelflightapi.saveAirLineNamesInDb();
					System.err.println("Working");
				} catch (Exception e) {
					System.out.println(e);
					resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
					resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
					resp.setMessage("Service Unavailable");
				}
			}
			else {
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("FAILED");
				resp.setDetails(CommonUtil.getFailedJSON().toString());
			}
		}else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(CommonUtil.getFailedJSON().toString());
		}
		return new ResponseEntity<FlightResponseDTO>(resp, HttpStatus.OK);
	}
}
