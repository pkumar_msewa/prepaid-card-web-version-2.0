package com.msscard.app.controller;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.msscard.app.api.ILoadMoneyApi;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.IMdexApi;
import com.msscard.app.api.ITransactionApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.MdexTransactionRequestDTO;
import com.msscard.app.model.request.PGHandlerDTO;
import com.msscard.app.model.request.PGTransaction;
import com.msscard.app.model.request.TransactionInitiateRequest;
import com.msscard.app.model.response.MdexTransactionResponseDTO;
import com.msscard.app.model.response.PGStatusCheckDTO;
import com.msscard.app.model.response.TransactionInitiateResponse;
import com.msscard.app.model.response.TransactionRedirectResponse;
import com.msscard.app.model.response.UPIResponseDTO;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.entity.MCommission;
import com.msscard.entity.MMCards;
import com.msscard.entity.MService;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.entity.MdexRequestLogs;
import com.msscard.entity.UpiPay;
import com.msscard.entity.UserSession;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Status;
import com.msscard.model.UserDTO;
import com.msscard.model.error.TransactionError;
import com.msscard.repositories.MCommissionRepository;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MServiceRepository;
import com.msscard.repositories.MTransactionRepository;
import com.msscard.repositories.MdexRequestLogsRepository;
import com.msscard.repositories.UpiPayRepository;
import com.msscard.repositories.UserSessionRepository;
import com.msscard.sms.constant.SMSConstant;
import com.msscard.util.AESEncryption;
import com.msscard.util.Authorities;
import com.msscard.util.SecurityUtil;
import com.msscard.validation.CommonValidation;
import com.msscard.validation.LoadMoneyValidation;
import com.msscards.session.PersistingSessionRegistry;
import com.razorpay.constants.RazorPayConstants;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

 @Controller
@RequestMapping(value="/Api/v1/{role}/{device}/{language}/LoadMoney")
public class LoadMoneyApiController {
	 
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final UserSessionRepository userSessionRepository;
	private final IUserApi userApi;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final ILoadMoneyApi loadMoneyApi;
	private final IMatchMoveApi matchMoveApi;
	private final MTransactionRepository transactionRepository;
	private final MMCardRepository cardRepository;
	private final LoadMoneyValidation loadMoneyValidation;
	private final MServiceRepository serviceRepository;
	private final ITransactionApi transactionApi;
	private final MCommissionRepository mCommissionRepository;
	private final MdexRequestLogsRepository mdexRequestLogsRepository;
	private final IMdexApi iMdexApi;
	private final UpiPayRepository upiPayRepository;
	private final SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
	public LoadMoneyApiController(UserSessionRepository userSessionRepository, IUserApi userApi,
			PersistingSessionRegistry persistingSessionRegistry,
			ILoadMoneyApi loadMoneyApi,IMatchMoveApi matchMoveApi,
			MTransactionRepository transactionRepository,MMCardRepository cardRepository,
			LoadMoneyValidation loadMoneyValidation,MServiceRepository serviceRepository,ITransactionApi transactionApi,MCommissionRepository mCommissionRepository,
			MdexRequestLogsRepository mdexRequestLogsRepository,IMdexApi iMdexApi, UpiPayRepository upiPayRepository) {
		super();
		this.userSessionRepository = userSessionRepository;
		this.userApi = userApi;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.loadMoneyApi = loadMoneyApi;
		this.matchMoveApi=matchMoveApi;
		this.transactionRepository=transactionRepository;
		this.cardRepository=cardRepository;
		this.loadMoneyValidation=loadMoneyValidation;
		this.serviceRepository=serviceRepository;
		this.transactionApi=transactionApi;
		this.mCommissionRepository=mCommissionRepository;
		this.mdexRequestLogsRepository=mdexRequestLogsRepository;
		this.iMdexApi=iMdexApi;
		this.upiPayRepository = upiPayRepository;
	}
	
	@RequestMapping(value="/InitiateRequest",method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TransactionInitiateResponse> initiateLoadMoney(@PathVariable(value="role") String role,@PathVariable(value="device") String device,
			@PathVariable(value="language") String language,@RequestHeader(value = "hash", required = true) String hash,@RequestHeader(value = "Authorization") String authorization,
			@RequestBody TransactionInitiateRequest transactionRequest,HttpServletRequest request,HttpServletResponse response){
		TransactionInitiateResponse initiateResponse=new TransactionInitiateResponse();
	try{
		UpiPay value = upiPayRepository.getDataUsingKeys(SMSConstant.UPIKEY);
		String upiBasicAuth = SMSConstant.getBasicAuthorizationUpi(value.getUsername(),value.getPassword());
		if(upiBasicAuth.equals(authorization)) {
			TransactionInitiateRequest treq = SMSConstant.prepareTopupRequest(AESEncryption.decrypt(transactionRequest.getUpiRequest()));
		boolean isValidHash = SecurityUtil.isHashMatches(treq.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = treq.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						String remoteAddress=request.getRemoteAddr();
						userSession.setIpAddress(remoteAddress);
						userSessionRepository.save(userSession);
						UserKycResponse resp=matchMoveApi.getConsumers();
						if(resp.getCode().equalsIgnoreCase("S00")){
						String prefundingBalance=resp.getPrefundingBalance();
						double doublePrefundingBalance=Double.parseDouble(prefundingBalance);
						double actTransactonAmt=Double.parseDouble(treq.getAmount());
						if(doublePrefundingBalance>=actTransactonAmt){
						MService service=serviceRepository.findServiceByCode(RazorPayConstants.SERVICE_CODE);
						TransactionError transactionError=loadMoneyValidation.validateLoadMoneyTransaction(treq.getAmount(), userSession.getUser().getUsername(), service);
						if(transactionError.isValid()){
						MTransaction transaction= loadMoneyApi.initiateTransaction(user.getUsername(), Double.parseDouble(treq.getAmount()),RazorPayConstants.SERVICE_CODE);
						if(transaction!=null){
							initiateResponse.setCode(ResponseStatus.SUCCESS.getValue());
							initiateResponse.setMessage("Transaction initiated");
							initiateResponse.setStatus("Success");
							initiateResponse.setTransactionRefNo(transaction.getTransactionRefNo());
							initiateResponse.setAmount(String.valueOf(transaction.getAmount()));
							initiateResponse.setKey_id(RazorPayConstants.KEY_ID);
							initiateResponse.setKey_secret(RazorPayConstants.KEY_SECRET);
						}else{
							initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
							initiateResponse.setMessage("Transaction failed");
						}
						}else{
							initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
							initiateResponse.setMessage(transactionError.getMessage());
						}
					}else{
						initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
						initiateResponse.setMessage("Currently we are facing some issues.Please try after sometime");
					}
						}else{
						initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
						initiateResponse.setMessage("Currently we are facing some issues.Please try after sometime");
					}
					}else{
						initiateResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						initiateResponse.setMessage("Unauthorized User");
					}
				}else{
					initiateResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
					initiateResponse.setMessage("Please login and try again");
				}
			}else{
				initiateResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				initiateResponse.setMessage("Unauthorized Role");
			}
		}else{
			initiateResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
			initiateResponse.setMessage("Invalid hash");
		}
	}else{
		initiateResponse.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
		initiateResponse.setMessage("Invalid access");
	}
	}catch(Exception e){
		e.printStackTrace();
		initiateResponse.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
		initiateResponse.setMessage("Fatal Error");
		return new ResponseEntity<TransactionInitiateResponse>(initiateResponse,HttpStatus.OK);

	}
		return new ResponseEntity<TransactionInitiateResponse>(initiateResponse,HttpStatus.OK);
	}
	

	@RequestMapping(value="/Redirect",method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TransactionInitiateResponse> redirectResponse(@PathVariable(value="role") String role,@PathVariable(value="device") String device,
			@PathVariable(value="language") String language,@RequestHeader(value = "hash", required = true) String hash,
			@RequestBody TransactionInitiateRequest transactionRequest,HttpServletRequest request,HttpServletResponse response){
		TransactionInitiateResponse initiateResponse=new TransactionInitiateResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(transactionRequest.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = transactionRequest.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						MTransaction transaction=transactionRepository.findByTransactionRefNo(transactionRequest.getTransactionRefNo());
						if(transaction!=null){
						transactionRequest.setAmount(transaction.getAmount()*100+"");
						TransactionRedirectResponse redirectResponse=loadMoneyApi.redirectResponse(transactionRequest);
						logger.info("transaction amount:: "+transaction.getAmount());
						if(redirectResponse.isSuccess()){
						double actualTransactionAmt=transaction.getAmount()-transaction.getCommissionEarned();
							String actAmount=String.valueOf(actualTransactionAmt);		
							logger.info("actual amount:: "+actAmount);
							WalletResponse walletResponse=matchMoveApi.initiateLoadFundsToMMWalletRazorpay(userSession.getUser(),actAmount, transactionRequest.getTransactionRefNo());
							if(walletResponse!=null) {
							if(walletResponse.getCode().equalsIgnoreCase("S00")){
								String authRefNo=walletResponse.getAuthRetrivalNo();
									transaction.setRetrivalReferenceNo(transactionRequest.getPaymentId());
									transaction.setAuthReferenceNo(authRefNo);
									transaction.setCardLoadStatus("Success");
									transaction.setStatus(Status.Success);
									transactionRepository.save(transaction);
									initiateResponse.setCode(ResponseStatus.SUCCESS.getValue());
									initiateResponse.setMessage("Transaction Success");
									initiateResponse.setStatus("Success");
							}else{
							transaction.setRetrivalReferenceNo(transactionRequest.getPaymentId());
							transaction.setCardLoadStatus("Pending");
							transaction.setRemarks("WalEr::"+walletResponse.getMessage());
							transactionRepository.save(transaction);
							initiateResponse.setCode("S00");
							initiateResponse.setMessage("Transaction Successful.But card not loaded");
							}
							}else {
								transaction.setRetrivalReferenceNo(transactionRequest.getPaymentId());
								transaction.setCardLoadStatus("Pending");
								transaction.setRemarks("Pending due to wallet load failed at MM end.");
								transactionRepository.save(transaction);
								initiateResponse.setCode("S00");
								initiateResponse.setMessage("Transaction Successful.But card not loaded");
							}
							
							}else {
								transaction.setCardLoadStatus("Faliure");
								transaction.setRemarks("Transaction not verified at PG end");
								transaction.setStatus(Status.Failed);
								transactionRepository.save(transaction);
								initiateResponse.setCode("F00");
								initiateResponse.setMessage("Transaction not verified at PG end");
							}
						}else{
							initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
							initiateResponse.setMessage("Transaction failed");
						}
						
					}else{
						initiateResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						initiateResponse.setMessage("Unauthorized User");
					}
				}else{
					initiateResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
					initiateResponse.setMessage("Please login and try again");
				}
			}else{
				initiateResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				initiateResponse.setMessage("Unauthorized Role");
			}
		}else{
			initiateResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
			initiateResponse.setMessage("Invalid hash");
		}
		
		return new ResponseEntity<TransactionInitiateResponse>(initiateResponse,HttpStatus.OK);
	}

	
	

	
	
	
	/**
	 * INITIATE CARD REQUEST
	 * */
	
	@RequestMapping(value="/InitiateCardRequest",method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TransactionInitiateResponse> initiateCardRequest(@PathVariable(value="role") String role,@PathVariable(value="device") String device,
			@PathVariable(value="language") String language,@RequestHeader(value = "hash", required = true) String hash,
			@RequestBody TransactionInitiateRequest transactionRequest,HttpServletRequest request,HttpServletResponse response){
		TransactionInitiateResponse initiateResponse=new TransactionInitiateResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(transactionRequest.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = transactionRequest.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						MTransaction transaction= loadMoneyApi.initiateTransaction(user.getUsername(), Double.parseDouble(transactionRequest.getAmount()),RazorPayConstants.SERVICE_CODE);
						if(transaction!=null){
							initiateResponse.setCode(ResponseStatus.SUCCESS.getValue());
							initiateResponse.setMessage("Transaction initiated");
							initiateResponse.setStatus("Success");
							initiateResponse.setTransactionRefNo(transaction.getTransactionRefNo());
							initiateResponse.setAmount(String.valueOf(transaction.getAmount()));
							initiateResponse.setKey_id(RazorPayConstants.KEY_ID);
							initiateResponse.setKey_secret(RazorPayConstants.KEY_SECRET);
						}else{
							initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
							initiateResponse.setMessage("Transaction failed");
						}
						
					}else{
						initiateResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						initiateResponse.setMessage("Unauthorized User");
					}
				}else{
					initiateResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
					initiateResponse.setMessage("Please login and try again");
				}
			}else{
				initiateResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				initiateResponse.setMessage("Unauthorized Role");
			}
		}else{
			initiateResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
			initiateResponse.setMessage("Invalid hash");
		}
		
		return new ResponseEntity<TransactionInitiateResponse>(initiateResponse,HttpStatus.OK);
	}
	
	
	/**
	 * CONFIRM CARD REQUEST
	 * */
	
	@RequestMapping(value="/ConfirmCardRequest",method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TransactionInitiateResponse> confirmCardRequest(@PathVariable(value="role") String role,@PathVariable(value="device") String device,
			@PathVariable(value="language") String language,@RequestHeader(value = "hash", required = true) String hash,
			@RequestBody TransactionInitiateRequest transactionRequest,HttpServletRequest request,HttpServletResponse response){
		TransactionInitiateResponse initiateResponse=new TransactionInitiateResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(transactionRequest.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = transactionRequest.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						TransactionRedirectResponse redirectResponse=loadMoneyApi.redirectResponse(transactionRequest);
						if(redirectResponse.isSuccess()){
							initiateResponse.setCode(ResponseStatus.SUCCESS.getValue());
							initiateResponse.setMessage("Transaction Success");
							initiateResponse.setStatus("Success");
						}else{
							initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
							initiateResponse.setMessage("Transaction failed");
						}
						
					}else{
						initiateResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						initiateResponse.setMessage("Unauthorized User");
					}
				}else{
					initiateResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
					initiateResponse.setMessage("Please login and try again");
				}
			}else{
				initiateResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				initiateResponse.setMessage("Unauthorized Role");
			}
		}else{
			initiateResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
			initiateResponse.setMessage("Invalid hash");
		}
		
		return new ResponseEntity<TransactionInitiateResponse>(initiateResponse,HttpStatus.OK);
	}

	
	@RequestMapping(value="/RefundPhyCard",method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TransactionInitiateResponse> refundOnlyCard(@PathVariable(value="role") String role,@PathVariable(value="device") String device,
			@PathVariable(value="language") String language,@RequestHeader(value = "hash", required = true) String hash,
			@RequestBody TransactionInitiateRequest transactionRequest,HttpServletRequest request,HttpServletResponse response){
		TransactionInitiateResponse initiateResponse=new TransactionInitiateResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(transactionRequest.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				MUser reUser=userApi.findByUserName(transactionRequest.getUsername());
					UserDTO user = userApi.getUserById(reUser.getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						MMCards Physical=null;
						MMCards virtual=null;
						WalletResponse cardTransferResposne=null;
						if(true){
							String actAmount=String.valueOf((Double.parseDouble(transactionRequest.getAmount()))/100);
							if(true){
								if(transactionRequest.getPhysicalCard()!=null){
								if(transactionRequest.getPhysicalCard().equalsIgnoreCase("Yes")){
							 Physical=cardRepository.getPhysicalCardByUser(reUser);
							 if(Physical.getStatus().equalsIgnoreCase("Active")){
								cardTransferResposne=matchMoveApi.transferFundsToMMCard(reUser,actAmount,Physical.getCardId());
								}
								}
								}
								if(transactionRequest.getVirtualCard()!=null){
								if(transactionRequest.getVirtualCard().equalsIgnoreCase("Yes")){
									virtual=cardRepository.getVirtualCardsByCardUser(reUser);
									if(virtual.getStatus().equalsIgnoreCase("Active")){
									cardTransferResposne=matchMoveApi.transferFundsToMMCard(reUser, actAmount,virtual.getCardId());
								}
								}
							if(cardTransferResposne.getCode().equalsIgnoreCase("S00")){
									initiateResponse.setCode(ResponseStatus.SUCCESS.getValue());
									initiateResponse.setMessage("Transaction Success");
									initiateResponse.setStatus("Success");
							}else{
								initiateResponse.setCode("S00");
								initiateResponse.setMessage("Transaction Successful.But card not loaded");
							}
							
						}else{
							initiateResponse.setCode("S00");
							initiateResponse.setMessage("Transaction Successful.But card not loaded");
						}
							}
					}
				}
			}else{
				initiateResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				initiateResponse.setMessage("Unauthorized Role");
			}
	}
		return new ResponseEntity<TransactionInitiateResponse>(initiateResponse,HttpStatus.OK);
	}
	
	
	@RequestMapping(value="/InitiateUPI",method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TransactionInitiateResponse> initiateUPI(@PathVariable(value="role") String role,@PathVariable(value="device") String device,
			@PathVariable(value="language") String language,@RequestHeader(value = "hash", required = true) String hash,
			@RequestHeader(value = "Authorization") String authorization,@RequestBody TransactionInitiateRequest transactionRequest,HttpServletRequest request,HttpServletResponse response){
		TransactionInitiateResponse initiateResponse=new TransactionInitiateResponse();
		try {
		UpiPay value = upiPayRepository.getDataUsingKeys(SMSConstant.UPIKEY);
		String upiBasicAuth = SMSConstant.getBasicAuthorizationUpi(value.getUsername(),value.getPassword());
		if(upiBasicAuth.equals(authorization)) {
			TransactionInitiateRequest treq = SMSConstant.prepareTopupRequest(AESEncryption.decrypt(transactionRequest.getUpiRequest()));
			boolean isValidHash = SecurityUtil.isHashMatches(treq.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = treq.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						System.err.println("ip address:: "+request.getRemoteAddr());
						UserKycResponse resp=matchMoveApi.getConsumers();
						if(resp.getCode().equalsIgnoreCase("S00")){
						String prefundingBalance=resp.getPrefundingBalance();
						double doublePrefundingBalance=Double.parseDouble(prefundingBalance);
						double actTransactonAmt=Double.parseDouble(treq.getAmount());
					
						if(doublePrefundingBalance>=actTransactonAmt){
						MService service=serviceRepository.findServiceByCode(RazorPayConstants.UPI_SERVICE);
						TransactionError transactionError=loadMoneyValidation.validateLoadMoneyTransactionUpi(treq.getAmount(), userSession.getUser().getUsername(), service, request.getRemoteAddr());
						if(transactionError.isValid()){
						MTransaction transaction= loadMoneyApi.initiateUPITransaction(user.getUsername(), Double.parseDouble(treq.getAmount()),RazorPayConstants.UPI_SERVICE,true);
						if(transaction!=null){
							initiateResponse.setCode(ResponseStatus.SUCCESS.getValue());
							initiateResponse.setMessage("Transaction initiated");
							initiateResponse.setStatus("Success");
							initiateResponse.setTransactionRefNo(transaction.getTransactionRefNo());
							initiateResponse.setAmount(String.valueOf(transaction.getAmount()));
							initiateResponse.setKey_id("");
							initiateResponse.setKey_secret("");
						}else{
							initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
							initiateResponse.setMessage("Transaction failed");
						}
						}else{
							initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
							initiateResponse.setMessage(transactionError.getMessage());
						}
					}else{
						initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
						initiateResponse.setMessage("Currently we are facing some issues.Please try after sometime");
					}
						}else{
						initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
						initiateResponse.setMessage("Currently we are facing some issues.Please try after sometime");
					}
					}else{
						initiateResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						initiateResponse.setMessage("Unauthorized User");
					}
				}else{
					initiateResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
					initiateResponse.setMessage("Please login and try again");
				}
			}else{
				initiateResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				initiateResponse.setMessage("Unauthorized Role");
			}
		}else{
			initiateResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
			initiateResponse.setMessage("Invalid hash");
		}
		} else {
			initiateResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
			initiateResponse.setMessage("Unauthorized Role");
		}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return new ResponseEntity<TransactionInitiateResponse>(initiateResponse,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/RedirectUPI", method = RequestMethod.POST)
	public String redirectLoadMoneyUPI(@ModelAttribute TransactionInitiateRequest transactionRequest, Model model,ModelMap map, HttpSession session) {
					String sessionId=transactionRequest.getSessionId();
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {
						UserDTO user = userApi.getUserById(userSession.getUser().getId());
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							try {
								MTransaction trx=transactionApi.getTransactionByRefNo(transactionRequest.getTransactionRefNo());
								if(trx!=null && Status.Initiated.equals(trx.getStatus())){
								String hash=SecurityUtil.md5(RazorPayConstants.UPI_TOKEN+"|"+String.format("%.2f",trx.getAmount())+"|"+RazorPayConstants.UPI_MERCHANT_ID+"|"+transactionRequest.getTransactionRefNo());
								transactionRequest.setHash(hash);
								transactionRequest.setMerchant_id(RazorPayConstants.UPI_MERCHANT_ID);
								transactionRequest.setAdditionalInfo(sessionId);								
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
							map.addAttribute("xxx", transactionRequest);
							return "User/UPIPay";
						}
					}
		return "redirect:/User/Login/Process";

	}
	
	/*@RequestMapping(value="/PaymentGate", method=RequestMethod.GET)
	public String redirectgate(Model model,ModelMap map, HttpSession session) {
		PGTransaction pgTrx=new PGTransaction();
		pgTrx.setSaleUrl(RazorPayConstants.SALE_URL);
		pgTrx.setMerchantId(RazorPayConstants.MERCHANT_ID);
		pgTrx.setMerchantTxnNo("123456789045C");
		pgTrx.setAmount(String.valueOf("52"));
		pgTrx.setCurrencyCode(RazorPayConstants.CURRENCY_CODE);
		pgTrx.setPayType(RazorPayConstants.PAY_TYPE);
		pgTrx.setCustomerEmailId("akduhasd123@gmail.com");
		pgTrx.setTransactionType(RazorPayConstants.TRANSACTION_TYPE);
		pgTrx.setReturnURL(RazorPayConstants.RETURN_URL);
		Date d = new Date();
		pgTrx.setTxnDate(sdf.format(d));
		pgTrx.setCustomerMobileNo("9999111123");
		String secureHash=RazorPayConstants.composeMessage(pgTrx);
		pgTrx.setSecureHash(secureHash);
		map.addAttribute("pg", pgTrx);
		return "User/Pay";
	}*/
	
	
	
	@RequestMapping(value = "/RedirectPG", method = RequestMethod.POST)
	public String redirectPG(@ModelAttribute TransactionInitiateRequest transactionRequest, Model model,ModelMap map, HttpSession session) {
					String sessionId=transactionRequest.getSessionId();
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {
						UserDTO user = userApi.getUserById(userSession.getUser().getId());
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
						
									PGTransaction pgTrx=new PGTransaction();

							try {

								MTransaction trx=transactionApi.getTransactionByRefNo(transactionRequest.getTransactionRefNo());
								if(trx!=null && Status.Initiated.equals(trx.getStatus())){	
									pgTrx.setSaleUrl(RazorPayConstants.SALE_URL);
									pgTrx.setMerchantId(RazorPayConstants.MERCHANT_ID);
									pgTrx.setMerchantTxnNo(trx.getTransactionRefNo());
									pgTrx.setAmount(String.valueOf(trx.getAmount()));
									pgTrx.setCurrencyCode(RazorPayConstants.CURRENCY_CODE);
									pgTrx.setPayType(RazorPayConstants.PAY_TYPE);
									pgTrx.setCustomerEmailId(userSession.getUser().getUserDetail().getEmail());
									pgTrx.setTransactionType(RazorPayConstants.TRANSACTION_TYPE);
									pgTrx.setReturnURL(RazorPayConstants.RETURN_URL);
									pgTrx.setTxnDate(sdf.format(trx.getCreated()));
									pgTrx.setCustomerMobileNo(userSession.getUser().getUsername());
									String secureHash=RazorPayConstants.composeMessage(pgTrx);
									pgTrx.setSecureHash(secureHash);
									
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
							map.addAttribute("pg", pgTrx);
							return "User/Pay";
						}
					}
					return "User/FailureUPI";

	}
/**
 * Handler for PG
 * @param transactionRequest
 * @param model
 * @param map
 * @param session
 * @return
 */
	
	@RequestMapping(value = "/PGHandler", method = RequestMethod.POST)
	public String pgHandler(@ModelAttribute PGHandlerDTO transactionRequest, Model model,ModelMap map, HttpSession session) {
		System.err.println("Request::"+transactionRequest.toString());
		MTransaction transaction=transactionApi.getTransactionByRefNo(transactionRequest.getMerchantTxnNo());
		if(transaction!=null){
			if(transaction.getStatus().getValue().equalsIgnoreCase("Initiated")){
			    PGStatusCheckDTO statusCheck=transactionApi.checkPGStatus(transaction.getTransactionRefNo(),transactionRequest.getTxnID());
				if(statusCheck.getResponseCode().equalsIgnoreCase("0000")){
					if(Double.parseDouble(statusCheck.getAmount())==transaction.getAmount()){
			   MTransaction successTransaction= transactionApi.successLoadMoneyCustom(transaction.getTransactionRefNo(),transactionRequest.getTxnID());
    			if(successTransaction!=null && successTransaction.getStatus().getValue().equalsIgnoreCase("Success")){
    			MUser user=	userApi.findByUserAccount(successTransaction.getAccount());
    			if(user!=null){
    				double amount=successTransaction.getAmount()-successTransaction.getCommissionEarned();
    				WalletResponse walletResponse=matchMoveApi.initiateLoadFundsToMMWalletPG(user,String.valueOf(amount),successTransaction.getTransactionRefNo(),successTransaction.getRetrivalReferenceNo());
    				System.err.println("walletResponse received>>>>>"+walletResponse);
    				if(walletResponse!=null){
    					System.err.println("wallet response is not null");
    					if(walletResponse.getCode().equalsIgnoreCase("S00")){
    						System.err.println("the response is success >>>>>");
    						successTransaction.setAuthReferenceNo(walletResponse.getAuthRetrivalNo());
    						successTransaction.setCardLoadStatus("Success");
    						transactionRepository.save(successTransaction);
    						return "User/SuccessUPI";
    					}
    				}
    			}

    			}
				}else{
					 	transactionApi.failedLoadMoneyCustom(transaction.getTransactionRefNo(),transactionRequest.getTxnID());
						return "User/FailureUPI";

				}
				}else{
				    transactionApi.failedLoadMoneyCustom(transaction.getTransactionRefNo(),transactionRequest.getTxnID());
					return "User/FailureUPI";

				}
			}else{
				transactionApi.failedLoadMoneyCustom(transaction.getTransactionRefNo(),transactionRequest.getTxnID());
				return "User/FailureUPI";

			}
		}
		return "User/SuccessUPI";
	}
	


	
	@RequestMapping(value = "/SuccessUPI", method = RequestMethod.POST)
	 public String successUPI(@ModelAttribute UPIResponseDTO upiResponse, Model model,ModelMap map, HttpSession session,HttpServletRequest request) {
	    System.err.println("upiresponse:: "+upiResponse.toString());
		String sessionId=upiResponse.getAdditionalInfo();
	     UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
	      UserDTO user = userApi.getUserById(userSession.getUser().getId());
	      if (user.getAuthority().contains(Authorities.USER)
	        && user.getAuthority().contains(Authorities.AUTHENTICATED)) {
	       persistingSessionRegistry.refreshLastRequest(sessionId);
	       userSession.setIpAddress(request.getRemoteAddr());
	       userSessionRepository.save(userSession);
	       MService service=serviceRepository.findServiceByOperatorCode("UPS");
	       if(service.getStatus().getValue().equalsIgnoreCase("Active")){
	       MTransaction trans=null;
	       UPIResponseDTO responseDTO=loadMoneyApi.checkUPIStatus(upiResponse.getPaymentId(), RazorPayConstants.UPI_MERCHANT_ID);
	       if(responseDTO.getCode().equalsIgnoreCase("S00")&&responseDTO.getStatus().equalsIgnoreCase("Success")){
	    	 MTransaction merchantTransaction=transactionApi.getTransactionByRefNo(responseDTO.getMerchantRefNo());
	    	 if(merchantTransaction!=null && Status.Initiated.equals(merchantTransaction.getStatus())){
	    	   trans=transactionApi.successLoadMoneyUPI(responseDTO.getMerchantRefNo(),responseDTO.getPaymentId(),responseDTO.getPayerVA(),responseDTO.getPayerName(),responseDTO.getPayerMobile());
	       MMCards Physical=null;
	        MMCards virtual=null;
	        MMCards activeCard=null;
	        WalletResponse cardTransferResposne=null;
	        MTransaction transaction=transactionRepository.findByTransactionRefNo(responseDTO.getMerchantRefNo());
	        String upiId=transaction.getUpiId();
	        String [] spllitted=null;
	        if(upiId!=null){
	        spllitted=upiId.split("D");
	        }
	        if(transaction!=null){
	        	if(!transaction.isMdexTransaction()){
	        		if(trans!=null){
	        			System.err.println("in wallet response");
	        			WalletResponse walletResponse=matchMoveApi.initiateLoadFundsToMMWalletUpi(userSession.getUser(),responseDTO.getAmount(),transaction.getTransactionRefNo(), transaction.getRetrivalReferenceNo());
	         if(walletResponse.getCode().equalsIgnoreCase("S00")){
	        	 System.err.println("in success s00");
	          String authRefNo=walletResponse.getAuthRetrivalNo();
	          transaction.setAuthReferenceNo(authRefNo);
	          transaction.setCardLoadStatus("Success");
	          transactionRepository.save(transaction);

	       
	        }else{
	         transaction.setCardLoadStatus("Pending");
	         transaction.setRemarks("NA");
	         transactionRepository.save(transaction);
	         if(trans.isAndriod()){
	        	 return "User/SuccessUPI";
	         }else{
	          map.put("LoadMess","Transaction Successful.But card not loaded");
	          session.setAttribute("LoadMessage", "Transaction Successful.But card not loaded");
	          return "User/FailureUPI";
	         }
	        }
	        	}
	        }else{} 
	       }
	       }else{
	         trans=transactionApi.failedLoadMoneyUPI(upiResponse.getMerchantRefNo());
	         trans.setCardLoadStatus("Failed");
	         transactionRepository.save(trans);
	        if(trans.isAndriod()){
	        	return "User/FailureUPI";
	         }
	        else{
	         map.put("LoadMess", "Transaction Failed");
	         session.setAttribute("LoadMessage", "Transaction Failed");
	         return "User/FailureUPI";
	        }
	       }
	      }
	      }else{
	    	  return "User/FailureUPI";
	      }
	      }
	  return "User/FailureUPI";

	 }
	

	
	@RequestMapping(value = "/FailureUPI", method = RequestMethod.POST)
	public String failureUPI(@ModelAttribute UPIResponseDTO upiResponse, Model model,ModelMap map, HttpSession session) {
					String sessionId=upiResponse.getAdditionalInfo();
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {
						UserDTO user = userApi.getUserById(userSession.getUser().getId());
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							
							MTransaction trans=transactionApi.failedLoadMoneyUPI(upiResponse.getMerchantRefNo());
							if(trans!=null && trans.isAndriod()){
								return "User/FailureUPI";
							}
							else{
								map.put("LoadMess", "Transaction Failed");
								return "redirect:/User/Login/Process";
							}
						}
					}else{
						return "User/FailureUPI";
					}
		return "redirect:/User/Login/Process";

	}
	
	
	/**
	 * CREDIT CASHBACK
	 * */

	@RequestMapping(value="/CreditCashBack",method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TransactionInitiateResponse> creditCashBack(@PathVariable(value="role") String role,@PathVariable(value="device") String device,
			@PathVariable(value="language") String language,@RequestHeader(value = "hash", required = true) String hash,
			@RequestBody TransactionInitiateRequest transactionRequest,HttpServletRequest request,HttpServletResponse response){
		TransactionInitiateResponse initiateResponse=new TransactionInitiateResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(transactionRequest.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = transactionRequest.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						/*if(transactionRequest.getVirtualCard()!=null){
							if(transactionRequest.getVirtualCard().equalsIgnoreCase("Yes")){
								MMCards cards=cardRepository.getVirtualCardsByCardUser(userSession.getUser());
								if(cards.getStatus().equalsIgnoreCase("Inactive")){
									initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
									initiateResponse.setMessage("Card Status Inactive.Unable to load funds.Please contact customer care");
								}
							}
						}
						
						if(transactionRequest.getPhysicalCard()!=null){
							if(transactionRequest.getPhysicalCard().equalsIgnoreCase("Yes")){
								MMCards cards=cardRepository.getPhysicalCardByUser(userSession.getUser());
								if(cards.getStatus().equalsIgnoreCase("Inactive")){
									initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
									initiateResponse.setMessage("Card Status Inactive.Unable to load funds.Please contact customer care");
								}
							}
						}*/
						//String actAmount=String.valueOf((Double.parseDouble(transactionRequest.getAmount()))/100);
						//boolean shouldProceed=matchMoveApi.checkCreditPrefundBalance(actAmount);
						UserKycResponse resp=matchMoveApi.getConsumers();
						if(resp.getCode().equalsIgnoreCase("S00")){
						String prefundingBalance=resp.getPrefundingBalance();
						double doublePrefundingBalance=Double.parseDouble(prefundingBalance);
						double actTransactonAmt=Double.parseDouble(transactionRequest.getAmount());
					
						if(doublePrefundingBalance>=actTransactonAmt){

						MService service=serviceRepository.findServiceByCode(RazorPayConstants.SERVICE_CODE);
						TransactionError transactionError=loadMoneyValidation.validateLoadMoneyTransaction(transactionRequest.getAmount(), userSession.getUser().getUsername(), service);
						if(transactionError.isValid()){
						MTransaction transaction= loadMoneyApi.initiateTransaction(user.getUsername(), Double.parseDouble(transactionRequest.getAmount()),RazorPayConstants.SERVICE_CODE);
						if(transaction!=null){
							initiateResponse.setCode(ResponseStatus.SUCCESS.getValue());
							initiateResponse.setMessage("Transaction initiated");
							initiateResponse.setStatus("Success");
							initiateResponse.setTransactionRefNo(transaction.getTransactionRefNo());
							initiateResponse.setAmount(String.valueOf(transaction.getAmount()));
							initiateResponse.setKey_id(RazorPayConstants.KEY_ID);
							initiateResponse.setKey_secret(RazorPayConstants.KEY_SECRET);
														
						
						}else{
							initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
							initiateResponse.setMessage("Transaction failed");
						}
						}else{
							initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
							initiateResponse.setMessage(transactionError.getMessage());
						}
					}else{
						initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
						initiateResponse.setMessage("Currently we are facing some issues.Please try after sometime");
					}
						}else{
						initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
						initiateResponse.setMessage("Currently we are facing some issues.Please try after sometime");
					}
					}else{
						initiateResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						initiateResponse.setMessage("Unauthorized User");
					}
				}else{
					initiateResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
					initiateResponse.setMessage("Please login and try again");
				}
			}else{
				initiateResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				initiateResponse.setMessage("Unauthorized Role");
			}
		}else{
			initiateResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
			initiateResponse.setMessage("Invalid hash");
		}
		
		return new ResponseEntity<TransactionInitiateResponse>(initiateResponse,HttpStatus.OK);
	}



}
