package com.msscard.app.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.request.VersionControlDTO;
import com.msscard.entity.VersionLogs;
import com.msscard.repositories.VersionLogsRepository;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}")
public class VersionController {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final VersionLogsRepository versionLogsRepository;
	
	
	public VersionController(VersionLogsRepository versionLogsRepository) {
		super();
		this.versionLogsRepository = versionLogsRepository;
	}



	@RequestMapping(value={"/CheckVersion"},method=RequestMethod.POST ,produces=MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<ResponseDTO> versionCheck(@PathVariable (value="version") String version,@PathVariable(value = "role") String role,
                                                 @PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
                                                 @RequestBody VersionControlDTO versionControl, @RequestHeader(value = "hash", required = false) String hash,
                                                 HttpServletRequest request, HttpServletResponse response){

		
    	ResponseDTO result = new ResponseDTO();
    	VersionLogs verse=null;
    	logger.info("version: "+versionControl.getVersionName());
    	logger.info("key:: "+versionControl.getKey());
    	if(device.equalsIgnoreCase("Android")){
    		verse = versionLogsRepository.findLogByVersionAndroid(versionControl.getVersionName());
    	} else {
    		verse = versionLogsRepository.findLogByVersionIos(versionControl.getVersionName());
    	}
    	if(verse!=null){
	    	if(verse.getVersion().equalsIgnoreCase(versionControl.getVersionName())){
	    		if(verse.getStatus().equalsIgnoreCase("Active")) {
	    			
	    		if(verse.getAndroidVersion().getApiKey().equalsIgnoreCase(versionControl.getKey())){
	    			if(verse.isNewVersion()){
	    			result.setCode("V00");
	    			result.setStatus("Success");
	    			result.setMessage("valid key and version");
	    			}else{
	    				result.setCode("V04");
	    				result.setStatus("Success");
	    				result.setMessage("There is a new update available for this app.Kindly update from playstore");
	    			}
	    		} else {
	    			result.setCode("V01");
	    			result.setStatus("Failed");
	    			result.setMessage("Invalid key");
	    		}
	    	}else{
	    		result.setCode("V02");
	    		result.setStatus("Failed");
	    		result.setMessage("There is a new update available for this app.Kindly update from playstore");
	    	}
	    	}else{
	    		result.setCode("V03");
	    		result.setStatus("Failed");
	    		result.setMessage("You are using a lower version.Kindly Update Your App From PlayStore");
	    	}
    	} else{
    		result.setCode("V00");
    		result.setStatus("Failed");
    		result.setMessage("please download the app again from playstore or update it");
    	}
    	return new ResponseEntity<>(result,HttpStatus.OK);
    }

}
