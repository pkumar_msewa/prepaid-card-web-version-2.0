package com.msscard.app.sms.util;

public enum SMSAccount {

	PAYQWIK_OTP("paulfincap", "uXYgcLNN"), 
	PAYQWIK_TRANSACTIONAL("paulfincap", "uXYgcLNN"), 
	PAYQWIK_PROMOTIONAL("paulfincap", "uXYgcLNN"),
	PAYQWIK_PROMOTIONAL_MESSAGE("paulfincap", "uXYgcLNN");

	private final String type = "0";
	private final String dlr = "1";
	private final String source = "PAULPY";
	
	private final String username;
	private final String password;

	SMSAccount(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public String getType() {
		return type;
	}

	public String getDlr() {
		return dlr;
	}

	public String getSource() {
		return source;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

}
