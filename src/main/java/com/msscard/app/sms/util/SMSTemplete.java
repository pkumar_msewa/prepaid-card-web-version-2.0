package com.msscard.app.sms.util;

public class SMSTemplete {

	public static String ADMIN_TEMPLATE = "admintemplate.vm";

	public static String MERCHANT_REGISTRATION = "merchant_registration.vm";

	public static String DEVICE_BINDING = "device_binding.vm";

	public static String IPAY_ALERT = "ipay_alert.vm";

	public static String VERIFICATION_MOBILE = "verification_mobile.vm";

	public static String BHARATQR_SUCCESS = "bqr_success.vm";

	public static String OFFLINE_VERIFICATION_MOBILE = "offline_verification_mobile.vm";
//
	public static String VERIFICATION_MOBILE_KYC = "verification_mobile_kyc.vm";

	public static String VERIFICATION_KYC_SUCCESS = "verification_kyc_success.vm";

	public static String REFUND_SUCCESS = "refund_transaction_success.vm";

	public static String REFUND_SUCCESS_SENDER = "refund_transaction_success_sender.vm";

	//
	public static String VERIFICATION_SUCCESS = "verification_success.vm";
//
	public static String REGENERATE_OTP = "regenerate_otp.vm";
	
	
	
	public static String TRANSACTION_OTP ="transaction_otp.vm";
//
//	public static String TRANSACTION_CREDIT = "transaction_credit.vm";
//
//	public static String TRANSACTION_DEBIT = "transaction_debit.vm";
//	
	public static String PROMOTIONAL_SMS = "promotional_sms.vm";
//
	public static String CHANGE_PASSWORD_REQUEST = "changepassword_request.vm";
//	
	public static String CHANGE_PASSWORD_SUCCESS = "changepassword_success.vm";
//	
//	public static String TRANSACTION_CREDIT_SEND_MONEY = "transaction_credit_sm.vm";

	public static String FUNDTRANSFER_SUCCESS_SENDER = "fundtransfer_success_sender.vm";
	
	public static String FUNDTRANSFER_SUCCESS_RECEIVER_UNREGISTERED = "fundtransfer_success_receiver_unregistered.vm";
	
	public static String FUNDTRANSFER_SUCCESS_RECEIVER = "fundtransfer_success_receiver.vm";
	
	public static String TRANSACTION_FAILED = "transaction_failed.vm";
	
	public static String BILLPAYMENT_SUCCESS = "billpayment_success.vm";
	
	public static String BILLPAYMENT_SUCCESS_MERAEVENT = "meraevent.vm";
	
	public static String LOADMONEY_SUCCESS = "loadmoney_success.vm";

	public static String PROMO_SUCCESS = "success_promo.vm";
	
	public static String PREDICT_SUCCESS = "predict_success.vm";

	public static String MERCHANT_SENDER = "merchant_success_sender.vm";

	public static String MERCHANT_RECEIVER = "merchant_success_receiver.vm";
	
	public static String NEFT_TRANSFER_SUCCESS = "NEFT_Transfer_Success.vm";
	
	public static String NIKKI_CHAT_SUCCESS = "nikki_chat.vm";
	
// gci sms
	public static String GCI_SMS = "gci_success.vm";
	
	public static String BDAY_ALERT = "bday_alert.vm";
	
	//Qwikrpay
	
	public static String QWIK_PAY="qwikrpay_success";
	
	//for topup reversed transactions
	
	public static String TRANSACTIONAL_SMS_PROMOTION="reversed_trx.vm";
	//for refer n earn
	public static String REFER_EARN_VM="RefernEarn.vm";
	
	public static String REFER_EARN_SMS = "success_refer.vm";
	
	public static String REMAINDER_SMS = "reminder_message.vm";
	
	public static String BUSTICKET_SUCCESS = "busTicketpayment_success.vm";
	
	public static String FLIGHTTICKET_SUCCESS = "flightTicketpayment_success.vm";
	
	public static String SUPERADMIN_VERIFICATION_MOBILE = "superadmin_verification.vm";
	
	public static String FLIGHT_LOADMONEY_SUCCESS = "FlightLoadMoney_Success.vm";
	
	public static String REDEEMPOINTS_SUCCESS = "success_redeempoints.vm";
	
	public static String ACTIVATION_CODE = "Activation.vm";
	
	public static String WELCOME_SMS="welcome.vm";
	
	public static String VIRTUAL_CARD_GENERATION="virtualcard_generation.vm";
	
	public static String PHYSICAL_CARD_REQUEST="physicalcard_request.vm";
	
	public static String LOAD_MONEY_TRANSACTIONS="loadmoney.vm";
	
	public static String ACTIVATE_CARD="activation_card.vm";
	
	public static String REFUND_SENDMONEY="sendmoney_refund.vm";
	
	public static String SENDMONEY_SUCCESS="sendmoney_success.vm";
	
	public static String REFUND_SENDMONEY_RECEIVER="sendmoney_refund_receiver.vm";
	
	public static String SENDMONEY_SUCCESS_RECEIVER="sendmoney_receiver.vm";
	
	public static String KYCREQUEST_SUCCESS="KYCRequest.vm";
	
	public static String KYCREQUEST_FAILURE="KYCRequest_Fail.vm";
	
	public static String MYNTRA_OFFERS="Myntra_offers.vm";

	public static String USER_TEMP_PWD="User_Temporary_Pwd.vm"; 
	
	public static String FORGET_PASSWORD_OTP="forget_password_otp.vm"; 

	public static String FORGET_PASSWORD_SUCCESS="forget_password_success.vm";


	public static String AGENT_CREATION_SUCCESS="agent_creation.vm";
	
	public static String CORPORATE_USER_PASSWORD="corporate_password.vm";
	
	public static String MDEX_SUCCESS="mdex_success.vm";
	
	public static String MDEX_FAILURE="mdex_failure.vm";
	
	public static String SMS_DEDUCT="sms_debit.vm";
	
	public static String SMS_CASHBACK="cashback.vm";
	
	public static String SMS_DEDUCT_CARD="deduct_card.vm";
	
	public static String CREATED_GROUP = "create_group.vm";
	
	public static String REJECTED_GROUP = "Rejected_Group.vm";
	
	public static String GROUP_MESSAGE_REJECTED = "Message_Group_Reject.vm";
	
	public static String GROUP_ADD = "add_group.vm";
	
	public static String GROUP_MESSAGE = "Message_Group.vm";
	
	public static String BULK_SMS = "bulk_sms.vm";
	
	public static String DONATION_ADD = "donation.vm";
}
