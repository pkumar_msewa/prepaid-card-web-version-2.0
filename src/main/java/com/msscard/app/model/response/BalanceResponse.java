package com.msscard.app.model.response;

import com.msscard.model.ResponseStatus;

public class BalanceResponse {

	private double balance;
	private String code;
	private String message;
	private Object details;
	
	
	
	public Object getDetails() {
		return details;
	}

	public void setDetails(Object details) {
		this.details = details;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(ResponseStatus code) {
		this.code = code.getValue();
		this.message=code.getValue();
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	
}
