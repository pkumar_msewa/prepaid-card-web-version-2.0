package com.msscard.app.model.response;

public class TopupAppResponse extends CommonResponse{
	
	private String transactionId;

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

}
