package com.msscard.app.model.response;

public class MdexTransactionResponseDTO {
	
  private String status;
  private String code;
  private String message;
  private String details;
  private String transactionId;
  private String referenceNo;
  private String operatorId;
  private boolean success;
  private String response;
  private String TicketPnr;
  private String OperatorPnr;
  private boolean Ticket;
 
private String mdexResponse;
  
public String getResponse() {
	return response;
}
public void setResponse(String response) {
	this.response = response;
}
public boolean isSuccess() {
	return success;
}
public void setSuccess(boolean success) {
	this.success = success;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public String getCode() {
	return code;
}
public void setCode(String code) {
	this.code = code;
}
public String getMessage() {
	return message;
}
public void setMessage(String message) {
	this.message = message;
}
public String getDetails() {
	return details;
}
public void setDetails(String details) {
	this.details = details;
}
public String getTransactionId() {
	return transactionId;
}
public void setTransactionId(String transactionId) {
	this.transactionId = transactionId;
}
public String getReferenceNo() {
	return referenceNo;
}
public void setReferenceNo(String referenceNo) {
	this.referenceNo = referenceNo;
}
public String getOperatorId() {
	return operatorId;
}
public void setOperatorId(String operatorId) {
	this.operatorId = operatorId;
}
public String getTicketPnr() {
	return TicketPnr;
}
public void setTicketPnr(String ticketPnr) {
	TicketPnr = ticketPnr;
}
public String getOperatorPnr() {
	return OperatorPnr;
}
public void setOperatorPnr(String operatorPnr) {
	OperatorPnr = operatorPnr;
}
public boolean isTicket() {
	return Ticket;
}
public void setTicket(boolean ticket) {
	Ticket = ticket;
}
public String getMdexResponse() {
	return mdexResponse;
}
public void setMdexResponse(String mdexResponse) {
	this.mdexResponse = mdexResponse;
}
}
