package com.msscard.app.model.response;

public class UPIResponseDTO {

	private String status;
	private String code;
	private String mid;
	private String paymentId;
	private String merchantRefNo;
	private String transactionDate;
	private String amount;
	private String msg;
	private String payerName;
	private String payerMobile;
	private String payerVA;
	private String additionalInfo;
	
	
	
	@Override
	public String toString() {
		return "UPIResponseDTO [status=" + status + ", code=" + code + ", mid=" + mid + ", paymentId=" + paymentId
				+ ", merchantRefNo=" + merchantRefNo + ", transactionDate=" + transactionDate + ", amount=" + amount
				+ ", msg=" + msg + ", payerName=" + payerName + ", payerMobile=" + payerMobile + ", payerVA=" + payerVA
				+ ", additionalInfo=" + additionalInfo + "]";
	}
	public String getAdditionalInfo() {
		return additionalInfo;
	}
	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	public String getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
	public String getMerchantRefNo() {
		return merchantRefNo;
	}
	public void setMerchantRefNo(String merchantRefNo) {
		this.merchantRefNo = merchantRefNo;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getPayerName() {
		return payerName;
	}
	public void setPayerName(String payerName) {
		this.payerName = payerName;
	}
	public String getPayerMobile() {
		return payerMobile;
	}
	public void setPayerMobile(String payerMobile) {
		this.payerMobile = payerMobile;
	}
	public String getPayerVA() {
		return payerVA;
	}
	public void setPayerVA(String payerVA) {
		this.payerVA = payerVA;
	}
	
	
}
