package com.msscard.app.model.response;

public class BusSaveSeatDetailsResponse extends CommonResponse {
	
	private String tripId;

	public String getTripId() {
		return tripId;
	}

	public void setTripId(String tripId) {
		this.tripId = tripId;
	}
	
	

}
