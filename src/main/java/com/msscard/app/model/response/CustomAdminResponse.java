package com.msscard.app.model.response;

public class CustomAdminResponse {

	private String code;
	private Object details;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Object getDetails() {
		return details;
	}
	public void setDetails(Object details) {
		this.details = details;
	}
	
	
}
