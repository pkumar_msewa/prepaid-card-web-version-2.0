package com.msscard.app.model.response;

public class CancelTicketResp extends CommonResponse{
	
	private double refundAmount;

	private double cancellationCharges;

	private boolean cancelStatus;

	private boolean isCancelRequested;

	private boolean isRefunded;

	private String pnrNo;

	private String operatorPnr;

	private String cancelSeatNo;

	private String remarks;
	
	private String seatNumber;

	private boolean isCancellable;
	
	public String getSeatNumber() {
		return seatNumber;
	}

	public void setSeatNumber(String seatNumber) {
		this.seatNumber = seatNumber;
	}

	public boolean isCancellable() {
		return isCancellable;
	}

	public void setCancellable(boolean isCancellable) {
		this.isCancellable = isCancellable;
	}

	public double getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(double refundAmount) {
		this.refundAmount = refundAmount;
	}

	public double getCancellationCharges() {
		return cancellationCharges;
	}

	public void setCancellationCharges(double cancellationCharges) {
		this.cancellationCharges = cancellationCharges;
	}

	public boolean isCancelStatus() {
		return cancelStatus;
	}

	public void setCancelStatus(boolean cancelStatus) {
		this.cancelStatus = cancelStatus;
	}

	public boolean isCancelRequested() {
		return isCancelRequested;
	}

	public void setCancelRequested(boolean isCancelRequested) {
		this.isCancelRequested = isCancelRequested;
	}

	public boolean isRefunded() {
		return isRefunded;
	}

	public void setRefunded(boolean isRefunded) {
		this.isRefunded = isRefunded;
	}

	public String getPnrNo() {
		return pnrNo;
	}

	public void setPnrNo(String pnrNo) {
		this.pnrNo = pnrNo;
	}

	public String getOperatorPnr() {
		return operatorPnr;
	}

	public void setOperatorPnr(String operatorPnr) {
		this.operatorPnr = operatorPnr;
	}

	public String getCancelSeatNo() {
		return cancelSeatNo;
	}

	public void setCancelSeatNo(String cancelSeatNo) {
		this.cancelSeatNo = cancelSeatNo;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}
