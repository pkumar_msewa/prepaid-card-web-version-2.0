package com.msscard.app.model.response;

import com.msscard.model.ResponseStatus;

public class RegisterResponseDTO {

	private String code;
	private String message;
	private String status;
	private String details;
	private boolean isFullyFilled;
	private boolean hasError;
	
	
	
	
	public boolean isHasError() {
		return hasError;
	}




	public void setHasError(boolean hasError) {
		this.hasError = hasError;
	}




	public void setStatus(String status) {
		this.status = status;
	}




	public boolean isFullyFilled() {
		return isFullyFilled;
	}




	public void setFullyFilled(boolean isFullyFilled) {
		this.isFullyFilled = isFullyFilled;
	}




	public String getDetails() {
		return details;
	}




	public void setDetails(String details) {
		this.details = details;
	}




	public String getCode() {
		return code;
	}




	public void setCode(String code) {
		this.code = code;
	}




	public String getMessage() {
		return message;
	}




	public void setMessage(String message) {
		this.message = message;
	}




	public String getStatus() {
		return status;
	}


	public void setStatus(ResponseStatus status) {
		this.status = status.getKey();
		this.code = status.getValue();
	}

}
