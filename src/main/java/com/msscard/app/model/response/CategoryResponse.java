package com.msscard.app.model.response;


import com.msscard.app.model.request.CategoryRequestDTO;

public class CategoryResponse {

	
	private String status;
	private String code;
	private String message;
	private CategoryRequestDTO categories;
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public CategoryRequestDTO getCategories() {
		return categories;
	}

	public void setCategories(CategoryRequestDTO categories) {
		this.categories = categories;
	}

}
