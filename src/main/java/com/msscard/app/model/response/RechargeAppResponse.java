package com.msscard.app.model.response;

import com.msscard.model.ResponseStatus;

public class RechargeAppResponse {

	private String code;
	private String message;
	private String transactionRefNo;
	private String authReferenceId;
	
	public String getCode() {
		return code;
	}
	public void setCode(ResponseStatus status) {
		this.code = status.getValue();
		this.message=status.getKey();
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getTransactionRefNo() {
		return transactionRefNo;
	}
	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}
	public String getAuthReferenceId() {
		return authReferenceId;
	}
	public void setAuthReferenceId(String authReferenceId) {
		this.authReferenceId = authReferenceId;
	}
	
	
	
}
