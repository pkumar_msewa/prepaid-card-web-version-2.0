package com.msscard.app.model.response;

import java.util.ArrayList;

import com.msscard.model.FingooleResponseDTO;

public class FingooleResponse {

	private String code;
	private String status;
	private String message;
	private Object details;
	private boolean success;
	private String batch;
	private Object serviceList;
	private ArrayList<FingooleResponseDTO> customerTransactionList;
	private String amount;
	private ArrayList<FingooleTransactionListDTO> transactions;
		
	
	public ArrayList<FingooleTransactionListDTO> getTransactions() {
		return transactions;
	}
	public void setTransactions(ArrayList<FingooleTransactionListDTO> transactions) {
		this.transactions = transactions;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}	

	public ArrayList<FingooleResponseDTO> getCustomerTransactionList() {
		return customerTransactionList;
	}
	public void setCustomerTransactionList(ArrayList<FingooleResponseDTO> customerTransactionList) {
		this.customerTransactionList = customerTransactionList;
	}
	public Object getServiceList() {
		return serviceList;
	}
	public void setServiceList(Object serviceList) {
		this.serviceList = serviceList;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getDetails() {
		return details;
	}
	public void setDetails(Object details) {
		this.details = details;
	}
	
	
}
