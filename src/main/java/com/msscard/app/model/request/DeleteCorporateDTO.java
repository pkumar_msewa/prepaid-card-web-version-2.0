package com.msscard.app.model.request;

import java.util.List;

public class DeleteCorporateDTO{

	
	private String id;
	private List<String> services;
	
	

	public List<String> getServices() {
		return services;
	}

	public void setServices(List<String> services) {
		this.services = services;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	
}
