package com.msscard.app.model.request;

import com.msscard.entity.MUser;

public class AssignPhysicalCardAgentDTO {
	
	private String kitNo;
	
	private MUser user;
	
	private String userName;
	
	private String otp;

	public String getKitNo() {
		return kitNo;
	}

	public void setKitNo(String kitNo) {
		this.kitNo = kitNo;
	}

	public MUser getUser() {
		return user;
	}

	public void setUser(MUser user) {
		this.user = user;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}
	
	

}
