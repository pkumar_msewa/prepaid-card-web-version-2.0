package com.msscard.app.model.request;

import javax.persistence.ManyToOne;

import com.msscard.entity.BusTicket;

public class TravellerDetailsDTO extends GSTDetailsDTO {
	
	private String title;

	private String fName;

	private String mName;

	private String lName;

	private String age;

	private String gender;

	private String seatNo;

	private String seatType;

	private String fare;

	private String seatId;

	private Object details;
	
	@ManyToOne
	BusTicket busTicketId;

	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getSeatNo() {
		return seatNo;
	}

	public void setSeatNo(String seatNo) {
		this.seatNo = seatNo;
	}

	public String getSeatType() {
		return seatType;
	}

	public void setSeatType(String seatType) {
		this.seatType = seatType;
	}

	public String getFare() {
		return fare;
	}

	public void setFare(String fare) {
		this.fare = fare;
	}

	public String getSeatId() {
		return seatId;
	}

	public void setSeatId(String seatId) {
		this.seatId = seatId;
	}

	public Object getDetails() {
		return details;
	}

	public void setDetails(Object details) {
		this.details = details;
	}
	
	public BusTicket getBusTicketId() {
		return busTicketId;
	}

	public void setBusTicketId(BusTicket busTicketId) {
		this.busTicketId = busTicketId;
	}

}
