package com.msscard.app.model.request;

public class MMCardCreateRequest extends AuthenticationDTO{

	private String email;
	private String password;
	private String first_name;
	private String last_name;
	private String mobile_country_code;
	private String mobile;
	private String preferred_name;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getMobile_country_code() {
		return mobile_country_code;
	}
	public void setMobile_country_code(String mobile_country_code) {
		this.mobile_country_code = mobile_country_code;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPreferred_name() {
		return preferred_name;
	}
	public void setPreferred_name(String preferred_name) {
		this.preferred_name = preferred_name;
	}
	
	
}
