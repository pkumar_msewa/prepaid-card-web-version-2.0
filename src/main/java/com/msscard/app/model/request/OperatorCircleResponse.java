package com.msscard.app.model.request;

import java.util.List;

import com.msscard.app.model.response.MdexCircleResponse;
import com.msscard.app.model.response.MdexOperatorResponseDTO;

public class OperatorCircleResponse {
	
	private String code;
	private String status;
	private String message;
	private List<MdexOperatorResponseDTO> details;
	private List<MdexCircleResponse> circles;
	
	
	public List<MdexCircleResponse> getCircles() {
		return circles;
	}
	public void setCircles(List<MdexCircleResponse> circles) {
		this.circles = circles;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<MdexOperatorResponseDTO> getDetails() {
		return details;
	}
	public void setDetails(List<MdexOperatorResponseDTO> details) {
		this.details = details;
	}
	
	
}
