package com.msscard.app.model.request;

public class BookTicketReq {
	
	private String transactionId;
	private String mdexTxnId;
	private String sessionId;
	private String vpqTxnId;
	private double amount;
	private String emtTxnId;
	private String seatHoldId;
	private String data;
	
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getMdexTxnId() {
		return mdexTxnId;
	}
	public void setMdexTxnId(String mdexTxnId) {
		this.mdexTxnId = mdexTxnId;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getVpqTxnId() {
		return vpqTxnId;
	}
	public void setVpqTxnId(String vpqTxnId) {
		this.vpqTxnId = vpqTxnId;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getEmtTxnId() {
		return emtTxnId;
	}
	public void setEmtTxnId(String emtTxnId) {
		this.emtTxnId = emtTxnId;
	}
	public String getSeatHoldId() {
		return seatHoldId;
	}
	public void setSeatHoldId(String seatHoldId) {
		this.seatHoldId = seatHoldId;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
	
}
