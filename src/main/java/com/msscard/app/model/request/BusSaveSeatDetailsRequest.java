package com.msscard.app.model.request;

import java.util.List;

public class BusSaveSeatDetailsRequest extends CommonRequest {
	
	private String busType;

	private String travelName;

	private String journeyDate;

	private String seatDetail;

	private String depTime;

	private String arrTime;

	private String source;

	private String destination;

	private String boardTime;

	private String busId;

	private String boardId;

	private String boardLocation;

	private String boardContactNo;

	private double totalFare;

	private String seatDetailsId;

	private String tripId;

	private String emtTxnId;

	private String userMobile;

	private String emtTransactionScreenId;

	private String userEmail;

	private String boardName;

	private String seatHoldId;

	private List<TravellerDetailsDTO> travellers;

	public String getBusType() {
		return busType;
	}

	public void setBusType(String busType) {
		this.busType = busType;
	}

	public String getTravelName() {
		return travelName;
	}

	public void setTravelName(String travelName) {
		this.travelName = travelName;
	}

	public String getJourneyDate() {
		return journeyDate;
	}

	public void setJourneyDate(String journeyDate) {
		this.journeyDate = journeyDate;
	}

	public String getSeatDetail() {
		return seatDetail;
	}

	public void setSeatDetail(String seatDetail) {
		this.seatDetail = seatDetail;
	}

	public String getDepTime() {
		return depTime;
	}

	public void setDepTime(String depTime) {
		this.depTime = depTime;
	}

	public String getArrTime() {
		return arrTime;
	}

	public void setArrTime(String arrTime) {
		this.arrTime = arrTime;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getBoardTime() {
		return boardTime;
	}

	public void setBoardTime(String boardTime) {
		this.boardTime = boardTime;
	}

	public String getBusId() {
		return busId;
	}

	public void setBusId(String busId) {
		this.busId = busId;
	}

	public String getBoardId() {
		return boardId;
	}

	public void setBoardId(String boardId) {
		this.boardId = boardId;
	}

	public String getBoardLocation() {
		return boardLocation;
	}

	public void setBoardLocation(String boardLocation) {
		this.boardLocation = boardLocation;
	}

	public String getBoardContactNo() {
		return boardContactNo;
	}

	public void setBoardContactNo(String boardContactNo) {
		this.boardContactNo = boardContactNo;
	}

	public double getTotalFare() {
		return totalFare;
	}

	public void setTotalFare(double totalFare) {
		this.totalFare = totalFare;
	}

	public String getSeatDetailsId() {
		return seatDetailsId;
	}

	public void setSeatDetailsId(String seatDetailsId) {
		this.seatDetailsId = seatDetailsId;
	}

	public String getTripId() {
		return tripId;
	}

	public void setTripId(String tripId) {
		this.tripId = tripId;
	}

	public String getEmtTxnId() {
		return emtTxnId;
	}

	public void setEmtTxnId(String emtTxnId) {
		this.emtTxnId = emtTxnId;
	}

	public String getUserMobile() {
		return userMobile;
	}

	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

	public String getEmtTransactionScreenId() {
		return emtTransactionScreenId;
	}

	public void setEmtTransactionScreenId(String emtTransactionScreenId) {
		this.emtTransactionScreenId = emtTransactionScreenId;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getBoardName() {
		return boardName;
	}

	public void setBoardName(String boardName) {
		this.boardName = boardName;
	}

	public String getSeatHoldId() {
		return seatHoldId;
	}

	public void setSeatHoldId(String seatHoldId) {
		this.seatHoldId = seatHoldId;
	}

	public List<TravellerDetailsDTO> getTravellers() {
		return travellers;
	}

	public void setTravellers(List<TravellerDetailsDTO> travellers) {
		this.travellers = travellers;
	}


	
}
