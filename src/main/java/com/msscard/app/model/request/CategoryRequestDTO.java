package com.msscard.app.model.request;

import org.springframework.web.multipart.MultipartFile;

public class CategoryRequestDTO extends SessionDTO {

	private String id;
	
	private String name;

	private String default_amount;

	private String factor = "UNIT";

	private String mcc;

	private String country = "IND";
	
	private MultipartFile imgUrl;
	

	public MultipartFile getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(MultipartFile imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFactor() {
		return factor;
	}

	public String getCountry() {
		return country;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {

		if (name.length() <= 10) {
			this.name = name;
		} else {
			this.name = null;
		}
	}

	public String getDefault_amount() {
		return default_amount;
	}

	public void setDefault_amount(String default_amount) {
		this.default_amount = default_amount;
	}

	public String getMcc() {
		return mcc;
	}

	public void setMcc(String mcc) {
		this.mcc = mcc;
	}

}
