package com.msscard.app.model.request;

public class AuthenticationDTO {

	private String clientIp;
	private String cllientKey;
	private String clientToken;
	private String clientApiName;
	
	public String getClientIp() {
		return clientIp;
	}
	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}
	public String getCllientKey() {
		return cllientKey;
	}
	public void setCllientKey(String cllientKey) {
		this.cllientKey = cllientKey;
	}
	public String getClientToken() {
		return clientToken;
	}
	public void setClientToken(String clientToken) {
		this.clientToken = clientToken;
	}
	public String getClientApiName() {
		return clientApiName;
	}
	public void setClientApiName(String clientApiName) {
		this.clientApiName = clientApiName;
	}

	
	
}
