package com.msscard.app.model.request;

public class Utility {

	 private String ipAddress;
	    private String version;
	    private String registrationId;
	    private String brand;
	    private String model;
	    private String captcha;
	    private String captchaText;
	    	    
	    
		public String getCaptcha() {
			return captcha;
		}
		public void setCaptcha(String captcha) {
			this.captcha = captcha;
		}
		public String getCaptchaText() {
			return captchaText;
		}
		public void setCaptchaText(String captchaText) {
			this.captchaText = captchaText;
		}
		public String getIpAddress() {
			return ipAddress;
		}
		public void setIpAddress(String ipAddress) {
			this.ipAddress = ipAddress;
		}
		public String getVersion() {
			return version;
		}
		public void setVersion(String version) {
			this.version = version;
		}
		public String getRegistrationId() {
			return registrationId;
		}
		public void setRegistrationId(String registrationId) {
			this.registrationId = registrationId;
		}
		public String getBrand() {
			return brand;
		}
		public void setBrand(String brand) {
			this.brand = brand;
		}
		public String getModel() {
			return model;
		}
		public void setModel(String model) {
			this.model = model;
		}
	   
	    
}
