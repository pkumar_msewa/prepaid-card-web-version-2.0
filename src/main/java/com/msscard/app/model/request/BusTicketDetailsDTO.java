package com.msscard.app.model.request;

public class BusTicketDetailsDTO {

	private String ticketPnr;

	private String operatorPnr;

	private String message;

	private boolean ticket;

	public String getTicketPnr() {
		return ticketPnr;
	}

	public void setTicketPnr(String ticketPnr) {
		this.ticketPnr = ticketPnr;
	}

	public String getOperatorPnr() {
		return operatorPnr;
	}

	public void setOperatorPnr(String operatorPnr) {
		this.operatorPnr = operatorPnr;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isTicket() {
		return ticket;
	}

	public void setTicket(boolean ticket) {
		this.ticket = ticket;
	}
	
	
}
