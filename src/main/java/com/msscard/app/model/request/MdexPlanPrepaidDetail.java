package com.msscard.app.model.request;

import java.util.List;

public class MdexPlanPrepaidDetail {
	
	private String serviceName;
	private String serviceCode;
	private String circleName;
	private String circleCode;
	private String code;
	private List<PlanPrepaidResponse> plans;
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String getCircleName() {
		return circleName;
	}
	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}
	public String getCircleCode() {
		return circleCode;
	}
	public void setCircleCode(String circleCode) {
		this.circleCode = circleCode;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public List<PlanPrepaidResponse> getPlans() {
		return plans;
	}
	public void setPlans(List<PlanPrepaidResponse> plans) {
		this.plans = plans;
	}
	
	
}
