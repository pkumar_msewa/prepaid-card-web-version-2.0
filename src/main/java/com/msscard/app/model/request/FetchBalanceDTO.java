package com.msscard.app.model.request;

public class FetchBalanceDTO extends SessionDTO {

	
	private String pocketName;

	private double pocketBalance;

	private String url;
	
	public String getPocketName() {
		return pocketName;
	}

	public void setPocketName(String pocketName) {
		this.pocketName = pocketName;
	}

	public double getPocketBalance() {
		return pocketBalance;
	}

	public void setPocketBalance(double pocketBalance) {
		this.pocketBalance = pocketBalance;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
