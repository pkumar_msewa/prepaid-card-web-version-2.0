package com.msscard.app.model.request;

import com.msscard.entity.MCommission;
import com.msscard.entity.MService;
import com.msscard.entity.MUser;

public class TransactionalRequest {

	private double amount;
	private MService service;
	private MUser user;
	private String receiverUser;
	private String description;
	private boolean isAndroid; 
	private MCommission commission;
	private String batch;
		
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public MCommission getCommission() {
		return commission;
	}
	public void setCommission(MCommission commission) {
		this.commission = commission;
	}
	public boolean isAndroid() {
		return isAndroid;
	}
	public void setAndroid(boolean isAndroid) {
		this.isAndroid = isAndroid;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public MService getService() {
		return service;
	}
	public void setService(MService service) {
		this.service = service;
	}
	public MUser getUser() {
		return user;
	}
	public void setUser(MUser user) {
		this.user = user;
	}
	public String getReceiverUser() {
		return receiverUser;
	}
	public void setReceiverUser(String receiverUser) {
		this.receiverUser = receiverUser;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	
	
}
