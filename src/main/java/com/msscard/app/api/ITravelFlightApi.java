package com.msscard.app.api;

import java.util.List;

import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.response.CommonResponse;
import com.msscard.entity.MService;
import com.msscard.entity.MUser;
import com.msscard.model.AirBookRQ;
import com.msscard.model.FlightBookReq;
import com.msscard.model.FlightCancelableResp;
import com.msscard.model.FlightCityList;
import com.msscard.model.FlightPriceCheckRequest;
import com.msscard.model.FlightResponseDTO;
import com.msscard.model.FligthcancelableReq;
import com.msscard.model.GetFlightDetailsForAdmin;
import com.msscard.model.MobileFlightBookRequest;
import com.msscard.model.PagingDTO;
import com.msscard.model.TravelFlightRequest;
import com.msscard.model.TravellerFlightDetails;

public interface ITravelFlightApi {

	String getAirlineNames();

	String getFlightSearchAPI(TravelFlightRequest req);

	String AirRePriceRQ(FlightPriceCheckRequest req);

	String mobileAirRePriceRQConnecting(FlightPriceCheckRequest req);

	String AirBookRQForMobile(MobileFlightBookRequest req);

	/*String FlightBookingSucess(String sessionId, String ticketNumber, String firstName, String bookingRefId,
			String transactionRefNomdex, String paymentAmount, String ticketDetails, String statusflight,
			String paymentstatus, String paymentmethod, String transactionRefno, boolean isSuccess, String email,
			String mobile);*/

	String FlightBookingInitiate(String sessionId, String firstName, String paymentAmount, String ticketDetails,
			String paymentmethod, MUser user);

	CommonResponse setFlightService();

	FlightCityList getAirLineNames(String sessionId);

	MService getServiceStatus();

	FlightResponseDTO searchFlight(TravelFlightRequest req);

	boolean compareCountry(TravelFlightRequest req);

	FlightResponseDTO airRePriceRQ(FlightPriceCheckRequest req);

	ResponseDTO flightBookingInitiate(FlightBookReq fligthBookReq, List<TravellerFlightDetails> details, MUser user);

	String AirBookRQForMobileConecting(AirBookRQ req);

	FlightResponseDTO saveAirLineNamesInDb();

	ResponseDTO flightBookingSucess(FlightBookReq req, List<TravellerFlightDetails> details, MUser user);

	ResponseDTO flightPaymentGatewaySucess(FlightBookReq req, List<TravellerFlightDetails> details, MUser user);

	FlightResponseDTO getAllBookTicketsForMobile(MUser user);

	ResponseDTO getFlightDetails();

	ResponseDTO getFlightDetailPost(PagingDTO dto);

	FlightResponseDTO getSingleFlightTicketDetails(String sessionId, long flightId);

	FlightCancelableResp isCancellableFlight(FligthcancelableReq dto);

	
	
}
