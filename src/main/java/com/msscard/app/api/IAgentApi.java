package com.msscard.app.api;

import java.util.List;

import org.codehaus.jettison.json.JSONObject;

import com.msscard.app.model.request.AssignPhysicalCardAgentDTO;
import com.msscard.app.model.request.ChangePasswordRequest;
import com.msscard.app.model.response.PrefundRequestDTO;
import com.msscard.entity.MMCards;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.model.AgentPrefundDTO;
import com.msscard.model.AgentTransactionListDTO;
import com.msscard.model.ForgetPasswordRequestDTO;
import com.msscard.model.LoadCardList;
import com.msscard.model.MAgentDTO;
import com.msscard.model.MUserDTO;
import com.msscard.model.PhysicalCardRequestListDTO;
import com.msscard.model.RegisterDTO;
import com.msscard.model.UserLoadCardDTO;
import com.msscard.model.ValidTransactionDTO;
import com.msscard.model.error.CommonError;

public interface IAgentApi {
	
	void saveAgent(RegisterDTO dto) throws Exception;
	
	List<MAgentDTO> getAllAgentDetail() throws Exception;
	
	MUser saveUserByAgent(RegisterDTO dto) throws Exception;
	
	List<MUserDTO> getAllUser(MUser user , MUserDTO dto) throws Exception;
	
	ValidTransactionDTO validTransaction(UserLoadCardDTO dto) throws Exception;
	
	MMCards existPhysicalCard(MUser user) throws Exception;
	
	MMCards existVirtualCard(MUser user) throws Exception;
	
	boolean checkUser(MUser agent, MUser mUser) throws Exception;
	
	MMCards existCard(MUser user) throws Exception;
	
	void loadCardFromAgent(UserLoadCardDTO dto , MMCards mmCards , MUser user) throws Exception;
	
	MTransaction findTransactionBasedOnTransactionRefNo(String transactionRefNo) throws Exception;
	
	void agentPrefundRequest(AgentPrefundDTO dto) throws Exception;
	
	List<AgentPrefundDTO> getAgentPrefundDetails() throws Exception;
	
	PrefundRequestDTO updateAgentPrefundStatus(AgentPrefundDTO prefundDTO) throws Exception;
	
	MUser checkUser(String userName) throws Exception;
	
	void updateCardDetails(MUser user) throws Exception;
	
	void updatePhysicalCard(MUser mUser) throws Exception;
	
	void transferMoneyToCard(MUser mUser) throws Exception;
	
	void saveAgentAssignPhysicalCard(MUser agent , MUser mUser , AssignPhysicalCardAgentDTO dto) throws Exception;
	
	List<LoadCardList> getLoadCardList(MUser agent  , MUserDTO dto) throws Exception;
	
	List<PhysicalCardRequestListDTO> getPhysicalCardAssignList(MUser agent , MUserDTO dto) throws Exception;
	
	long userCountBasedOnAgent(MUser agent) throws Exception;
	
	void changePassword(ChangePasswordRequest dto) throws Exception;
	
	ForgetPasswordRequestDTO otpForForgetPassword(MUser mUser) throws Exception;
	
	ForgetPasswordRequestDTO forgetPasswordRequest(MUser mUser , ChangePasswordRequest dto) throws Exception;
	
	MUser checkAgentExist(String userName) throws Exception;
	
	List<AgentTransactionListDTO> getAgentTransactionList() throws Exception;
	
	double countTotalTransferredAmount(MUser agent) throws Exception;
	
	long countPhysicalCards(MUser agent) throws Exception;
	
	CommonError checkUserKYC(MUser mUser) throws Exception;

	public JSONObject findTotalTransactionByMonth(MUser agent);
	
	public JSONObject findTotalUserByMonth(MUser agent);
	
	public List<String> getAllBankDetails() throws Exception;
}
