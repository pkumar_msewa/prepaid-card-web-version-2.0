package com.msscard.app.api;

import com.msscard.app.model.request.MatchMoveCreateCardRequest;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.entity.MUser;

public interface IMMBasicAuthApi {

	double getBalance(MUser request);

	Double getBalanceApp(MUser request);

	ResponseDTO getBalanceExp(MUser request);

	double myBalance(MUser userMob);

	ResponseDTO createUserOnMM(MUser request);

	WalletResponse createWallet(MatchMoveCreateCardRequest request);

}
