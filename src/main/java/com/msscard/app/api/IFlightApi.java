package com.msscard.app.api;

import java.util.List;

import org.codehaus.jettison.json.JSONObject;
import org.json.JSONException;

import com.msscard.entity.FlightAirLineList;
import com.msscard.entity.FlightDetails;
import com.msscard.entity.FlightTicket;
import com.msscard.entity.FlightTravellers;
import com.msscard.entity.MService;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.model.CompareCountry;
import com.msscard.model.FlghtTicketResp;
import com.msscard.model.FlightPayment;
import com.msscard.model.FlightPaymentRequest;
import com.msscard.model.FlightPaymentResponse;
import com.msscard.model.PagingDTO;
import com.msscard.model.TicketsResp;
import com.msscard.model.TravelFlightRequest;

public interface IFlightApi {

	JSONObject getFlightSerachJson(TravelFlightRequest req);

	MTransaction initiateTransaction(String transactionRefNo, double amount, MService service, MUser senderUser);

	FlightPaymentResponse placeOrder(FlightPaymentRequest order, MService service, MUser senderUser);

	FlightDetails saveFlightBooking(FlightPaymentRequest order, MTransaction transaction, MUser user);

	List<FlightAirLineList> getAllAirLineList();

	boolean comCountry(CompareCountry dto);

	FlightPaymentResponse flightInit(FlightPayment order, MService service, MUser senderUser);

	MTransaction initiateTransactionFlight(String transactionRefNo, double amount, MService service, MUser senderUser,
			String convenienceFee, String baseFare);

	FlightTicket saveFlight(FlightPayment order, MTransaction transaction, MUser user, String baseFare);

	FlightPaymentResponse flightPayment(FlightPayment dto, MUser user) throws Exception;

	FlightPaymentResponse paymentGateWaySuccess(FlightPayment dto, MUser user) throws JSONException;

	List<FlghtTicketResp> getAllTickets(MUser user);

	List<FlightTicket> getFlightDetailForAdmin();

	List<FlightTicket> getFlightDetailForAdminDate(PagingDTO dto);

	List<FlightTravellers> getFlightTravellersForAdmin(long flightTicketId);

	TicketsResp getFlightTravellersNameForAdmin(long flightTicketId);

	

	

}
