package com.msscard.app.api;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.msscard.app.model.request.CategoryRequestDTO;
import com.msscard.app.model.request.PocketFundTransferDTO;
import com.msscard.entity.MUser;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;

public interface IfetchApi {

	List<CategoryRequestDTO> fetchCategories();
	ClientResponse createCategory(CategoryRequestDTO dto, MultipartFile file, String dest);
	ClientResponse updateCategory(CategoryRequestDTO dto);
	ClientResponse deleteCategory(CategoryRequestDTO dto);
	ClientResponse transferFundsToPockets(PocketFundTransferDTO dto, MUser mUser);

}
