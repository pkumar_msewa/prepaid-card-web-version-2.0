
package com.msscard.app.api.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;

import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.ISMSSenderApi;
import com.msscard.app.model.request.MatchMoveCreateCardRequest;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.request.UserKycRequest;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.app.sms.util.SMSAccount;
import com.msscard.app.sms.util.SMSTemplete;
import com.msscard.connection.javasdk.Connection;
import com.msscard.connection.javasdk.HttpRequest;
import com.msscard.connection.javasdk.HttpRequest.HttpRequestException;
import com.msscard.connection.javasdk.ResourceException;
import com.msscard.connection.javasdk.UnsupportedMethodException;
import com.msscard.entity.MKycDetail;
import com.msscard.entity.MMCards;
import com.msscard.entity.MService;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.entity.MatchMoveWallet;
import com.msscard.entity.PhysicalCardDetails;
import com.msscard.entity.SendMoneyDetails;
import com.msscard.model.CustomUserTransactions;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Status;
import com.msscard.model.TransactionType;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MServiceRepository;
import com.msscard.repositories.MTransactionRepository;
import com.msscard.repositories.MUserRespository;
import com.msscard.repositories.MatchMoveWalletRepository;
import com.msscard.repositories.PhysicalCardDetailRepository;
import com.msscard.repositories.SendMoneyDetailsRepository;
import com.msscard.sms.constant.SMSConstant;
import com.msscard.util.MatchMoveUtil;
import com.msscard.util.SecurityUtil;
import com.msscard.util.TrippleDSEncryption;
import com.msscard.validation.CommonValidation;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class MatchMoveApiImpl implements IMatchMoveApi {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final MatchMoveWalletRepository matchMoveWalletRepository;
	private final MMCardRepository mmCardRepository;
	private final PhysicalCardDetailRepository physicalCardDetailRepository;
	private final ISMSSenderApi senderApi;
	private final MUserRespository userRepository;
	private final SendMoneyDetailsRepository sendMoneyDetailsRepository;
	private final MTransactionRepository mTransactionRepository;
	private final MServiceRepository mServiceRepository;
	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	public MatchMoveApiImpl(MatchMoveWalletRepository matchMoveWalletRepository, MMCardRepository mmCardRepository,
			PhysicalCardDetailRepository physicalCardDetailRepository, ISMSSenderApi senderApi,
			MUserRespository userRespository, SendMoneyDetailsRepository sendMoneyDetailsRepository,
			MTransactionRepository mTransactionRepository, MServiceRepository mServiceRepository) {
		super();
		this.matchMoveWalletRepository = matchMoveWalletRepository;
		this.mmCardRepository = mmCardRepository;
		this.physicalCardDetailRepository = physicalCardDetailRepository;
		this.senderApi = senderApi;
		this.userRepository = userRespository;
		this.sendMoneyDetailsRepository = sendMoneyDetailsRepository;
		this.mTransactionRepository = mTransactionRepository;
		this.mServiceRepository = mServiceRepository;
	}

	@Override
	public ResponseDTO createUserOnMM(MUser request) {
		ResponseDTO resp = new ResponseDTO();
		Security.addProvider(new BouncyCastleProvider());

		MultivaluedMap<String, String> userData = new MultivaluedMapImpl();

		userData.add("email", request.getUserDetail().getEmail());
		try {
			userData.add("password", SecurityUtil.md5(request.getUsername()));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		userData.add("first_name", request.getUserDetail().getFirstName());
		userData.add("last_name", request.getUserDetail().getLastName());
		userData.add("mobile_country_code", "91");
		userData.add("mobile", request.getUsername());
		if (request.getUserDetail().getMiddleName() != null) {
			userData.add("middle_name", request.getUserDetail().getMiddleName());
		}
		userData.add("preferred_name",
				request.getUserDetail().getFirstName() + " " + request.getUserDetail().getLastName());
		try {
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).post(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			if (user != null && user.has("code")) {
				String code = user.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = user.getString("description");
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(description);
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					return resp;
				}
			} else {
				// tempSetIdDetails(request.getUserDetail().getEmail(), request.getUsername());
				MatchMoveCreateCardRequest cardRequest = new MatchMoveCreateCardRequest();
				cardRequest.setEmail(request.getUserDetail().getEmail());
				cardRequest.setPassword(SecurityUtil.md5(request.getUsername()));
				cardRequest.setIdNo(user.getString("id"));
				WalletResponse walletResponse = createWallet(cardRequest);
				if (walletResponse.getCode().equalsIgnoreCase("S00")) {
					MatchMoveWallet moveWallet = new MatchMoveWallet();
					moveWallet.setWalletId(walletResponse.getWalletId());
					moveWallet.setWalletNumber(walletResponse.getWalletNumber());
					moveWallet.setUser(request);
					// moveWallet.setMmUserId(walletResponse.getMmUserId());
					moveWallet.setMmUserId(user.getString("id"));
					matchMoveWalletRepository.save(moveWallet);
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					resp.setMessage("wallet created successfully");
					return resp;
				} else {
					resp.setCode("F00");
					resp.setMessage("Creation of wallet failed....");
					return resp;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setMessage("Unable to create user, please try again later.");
		resp.setStatus(ResponseStatus.FAILURE.getKey());
		resp.setStatus(ResponseStatus.FAILURE);
		return resp;
	}

	@Override
	public WalletResponse createWallet(MatchMoveCreateCardRequest request) {
		WalletResponse resp = new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());
		try {
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).header("X-Auth-User-Id", request.getIdNo())
					.post(ClientResponse.class);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			System.err.println("create wallet::" + user);
			if (user != null && user.has("code")) {
				String code = user.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = user.getString("description");
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(description);
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					return resp;
				}
			} else {
				JSONObject date = user.getJSONObject("date");
				String expiry = date.getString("expiry");
				String issue = date.getString("issued");
				String withholdingamt = user.getJSONObject("funds").getJSONObject("withholding").getString("amount");
				String availabeamt = user.getJSONObject("funds").getJSONObject("available").getString("amount");
				boolean cardstatus = user.getJSONObject("status").getBoolean("is_active");
				String walletId = user.getString("id");
				String walletnumber = user.getString("number");

				resp.setAvailableAmt(availabeamt);
				resp.setCard_status(cardstatus);
				resp.setExpiryDate(expiry);
				resp.setIssueDate(issue);
				resp.setWithholdingAmt(withholdingamt);
				resp.setWalletNumber(walletnumber);
				resp.setWalletId(walletId);
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				return resp;
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getValue());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		resp.setStatus(ResponseStatus.FAILURE.getKey());
		return resp;
	}

	@Override
	public WalletResponse assignVirtualCard(MUser mUser) {
		WalletResponse resp = new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());
		try {
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);
			String userId = null;
			if (mmw != null && mmw.getMmUserId() != null) {
				userId = mmw.getMmUserId();

			} else {
				UserKycResponse userResp = getUsers(mUser.getUserDetail().getEmail(), mUser.getUsername());
				userId = userResp.getMmUserId();
			}
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/"
					+ MatchMoveUtil.MATCHMOVE_CARDTYPE_VIRUAL);
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).header("X-Auth-User-Id", userId).post(ClientResponse.class);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);

			if (user != null && user.has("code")) {
				String code = user.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = user.getString("description");
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(description);
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					return resp;
				}
			} else {

				JSONObject date = user.getJSONObject("date");
				String expiry = date.getString("expiry");
				String issue = date.getString("issued");
				String withholdingamt = user.getJSONObject("funds").getJSONObject("withholding").getString("amount");
				String availabeamt = user.getJSONObject("funds").getJSONObject("available").getString("amount");
				boolean cardstatus = user.getJSONObject("status").getBoolean("is_active");
				String walletId = user.getString("id");
				String walletnumber = user.getString("number");
				String authorization1 = SMSConstant.getBasicAuthorization();
				Client client1 = Client.create();
				client1.addFilter(new LoggingFilter(System.out));
				WebResource resource1 = client1.resource(
						MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + walletId + "/securities/tokens");
				ClientResponse resp2 = resource1.accept("application/json").header("Authorization", authorization1)
						.header("X-Auth-User-Id", mmw.getMmUserId()).get(ClientResponse.class);
				String strResponse1 = resp2.getEntity(String.class);
				JSONObject cvvRequest = new JSONObject(strResponse1);

				String cvv = null;
				if (cvvRequest.has("value")) {
					cvv = cvvRequest.getString("value");
				} else {
					cvv = "xxx";
				}
				MatchMoveWallet wal = matchMoveWalletRepository.findByUser(mUser);
				if (wal != null) {
					wal.setIssueDate(issue);
					MMCards cards = mmCardRepository.getVirtualCardsByCardUser(mUser);
					if (cards == null) {
						MMCards virtualCard = new MMCards();
						virtualCard.setCardId(walletId);
						virtualCard.setHasPhysicalCard(false);
						virtualCard.setStatus("Active");
						virtualCard.setWallet(wal);
						MMCards cd = mmCardRepository.save(virtualCard);
						System.err.println("card saved" + cd.getId());

					} else {
						resp.setCode("F00");
						resp.setMessage("You are already assigned with a card.Please Contact Admin for queries");
						return resp;
					}
					matchMoveWalletRepository.save(wal);
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					resp.setMessage("card successfully assigned...");
					return resp;

				} else {
					resp.setCode("F00");
					resp.setMessage("Due to some internal issue card cannot be issued.Please contact customer care");
					return resp;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		resp.setStatus(ResponseStatus.FAILURE.getKey());
		return resp;
	}

	@Override
	public WalletResponse inquireCard(MatchMoveCreateCardRequest request) {
		WalletResponse resp = new WalletResponse();

		try {
			MUser mUser = userRepository.findByUsername(request.getUsername());
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);
			String userId = null;
			if (mmw != null && mmw.getMmUserId() != null) {
				userId = mmw.getMmUserId();

			} else {
				UserKycResponse userResp = getUsers(request.getEmail(), request.getUsername());
				userId = userResp.getMmUserId();
			}

			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client
					.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + request.getCardId());
			ClientResponse resp1 = resource.accept("application/json").header("Authorization", authorization)
					.header("X-Auth-User-Id", userId).get(ClientResponse.class);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			System.err.println("wallet iquirry::" + user);
			if (user != null && user.has("code")) {
				String code = user.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = user.getString("description");
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(description);
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					return resp;
				}
			} else {

				JSONObject date = user.getJSONObject("date");
				String expiry = date.getString("expiry");
				String issue = date.getString("issued");
				String holderName = user.getJSONObject("holder").getString("name");
				if (user.has("funds")) {
					String withholdingamt = user.getJSONObject("funds").getJSONObject("withholding")
							.getString("amount");
					String availabeamt = user.getJSONObject("funds").getJSONObject("available").getString("amount");
				}
				boolean cardstatus = user.getJSONObject("status").getBoolean("is_active");
				String text = user.getJSONObject("status").getString("text");
				String walletId = user.getString("id");
				String walletnumber = user.getString("number");
				System.err.println("walletId:::" + walletId);
				String authorization1 = SMSConstant.getBasicAuthorization();
				Client client1 = Client.create();
				client1.addFilter(new LoggingFilter(System.out));
				WebResource resource1 = client1.resource(
						MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + walletId + "/securities/tokens");
				ClientResponse resp2 = resource1.accept("application/json").header("Authorization", authorization1)
						.header("X-Auth-User-Id", userId).get(ClientResponse.class);
				String strResponse1 = resp2.getEntity(String.class);
				JSONObject cvvRequest = new JSONObject(strResponse1);
				String cvv = null;
				if (cvvRequest.has("value")) {
					cvv = cvvRequest.getString("value");
				} else {
					cvv = "xxx";
				}
				resp.setWalletNumber(walletnumber);
				String expDate = expiry.substring(2, 4) + "/" + expiry.substring(5, 7);
				System.err.println(expDate);
				resp.setExpiryDate(expDate);
				resp.setIssueDate(issue);
				resp.setCardId(walletId);
				resp.setCvv(cvv);
				resp.setHolderName(holderName);
				resp.setStatus(text);
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				resp.setMessage("card successfully assigned...");
				return resp;

			}

		} catch (Exception e) {
			e.printStackTrace();
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		resp.setStatus(ResponseStatus.FAILURE.getKey());
		return resp;
	}

	/**
	 * FETCH USER FROM MATCHMOVE
	 */
	@Override
	public ResponseDTO fetchMMUser(MatchMoveCreateCardRequest request) {
		ResponseDTO resp = new ResponseDTO();
		Security.addProvider(new BouncyCastleProvider());

		try {
			MUser mUser = userRepository.findByUsername(request.getUsername());
			if (mUser == null) {
				resp.setCode("F00");
				resp.setMessage("User Not found");
				return resp;
			}
			String userId = null;
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);
			if (mmw != null && mmw.getMmUserId() != null) {
				userId = mmw.getMmUserId();

			} else {
				UserKycResponse userResp = getUsers(request.getEmail(), request.getUsername());
				userId = userResp.getMmUserId();
			}
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users");
			ClientResponse resp1 = resource.accept("application/json").header("Authorization", authorization)
					.header("X-Auth-User-Id", userId).get(ClientResponse.class);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			System.err.println("fetch mm user response:::" + user);
			if (user != null && user.has("code")) {
				String code = user.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = user.getString("description");
					resp.setCode(ResponseStatus.OAUTH_FAILURE.getValue());
					resp.setMessage(ResponseStatus.OAUTH_FAILURE.getKey());
					resp.setStatus(ResponseStatus.OAUTH_FAILURE.getKey());
					return resp;
				}
			} else {
				resp.setCode("S00");
				resp.setMessage("User fetched successfully");
				return resp;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setMessage("Unable to fetch User");
		resp.setStatus(ResponseStatus.FAILURE.getKey());
		resp.setStatus(ResponseStatus.FAILURE);
		return resp;
	}

	/**
	 * FETCH WALLET ON MM
	 */
	@Override
	public WalletResponse fetchWalletMM(MatchMoveCreateCardRequest request) {
		WalletResponse resp = new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());
		try {
			String userId = null;
			MUser mUser = userRepository.findByUsername(request.getUsername());
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);
			if (mmw != null) {
				userId = mmw.getMmUserId();
			} else {
				UserKycResponse userResp = getUsers(mUser.getUserDetail().getEmail(), mUser.getUsername());
				if (userResp != null && userResp.getCode().equalsIgnoreCase("S00")) {
					userId = userResp.getMmUserId();
					System.err.println(userId + "this is userId");
				}
			}

			System.err.println("UserID:::" + userId);
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets");
			ClientResponse resp1 = resource.header("Authorization", authorization).header("X-Auth-User-Id", userId)
					.get(ClientResponse.class);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			System.err.println("fetch wallet on MM::" + user);
			if (user != null && user.has("code")) {
				String code = user.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = user.getString("description");
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(description);
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					return resp;
				}
			} else {
				JSONObject date = user.getJSONObject("date");
				String expiry = date.getString("expiry");
				String issue = date.getString("issued");
				String withholdingamt = user.getJSONObject("funds").getJSONObject("withholding").getString("amount");
				String availabeamt = user.getJSONObject("funds").getJSONObject("available").getString("amount");
				boolean cardstatus = user.getJSONObject("status").getBoolean("is_active");
				String walletId = user.getString("id");
				String walletnumber = user.getString("number");
				resp.setAvailableAmt(availabeamt);
				resp.setCard_status(cardstatus);
				resp.setExpiryDate(expiry);
				resp.setIssueDate(issue);
				resp.setWithholdingAmt(withholdingamt);
				resp.setWalletNumber(walletnumber);
				resp.setWalletId(walletId);
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				return resp;

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		resp.setStatus(ResponseStatus.FAILURE.getKey());
		return resp;
	}

	@Override
	public WalletResponse fetchWalletMMOld(MatchMoveCreateCardRequest request) {
		WalletResponse resp = new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());
		try {
			String userId = null;
			MUser mUser = userRepository.findByUsername(request.getUsername());
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);
			if (mmw != null) {
				userId = mmw.getMmUserId();
			} else {
				UserKycResponse userResp = getUsers(mUser.getUserDetail().getEmail(), mUser.getUsername());
				if (userResp != null && userResp.getCode().equalsIgnoreCase("S00")) {
					userId = userResp.getMmUserId();
					System.err.println(userId + "this is userId");
				}
			}
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets");
			ClientResponse resp1 = resource.header("Authorization", authorization).header("X-Auth-User-Id", userId)
					.get(ClientResponse.class);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			if (user != null && user.has("code")) {
				String code = user.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = user.getString("description");
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(description);
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					return resp;
				}
			} else {
				JSONObject date = user.getJSONObject("date");
				String expiry = date.getString("expiry");
				String issue = date.getString("issued");
				String withholdingamt = user.getJSONObject("funds").getJSONObject("withholding").getString("amount");
				String availabeamt = user.getJSONObject("funds").getJSONObject("available").getString("amount");
				boolean cardstatus = user.getJSONObject("status").getBoolean("is_active");
				String walletId = user.getString("id");
				String walletnumber = user.getString("number");
				resp.setAvailableAmt(availabeamt);
				resp.setCard_status(cardstatus);
				resp.setExpiryDate(expiry);
				resp.setIssueDate(issue);
				resp.setWithholdingAmt(withholdingamt);
				resp.setWalletNumber(walletnumber);
				resp.setWalletId(walletId);
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				return resp;

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		resp.setStatus(ResponseStatus.FAILURE.getKey());
		return resp;
	}

	// fetch wallet on login
	@Override
	public WalletResponse fetchWalletMMOnLogin(MatchMoveCreateCardRequest request, MUser u) {
		WalletResponse resp = new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());
		try {
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(u);
			String userId = null;
			if (mmw != null && mmw.getMmUserId() != null) {
				userId = mmw.getMmUserId();

			} else {
				UserKycResponse userResp = getUsers(request.getEmail(), request.getUsername());
				userId = userResp.getMmUserId();
			}
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets");
			ClientResponse resp1 = resource.header("Authorization", authorization).header("X-Auth-User-Id", userId)
					.get(ClientResponse.class);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			System.err.println("fetch wallet on MM::" + user);
			if (user != null && user.has("code")) {
				String code = user.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = user.getString("description");
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(description);
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					return resp;
				}
			} else {
				JSONObject date = user.getJSONObject("date");
				String expiry = date.getString("expiry");
				String issue = date.getString("issued");
				String withholdingamt = user.getJSONObject("funds").getJSONObject("withholding").getString("amount");
				String availabeamt = user.getJSONObject("funds").getJSONObject("available").getString("amount");
				boolean cardstatus = user.getJSONObject("status").getBoolean("is_active");
				String walletId = user.getString("id");
				String walletnumber = user.getString("number");
				resp.setAvailableAmt(availabeamt);
				resp.setCard_status(cardstatus);
				resp.setExpiryDate(expiry);
				resp.setIssueDate(issue);
				resp.setWithholdingAmt(withholdingamt);
				resp.setWalletNumber(walletnumber);
				resp.setWalletId(walletId);

				MatchMoveWallet mmWallet = new MatchMoveWallet();
				mmWallet.setIssueDate(issue);
				mmWallet.setWalletId(walletId);
				mmWallet.setWalletNumber(walletnumber);
				mmWallet.setUser(u);
				matchMoveWalletRepository.save(mmWallet);

				resp.setCode(ResponseStatus.SUCCESS.getValue());
				return resp;

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		resp.setStatus(ResponseStatus.FAILURE.getKey());
		return resp;
	}

	/**
	 * DEACTIVATE CARD
	 */

	@Override
	public ResponseDTO deActivateCards(MatchMoveCreateCardRequest request) {
		ResponseDTO resp = new ResponseDTO();
		Security.addProvider(new BouncyCastleProvider());
		JSONObject user = null;
		try {
			MMCards card = mmCardRepository.getCardByCardId(request.getCardId());

			if (card != null) {
				String password = SecurityUtil.md5(card.getWallet().getUser().getUsername());
				String email = card.getWallet().getUser().getUserDetail().getEmail();
			}
			if (request.getRequestType().equalsIgnoreCase("suspend")) {
				MatchMoveWallet mmw = card.getWallet();
				String authorization = SMSConstant.getBasicAuthorization();
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client
						.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + request.getCardId());
				ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
						.delete(ClientResponse.class);
				String strResponse = resp1.getEntity(String.class);
				user = new JSONObject(strResponse);
			} else {
				MultivaluedMap<String, String> data = new MultivaluedMapImpl();
				data.add("type", request.getRequestType().toLowerCase());
				MatchMoveWallet mmw = card.getWallet();
				String authorization = SMSConstant.getBasicAuthorization();
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client
						.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + request.getCardId());
				ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
						.delete(ClientResponse.class, data);
				String strResponse = resp1.getEntity(String.class);
				user = new JSONObject(strResponse);
			}
			System.err.println("deactivate card:::" + user);
			if (user != null && user.has("code")) {
				String code = user.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = user.getString("description");
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(description);
					return resp;
				}
			} else {
				card.setBlocked(true);
				mmCardRepository.save(card);
				resp.setDetails(user.toString());
				resp.setCode("S00");
				resp.setCardDetails(user.getString("id"));
				resp.setStatus(user.getString("status"));
				resp.setMessage("User card Deactivated");
				return resp;
			}
		} catch (InvalidKeyException | NoSuchAlgorithmException | UnsupportedEncodingException | NoSuchProviderException
				| HttpRequestException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException
				| JSONException | ResourceException | UnsupportedMethodException | ShortBufferException
				| InvalidKeySpecException | InvalidAlgorithmParameterException | URISyntaxException e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		resp.setStatus(ResponseStatus.FAILURE.getKey());
		return resp;
	}

	/**
	 * REACTIVATE CARD
	 */

	@Override
	public ResponseDTO reActivateCard(MatchMoveCreateCardRequest request) {
		ResponseDTO result = new ResponseDTO();
		Security.addProvider(new BouncyCastleProvider());
		try {
			MMCards card = mmCardRepository.getCardByCardId(request.getCardId());
			if (card != null) {
				String password = SecurityUtil.md5(card.getWallet().getUser().getUsername());
				String email = card.getWallet().getUser().getUserDetail().getEmail();
			}
			String cardType = null;

			MultivaluedMap<String, String> data = new MultivaluedMapImpl();
			data.add("id", request.getCardId());
			MMCards cards = mmCardRepository.getCardByCardId(request.getCardId());
			if (cards.isHasPhysicalCard()) {
				cardType = MatchMoveUtil.MATCHMOVE_CARDTYPE_PHYSICAL;
			} else {
				cardType = MatchMoveUtil.MATCHMOVE_CARDTYPE_VIRUAL;
			}
			MatchMoveWallet mmw = cards.getWallet();
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client
					.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cardType);
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
					.post(ClientResponse.class, data);
			String strResponse = resp1.getEntity(String.class);
			JSONObject json = new JSONObject(strResponse);
			System.err.println("reactivate card response:::::" + json);
			if (json != null) {
				result.setStatus(json.getString("status"));
				result.setCode("S00");
				result.setMessage("Card unblocked");
				card.setStatus("Active");
				card.setBlocked(false);
				mmCardRepository.save(card);
				return result;
			} else {
				result.setCode("F00");
				result.setMessage("Operation Failed..");
				return result;
			}
		} catch (Exception e) {
			result.setCode("F00");
			result.setMessage("Operation Failed due to some exception...");
			return result;
		}
	}

	@Override
	public ResponseDTO reActivateCardCorp(MatchMoveCreateCardRequest request) {
		ResponseDTO result = new ResponseDTO();
		Security.addProvider(new BouncyCastleProvider());

		try {
			MMCards card = mmCardRepository.getCardByCardId(request.getCardId());
			if (card != null) {
				String password = SecurityUtil.md5(card.getWallet().getUser().getUsername());
				String email = card.getWallet().getUser().getUserDetail().getEmail();
			}
			String cardType = null;
			MultivaluedMap<String, String> data = new MultivaluedMapImpl();
			data.add("id", request.getCardId());
			MMCards cards = mmCardRepository.getCardByCardId(request.getCardId());

			MatchMoveWallet mmw = cards.getWallet();
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/"
					+ MatchMoveUtil.MATCHMOVE_CARDTYPE_PHYSICAL);
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
					.post(ClientResponse.class, data);
			String strResponse = resp1.getEntity(String.class);
			JSONObject json = new JSONObject(strResponse);
			System.err.println("reactivate card response:::::" + json);
			if (json != null) {
				result.setStatus(json.getString("status"));

				result.setCode("S00");
				result.setMessage("User unblocked");
				card.setStatus("Active");
				card.setBlocked(false);
				mmCardRepository.save(card);
				return result;
			} else {
				result.setCode("F00");
				result.setMessage("Operation Failed..");
				return result;
			}
		} catch (Exception e) {
			result.setCode("F00");
			result.setMessage("Operation Failed due to some exception...");
			return result;
		}
	}

	/**
	 * ADD PHYSICAL CARD
	 */

	@Override
	public WalletResponse assignPhysicalCard(MUser mUser, String proxy_number) {
		WalletResponse resp = new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());

		try {
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("assoc_number", "PY" + proxy_number);

			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/"
					+ MatchMoveUtil.MATCHMOVE_CARDTYPE_PHYSICAL);
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
					.post(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);

			System.err.println("assign physical card issuance::" + user);
			if (user != null && user.has("code")) {
				String code = user.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = user.getString("description");
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(description);
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					return resp;
				}
			} else {

				JSONObject date = user.getJSONObject("date");
				String expiry = date.getString("expiry");

				String issue = date.getString("issued");
				String withholdingamt = user.getJSONObject("funds").getJSONObject("withholding").getString("amount");
				String availabeamt = user.getJSONObject("funds").getJSONObject("available").getString("amount");
				boolean cardstatus = user.getJSONObject("status").getBoolean("is_active");
				String walletId = user.getString("id");
				String walletnumber = user.getString("number");
				String activationCode = user.getString("activation_code");
				Client client1 = Client.create();
				client1.addFilter(new LoggingFilter(System.out));
				WebResource resource1 = client1.resource(
						MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + walletId + "/securities/tokens");
				ClientResponse resp2 = resource1.accept("application/json").header("Authorization", authorization)
						.header("X-Auth-User-Id", mmw.getMmUserId()).get(ClientResponse.class);
				String strResponse1 = resp2.getEntity(String.class);
				JSONObject cvvRequest = new JSONObject(strResponse1);

				String cvv = null;
				System.err.println("cvv request::::" + cvvRequest);
				if (cvvRequest.has("value")) {
					cvv = cvvRequest.getString("value");
				} else {
					cvv = "xxx";
				}
				MatchMoveWallet wal = matchMoveWalletRepository.findByUser(mUser);
				if (wal != null) {
					MMCards cards = mmCardRepository.getPhysicalCardByUser(mUser);
					if (cards == null) {
						MMCards virtualCard = new MMCards();
						virtualCard.setCardId(walletId);
						virtualCard.setHasPhysicalCard(true);
						virtualCard.setStatus("Inactive");
						virtualCard.setWallet(wal);
						cards = mmCardRepository.save(virtualCard);
						System.err.println("card saved" + cards.getId());

					} else {
						resp.setCode("F00");
						resp.setMessage("We are facing issues while generating card.Please contact admin for queries");
						return resp;
					}
					PhysicalCardDetails phyDetails = physicalCardDetailRepository.findByWallet(wal);
					if (phyDetails != null) {
						phyDetails.setProxyNumber(proxy_number);
						phyDetails.setActivationCode(activationCode);
						phyDetails.setCards(cards);
						phyDetails = physicalCardDetailRepository.save(phyDetails);
						if (phyDetails != null) {
							senderApi.sendUserSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplete.ACTIVATION_CODE, mUser,
									activationCode);
							resp.setCode(ResponseStatus.SUCCESS.getValue());
							resp.setMessage("card successfully assigned,Activation Code sent to your Contact Number.");
							return resp;
						}
					}

				} else {
					resp.setCode("F00");
					resp.setMessage("Due to some internal issue card cannot be issued.Please contact customer care");
					return resp;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		resp.setStatus(ResponseStatus.FAILURE.getKey());
		return resp;
	}

	@Override
	public ResponseDTO activationPhysicalCard(String activationCode, MUser user) {

		ResponseDTO respDTO = new ResponseDTO();
		MMCards card = mmCardRepository.getPhysicalCardByUser(user);
		PhysicalCardDetails physicalCardDetails = physicalCardDetailRepository.findByCard(card);
		if (physicalCardDetails.getActivationCode().equalsIgnoreCase(activationCode)) {
			ResponseDTO resp = activateMMPhysicalCard(card.getCardId(), activationCode);
			respDTO.setCode(resp.getCode());
			respDTO.setMessage(resp.getMessage());
			return respDTO;
		} else {
			respDTO.setCode("F00");
			respDTO.setMessage("Card Activation failed...Please contact Customer Care");
			return respDTO;
		}
	}

	@Override
	public ResponseDTO activateMMPhysicalCard(String cardId, String activationCode) {
		ResponseDTO result = new ResponseDTO();
		Security.addProvider(new BouncyCastleProvider());
		try {
			MMCards card = mmCardRepository.getCardByCardId(cardId);
			if (card != null) {
				String password = SecurityUtil.md5(card.getWallet().getUser().getUsername());
				String email = card.getWallet().getUser().getUserDetail().getEmail();
				MultivaluedMap<String, String> data = new MultivaluedMapImpl();
				data.add("activation_code", activationCode);
				MatchMoveWallet mmw = card.getWallet();
				String authorization = SMSConstant.getBasicAuthorization();
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client
						.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cardId);
				ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
						.put(ClientResponse.class, data);
				String strResponse = resp1.getEntity(String.class);
				JSONObject json = new JSONObject(strResponse);
				System.err.println("activation response::" + json);
				if (json != null) {
					String status = json.getString("status");
					if (status != null) {
						if (status.equalsIgnoreCase("Success")) {
							card.setStatus("Active");
							mmCardRepository.save(card);
							result.setStatus(status);
							result.setCode("S00");
							result.setMessage("Card Activated Successfully");
							return result;

						} else {
							result.setCode("F00");
							result.setMessage("Activation failed");
							return result;
						}
					}
				} else {
					result.setCode("F00");
					result.setMessage("Operation Failed..");
					return result;
				}
			}
		} catch (Exception e) {
			result.setCode("F00");
			result.setMessage("Operation Failed due to some exception...");
			return result;
		}
		result.setCode("F00");
		result.setMessage("Card Activation failed");
		return result;

	}

	/**
	 * LOAD FUNDS TO WALLET
	 */

	@Override
	public WalletResponse initiateLoadFundsToMMWallet(MUser mUser, String amount) {
		WalletResponse resp = new WalletResponse();
		/*
		 * Security.addProvider(new BouncyCastleProvider()); try { MatchMoveWallet mmw =
		 * matchMoveWalletRepository.findByUser(mUser); MultivaluedMap<String, String>
		 * userData = new MultivaluedMapImpl(); userData.add("email",
		 * mUser.getUserDetail().getEmail()); userData.add("amount", amount); JSONObject
		 * details=new JSONObject(); details.put("pp","razorpay");
		 * userData.add("details", details.toString()); String authorization =
		 * SMSConstant.getBasicAuthorization(); Client client = Client.create();
		 * client.addFilter(new LoggingFilter(System.out)); WebResource resource =
		 * client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL+"/users/wallets/funds");
		 * ClientResponse resp1 =
		 * resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE).header(
		 * "Authorization", authorization).post(ClientResponse.class, userData); String
		 * strResponse = resp1.getEntity(String.class); JSONObject user = new
		 * JSONObject(strResponse); System.err.println("wallet fund initiate load::" +
		 * user); if (user != null && user.has("code")) { String code =
		 * user.getString("code"); if (!CommonValidation.isNull(code) &&
		 * !code.equalsIgnoreCase("200")) { String description =
		 * user.getString("description");
		 * resp.setCode(ResponseStatus.FAILURE.getValue());
		 * resp.setMessage(description);
		 * resp.setStatus(ResponseStatus.FAILURE.getKey()); return resp; } } else {
		 * String authRetrievalId=user.getString("id"); String
		 * walletId=user.getString("wallet_id"); String
		 * status=user.getString("confirm"); resp.setWalletId(walletId);
		 * resp.setAuthRetrivalNo(authRetrievalId); resp.setCode("S00");
		 * resp.setMessage("Load Initiated"); resp.setStatus(status); return resp; } }
		 * catch (Exception e) { // TODO Auto-generated catch block e.printStackTrace();
		 * resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
		 * resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
		 * resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey()); return resp; }
		 */
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		resp.setStatus(ResponseStatus.FAILURE.getKey());
		return resp;
	}

	/**
	 * corporate load to wallet used in bulk load
	 */

	@Override
	public WalletResponse initiateLoadFundsToMMWalletCorp(MUser mUser, String amount) {
		WalletResponse resp = new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());
		try {
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("email", mUser.getUserDetail().getEmail());
			userData.add("amount", amount);
			JSONObject details = new JSONObject();
			details.put("pp", "CORP_LOAD");
			userData.add("details", details.toString());
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).post(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			System.out.println("Strresponse: " + strResponse);
			JSONObject user = new JSONObject(strResponse);
			if (user != null && user.has("code")) {
				String code = user.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = user.getString("description");
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(description);
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					return resp;
				}
			} else {
				String authRetrievalId = user.getString("id");
				String walletId = user.getString("wallet_id");
				String status = user.getString("confirm");
				resp.setWalletId(walletId);
				resp.setAuthRetrivalNo(authRetrievalId);
				resp.setCode("S00");
				resp.setMessage("Load Initiated");
				resp.setStatus(status);
				return resp;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		resp.setStatus(ResponseStatus.FAILURE.getKey());
		return resp;
	}

	@Override
	public WalletResponse initiateLoadFundsToMMWalletUpi(MUser mUser, String amount, String transactionRefNo,
			String upiid) {
		WalletResponse resp = new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());
		try {
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("email", mUser.getUserDetail().getEmail());
			userData.add("amount", amount);
			JSONObject details = new JSONObject();
			details.put("pp", "upi");
			details.put("payment_ref", transactionRefNo);
			details.put("status", "Success");
			details.put("description", upiid);
			userData.add("details", details.toString());
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).post(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			System.err.println("wallet fund initiate load::" + user);
			if (user != null && user.has("code")) {
				String code = user.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = user.getString("description");
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(description);
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					return resp;
				}
			} else {
				String authRetrievalId = user.getString("id");
				String walletId = user.getString("wallet_id");
				String status = user.getString("confirm");
				resp.setWalletId(walletId);
				resp.setAuthRetrivalNo(authRetrievalId);
				resp.setCode("S00");
				resp.setMessage("Load Initiated");
				resp.setStatus(status);
				return resp;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		resp.setStatus(ResponseStatus.FAILURE.getKey());
		return resp;
	}

	@Override
	public WalletResponse initiateLoadFundsToMMWalletPG(MUser mUser, String amount, String transactionRefNo,
			String upiid) {
		WalletResponse resp = new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());
		try {
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("email", mUser.getUserDetail().getEmail());
			userData.add("amount", amount);
			JSONObject details = new JSONObject();
			details.put("pp", "PG");
			details.put("payment_ref", transactionRefNo);
			details.put("status", "Success");
			details.put("description", upiid);
			userData.add("details", details.toString());
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).post(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			System.err.println("wallet fund initiate load::" + user);
			if (user != null && user.has("code")) {
				String code = user.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = user.getString("description");
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(description);
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					return resp;
				}
			} else {
				String authRetrievalId = user.getString("id");
				String walletId = user.getString("wallet_id");
				String status = user.getString("confirm");
				resp.setWalletId(walletId);
				resp.setAuthRetrivalNo(authRetrievalId);
				resp.setCode("S00");
				resp.setMessage("Load Initiated");
				resp.setStatus(status);
				return resp;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		resp.setStatus(ResponseStatus.FAILURE.getKey());
		return resp;
	}

	@Override
	public WalletResponse initiateLoadFundsToMMWalletRazorpay(MUser mUser, String amount, String transactionRefNo) {
		WalletResponse resp = new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());
		try {
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("email", mUser.getUserDetail().getEmail());
			userData.add("amount", amount);
			JSONObject details = new JSONObject();
			details.put("pp", "PG");
			details.put("payment_ref", transactionRefNo);
			details.put("status", "Success");
			userData.add("details", details.toString());
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).post(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			System.err.println("wallet fund initiate load::" + user);
			if (user != null && user.has("code")) {
				String code = user.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = user.getString("description");
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(description);
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					return resp;
				}
			} else {
				String authRetrievalId = user.getString("id");
				String walletId = user.getString("wallet_id");
				String status = user.getString("confirm");
				resp.setWalletId(walletId);
				resp.setAuthRetrivalNo(authRetrievalId);
				resp.setCode("S00");
				resp.setMessage("Load Initiated");
				resp.setStatus(status);
				return resp;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		resp.setStatus(ResponseStatus.FAILURE.getKey());
		return resp;
	}

	/**
	 * CONFIRM FUNDS TO WALLET
	 */

	@Override
	public WalletResponse confirmFundsToMMWallet(MUser mUser, String amount, String authRetrievalNo) {
		WalletResponse resp = new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());
		try {
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client
					.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds/" + authRetrievalNo);
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
					.put(ClientResponse.class);
			String strResponse = resp1.getEntity(String.class);
			JSONObject response = new JSONObject(strResponse);

			System.err.println("confirm funds wallet load::" + response);
			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(description);
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					return resp;
				}
			} else {
				JSONArray transactions = response.getJSONArray("transactions");
				if (transactions != null) {
					JSONObject jobject = transactions.getJSONObject(0);
					if (jobject != null) {
						String status = jobject.getString("status");
						resp.setStatus(status);
						if (status.equalsIgnoreCase("status")) {
							resp.setCode("S00");
							resp.setMessage("Load Confirmed");
							return resp;
						} else {
							resp.setCode("F00");
							resp.setMessage("Load confirmation failed");
							return resp;
						}

					}
				}

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		resp.setStatus(ResponseStatus.FAILURE.getKey());
		return resp;
	}

	/**
	 * TRANSFER FUNDS TO CARD
	 */

	@Override
	public WalletResponse transferFundsToMMCard(MUser mUser, String amount, String cardId) {
		WalletResponse resp = new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());
		try {
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("amount", amount);
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));

			WebResource resource = client
					.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cardId + "/funds");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
					.post(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);

			JSONObject user = new JSONObject(strResponse);

			if (user != null && user.has("code")) {
				String code = user.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = user.getString("description");
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(description);
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					return resp;
				} else {
					resp.setStatus("Success");
					resp.setMessage("Tranfered sucessfully");
					resp.setCode("S00");
					return resp;

				}
			} else {
				if (user != null) {
					resp.setStatus("Sucess");
					resp.setMessage("Tranfered sucessfully");
					resp.setCode("S00");
					return resp;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		resp.setStatus(ResponseStatus.FAILURE.getKey());
		return resp;
	}

	@Override
	public WalletResponse transferFundsToMMCardTemp(MUser mUser, String amount, String cardId) {
		WalletResponse resp = new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());

		try {

			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);

			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));

			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("amount", amount);

			WebResource resource = client
					.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cardId + "/funds");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
					.post(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);

			JSONObject user = new JSONObject(strResponse);
			if (user != null && user.has("code")) {
				String code = user.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = user.getString("description");
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(description);
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					return resp;
				} else {
					resp.setStatus("Sucess");
					resp.setMessage("Tranfered sucessfully");
					resp.setCode("S00");
					return resp;

				}
			} else {
				if (user != null) {
					resp.setStatus("Sucess");
					resp.setMessage("Tranfered sucessfully");
					resp.setCode("S00");
					return resp;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		resp.setStatus(ResponseStatus.FAILURE.getKey());
		return resp;
	}

	/**
	 * KYC PROCESS
	 */
	@Override
	public UserKycResponse kycUserMM(UserKycRequest request) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());

		try {

			String authorization = SMSConstant.getBasicAuthorization();

			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(request.getUser());
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();

			userData.add("address_1", request.getAddress1());
			userData.add("address_2", request.getAddress2());
			userData.add("city", request.getCity());
			userData.add("state", request.getState());
			userData.add("country", request.getCountry());
			userData.add("zipcode", request.getPinCode());

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/addresses/residential");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
					.put(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject address = new JSONObject(strResponse);

			System.err.println(address);
			if (address != null && address.has("code")) {
				String code = address.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = address.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {

				resp.setMessage("residential address saved");
				resp.setCode("S00");
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public UserKycResponse setIdDetails(UserKycRequest request) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());

		try {
			String authorization = SMSConstant.getBasicAuthorization();
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(request.getUser());

			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("title", "Mr");
			userData.add("id_type", request.getId_type());
			userData.add("id_number", request.getId_number());
			userData.add("country_of_issue", request.getCountry());
			userData.add("birthday", sdf.format(request.getUser().getUserDetail().getDateOfBirth()));
			userData.add("gender", "male");

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
					.put(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);

			System.err.println("id::" + user);
			if (user != null && user.has("code")) {
				String code = user.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = user.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {

				resp.setMessage("Id Details saved");
				resp.setCode("S00");
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	/**
	 * set id details for corporate
	 */

	/*
	 * @Override public UserKycResponse setIdDetailsCorp(MUser user) {
	 * UserKycResponse resp = new UserKycResponse(); Security.addProvider(new
	 * BouncyCastleProvider()); String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
	 * String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY; String
	 * consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET; Connection wallet =
	 * new Connection(host, consumerKey, consumerSecret); JSONObject user = null;
	 * 
	 * try { //wallet.authenticate(request.getUser().getUserDetail().getEmail(),
	 * SecurityUtil.md5(request.getUser().getUsername())); String authorization =
	 * SMSConstant.getBasicAuthorization(); MatchMoveWallet mmw =
	 * matchMoveWalletRepository.findByUser(user);
	 * 
	 * //Map<String, String> userData = new HashMap<String, String>();
	 * MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
	 * if(user.getUser) userData.add("title",request.getTitle());
	 * userData.add("id_type", request.getId_type());
	 * userData.add("id_number",request.getId_number());
	 * userData.add("country_of_issue",request.getCountry());
	 * userData.add("birthday",
	 * sdf.format(request.getUser().getUserDetail().getDateOfBirth()));
	 * userData.add("gender",request.getGender()); //user = wallet.consume("users",
	 * HttpRequest.METHOD_PUT, userData);
	 * 
	 * Client client = Client.create(); client.addFilter(new
	 * LoggingFilter(System.out)); WebResource resource =
	 * client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL+"/users"); ClientResponse
	 * resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE).header(
	 * "Authorization", authorization).header("X-Auth-User-Id",
	 * mmw.getMmUserId()).put(ClientResponse.class, userData); String strResponse =
	 * resp1.getEntity(String.class); JSONObject user = new JSONObject(strResponse);
	 * 
	 * System.err.println("id::"+user); if (user != null && user.has("code")) {
	 * String code = user.getString("code"); if (!CommonValidation.isNull(code) &&
	 * !code.equalsIgnoreCase("200")) { String description =
	 * user.getString("description"); resp.setCode("F00");
	 * resp.setMessage(description); return resp; } } else {
	 * 
	 * resp.setMessage("Id Details saved"); resp.setCode("S00"); return resp;
	 * 
	 * } } catch (Exception e) { // TODO Auto-generated catch block
	 * e.printStackTrace();
	 * resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
	 * resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey()); return resp; }
	 * resp.setCode(ResponseStatus.FAILURE.getValue());
	 * resp.setMessage("Ohh!! snap, please try again later."); return resp; }
	 */

	@Override
	public UserKycResponse setImagesForKyc(UserKycRequest request) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());

		try {
			byte[] array = recoverImageFromUrl(request.getFilePath());

			String encodedfile = new String(Base64.encodeBase64(array), "UTF-8");
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("data", encodedfile.trim());
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(request.getUser());
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client
					.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/authentications/documents");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
					.post(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject response = new JSONObject(strResponse);

			System.err.println("the upload response::" + response);
			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {

				resp.setMessage("Images saved saved");
				resp.setCode("S00");
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	public byte[] recoverImageFromUrl(String urlText) throws Exception {
		URL url = new URL(urlText);
		ByteArrayOutputStream output = new ByteArrayOutputStream();

		try (InputStream inputStream = url.openStream()) {
			int n = 0;
			byte[] buffer = new byte[1024];
			while (-1 != (n = inputStream.read(buffer))) {
				output.write(buffer, 0, n);
			}
		}

		return output.toByteArray();
	}

	@Override
	public UserKycResponse confirmKyc(UserKycRequest request) {
		UserKycResponse resp = new UserKycResponse();

		try {
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(request.getUser());
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client
					.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/authentications/documents");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
					.put(ClientResponse.class);
			String strResponse = resp1.getEntity(String.class);
			JSONObject response = new JSONObject(strResponse);

			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {

				resp.setMessage("Kyc confirmed");
				resp.setCode("S00");
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	/**
	 * BALANCE API
	 */
	@Override
	public double getBalance(MUser request) {
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		JSONObject user = null;
		String cardId = null;
		double balance = 0;
		try {

			wallet.authenticate(request.getUserDetail().getEmail(), SecurityUtil.md5(request.getUsername()));
			JSONObject response = wallet.consume("users/wallets");

			System.err.println("balance API::" + response);
			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					return balance;
				}
			} else {
				JSONObject funds = response.getJSONObject("funds");
				if (funds != null) {
					balance = Double.parseDouble(funds.getJSONObject("available").getString("amount"));
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			return balance;
		}

		return balance;
	}

	// getbalanceapp

	@Override
	public Double getBalanceApp(MUser request) {
		ResponseDTO res = new ResponseDTO();
		Security.addProvider(new BouncyCastleProvider());
		JSONObject user = null;
		String cardId = null;
		double balance = 0;
		String mmUserId = null;
		try {

			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(request);
			if (mmw == null) {
				UserKycResponse kycResponse = getUsers(request.getUserDetail().getEmail(), request.getUsername());
				if (kycResponse != null) {
					mmUserId = kycResponse.getMmUserId();
				}
			} else {
				mmUserId = mmw.getMmUserId();
			}
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets");
			ClientResponse resp = resource.accept("application/json").header("Authorization", authorization)
					.header("X-Auth-User-Id", mmUserId).get(ClientResponse.class);
			String strResponse = resp.getEntity(String.class);
			JSONObject response = new JSONObject(strResponse);

			System.err.println("balance API::" + response);
			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					return balance;
				}
			} else {
				JSONObject funds = response.getJSONObject("funds");
				if (funds != null) {
					balance = Double.parseDouble(funds.getJSONObject("available").getString("amount"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return balance;
		}

		return balance;
	}

	@Override

	public Double getBalanceAppWallet(MUser request) {
		ResponseDTO res = new ResponseDTO();
		Security.addProvider(new BouncyCastleProvider());

		JSONObject user = null;
		String cardId = null;
		double balance = 0;
		String mmUserId = null;
		try {

			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(request);
			if (mmw == null) {
				UserKycResponse kycResponse = getUsers(request.getUserDetail().getEmail(), request.getUsername());
				if (kycResponse != null) {
					mmUserId = kycResponse.getMmUserId();
				}
			} else {
				mmUserId = mmw.getMmUserId();
			}
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets");
			ClientResponse resp = resource.header("Authorization", authorization).header("X-Auth-User-Id", mmUserId)
					.get(ClientResponse.class);
			String strResponse = resp.getEntity(String.class);
			JSONObject response = new JSONObject(strResponse);

			System.err.println("balance API::" + response);
			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");

					return balance;
				}
			} else {
				JSONObject funds = response.getJSONObject("funds");
				if (funds != null) {
					balance = Double.parseDouble(funds.getJSONObject("available").getString("amount"));

				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			return balance;
		}

		return balance;
	}

	@Override
	public Double getBalanceCus(MUser request) {
		ResponseDTO res = new ResponseDTO();
		Security.addProvider(new BouncyCastleProvider());
		JSONObject user = null;
		String cardId = null;
		String mmUserId = null;
		double balance = 0;
		try {

			MMCards cards = mmCardRepository.getPhysicalCardByUser(request);
			if (cards != null) {
				cardId = cards.getCardId();

			}
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(request);
			if (mmw != null && mmw.getMmUserId() != null) {
				mmUserId = mmw.getMmUserId();
			} else {
				UserKycResponse userResponse = getUsers(request.getUserDetail().getEmail(), request.getUsername());
				if (userResponse != null && userResponse.getCode().equalsIgnoreCase("S00")) {
					mmUserId = userResponse.getMmUserId();
				}
			}
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(SMSConstant.GETBALANCE + cardId);
			ClientResponse resp = resource.accept("application/json").header("Authorization", authorization)
					.header("X-Auth-User-Id", mmUserId).get(ClientResponse.class);
			String strResponse = resp.getEntity(String.class);
			JSONObject response = new JSONObject(strResponse);

			System.err.println("balance API::" + response);
			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					return balance;
				}
			} else {
				JSONObject funds = response.getJSONObject("funds");
				if (funds != null) {
					balance = Double.parseDouble(funds.getJSONObject("available").getString("amount"));
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return balance;
		}

		return balance;
	}

	@Override
	public ResponseDTO getBalanceExp(MUser request) {
		ResponseDTO dto = new ResponseDTO();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		JSONObject user = null;
		String cardId = null;
		double balance = 0;
		try {

			MMCards cards = mmCardRepository.getPhysicalCardByUser(request);
			if (cards != null) {
				if (cards.getStatus().equalsIgnoreCase("Active")) {
					cardId = cards.getCardId();
				}
			}
			MMCards phyCard = mmCardRepository.getVirtualCardsByCardUser(request);
			if (phyCard != null) {
				if (phyCard.getStatus().equalsIgnoreCase("Active")) {
					cardId = phyCard.getCardId();
				}
			}
			wallet.authenticate(request.getUserDetail().getEmail(), SecurityUtil.md5(request.getUsername()));
			JSONObject response = wallet.consume("users/wallets/cards/" + cardId);
			System.err.println("balance API::" + response);
			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					dto.setCode("F00");

					return dto;
				}
			} else {

				JSONObject funds = response.getJSONObject("funds");
				if (funds != null) {
					balance = Double.parseDouble(funds.getJSONObject("available").getString("amount"));
				}

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			dto.setCode("F00");
			return dto;
		}
		dto.setCode("S00");
		dto.setBalance(balance);
		return dto;
	}

	/**
	 * MY BALANCE
	 * 
	 */
	@Override
	public double myBalance(MUser userMob) {
		Security.addProvider(new BouncyCastleProvider());
		String cardId = null;
		double balance = 0;
		try {
			MMCards cards = mmCardRepository.getPhysicalCardByUser(userMob);
			if (cards != null) {
				if (cards.getStatus().equalsIgnoreCase("Active")) {
					cardId = cards.getCardId();
				}
			}
			MMCards phyCard = mmCardRepository.getVirtualCardsByCardUser(userMob);
			if (phyCard != null) {
				if (phyCard.getStatus().equalsIgnoreCase("Active")) {
					cardId = phyCard.getCardId();
				}
			}
			System.err.println("cardid ----:: " + cardId);
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(userMob);
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(SMSConstant.GETBALANCE + cardId);
			ClientResponse resp = resource.accept("application/json").type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
					.get(ClientResponse.class);
			String strResponse = resp.getEntity(String.class);
			JSONObject response = new JSONObject(strResponse);
			System.err.println("balance API::" + response);
			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					return balance;
				}
			} else {

				JSONObject funds = response.getJSONObject("funds");
				if (funds != null) {
					balance = Double.parseDouble(funds.getJSONObject("available").getString("amount"));
				}

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			return -1;
		}

		return balance;
	}

	/**
	 * TEMPORARY KYC PROCESS
	 * 
	 * @throws Exception
	 */

	public static void main(String[] args) throws Exception {
		System.err.println(SecurityUtil.md5("8895093779"));
	}

	@Override
	public UserKycResponse tempKycUserMM(String email, String mobile) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		JSONObject user = null;

		try {
			wallet.authenticate(email, SecurityUtil.md5(mobile));
			Map<String, String> userData = new HashMap<String, String>();
			userData.put("address_1", "1/1,DCN homes,Kormangala");
			userData.put("address_2", "6th block");
			userData.put("city", "Bangalore");
			userData.put("state", "Karnataka");
			userData.put("country", "India");
			userData.put("zipcode", "560095");
			JSONObject address = wallet.consume("users/addresses/residential", HttpRequest.METHOD_PUT, userData);
			System.err.println("residential address::" + address);
			if (address != null && address.has("code")) {
				String code = address.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = address.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {

				resp.setMessage("residential address saved");
				resp.setCode("S00");
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public UserKycResponse tempSetIdDetails(String email, String mobile) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		JSONObject user = null;

		try {
			wallet.authenticate(email, SecurityUtil.md5(mobile));

			Map<String, String> userData = new HashMap<String, String>();
			MUser username = userRepository.findByUsername(mobile);
			Date dob = username.getUserDetail().getDateOfBirth();

			String date = sdf.format(dob);

			System.err.println("date format:::::::::::::::::::::::::::::" + date);
			userData.put("title", "Mr");
			userData.put("id_type", "voters_id");
			userData.put("id_number", "RPZ" + mobile);
			userData.put("country_of_issue", "India");
			userData.put("birthday", date);
			userData.put("gender", "male");
			user = wallet.consume("users", HttpRequest.METHOD_PUT, userData);
			System.err.println("date format:::::::::::::::::::::::::::::" + date);

			System.err.println("Id type title:::" + user);
			System.err.println("date format:::::::::::::::::::::::::::::" + date);

			if (user != null && user.has("code")) {
				String code = user.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = user.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {

				resp.setMessage("Id Details saved");
				resp.setCode("S00");
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public UserKycResponse edittempSetIdDetails(String email, String mobile) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		JSONObject user = null;

		try {
			wallet.authenticate(email, SecurityUtil.md5(mobile));

			Map<String, String> userData = new HashMap<String, String>();
			MUser username = userRepository.findByUsername(mobile);
			Date dob = username.getUserDetail().getDateOfBirth();

			String date = sdf.format(dob);

			System.err.println("date format is:::::::::::::::::::::::::::::::::::::::::::" + date);
			userData.put("title", "Mr");
			userData.put("country_of_issue", "India");
			userData.put("id_type", "voters_id");
			userData.put("id_number", "RPZ" + mobile);
			userData.put("birthday", date);
			userData.put("gender", "male");
			user = wallet.consume("users", HttpRequest.METHOD_PUT, userData);
			System.err.println("Id type title:::" + user);
			if (user != null && user.has("code")) {
				String code = user.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = user.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {

				resp.setMessage("Id Details saved");
				resp.setCode("S00");
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public UserKycResponse getUsersOath(String email, String mobile) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		try {
			MUser u = userRepository.findByUsername(mobile);
			if (u == null) {
				resp.setCode("F00");
				resp.setMessage("User not found");
				return resp;
			}
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(u);
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users");
			ClientResponse res = resource.header("Authorization", authorization)
					.header("X-Auth-User-Id", mmw.getMmUserId()).get(ClientResponse.class);
			String strResponse = res.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);

			if (user != null && user.has("code")) {
				String code = user.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = user.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {
				resp.setMmUserId(user.getString("id"));
				resp.setDetails(user.toString());
				resp.setMessage("Id Details saved");
				resp.setCode("S00");
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public UserKycResponse getUsers(String email, String mobile) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		JSONObject user = null;

		try {
			wallet.authenticate(email, SecurityUtil.md5(mobile));

			user = wallet.consume("users");
			if (user != null && user.has("code")) {
				String code = user.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = user.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {
				resp.setMmUserId(user.getString("id"));
				resp.setDetails(user.toString());
				resp.setMessage("Id Details saved");
				resp.setCode("S00");
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public UserKycResponse getConsumers() {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		try {

			JSONObject response = wallet.consume("oauth/consumer");
			System.err.println("get response:::" + response);
			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {
				resp.setPrefundingBalance(response.getJSONObject("consumer").getString("fund_floating_balance"));
				resp.setDetails(response.toString());
				resp.setMessage("Balance fetched");
				resp.setCode("S00");
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public UserKycResponse tempSetImagesForKyc(String email, String mobile) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		JSONObject user = null;

		try {
			wallet.authenticate(email, SecurityUtil.md5(mobile));

			String files = "/usr/local/tomcat/webapps/ROOT/WEB-INF/startup/easemytrip.png";
			byte[] array = Files.readAllBytes(new File(files).toPath());
			String encodedfile = new String(Base64.encodeBase64(array), "UTF-8");
			Map<String, String> userData = new HashMap<String, String>();
			userData.put("data", encodedfile.trim());
			JSONObject response = wallet.consume("users/authentications/documents", HttpRequest.METHOD_POST, userData);
			System.err.println("image upload response:::" + response);
			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {

				resp.setMessage("Images saved saved");
				resp.setCode("S00");
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public UserKycResponse tempConfirmKyc(String email, String mobile) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		JSONObject user = null;

		try {
			wallet.authenticate(email, SecurityUtil.md5(mobile));
			JSONObject response = wallet.consume("users/authentications/documents", HttpRequest.METHOD_PUT);

			System.err.println("confirm kyc response::::" + response);
			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {
				tempKycStatusApproval(email, SecurityUtil.md5(mobile));
				resp.setMessage("Kyc confirmed");
				resp.setCode("S00");
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public UserKycResponse tempKycStatusApproval(String email, String mobile) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		/*
		 * String host = MatchMoveUtil.MATCHMOVE_HOST_URL; String consumerKey =
		 * MatchMoveUtil.MATCHMOVE_CONSUMER_KEY; String consumerSecret =
		 * MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET; Connection wallet = new
		 * Connection(host, consumerKey, consumerSecret); JSONObject user = null;
		 */

		try {
			// wallet.authenticate(email,SecurityUtil.md5(mobile));

			// Map<String, String> userData = new HashMap<String, String>();
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("status", "approved");
			// JSONObject response = wallet.consume("users/authentications/statuses",
			// HttpRequest.METHOD_PUT, userData);

			MUser u = userRepository.findByUsername(mobile);
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(u);
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client
					.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/authentications/statuses");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
					.put(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject response = new JSONObject(strResponse);

			System.err.println("temp kyc approval::::" + response);
			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {

				resp.setMessage("Kyc confirmed");
				resp.setCode("S00");
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public UserKycResponse debitFromMMWalletToCard(String email, String mobile, String cardId, String amount) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		/*
		 * String host = MatchMoveUtil.MATCHMOVE_HOST_URL; String consumerKey =
		 * MatchMoveUtil.MATCHMOVE_CONSUMER_KEY; String consumerSecret =
		 * MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET; Connection wallet = new
		 * Connection(host, consumerKey, consumerSecret); JSONObject user = null;
		 */

		try {
			// wallet.authenticate(email,SecurityUtil.md5(mobile));
			// Map<String, String> userData = new HashMap<String, String>();
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("amount", amount);
			userData.add("message", "return funds");
			// JSONObject response = wallet.consume("users/wallets/cards/"+cardId+"/funds",
			// HttpRequest.METHOD_DELETE,userData);

			MUser u = userRepository.findByUsername(mobile);
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(u);
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client
					.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cardId + "/funds");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
					.delete(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject response = new JSONObject(strResponse);

			System.err.println("Debit from card to wallet response::" + response);
			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {

				resp.setMessage("Funds transferred to wallet");
				resp.setCode("S00");
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	/**
	 * DEBIT TO PREFUND ACCOUNT
	 */

	@Override
	public UserKycResponse debitFromCard(String email, String mobile, String cardId, String amount) {
		UserKycResponse resp = new UserKycResponse();

		try {
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("amount", amount);
			userData.add("message", "return funds");

			MUser u = userRepository.findByUsername(mobile);
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(u);
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
					.delete(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject response = new JSONObject(strResponse);

			System.err.println("debit from card to prefund:::" + response);
			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {
				String id = response.getString("id");
				deductFromCardConfirmation(id);
				resp.setMessage("Funds transferred to wallet");
				resp.setCode("S00");
				resp.setIndicator(id);
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public UserKycResponse debitFromCardFingoole(String email, String mobile, String cardId, String amount) {
		UserKycResponse resp = new UserKycResponse();

		try {
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("amount", amount);
			userData.add("message", "return funds");
			JSONObject details = new JSONObject();
			details.put("pp", "Fingoole");
			userData.add("details", details.toString());

			MUser u = userRepository.findByUsername(mobile);
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(u);
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client
					.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cardId + "/funds");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
					.delete(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject response = new JSONObject(strResponse);

			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {
				String id = response.getString("id");
				deductFromCardConfirmation(id);
				resp.setMessage("Funds transferred to wallet");
				resp.setCode("S00");
				resp.setIndicator(id);
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public UserKycResponse debitFromCardCorp(String email, String mobile, String cardId, String amount) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		JSONObject user = null;

		try {
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("amount", amount);
			userData.add("message", "Corporate Debit");
			JSONObject details = new JSONObject();
			details.put("pp", "Corporate Debit");
			userData.add("details", details.toString());
			MUser u = userRepository.findByUsername(mobile);
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(u);
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client
					.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cardId + "/funds");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
					.delete(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject response = new JSONObject(strResponse);

			System.err.println("debit from card to prefund:::" + response);
			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {
				String id = response.getString("id");
				deductFromCardConfirmation(id);
				resp.setMessage("Funds transferred to wallet");
				resp.setCode("S00");
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public UserKycResponse debitFromCardInHouse(String email, String mobile, String amount, String transactionRefNo) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		JSONObject user = null;

		try {
			Map<String, String> userData = new HashMap<String, String>();
			userData.put("amount", amount);
			userData.put("message", "Bill Payment and recharges");
			userData.put("email", email);

			JSONObject details = new JSONObject();
			details.put("pp", "Bill Payment and recharges");
			details.put("payment_ref", transactionRefNo);
			userData.put("details", details.toString());
			JSONObject response = wallet.consume("users/wallets/funds", HttpRequest.METHOD_DELETE, userData);
			System.err.println("debit from card to prefund:::" + response);
			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {
				String id = response.getString("id");
				deductFromCardConfirmation(id);
				resp.setMessage("Funds transferred to wallet");
				resp.setAuthID(id);
				resp.setCode("S00");
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public UserKycResponse debitFromCardInBus(String email, String mobile, String amount, String transactionRefNo) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		JSONObject user = null;

		try {
			Map<String, String> userData = new HashMap<String, String>();
			userData.put("amount", amount);
			userData.put("message", "Bus Payment");
			userData.put("email", email);

			JSONObject details = new JSONObject();
			details.put("pp", "Bus Payment");
			details.put("payment_ref", transactionRefNo);
			userData.put("details", details.toString());
			JSONObject response = wallet.consume("users/wallets/funds", HttpRequest.METHOD_DELETE, userData);
			System.err.println("debit from card to prefund:::" + response);
			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {
				String id = response.getString("id");
				deductFromCardConfirmation(id);
				resp.setMessage("Funds transferred to wallet");
				resp.setAuthID(id);
				resp.setCode("S00");
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public UserKycResponse debitFromCardInFlight(String email, String mobile, String amount, String transactionRefNo) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		JSONObject user = null;

		try {
			Map<String, String> userData = new HashMap<String, String>();
			userData.put("amount", amount);
			userData.put("message", "Flight Payment");
			userData.put("email", email);

			JSONObject details = new JSONObject();
			details.put("pp", "Bus Payment");
			details.put("payment_ref", transactionRefNo);
			userData.put("details", details.toString());
			JSONObject response = wallet.consume("users/wallets/funds", HttpRequest.METHOD_DELETE, userData);
			System.err.println("debit from card to prefund:::" + response);
			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {
				String id = response.getString("id");
				deductFromCardConfirmation(id);
				resp.setMessage("Funds transferred to wallet");
				resp.setAuthID(id);
				resp.setCode("S00");
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public UserKycResponse deductFromCardConfirmation(String id) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		JSONObject user = null;

		try {
			// wallet.authenticate(email,SecurityUtil.md5(mobile));
			Map<String, String> userData = new HashMap<String, String>();
			userData.put("ids", id);

			JSONObject transaction = wallet.consume("oauth/consumer/funds", HttpRequest.METHOD_DELETE, userData);
			System.err.println("deduct confirmation:::" + transaction);
			if (transaction != null && transaction.has("code")) {
				String code = transaction.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = transaction.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {

				resp.setMessage("Funds transferred to wallet");
				resp.setCode("S00");
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public UserKycResponse getTransactions(MUser userRequest) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		JSONObject responseVir = null;
		JSONObject responsePhy = null;
		try {
			wallet.authenticate(userRequest.getUserDetail().getEmail(), SecurityUtil.md5(userRequest.getUsername()));
			MMCards virCard = mmCardRepository.getVirtualCardsByCardUser(userRequest);
			if (virCard != null) {
				String vircardId = virCard.getCardId();
				Map<String, String> data = new HashMap<>();
				data.put("limit", "90");
				responseVir = wallet.consume("users/wallets/cards/" + vircardId + "/transactions",
						HttpRequest.METHOD_GET, data);
				System.err.println("virtual response::::" + responseVir);
			}

			MMCards phyCard = mmCardRepository.getPhysicalCardByUser(userRequest);
			if (phyCard != null) {
				String phycardId = phyCard.getCardId();
				Map<String, String> data = new HashMap<>();
				data.put("limit", "90");
				responsePhy = wallet.consume("users/wallets/cards/" + phycardId + "/transactions",
						HttpRequest.METHOD_GET, data);
				System.err.println("physical response::" + responsePhy);
			}

			if (responsePhy != null && responsePhy.has("code")) {
				String code = responsePhy.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = responsePhy.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
				}

			} else {
				if (responsePhy != null) {
					resp.setPhysicalCardTransactions(responsePhy.toString());
					resp.setMessage("TransactionList fetched");
					resp.setCode("S00");

				}
			}

			if (responseVir != null && responseVir.has("code")) {
				String code = responseVir.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = responseVir.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
				}

			} else {
				resp.setVirtualTransactions(responseVir.toString());
				resp.setMessage("TransactionList fetched");
				resp.setCode("S00");

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}

		return resp;
	}

	/**
	 * RESETTING PINS
	 */

	@Override
	public UserKycResponse resetPins(MUser userRequest) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		JSONObject user = null;
		String cardId = null;
		try {
			wallet.authenticate(userRequest.getUserDetail().getEmail(), SecurityUtil.md5(userRequest.getUsername()));
			MMCards phyCard = mmCardRepository.getPhysicalCardByUser(userRequest);
			if (phyCard != null) {
				if (phyCard.getStatus().equalsIgnoreCase("Active")) {
					cardId = phyCard.getCardId();
				}
			}
			Map<String, String> userData = new HashMap<String, String>();
			// MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.put("mode", "M");
			JSONObject response = wallet.consume("users/wallets/cards/" + cardId + "/pins/reset",
					HttpRequest.METHOD_GET, userData);

			/*
			 * MatchMoveWallet mmw = phyCard.getWallet(); String authorization =
			 * SMSConstant.getBasicAuthorization(); Client client = Client.create();
			 * client.addFilter(new LoggingFilter(System.out)); WebResource resource =
			 * client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL+"/users/wallets/cards/"+
			 * cardId+"/pins/reset"); ClientResponse resp1 = resource.queryParam("mode",
			 * "M").header("Authorization", authorization).header("X-Auth-User-Id",
			 * mmw.getMmUserId()).get(ClientResponse.class); String strResponse =
			 * resp1.getEntity(String.class); JSONObject response = new
			 * JSONObject(strResponse);
			 */

			System.err.println("pin reset response::::" + response);
			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {

				resp.setDetails(response.toString());
				resp.setMessage("Your 4-digit secure PIN has been sent to your registered Email Id");
				resp.setCode("S00");
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public MMCards findCardByCardId(String cardId) {
		// TODO Auto-generated method stub
		return mmCardRepository.getCardByCardId(cardId);
	}

	@Override
	public MMCards findCardByUserAndStatus(MUser user) {
		// TODO Auto-generated method stub
		return mmCardRepository.getCardByUserAndStatus(user, "Active");
	}

	/**
	 * FUND TRANSFER FROM CARD TO CARD
	 */

	/**
	 * CHECK CREDIT PREFUND BALANCE
	 */

	@Override
	public boolean checkCreditPrefundBalance(String amount) {

		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		try {
			JSONObject transaction = wallet.consume(
					"oauth/consumer/" + MatchMoveUtil.MATCHMOVE_CONSUMER_KEY + "/prefund/credit",
					HttpRequest.METHOD_GET);
			System.err.println("prefund credit balance::" + transaction);
			if (transaction != null && transaction.has("code")) {
				String code = transaction.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = transaction.getString("description");
					return false;
				}
			}
			if (transaction != null) {
				String balance = transaction.getString("balance");
				double actAmount = Double.parseDouble(amount);
				double actBalance = Double.parseDouble(balance);
				if (actBalance < actAmount) {
					return false;
				} else {
					return true;
				}
			}
		} catch (InvalidKeyException | NoSuchAlgorithmException | UnsupportedEncodingException | NoSuchProviderException
				| HttpRequestException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException
				| JSONException | ResourceException | UnsupportedMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public WalletResponse assignPhysicalCardFromAdmin(MUser mUser, String proxy_number) {

		WalletResponse resp = new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		// JSONObject user = null;
		try {
			wallet.authenticate(mUser.getUserDetail().getEmail(), SecurityUtil.md5(mUser.getUsername()));

			// Map<String,String> userData=new HashMap<String,String>();
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("assoc_number", "PY" + proxy_number);
			// user = wallet.consume("users/wallets/cards/" +
			// MatchMoveUtil.MATCHMOVE_CARDTYPE_PHYSICAL, HttpRequest.METHOD_POST,userData);

			String authorization = SMSConstant.getBasicAuthorization();
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/"
					+ MatchMoveUtil.MATCHMOVE_CARDTYPE_PHYSICAL);
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
					.post(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);

			System.err.println("assign physical card issuance::" + user);
			if (user != null && user.has("code")) {
				String code = user.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = user.getString("description");
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(description);
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					return resp;
				}
			} else {

				JSONObject date = user.getJSONObject("date");
				String expiry = date.getString("expiry");
				String issue = date.getString("issued");
				String withholdingamt = user.getJSONObject("funds").getJSONObject("withholding").getString("amount");
				String availabeamt = user.getJSONObject("funds").getJSONObject("available").getString("amount");
				boolean cardstatus = user.getJSONObject("status").getBoolean("is_active");
				String walletId = user.getString("id");
				String walletnumber = user.getString("number");
				String activationCode = user.getString("activation_code");

				JSONObject cvvRequest = wallet.consume("users/wallets/cards/" + walletId + "/securities/tokens");
				String cvv = null;
				System.err.println("cvv request::::" + cvvRequest);
				if (cvvRequest.has("value")) {
					cvv = cvvRequest.getString("value");
				} else {
					cvv = "xxx";
				}
				MatchMoveWallet wal = matchMoveWalletRepository.findByUser(mUser);
				if (wal != null) {
					MMCards cards = mmCardRepository.getPhysicalCardByUser(mUser);
					if (cards == null) {
						MMCards virtualCard = new MMCards();
						virtualCard.setCardId(walletId);
						virtualCard.setHasPhysicalCard(true);
						virtualCard.setStatus("Inactive");
						virtualCard.setWallet(wal);
						cards = mmCardRepository.save(virtualCard);
						System.err.println("card saved" + cards.getId());
					} else {
						resp.setCode("F00");
						resp.setMessage("We are facing issues while generating card.Please contact admin for queries");
						return resp;
					}
					ResponseDTO response = activationPhysicalCard(activationCode, mUser);
					resp.setCode(response.getCode());
					resp.setMessage("Physical card activated.");
				} else {
					resp.setCode("F00");
					resp.setMessage("Due to some internal issue card cannot be issued.Please contact customer care");
					return resp;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		resp.setStatus(ResponseStatus.FAILURE.getKey());
		return resp;
	}

	@Override
	public WalletResponse transferFundFromCardToWallet(String amount, MUser user, String cardId, String recipient) {
		WalletResponse resp = new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());
		try {
			MUser recipeintUser = userRepository.findByUsername(recipient);
			return walletToWalletTransfer(amount, user, cardId, recipeintUser);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public WalletResponse transferFundFromCardToWalletDonation(String amount, MUser user, String cardId,
			String recipient) {
		WalletResponse resp = new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		try {
			// MUser recipeintUser=userRepository.findByUsername(recipient);
			// wallet.authenticate(user.getUserDetail().getEmail(),
			// SecurityUtil.md5(user.getUsername()));
			// Map<String, String> userData = new HashMap<String, String>();
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();

			userData.add("amount", amount);
			userData.add("message", "return funds");

			// JSONObject transaction = wallet.consume("users/wallets/cards/" + cardId +
			// "/funds", HttpRequest.METHOD_DELETE, userData);

			String authorization = SMSConstant.getBasicAuthorization();
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(user);

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client
					.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cardId + "/funds");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
					.delete(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject transaction = new JSONObject(strResponse);

			System.out.println("transfer from card to wallet::::" + transaction);
			if (transaction != null && transaction.has("code")) {
				String code = transaction.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = transaction.getString("description");
					resp.setMessage(description);
					resp.setCode("F00");
					return resp;
				}
			}
			if (transaction != null) {
				// return walletToWalletTransfer(amount, user, cardId, recipeintUser);
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				resp.setStatus(ResponseStatus.SUCCESS.getKey());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public WalletResponse walletToWalletTransfer(String amount, MUser user, String cardId, MUser recipient) {
		WalletResponse resp = new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());
		try {

			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("email", recipient.getUserDetail().getEmail());
			userData.add("amount", amount);

			JSONObject details = new JSONObject();
			details.put("pp", "fund transfer");
			userData.add("details", details.toString());
			String authorization = SMSConstant.getBasicAuthorization();
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(user);

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
					.post(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject transaction = new JSONObject(strResponse);

			System.out.println("wallet to wallet::::" + transaction);
			if (transaction != null && transaction.has("code")) {
				String code = transaction.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = transaction.getString("description");
					resp.setMessage(description);
					resp.setCode("F00");
					return resp;
				}
			}
			if (transaction != null) {

				String transactionRefNo = transaction.getString("id");
				resp.setAuthRetrivalNo(transactionRefNo);
				SendMoneyDetails sendMoney = new SendMoneyDetails();
				sendMoney.setRecipientNo(recipient.getUsername());
				sendMoney.setAmount(amount);
				sendMoney.setChecked(false);
				sendMoney.setStatus("Pending");
				sendMoney.setTransactionId(transactionRefNo);
				sendMoney.setUser(user);
				sendMoneyDetailsRepository.save(sendMoney);
				resp.setCode("S00");
				resp.setMessage("Transaction Successful.Amount will be reflected in Recipient's Card in 5 minutes");
				/*
				 * MMCards vCard=mmCardRepository.getVirtualCardsByCardUser(recipient); MMCards
				 * pCard=mmCardRepository.getPhysicalCardByUser(recipient);
				 */
				/*
				 * acknowledgeFundTransfer(amount, user, transactionRefNo,
				 * recipient.getUsername()); if(pCard.getStatus().equalsIgnoreCase("Active")){
				 * resp=transferFundsToMMCard(recipient, amount, pCard.getCardId()); }else{
				 * if(vCard.getStatus().equalsIgnoreCase("Active")){
				 * resp=transferFundsToMMCard(recipient, amount, vCard.getCardId()); } }
				 * if(resp.getCode().equalsIgnoreCase("S00")){ sendMoney.setStatus("Success");
				 * sendMoneyDetailsRepository.save(sendMoney);
				 * 
				 * }else{ sendMoneyDetailsRepository.save(sendMoney);
				 * 
				 * }
				 */ return resp;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode("F00");
			resp.setMessage("OPeration failed please contact customer care");
			return resp;
		}
		return resp;
	}

	@Override
	public WalletResponse acknowledgeFundTransfer(String amount, String transactionId, String recipient) {
		WalletResponse resp = new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());
		try {
			MUser recipientUser = userRepository.findByUsername(recipient);
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(recipientUser);
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client
					.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds/" + transactionId);
			ClientResponse resp1 = resource.accept("application/json").header("Authorization", authorization)
					.header("X-Auth-User-Id", mmw.getMmUserId()).put(ClientResponse.class);
			String strResponse = resp1.getEntity(String.class);
			JSONObject transaction = new JSONObject(strResponse);

			System.out.println("acknowledge transfer:::" + transaction);
			if (transaction != null && transaction.has("code")) {
				String code = transaction.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = transaction.getString("description");
					resp.setMessage(description);
					resp.setCode("F00");
					return resp;
				}
			}
			if (transaction != null) {
				resp.setCode("S00");
				return resp;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public WalletResponse getFundTransferDetails(MUser user, String type) {
		WalletResponse resp = new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		try {
			wallet.authenticate(user.getUserDetail().getEmail(), SecurityUtil.md5(user.getUsername()));
			Map<String, String> userData = new HashMap<String, String>();
			userData.put("type", type);
			JSONObject transaction = wallet.consume("users/wallets/funds", HttpRequest.METHOD_GET, userData);

			System.out.println(transaction);
			if (transaction != null && transaction.has("code")) {
				String code = transaction.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = transaction.getString("description");
					resp.setMessage(description);
					resp.setCode("F00");
					return resp;
				}
			}
			if (transaction != null) {
				resp.setCode("S00");
				resp.setDetails(transaction.toString());
				return resp;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public WalletResponse getFundTransferDetails(String id) {
		WalletResponse resp = new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		try {
			JSONObject transaction = wallet.consume("users/wallets/funds/" + id, HttpRequest.METHOD_GET);

			System.out.println(transaction);
			if (transaction != null && transaction.has("code")) {
				String code = transaction.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = transaction.getString("description");
					resp.setMessage(description);
					resp.setCode("F00");
					return resp;
				}
			}
			if (transaction != null) {
				resp.setCode("S00");
				resp.setDetails(transaction.toString());
				return resp;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public UserKycResponse getTransactions(MUser userRequest, String page) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		JSONObject responseVir = null;
		JSONObject responsePhy = null;
		try {
			wallet.authenticate(userRequest.getUserDetail().getEmail(), SecurityUtil.md5(userRequest.getUsername()));
			MMCards virCard = mmCardRepository.getVirtualCardsByCardUser(userRequest);
			if (virCard != null) {
				String vircardId = virCard.getCardId();
				Map<String, String> data = new HashMap<>();
				data.put("limit", "10");
				responseVir = wallet.consume("users/wallets/cards/" + vircardId + "/transactions/" + page,
						HttpRequest.METHOD_GET, data);
				System.err.println("virtual response::::" + responseVir);
			}

			MMCards phyCard = mmCardRepository.getPhysicalCardByUser(userRequest);
			if (phyCard != null) {
				String phycardId = phyCard.getCardId();
				Map<String, String> data = new HashMap<>();
				data.put("limit", "10");
				responsePhy = wallet.consume("users/wallets/cards/" + phycardId + "/transactions/" + page,
						HttpRequest.METHOD_GET, data);
				System.err.println("physical response::" + responsePhy);
			}

			if (responsePhy != null && responsePhy.has("code")) {
				String code = responsePhy.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = responsePhy.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
				}

			} else {
				if (responsePhy != null) {

					resp.setPhysicalCardTransactions(responsePhy.toString());
					resp.setMessage("TransactionList fetched");
					resp.setCode("S00");

				}
			}

			if (responseVir != null && responseVir.has("code")) {
				String code = responseVir.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = responseVir.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
				}
			} else {
				resp.setVirtualTransactions(responseVir.toString());
				resp.setMessage("TransactionList fetched");
				resp.setCode("S00");

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}

		return resp;
	}

	@Override
	public WalletResponse cancelFundTransfer(MUser recipient, String amount, String id) {
		WalletResponse resp = new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());
		try {
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("email", recipient.getUserDetail().getEmail());
			userData.add("amount", amount);
			JSONObject details = new JSONObject();
			details.put("pp", "PPI");
			userData.add("details", details.toString());
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(recipient);
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds/" + id);
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
					.delete(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject transaction = new JSONObject(strResponse);

			System.out.println("cancel fund Transfer:::::::::" + transaction);
			if (transaction != null && transaction.has("code")) {
				String code = transaction.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = transaction.getString("description");
					resp.setMessage(description);
					resp.setCode("F00");
					return resp;
				}
			}
			if (transaction != null) {
				resp.setCode("S00");
				resp.setDetails(transaction.toString());
				return resp;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public WalletResponse assignPhysicalCardFromAgent(MUser mUser, String proxy_number) {
		WalletResponse resp = new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());
		/*
		 * String host = MatchMoveUtil.MATCHMOVE_HOST_URL; String consumerKey =
		 * MatchMoveUtil.MATCHMOVE_CONSUMER_KEY; String consumerSecret =
		 * MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET; Connection wallet = new
		 * Connection(host, consumerKey, consumerSecret); JSONObject user = null;
		 */
		try {
			// wallet.authenticate(mUser.getUserDetail().getEmail(),
			// SecurityUtil.md5(mUser.getUsername()));
			// Map<String,String> userData=new HashMap<String,String>();
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("assoc_number", "PY" + proxy_number);
			// user = wallet.consume("users/wallets/cards/" +
			// MatchMoveUtil.MATCHMOVE_CARDTYPE_PHYSICAL, HttpRequest.METHOD_POST,userData);

			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/"
					+ MatchMoveUtil.MATCHMOVE_CARDTYPE_PHYSICAL);
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
					.post(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);

			System.err.println("assign physical card issuance::" + user);
			if (user != null && user.has("code")) {
				String code = user.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = user.getString("description");
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(description);
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					return resp;
				}
			} else {

				JSONObject date = user.getJSONObject("date");
				String expiry = date.getString("expiry");

				String issue = date.getString("issued");
				String withholdingamt = user.getJSONObject("funds").getJSONObject("withholding").getString("amount");
				String availabeamt = user.getJSONObject("funds").getJSONObject("available").getString("amount");
				boolean cardstatus = user.getJSONObject("status").getBoolean("is_active");
				String walletId = user.getString("id");
				String walletnumber = user.getString("number");
				String activationCode = user.getString("activation_code");

				// JSONObject cvvRequest = wallet.consume("users/wallets/cards/" + walletId +
				// "/securities/tokens");

				Client client1 = Client.create();
				client1.addFilter(new LoggingFilter(System.out));
				WebResource resource1 = client1.resource(
						MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + walletId + "/securities/tokens");
				ClientResponse resp2 = resource1.accept("application/json").header("Authorization", authorization)
						.header("X-Auth-User-Id", mmw.getMmUserId()).get(ClientResponse.class);
				String strResponse1 = resp2.getEntity(String.class);
				JSONObject cvvRequest = new JSONObject(strResponse1);

				String cvv = null;
				System.err.println("cvv request::::" + cvvRequest);
				if (cvvRequest.has("value")) {
					cvv = cvvRequest.getString("value");
				} else {
					cvv = "xxx";
				}
				MatchMoveWallet wal = matchMoveWalletRepository.findByUser(mUser);
				if (wal != null) {
					MMCards cards = mmCardRepository.getPhysicalCardByUser(mUser);
					if (cards == null) {
						MMCards virtualCard = new MMCards();
						virtualCard.setCardId(walletId);
						virtualCard.setHasPhysicalCard(true);
						virtualCard.setStatus("Inactive");
						virtualCard.setWallet(wal);
						cards = mmCardRepository.save(virtualCard);
						logger.error("card saved" + cards.getId());
					} else {
						resp.setCode("F00");
						resp.setMessage("We are facing issues while generating card.Please contact admin for queries");
						return resp;
					}
					PhysicalCardDetails phyDetails = physicalCardDetailRepository.findByWallet(wal);
					if (phyDetails != null) {
						phyDetails.setProxyNumber(proxy_number);
						phyDetails.setActivationCode(activationCode);
						phyDetails.setCards(cards);
						phyDetails = physicalCardDetailRepository.save(phyDetails);
						if (phyDetails != null) {
							senderApi.sendUserSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplete.ACTIVATION_CODE, mUser,
									activationCode);
							resp.setCode(ResponseStatus.SUCCESS.getValue());
							resp.setMessage("card successfully assigned,Activation Code sent to your Contact Number.");
							return resp;
						}
					} else {
						phyDetails = new PhysicalCardDetails();
						phyDetails.setProxyNumber(proxy_number);
						phyDetails.setActivationCode(activationCode);
						phyDetails.setCards(cards);
						phyDetails.setWallet(wal);
						phyDetails = physicalCardDetailRepository.save(phyDetails);
						if (phyDetails != null) {
							senderApi.sendUserSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplete.ACTIVATION_CODE, mUser,
									activationCode);
							resp.setCode(ResponseStatus.SUCCESS.getValue());
							resp.setMessage("card successfully assigned,Activation Code sent to your Contact Number.");
							return resp;
						}

					}
				} else {
					resp.setCode("F00");
					resp.setMessage("Due to some internal issue card cannot be issued.Please contact customer care");
					return resp;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		resp.setStatus(ResponseStatus.FAILURE.getKey());
		return resp;
	}

	@Override
	public UserKycResponse deductFromWalletToConsumer(String userName, String amount) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		JSONObject user = null;

		try {
			// wallet.authenticate(userName.getUserDetail().getEmail(),SecurityUtil.md5(userName.getUsername()));
			Map<String, String> userData = new HashMap<String, String>();
			userData.put("email", userName);
			userData.put("amount", amount);
			JSONObject details = new JSONObject();
			details.put("pp", "razorpay");
			userData.put("details", details.toString());
			JSONObject transaction = wallet.consume("users/wallets/funds", HttpRequest.METHOD_DELETE, userData);

			System.err.println("deduct confirmation:::" + transaction);
			if (transaction != null && transaction.has("code")) {
				String code = transaction.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = transaction.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {

				resp.setMessage("Funds transferred to wallet");
				resp.setCode("S00");
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public WalletResponse getFundTransfer(MUser recipient) {
		WalletResponse resp = new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());
		try {

			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(recipient);
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds/transfers");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
					.get(ClientResponse.class);
			String strResponse = resp1.getEntity(String.class);
			JSONObject transaction = new JSONObject(strResponse);

			System.out.println("Fund transfer resp::" + transaction);
			if (transaction != null && transaction.has("code")) {
				String code = transaction.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = transaction.getString("description");
					resp.setMessage(description);
					resp.setCode("F00");
					return resp;
				}
			}
			if (transaction != null) {
				resp.setCode("S00");
				resp.setDetails(transaction.toString());
				return resp;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public UserKycResponse getTransactionsCustom(MUser userRequest, String page) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		JSONObject responseVir = null;
		JSONObject responsePhy = null;
		try {
			wallet.authenticate(userRequest.getUserDetail().getEmail(), SecurityUtil.md5(userRequest.getUsername()));
			MMCards virCard = mmCardRepository.getVirtualCardsByCardUser(userRequest);
			if (virCard != null) {
				String vircardId = virCard.getCardId();
				Map<String, String> data = new HashMap<>();
				data.put("limit", "10");
				responseVir = wallet.consume("users/wallets/cards/" + vircardId + "/transactions/" + page,
						HttpRequest.METHOD_GET, data);
				System.err.println("virtual response::::" + responseVir);
			}

			MMCards phyCard = mmCardRepository.getPhysicalCardByUser(userRequest);
			if (phyCard != null) {
				String phycardId = phyCard.getCardId();
				Map<String, String> data = new HashMap<>();
				data.put("limit", "10");
				responsePhy = wallet.consume("users/wallets/cards/" + phycardId + "/transactions/" + page,
						HttpRequest.METHOD_GET, data);
				System.err.println("physical response::" + responsePhy);
			}

			if (responsePhy != null && responsePhy.has("code")) {
				String code = responsePhy.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = responsePhy.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
				}

			} else {
				if (responsePhy != null) {
					JSONArray transactionArray = responsePhy.getJSONArray("transactions");
					List<CustomUserTransactions> transListPhy = new ArrayList<>();
					if (transactionArray != null) {
						for (int i = 0; i < transactionArray.length(); i++) {
							CustomUserTransactions trRespPhy = new CustomUserTransactions();
							JSONObject transactionObject = transactionArray.getJSONObject(i);
							String amount = transactionObject.getString("amount");
							String date = transactionObject.getString("date");
							String indicator = transactionObject.getString("indicator");
							String status = transactionObject.getString("status");
							String description = transactionObject.getString("description");
							JSONObject detailsJson = transactionObject.getJSONObject("details");

							String type = transactionObject.getString("type");
							if (detailsJson != null) {
								if (type != null) {
									if (type.trim().equalsIgnoreCase("Purchase")) {
										String merchantName = detailsJson.getString("merchantname");
										trRespPhy.setMessage(merchantName);

									} else if (type.trim().equalsIgnoreCase("Load")) {
										trRespPhy.setMessage("Load money of Rs " + amount);
									} else if (type.trim().equalsIgnoreCase("Unload")) {
										trRespPhy.setMessage("Send Money of Rs " + amount);
									} else if (type.trim().equalsIgnoreCase("Get Card")) {
										trRespPhy.setMessage("Card Fees");
									} else if (type.trim().equalsIgnoreCase("Money Transfer Debit")) {
										trRespPhy.setMessage(description);
									} else {
										trRespPhy.setMessage("ATM withdrawal");
									}
								} else {
									trRespPhy.setMessage("ATM withdrawal");
								}
							}
							trRespPhy.setAmount(amount);
							trRespPhy.setIndicator(indicator);
							trRespPhy.setDate(date);
							trRespPhy.setStatus(status);
							transListPhy.add(trRespPhy);

						}
					}
					resp.setCustomPhysicalTransaction(transListPhy);
					resp.setPhysicalCardTransactions(responsePhy.toString());
					resp.setCode("S00");

				}
			}

			if (responseVir != null && responseVir.has("code")) {
				String code = responseVir.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = responseVir.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
				}

			} else {
				List<CustomUserTransactions> transListVir = new ArrayList<>();

				if (responseVir != null) {
					JSONArray transactionArray = responseVir.getJSONArray("transactions");
					if (transactionArray != null) {

						for (int i = 0; i < transactionArray.length(); i++) {

							CustomUserTransactions trRespVir = new CustomUserTransactions();
							JSONObject transactionObject = transactionArray.getJSONObject(i);
							String amount = transactionObject.getString("amount");
							String date = transactionObject.getString("date");
							String indicator = transactionObject.getString("indicator");
							String description = transactionObject.getString("description");
							String status = transactionObject.getString("status");
							JSONObject detailsJson = transactionObject.getJSONObject("details");

							String type = transactionObject.getString("type");
							if (detailsJson != null) {
								if (type != null) {
									if (type.trim().equalsIgnoreCase("Purchase")) {
										String merchantName = detailsJson.getString("merchantname");
										trRespVir.setMessage(merchantName);

									} else if (type.trim().equalsIgnoreCase("Load")) {
										trRespVir.setMessage("Load money of Rs " + amount);
									} else if (type.trim().equalsIgnoreCase("Unload")) {
										trRespVir.setMessage("Send Money of Rs " + amount);
									} else if (type.trim().equalsIgnoreCase("Get Card")) {
										trRespVir.setMessage("Card Fees");
									} else if (type.trim().equalsIgnoreCase("Money Transfer Debit")) {
										trRespVir.setMessage(description);
									} else {
										trRespVir.setMessage("ATM withdrawal");
									}
								} else {
									trRespVir.setMessage("ATM withdrawal");
								}
							}
							trRespVir.setAmount(amount);
							trRespVir.setIndicator(indicator);
							trRespVir.setDate(date);
							trRespVir.setStatus(status);
							transListVir.add(trRespVir);

						}
					}
					/*
					 * resp.setPhysicalCardTransactions(responsePhy.toString());
					 * resp.setMessage("TransactionList fetched"); resp.setCode("S00");
					 */
					resp.setVirtualTransactions(responseVir.toString());
					resp.setCode("S00");
					resp.setCustomVirtualTransaction(transListVir);

				}

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}

		return resp;
	}

	// Admin user trax

	@Override
	public UserKycResponse getTransactionsCustomAdmin(MUser userRequest) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		JSONObject responseVir = null;
		JSONObject responsePhy = null;
		try {
			wallet.authenticate(userRequest.getUserDetail().getEmail(), SecurityUtil.md5(userRequest.getUsername()));
			MMCards virCard = mmCardRepository.getVirtualCardsByCardUser(userRequest);
			if (virCard != null) {
				String vircardId = virCard.getCardId();
				Map<String, String> data = new HashMap<>();
				data.put("limit", "90");
				responseVir = wallet.consume("users/wallets/cards/" + vircardId + "/transactions",
						HttpRequest.METHOD_GET, data);
				System.err.println("virtual response::::" + responseVir);
			}

			MMCards phyCard = mmCardRepository.getPhysicalCardByUser(userRequest);
			if (phyCard != null) {
				String phycardId = phyCard.getCardId();
				Map<String, String> data = new HashMap<>();
				data.put("limit", "90");
				responsePhy = wallet.consume("users/wallets/cards/" + phycardId + "/transactions",
						HttpRequest.METHOD_GET, data);
				System.err.println("physical response::" + responsePhy);
			}

			if (responsePhy != null && responsePhy.has("code")) {
				String code = responsePhy.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = responsePhy.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
				}

			} else {
				if (responsePhy != null) {
					JSONArray transactionArray = responsePhy.getJSONArray("transactions");
					List<CustomUserTransactions> transListPhy = new ArrayList<>();
					if (transactionArray != null) {
						for (int i = 0; i < transactionArray.length(); i++) {
							CustomUserTransactions trRespPhy = new CustomUserTransactions();
							JSONObject transactionObject = transactionArray.getJSONObject(i);
							String amount = transactionObject.getString("amount");
							String date = transactionObject.getString("date");
							String indicator = transactionObject.getString("indicator");
							String status = transactionObject.getString("status");
							String description = transactionObject.getString("description");

							JSONObject detailsJson = transactionObject.getJSONObject("details");

							String type = transactionObject.getString("type");
							if (detailsJson != null) {
								if (type != null) {
									if (type.trim().equalsIgnoreCase("Purchase")) {
										String merchantName = detailsJson.getString("merchantname");
										trRespPhy.setMessage(merchantName);

									} else if (type.trim().equalsIgnoreCase("Load")) {
										trRespPhy.setMessage("Load money of Rs " + amount);
									} else if (type.trim().equalsIgnoreCase("Unload")) {
										trRespPhy.setMessage("Send Money of Rs " + amount);
									} else if (type.trim().equalsIgnoreCase("Get Card")) {
										trRespPhy.setMessage("Card Fees");
									} else if (type.trim().equalsIgnoreCase("Money Transfer Debit")) {
										trRespPhy.setMessage(description);
									} else {
										trRespPhy.setMessage("ATM withdrawal");
									}
								} else {
									trRespPhy.setMessage("ATM withdrawal");
								}
							}
							trRespPhy.setAmount(amount);
							trRespPhy.setIndicator(indicator);
							trRespPhy.setDate(date);
							trRespPhy.setStatus(status);
							transListPhy.add(trRespPhy);

						}
					}
					resp.setCustomPhysicalTransaction(transListPhy);
					resp.setPhysicalCardTransactions(responsePhy.toString());
					resp.setCode("S00");

				}
			}

			if (responseVir != null && responseVir.has("code")) {
				String code = responseVir.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = responseVir.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
				}

			} else {
				List<CustomUserTransactions> transListVir = new ArrayList<>();

				if (responseVir != null) {
					JSONArray transactionArray = responseVir.getJSONArray("transactions");
					if (transactionArray != null) {

						for (int i = 0; i < transactionArray.length(); i++) {

							CustomUserTransactions trRespVir = new CustomUserTransactions();
							JSONObject transactionObject = transactionArray.getJSONObject(i);
							String amount = transactionObject.getString("amount");
							String date = transactionObject.getString("date");
							String indicator = transactionObject.getString("indicator");
							String status = transactionObject.getString("status");
							String description = transactionObject.getString("description");

							JSONObject detailsJson = transactionObject.getJSONObject("details");

							String type = transactionObject.getString("type");
							if (detailsJson != null) {
								if (type != null) {
									if (type.trim().equalsIgnoreCase("Purchase")) {
										String merchantName = detailsJson.getString("merchantname");
										trRespVir.setMessage(merchantName);

									} else if (type.trim().equalsIgnoreCase("Load")) {
										trRespVir.setMessage("Load money of Rs " + amount);
									} else if (type.trim().equalsIgnoreCase("Unload")) {
										trRespVir.setMessage("Send Money of Rs " + amount);
									} else if (type.trim().equalsIgnoreCase("Get Card")) {
										trRespVir.setMessage("Card Fees");
									} else if (type.trim().equalsIgnoreCase("Money Transfer Debit")) {
										trRespVir.setMessage(description);
									} else {
										trRespVir.setMessage("ATM withdrawal");
									}
								} else {
									trRespVir.setMessage("ATM withdrawal");
								}
							}
							trRespVir.setAmount(amount);
							trRespVir.setIndicator(indicator);
							trRespVir.setDate(date);
							trRespVir.setStatus(status);
							transListVir.add(trRespVir);

						}
					}
					/*
					 * resp.setPhysicalCardTransactions(responsePhy.toString());
					 * resp.setMessage("TransactionList fetched"); resp.setCode("S00");
					 */
					resp.setVirtualTransactions(responseVir.toString());
					resp.setCode("S00");
					resp.setCustomVirtualTransaction(transListVir);

				}

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}

		return resp;
	}

	@Override
	public UserKycResponse resetPinsCorp(MUser userRequest) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		JSONObject user = null;
		String cardId = null;
		try {
			wallet.authenticate(userRequest.getUserDetail().getEmail(), SecurityUtil.md5(userRequest.getUsername()));
			MMCards phyCard = mmCardRepository.getPhysicalCardByUser(userRequest);
			if (phyCard != null) {
				if (phyCard.getStatus().equalsIgnoreCase("Active")) {
					cardId = phyCard.getCardId();
				}
			}
			Map<String, String> userData = new HashMap<String, String>();
			userData.put("mode", "S");
			JSONObject response = wallet.consume("users/wallets/cards/" + cardId + "/pins/reset",
					HttpRequest.METHOD_GET, userData);
			System.err.println("pin reset response::::" + response);
			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {

				resp.setDetails(response.toString());
				resp.setMessage("Your 4-digit secure PIN has been sent to your registered Email Id");
				resp.setCode("S00");
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public UserKycResponse updateEmail(MUser userRequest, String email) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		JSONObject user = null;
		String cardId = null;
		try {
			wallet.authenticate(userRequest.getUserDetail().getEmail(), SecurityUtil.md5(userRequest.getUsername()));

			Map<String, String> userData = new HashMap<String, String>();
			userData.put("email", email);
			JSONObject response = wallet.consume("users", HttpRequest.METHOD_PUT, userData);

			System.err.println("response from update email::::" + response);
			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {

				resp.setDetails(response.toString());
				resp.setMessage("Success");
				resp.setCode("S00");
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public UserKycResponse checkKYCStatus(MUser userRequest) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		/*
		 * String host = MatchMoveUtil.MATCHMOVE_HOST_URL; String consumerKey =
		 * MatchMoveUtil.MATCHMOVE_CONSUMER_KEY; String consumerSecret =
		 * MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET; Connection wallet = new
		 * Connection(host, consumerKey, consumerSecret); JSONObject user = null;
		 */
		String cardId = null;
		try {
			// wallet.authenticate(userRequest.getUserDetail().getEmail(),SecurityUtil.md5(userRequest.getUsername()));
			// JSONObject response = wallet.consume("users/authentications/documents",
			// HttpRequest.METHOD_GET);

			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(userRequest);
			String userId = null;
			if (mmw != null && mmw.getMmUserId() != null) {
				userId = mmw.getMmUserId();
			} else {
				UserKycResponse userResp = getUsers(userRequest.getUserDetail().getEmail(), userRequest.getUsername());
				userId = userResp.getMmUserId();
			}
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client
					.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/authentications/documents");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).header("X-Auth-User-Id", userId).get(ClientResponse.class);
			String strResponse = resp1.getEntity(String.class);
			JSONObject response = new JSONObject(strResponse);

			System.err.println("check status kyc::::" + response);
			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {
				resp.setDetails(response.toString());
				resp.setStatus(response.getString("status"));
				resp.setMessage("Success");
				resp.setCode("S00");
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	/**
	 * CORPORATE KYC
	 */

	@Override
	public UserKycResponse kycUserMMCorporate(MKycDetail request) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		JSONObject user = null;

		try {
			wallet.authenticate(request.getUser().getUserDetail().getEmail(),
					SecurityUtil.md5(request.getUser().getUsername()));

			Map<String, String> userData = new HashMap<String, String>();
			userData.put("address_1", request.getAddress1());
			userData.put("address_2", request.getAddress2());
			userData.put("city", request.getCity());
			userData.put("state", request.getState());
			userData.put("country", request.getCountry());
			userData.put("zipcode", request.getPinCode());
			JSONObject address = wallet.consume("users/addresses/residential", HttpRequest.METHOD_PUT, userData);
			System.err.println(address);
			if (address != null && address.has("code")) {
				String code = address.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = address.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {

				resp.setMessage("residential address saved");
				resp.setCode("S00");
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public UserKycResponse setIdDetailsCorporate(MKycDetail request) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		JSONObject user = null;

		try {
			wallet.authenticate(request.getUser().getUserDetail().getEmail(),
					SecurityUtil.md5(request.getUser().getUsername()));

			Map<String, String> userData = new HashMap<String, String>();

			userData.put("title", "Mr");
			userData.put("id_type", request.getIdType());
			userData.put("id_number", request.getIdNumber());
			userData.put("country_of_issue", request.getCountry());
			userData.put("birthday", sdf.format(request.getUser().getUserDetail().getDateOfBirth()));
			userData.put("gender", "male");
			user = wallet.consume("users", HttpRequest.METHOD_PUT, userData);
			System.err.println("id::" + user);
			if (user != null && user.has("code")) {
				String code = user.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = user.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {

				resp.setMessage("Id Details saved");
				resp.setCode("S00");
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public UserKycResponse setImagesForKyc1(MKycDetail request) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		JSONObject user = null;

		try {
			wallet.authenticate(request.getUser().getUserDetail().getEmail(),
					SecurityUtil.md5(request.getUser().getUsername()));
			byte[] array = recoverImageFromUrl(request.getIdImage());

			// InputStream inputStream = new ByteArrayInputStream(array);
			String encodedfile = new String(Base64.encodeBase64(array), "UTF-8");
			Map<String, String> userData = new HashMap<String, String>();
			userData.put("data", encodedfile.trim());
			JSONObject response = wallet.consume("users/authentications/documents", HttpRequest.METHOD_POST, userData);
			System.err.println("the upload response::" + response);
			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {

				resp.setMessage("Images saved saved");
				resp.setCode("S00");
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public UserKycResponse setImagesForKyc2(MKycDetail request) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		JSONObject user = null;

		try {
			wallet.authenticate(request.getUser().getUserDetail().getEmail(),
					SecurityUtil.md5(request.getUser().getUsername()));
			byte[] array = recoverImageFromUrl(request.getIdImage1());

			// InputStream inputStream = new ByteArrayInputStream(array);
			String encodedfile = new String(Base64.encodeBase64(array), "UTF-8");
			Map<String, String> userData = new HashMap<String, String>();
			userData.put("data", encodedfile.trim());
			JSONObject response = wallet.consume("users/authentications/documents", HttpRequest.METHOD_POST, userData);
			System.err.println("the upload response::" + response);
			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {

				resp.setMessage("Images saved saved");
				resp.setCode("S00");
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public UserKycResponse tempConfirmKycCorporate(MKycDetail detail) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		JSONObject user = null;

		try {
			wallet.authenticate(detail.getUser().getUserDetail().getEmail(),
					SecurityUtil.md5(detail.getUser().getUsername()));
			JSONObject response = wallet.consume("users/authentications/documents", HttpRequest.METHOD_PUT);
			System.err.println("confirm kyc response::::" + response);
			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {
				UserKycResponse approvalResp = tempKycStatusApproval(detail.getUser().getUserDetail().getEmail(),
						detail.getUser().getUsername());
				if (approvalResp.getCode().equalsIgnoreCase("S00")) {
					resp.setMessage("Kyc confirmed");
					resp.setCode("S00");
					return resp;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public UserKycResponse setPIN(MUser detail, String pin) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		/*
		 * String host = MatchMoveUtil.MATCHMOVE_HOST_URL; String consumerKey =
		 * MatchMoveUtil.MATCHMOVE_CONSUMER_KEY; String consumerSecret =
		 * MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET; Connection wallet = new
		 * Connection(host, consumerKey, consumerSecret);
		 */
		JSONObject response = null;

		try {
			// wallet.authenticate(detail.getUserDetail().getEmail(),SecurityUtil.md5(detail.getUsername()));
			MMCards cards = mmCardRepository.getPhysicalCardByUser(detail);
			if (cards != null) {
				// Map<String, String> userData = new HashMap<String, String>();
				MultivaluedMap<String, String> userData = new MultivaluedMapImpl();

				// String
				// encodedBlock=PINBlockUtil.format0Encode(pin,"000091"+detail.getUsername());
				String encodedBlock = TrippleDSEncryption.get3desPinBlock(pin, "91" + detail.getUsername());
				System.err.println("encoded block:::" + encodedBlock.toUpperCase());
				System.err.println("The sent PIN::::" + pin);
				userData.add("pinblock", encodedBlock.toUpperCase());
				// response = wallet.consume("users/wallets/cards/" + cards.getCardId() +
				// "/pins", HttpRequest.METHOD_POST, userData);

				MatchMoveWallet mmw = cards.getWallet();
				String authorization = SMSConstant.getBasicAuthorization();
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client.resource(
						MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cards.getCardId() + "/pins");
				ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
						.post(ClientResponse.class, userData);
				String strResponse = resp1.getEntity(String.class);
				response = new JSONObject(strResponse);

				System.err.println("pinset::::" + response);
			}

			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {
				resp.setCode("S00");
				resp.setMessage("Pin Reset successful");
				return resp;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public UserKycResponse setPINConfirm(MUser detail, String pin) {
		UserKycResponse resp = new UserKycResponse();
		/*
		 * Security.addProvider(new BouncyCastleProvider()); String host =
		 * MatchMoveUtil.MATCHMOVE_HOST_URL; String consumerKey =
		 * MatchMoveUtil.MATCHMOVE_CONSUMER_KEY; String consumerSecret =
		 * MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET; Connection wallet = new
		 * Connection(host, consumerKey, consumerSecret);
		 */
		JSONObject response = null;

		try {
			// wallet.authenticate(detail.getUserDetail().getEmail(),SecurityUtil.md5(detail.getUsername()));
			MMCards cards = mmCardRepository.getPhysicalCardByUser(detail);
			if (cards != null) {
				// Map<String, String> userData = new HashMap<String, String>();
				MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
				// encrypted pin block for 1234
				// String
				// encodedBlock=PINBlockUtil.format0Encode(pin,"000091"+detail.getUsername());
				String encodedBlock = TrippleDSEncryption.get3desPinBlock(pin, "91" + detail.getUsername());
				userData.add("pinblock", encodedBlock);
				userData.add("birthday", sdf.format(detail.getUserDetail().getDateOfBirth()));
				// response = wallet.consume("users/wallets/cards/" + cards.getCardId() +
				// "/pins", HttpRequest.METHOD_POST, userData);

				MatchMoveWallet mmw = cards.getWallet();
				String authorization = SMSConstant.getBasicAuthorization();
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client.resource(
						MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cards.getCardId() + "/pins");
				ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
						.post(ClientResponse.class, userData);
				String strResponse = resp1.getEntity(String.class);
				response = new JSONObject(strResponse);

				System.err.println("pinsetConfirm::::" + response);
			}

			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {
				resp.setCode("S00");
				resp.setMessage("Pin Reset Confirmation");
				return resp;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public UserKycResponse setPINConfirmReset(MUser detail, String pin, Date dob) {
		UserKycResponse resp = new UserKycResponse();
		/*
		 * Security.addProvider(new BouncyCastleProvider()); String host =
		 * MatchMoveUtil.MATCHMOVE_HOST_URL; String consumerKey =
		 * MatchMoveUtil.MATCHMOVE_CONSUMER_KEY; String consumerSecret =
		 * MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET; Connection wallet = new
		 * Connection(host, consumerKey, consumerSecret);
		 */
		JSONObject response = null;

		try {
			// wallet.authenticate(detail.getUserDetail().getEmail(),SecurityUtil.md5(detail.getUsername()));
			MMCards cards = mmCardRepository.getPhysicalCardByUser(detail);
			if (cards != null) {
				// Map<String, String> userData = new HashMap<String, String>();
				MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
				// encrypted pin block for 1234
				// String
				// encodedBlock=PINBlockUtil.format0Encode(pin,"000091"+detail.getUsername());
				String encodedBlock = TrippleDSEncryption.get3desPinBlock(pin, "91" + detail.getUsername());
				userData.add("pinblock", encodedBlock);
				userData.add("birthday", sdf.format(dob));
				// response = wallet.consume("users/wallets/cards/" + cards.getCardId() +
				// "/pins", HttpRequest.METHOD_POST, userData);

				MatchMoveWallet mmw = cards.getWallet();
				String authorization = SMSConstant.getBasicAuthorization();
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client.resource(
						MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cards.getCardId() + "/pins");
				ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
						.post(ClientResponse.class, userData);
				String strResponse = resp1.getEntity(String.class);
				response = new JSONObject(strResponse);

				System.err.println("pinsetConfirm::::" + response);
			}

			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {
				resp.setCode("S00");
				resp.setMessage("Pin Reset Confirmation");
				return resp;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public UserKycResponse debitFromWalletToPool(MatchMoveWallet wallet, String amount) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());

		try {
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("email", wallet.getUser().getUserDetail().getEmail());
			userData.add("amount", amount);
			userData.add("message", "Deduct funds");
			JSONObject details = new JSONObject();
			details.put("pp", "CORP_DEBIT");
			userData.add("details", details.toString());

			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).header("Content-Type", "application/x-www-form-urlencoded")
					.delete(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject response = new JSONObject(strResponse);

			System.err.println("debit from card to prefund:::" + response);
			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {
				String id = response.getString("id");
				deductFromCardConfirmation(id);
				resp.setMessage("Funds transferred to wallet");
				resp.setCode("S00");
				resp.setIndicator(id);
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public UserKycResponse checkStatusFromTransaction(String id) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());

		try {
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds/" + id);
			ClientResponse resp1 = resource.header("Authorization", authorization).get(ClientResponse.class);
			String strResponse = resp1.getEntity(String.class);
			JSONObject response = new JSONObject(strResponse);

			System.err.println("Status:::" + response);
			if (response != null && response.has("code")) {
				String code = response.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = response.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
					return resp;
				}
			} else {
				String status = response.getString("status");
				resp.setMessage("Status");
				resp.setCode("S00");
				resp.setIndicator(status);
				return resp;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public WalletResponse moveToCardFromWallet(MUser mUser, String amount, String cardId) {
		WalletResponse resp = new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());
		try {
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("amount", amount);
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));

			WebResource resource = client
					.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cardId + "/funds");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
					.post(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			// Map<String, String> userData = new HashMap<String, String>();

			JSONObject user = new JSONObject(strResponse);

			// user = wallet.consume("users/wallets/cards/"+cardId+"/funds",
			// HttpRequest.METHOD_POST,userData);
			System.err.println("Transfer funds to card::" + user);
			if (user != null && user.has("code")) {
				String code = user.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = user.getString("description");
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(description);
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					return resp;
				} else {
					resp.setStatus("Sucess");
					resp.setMessage("Tranfered sucessfully");
					resp.setCode("S00");
					return resp;

				}
			} else {
				if (user != null) {
					resp.setStatus("Sucess");
					resp.setMessage("Tranfered sucessfully");
					resp.setCode("S00");
					return resp;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		resp.setStatus(ResponseStatus.FAILURE.getKey());
		return resp;
	}

	/**
	 * Custom method for doing refunds to wallet
	 * 
	 * @param mUser
	 * @param amount
	 * @return
	 */

	@Override
	public WalletResponse refundToWalletFromCustomAdmin(MUser mUser, String amount, String serviceCode,
			String trxIdOfOriginalTrx, String ipAdrr) {
		WalletResponse resp = new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());
		try {
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("email", mUser.getUserDetail().getEmail());
			userData.add("amount", amount);
			JSONObject details = new JSONObject();
			details.put("pp", "REFUND");
			userData.add("details", details.toString());
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).post(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			System.err.println("wallet fund initiate load::" + user);
			if (user != null && user.has("code")) {
				String code = user.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = user.getString("description");
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(description);
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					return resp;
				}
			} else {
				String authRetrievalId = user.getString("id");
				String walletId = user.getString("wallet_id");
				String status = user.getString("confirm");
				resp.setWalletId(walletId);
				resp.setAuthRetrivalNo(authRetrievalId);
				resp.setCode("S00");
				resp.setMessage("Load Initiated");
				resp.setStatus(status);
				MService service = mServiceRepository.findServiceByCode(serviceCode);
				String transactionRefNo = System.currentTimeMillis() + "C";
				MTransaction transaction = mTransactionRepository.findByTransactionRefNo(transactionRefNo);
				if (transaction == null) {
					transaction = new MTransaction();
					transaction.setAmount(Double.parseDouble(amount));
					transaction.setTransactionRefNo(transactionRefNo);
					transaction.setRetrivalReferenceNo(authRetrievalId);
					transaction.setTransactionType(TransactionType.REFUND);
					transaction.setDescription("Refund of Rs " + amount + " from Admin");
					transaction.setCardLoadStatus("Pending");
					transaction.setCurrentBalance(mUser.getAccountDetail().getBalance());
					if (service != null) {
						transaction.setService(service);
					}
					transaction.setAuthReferenceNo(trxIdOfOriginalTrx);
					transaction.setRemarks(ipAdrr);
					transaction.setAccount(mUser.getAccountDetail());
					mTransactionRepository.save(transaction);
				}
				return resp;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		resp.setStatus(ResponseStatus.FAILURE.getKey());
		return resp;
	}

	@Override
	public WalletResponse reversalToWalletForRechargeFailure(MUser mUser, String amount, String serviceCode,
			String trxIdOfOriginalTrx, String desc) {
		WalletResponse resp = new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());
		try {
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("email", mUser.getUserDetail().getEmail());
			userData.add("amount", amount);
			JSONObject details = new JSONObject();
			details.put("pp", "REVERSAL_BILLPAY");
			details.put("payment_ref", trxIdOfOriginalTrx);
			details.put("description", desc);
			userData.add("details", details.toString());
			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).post(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			System.err.println("wallet fund initiate load::" + user);
			if (user != null && user.has("code")) {
				String code = user.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = user.getString("description");
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(description);
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					return resp;
				}
			} else {
				String authRetrievalId = user.getString("id");
				String walletId = user.getString("wallet_id");
				String status = user.getString("confirm");
				resp.setWalletId(walletId);
				resp.setAuthRetrivalNo(authRetrievalId);
				resp.setCode("S00");
				resp.setMessage("Reversal");
				resp.setStatus(status);
				MService service = mServiceRepository.findServiceByCode(serviceCode);
				String transactionRefNo = System.currentTimeMillis() + "C";
				MTransaction transaction = mTransactionRepository.findByTransactionRefNo(transactionRefNo);
				if (transaction == null) {
					transaction = new MTransaction();
					transaction.setAmount(Double.parseDouble(amount));
					transaction.setStatus(Status.Success);
					transaction.setTransactionRefNo(transactionRefNo);
					transaction.setRetrivalReferenceNo(authRetrievalId);
					transaction.setTransactionType(TransactionType.REVERSAL);
					transaction.setDescription(
							"Reversal of Rs " + amount + " for bill_pay & recharges for " + trxIdOfOriginalTrx);
					transaction.setCardLoadStatus("Success");
					transaction.setCurrentBalance(mUser.getAccountDetail().getBalance());
					if (service != null) {
						transaction.setService(service);
					}
					transaction.setAuthReferenceNo(trxIdOfOriginalTrx);
					transaction.setAccount(mUser.getAccountDetail());
					mTransactionRepository.save(transaction);
				}
				return resp;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		resp.setStatus(ResponseStatus.FAILURE.getKey());
		return resp;
	}

	@Override
	public WalletResponse assignPhysicalCardFromGroup(String mobile, String proxy_number) {

		WalletResponse resp = new WalletResponse();

		try {

			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("assoc_number", "PY0000" + proxy_number);
			String authorization = SMSConstant.getBasicAuthorization();
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUserName(mobile);
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/"
					+ MatchMoveUtil.MATCHMOVE_CARDTYPE_PHYSICAL);
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", authorization).header("X-Auth-User-Id", mmw.getMmUserId())
					.post(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			if (user != null && user.has("code")) {
				String code = user.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = user.getString("description");
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(description);
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					return resp;
				}
			} else {

				JSONObject date = user.getJSONObject("date");
				String expiry = date.getString("expiry");
				String issue = date.getString("issued");
				String withholdingamt = user.getJSONObject("funds").getJSONObject("withholding").getString("amount");
				String availabeamt = user.getJSONObject("funds").getJSONObject("available").getString("amount");
				boolean cardstatus = user.getJSONObject("status").getBoolean("is_active");
				String walletId = user.getString("id");
				String walletnumber = user.getString("number");
				String activationCode = user.getString("activation_code");
				MatchMoveWallet wal = matchMoveWalletRepository.findByUserName(mobile);
				if (wal != null) {
					MMCards cards = mmCardRepository.getPhysicalCardByUserName(mobile);
					if (cards == null) {
						cards = new MMCards();
						cards.setCardId(walletId);
						cards.setHasPhysicalCard(true);
						cards.setWallet(wal);
						cards.setActivationCode(activationCode);
						ResponseDTO activationResponse = activateMMPhysicalCard(walletId, activationCode);
						if (activationResponse.getCode().equalsIgnoreCase("S00")) {
							cards.setStatus("Active");
							resp.setCode("S00");
							resp.setMessage("Card Activated");

						} else {
							cards.setStatus("Inactive");
							resp.setCode("A00");
							resp.setMessage("Activation Failed.");
						}
						mmCardRepository.save(cards);
						return resp;
					} else {
						resp.setCode("F00");
						resp.setMessage("Physical card already exist for this user");
						return resp;
					}
				} else {
					resp.setCode("F00");
					resp.setMessage("Wallet doesn't exist for this user");
					return resp;
				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		resp.setStatus(ResponseStatus.FAILURE.getKey());
		return resp;
	}

}
