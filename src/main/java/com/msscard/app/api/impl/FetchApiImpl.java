package com.msscard.app.api.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MultivaluedMap;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;

import com.msscard.app.api.IfetchApi;
import com.msscard.app.model.request.CategoryRequestDTO;
import com.msscard.app.model.request.PocketFundTransferDTO;
import com.msscard.entity.Category;
import com.msscard.entity.MUser;
import com.msscard.repositories.CategoryRepository;
import com.msscard.sms.constant.SMSConstant;
import com.msscard.util.MatchMoveUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class FetchApiImpl implements IfetchApi {

	private final CategoryRepository categoryRepository;

	public FetchApiImpl(CategoryRepository categoryRepository) {
		super();
		this.categoryRepository = categoryRepository;
	}

	@Override
	public ClientResponse createCategory(CategoryRequestDTO dto, MultipartFile file, String contextPath) {

		if (dto != null) {
			try {
				if (dto.getName().length() <= 10) {
					MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
					userData.add("name", dto.getName());
					userData.add("default_amount", dto.getDefault_amount());
					userData.add("factor", "UNIT");
					userData.add("mcc", dto.getMcc());
					userData.add("country", "IND");
					String authorization = SMSConstant.getBasicAuthorization();
					Client client = Client.create();
					client.addFilter(new LoggingFilter(System.out));
					WebResource resource = client
							.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/funds/categories");
					ClientResponse resp = resource.accept("application/json").header("Authorization", authorization)
							.post(ClientResponse.class, userData);
					String strResponse = resp.getEntity(String.class);
					JSONObject response = new JSONObject(strResponse);
					System.err.println("multipart file:: " + file.getOriginalFilename());
					String path = contextPath + "/resources/cate" + file.getOriginalFilename();
					System.err.println(contextPath);
					File newFile = new File(path);
					file.transferTo(newFile);
					System.err.println("jsonresponse:: " + strResponse);
					int status = resp.getStatus();
					if (status == 200) {
						Category category = new Category();
						category.setCategoryName(dto.getName());
						category.setDefaultBalance(dto.getDefault_amount());
						category.setImageUrl(path);
						categoryRepository.save(category);
					}
					return resp;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public List<CategoryRequestDTO> fetchCategories() {

		String authorization = SMSConstant.getBasicAuthorization();
		Client client = Client.create();
		client.addFilter(new LoggingFilter(System.out));
		WebResource resource = client
				.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/funds/categories");
		ClientResponse resp = resource.accept("application/json").header("Authorization", authorization)
				.get(ClientResponse.class);
		String strResponse = resp.getEntity(String.class);
		try {
			JSONObject response = new JSONObject(strResponse);
			if (response.has("categories")) {
				JSONArray categories = response.getJSONArray("categories");
				System.err.println(categories);
				List<CategoryRequestDTO> list = new ArrayList<CategoryRequestDTO>();
				for (int i = 0; i < categories.length(); i++) {
					CategoryRequestDTO cateTypes = new CategoryRequestDTO();
					JSONObject cat = categories.getJSONObject(i);
					cateTypes.setId(cat.getString("id"));
					cateTypes.setName(cat.getString("name"));
					cateTypes.setDefault_amount(cat.getString("default_amount"));
					cateTypes.setMcc(cat.getString("mcc"));
					list.add(cateTypes);
				}
				return list;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public ClientResponse updateCategory(CategoryRequestDTO dto) {

		if (dto != null) {
			try {
				Category category = categoryRepository.findByName(dto.getName());
					MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
					userData.add("default_amount", dto.getDefault_amount());
					String authorization = SMSConstant.getBasicAuthorization();
					Client client = Client.create();
					client.addFilter(new LoggingFilter(System.out));
					WebResource resource = client
							.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/funds/categories/"+dto.getName());
					ClientResponse resp = resource.accept("application/json").header("Authorization", authorization)
							.put(ClientResponse.class, userData);
					String strResponse = resp.getEntity(String.class);
					if (resp.getStatus() == 200) {
						category.setDefaultBalance(dto.getDefault_amount());
						categoryRepository.save(category);
						return resp;
					}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	@Override
	public ClientResponse deleteCategory(CategoryRequestDTO dto) {
			if(dto !=null) {
				Category category = categoryRepository.findByName(dto.getName());
				if(category !=null) {
					String authorization = SMSConstant.getBasicAuthorization();
					Client client = Client.create();
					client.addFilter(new LoggingFilter(System.out));
					WebResource resource = client
							.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/funds/categories/"+dto.getName());
					ClientResponse resp = resource.accept("application/json").header("Authorization", authorization)
							.delete(ClientResponse.class);
					if(resp.getStatus()==200) {
						categoryRepository.delete(category);
						return resp;
					}
				}else {
					return null;
				}
			}
		return null;
	}

	@Override
	public ClientResponse transferFundsToPockets(PocketFundTransferDTO dto, MUser user) {
		
		if(dto !=null) {
			try {
				MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
				userData.add("amount",Double.toString(dto.getAmount()));
				userData.add("from",dto.getFrom());
				userData.add("to",dto.getTo());
				String authorization = SMSConstant.getBasicAuthorization();
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client
						.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/funds/categories/transfers");
				ClientResponse resp = resource.accept("application/json").header("Authorization", authorization).header("X-Auth-User-Id", user.getmMUserId())
						.post(ClientResponse.class, userData);
					return resp;
					
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}
