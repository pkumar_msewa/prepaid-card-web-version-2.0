package com.msscard.app.api.impl;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.msscard.app.api.ILoadMoneyApi;
import com.msscard.app.api.ITransactionApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.TransactionInitiateRequest;
import com.msscard.app.model.request.TransactionalRequest;
import com.msscard.app.model.response.TransactionRedirectResponse;
import com.msscard.app.model.response.TransactionVerificationResponse;
import com.msscard.app.model.response.UPIResponseDTO;
import com.msscard.entity.MCommission;
import com.msscard.entity.MService;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.repositories.MCommissionRepository;
import com.msscard.repositories.MServiceRepository;
import com.msscard.repositories.MTransactionRepository;
import com.razorpay.Payment;
import com.razorpay.RazorpayClient;
import com.razorpay.constants.RazorPayConstants;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class LoadMoneyApiImpl implements ILoadMoneyApi {
	
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final ITransactionApi transactionApi;
	private final IUserApi userApi;
	private final MServiceRepository serviceRepository;
	private final MCommissionRepository mCommissionRepository;
	private final MTransactionRepository mTransactionRepository;
	
	
	
	public LoadMoneyApiImpl(ITransactionApi transactionApi, IUserApi userApi,MServiceRepository serviceRepository,MCommissionRepository mCommissionRepository,MTransactionRepository mTransactionRepository) {
		super();
		this.transactionApi = transactionApi;
		this.userApi = userApi;
		this.serviceRepository=serviceRepository;
		this.mCommissionRepository=mCommissionRepository;
		this.mTransactionRepository=mTransactionRepository;
		
	}
	
	@Override
	public MTransaction initiateTransaction(String username,double amount,String servic){
		try{
		MUser user=userApi.findByUserName(username);
		MService service=serviceRepository.findServiceByCode(servic);
		MCommission commission=mCommissionRepository.findCommissionByService(service);
		TransactionalRequest transactionRequest=new TransactionalRequest();
		transactionRequest.setAmount(amount);
		transactionRequest.setDescription(service.getDescription());
		transactionRequest.setService(service);
		transactionRequest.setUser(user);
		if(commission!=null){
		transactionRequest.setCommission(commission);
		}
		return transactionApi.initiateUPITransaction(transactionRequest);
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public MTransaction initiateUPITransaction(String username, double amount, String servic,boolean andriod) {
		try{
			MUser user=userApi.findByUserName(username);
			MService service=serviceRepository.findServiceByCode(servic);
			MCommission commission=mCommissionRepository.findCommissionByService(service);
			TransactionalRequest transactionRequest=new TransactionalRequest();
			transactionRequest.setAmount(amount);
			transactionRequest.setDescription(service.getDescription());
			transactionRequest.setService(service);
			transactionRequest.setAndroid(andriod);
			transactionRequest.setUser(user);
			if(commission!=null){
				transactionRequest.setCommission(commission);
			}
			return transactionApi.initiatePhyCardTransaction(transactionRequest);
			}catch(Exception e){
				e.printStackTrace();
				return null;
			}
	}
	
	@Override
	public TransactionRedirectResponse redirectResponse(TransactionInitiateRequest initiateRequest){
		 TransactionRedirectResponse redirectResponse=new TransactionRedirectResponse();
		try{
		if(initiateRequest.getStatus().equalsIgnoreCase("Captured")){
			TransactionVerificationResponse verificationResponse=verifyRazorPay(initiateRequest);
			if(verificationResponse.isCaptured()){
			transactionApi.successLoadMoney(initiateRequest.getTransactionRefNo(), initiateRequest.getPaymentId());
			redirectResponse.setSuccess(true);
			return redirectResponse;
			}else{
				transactionApi.failedLoadMoney(initiateRequest.getTransactionRefNo());
				redirectResponse.setSuccess(false);
				return redirectResponse;
			}
		}else{
			transactionApi.failedLoadMoney(initiateRequest.getTransactionRefNo());
			redirectResponse.setSuccess(false);
			return redirectResponse;

		}
		}catch(Exception e){
			e.printStackTrace();
			transactionApi.failedLoadMoney(initiateRequest.getTransactionRefNo());
			redirectResponse.setSuccess(false);
			return redirectResponse;

		}
	}
	
	
	@Override
	public TransactionVerificationResponse verifyRazorPay(TransactionInitiateRequest dto) {
		TransactionVerificationResponse resp = new TransactionVerificationResponse();
		try {
			double amt = Double.valueOf(dto.getAmount());
			logger.info("amount:: "+amt);
			RazorpayClient razorpayClient = new RazorpayClient(RazorPayConstants.KEY_ID,RazorPayConstants.KEY_SECRET);
			org.json.JSONObject options = new org.json.JSONObject();
			options.put("amount", amt);
			Payment strResponse = razorpayClient.Payments.capture(dto.getPaymentId(), options);
			JSONObject jObj = new JSONObject(strResponse.toString());
			final String message = jObj.getString("description");
			final String status = jObj.getString("status");
			if (status.equalsIgnoreCase("captured")) {
				resp.setId(jObj.getString("id"));
				resp.setCaptured(jObj.getBoolean("captured"));
				resp.setResponse("00");
			} else {
				resp.setMessage(message);
				resp.setResponse("11");
				resp.setId(jObj.getString("id"));
				resp.setCaptured(jObj.getBoolean("captured"));
			}

		} catch (Exception e) {
			e.printStackTrace();
			resp.setCaptured(false);
			resp.setResponse("11");
		}
		return resp;
	}
	
	@Override
	public UPIResponseDTO checkUPIStatus(String transactionId,String merchantId){
		UPIResponseDTO upiResponse=new UPIResponseDTO();
		org.codehaus.jettison.json.JSONObject payload=new org.codehaus.jettison.json.JSONObject();
		try {
			payload.put("merchantId", RazorPayConstants.UPI_MERCHANT_ID);
			payload.put("merchantRefNo", transactionId);
			Client client=Client.create();
			WebResource webResource=client.resource(RazorPayConstants.UPI_STATUS);
			ClientResponse clientResponse=webResource.accept("application/json").type("application/json").post(ClientResponse.class,payload);
			String strResponse=clientResponse.getEntity(String.class);
			System.err.println(strResponse);
			if(strResponse!=null){
				org.codehaus.jettison.json.JSONObject jObject=new org.codehaus.jettison.json.JSONObject(strResponse);
				if(jObject!=null&&clientResponse.getStatus()==200){
					String status=jObject.getString("status");
					String code=jObject.getString("code");
					String amount=jObject.getString("amount");
					String merchantRefNo=jObject.getString("txnRefId");
					String paymentId=jObject.getString("txnId");
					upiResponse.setAmount(amount);
					upiResponse.setCode(code);
					upiResponse.setMerchantRefNo(merchantRefNo);
					upiResponse.setPaymentId(paymentId);
					upiResponse.setStatus(status);
					System.err.println("code:: "+code);
					System.err.println("status:: in check upi "+status);
					System.err.println("amount:: "+amount);
					System.err.println("merchantrefno:: "+merchantRefNo);
					System.err.println("payment id:: "+paymentId);
					return upiResponse;
				}else{
					upiResponse.setCode("F00");
					upiResponse.setStatus("Failure");
					return upiResponse;
				}
			}
		} catch (org.codehaus.jettison.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			upiResponse.setCode("F00");
			upiResponse.setStatus("Failed");
			return upiResponse;
		}
		upiResponse.setCode("F00");
		upiResponse.setStatus("Failed");
		return upiResponse;
		
	}
	
	/*//FOR CRON 
	@SuppressWarnings("null")
	public void autoRefund(){
		Calendar cal = Calendar.getInstance();
		Date date1 = cal.getTime();
		String pattern = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		String date = simpleDateFormat.format(date1);
		
		MTransaction upiTransactions= mTransactionRepository.getAllUpiInitiatedTransactions(date);
		UPIResponseDTO responseDTO=loadMoneyApi.checkUPIStatus(upiTransactions.getTransactionRefNo(), RazorPayConstants.UPI_MERCHANT_ID);
		if(responseDTO.getStatus().equalsIgnoreCase("Success")){
			
		}
		else{
			
		}
		
	}*/
}
