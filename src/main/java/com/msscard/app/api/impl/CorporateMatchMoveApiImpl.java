package com.msscard.app.api.impl;

import com.msscard.app.api.CorporateMatchMoveApi;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.MediaType;

import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.ISMSSenderApi;
import com.msscard.app.model.request.MatchMoveCreateCardRequest;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.request.UserKycRequest;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.app.sms.util.SMSAccount;
import com.msscard.app.sms.util.SMSTemplete;
import com.msscard.connection.javasdk.Connection;
import com.msscard.connection.javasdk.HttpRequest;
import com.msscard.connection.javasdk.HttpRequest.HttpRequestException;
import com.msscard.connection.javasdk.ResourceException;
import com.msscard.connection.javasdk.UnsupportedMethodException;
import com.msscard.entity.MMCards;
import com.msscard.entity.MUser;
import com.msscard.entity.MUserDetails;
import com.msscard.entity.MatchMoveWallet;
import com.msscard.entity.PhysicalCardDetails;
import com.msscard.entity.SendMoneyDetails;
import com.msscard.model.ResponseStatus;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MUserDetailRepository;
import com.msscard.repositories.MUserRespository;
import com.msscard.repositories.MatchMoveWalletRepository;
import com.msscard.repositories.PhysicalCardDetailRepository;
import com.msscard.repositories.SendMoneyDetailsRepository;
import com.msscard.sms.constant.SMSConstant;
import com.msscard.util.JSONParserUtil;
import com.msscard.util.MatchMoveUtil;
import com.msscard.util.SecurityUtil;
import com.msscard.validation.CommonValidation;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;


public class CorporateMatchMoveApiImpl implements CorporateMatchMoveApi {

	
		private final MatchMoveWalletRepository matchMoveWalletRepository;
		private final MMCardRepository mmCardRepository;
		private final PhysicalCardDetailRepository physicalCardDetailRepository;
		private final ISMSSenderApi senderApi;
		private final MUserRespository userRepository;
		private final SendMoneyDetailsRepository sendMoneyDetailsRepository;
		private final SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		
		
		public CorporateMatchMoveApiImpl(MatchMoveWalletRepository matchMoveWalletRepository,
				MMCardRepository mmCardRepository, PhysicalCardDetailRepository physicalCardDetailRepository,
				ISMSSenderApi senderApi, MUserRespository userRepository,
				SendMoneyDetailsRepository sendMoneyDetailsRepository) {
			super();
			this.matchMoveWalletRepository = matchMoveWalletRepository;
			this.mmCardRepository = mmCardRepository;
			this.physicalCardDetailRepository = physicalCardDetailRepository;
			this.senderApi = senderApi;
			this.userRepository = userRepository;
			this.sendMoneyDetailsRepository = sendMoneyDetailsRepository;
		}

		@Override
		public ResponseDTO createUserOnMM(MUser request) {
			ResponseDTO resp = new ResponseDTO();
			Security.addProvider(new BouncyCastleProvider());
			/*String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);*/
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();	
			userData.add("email", request.getUserDetail().getEmail());
			try {
				userData.add("password", SecurityUtil.md5(request.getUserDetail().getContactNo()));
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			userData.add("first_name", request.getUserDetail().getFirstName());
			userData.add("last_name", request.getUserDetail().getLastName());
			userData.add("mobile_country_code", "91");
			userData.add("mobile", request.getUserDetail().getContactNo());
			userData.add("preferred_name", request.getUserDetail().getFirstName()+" "+request.getUserDetail().getLastName());
			JSONObject user = null;
			try {
				String authorization = SMSConstant.getBasicAuthorization();
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL+"/users");
				ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE).header("Authorization", authorization).post(ClientResponse.class,userData);
				String strResponse = resp1.getEntity(String.class);
				user = new JSONObject(strResponse);				
				System.err.println("create user response:::" + user);
				if (user != null && user.has("code")) {
					String code = user.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = user.getString("description");
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage(description);
						resp.setStatus(ResponseStatus.FAILURE.getKey());
						return resp;
					}
				} else {
					//tempSetIdDetails(request.getUserDetail().getEmail(), request.getUsername());
					MatchMoveCreateCardRequest cardRequest=new MatchMoveCreateCardRequest();
					cardRequest.setEmail(request.getUserDetail().getEmail());
					cardRequest.setPassword(SecurityUtil.md5(request.getUserDetail().getContactNo()));
					cardRequest.setIdNo(user.getString("id"));
					WalletResponse walletResponse = createWallet(cardRequest);
					System.err.println("wallet response is::"+walletResponse);
					if (walletResponse.getCode().equalsIgnoreCase("S00")) {
						MatchMoveWallet moveWallet=new MatchMoveWallet();
						moveWallet.setWalletId(walletResponse.getWalletId());
						moveWallet.setWalletNumber(walletResponse.getWalletNumber());
						moveWallet.setUser(request);
						//moveWallet.setMmUserId(walletResponse.getMmUserId());
						moveWallet.setMmUserId(user.getString("id"));
						matchMoveWalletRepository.save(moveWallet);
						resp.setCode("S00");
						resp.setMessage("wallet created successfully");
						return resp;
										} else {
						resp.setCode("F00");
						resp.setMessage("Creation of wallet failed....");
						return resp;
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
				return resp;
			}
			resp.setMessage("Unable to create user, please try again later.");
			resp.setStatus(ResponseStatus.FAILURE.getKey());
			resp.setStatus(ResponseStatus.FAILURE);
			return resp;
		}

		@Override
		public WalletResponse createWallet(MatchMoveCreateCardRequest request) {
			WalletResponse resp = new WalletResponse();
			Security.addProvider(new BouncyCastleProvider());
			/*String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);*/

			JSONObject user = null;
			try {
				String authorization = SMSConstant.getBasicAuthorization();
				
				
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL+"/users/wallets");
				ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE).header("Authorization", authorization).header("X-Auth-User-Id", request.getIdNo()).post(ClientResponse.class);
				String strResponse = resp1.getEntity(String.class);
				user = new JSONObject(strResponse);
				System.err.println("create wallet::" + user);
				if (user != null && user.has("code")) {
					String code = user.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = user.getString("description");
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage(description);
						resp.setStatus(ResponseStatus.FAILURE.getKey());
						return resp;
					}
				} else {
					JSONObject date = user.getJSONObject("date");
					String expiry = date.getString("expiry");
					String issue = date.getString("issued");
					String withholdingamt = user.getJSONObject("funds").getJSONObject("withholding").getString("amount");
					String availabeamt = user.getJSONObject("funds").getJSONObject("available").getString("amount");
					boolean cardstatus = user.getJSONObject("status").getBoolean("is_active");
					String walletId = user.getString("id");
					String walletnumber = user.getString("number");
					resp.setAvailableAmt(availabeamt);
					resp.setCard_status(cardstatus);
					resp.setExpiryDate(expiry);
					resp.setIssueDate(issue);
					resp.setWithholdingAmt(withholdingamt);
					resp.setWalletNumber(walletnumber);
					resp.setWalletId(walletId);
					resp.setCode(ResponseStatus.SUCCESS.getValue());
										
					return resp;
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getValue());
				return resp;
			}
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Ohh!! snap, please try again later.");
			resp.setStatus(ResponseStatus.FAILURE.getKey());
			return resp;
		}

		@Override
		public WalletResponse assignVirtualCard(MUser mUser) {
			WalletResponse resp = new WalletResponse();
			Security.addProvider(new BouncyCastleProvider());
			/*String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);
*/			JSONObject user = null;
			try {
				MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);		
				String userId=null;
				if(mmw!=null && mmw.getMmUserId()!=null){
					userId=mmw.getMmUserId();

				}else{
					UserKycResponse userResp=getUsers(mUser.getUserDetail().getEmail(),mUser.getUsername());
					userId=userResp.getMmUserId();
				}
				String authorization = SMSConstant.getBasicAuthorization();
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL+"/users/wallets/cards/"+MatchMoveUtil.MATCHMOVE_CARDTYPE_VIRUAL);
				ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE).header("Authorization", authorization).header("X-Auth-User-Id", userId).post(ClientResponse.class);
				String strResponse = resp1.getEntity(String.class);
				user = new JSONObject(strResponse);
				System.err.println("wallet card issuance::" + user);
				if (user != null && user.has("code")) {
					String code = user.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = user.getString("description");
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage(description);
						resp.setStatus(ResponseStatus.FAILURE.getKey());
						return resp;
					}
				} else {

					JSONObject date = user.getJSONObject("date");
					String expiry = date.getString("expiry");
					String issue = date.getString("issued");
					String withholdingamt = user.getJSONObject("funds").getJSONObject("withholding").getString("amount");
					String availabeamt = user.getJSONObject("funds").getJSONObject("available").getString("amount");
					boolean cardstatus = user.getJSONObject("status").getBoolean("is_active");
					String walletId = user.getString("id");
					String walletnumber = user.getString("number");
					//JSONObject cvvRequest = wallet.consume("users/wallets/cards/" + walletId + "/securities/tokens");
					
					String authorization1 = SMSConstant.getBasicAuthorization();
					Client client1 = Client.create();
					client1.addFilter(new LoggingFilter(System.out));
					WebResource resource1 = client1.resource(MatchMoveUtil.MATCHMOVE_HOST_URL+"/users/wallets/cards/"+ walletId + "/securities/tokens");
					ClientResponse resp2 = resource1.accept("application/json").header("Authorization", authorization1).header("X-Auth-User-Id", mmw.getMmUserId()).get(ClientResponse.class);
					String strResponse1 = resp2.getEntity(String.class);
					JSONObject cvvRequest = new JSONObject(strResponse1);
					
										
					String cvv = null; 
					System.err.println("cvv request::::"+cvvRequest);
					if (cvvRequest.has("value")) {
						cvv = cvvRequest.getString("value");
					} else {
						cvv = "xxx";
					}
					MatchMoveWallet wal=matchMoveWalletRepository.findByUser(mUser);
					if(wal!=null){
					wal.setIssueDate(issue);
					MMCards cards=mmCardRepository.getVirtualCardsByCardUser(mUser);
					if(cards==null){
						MMCards virtualCard=new MMCards();
						virtualCard.setCardId(walletId);
						virtualCard.setHasPhysicalCard(false);
						virtualCard.setStatus("Active");
						virtualCard.setWallet(wal);
						MMCards cd=mmCardRepository.save(virtualCard);
						System.err.println("card saved"+cd.getId());
						
					}else{
						resp.setCode("F00");
						resp.setMessage("You are already assigned with a card.Please Contact Admin for queries");
						return resp;
					}
					matchMoveWalletRepository.save(wal);
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					resp.setMessage("card successfully assigned...");
					return resp;

				}
				else{
					resp.setCode("F00");
					resp.setMessage("Due to some internal issue card cannot be issued.Please contact customer care");
					return resp;
				}
				}
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
				return resp;
			}
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Ohh!! snap, please try again later.");
			resp.setStatus(ResponseStatus.FAILURE.getKey());
			return resp;
		}
		
		@Override
		public WalletResponse inquireCard(MatchMoveCreateCardRequest request) {
			WalletResponse resp = new WalletResponse();
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);
			JSONObject user = null;
			try {
				wallet.authenticate(request.getEmail(), request.getPassword());
				user = wallet.consume("users/wallets/cards/" + request.getCardId());
				System.err.println("wallet iquirry::" + user);
				if (user != null && user.has("code")) {
					String code = user.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = user.getString("description");
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage(description);
						resp.setStatus(ResponseStatus.FAILURE.getKey());
						return resp;
					}
				} else {
	 
					JSONObject date = user.getJSONObject("date");
					String expiry = date.getString("expiry");
					String issue = date.getString("issued");
					String holderName = user.getJSONObject("holder").getString("name");
					if(user.has("funds")){
					String withholdingamt = user.getJSONObject("funds").getJSONObject("withholding").getString("amount");
					String availabeamt = user.getJSONObject("funds").getJSONObject("available").getString("amount");
					}
					boolean cardstatus = user.getJSONObject("status").getBoolean("is_active");
					String walletId = user.getString("id");
					String walletnumber = user.getString("number");
					System.err.println("walletId:::" + walletId);
					JSONObject cvvRequest = wallet
							.consume("users/wallets/cards/" + walletId + "/securities/tokens");
					System.err.println("cvvRequest:::::::" + cvvRequest);
					String cvv = null;
					if (cvvRequest.has("value")) {
						cvv = cvvRequest.getString("value");
					} else {
						cvv = "xxx";
					}
					resp.setWalletNumber(walletnumber);
					// resp.setMmUserId(cardUser.getMmUserId());
					resp.setExpiryDate(expiry);
					resp.setIssueDate(issue);
					resp.setCardId(walletId);
					resp.setCvv(cvv);
					resp.setHolderName(holderName);
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					resp.setMessage("card successfully assigned...");
					return resp;

				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				e.printStackTrace();
				resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
				return resp;
			}
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Ohh!! snap, please try again later.");
			resp.setStatus(ResponseStatus.FAILURE.getKey());
			return resp;
		}

		
		/**
		 * FETCH USER FROM MATCHMOVE
		 * */
		@Override
		public ResponseDTO fetchMMUser(MatchMoveCreateCardRequest request) {
			ResponseDTO resp = new ResponseDTO();
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);
			
			JSONObject user = null;
			try {
				
				try{
				wallet.authenticate(request.getEmail(), request.getPassword());
				user = wallet.consume("users");
				}catch(JSONException e){
					resp.setCode(ResponseStatus.OAUTH_FAILURE.getValue());
					resp.setMessage(ResponseStatus.OAUTH_FAILURE.getKey());
					resp.setStatus(ResponseStatus.OAUTH_FAILURE.getKey());
					return resp;
				}
				System.err.println("fetch mm user response:::" + user);
				if (user != null && user.has("code")) {
					String code = user.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = user.getString("description");
						resp.setCode(ResponseStatus.OAUTH_FAILURE.getValue());
						resp.setMessage(ResponseStatus.OAUTH_FAILURE.getKey());
						resp.setStatus(ResponseStatus.OAUTH_FAILURE.getKey());
						return resp;
					}
				} else {
						resp.setCode("S00");
						resp.setMessage("User fetched successfully");
						return resp;
										}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
				return resp;

			}
			resp.setMessage("Unable to fetch User");
			resp.setStatus(ResponseStatus.FAILURE.getKey());
			resp.setStatus(ResponseStatus.FAILURE);
			return resp;
		}

		/**
		 * FETCH WALLET ON MM
		 * */
		@Override
		public WalletResponse fetchWalletMM(MatchMoveCreateCardRequest request) {
			WalletResponse resp = new WalletResponse();
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);
			JSONObject user = null;
			try {
				wallet.authenticate(request.getEmail(), request.getPassword());
				user = wallet.consume("users/wallets");
				System.err.println("fetch wallet on MM::" + user);
				if (user != null && user.has("code")) {
					String code = user.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = user.getString("description");
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage(description);
						resp.setStatus(ResponseStatus.FAILURE.getKey());
						return resp;
					}
				} else {
					JSONObject date = user.getJSONObject("date");
					String expiry = date.getString("expiry");
					String issue = date.getString("issued");
					String withholdingamt = user.getJSONObject("funds").getJSONObject("withholding").getString("amount");
					String availabeamt = user.getJSONObject("funds").getJSONObject("available").getString("amount");
					boolean cardstatus = user.getJSONObject("status").getBoolean("is_active");
					String walletId = user.getString("id");
					String walletnumber = user.getString("number");
					/*int cardCount=  user.getJSONArray("cards").length();
					resp.setCardCount(cardCount);
					if(cardCount<=2){
					resp.setCountLimit(true);
					}else{
						resp.setCountLimit(false);
					}*/
					resp.setAvailableAmt(availabeamt);
					resp.setCard_status(cardstatus);
					resp.setExpiryDate(expiry);
					resp.setIssueDate(issue);
					resp.setWithholdingAmt(withholdingamt);
					resp.setWalletNumber(walletnumber);
					resp.setWalletId(walletId);
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					return resp;

				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
				return resp;
			}
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Ohh!! snap, please try again later.");
			resp.setStatus(ResponseStatus.FAILURE.getKey());
			return resp;
		}

		/**
		 * DEACTIVATE CARD
		 * */
		
		@Override
		public ResponseDTO deActivateCards(MatchMoveCreateCardRequest request) {
			ResponseDTO resp = new ResponseDTO();
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);

			JSONObject user = null;
			try {
				MMCards card=mmCardRepository.getCardByCardId(request.getCardId());
				if(card!=null){
				String password=SecurityUtil.md5(card.getWallet().getUser().getUsername());
				String email=card.getWallet().getUser().getUserDetail().getEmail();
				wallet.authenticate(email, password);
				}
				if (request.getRequestType().equalsIgnoreCase("suspend")) {
					user = wallet.consume("users/wallets/cards/" + request.getCardId(), HttpRequest.METHOD_DELETE);
				} else {
					Map<String, String> data = new HashMap<>();
					data.put("type", request.getRequestType().toLowerCase());
					wallet.consume("users/wallets/cards/" + request.getCardId(), HttpRequest.METHOD_DELETE, data);
				}
				System.err.println("deactivate card:::"+user);
				if (user != null && user.has("code")) {
					String code = user.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = user.getString("description");
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage(description);
						return resp;
					}
				} else {
					card.setStatus("Inactive");
					mmCardRepository.save(card);
					resp.setDetails(user.toString());
					resp.setCode("S00");
					resp.setCardDetails(user.getString("id"));
					resp.setStatus(user.getString("status"));
					resp.setMessage("User Deactivated");
					return resp;
				}
			} catch (InvalidKeyException | NoSuchAlgorithmException | UnsupportedEncodingException | NoSuchProviderException
					| HttpRequestException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException
					| JSONException | ResourceException | UnsupportedMethodException | ShortBufferException
					| InvalidKeySpecException | InvalidAlgorithmParameterException | URISyntaxException e) {
				e.printStackTrace();
				resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
				return resp;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Ohh!! snap, please try again later.");
			resp.setStatus(ResponseStatus.FAILURE.getKey());
			return resp;
		}

		/**
		 * REACTIVATE CARD
		 * */
		
		@Override
		public ResponseDTO reActivateCard(MatchMoveCreateCardRequest request) {
			ResponseDTO result = new ResponseDTO();
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;

			Connection connection = new Connection(host, consumerKey, consumerSecret);
			try {
				MMCards card=mmCardRepository.getCardByCardId(request.getCardId());
				if(card!=null){
				String password=SecurityUtil.md5(card.getWallet().getUser().getUsername());
				String email=card.getWallet().getUser().getUserDetail().getEmail();
				connection.authenticate(email, password);
				}
				String cardType=null;
				Map<String, String> data = new HashMap<>();
				data.put("id", request.getCardId());
				MMCards cards=mmCardRepository.getCardByCardId(request.getCardId());
				if(cards.isHasPhysicalCard()){
					cardType=MatchMoveUtil.MATCHMOVE_CARDTYPE_PHYSICAL;
				}else{
					cardType=MatchMoveUtil.MATCHMOVE_CARDTYPE_VIRUAL;
				}
				JSONObject json = connection.consume("users/wallets/cards/"+cardType,
						HttpRequest.METHOD_POST, data);
				System.err.println("reactivate card response:::::"+json);
				if (json != null) {
					JSONObject status = json.getJSONObject("status");
					if (status != null) {
						result.setStatus(status.getString("text"));
					}
				result.setCode("S00");
				result.setMessage("User unblocked");
				return result;
				}else{
					result.setCode("F00");
					result.setMessage("Operation Failed..");
					return result;
				}
			} 	catch(Exception e){
				result.setCode("F00");
				result.setMessage("Operation Failed due to some exception...");
				return result;
			}
		}

		
		
			
		
		/**
		 * ADD PHYSICAL CARD
		 * */
		
		@Override
		public WalletResponse assignPhysicalCard(MUser mUser,String proxy_number) {
			WalletResponse resp = new WalletResponse();
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);
			JSONObject user = null;
			try {
				wallet.authenticate(mUser.getUserDetail().getEmail(), SecurityUtil.md5(mUser.getUserDetail().getContactNo()));
				Map<String,String> userData=new HashMap<String,String>();
				userData.put("assoc_number", "PY"+proxy_number);
				user = wallet.consume("users/wallets/cards/" + MatchMoveUtil.MATCHMOVE_CARDTYPE_PHYSICAL, HttpRequest.METHOD_POST,userData);
				System.err.println("assign physical card issuance::" + user);
				if (user != null && user.has("code")) {
					String code = user.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = user.getString("description");
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage(description);
						resp.setStatus(ResponseStatus.FAILURE.getKey());
						return resp;
					}
				} else {

					JSONObject date = user.getJSONObject("date");
					String expiry = date.getString("expiry");
					
					String issue = date.getString("issued");
					String withholdingamt = user.getJSONObject("funds").getJSONObject("withholding").getString("amount");
					String availabeamt = user.getJSONObject("funds").getJSONObject("available").getString("amount");
					boolean cardstatus = user.getJSONObject("status").getBoolean("is_active");
					String walletId = user.getString("id");
					String walletnumber = user.getString("number");
					String activationCode=user.getString("activation_code");

					JSONObject cvvRequest = wallet.consume("users/wallets/cards/" + walletId + "/securities/tokens");
					String cvv = null;
					System.err.println("cvv request::::"+cvvRequest);
					if (cvvRequest.has("value")) {
						cvv = cvvRequest.getString("value");
					} else {
						cvv = "xxx";
					}
					MatchMoveWallet wal=matchMoveWalletRepository.findByUser(mUser);
					if(wal!=null){
					MMCards cards=mmCardRepository.getPhysicalCardByUser(mUser);
					if(cards==null){
						MMCards virtualCard=new MMCards();
						virtualCard.setCardId(walletId);
						virtualCard.setHasPhysicalCard(true);
						virtualCard.setStatus("Inactive");
						virtualCard.setWallet(wal);
						cards=mmCardRepository.save(virtualCard);
						System.err.println("card saved"+cards.getId());
						
						
					}else{
						resp.setCode("F00");
						resp.setMessage("We are facing issues while generating card.Please contact admin for queries");
						return resp;
					}
					PhysicalCardDetails phyDetails=physicalCardDetailRepository.findByWallet(wal);
					if(phyDetails==null){
					 phyDetails=new PhysicalCardDetails();
					phyDetails.setWallet(wal);
					phyDetails.setProxyNumber(proxy_number);
					phyDetails.setActivationCode(activationCode);
					phyDetails.setCards(cards);
					phyDetails=physicalCardDetailRepository.save(phyDetails);
					if(phyDetails!=null){
					senderApi.sendUserSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplete.ACTIVATION_CODE, mUser,activationCode );
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					resp.setActivationCode(activationCode);
					resp.setCardId(walletId);
					resp.setMessage("card successfully assigned,Activation Code sent to your Contact Number.");
					return resp;
					}
					}
					
				}
				else{
					resp.setCode("F00");
					resp.setMessage("Due to some internal issue card cannot be issued.Please contact customer care");
					return resp;
				}
				}
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
				return resp;
			}
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Ohh!! snap, please try again later.");
			resp.setStatus(ResponseStatus.FAILURE.getKey());
			return resp;
		}

		/**
		 * Corporate physical card assignment
		 */
		
		
		@Override
		public WalletResponse assignPhysicalCardCorp(MUser mUser,String proxy_number) {
			WalletResponse resp = new WalletResponse();
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);
			JSONObject user = null;
			try {
				/*wallet.authenticate(mUser.getUserDetail().getEmail(), SecurityUtil.md5(mUser.getUserDetail().getContactNo()));
				Map<String,String> userData=new HashMap<String,String>();
				userData.put("assoc_number", assoc_number);
				user = wallet.consume("users/wallets/cards/" + MatchMoveUtil.MATCHMOVE_CARDTYPE_PHYSICAL, HttpRequest.METHOD_POST,userData);
				*/
				MultivaluedMap<String,String> multiMap=new MultivaluedMapImpl();
				multiMap.add("assoc_number","PY"+proxy_number);
				
				MatchMoveWallet mmw=matchMoveWalletRepository.findByUser(mUser);
				
				Client client=Client.create();
				WebResource webResource=client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL+"/users/wallets/cards/"+MatchMoveUtil.MATCHMOVE_CARDTYPE_PHYSICAL);
				ClientResponse clientResponse=webResource.header("Authorization",SMSConstant.getBasicAuthorization()).header("X-Auth-User-ID",mmw.getMmUserId()).header("Content-Type", "application/x-www-form-urlencoded")
				.post(ClientResponse.class,multiMap);
				String strResponse=clientResponse.getEntity(String.class);
				user=new JSONObject(strResponse);
				System.err.println("assign physical card issuance::" + user);
				if (user != null && user.has("code")) {
					String code = user.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = user.getString("description");
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage(description);
						resp.setStatus(ResponseStatus.FAILURE.getKey());
						return resp;
					}
				} else {

					JSONObject date = user.getJSONObject("date");
					String expiry = date.getString("expiry");
					
					String issue = date.getString("issued");
					String withholdingamt = user.getJSONObject("funds").getJSONObject("withholding").getString("amount");
					String availabeamt = user.getJSONObject("funds").getJSONObject("available").getString("amount");
					boolean cardstatus = user.getJSONObject("status").getBoolean("is_active");
					String walletId = user.getString("id");
					String walletnumber = user.getString("number");
					String activationCode=user.getString("activation_code");

					JSONObject cvvRequest = wallet.consume("users/wallets/cards/" + walletId + "/securities/tokens");
					String cvv = null;
					System.err.println("cvv request::::"+cvvRequest);
					if (cvvRequest.has("value")) {
						cvv = cvvRequest.getString("value");
					} else {
						cvv = "xxx";
					}
					MatchMoveWallet wal=matchMoveWalletRepository.findByUser(mUser);
					if(wal!=null){
					MMCards cards=mmCardRepository.getPhysicalCardByUser(mUser);
					if(cards==null){
						MMCards virtualCard=new MMCards();
						virtualCard.setCardId(walletId);
						virtualCard.setHasPhysicalCard(true);
						virtualCard.setStatus("Inactive");
						virtualCard.setWallet(wal);
						cards=mmCardRepository.save(virtualCard);
						System.err.println("card saved"+cards.getId());
						
						
					}else{
						resp.setCode("F00");
						resp.setMessage("We are facing issues while generating card.Please contact admin for queries");
						return resp;
					}
					PhysicalCardDetails phyDetails=physicalCardDetailRepository.findByWallet(wal);
					if(phyDetails==null){
					 phyDetails=new PhysicalCardDetails();
					phyDetails.setWallet(wal);
					phyDetails.setProxyNumber(proxy_number);
					phyDetails.setActivationCode(activationCode);
					phyDetails.setCards(cards);
					phyDetails=physicalCardDetailRepository.save(phyDetails);
					if(phyDetails!=null){
					//senderApi.sendUserSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplete.ACTIVATION_CODE, mUser,activationCode );
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					resp.setActivationCode(activationCode);
					resp.setCardId(walletId);
					resp.setMessage("card successfully assigned,Activation Code sent to your Contact Number.");
					return resp;
					}
					}
					
				}
				else{
					resp.setCode("F00");
					resp.setMessage("Due to some internal issue card cannot be issued.Please contact customer care");
					return resp;
				}
				}
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
				return resp;
			}
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Ohh!! snap, please try again later.");
			resp.setStatus(ResponseStatus.FAILURE.getKey());
			return resp;
		}

		
		
		@Override
		public ResponseDTO activationPhysicalCard(String activationCode,MUser user){
			
			ResponseDTO respDTO=new ResponseDTO();
			MMCards card=mmCardRepository.getPhysicalCardByUser(user);
			PhysicalCardDetails physicalCardDetails=physicalCardDetailRepository.findByCard(card);
			if(physicalCardDetails.getActivationCode().equalsIgnoreCase(activationCode)){
				ResponseDTO resp=activateMMPhysicalCard(card.getCardId(),activationCode);
				respDTO.setCode(resp.getCode());
				respDTO.setMessage(resp.getMessage());
				return respDTO;
			}else{
				respDTO.setCode("F00");
				respDTO.setMessage("Card Activation failed...Please contact Customer Care");
				return respDTO;
			}
		}
		
		@Override
		public ResponseDTO activateMMPhysicalCard(String cardId,String activationCode) {
			ResponseDTO result = new ResponseDTO();
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			JSONObject json=null;
			Connection connection = new Connection(host, consumerKey, consumerSecret);
			try {
				MMCards card=mmCardRepository.getCardByCardId(cardId);
				if(card!=null){
				/*String password=SecurityUtil.md5(card.getWallet().getUser().getUsername());
				String email=card.getWallet().getUser().getUserDetail().getEmail();
				connection.authenticate(email, password);
				
				Map<String, String> data = new HashMap<>();
				data.put("activation_code",activationCode);
				json=connection.consume("users/wallets/cards/" + cardId, HttpRequest.METHOD_PUT, data);
				System.err.println("activation response::"+json);*/
				
				MultivaluedMap<String,String> multiMap=new MultivaluedMapImpl();
				multiMap.add("activation_code", activationCode);
				
				Client client=Client.create();
				WebResource webResource=client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL+"/users/wallets/cards/"+cardId);
				ClientResponse clientResponse=webResource.header("Authorization",SMSConstant.getBasicAuthorization()).header("X-Auth-User-ID", card.getWallet().getMmUserId())
				.header("Content-Type","application/x-www-form-urlencoded").put(ClientResponse.class,multiMap);
				String strResponse=clientResponse.getEntity(String.class);
				json=new JSONObject(strResponse);
				if (json != null) {
					String status = json.getString("status");
					if (status != null) {
						if(status.equalsIgnoreCase("Success")){
						card.setStatus("Active");
						mmCardRepository.save(card);
						result.setStatus(status);
						result.setCode("S00");
						result.setMessage("Card Activated Successfully");
						return result;

					}else{
						result.setCode("F00");
						result.setMessage("Activation failed");
						return result;
					}
					}
							}else{
					result.setCode("F00");
					result.setMessage("Operation Failed..");
					return result;
				}
			} }	catch(Exception e){
				result.setCode("F00");
				result.setMessage("Operation Failed due to some exception...");
				return result;
			}
			result.setCode("F00");
			result.setMessage("Card Activation failed");
			return result;
			
		}

		/**
		 * LOAD FUNDS TO WALLET
		 * */
		
	

		
		/**
		 * CONFIRM FUNDS TO WALLET
		 * */

		/**
		 * TRANSFER FUNDS TO CARD
		 * */
		
		@Override
		public WalletResponse transferFundsToMMCard(MUser mUser,String amount,String cardId) {
			WalletResponse resp = new WalletResponse();
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);
			JSONObject user = null;
			try {
				wallet.authenticate(mUser.getUserDetail().getEmail(), SecurityUtil.md5(mUser.getUsername()));
				Map<String, String> userData = new HashMap<String, String>();
				userData.put("amount", amount);
				user = wallet.consume("users/wallets/cards/"+cardId+"/funds", HttpRequest.METHOD_POST,userData);
				System.err.println("Transfer funds to card::" + user);
				if (user != null && user.has("code")) {
					String code = user.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = user.getString("description");
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage(description);
						resp.setStatus(ResponseStatus.FAILURE.getKey());
						return resp;
					}else {
						resp.setStatus("Sucess");
						resp.setMessage("Tranfered sucessfully");
						resp.setCode("S00");
						return resp;
						
					}
				}else{
					if(user!=null){
						resp.setStatus("Sucess");
						resp.setMessage("Tranfered sucessfully");
						resp.setCode("S00");
						return resp;
					}
				} 
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
				return resp;
			}
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Ohh!! snap, please try again later.");
			resp.setStatus(ResponseStatus.FAILURE.getKey());
			return resp;
		}

		/**
		 * KYC PROCESS
		 * */
		@Override
		public UserKycResponse kycUserMM(UserKycRequest request) {
			UserKycResponse resp = new UserKycResponse();
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);
			JSONObject user = null;

			try {
				wallet.authenticate(request.getUser().getUserDetail().getEmail(),SecurityUtil.md5(request.getUser().getUsername()));

				Map<String, String> userData = new HashMap<String, String>();
				userData.put("address_1", request.getAddress1());
				userData.put("address_2", request.getAddress2());
				userData.put("city", request.getCity());
				userData.put("state", request.getState());
				userData.put("country",request.getCountry());
				userData.put("zipcode",request.getPinCode());
				JSONObject address = wallet.consume("users/addresses/residential", HttpRequest.METHOD_PUT, userData);
				System.err.println(address);
				if (address != null && address.has("code")) {
					String code = address.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = address.getString("description");
						resp.setCode("F00");
						resp.setMessage(description);
						return resp;
					}
				} else {
					
					resp.setMessage("residential address saved");
					resp.setCode("S00");
					return resp;
					
				}
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				return resp;
			}
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Ohh!! snap, please try again later.");
			return resp;
		}

		@Override
		public UserKycResponse setIdDetails(UserKycRequest request) {
			UserKycResponse resp = new UserKycResponse();
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);
			JSONObject user = null;

			try {
				wallet.authenticate(request.getUser().getUserDetail().getEmail(),SecurityUtil.md5(request.getUser().getUsername()));

				Map<String, String> userData = new HashMap<String, String>();

				userData.put("title", "Mr");
				userData.put("id_type", request.getId_type());
				userData.put("id_number",request.getId_number());
				userData.put("country_of_issue",request.getCountry());
				userData.put("birthday", String.valueOf(request.getUser().getUserDetail().getDateOfBirth()));
				userData.put("gender", "male");
				user = wallet.consume("users", HttpRequest.METHOD_PUT, userData);
				System.err.println("id::"+user);
					if (user != null && user.has("code")) {
					String code = user.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = user.getString("description");
						resp.setCode("F00");
						resp.setMessage(description);
						return resp;
					}
				} else {
					
					resp.setMessage("Id Details saved");
					resp.setCode("S00");
					return resp;
					
				}
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				return resp;
			}
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Ohh!! snap, please try again later.");
			return resp;
		}

		@Override
		public UserKycResponse setImagesForKyc(UserKycRequest request) {
			UserKycResponse resp = new UserKycResponse();
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);
			JSONObject user = null;

			try {
				wallet.authenticate(request.getUser().getUserDetail().getEmail(),SecurityUtil.md5(request.getUser().getUsername()));
				byte[] array =recoverImageFromUrl(request.getFilePath());

//				InputStream inputStream = new ByteArrayInputStream(array);
				String encodedfile = new String(Base64.encodeBase64(array), "UTF-8");
				Map<String, String> userData = new HashMap<String, String>();
				userData.put("data", encodedfile.trim());
				JSONObject response = wallet.consume("users/authentications/documents", HttpRequest.METHOD_POST, userData);
				System.err.println("the upload response::"+response);
					if (response != null && response.has("code")) {
					String code = response.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = response.getString("description");
						resp.setCode("F00");
						resp.setMessage(description);
						return resp;
					}
				} else {
					
					resp.setMessage("Images saved saved");
					resp.setCode("S00");
					return resp;
					
				}
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				return resp;
			}
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Ohh!! snap, please try again later.");
			return resp;
		}
		
		 public byte[] recoverImageFromUrl(String urlText) throws Exception {
		        URL url = new URL(urlText);
		        ByteArrayOutputStream output = new ByteArrayOutputStream();
		         
		        try (InputStream inputStream = url.openStream()) {
		            int n = 0;
		            byte [] buffer = new byte[ 1024 ];
		            while (-1 != (n = inputStream.read(buffer))) {
		                output.write(buffer, 0, n);
		            }
		        }
		     
		        return output.toByteArray();
		    }
		@Override
		public UserKycResponse confirmKyc(UserKycRequest request) {
			UserKycResponse resp = new UserKycResponse();
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);
			JSONObject user = null;

			try {
				wallet.authenticate(request.getUser().getUserDetail().getEmail(),SecurityUtil.md5(request.getUser().getUsername()));
				JSONObject response = wallet.consume("users/authentications/documents", HttpRequest.METHOD_PUT);
					if (response != null && response.has("code")) {
					String code = response.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = response.getString("description");
						resp.setCode("F00");
						resp.setMessage(description);
						return resp;
					}
				} else {
					
					resp.setMessage("Kyc confirmed");
					resp.setCode("S00");
					return resp;
					
				}
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				return resp;
			}
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Ohh!! snap, please try again later.");
			return resp;
		}

		/**
		 * BALANCE API
		 * */
		@Override
		public double getBalance(MUser request) {
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);
			JSONObject user = null;
			String cardId=null;
			double balance=0;
			try {
				
				MMCards cards=mmCardRepository.getPhysicalCardByUser(request);
				if(cards!=null){
					if(cards.getStatus().equalsIgnoreCase("Active")){
					cardId=cards.getCardId();
					}
				}
				MMCards phyCard=mmCardRepository.getVirtualCardsByCardUser(request);
				if(phyCard!=null){
					if(phyCard.getStatus().equalsIgnoreCase("Active")){
					cardId=phyCard.getCardId();
					}
				}
				wallet.authenticate(request.getUserDetail().getEmail(),SecurityUtil.md5(request.getUsername()));
				JSONObject response = wallet.consume("users/wallets/cards/"+cardId);
				System.err.println("balance API::"+response);
					if (response != null && response.has("code")) {
					String code = response.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = response.getString("description");
						return balance;
					}
				} else {
					
					JSONObject funds=response.getJSONObject("funds");
					if(funds!=null){
						balance=Double.parseDouble(funds.getJSONObject("available").getString("amount"));
					}
					
				}
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
				return balance;
			}
			
			return balance;
		}

		
		
		/**
		 * TEMPORARY KYC PROCESS
		 * */
		
		
		@Override
		public UserKycResponse tempKycUserMM(String email,String mobile) {
			UserKycResponse resp = new UserKycResponse();
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);
			JSONObject user = null;

			try {
				wallet.authenticate(email,SecurityUtil.md5(mobile));
				Map<String, String> userData = new HashMap<String, String>();
				userData.put("address_1", "1/1,DCN homes,Kormangala");
				userData.put("address_2", "6th block");
				userData.put("city", "Bangalore");
				userData.put("state", "Karnataka");
				userData.put("country","India");
				userData.put("zipcode","560095");
				JSONObject address = wallet.consume("users/addresses/residential", HttpRequest.METHOD_PUT, userData);
				System.err.println("residential address::"+address);
				if (address != null && address.has("code")) {
					String code = address.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = address.getString("description");
						resp.setCode("F00");
						resp.setMessage(description);
						return resp;
					}
				} else {
					
					resp.setMessage("residential address saved");
					resp.setCode("S00");
					return resp;
					
				}
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				return resp;
			}
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Ohh!! snap, please try again later.");
			return resp;
		}

		@Override
		public UserKycResponse tempSetIdDetails(String email,String mobile) {
			UserKycResponse resp = new UserKycResponse();
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);
			JSONObject user = null;

			try {
				wallet.authenticate(email,SecurityUtil.md5(mobile));

				Map<String, String> userData = new HashMap<String, String>();
				MUser username=userRepository.findByUsername(mobile);
				Date dob=username.getUserDetail().getDateOfBirth();
				
				String date=sdf.format(dob);
				
				System.err.println("date format:::::::::::::::::::::::::::::"+date);
				userData.put("title", "Mr");
				userData.put("id_type", "voters_id");
				userData.put("id_number","RPZ"+mobile);
				userData.put("country_of_issue","India");
				userData.put("birthday",date);
				userData.put("gender", "male");
				user = wallet.consume("users", HttpRequest.METHOD_PUT, userData);
				System.err.println("date format:::::::::::::::::::::::::::::"+date);

				System.err.println("Id type title:::"+user);
				System.err.println("date format:::::::::::::::::::::::::::::"+date);

					if (user != null && user.has("code")) {
					String code = user.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = user.getString("description");
						resp.setCode("F00");
						resp.setMessage(description);
						return resp;
					}
				} else {
					
					resp.setMessage("Id Details saved");
					resp.setCode("S00");
					return resp;
					
				}
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				return resp;
			}
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Ohh!! snap, please try again later.");
			return resp;
		}

		
		@Override
		public UserKycResponse edittempSetIdDetails(String email,String mobile) {
			UserKycResponse resp = new UserKycResponse();
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);
			JSONObject user = null;

			try {
				wallet.authenticate(email,SecurityUtil.md5(mobile));

				Map<String, String> userData = new HashMap<String, String>();
				MUser username=userRepository.findByUsername(mobile);
				Date dob=username.getUserDetail().getDateOfBirth();
				
				String date=sdf.format(dob);
				
				System.err.println("date format is:::::::::::::::::::::::::::::::::::::::::::"+date);
				userData.put("title", "Mr");
				userData.put("country_of_issue","India");
				userData.put("id_type", "voters_id");
				userData.put("id_number","RPZ"+mobile);
				userData.put("birthday",date);
				userData.put("gender", "male");
				user = wallet.consume("users", HttpRequest.METHOD_PUT, userData);
				System.err.println("Id type title:::"+user);
					if (user != null && user.has("code")) {
					String code = user.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = user.getString("description");
						resp.setCode("F00");
						resp.setMessage(description);
						return resp;
					}
				} else {
					
					resp.setMessage("Id Details saved");
					resp.setCode("S00");
					return resp;
					
				}
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				return resp;
			}
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Ohh!! snap, please try again later.");
			return resp;
		}

		
		@Override
		public UserKycResponse getUsers(String email,String mobile) {
			UserKycResponse resp = new UserKycResponse();
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);
			JSONObject user = null;

			try {
				wallet.authenticate(email,SecurityUtil.md5(mobile));

				user = wallet.consume("users");
				System.err.println("get users:::"+user);
					if (user != null && user.has("code")) {
					String code = user.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = user.getString("description");
						resp.setCode("F00");
						resp.setMessage(description);
						return resp;
					}
				} else {
					resp.setDetails(user.toString());
					resp.setMessage("Id Details saved");
					resp.setCode("S00");
					return resp;
					
				}
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				return resp;
			}
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Ohh!! snap, please try again later.");
			return resp;
		}

		
		@Override
		public UserKycResponse getConsumers() {
			UserKycResponse resp = new UserKycResponse();
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);
			JSONObject user = null;

			try {
				
				JSONObject response = wallet.consume("oauth/consumer");
				System.err.println("get response:::"+response);
					if (response != null && response.has("code")) {
					String code = response.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = response.getString("description");
						resp.setCode("F00");
						resp.setMessage(description);
						return resp;
					}
				} else {
					resp.setPrefundingBalance(response.getJSONObject("consumer").getString("fund_floating_balance"));
					resp.setDetails(response.toString());
					resp.setMessage("Id Details saved");
					resp.setCode("S00");
					return resp;
					
				}
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				return resp;
			}
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Ohh!! snap, please try again later.");
			return resp;
		}

		
		@Override
		public UserKycResponse tempSetImagesForKyc(String email,String mobile) {
			UserKycResponse resp = new UserKycResponse();
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);
			JSONObject user = null;

			try {
				wallet.authenticate(email,SecurityUtil.md5(mobile));
				/*File file=new File("card.png");
				String str = FileUtils.readFileToString(new File("cards.png"), "UTF-8");
				String encodedfile = new String(Base64.encodeBase64(str.getBytes()), "UTF-8");*/
				//String files="cards.png";

				String files="/usr/local/tomcat/webapps/ROOT/WEB-INF/startup/easemytrip.png";
				byte[] array = Files.readAllBytes(new File(files).toPath());
				String encodedfile = new String(Base64.encodeBase64(array), "UTF-8");
				Map<String, String> userData = new HashMap<String, String>();
				userData.put("data", encodedfile.trim());
				JSONObject response = wallet.consume("users/authentications/documents", HttpRequest.METHOD_POST, userData);
				System.err.println("image upload response:::"+response);
					if (response != null && response.has("code")) {
					String code = response.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = response.getString("description");
						resp.setCode("F00");
						resp.setMessage(description);
						return resp;
					}
				} else {
					
					resp.setMessage("Images saved saved");
					resp.setCode("S00");
					return resp;
					
				}
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				return resp;
			}
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Ohh!! snap, please try again later.");
			return resp;
		}
		
		
		@Override
		public UserKycResponse tempConfirmKyc(String email,String mobile) {
			UserKycResponse resp = new UserKycResponse();
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);
			JSONObject user = null;

			try {
				wallet.authenticate(email,SecurityUtil.md5(mobile));
				JSONObject response = wallet.consume("users/authentications/documents", HttpRequest.METHOD_PUT);
				System.err.println("confirm kyc response::::"+response);
					if (response != null && response.has("code")) {
					String code = response.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = response.getString("description");
						resp.setCode("F00");
						resp.setMessage(description);
						return resp;
					}
				} else {
					tempKycStatusApproval(email, SecurityUtil.md5(mobile));
					resp.setMessage("Kyc confirmed");
					resp.setCode("S00");
					return resp;
					
				}
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				return resp;
			}
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Ohh!! snap, please try again later.");
			return resp;
		}

		@Override
		public UserKycResponse tempKycStatusApproval(String email,String mobile) {
			UserKycResponse resp = new UserKycResponse();
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);
			JSONObject user = null;

			try {
				wallet.authenticate(email,SecurityUtil.md5(mobile));
				Map<String, String> userData = new HashMap<String, String>();
				userData.put("status", "approved");
				JSONObject response = wallet.consume("users/authentications/statuses", HttpRequest.METHOD_PUT, userData);
				System.err.println("temp kyc approval::::"+response);
					if (response != null && response.has("code")) {
					String code = response.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = response.getString("description");
						resp.setCode("F00");
						resp.setMessage(description);
						return resp;
					}
				} else {
					
					resp.setMessage("Kyc confirmed");
					resp.setCode("S00");
					return resp;
					
				}
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				return resp;
			}
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Ohh!! snap, please try again later.");
			return resp;
		}

		
		@Override
		public UserKycResponse debitFromMMWalletToCard(String email,String mobile,String cardId,String amount) {
			UserKycResponse resp = new UserKycResponse();
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);
			JSONObject user = null;

			try {
				wallet.authenticate(email,SecurityUtil.md5(mobile));
				Map<String, String> userData = new HashMap<String, String>();
				userData.put("amount",amount);
				userData.put("message", "return funds");
				JSONObject response = wallet.consume("users/wallets/cards/"+cardId+"/funds", HttpRequest.METHOD_DELETE,userData);
				System.err.println("Debit from card to wallet response::"+response);
					if (response != null && response.has("code")) {
					String code = response.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = response.getString("description");
						resp.setCode("F00");
						resp.setMessage(description);
						return resp;
					}
				} else {
					
					resp.setMessage("Funds transferred to wallet");
					resp.setCode("S00");
					return resp;
					
				}
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				return resp;
			}
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Ohh!! snap, please try again later.");
			return resp;
		}

		/**
		 * DEBIT TO PREFUND ACCOUNT
		 * */
		
		@Override
		public UserKycResponse debitFromCard(String email,String mobile,String cardId,String amount) {
			UserKycResponse resp = new UserKycResponse();
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);
			JSONObject user = null;

			try {
			//	wallet.authenticate(email,SecurityUtil.md5(mobile));
				Map<String, String> userData = new HashMap<String, String>();
				userData.put("amount",amount);
				userData.put("message", "return funds");
				JSONObject response = wallet.consume("users/wallets/cards/"+cardId+"/funds", HttpRequest.METHOD_DELETE,userData);
				System.err.println("debit from card to prefund:::"+response);
					if (response != null && response.has("code")) {
					String code = response.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = response.getString("description");
						resp.setCode("F00");
						resp.setMessage(description);
						return resp;
					}
				} else {
					String id=response.getString("id");
					deductFromCardConfirmation(id);
					resp.setMessage("Funds transferred to wallet");
					resp.setCode("S00");
					return resp;
					
				}
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				return resp;
			}
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Ohh!! snap, please try again later.");
			return resp;
		}

		public UserKycResponse deductFromCardConfirmation(String id) {
			UserKycResponse resp = new UserKycResponse();
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);
			JSONObject user = null;

			try {
			//	wallet.authenticate(email,SecurityUtil.md5(mobile));
				Map<String, String> userData = new HashMap<String, String>();
				userData.put("ids",id);

				JSONObject transaction = wallet.consume("oauth/consumer/funds", HttpRequest.METHOD_DELETE, userData);
				System.err.println("deduct confirmation:::"+transaction);
					if (transaction != null && transaction.has("code")) {
					String code = transaction.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = transaction.getString("description");
						resp.setCode("F00");
						resp.setMessage(description);
						return resp;
					}
				} else {
					
					resp.setMessage("Funds transferred to wallet");
					resp.setCode("S00");
					return resp;
					
				}
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				return resp;
			}
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Ohh!! snap, please try again later.");
			return resp;
		}
		
		@Override
		public UserKycResponse getTransactions(MUser userRequest) {
			UserKycResponse resp = new UserKycResponse();
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);
			JSONObject responseVir=null;
			JSONObject responsePhy=null;
			try {
				wallet.authenticate(userRequest.getUserDetail().getEmail(),SecurityUtil.md5(userRequest.getUsername()));
				MMCards virCard=mmCardRepository.getVirtualCardsByCardUser(userRequest);
				if(virCard!=null){
						String vircardId=virCard.getCardId();
						Map<String, String> data = new HashMap<>();
						data.put("limit", "60");
						responseVir = wallet.consume("users/wallets/cards/"+vircardId+"/transactions",HttpRequest.METHOD_GET,data);
	System.err.println("virtual response::::"+responseVir);
					}
				
				MMCards phyCard=mmCardRepository.getPhysicalCardByUser(userRequest);
				if(phyCard!=null){
						String phycardId=phyCard.getCardId();
						Map<String, String> data = new HashMap<>();
						data.put("limit", "60");
						responsePhy = wallet.consume("users/wallets/cards/"+phycardId+"/transactions",HttpRequest.METHOD_GET,data);
	System.err.println("physical response::"+responsePhy);
					}
				
				
					if (responsePhy != null && responsePhy.has("code")) {
					String code = responsePhy.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = responsePhy.getString("description");
						resp.setCode("F00");
						resp.setMessage(description);
					}
					
				} else{
					if(responsePhy!=null){
					resp.setPhysicalCardTransactions(responsePhy.toString());
					resp.setMessage("TransactionList fetched");
					resp.setCode("S00");
					
					}
				}
					
					if (responseVir != null && responseVir.has("code")) {
						String code = responseVir.getString("code");
						if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
							String description = responseVir.getString("description");
							resp.setCode("F00");
							resp.setMessage(description);
						}
						
						
					} 
					else{
						resp.setVirtualTransactions(responseVir.toString());
						resp.setMessage("TransactionList fetched");
						resp.setCode("S00");
						
						
					}
					
					
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				return resp;
			}
			
			return resp;
		}

		/**
		 * RESETTING PINS
		 * */
		
		@Override
		public UserKycResponse resetPins(MUser userRequest) {
			UserKycResponse resp = new UserKycResponse();
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);
			JSONObject user = null;
			String cardId=null;
			try {
				wallet.authenticate(userRequest.getUserDetail().getEmail(),SecurityUtil.md5(userRequest.getUsername()));
				MMCards phyCard=mmCardRepository.getPhysicalCardByUser(userRequest);
				if(phyCard!=null){
					if(phyCard.getStatus().equalsIgnoreCase("Active")){
						cardId=phyCard.getCardId();
					}
				}
				Map<String, String> userData = new HashMap<String, String>();
				userData.put("mode", "M");
				JSONObject response = wallet.consume("users/wallets/cards/" + cardId + "/pins/reset", HttpRequest.METHOD_GET, userData);
				System.err.println("pin reset response::::"+response);
					if (response != null && response.has("code")) {
					String code = response.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = response.getString("description");
						resp.setCode("F00");
						resp.setMessage(description);
						return resp;
					}
				} else {
					
					resp.setDetails(response.toString());
					resp.setMessage("Your 4-digit secure PIN has been sent to your registered Email Id");
					resp.setCode("S00");
					return resp;
					
				}
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				return resp;
			}
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Ohh!! snap, please try again later.");
			return resp;
		}

		@Override
		public MMCards findCardByCardId(String cardId) {
			// TODO Auto-generated method stub
			return mmCardRepository.getCardByCardId(cardId);
		}
		
		
		@Override
		public MMCards findCardByUserAndStatus(MUser user) {
			// TODO Auto-generated method stub
			return mmCardRepository.getCardByUserAndStatus(user,"Active");
		}
		
		
		
		/**
		 * FUND TRANSFER FROM CARD TO CARD
		 * */
		
		
		
		
		
		/**
		 * CHECK CREDIT PREFUND BALANCE
		 * */
		
		@Override
		public boolean checkCreditPrefundBalance(String amount){
			
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);
			try {
				JSONObject transaction = wallet.consume("oauth/consumer/" + MatchMoveUtil.MATCHMOVE_CONSUMER_KEY + "/prefund/credit", HttpRequest.METHOD_GET);
			System.err.println("prefund credit balance::"+transaction);
				if (transaction != null && transaction.has("code")) {
					String code = transaction.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = transaction.getString("description");
						return false;
					}
				} 
				if(transaction!=null){
					String balance=transaction.getString("balance");
					double actAmount=Double.parseDouble(amount);
					double actBalance=Double.parseDouble(balance);
					if(actBalance<actAmount){
						return false;
					}else{
						return true;
					}
				}
			} catch (InvalidKeyException | NoSuchAlgorithmException | UnsupportedEncodingException | NoSuchProviderException
					| HttpRequestException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException
					| JSONException | ResourceException | UnsupportedMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
		}
		
		
		@Override
		public WalletResponse assignPhysicalCardFromAdmin(MUser mUser, String proxy_number) {

			WalletResponse resp = new WalletResponse();
			Security.addProvider(new BouncyCastleProvider());
			String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
			String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
			String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			Connection wallet = new Connection(host, consumerKey, consumerSecret);
			JSONObject user = null;
			try {
				wallet.authenticate(mUser.getUserDetail().getEmail(), SecurityUtil.md5(mUser.getUsername()));
				Map<String,String> userData=new HashMap<String,String>();
				userData.put("assoc_number", "PY"+proxy_number);
				user = wallet.consume("users/wallets/cards/" + MatchMoveUtil.MATCHMOVE_CARDTYPE_PHYSICAL, HttpRequest.METHOD_POST,userData);
				System.err.println("assign physical card issuance::" + user);
				if (user != null && user.has("code")) {
					String code = user.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = user.getString("description");
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage(description);
						resp.setStatus(ResponseStatus.FAILURE.getKey());
						return resp;
					}
				} else {

					JSONObject date = user.getJSONObject("date");
					String expiry = date.getString("expiry");
					String issue = date.getString("issued");
					String withholdingamt = user.getJSONObject("funds").getJSONObject("withholding").getString("amount");
					String availabeamt = user.getJSONObject("funds").getJSONObject("available").getString("amount");
					boolean cardstatus = user.getJSONObject("status").getBoolean("is_active");
					String walletId = user.getString("id");
					String walletnumber = user.getString("number");
					String activationCode=user.getString("activation_code");

					JSONObject cvvRequest = wallet.consume("users/wallets/cards/" + walletId + "/securities/tokens");
					String cvv = null;
					System.err.println("cvv request::::"+cvvRequest);
					if (cvvRequest.has("value")) {
						cvv = cvvRequest.getString("value");
					} else {
						cvv = "xxx";
					}
					MatchMoveWallet wal=matchMoveWalletRepository.findByUser(mUser);
					if(wal!=null){
					MMCards cards=mmCardRepository.getPhysicalCardByUser(mUser);
					if(cards==null){
						MMCards virtualCard=new MMCards();
						virtualCard.setCardId(walletId);
						virtualCard.setHasPhysicalCard(true);
						virtualCard.setStatus("Inactive");
						virtualCard.setWallet(wal);
						cards=mmCardRepository.save(virtualCard);
						System.err.println("card saved"+cards.getId());
					}else{
						resp.setCode("F00");
						resp.setMessage("We are facing issues while generating card.Please contact admin for queries");
						return resp;
					}
					ResponseDTO response=activationPhysicalCard(activationCode, mUser);
					resp.setCode(response.getCode());
					resp.setMessage("Physical card activated.");
				}
				else{
					resp.setCode("F00");
					resp.setMessage("Due to some internal issue card cannot be issued.Please contact customer care");
					return resp;
				}
				}
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
				return resp;
			}
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Ohh!! snap, please try again later.");
			resp.setStatus(ResponseStatus.FAILURE.getKey());
			return resp;
		}
		
		
		public static void main(String[] args) {
			Date d1=new Date();
			System.err.println(new SimpleDateFormat("yyyy-MM-dd").format(d1));
		}
		

	

	@Override
	public WalletResponse getFundTransferDetails(MUser user,String type){
		WalletResponse resp=new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		try {
			wallet.authenticate(user.getUserDetail().getEmail(), SecurityUtil.md5(user.getUsername()));
			Map<String, String> userData = new HashMap<String, String>();
			userData.put("type",type);
			JSONObject transaction = wallet.consume("users/wallets/funds", HttpRequest.METHOD_GET, userData);
			System.out.println(transaction);
			if (transaction != null && transaction.has("code")) {
				String code = transaction.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = transaction.getString("description");
					resp.setMessage(description);
					resp.setCode("F00");
					return resp;
				}
			} 
			if(transaction!=null){
				resp.setCode("S00");
				resp.setDetails(transaction.toString());
				return resp;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public WalletResponse getFundTransferDetails(String id){
		WalletResponse resp=new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		try {
			JSONObject transaction = wallet.consume("users/wallets/funds/" + id, HttpRequest.METHOD_GET);
			System.out.println(transaction);
			if (transaction != null && transaction.has("code")) {
				String code = transaction.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = transaction.getString("description");
					resp.setMessage(description);
					resp.setCode("F00");
					return resp;
				}
			} 
			if(transaction!=null){
				resp.setCode("S00");
				resp.setDetails(transaction.toString());
				return resp;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public UserKycResponse getTransactions(MUser userRequest,String page) {
		UserKycResponse resp = new UserKycResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		JSONObject responseVir=null;
		JSONObject responsePhy=null;
		try {
			wallet.authenticate(userRequest.getUserDetail().getEmail(),SecurityUtil.md5(userRequest.getUsername()));
			MMCards virCard=mmCardRepository.getVirtualCardsByCardUser(userRequest);
			if(virCard!=null){
					String vircardId=virCard.getCardId();
					Map<String, String> data = new HashMap<>();
					data.put("limit", "10");
					responseVir = wallet.consume("users/wallets/cards/"+vircardId+"/transactions/"+page,HttpRequest.METHOD_GET,data);
	System.err.println("virtual response::::"+responseVir);
				}
			
			MMCards phyCard=mmCardRepository.getPhysicalCardByUser(userRequest);
			if(phyCard!=null){
					String phycardId=phyCard.getCardId();
					Map<String, String> data = new HashMap<>();
					data.put("limit", "10");
					responsePhy = wallet.consume("users/wallets/cards/"+phycardId+"/transactions/"+page,HttpRequest.METHOD_GET,data);
	System.err.println("physical response::"+responsePhy);
				}
			
			
				if (responsePhy != null && responsePhy.has("code")) {
				String code = responsePhy.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = responsePhy.getString("description");
					resp.setCode("F00");
					resp.setMessage(description);
				}
				
			} else{
				if(responsePhy!=null){
				resp.setPhysicalCardTransactions(responsePhy.toString());
				resp.setMessage("TransactionList fetched");
				resp.setCode("S00");
				
				}
			}
				
				if (responseVir != null && responseVir.has("code")) {
					String code = responseVir.getString("code");
					if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
						String description = responseVir.getString("description");
						resp.setCode("F00");
						resp.setMessage(description);
					}
					
					
				} 
				else{
					resp.setVirtualTransactions(responseVir.toString());
					resp.setMessage("TransactionList fetched");
					resp.setCode("S00");
					
					
				}
				
				
			} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		
		return resp;
	}

	@Override
	public WalletResponse cancelFundTransfer(MUser recipient,String amount,String id){
		WalletResponse resp=new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());
		String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
		String consumerKey = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
		String consumerSecret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
		Connection wallet = new Connection(host, consumerKey, consumerSecret);
		try {
			Map<String, String> userData = new HashMap<String, String>();
			userData.put("email",recipient.getUserDetail().getEmail());
			userData.put("amount", amount);
			JSONObject details=new JSONObject();
			details.put("pp","PPI");
			userData.put("details", details.toString());
			JSONObject transaction = wallet.consume("users/wallets/funds/" + id, HttpRequest.METHOD_DELETE,userData);
			System.out.println("cancel fund Transfer:::::::::"+transaction);
			if (transaction != null && transaction.has("code")) {
				String code = transaction.getString("code");
				if (!CommonValidation.isNull(code) && !code.equalsIgnoreCase("200")) {
					String description = transaction.getString("description");
					resp.setMessage(description);
					resp.setCode("F00");
					return resp;
				}
			} 
			if(transaction!=null){
				resp.setCode("S00");
				resp.setDetails(transaction.toString());
				return resp;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resp;
	}


	}

