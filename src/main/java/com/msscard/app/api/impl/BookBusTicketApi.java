/*package com.msscard.app.api.impl;

import com.msscard.app.api.IBookBusTicketApi;
import com.msscard.app.model.request.BookBusTicketRequest;
import com.msscard.entity.MService;
import com.msscard.entity.MUser;
import com.msscard.entity.TravelBusDetail;
import com.msscard.entity.TravelBusTransaction;
import com.msscard.entity.TravelSeatDetail;
import com.msscard.entity.TravelUserDetail;
import com.msscard.model.Status;
import com.msscard.repositories.BusDetailRepository;
import com.msscard.repositories.BusTransactionRepository;
import com.msscard.repositories.MTransactionRepository;
import com.msscard.repositories.SeatDetailRepository;
import com.msscard.repositories.TravelUserDetailRepository;
import com.msscard.validation.CommonValidation;

public class BookBusTicketApi implements IBookBusTicketApi{
	
	private final BusDetailRepository busDetailRepository;
	private final SeatDetailRepository seatDetailRepository;
	private final TravelUserDetailRepository travelUserDetailRepository;
	private final BusTransactionRepository busTransactionRepository;
	
	
	public BookBusTicketApi(BusDetailRepository busDetailRepository,SeatDetailRepository seatDetailRepository,TravelUserDetailRepository travelUserDetailRepository,BusTransactionRepository busTransactionRepository) {
		   this.busDetailRepository=busDetailRepository;
		   this.seatDetailRepository=seatDetailRepository;
		   this.travelUserDetailRepository=travelUserDetailRepository;
		   this.busTransactionRepository=busTransactionRepository;
		  
		   
		
	}
	

	@Override
	public void saveBusTicket(BookBusTicketRequest req, MUser user, MService service) {
		TravelSeatDetail seatDetail = new TravelSeatDetail();
		seatDetail.setFare(req.getFare());
		seatDetail.setNoOfSeats(req.getNoOfSeats());
		seatDetail.setSeatNo(req.getSeatNo());
		seatDetail.setCancelPolicy(req.getCancelPolicy());
		seatDetailRepository.save(seatDetail);

		TravelUserDetail userDetail = new TravelUserDetail();
		userDetail.setAddress(req.getAddress());
		userDetail.setAge(req.getAge());
		userDetail.setEmailId(req.getEmailId());
		userDetail.setGender(req.getGender());
		userDetail.setMobileNo(req.getMobileNo());
		userDetail.setName(req.getName());
		userDetail.setPostalCode(req.getPostalCode());
		travelUserDetailRepository.save(userDetail);

		TravelBusDetail busDetail = new TravelBusDetail();
		busDetail.setActualFare(req.getFare());
		busDetail.setBlockRefNo(req.getBlockingRefNo());
		busDetail.setBookingDate(req.getBookingDate());
		busDetail.setBookingRefNo(req.getBookingRefNo());
		busDetail.setBusTypeName(req.getBusTypeName());
		busDetail.setDepartureTime(req.getDepartureTime());
		busDetail.setDestinationId(req.getDestinationId());
		busDetail.setDestinationName(req.getDestinationName());
		busDetail.setJourneyDate(req.getJourneyDate());
		busDetail.setProvider(req.getProvider());
		busDetail.setReturnDate(req.getReturnDate());
		busDetail.setSourceId(req.getSourceId());
		busDetail.setSourceName(req.getSourceName());
		busDetail.setTravelOperator(req.getTravelOperator());
		busDetail.setTripId(req.getTripId());
		busDetail.setSeatDetail(seatDetail);
		busDetail.setUserDetail(userDetail);
		busDetail.setServiceCharge(req.getServiceCharge());
		busDetail.setServiceTax(req.getServiceTax());
		busDetailRepository.save(busDetail);

		double amount = Double.parseDouble(req.getFare());
		String transactionRefNo = req.getBookingRefNo();

		transactionApi.initiateBusBooking(amount,
				"Bus Ticket Booked Rs " + req.getFare() + " from " + req.getSourceName() + " to "
						+ req.getDestinationName() + " to RefNo. " + req.getBookingRefNo(),
				service, transactionRefNo, user.getUsername(), StartupUtil.TRAVEL, "");

		MTransaction transaction = transactionApi.getTransactionByRefNo(transactionRefNo + "D");
		MTransaction PqTransaction = transactionApi.getTransactionByRefNo(transactionRefNo + "D");
		if (transaction != null) {
			String busTransactionRefNo = req.getTransactionRefNo();
			TravelBusTransaction busTransaction = new TravelBusTransaction();
			busTransaction.setAmount(req.getFare());
			busTransaction.setBookingRefNo(req.getBookingRefNo());
			busTransaction.setBusTransactionRefNo(busTransactionRefNo);
			busTransaction.setStatus(Status.Initiated);
			busTransaction.setTransaction(transaction);
			busTransaction.setDescription(transaction.getDescription());
			busTransaction.setBlockRefNo(req.getBlockingRefNo());
			busTransaction.setBookingDate(req.getBookingDate());
			busTransaction.setBusDetail(busDetail);
			busTransactionRepository.save(busTransaction);
		}
	}

	
	

	@Override
	public boolean checkBalance(MUser user, BookBusTicketRequest req) {
			boolean valid = false;
			double amount = Double.parseDouble(req.getFare());
			if (CommonValidation.balanceCheck(user.getAccountDetail().getBalance(), amount)) {
				valid = true;
			}
			return valid;
		}
	}
	


*/