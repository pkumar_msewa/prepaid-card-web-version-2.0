package com.msscard.app.api.impl;

import java.net.Inet4Address;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.msscard.app.api.IFlightApi;
import com.msscard.app.api.ITravelFlightApi;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.response.CommonResponse;
import com.msscard.entity.ERCredentialsManager;
import com.msscard.entity.FlightAirLineList;
import com.msscard.entity.FlightTicket;
import com.msscard.entity.FlightTravellers;
import com.msscard.entity.MCommission;
import com.msscard.entity.MOperator;
import com.msscard.entity.MService;
import com.msscard.entity.MServiceType;
import com.msscard.entity.MUser;
import com.msscard.entity.TravellerDetails;
import com.msscard.model.AirBookRQ;
import com.msscard.model.AirLineNames;
import com.msscard.model.AirPriceCheckRequest;
import com.msscard.model.Bonds;
import com.msscard.model.BookFare;
import com.msscard.model.BookingClass;
import com.msscard.model.Cabin;
import com.msscard.model.CabinClasses;
import com.msscard.model.CompareCountry;
import com.msscard.model.CreateJsonRequestFlight;
import com.msscard.model.DroppingPointsDTO;
import com.msscard.model.EngineID;
import com.msscard.model.Errors;
import com.msscard.model.Fare;
import com.msscard.model.FlghtTicketResp;
import com.msscard.model.FlightAvailability;
import com.msscard.model.FlightBookReq;
import com.msscard.model.FlightCancelableResp;
import com.msscard.model.FlightCityList;
import com.msscard.model.FlightPayment;
import com.msscard.model.FlightPaymentRequest;
import com.msscard.model.FlightPaymentResponse;
import com.msscard.model.FlightPriceCheckRequest;
import com.msscard.model.FlightPriceReCheckResponse;
import com.msscard.model.FlightResponseDTO;
import com.msscard.model.FlightSearchDetails;
import com.msscard.model.FlightSearchResponse;
import com.msscard.model.FlightTicketDeatilsDTO;
import com.msscard.model.FlightTicketMobileDTO;
import com.msscard.model.FlightTicketResp;
import com.msscard.model.FlightTravellerDetailsDTO;
import com.msscard.model.FligthcancelableReq;
import com.msscard.model.GetFlightDetailsForAdmin;
import com.msscard.model.JourneyDetail;
import com.msscard.model.Journeys;
import com.msscard.model.Legs;
import com.msscard.model.MobileFlightBookRequest;
import com.msscard.model.PagingDTO;
import com.msscard.model.PaxFares;
import com.msscard.model.ResponseStatus;
import com.msscard.model.SSRDetails;
import com.msscard.model.Segment;
import com.msscard.model.Status;
import com.msscard.model.TicketObj;
import com.msscard.model.TicketsResp;
import com.msscard.model.TravelFlightRequest;
import com.msscard.model.TravellerDetailst;
import com.msscard.model.TravellerFlightDetails;
import com.msscard.model.error.TransactionError;
import com.msscard.repositories.ERCredentialManager;
import com.msscard.repositories.FlightListRepository;
import com.msscard.repositories.MCommissionRepository;
import com.msscard.repositories.MOperatorRepository;
import com.msscard.repositories.MServiceRepository;
import com.msscard.repositories.MServiceTypeRepository;
import com.msscard.repositories.MUserRespository;
import com.msscard.util.ConvertUtil;
import com.msscard.util.MatchMoveUtil;
import com.msscard.util.SecurityUtil;
import com.msscard.util.ServiceCodeUtil;
import com.msscard.validation.LoadMoneyValidation;
import com.msscard.validation.TransactionTypeValidation;
import com.razorpay.constants.MdexConstants;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class TravelFlightApi implements ITravelFlightApi{

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());	
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	
	private final ERCredentialManager eRCredentialManager;
	private final IFlightApi flightApi;
	private final MServiceRepository mServiceRepository;
	private final LoadMoneyValidation loadMoneyValidation;
	private final MOperatorRepository mOperatorRepository;
	private final MServiceTypeRepository mServiceTypeRepository;
	private final MCommissionRepository mCommissionRepository;
	private final MUserRespository mUserRespository;
	private final FlightListRepository flightListRepository;
	
	public TravelFlightApi(ERCredentialManager eRCredentialManager, IFlightApi flightApi, MServiceRepository mServiceRepository,
			LoadMoneyValidation loadMoneyValidation, MOperatorRepository mOperatorRepository, MServiceTypeRepository mServiceTypeRepository,
			MCommissionRepository mCommissionRepository, MUserRespository mUserRespository, FlightListRepository flightListRepository) {
		this.eRCredentialManager = eRCredentialManager;
		this.flightApi = flightApi;
		this.mServiceRepository = mServiceRepository;
		this.loadMoneyValidation = loadMoneyValidation;
		this.mOperatorRepository = mOperatorRepository;
		this.mServiceTypeRepository = mServiceTypeRepository;
		this.mCommissionRepository = mCommissionRepository;
		this.mUserRespository = mUserRespository;
		this.flightListRepository = flightListRepository;
	}
	
	@Override
	public CommonResponse setFlightService() {
		CommonResponse result = new CommonResponse();
		try {
			MOperator operator = mOperatorRepository.findOperatorByName("Flight");
			if(operator == null) {
				operator = new MOperator();
				operator.setName("Flight");
				operator.setStatus(Status.Active);
				mOperatorRepository.save(operator);
			}
			
			MServiceType sertype =  mServiceTypeRepository.findServiceTypeByName("Flight");
			if(sertype == null) {
				sertype = new MServiceType();
				sertype.setName("Flight");
				sertype.setDescription("Flight");
				mServiceTypeRepository.save(sertype);
			}
			
			MOperator op = mOperatorRepository.findOperatorByName("Flight");
			MServiceType sert =  mServiceTypeRepository.findServiceTypeByName("Flight");
			
			MService service = mServiceRepository.findServiceByCode("EMTF");
			if(service == null) {
				service = new MService();
				service.setCode("EMTF");
				service.setDescription("Flight service");
				service.setMaxAmount(1000);
				service.setMinAmount(1);
				service.setName("Flight");
				service.setOperator(op);
				service.setOperatorCode("EMTF");
				service.setServiceType(sert);
				service.setServiceName("Flight");
				service.setStatus(Status.Active);
				mServiceRepository.save(service);
			}
			
			MService sercomm = mServiceRepository.findServiceByCode("EMTF");
			MCommission comm = mCommissionRepository.findCommissionByService(sercomm);
			if(comm == null) {
				comm = new MCommission();
				comm.setMinAmount(0);
				comm.setMaxAmount(100000);
				comm.setValue(2);
				comm.setFixed(false);
				comm.setService(sercomm);
				mCommissionRepository.save(comm);
			}
			
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		result.setCode(ResponseStatus.SUCCESS.getValue());
		return result;
	}
	
	@Override
	public MService getServiceStatus() {
		MService ser = mServiceRepository.findServiceByCode("EMTF");
		return ser;
	}
	
	@Override
	public String getAirlineNames() {
		try {
			String domain = null;
			String basicAuth=null;
			String key=null;
			String token=null;
			if(MatchMoveUtil.PRODUCTION){
			ERCredentialsManager credentialManager=eRCredentialManager.getCredentials("LIVE");
			if(credentialManager!=null){
				domain=credentialManager.getMdex_base_url();
				basicAuth=credentialManager.getBasic_auth_header();
				key=credentialManager.getMdex_keys();
				token=credentialManager.getMdex_token();
			}
			} else {
				ERCredentialsManager credentialManager=eRCredentialManager.getCredentials("TEST");
				if(credentialManager!=null){
				domain=credentialManager.getMdex_base_url();
				basicAuth=credentialManager.getBasic_auth_header();
				key=credentialManager.getMdex_keys();
				token=credentialManager.getMdex_token();
				}
			}
						
			JSONObject payload = new JSONObject();
			payload.put("clientIp", Inet4Address.getLocalHost().getHostAddress());
			payload.put("cllientKey", key);
			payload.put("clientToken", token);
			payload.put("clientApiName", "Flight Name Request");
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(domain+MdexConstants.MDEX_TRAVEL_URL+MdexConstants.Flight_SOURCE);
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtil.getHashBus(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			logger.info(strResponse);
			return strResponse;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public String getFlightSearchAPI(TravelFlightRequest req) {
		try {
			String domain = null;
			String basicAuth=null;
			String key=null;
			String token=null;
			if(MatchMoveUtil.PRODUCTION){
			ERCredentialsManager credentialManager=eRCredentialManager.getCredentials("LIVE");
			if(credentialManager!=null){
				domain=credentialManager.getMdex_base_url();
				basicAuth=credentialManager.getBasic_auth_header();
				key=credentialManager.getMdex_keys();
				token=credentialManager.getMdex_token();
			}
			} else {
				ERCredentialsManager credentialManager=eRCredentialManager.getCredentials("TEST");
				if(credentialManager!=null){
				domain=credentialManager.getMdex_base_url();
				basicAuth=credentialManager.getBasic_auth_header();
				key=credentialManager.getMdex_keys();
				token=credentialManager.getMdex_token();
				}
			}
			
			JSONObject payload = flightApi.getFlightSerachJson(req);
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(domain+MdexConstants.MDEX_TRAVEL_URL+MdexConstants.Flight_Search_API);
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtil.getHashBus(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			logger.info(strResponse);
			return strResponse;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public String AirRePriceRQ(FlightPriceCheckRequest req) {
		try {
			String domain = null;
			String basicAuth=null;
			String key=null;
			String token=null;
			if(MatchMoveUtil.PRODUCTION){
			ERCredentialsManager credentialManager=eRCredentialManager.getCredentials("LIVE");
			if(credentialManager!=null){
				domain=credentialManager.getMdex_base_url();
				basicAuth=credentialManager.getBasic_auth_header();
				key=credentialManager.getMdex_keys();
				token=credentialManager.getMdex_token();
			}
			} else {
				ERCredentialsManager credentialManager=eRCredentialManager.getCredentials("TEST");
				if(credentialManager!=null){
				domain=credentialManager.getMdex_base_url();
				basicAuth=credentialManager.getBasic_auth_header();
				key=credentialManager.getMdex_keys();
				token=credentialManager.getMdex_token();
				}
			}
			
			JSONObject payload = new JSONObject();
			payload.put("clientIp", Inet4Address.getLocalHost().getHostAddress());
			payload.put("cllientKey", key);
			payload.put("clientToken", token);
			payload.put("clientApiName", "Flight Price Re-Check");
			payload.put("adults", req.getAdults());
			payload.put("cabin", Cabin.Economy);
			payload.put("childs", req.getChilds());
			// ArrayList<String> objhasmap = new ArrayList<>();
			// objhasmap.add("" + EngineID.Indigo);
			// objhasmap.add("" + EngineID.AirAsia);
			// objhasmap.add("" + EngineID.AirCosta);
			// objhasmap.add("" + EngineID.GoAir);
			// objhasmap.add("" + EngineID.Spicjet);
			// objhasmap.add("" + EngineID.Trujet);
			payload.put("engineID", req.getEngineID());
			payload.put("traceId", req.getTraceId());
			payload.put("origin", req.getOrigin());
			payload.put("destination", req.getDestination());
			payload.put("beginDate", req.getBeginDate());
			payload.put("infants", req.getInfants());
			payload.put("tripType", req.getTripType());
			payload.put("bondType", req.getBondType());
			payload.put("boundType", req.getBoundType());
			payload.put("isBaggageFare", false);
			payload.put("isSSR", false);
			payload.put("itineraryKey", req.getItineraryKey());
			payload.put("journeyTime", req.getJourneyTime());
			payload.put("airlineName", req.getAirlineName());
			payload.put("amount", Double.parseDouble(req.getAmount()));
			payload.put("arrivalDate", req.getArrivalDate());
			payload.put("arrivalTerminal", req.getArrivalTerminal());
			payload.put("arrivalTime", req.getArrivalTime());
			payload.put("baggageUnit", req.getBaggageUnit());
			payload.put("baggageWeight", req.getBaggageWeight());
			payload.put("capacity", req.getCapacity());
			payload.put("carrierCode", req.getCarrierCode());
			payload.put("currencyCode", req.getCurrencyCode());
			payload.put("departureDate", req.getDepartureDate());
			payload.put("departureTerminal", req.getDepartureTerminal());
			payload.put("departureTime", req.getDepartureTime());
			payload.put("duration", req.getDuration());
			payload.put("fareClassOfService", req.getFareClassOfService());
			payload.put("flightName", req.getFlightName());
			payload.put("flightNumber", req.getFlightNumber());
			payload.put("isConnecting", false);
			payload.put("sold", req.getSold());
			payload.put("status", req.getStatus());
			payload.put("basicFare", req.getBasicFare());
			payload.put("exchangeRate", req.getExchangeRate());
			payload.put("baseTransactionAmount", req.getBaseTransactionAmount());
			payload.put("cancelPenalty", req.getCancelPenalty());
			payload.put("changePenalty", req.getChangePenalty());
			payload.put("chargeCode", req.getChargeCode());
			payload.put("chargeType", req.getChargeType());
			payload.put("markUP", req.getMarkUP());
			payload.put("paxType", 0);
			payload.put("refundable", true);
			payload.put("totalFare", req.getTotalFare());
			payload.put("totalTax", req.getTotalTax());
			payload.put("transactionAmount", req.getTransactionAmount());
			payload.put("totalFareWithOutMarkUp", req.getTotalFareWithOutMarkUp());
			payload.put("totalTaxWithOutMarkUp", req.getTotalTaxWithOutMarkUp());
			payload.put("fareRule", req.getFareRule());
			payload.put("isCache", false);
			payload.put("isHoldBooking", false);
			payload.put("isInternational", false);
			payload.put("isRoundTrip", false);
			payload.put("isSpecial", true);
			payload.put("isSpecialId", false);
			payload.put("journeyIndex", req.getJourneyIndex());
			payload.put("nearByAirport", false);
			payload.put("searchId", req.getSearchId());
			System.out.println(payload.toString());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(domain+MdexConstants.MDEX_TRAVEL_URL+MdexConstants.Flight_AirRePriceRQ_API);
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtil.getHashBus(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			logger.info(strResponse);			
			return "" + strResponse;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	@Override
	public String mobileAirRePriceRQConnecting(FlightPriceCheckRequest req) {
		try {
			String domain = null;
			String basicAuth=null;
			String key=null;
			String token=null;
			if(MatchMoveUtil.PRODUCTION){
			ERCredentialsManager credentialManager=eRCredentialManager.getCredentials("LIVE");
			if(credentialManager!=null){
				domain=credentialManager.getMdex_base_url();
				basicAuth=credentialManager.getBasic_auth_header();
				key=credentialManager.getMdex_keys();
				token=credentialManager.getMdex_token();
			}
			} else {
				ERCredentialsManager credentialManager=eRCredentialManager.getCredentials("TEST");
				if(credentialManager!=null){
				domain=credentialManager.getMdex_base_url();
				basicAuth=credentialManager.getBasic_auth_header();
				key=credentialManager.getMdex_keys();
				token=credentialManager.getMdex_token();
				}
			}

			JSONObject payload = new JSONObject();

			FlightAvailability flightAvailability = new FlightAvailability();

			FlightSearchDetails flightSearchDetails = new FlightSearchDetails();

			flightSearchDetails.setBeginDate(req.getBeginDate());
			flightSearchDetails.setDestination(req.getDestination());
			flightSearchDetails.setEndDate(req.getEndDate());
			flightSearchDetails.setOrigin(req.getOrigin());

			ArrayList<String> engineIds = new ArrayList<>();
			engineIds.add(req.getEngineID());

			flightAvailability.setEngineIDs(engineIds);
			flightAvailability.setCabin(req.getCabin());
			flightAvailability.setTripType(req.getTripType());
			flightAvailability.setAdults(req.getAdults());
			flightAvailability.setChilds(req.getChilds());
			flightAvailability.setInfants(req.getInfants());
			flightAvailability.setTraceId(req.getTraceId());
			ArrayList<FlightSearchDetails> flightSearchArray = new ArrayList<>();
			flightSearchArray.add(flightSearchDetails);
			flightAvailability.setFlightSearchDetails(flightSearchArray);

			Segment segment = new Segment();

			//segment.setBondType(req.getBondType());
			segment.setBaggageFare(req.isBaggageFare());
			segment.setDeeplink("");
			segment.setEngineID(req.getEngineID());
			segment.setFares(req.getFares());
			segment.setFareIndicator("0");
			segment.setFareRule(req.getFareRule());
			segment.setCache(req.isCache());
			segment.setHoldBooking(req.isHoldBooking());
			segment.setInternational(req.isInternational());
			segment.setRoundTrip(req.isRoundTrip());
			segment.setSpecial(req.isSpecial());
			segment.setSpecialId(req.isSpecialId());
			segment.setItineraryKey(req.getItineraryKey());
			segment.setMemoryCreationTime(new Date().toString());
			segment.setJourneyIndex(Integer.parseInt(req.getJourneyIndex()));
			segment.setNearByAirport(req.isNearByAirport());
			segment.setRemark("");
			segment.setSearchId(req.getSearchId());

			/*
			 * Bonds bonds=new Bonds();
			 * 
			 * bonds.setBaggageFare(req.isBaggageFare());
			 * bonds.setSsrFare(false);
			 * bonds.setItineraryKey(req.getItineraryKey());
			 * bonds.setJourneyTime(req.getJourneyTime());
			 * bonds.setLegs(req.getLegs());
			 */

			segment.setBonds(req.getBonds());

			ArrayList<Segment> segments = new ArrayList<>();

			segments.add(segment);
			AirPriceCheckRequest airPrice = new AirPriceCheckRequest();

			airPrice.setClientApiName("Flight Search");
			airPrice.setClientIp(Inet4Address.getLocalHost().getHostAddress());
			airPrice.setClientToken(token);
			airPrice.setCllientKey(key);
			airPrice.setFlightAvailability(flightAvailability);
			airPrice.setSegments(segments);

			// String jsonObject=airPrice.getJsonRequest();

			// System.err.println(jsonObject);

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(domain+MdexConstants.MDEX_TRAVEL_URL+MdexConstants.Flight_AirRePriceRQ_API_Connecting);
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtil.getHashBus(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			logger.info(strResponse);			
			return "" + strResponse;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);

		}
		return "";
	}

	@Override
	public String FlightBookingInitiate(String sessionId, String firstName, String paymentAmount, String ticketDetails,
			String paymentmethod, MUser user) {
		try {
			String domain = null;
			String basicAuth=null;
			String key=null;
			String token=null;
			if(MatchMoveUtil.PRODUCTION){
			ERCredentialsManager credentialManager=eRCredentialManager.getCredentials("LIVE");
			if(credentialManager!=null){
				domain=credentialManager.getMdex_base_url();
				basicAuth=credentialManager.getBasic_auth_header();
				key=credentialManager.getMdex_keys();
				token=credentialManager.getMdex_token();
			}
			} else {
				ERCredentialsManager credentialManager=eRCredentialManager.getCredentials("TEST");
				if(credentialManager!=null){
				domain=credentialManager.getMdex_base_url();
				basicAuth=credentialManager.getBasic_auth_header();
				key=credentialManager.getMdex_keys();
				token=credentialManager.getMdex_token();
				}
			}
			
			MService service = mServiceRepository.findServiceByCode(ServiceCodeUtil.FLIGHT);
			if(service != null) {
				TransactionError error = loadMoneyValidation.validateEaseMyTrip(String.valueOf(paymentAmount),user.getUsername(),service);
				if(error.isValid()) {
					FlightPaymentRequest dto = new FlightPaymentRequest();
					dto.setSessionId(sessionId);
					dto.setFirstName(firstName);
					dto.setTicketDetails(ticketDetails);
					dto.setPaymentAmount(Double.parseDouble(paymentAmount));
					dto.setPaymentmethod(paymentmethod);
					FlightPaymentResponse orderResponse = flightApi.placeOrder(dto,service,user);
					if(orderResponse.isValid()) {
						return "S00";
					}else {
						return "F00";
					}
				}
			}
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			return "F00";
		}
		return "F00";
	}
	
	@Override
	public String AirBookRQForMobile(MobileFlightBookRequest req) {
		// TODO Auto-generated method stub
		try {
			String domain = null;
			String basicAuth=null;
			String key=null;
			String token=null;
			if(MatchMoveUtil.PRODUCTION){
			ERCredentialsManager credentialManager=eRCredentialManager.getCredentials("LIVE");
			if(credentialManager!=null){
				domain=credentialManager.getMdex_base_url();
				basicAuth=credentialManager.getBasic_auth_header();
				key=credentialManager.getMdex_keys();
				token=credentialManager.getMdex_token();
			}
			} else {
				ERCredentialsManager credentialManager=eRCredentialManager.getCredentials("TEST");
				if(credentialManager!=null){
				domain=credentialManager.getMdex_base_url();
				basicAuth=credentialManager.getBasic_auth_header();
				key=credentialManager.getMdex_keys();
				token=credentialManager.getMdex_token();
				}
			}
			
			JSONObject payload = CreateJsonRequestFlight.createjsonForMobile(req);
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(domain+MdexConstants.MDEX_TRAVEL_URL+MdexConstants.Flight_AirBookRQ_API);
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtil.getHashBus(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			System.out.println(strResponse);
			return "" + strResponse;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		return "";
	}


	/*@Override
	public String FlightBookingSucess(String sessionId, String ticketNumber, String firstName, String bookingRefId,
			String transactionRefNomdex, String paymentAmount, String ticketDetails, String statusflight,
			String paymentstatus, String paymentmethod, String transactionRefno, boolean isSuccess,String email,String mobile) {
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);
			payload.put("ticketNumber", ticketNumber);
			payload.put("firstName", firstName);
			payload.put("bookingRefId", bookingRefId);
			payload.put("transactionRefNomdex", transactionRefNomdex);
			payload.put("paymentAmount", paymentAmount);
			payload.put("ticketDetails", ticketDetails);
			payload.put("statusflight", statusflight);
			payload.put("paymentstatus", paymentstatus);
			payload.put("paymentmethod", paymentmethod);
			payload.put("transactionRefno", transactionRefno);
			payload.put("success", isSuccess);
			payload.put("email", email);
			payload.put("mobile", mobile);
			// System.out.println(payload.toString());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.getFlightSucessURL(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			// System.out.println(strResponse);
			return "" + strResponse;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			return "";
		}
	}*/
	
	//new code
	
	@Override
	public FlightCityList getAirLineNames(String sessionId) {
		FlightCityList resp=new FlightCityList();
		try {
			List<FlightAirLineList> airLineLists=flightApi.getAllAirLineList();
								
			resp.setCode(ResponseStatus.SUCCESS.getValue());
			resp.setMessage("Get All AriLine List From DB");
			resp.setStatus(ResponseStatus.SUCCESS.getKey());
			resp.setDetails(airLineLists);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public FlightResponseDTO searchFlight(TravelFlightRequest req) {

		FlightResponseDTO result=new FlightResponseDTO();

		try {
			String domain = null;
			String basicAuth=null;
			String key=null;
			String token=null;
			if(MatchMoveUtil.PRODUCTION){
			ERCredentialsManager credentialManager=eRCredentialManager.getCredentials("LIVE");
			if(credentialManager!=null){
				domain=credentialManager.getMdex_base_url();
				basicAuth=credentialManager.getBasic_auth_header();
				key=credentialManager.getMdex_keys();
				token=credentialManager.getMdex_token();
			}
			} else {
				ERCredentialsManager credentialManager=eRCredentialManager.getCredentials("TEST");
				if(credentialManager!=null){
				domain=credentialManager.getMdex_base_url();
				basicAuth=credentialManager.getBasic_auth_header();
				key=credentialManager.getMdex_keys();
				token=credentialManager.getMdex_token();
				}
			}
			
			boolean val=compareCountry(req);
			
			org.json.simple.JSONArray flightSearchDetails = new org.json.simple.JSONArray();

			org.json.JSONArray engineIDs = new org.json.JSONArray();

			JSONObject flightSearchDetailsobj = new JSONObject();
			JSONObject roundtripDetailsobj = new JSONObject();

			JSONObject engineIDsobj = new JSONObject();
			JSONObject payload = new JSONObject();
			payload.put("clientIp", Inet4Address.getLocalHost().getHostAddress());
			payload.put("cllientKey", key);
			payload.put("clientToken", token);
			payload.put("clientApiName", "Flight Search");

			flightSearchDetailsobj.put("origin", req.getOrigin());
			flightSearchDetailsobj.put("destination", req.getDestination());
			flightSearchDetailsobj.put("beginDate", req.getBeginDate());

			flightSearchDetails.add(flightSearchDetailsobj);
			if (req.getTripType().equals("RoundTrip")) {
				roundtripDetailsobj.put("origin", req.getDestination());
				roundtripDetailsobj.put("destination", req.getOrigin());
				roundtripDetailsobj.put("beginDate", req.getEndDate());

				flightSearchDetails.add(roundtripDetailsobj);
			}

			ArrayList<String> objhasmap = new ArrayList<>();
			objhasmap.add("" + EngineID.Indigo);
			objhasmap.add("" + EngineID.AirAsia);
			objhasmap.add("" + EngineID.AirCosta);
			objhasmap.add("" + EngineID.GoAir);
			objhasmap.add("" + EngineID.Spicjet);
			objhasmap.add("" + EngineID.Trujet);
			objhasmap.add("" + EngineID.TravelPort);

			payload.put("engineIDs", objhasmap);

			payload.put("flightSearchDetails", flightSearchDetails);
			payload.put("tripType", req.getTripType());
			payload.put("cabin", req.getCabin());
			payload.put("adults", req.getAdults());
			payload.put("childs", req.getChilds());
			payload.put("infants", req.getInfants());
			payload.put("traceId", "AYTM00011111111110002");

			System.out.println(payload.toString());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(domain+MdexConstants.MDEX_TRAVEL_URL+MdexConstants.Flight_Search_API);
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtil.getHashBus(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			System.out.println(strResponse);
			if (response.getStatus() != 200) {
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setMessage("Service unavailable");
				result.setDetails("Service unavailable");
			} else {
				FlightSearchResponse searchResponse = new FlightSearchResponse();
				JSONObject jobjres = new JSONObject(strResponse);
				JSONObject jobj=jobjres.getJSONObject("details");
				if (jobj != null) {
					String errStr = jobj.getString("errors");
					//System.err.println("err Str : " + errStr);
					if (!errStr.equals("null")) {
						JSONObject errObj = jobj.getJSONObject("errors");
						Errors errCls = new Errors();
						errCls.setCode(errObj.getString("code"));
						errCls.setDescription(errObj.getString("description"));
						searchResponse.setErrors(errCls);
						result.setStatus(ResponseStatus.FAILURE.getKey());
						result.setMessage(errObj.getString("description"));
						result.setDetails(searchResponse);
					} else {
						String JourneysStr = jobj.getString("journeys");
						//System.out.println("Journeys  " + JourneysStr);
						if(JourneysStr.equals("null")){
							result.setStatus(ResponseStatus.FAILURE.getKey());
							result.setMessage("Flight Search Details are not Found");
							result.setDetails(searchResponse);
							return result;
						}else{
							ArrayList<Journeys> journeysList = new ArrayList<>();
							JSONArray Journeys = jobj.getJSONArray("journeys");
							for (int i = 0; i < Journeys.length(); i++) {
								Journeys journeysCls = new Journeys();
								JSONObject job = Journeys.getJSONObject(i);
								if (job != null) {
									String jrnyStr = job.getString("journeyDetail");
									if (!jrnyStr.equals("null")) {
										JSONObject jrnyD = job.getJSONObject("journeyDetail");
										//System.err.println("journeyDetails : " + jrnyD);
										if (jrnyD != null) {
											JourneyDetail journeyDetail = new JourneyDetail();
											journeyDetail.setOrigin(jrnyD.getString("origin"));
											journeyDetail.setDestination(jrnyD.getString("destination"));
											journeyDetail.setBeginDate(jrnyD.getString("beginDate"));
											journeyDetail.setEndDate(jrnyD.getString("endDate"));
											
											journeyDetail.setEngineID(engineId(jrnyD.getString("engineID")));
											journeyDetail.setCabin(ConvertUtil.convertCabin(jrnyD.getString("cabin")));
											journeyDetail.setCurrencyCode(jrnyD.getString("currencyCode"));
											journeysCls.setJourneyDetail(journeyDetail);
										}
									}
									ArrayList<Segment> segList = new ArrayList<>();
									JSONArray segments = job.getJSONArray("segments");
									for (int j = 0; j < segments.length(); j++) {
										ArrayList<Bonds> bondList = new ArrayList<>();
										ArrayList<Fare> fareList = new ArrayList<>();
										Segment segCls = new Segment();
										job = segments.getJSONObject(j);
										segCls.setBondType(job.getString("bondType"));
										segCls.setDeeplink(job.getString("deeplink"));
										segCls.setEngineID(job.getString("engineID"));
										segCls.setFareRule(job.getString("fareRule"));
										segCls.setBaggageFare(job.getBoolean("isBaggageFare"));
										segCls.setCache(job.getBoolean("isCache"));
										segCls.setHoldBooking(job.getBoolean("isHoldBooking"));
//										segCls.setInternational(job.getBoolean("isInternational"));
										segCls.setInternational(val);
										segCls.setRoundTrip(job.getBoolean("isRoundTrip"));
										segCls.setSpecial(job.getBoolean("isSpecial"));
										segCls.setSpecialId(job.getBoolean("isSpecialId"));
										segCls.setItineraryKey(job.getString("itineraryKey"));
										segCls.setJourneyIndex(job.getInt("journeyIndex"));
										segCls.setNearByAirport(job.getBoolean("nearByAirport"));
										segCls.setRemark(job.getString("remark"));
										segCls.setSearchId(job.getString("searchId"));
										segCls.setBonds(bondList);
										segCls.setFares(fareList);
										segList.add(segCls);
										JSONArray bonds = job.getJSONArray("bonds");
										JSONArray fareArr = job.getJSONArray("fare");
										for (int k = 0; k < bonds.length(); k++) {
											Bonds bondCls = new Bonds();
											job = bonds.getJSONObject(k);
											bondCls.setBoundType(job.getString("boundType"));
											bondCls.setItineraryKey(job.getString("itineraryKey"));
											bondCls.setBaggageFare(job.getBoolean("isBaggageFare"));
											bondCls.setSsrFare(job.getBoolean("isSSR"));
											bondCls.setJourneyTime(job.getString("journeyTime"));
											ArrayList<Legs> legsList = new ArrayList<>();
											JSONArray legs = job.getJSONArray("legs");
											for (int l = 0; l < legs.length(); l++) {
												Legs legsCls = new Legs();
												job = legs.getJSONObject(l);
												legsCls.setAircraftCode(job.getString("aircraftCode"));
												legsCls.setAircraftType(job.getString("aircraftType"));
												legsCls.setAirlineName(job.getString("airlineName"));
												//legsCls.setAmount(job.getDouble("Amount"));
												legsCls.setArrivalDate(job.getString("arrivalDate"));
												legsCls.setArrivalTerminal(job.getString("arrivalTerminal"));
												legsCls.setArrivalTime(job.getString("arrivalTime"));
												legsCls.setAvailableSeat(job.getString("availableSeat"));
												legsCls.setBaggageUnit(job.getString("baggageUnit"));
												legsCls.setBaggageWeight(job.getString("baggageWeight"));
												legsCls.setBoundTypes(job.getString("boundType"));
												legsCls.setCabin(ConvertUtil.convertCabin(job.getString("cabin")));
												legsCls.setCapacity(job.getString("capacity"));
												legsCls.setCarrierCode(job.getString("carrierCode"));
												legsCls.setCurrencyCode(job.getString("currencyCode"));
												legsCls.setDepartureDate(job.getString("departureDate"));
												legsCls.setDepartureTerminal(job.getString("departureTerminal"));
												legsCls.setDepartureTime(job.getString("departureTime"));
												legsCls.setDestination(job.getString("destination"));
												legsCls.setDuration(job.getString("duration"));
												legsCls.setFareBasisCode(job.getString("fareBasisCode"));
												legsCls.setFareClassOfService(job.getString("fareClassOfService"));
												legsCls.setFlightDesignator(job.getString("flightDesignator"));
												legsCls.setFlightDetailRefKey(job.getString("flightDetailRefKey"));
												legsCls.setFlightNumber(job.getString("flightNumber"));
												legsCls.setFlightName(job.getString("flightName"));
												legsCls.setGroup(job.getString("group"));
												legsCls.setConnecting(job.getBoolean("isConnecting"));
												legsCls.setNumberOfStops(job.getString("numberOfStops"));
												legsCls.setOrigin(job.getString("origin"));
												legsCls.setProviderCode(job.getString("providerCode"));
												legsCls.setRemarks(job.getString("remarks"));
												legsCls.setSold(job.getInt("sold")+"");
												legsCls.setStatus(job.getString("status"));
												String cbnStr = job.getString("cabinClasses");
												ArrayList<CabinClasses> cbnList = new ArrayList<>();
												if(!cbnStr.equals("null")){
													JSONArray cabinClasses = job.getJSONArray("cabinClasses");
													for (int m = 0; m < cabinClasses.length(); m++) {
														CabinClasses cbnCls = new CabinClasses();
														job = cabinClasses.getJSONObject(m);
														cbnCls.setCabinType(job.getString("cabinType"));
														ArrayList<BookingClass> bookingList = new ArrayList<>();
														String bkStr = job.getString("bookingClass");
														if(!bkStr.equals("null")){
															JSONArray bookingClasses = job.getJSONArray("bookingClass");
															for (int n = 0; n < bookingClasses.length(); n++) {
																BookingClass bookingCls = new BookingClass();
																job = bookingClasses.getJSONObject(n);
																bookingCls.setBClass(job.getString("bClass"));
																bookingCls.setCount(job.getInt("count")+"");
																bookingList.add(bookingCls);
															}
														}
														cbnCls.setBookingClasses(bookingList);
														cbnList.add(cbnCls);
													}
												}
												legsCls.setCabinClasses(cbnList);
												ArrayList<SSRDetails> ssrList = new ArrayList<>();
												String ssrStr = job.getString("ssrDetails");
												if(!ssrStr.equals("null")){
													JSONArray ssrDetails = job.getJSONArray("ssrDetails");
													for (int m = 0; m < ssrDetails.length(); m++) {
														SSRDetails ssrCls = new SSRDetails();
														job = ssrDetails.getJSONObject(m);
														ssrCls.setSSRAmount(job.getDouble("amount"));
														ssrCls.setSSRDetails(job.getString("detail"));
														ssrCls.setSSRCode(job.getString("ssrCode"));
														ssrCls.setSSRType(job.getString("ssrType"));
														ssrList.add(ssrCls);
													}
												}
												legsCls.setSsrDetails(ssrList);
												legsList.add(legsCls);
											}
											bondCls.setLegs(legsList);
											bondList.add(bondCls);
										}

										for (int k = 0; k < fareArr.length(); k++) {
											JSONObject fare=fareArr.getJSONObject(k);										
											Fare fareCls = new Fare();
											fareCls.setBasicFare(fare.getDouble("basicFare"));
											fareCls.setExchangeRate(fare.getDouble("exchangeRate"));
											fareCls.setTotalFareWithOutMarkUp(fare.getDouble("totalFareWithOutMarkUp"));
											fareCls.setTotalTaxWithOutMarkUp(fare.getDouble("totalTaxWithOutMarkUp"));
											fareList.add(fareCls);
											JSONArray paxFares = fare.getJSONArray("paxFares");
											ArrayList<PaxFares> paxFaresList = new ArrayList<>();
											for (int l = 0; l < paxFares.length(); l++) {
												job = paxFares.getJSONObject(l);
												JSONArray fares = job.getJSONArray("fares");
												ArrayList<BookFare> faresList = new ArrayList<>();
												PaxFares paxFaresCls = new PaxFares();
												paxFaresCls.setBaggageUnit(job.getString("baggageUnit"));
												paxFaresCls.setBaggageWeight(job.getString("baggageWeight"));
												paxFaresCls.setBaseTransactionAmount(job.getDouble("baseTransactionAmount"));
												paxFaresCls.setBasicFare(job.getDouble("basicFare"));
												paxFaresCls.setCancelPenalty(job.getDouble("cancelPenalty"));
												paxFaresCls.setChangePenalty(job.getDouble("changePenalty"));
												paxFaresCls.setEquivCurrencyCode(job.getString("equivCurrencyCode"));
												paxFaresCls.setFareBasisCode(job.getString("fareBasisCode"));
												paxFaresCls.setFareInfoKey(job.getString("fareInfoKey"));
												paxFaresCls.setFareInfoValue(job.getString("fareInfoValue"));
												paxFaresCls.setMarkUP(job.getDouble("markUP"));
												paxFaresCls.setPaxType(ConvertUtil.convertPaxType(job.getString("paxType")));
												paxFaresCls.setRefundable(job.getBoolean("refundable"));
												paxFaresCls.setTotalFare(job.getDouble("totalFare"));
												paxFaresCls.setTotalTax(job.getDouble("totalTax"));
												paxFaresCls.setPaxType(job.getString("paxType"));
												paxFaresCls.setTransactionAmount(job.getDouble("transactionAmount"));
												for (int l2 = 0; l2 < fares.length(); l2++) {
													job = fares.getJSONObject(l2);
													BookFare faresCls = new BookFare();
													faresCls.setAmount(job.getDouble("amount"));
													faresCls.setChargeCode(job.getString("chargeCode"));
													faresCls.setChargeType(job.getString("chargeType"));
													faresList.add(faresCls);
												}
												paxFaresCls.setBookFares(faresList);
												paxFaresList.add(paxFaresCls);
											}
											fareCls.setPaxFares(paxFaresList);
										}
									}
									journeysCls.setSegments(segList);
									journeysList.add(journeysCls);
								}

							}
							searchResponse.setJourneys(journeysList);
							result.setCode(ResponseStatus.SUCCESS.getValue());
							result.setStatus(ResponseStatus.SUCCESS.getKey());
							result.setMessage("Flight Search Response");
							result.setDetails(searchResponse);
							return result;
						}
					}

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setStatus(ResponseStatus.FAILURE.getKey());
			result.setMessage("Service unavailable");
			result.setDetails("Service unavailable");
		}
		return result;
	}
	
	@Override
	public boolean compareCountry(TravelFlightRequest req)
	{
		boolean resp=false;
		try {			
			CompareCountry dto = new CompareCountry();
			dto.setSessionId(req.getSessionId());
			dto.setDest(req.getDestination());
			dto.setOrg(req.getOrigin());
			resp = flightApi.comCountry(dto);
		} catch (Exception e) {
			System.out.println(e);
			
		}
		return resp;
		
	}
	
	private String engineId(String engineId)
	{
		String val=engineId;
		
		if (engineId.equalsIgnoreCase("AirAsia")) {
			val="AirAsia";
		}
		else if (engineId.equalsIgnoreCase("Airindia")) {
			val="Airindia";
		}
		else if (engineId.equalsIgnoreCase("Spicjet")) {
			val="Spicjet";
		}
		else if (engineId.equalsIgnoreCase("Jetairways")) {
			val="Jetairways";
		}
		else if (engineId.equalsIgnoreCase("Indigo")) {
			val="Indigo";
		}
		else if (engineId.equalsIgnoreCase("GoAir")) {
			val="GoAir";
		}
		return val;
		
	}
	
	@Override
	public FlightResponseDTO airRePriceRQ(FlightPriceCheckRequest req) {

		FlightResponseDTO result=new FlightResponseDTO();

		try {
			
			String domain = null;
			String basicAuth=null;
			String key=null;
			String token=null;
			if(MatchMoveUtil.PRODUCTION){
			ERCredentialsManager credentialManager=eRCredentialManager.getCredentials("LIVE");
			if(credentialManager!=null){
				domain=credentialManager.getMdex_base_url();
				basicAuth=credentialManager.getBasic_auth_header();
				key=credentialManager.getMdex_keys();
				token=credentialManager.getMdex_token();
			}
			} else {
				ERCredentialsManager credentialManager=eRCredentialManager.getCredentials("TEST");
				if(credentialManager!=null){
				domain=credentialManager.getMdex_base_url();
				basicAuth=credentialManager.getBasic_auth_header();
				key=credentialManager.getMdex_keys();
				token=credentialManager.getMdex_token();
				}
			}

			JSONObject payload = new JSONObject();

			FlightAvailability flightAvailability = new FlightAvailability();

			FlightSearchDetails flightSearchDetails = new FlightSearchDetails();

			flightSearchDetails.setBeginDate(req.getBeginDate());
			flightSearchDetails.setDestination(req.getDestination());
			flightSearchDetails.setEndDate(req.getEndDate());
			flightSearchDetails.setOrigin(req.getOrigin());

			ArrayList<String> engineIds = new ArrayList<>();
			engineIds.add(req.getEngineID());

			flightAvailability.setEngineIDs(engineIds);
			flightAvailability.setCabin(req.getCabin());
			flightAvailability.setTripType(req.getTripType());
			flightAvailability.setAdults(req.getAdults());
			flightAvailability.setChilds(req.getChilds());
			flightAvailability.setInfants(req.getInfants());
			flightAvailability.setTraceId(req.getTraceId());
			ArrayList<FlightSearchDetails> flightSearchArray = new ArrayList<>();
			flightSearchArray.add(flightSearchDetails);


			Segment segment = new Segment();

			//segment.setBondType(req.getBondType());
			segment.setBaggageFare(req.isBaggageFare());
			segment.setDeeplink("");
			segment.setEngineID(req.getEngineID());
			segment.setFares(req.getFares());
			segment.setFareIndicator("0");
			segment.setFareRule(req.getFareRule());
			segment.setCache(req.isCache());
			segment.setHoldBooking(req.isHoldBooking());
			segment.setInternational(req.isInternational());
			segment.setRoundTrip(req.isRoundTrip());
			segment.setSpecial(req.isSpecial());
			segment.setSpecialId(req.isSpecialId());
			segment.setItineraryKey(req.getItineraryKey());
			segment.setMemoryCreationTime(new Date().toString());
			segment.setJourneyIndex(Integer.parseInt(req.getJourneyIndex()));
			segment.setNearByAirport(req.isNearByAirport());
			segment.setRemark("");
			segment.setSearchId(req.getSearchId());
			segment.setBonds(req.getBonds());

			if (req.getBonds().size()==2) {
				FlightSearchDetails flightSearchDetailsreturn = new FlightSearchDetails();

				flightSearchDetailsreturn.setBeginDate(req.getEndDate());
				flightSearchDetailsreturn.setDestination(req.getOrigin());
				flightSearchDetailsreturn.setOrigin(req.getDestination());
				flightSearchArray.add(flightSearchDetailsreturn);
			}

			flightAvailability.setFlightSearchDetails(flightSearchArray);

			ArrayList<Segment> segments = new ArrayList<>();

			segments.add(segment);
			AirPriceCheckRequest airPrice = new AirPriceCheckRequest();

			airPrice.setClientApiName("Flight Price Recheck");
			airPrice.setClientIp(Inet4Address.getLocalHost().getHostAddress());
			airPrice.setClientToken(token);
			airPrice.setCllientKey(key);
			airPrice.setFlightAvailability(flightAvailability);
			airPrice.setSegments(segments);

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(domain+MdexConstants.MDEX_TRAVEL_URL+MdexConstants.Flight_AirRePriceRQ_API_Connecting);
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtil.getHashBus(payload.toString())).post(ClientResponse.class, airPrice.getJsonRequest());

			String strResponse = response.getEntity(String.class);
			System.out.println(strResponse);

			if (response.getStatus() != 200) {
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setMessage("Service unavailable");
				result.setDetails("Service unavailable");

			} else {
				FlightPriceReCheckResponse reCheckResponse =  new FlightPriceReCheckResponse();
				JSONObject jobjres = new JSONObject(strResponse);
				JSONObject jobj=jobjres.getJSONObject("details");
				if (jobj != null) {
					String errStr = jobj.getString("errors");
					System.err.println("err Str : " + errStr);
					if (!errStr.equals("null")) {
						JSONObject errObj = jobj.getJSONObject("errors");
						Errors errCls = new Errors();
						errCls.setCode(errObj.getString("code"));
						errCls.setDescription(errObj.getString("description"));
						reCheckResponse.setErrors(errCls);
						result.setCode(ResponseStatus.FAILURE.getValue());
						result.setStatus(ResponseStatus.FAILURE.getKey());
						result.setMessage(errObj.getString("description"));
						result.setDetails(reCheckResponse);
					} else {
						ArrayList<Journeys> journeysList = new ArrayList<>();
						JSONArray Journeys = jobj.getJSONArray("journeys");
						for (int i = 0; i < Journeys.length(); i++) {
							Journeys journeysCls = new Journeys();
							JSONObject job = Journeys.getJSONObject(i);
							if (job != null) {
								String jrnyStr = job.getString("journeyDetail");
								if (!jrnyStr.equals("null")) {
									JSONObject jrnyD = job.getJSONObject("journeyDetail");
									System.err.println("journeyDetails : " + jrnyD);
									if (jrnyD != null) {
										JourneyDetail journeyDetail = new JourneyDetail();
										journeyDetail.setOrigin(jrnyD.getString("origin"));
										journeyDetail.setDestination(jrnyD.getString("destination"));
										journeyDetail.setBeginDate(jrnyD.getString("beginDate"));
										journeyDetail.setEndDate(jrnyD.getString("endDate"));
										journeyDetail.setEngineID(jrnyD.getString("engineID"));
										journeyDetail.setCabin(jrnyD.getString("cabin"));
										journeyDetail.setCurrencyCode(jrnyD.getString("currencyCode"));
										journeysCls.setJourneyDetail(journeyDetail);
									}
								}
								ArrayList<Segment> segList = new ArrayList<>();
								JSONArray segmentsArr = job.getJSONArray("segments");
								if(segmentsArr.length() != 0){
									for (int j = 0; j < segmentsArr.length(); j++) {
										ArrayList<Bonds> bondList = new ArrayList<>();
										ArrayList<Fare> fareList = new ArrayList<>();
										Segment segCls = new Segment();
										job = segmentsArr.getJSONObject(j);
										segCls.setBondType(job.getString("bondType"));
										segCls.setDeeplink(job.getString("deeplink"));
										segCls.setEngineID(job.getString("engineID"));
										segCls.setFareRule(job.getString("fareRule"));
										segCls.setBaggageFare(job.getBoolean("isBaggageFare"));
										segCls.setCache(job.getBoolean("isCache"));
										segCls.setHoldBooking(job.getBoolean("isHoldBooking"));
										segCls.setInternational(job.getBoolean("isInternational"));
										segCls.setRoundTrip(job.getBoolean("isRoundTrip"));
										segCls.setSpecial(job.getBoolean("isSpecial"));
										segCls.setSpecialId(job.getBoolean("isSpecialId"));
										segCls.setItineraryKey(job.getString("itineraryKey"));
										segCls.setJourneyIndex(job.getInt("journeyIndex"));
										segCls.setNearByAirport(job.getBoolean("nearByAirport"));
										segCls.setRemark(job.getString("remark"));
										segCls.setSearchId(job.getString("searchId"));
										segCls.setBonds(bondList);
										segCls.setFares(fareList);
										segList.add(segCls);
										JSONArray bonds = job.getJSONArray("bonds");
										//JSONObject fare = job.getJSONObject("fare");
										JSONArray fareArr = job.getJSONArray("fare");
										//shjkjsk
										for (int k = 0; k < bonds.length(); k++) {
											Bonds bondCls = new Bonds();
											job = bonds.getJSONObject(k);
											bondCls.setBoundType(job.getString("boundType"));
											bondCls.setItineraryKey(job.getString("itineraryKey"));
											bondCls.setBaggageFare(job.getBoolean("isBaggageFare"));
											bondCls.setSsrFare(job.getBoolean("isSSR"));
											bondCls.setJourneyTime(job.getString("journeyTime"));
											ArrayList<Legs> legsList = new ArrayList<>();
											JSONArray legs = job.getJSONArray("legs");
											for (int l = 0; l < legs.length(); l++) {
												Legs legsCls = new Legs();
												job = legs.getJSONObject(l);
												legsCls.setAircraftCode(job.getString("aircraftCode"));
												legsCls.setAircraftType(job.getString("aircraftType"));
												legsCls.setAirlineName(job.getString("airlineName"));
												//legsCls.setAmount(job.getDouble("Amount"));
												legsCls.setArrivalDate(job.getString("arrivalDate"));
												legsCls.setArrivalTerminal(job.getString("arrivalTerminal"));
												legsCls.setArrivalTime(job.getString("arrivalTime"));
												legsCls.setAvailableSeat(job.getString("availableSeat"));
												legsCls.setBaggageUnit(job.getString("baggageUnit"));
												legsCls.setBaggageWeight(job.getString("baggageWeight"));
												legsCls.setBoundTypes(job.getString("boundType"));
												legsCls.setCabin(job.getString("cabin"));
												legsCls.setCapacity(job.getString("capacity"));
												legsCls.setCarrierCode(job.getString("carrierCode"));
												legsCls.setCurrencyCode(job.getString("currencyCode"));
												legsCls.setDepartureDate(job.getString("departureDate"));
												legsCls.setDepartureTerminal(job.getString("departureTerminal"));
												legsCls.setDepartureTime(job.getString("departureTime"));
												legsCls.setDestination(job.getString("destination"));
												legsCls.setDuration(job.getString("duration"));
												legsCls.setFareBasisCode(job.getString("fareBasisCode"));
												legsCls.setFareClassOfService(job.getString("fareClassOfService"));
												legsCls.setFlightDesignator(job.getString("flightDesignator"));
												legsCls.setFlightDetailRefKey(job.getString("flightDetailRefKey"));
												legsCls.setFlightNumber(job.getString("flightNumber"));
												legsCls.setGroup(job.getString("group"));
												legsCls.setConnecting(job.getBoolean("isConnecting"));
												legsCls.setNumberOfStops(job.getString("numberOfStops"));
												legsCls.setOrigin(job.getString("origin"));
												legsCls.setProviderCode(job.getString("providerCode"));
												legsCls.setRemarks(job.getString("remarks"));
												legsCls.setSold(job.getInt("sold")+"");
												legsCls.setStatus(job.getString("status"));
												String cbnStr = job.getString("cabinClasses");
												ArrayList<CabinClasses> cbnList = new ArrayList<>();
												if(!cbnStr.equals("null")){
													JSONArray cabinClasses = job.getJSONArray("cabinClasses");
													for (int m = 0; m < cabinClasses.length(); m++) {
														CabinClasses cbnCls = new CabinClasses();
														job = cabinClasses.getJSONObject(m);
														cbnCls.setCabinType(job.getString("cabinType"));
														ArrayList<BookingClass> bookingList = new ArrayList<>();
														String bkStr = job.getString("bookingClass");
														if(!bkStr.equals("null")){
															JSONArray bookingClasses = job.getJSONArray("bookingClass");
															for (int n = 0; n < bookingClasses.length(); n++) {
																BookingClass bookingCls = new BookingClass();
																job = bookingClasses.getJSONObject(n);
																bookingCls.setBClass(job.getString("bClass"));
																bookingCls.setCount(job.getInt("count")+"");
																bookingList.add(bookingCls);
															}
														}
														cbnCls.setBookingClasses(bookingList);
														cbnList.add(cbnCls);
													}
												}
												legsCls.setCabinClasses(cbnList);
												ArrayList<SSRDetails> ssrList = new ArrayList<>();
												String ssrStr = job.getString("ssrDetails");
												if(!ssrStr.equals("null")){
													JSONArray ssrDetails = job.getJSONArray("ssrDetails");
													for (int m = 0; m < ssrDetails.length(); m++) {
														SSRDetails ssrCls = new SSRDetails();
														job = ssrDetails.getJSONObject(m);
														ssrCls.setSSRAmount(job.getDouble("amount"));
														ssrCls.setSSRDetails(job.getString("detail"));
														ssrCls.setSSRCode(job.getString("ssrcode"));
														ssrCls.setSSRType(job.getString("ssrtype"));
														ssrList.add(ssrCls);
													}
												}
												legsCls.setSsrDetails(ssrList);
												legsList.add(legsCls);
											}
											bondCls.setLegs(legsList);
											bondList.add(bondCls);
										}


										for (int k = 0; k < fareArr.length(); k++) {
											JSONObject fare=fareArr.getJSONObject(k);										
											Fare fareCls = new Fare();
											fareCls.setBasicFare(fare.getDouble("basicFare"));
											fareCls.setExchangeRate(fare.getDouble("exchangeRate"));
											fareCls.setTotalFareWithOutMarkUp(fare.getDouble("totalFareWithOutMarkUp"));
											fareCls.setTotalTaxWithOutMarkUp(fare.getDouble("totalTaxWithOutMarkUp"));
											fareList.add(fareCls);
											JSONArray paxFares = fare.getJSONArray("paxFares");
											ArrayList<PaxFares> paxFaresList = new ArrayList<>();
											for (int l = 0; l < paxFares.length(); l++) {
												job = paxFares.getJSONObject(l);
												JSONArray fares = job.getJSONArray("fares");
												ArrayList<BookFare> faresList = new ArrayList<>();
												PaxFares paxFaresCls = new PaxFares();
												paxFaresCls.setBaggageUnit(job.getString("baggageUnit"));
												paxFaresCls.setBaggageWeight(job.getString("baggageWeight"));
												paxFaresCls.setBaseTransactionAmount(job.getDouble("baseTransactionAmount"));
												paxFaresCls.setBasicFare(job.getDouble("basicFare"));
												paxFaresCls.setCancelPenalty(job.getDouble("cancelPenalty"));
												paxFaresCls.setChangePenalty(job.getDouble("changePenalty"));
												paxFaresCls.setEquivCurrencyCode(job.getString("equivCurrencyCode"));
												paxFaresCls.setFareBasisCode(job.getString("fareBasisCode"));
												paxFaresCls.setFareInfoKey(job.getString("fareInfoKey"));
												paxFaresCls.setFareInfoValue(job.getString("fareInfoValue"));
												paxFaresCls.setMarkUP(job.getDouble("markUP"));
												paxFaresCls.setPaxType((job.getString("paxType")));
												paxFaresCls.setRefundable(job.getBoolean("refundable"));
												paxFaresCls.setTotalFare(job.getDouble("totalFare"));
												paxFaresCls.setTotalTax(job.getDouble("totalTax"));
												paxFaresCls.setTransactionAmount(job.getDouble("transactionAmount"));
												for (int l2 = 0; l2 < fares.length(); l2++) {
													job = fares.getJSONObject(l2);
													BookFare faresCls = new BookFare();
													faresCls.setAmount(job.getDouble("amount"));
													faresCls.setChargeCode(job.getString("chargeCode"));
													faresCls.setChargeType(job.getString("chargeType"));
													faresList.add(faresCls);
												}
												paxFaresCls.setBookFares(faresList);
												paxFaresList.add(paxFaresCls);
											}
											fareCls.setPaxFares(paxFaresList);
										}
									}
									journeysCls.setSegments(segList);
									journeysList.add(journeysCls);
								}else{
									reCheckResponse.setJourneys(journeysList);
									result.setStatus(ResponseStatus.FAILURE.getKey());
									result.setMessage("Flight Details Not Found");
									result.setDetails(reCheckResponse);
									System.err.println("Details not found");
									return result;
								}

							}

						}
						reCheckResponse.setJourneys(journeysList);
						reCheckResponse.setTraceId(jobj.getString("traceId"));
						result.setCode(ResponseStatus.SUCCESS.getValue());
						result.setStatus(ResponseStatus.SUCCESS.getKey());
						result.setMessage("Flight Price Re-Check Response");
						result.setDetails(reCheckResponse);
						return result;
					}

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
			result.setMessage("Internal Server Error");
			result.setDetails("Internal Server Error");
		}
		return result;
	}
	
	@Override
	public ResponseDTO flightBookingInitiate(FlightBookReq fligthBookReq,List<TravellerFlightDetails> details, MUser user) {
		ResponseDTO result = new ResponseDTO();
		try {
			//JSONObject payload = new JSONObject();
			
			FlightPayment pay = new FlightPayment();
			pay.setSessionId(fligthBookReq.getSessionId());
			pay.setTicketDetails(fligthBookReq.getTicketDetails());
			pay.setPaymentAmount(Double.parseDouble(fligthBookReq.getPaymentAmount()));
			pay.setBaseFare(fligthBookReq.getBaseFare());
			pay.setPaymentMethod(fligthBookReq.getPaymentMethod());
			pay.setSource(fligthBookReq.getSource());
			pay.setDestination(fligthBookReq.getDestination());
			pay.setBaseFare(fligthBookReq.getBaseFare());
			
			/*payload.put("sessionId", fligthBookReq.getSessionId());
			payload.put("ticketDetails", fligthBookReq.getTicketDetails());
			payload.put("paymentAmount", fligthBookReq.getPaymentAmount());
			payload.put("baseFare", fligthBookReq.getBaseFare());
			payload.put("paymentMethod", fligthBookReq.getPaymentMethod());
			payload.put("source", fligthBookReq.getSource());
			payload.put("destination", fligthBookReq.getDestination());
			payload.put("baseFare", fligthBookReq.getBaseFare());*/
			
			ArrayList<FlightTravellerDetailsDTO> travellerDetails = new ArrayList<>();
			for (int j = 0; j < details.size(); j++) {
				//JSONObject travellersDetail=new JSONObject();
				FlightTravellerDetailsDTO travellersDetail = new FlightTravellerDetailsDTO();
				travellersDetail.setfName((details.get(j).getfName()== null) ? "" :details.get(j).getfName());
				travellersDetail.setlName((details.get(j).getlName()== null) ? "" :details.get(j).getlName());
				travellersDetail.setAge((details.get(j).getDob()== null) ? "" :details.get(j).getDob());
				travellersDetail.setGender((details.get(j).getGender()== null) ? "" :details.get(j).getGender());
				travellersDetail.setFare((details.get(j).getFare()== null) ? "" :details.get(j).getFare());
				travellersDetail.setTravellerType((details.get(j).getType()== null) ? "" :details.get(j).getType());
				travellersDetail.setTicketNo((details.get(j).getTicketNo()== null) ? "" :details.get(j).getTicketNo());
				travellerDetails.add(travellersDetail);
			}
			//payload.put("travellerDetails", travellersDetails);
			pay.setTravellerDetails(travellerDetails);
			MService service = getServiceStatus();
			if(service != null && service.getStatus().getValue().equalsIgnoreCase(Status.Active.getValue())) {
				TransactionError error = loadMoneyValidation.validateEaseMyTrip(fligthBookReq.getPaymentAmount(),user.getUsername(),service);
				if(error.isValid()) {
					FlightPaymentResponse orderResponse = flightApi.flightInit(pay,service,user);
					if(orderResponse.isValid()) {
						result.setStatus(ResponseStatus.SUCCESS.getKey());
						result.setMessage(orderResponse.getMessage());
						result.setCode(ResponseStatus.SUCCESS.getValue());
						result.setTransactionRefNo(orderResponse.getTransactionRefNo());
						result.setDetails(orderResponse);
					}else {
						result.setStatus(ResponseStatus.FAILURE.getKey());
						result.setMessage(orderResponse.getMessage());
						result.setCode(ResponseStatus.FAILURE.getValue());
						result.setDetails(orderResponse.getMessage());
					}
				} else {
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setMessage(error.getMessage());
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setDetails(error.getMessage());
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setMessage("Service Inactive");
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setDetails("Service Inactive");
			}			
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			result.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			result.setStatus(ResponseStatus.EXCEPTION_OCCURED);
			result.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return result;
		}
		return result;
	}
	
	@Override
	public String AirBookRQForMobileConecting(AirBookRQ req) {
		try {
			String domain = null;
			String basicAuth=null;
			String key=null;
			String token=null;
			if(MatchMoveUtil.PRODUCTION){
			ERCredentialsManager credentialManager=eRCredentialManager.getCredentials("LIVE");
			if(credentialManager!=null){
				domain=credentialManager.getMdex_base_url();
				basicAuth=credentialManager.getBasic_auth_header();
				key=credentialManager.getMdex_keys();
				token=credentialManager.getMdex_token();
			}
			} else {
				ERCredentialsManager credentialManager=eRCredentialManager.getCredentials("TEST");
				if(credentialManager!=null){
				domain=credentialManager.getMdex_base_url();
				basicAuth=credentialManager.getBasic_auth_header();
				key=credentialManager.getMdex_keys();
				token=credentialManager.getMdex_token();
				}
			}
			
			String payload = req.getJsonRequest();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(domain+MdexConstants.MDEX_TRAVEL_URL+MdexConstants.Flight_AirBookRQ_API);
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtil.getHashBus(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			System.err.println(strResponse);
			return "" + strResponse;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	@Override
	public ResponseDTO flightBookingSucess(FlightBookReq req,List<TravellerFlightDetails> details, MUser user) {
		ResponseDTO result = new ResponseDTO();
		try {
			FlightPayment dto = new FlightPayment();
			dto.setSessionId(req.getSessionId());
			dto.setBookingRefId(req.getBookingRefId());
			dto.setMdexTxnRefNo(req.getMdexTxnRefNo());
			dto.setTicketDetails(req.getTicketDetails());
			dto.setFlightStatus(Status.getEnum(req.getFlightStatus()));			
			dto.setPaymentAmount(Double.parseDouble(req.getPaymentAmount()));
			dto.setPaymentStatus(Status.getEnum(req.getPaymentStatus()));
			dto.setPaymentMethod(req.getPaymentMethod());
			dto.setTxnRefno(req.getTxnRefno());
			dto.setStatus(String.valueOf(req.isSuccess()));
			dto.setEmail(req.getEmail());
			dto.setMobile(req.getMobile());
			dto.setSuccess(req.isSuccess());
			dto.setPnrNo(req.getPnrNo());
			
			
			List<FlightTravellerDetailsDTO> travellersDetails=new ArrayList<>();
			for (int j = 0; j < details.size(); j++) {
				FlightTravellerDetailsDTO travellersDetail=new FlightTravellerDetailsDTO();
				travellersDetail.setfName((details.get(j).getfName()== null) ? "" :details.get(j).getfName());
				travellersDetail.setlName((details.get(j).getlName()== null) ? "" :details.get(j).getlName());
				travellersDetail.setAge((details.get(j).getDob()== null) ? "" :details.get(j).getDob());
				travellersDetail.setGender((details.get(j).getGender()== null) ? "" :details.get(j).getGender());
				travellersDetail.setFare((details.get(j).getFare()== null) ? "" :details.get(j).getFare());
				travellersDetail.setTravellerType((details.get(j).getType()== null) ? "" :details.get(j).getType());
				travellersDetail.setTicketNo((details.get(j).getTicketNo()== null) ? "" :details.get(j).getTicketNo());
				travellersDetails.add(travellersDetail);
			}			
			dto.setTravellerDetails(travellersDetails);
							
			FlightPaymentResponse paymentResponse = flightApi.flightPayment(dto,user);
			if(paymentResponse.isValid()) {
				result.setStatus(ResponseStatus.SUCCESS.getKey());
				result.setMessage(paymentResponse.getMessage());
				result.setCode(ResponseStatus.SUCCESS.getValue());
				result.setDetails(paymentResponse.getMessage());
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(paymentResponse.getMessage());
				result.setDetails(paymentResponse.getMessage());
			}			
			return result;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			result.setStatus(ResponseStatus.EXCEPTION_OCCURED);
			result.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			result.setMessage("Service down temporarily");
			return result;
		}
	}
	
	@Override
	public ResponseDTO flightPaymentGatewaySucess(FlightBookReq req, List<TravellerFlightDetails> details, MUser user) {
		ResponseDTO result = new ResponseDTO();
		try {
			FlightPayment dto = new FlightPayment();
			dto.setSessionId(req.getSessionId());
			dto.setBookingRefId(req.getBookingRefId());
			dto.setMdexTxnRefNo(req.getMdexTxnRefNo());
			dto.setTicketDetails(req.getTicketDetails());
			dto.setFlightStatus(Status.getEnum(req.getFlightStatus()));			
			dto.setPaymentAmount(Double.parseDouble(req.getPaymentAmount()));
			dto.setPaymentStatus(Status.getEnum(req.getPaymentStatus()));
			dto.setPaymentMethod(req.getPaymentMethod());
			dto.setTxnRefno(req.getTxnRefno());
			dto.setStatus(String.valueOf(req.isSuccess()));
			dto.setEmail(req.getEmail());
			dto.setMobile(req.getMobile());
			dto.setSuccess(req.isSuccess());
			dto.setPnrNo(req.getPnrNo());
			
			
			List<FlightTravellerDetailsDTO> travellersDetails=new ArrayList<>();
			for (int j = 0; j < details.size(); j++) {
				FlightTravellerDetailsDTO travellersDetail=new FlightTravellerDetailsDTO();
				travellersDetail.setfName((details.get(j).getfName()== null) ? "" :details.get(j).getfName());
				travellersDetail.setlName((details.get(j).getlName()== null) ? "" :details.get(j).getlName());
				travellersDetail.setAge((details.get(j).getDob()== null) ? "" :details.get(j).getDob());
				travellersDetail.setGender((details.get(j).getGender()== null) ? "" :details.get(j).getGender());
				travellersDetail.setFare((details.get(j).getFare()== null) ? "" :details.get(j).getFare());
				travellersDetail.setTravellerType((details.get(j).getType()== null) ? "" :details.get(j).getType());
				travellersDetail.setTicketNo((details.get(j).getTicketNo()== null) ? "" :details.get(j).getTicketNo());
				travellersDetails.add(travellersDetail);
			}			
			dto.setTravellerDetails(travellersDetails);

			FlightPaymentResponse paymentResponse = flightApi.paymentGateWaySuccess(dto,user);
			if(paymentResponse.isValid()) {
				result.setStatus(ResponseStatus.SUCCESS.getKey());
				result.setMessage(paymentResponse.getMessage());
				result.setCode(ResponseStatus.SUCCESS.getValue());
				result.setDetails(paymentResponse.getMessage());
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(paymentResponse.getMessage());
				result.setDetails(paymentResponse.getMessage());
			}			
			return result;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			result.setStatus(ResponseStatus.EXCEPTION_OCCURED);
			result.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			result.setMessage("Service down temporarily");
			return result;
		}
	}
	
	@Override
	public FlightResponseDTO getAllBookTicketsForMobile(MUser user) {
		FlightResponseDTO resp=new FlightResponseDTO();

		List<FlightTicketMobileDTO> dtos=new ArrayList<>();

		try {
			
			
			List<FlghtTicketResp> allTickets=flightApi.getAllTickets(user);

			if (allTickets != null) {
				
				for (int i = 0; i <allTickets.size(); i++) {

					//JSONObject detail1=detailsArr.getJSONObject(i);
					List<TicketObj> onewayList=new ArrayList<>();
					List<TicketObj> roundwayList=new ArrayList<>();
					
					List<TravellerDetailst> tDetailsList=new ArrayList<>();
					for (int j = 0; j < allTickets.get(i).getTravellerDetails().size(); j++) {
						TravellerDetailst tdDetails=new TravellerDetailst();
						FlightTravellers single=allTickets.get(i).getTravellerDetails().get(i);

						tdDetails.setfName(single.getfName());
						tdDetails.setlName(single.getlName());
						tdDetails.setGender(single.getGender());
						tdDetails.setTicketNo(single.getTicketNo());
						tdDetails.setTravellerType(single.getType());
						tDetailsList.add(tdDetails);
					}

					FlightTicket flightTicket=allTickets.get(i).getFlightTicket();

					FlightTicketMobileDTO fDto=new FlightTicketMobileDTO();

					String source=null;
					String destination=null;
					String journeyDate=null;
					String arrTime=null;
					String deptTime=null;
					fDto.setBookingRefId(flightTicket.getBookingRefId());
					fDto.setMdexTxnRefNo(flightTicket.getMdexTxnRefNo());
					fDto.setPaymentStatus(flightTicket.getPaymentStatus().getValue());
					fDto.setFlightStatus(flightTicket.getFlightStatus().getValue());
					fDto.setTotalFare(String.valueOf(flightTicket.getPaymentAmount()));
					fDto.setPaymentMethod(flightTicket.getPaymentMethod());
					fDto.setTravellerDetails(tDetailsList);
					String txn=flightTicket.getTransaction().getTransactionRefNo();
					if (txn!=null) {
						if (!txn.equalsIgnoreCase("null")) {							
							String txnRefNo=flightTicket.getTransaction().getTransactionRefNo();
							fDto.setTransactionRefNo(txnRefNo);
						}
					}

					JSONObject tickets=null;
					String ticketsStr=flightTicket.getTicketDetails();
					if (!ticketsStr.equalsIgnoreCase("null") && !ticketsStr.equalsIgnoreCase("nullnull")) {
						tickets=new JSONObject(ticketsStr);

						JSONObject ticket=tickets.getJSONObject("Tickets");
						JSONArray oneway=ticket.getJSONArray("Oneway");
						JSONArray roundway=ticket.getJSONArray("Roundway");

						if (oneway.length()>0) {

							for (int j = 0; j < oneway.length(); j++) {

								TicketObj ticketObj=new TicketObj(); 

								String onewayObjStr=oneway.get(j).toString();
								String onewayObjStrs=oneway.get(j).toString();

								JSONObject onewayObj=new JSONObject(onewayObjStr);
								JSONObject onewayObjls=new JSONObject(onewayObjStrs);

								ticketObj.setOrigin(onewayObj.getString("origin"));
								ticketObj.setArrivalDate(onewayObj.getString("arrivalDate"));
								ticketObj.setDepartureDate(onewayObj.getString("departureDate"));
								ticketObj.setArrivalTime(onewayObj.getString("arrivalTime"));
								ticketObj.setDestination(onewayObjls.getString("destination"));
								ticketObj.setDepartureTime(onewayObjls.getString("departureTime"));
								ticketObj.setJourneyTime(onewayObj.getString("journeyTime"));
								ticketObj.setCabin(onewayObj.getString("cabin"));
								ticketObj.setAirlineName(onewayObj.getString("airlineName"));
								ticketObj.setDuration(onewayObj.getString("duration"));
								ticketObj.setFlightNumber(onewayObj.getString("flightNumber"));
								onewayList.add(ticketObj);
							}
							TicketObj fsTicketObj=onewayList.get(0);
							TicketObj lsTicketObj=onewayList.get(onewayList.size()-1);
							source=fsTicketObj.getOrigin();
							destination=lsTicketObj.getDestination();
							journeyDate=fsTicketObj.getDepartureDate();
							arrTime=lsTicketObj.getArrivalTime();
							deptTime=lsTicketObj.getDepartureTime();
						}
						fDto.setSource(source);
						fDto.setDestination(destination);
						fDto.setArrTime(arrTime);
						fDto.setJourneyDate(journeyDate);
						fDto.setDeptTime(deptTime);
						
						if (roundway.length()>0) {
							for (int j = 0; j < roundway.length(); j++) {

								TicketObj ticketObj=new TicketObj(); 

								String roundwayObjStr=roundway.get(j).toString();
								String roundwayObjStrs=roundway.get(j).toString();
								JSONObject roundwayObj=new JSONObject(roundwayObjStr);
								JSONObject roundwayObjls=new JSONObject(roundwayObjStrs);
								
								ticketObj.setOrigin(roundwayObj.getString("origin"));
								ticketObj.setDestination(roundwayObjls.getString("destination"));
								ticketObj.setArrivalDate(roundwayObjls.getString("arrivalDate"));
								ticketObj.setDepartureDate(roundwayObj.getString("departureDate"));
								ticketObj.setArrivalTime(roundwayObjls.getString("arrivalTime"));
								ticketObj.setDepartureTime(roundwayObjls.getString("departureTime"));
								ticketObj.setJourneyTime(roundwayObjls.getString("journeyTime"));
								ticketObj.setCabin(roundwayObjls.getString("cabin"));
								ticketObj.setAirlineName(roundwayObjls.getString("airlineName"));
								ticketObj.setDuration(roundwayObjls.getString("duration"));
								ticketObj.setFlightNumber(roundwayObjls.getString("flightNumber"));
								roundwayList.add(ticketObj);
								TicketObj fsTicketObj=roundwayList.get(0);
								TicketObj lsTicketObj=roundwayList.get(roundwayList.size()-1);
								source=fsTicketObj.getOrigin();
								destination=lsTicketObj.getDestination();
								journeyDate=fsTicketObj.getDepartureDate();
								arrTime=lsTicketObj.getArrivalTime();
								deptTime=lsTicketObj.getDepartureTime();
							}
						}

						fDto.setSourceReturn(source);
						fDto.setDestinationReturn(destination);
						fDto.setArrTimeReturn(arrTime);
						fDto.setJourneyDateReturn(journeyDate);
						fDto.setDeptTimeReturn(deptTime);
						fDto.setOneway(onewayList);
						fDto.setRoundway(roundwayList);
						dtos.add(fDto);
					}

					resp.setDetails(dtos);
				}
			}
			resp.setCode("S00");
			resp.setMessage("Get All Book Tickets");
			resp.setStatus(ResponseStatus.SUCCESS.getKey());

		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;
	}

	@Override
	public FlightResponseDTO saveAirLineNamesInDb() {

		String domain = null;
		String basicAuth=null;
		String key=null;
		String token=null;
		if(MatchMoveUtil.PRODUCTION){
		ERCredentialsManager credentialManager=eRCredentialManager.getCredentials("LIVE");
		if(credentialManager!=null){
			domain=credentialManager.getMdex_base_url();
			basicAuth=credentialManager.getBasic_auth_header();
			key=credentialManager.getMdex_keys();
			token=credentialManager.getMdex_token();
		}
		} else {
			ERCredentialsManager credentialManager=eRCredentialManager.getCredentials("TEST");
			if(credentialManager!=null){
			domain=credentialManager.getMdex_base_url();
			basicAuth=credentialManager.getBasic_auth_header();
			key=credentialManager.getMdex_keys();
			token=credentialManager.getMdex_token();
			}
		}
		
		
		FlightResponseDTO resp=new FlightResponseDTO();
		List<AirLineNames>airLineNames=new ArrayList<>(); 
		try {
			JSONObject payload = new JSONObject();
			payload.put("clientIp", Inet4Address.getLocalHost().getHostAddress());
			payload.put("cllientKey", key);
			payload.put("clientToken", token);
			payload.put("clientApiName", "Flight Name Request");
			Client client = Client.create();
			WebResource webResource = client.resource(domain+MdexConstants.MDEX_TRAVEL_URL+MdexConstants.Flight_SOURCE);

			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtil.getHashBus(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			//System.err.println("strResponse: "+strResponse);
			JSONObject jobj=new JSONObject(strResponse);
			final String code=jobj.getString("code");
			final String status=jobj.getString("status");
			final String message=jobj.getString("message");

			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
				JSONArray strDetails= jobj.getJSONArray("details");
				if (strDetails!=null) {
					
					String airportName=null;
					String cityCode=null;
					String cityName=null;
					String country=null;					
					for (int i = 0; i < strDetails.length(); i++) {
						JSONObject cList = strDetails.getJSONObject(i);
						airportName=cList.getString("airportName");
						cityCode=cList.getString("cityCode");
						cityName=cList.getString("cityName");
						country=cList.getString("country");
						FlightAirLineList list = flightListRepository.getCityByCode(cityCode);
						if(list == null) {
							list = new FlightAirLineList();
							list.setCityCode(cityCode);
							list.setCityName(cityName);
							list.setCountry(country);
							list.setAirportName(airportName);
							flightListRepository.save(list);
						}
						AirLineNames  dto=new AirLineNames();
						dto.setAirportName(airportName);
						dto.setCityCode(cityCode);
						dto.setCityName(cityName);
						dto.setCountry(country);
						airLineNames.add(dto);
					}

					resp.setDetails(airLineNames);

				}
				resp.setCode(code);
				resp.setMessage(message);
				resp.setStatus(status);
			}else{
				resp.setCode(code);
				resp.setMessage(message);
				resp.setStatus(status);
			}

		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;
	}
	
	@Override
	public ResponseDTO getFlightDetails() {
		ResponseDTO result = new ResponseDTO();
		try {
			List<FlightTicket> list=flightApi.getFlightDetailForAdmin();
			if(list!=null){
				result.setCode("S00");
				result.setMessage("Flight Details received");	
				result.setList(list);
			}else{
				result.setCode("F00");
				result.setMessage("no flight details found");			
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@Override
	public ResponseDTO getFlightDetailPost(PagingDTO dto) {
		ResponseDTO result = new ResponseDTO();
		try {
			List<FlightTicket> list=flightApi.getFlightDetailForAdminDate(dto);
			if(list != null) {
				result.setCode("S00");
				result.setMessage("Flight Details received");	
				result.setList(list);
			} else {
				result.setCode("F00");
				result.setMessage("no flight details found");
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
		
	@Override
	public FlightResponseDTO getSingleFlightTicketDetails(String sessionId, long flightId) {
		FlightResponseDTO resp = new FlightResponseDTO();
		List<TravellerFlightDetails> travellerFlightDetails = new ArrayList<>();
		FlightTicketResp flightTicketResp = new FlightTicketResp();		
		try {
			List<FlightTravellers> detail=flightApi.getFlightTravellersForAdmin(flightId);
			TicketsResp detail2=flightApi.getFlightTravellersNameForAdmin(flightId);
			for (int i = 0; i < detail.size(); i++) {
				TravellerFlightDetails tDetails = new TravellerFlightDetails();
				tDetails.setfName(detail.get(i).getfName());
				tDetails.setlName(detail.get(i).getlName());
				tDetails.setGender(detail.get(i).getGender());
				tDetails.setType(detail.get(i).getType());
				tDetails.setTicketNo(detail.get(i).getTicketNo());

				travellerFlightDetails.add(tDetails);
			}
			if(detail.size() > 0) {
				for(int i = 0; i < detail.size(); i++) {
					FlightTicket flDetails = detail.get(i).getFlightTicket();
					String ticketsStr = detail.get(i).getFlightTicket().getTicketDetails();
					if (!ticketsStr.equalsIgnoreCase("null") && !ticketsStr.equalsIgnoreCase("nullnull")) {
						TicketsResp ticketDeatilsDTO = new TicketsResp();
						ObjectMapper mapper = new ObjectMapper();
						ticketDeatilsDTO = mapper.readValue(ticketsStr, TicketsResp.class);
						logger.info("ticketDeatilsDTO:: " + ticketDeatilsDTO);
						if(detail2 != null) {
							List<FlightTicketDeatilsDTO> job = detail2.getTickets().getOneway();
							for (int j = 0; j < job.size(); j++) {
								ticketDeatilsDTO.getTickets().getOneway().get(i).setDestinationName(job.get(i).getDestinationName());
								ticketDeatilsDTO.getTickets().getOneway().get(i).setSourceName(job.get(i).getSourceName());
							}
							List<FlightTicketDeatilsDTO> job2 = detail2.getTickets().getRoundway();
							for(int k = 0; k < job2.size(); k++) {
								ticketDeatilsDTO.getTickets().getRoundway().get(i).setDestinationName(job2.get(i).getDestinationName());
								ticketDeatilsDTO.getTickets().getRoundway().get(i).setSourceName(job2.get(i).getSourceName());
							}
						}
						flightTicketResp.setTicketsResp(ticketDeatilsDTO);
					}
					flightTicketResp.setBookingRefNo(flDetails.getBookingRefId());
					flightTicketResp.setEmail(flDetails.getEmail());
					
					Date date = flDetails.getCreated();
					String dateStr = dateFormat.format(date);
					flightTicketResp.setCreated(dateStr);
				}
				flightTicketResp.setTravellerDetails(travellerFlightDetails);
			}
			resp.setCode("S00");
			resp.setStatus("Success");
			resp.setMessage("Success");
			resp.setDetails(flightTicketResp);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
	
	@Override
	public FlightCancelableResp isCancellableFlight(FligthcancelableReq dto) {
		FlightCancelableResp resp = new FlightCancelableResp();
		try {
			String domain = null;
			String basicAuth=null;
			String key=null;
			String token=null;
			if(MatchMoveUtil.PRODUCTION) {
			ERCredentialsManager credentialManager=eRCredentialManager.getCredentials("LIVE");
			if(credentialManager!=null){
				domain=credentialManager.getMdex_base_url();
				basicAuth=credentialManager.getBasic_auth_header();
				key=credentialManager.getMdex_keys();
				token=credentialManager.getMdex_token();
			}
			} else {
				ERCredentialsManager credentialManager=eRCredentialManager.getCredentials("TEST");
				if(credentialManager!=null){
				domain=credentialManager.getMdex_base_url();
				basicAuth=credentialManager.getBasic_auth_header();
				key=credentialManager.getMdex_keys();
				token=credentialManager.getMdex_token();
				}
			}
			
			JSONObject payload = new JSONObject();
			payload.put("clientIp", MdexConstants.MDEX_CLIENTIP);
			payload.put("cllientKey", key);
			payload.put("clientToken", token);
			payload.put("clientApiName", "Ewire");
			payload.put("subUserId", dto.getSubUserId());
			payload.put("transactionScreenId", dto.getTransactionScreenId());
			
			Client client = Client.create();
			WebResource webResource = client.resource(domain+MdexConstants.MDEX_TRAVEL_URL+MdexConstants.FLIGHT_ISCANCELABLE_TICKET);
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtil.getHashBus(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setCode("F00");
				resp.setMessage("Service Unavailable");
				resp.setStatus(ResponseStatus.FAILURE.getValue());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					JSONObject jObj = new JSONObject(strResponse);
					String code = jObj.getString("code");

					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
						resp.setStatus(ResponseStatus.SUCCESS.getKey());
						resp.setCode(ResponseStatus.SUCCESS.getValue());
						resp.setMessage("Flight cancellable response");
					} else {
						resp.setStatus(ResponseStatus.FAILURE.getKey());
						resp.setCode(ResponseStatus.FAILURE.getValue());
					}
					resp.setMessage("Get Cancellation Details");
					resp.setDetails(strResponse);
				} else {
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus(ResponseStatus.FAILURE.getValue());
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
}
