package com.msscard.app.api.impl;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.context.request.RequestContextHolder;

import com.ewire.errormessages.ErrorMessage;
import com.msscard.app.api.IMailSenderApi;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.ISMSSenderApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.ChangePasswordRequest;
import com.msscard.app.model.request.DateDTO;
import com.msscard.app.model.request.LoginDTO;
import com.msscard.app.model.request.LoginResponse;
import com.msscard.app.model.request.MatchMoveCreateCardRequest;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.request.UserKycRequest;
import com.msscard.app.model.response.CommonResponse;
import com.msscard.app.model.response.PiChartResponseDTO;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.sms.util.SMSAccount;
import com.msscard.app.sms.util.SMSTemplete;
import com.msscard.app.sms.util.SMSUtil;
import com.msscard.entity.ApiVersion;
import com.msscard.entity.BulkCardAssignmentGroup;
import com.msscard.entity.BulkRegister;
import com.msscard.entity.BusTicket;
import com.msscard.entity.CorporateAgentDetails;
import com.msscard.entity.Donation;
import com.msscard.entity.GroupBulkKyc;
import com.msscard.entity.GroupBulkRegister;
import com.msscard.entity.GroupDetails;
import com.msscard.entity.GroupOtpMobile;
import com.msscard.entity.GroupSms;
import com.msscard.entity.LoadCardOtp;
import com.msscard.entity.LoginLog;
import com.msscard.entity.MBulkRegister;
import com.msscard.entity.MKycDetail;
import com.msscard.entity.MLocationDetails;
import com.msscard.entity.MMCards;
import com.msscard.entity.MOperator;
import com.msscard.entity.MPQAccountDetails;
import com.msscard.entity.MPQAccountType;
import com.msscard.entity.MService;
import com.msscard.entity.MServiceType;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.entity.MUserDetails;
import com.msscard.entity.MpinLogs;
import com.msscard.entity.PartnerDetails;
import com.msscard.entity.PhysicalCardDetails;
import com.msscard.entity.PromoCode;
import com.msscard.entity.PromoCodeRequest;
import com.msscard.entity.UserSession;
import com.msscard.entity.VersionLogs;
import com.msscard.model.AddGroupRequest;
import com.msscard.model.ApiVersionDTO;
import com.msscard.model.DonationDTO;
import com.msscard.model.GroupDetailDTO;
import com.msscard.model.GroupDetailsListDTO;
import com.msscard.model.MPinChangeDTO;
import com.msscard.model.MpinDTO;
import com.msscard.model.PagingDTO;
import com.msscard.model.PromoCodeDTO;
import com.msscard.model.RegisterDTO;
import com.msscard.model.RequestDTO;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Status;
import com.msscard.model.SuperAdminDTO;
import com.msscard.model.UpgradeAccountDTO;
import com.msscard.model.UserDTO;
import com.msscard.model.UserDataRequestDTO;
import com.msscard.model.UserType;
import com.msscard.model.error.AuthenticationError;
import com.msscard.model.error.LoginError;
import com.msscard.repositories.ApiVersionRepository;
import com.msscard.repositories.BulkCardAssignmentGroupRepository;
import com.msscard.repositories.BulkRegisterRepository;
import com.msscard.repositories.BusTicketRepository;
import com.msscard.repositories.CorporateAgentDetailsRepository;
import com.msscard.repositories.DonationRepository;
import com.msscard.repositories.GroupBulkKycRepository;
import com.msscard.repositories.GroupBulkRegisterRepository;
import com.msscard.repositories.GroupDetailsRepository;
import com.msscard.repositories.GroupFileCatalogueRepository;
import com.msscard.repositories.GroupOtpRepository;
import com.msscard.repositories.GroupSmsRepository;
import com.msscard.repositories.LoadCardOtpRepository;
import com.msscard.repositories.LoginLogRepository;
import com.msscard.repositories.MBulkRegisterRepository;
import com.msscard.repositories.MKycRepository;
import com.msscard.repositories.MLocationDetailRepository;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MOperatorRepository;
import com.msscard.repositories.MPQAccountDetailRepository;
import com.msscard.repositories.MPQAccountTypeRepository;
import com.msscard.repositories.MPinLogsRepository;
import com.msscard.repositories.MServiceRepository;
import com.msscard.repositories.MServiceTypeRepository;
import com.msscard.repositories.MTransactionRepository;
import com.msscard.repositories.MUserDetailRepository;
import com.msscard.repositories.MUserRespository;
import com.msscard.repositories.MatchMoveWalletRepository;
import com.msscard.repositories.OtpRepository;
import com.msscard.repositories.PartnerDetailsRepository;
import com.msscard.repositories.PhysicalCardDetailRepository;
import com.msscard.repositories.PromoCodeRepository;
import com.msscard.repositories.PromoCodeRequestRepository;
import com.msscard.repositories.TwoMinBlockRepository;
import com.msscard.repositories.UserSessionRepository;
import com.msscard.repositories.VersionLogsRepository;
import com.msscard.util.Authorities;
import com.msscard.util.CommonUtil;
import com.msscard.util.ConvertUtil;
import com.msscard.util.MailTemplate;
import com.msscard.validation.CommonValidation;
import com.msscards.session.SessionLoggingStrategy;

public class UserApiImpl implements IUserApi {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	DateFormat formatter1 = new SimpleDateFormat("MM/dd/yyyy");

	private final PasswordEncoder passwordEncoder;
	private final MUserRespository mUserRespository;
	private final MUserDetailRepository mUserDetailRepository;
	private final MPQAccountDetailRepository mPQAccountDetailRepository;
	private final MPQAccountTypeRepository mPQAccountTypeRespository;
	private final MLocationDetailRepository mLocationDetailRespository;
	private final ISMSSenderApi smsSenderApi;
	private final LoginLogRepository loginLogRepository;
	private final MMCardRepository mmCardRepository;
	private final MTransactionRepository mTransactionRepository;
	private final MServiceRepository mServiceRepository;
	private final MKycRepository mKycRepository;
	private final MBulkRegisterRepository mBulkRegisterRepository;
	private final PhysicalCardDetailRepository physicalCardDetailRepository;
	private final PromoCodeRepository promoCodeRepository;
	private final PromoCodeRequestRepository promoCodeRequestRepository;
	private final ApiVersionRepository apiVersionRepository;
	private final VersionLogsRepository versionLogsRepository;
	private final IMatchMoveApi matchMoveApi;
	private final CorporateAgentDetailsRepository corporateAgentDetailsRepository;
	private final BulkRegisterRepository bulkRegisterRepository;
	private final PartnerDetailsRepository partnerDetailsRepository;
	private final MPinLogsRepository mPinLogsRepository;
	private final GroupDetailsRepository groupDetailsRepository;
	private final SessionLoggingStrategy sls;
	private final AuthenticationManager authenticationManager;
	private final UserSessionRepository userSessionRepository;
	private final DonationRepository donationRepository;
	private final MatchMoveWalletRepository matchMoveWalletRepository;
	private final MMCardRepository mMCardRepository;
	private final OtpRepository otpRepository;
	private final GroupOtpRepository groupOtpRepository;
	private final GroupSmsRepository groupSmsRepository;
	private final BusTicketRepository busTicketRepository;
	private final TwoMinBlockRepository twoMinBlockRepository;
	private final IMailSenderApi mailSenderApi;
	private final LoadCardOtpRepository loadCardOtpRepository;
	private final MOperatorRepository mOperatorRepository;
	private final MServiceTypeRepository mServiceTypeRepository;
	private final GroupBulkRegisterRepository groupBulkRegisterRepository;
	private final BulkCardAssignmentGroupRepository bulkCardAssignmentGroupRepository;
	private final GroupFileCatalogueRepository groupFileCatalogueRepository;
	private final GroupBulkKycRepository groupBulkKycRepository;

	private final SimpleDateFormat date = new SimpleDateFormat("yyyy/MM/dd");
	private final SimpleDateFormat date1 = new SimpleDateFormat("mm/dd/yyyy");
	private final SimpleDateFormat dateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public UserApiImpl(PasswordEncoder passwordEncoder, MUserRespository mUserRespository,
			MUserDetailRepository mUserDetailRepository, MPQAccountDetailRepository mPQAccountDetailRepository,
			MPQAccountTypeRepository mPQAccountTypeRespository, MLocationDetailRepository mLocationDetailRespository,
			ISMSSenderApi smsSenderApi, LoginLogRepository loginLogRepository, MMCardRepository mmCardRepository,
			MTransactionRepository mTransactionRepository, MServiceRepository mServiceRepository,
			MKycRepository mKycRepository, MBulkRegisterRepository mBulkRegisterRepository,
			PhysicalCardDetailRepository physicalCardDetailRepository, PromoCodeRepository promoCodeRepository,
			PromoCodeRequestRepository promoCodeRequestRepository, ApiVersionRepository apiVersionRepository,
			VersionLogsRepository versionLogsRepository, IMatchMoveApi matchMoveApi, SessionLoggingStrategy sls,
			CorporateAgentDetailsRepository corporateAgentDetailsRepository,
			GroupDetailsRepository groupDetailsRepository, BulkRegisterRepository bulkRegisterRepository,
			PartnerDetailsRepository partnerDetailsRepository, MPinLogsRepository mPinLogsRepository,
			AuthenticationManager authenticationManager, UserSessionRepository userSessionRepository,
			DonationRepository donationRepository, MatchMoveWalletRepository matchMoveWalletRepository,
			MMCardRepository mMCardRepository, OtpRepository otpRepository, GroupOtpRepository groupOtpRepository,
			GroupSmsRepository groupSmsRepository, BusTicketRepository busTicketRepository,
			TwoMinBlockRepository twoMinBlockRepository, IMailSenderApi mailSenderApi,
			LoadCardOtpRepository loadCardOtpRepository, MOperatorRepository mOperatorRepository,
			MServiceTypeRepository mServiceTypeRepository, GroupBulkRegisterRepository groupBulkRegisterRepository,
			BulkCardAssignmentGroupRepository bulkCardAssignmentGroupRepository,
			GroupFileCatalogueRepository groupFileCatalogueRepository, GroupBulkKycRepository groupBulkKycRepository) {
		super();
		this.passwordEncoder = passwordEncoder;
		this.mUserRespository = mUserRespository;
		this.mUserDetailRepository = mUserDetailRepository;
		this.mPQAccountDetailRepository = mPQAccountDetailRepository;
		this.mPQAccountTypeRespository = mPQAccountTypeRespository;
		this.mLocationDetailRespository = mLocationDetailRespository;
		this.smsSenderApi = smsSenderApi;
		this.loginLogRepository = loginLogRepository;
		this.mmCardRepository = mmCardRepository;
		this.mTransactionRepository = mTransactionRepository;
		this.mServiceRepository = mServiceRepository;
		this.mKycRepository = mKycRepository;
		this.mBulkRegisterRepository = mBulkRegisterRepository;
		this.physicalCardDetailRepository = physicalCardDetailRepository;
		this.promoCodeRepository = promoCodeRepository;
		this.promoCodeRequestRepository = promoCodeRequestRepository;
		this.apiVersionRepository = apiVersionRepository;
		this.versionLogsRepository = versionLogsRepository;
		this.matchMoveApi = matchMoveApi;
		this.corporateAgentDetailsRepository = corporateAgentDetailsRepository;
		this.bulkRegisterRepository = bulkRegisterRepository;
		this.partnerDetailsRepository = partnerDetailsRepository;
		this.mPinLogsRepository = mPinLogsRepository;
		this.groupDetailsRepository = groupDetailsRepository;
		this.sls = sls;
		this.authenticationManager = authenticationManager;
		this.userSessionRepository = userSessionRepository;
		this.donationRepository = donationRepository;
		this.matchMoveWalletRepository = matchMoveWalletRepository;
		this.mMCardRepository = mMCardRepository;
		this.otpRepository = otpRepository;
		this.groupOtpRepository = groupOtpRepository;
		this.groupSmsRepository = groupSmsRepository;
		this.busTicketRepository = busTicketRepository;
		this.twoMinBlockRepository = twoMinBlockRepository;
		this.mailSenderApi = mailSenderApi;
		this.loadCardOtpRepository = loadCardOtpRepository;
		this.mOperatorRepository = mOperatorRepository;
		this.mServiceTypeRepository = mServiceTypeRepository;
		this.groupBulkRegisterRepository = groupBulkRegisterRepository;
		this.bulkCardAssignmentGroupRepository = bulkCardAssignmentGroupRepository;
		this.groupFileCatalogueRepository = groupFileCatalogueRepository;
		this.groupBulkKycRepository = groupBulkKycRepository;
	}

	@Override
	public void saveUser(RegisterDTO dto) throws Exception {
		// TODO Auto-generated method stub

		MUser user = new MUser();
		MUserDetails userDetail = new MUserDetails();
		userDetail.setAddress(dto.getAddress());
		userDetail.setContactNo(dto.getContactNo());
		userDetail.setFirstName(dto.getFirstName());
		userDetail.setLastName(dto.getLastName());
		userDetail.setMiddleName(dto.getMiddleName());
		userDetail.setEmail(dto.getEmail());

		// userDetail.setDateOfBirth(formatter.parse(dto.getDateOfBirth()));
		if (!String.valueOf(dto.getUserType().Corporate).equalsIgnoreCase("Corporate")) {
			userDetail.setDateOfBirth(formatter1.parse(dto.getDateOfBirth()));
		}
		userDetail.setBrand(dto.getBrand());
		userDetail.setModel(dto.getModel());
		userDetail.setImeiNo(dto.getImeiNo());
		userDetail.setIdType(dto.getIdType());
		userDetail.setIdNo(dto.getIdNo());
		userDetail.setGroupStatus(Status.Inactive.getValue());
		userDetail.setRemark(dto.getRemark());
		if (dto.isHasAadhar()) {
			userDetail.setAadharNo(dto.getAadharNo());
		}

		if (dto.getAndroidDeviceID() != null) {
			System.err.println(dto.getAndroidDeviceID());
			user.setAndroidDeviceID(dto.getAndroidDeviceID());
			MUser androidUser = mUserRespository.findByAndroidDeviceId(dto.getAndroidDeviceID(), true);
			if (androidUser != null) {
				user.setDeviceLocked(false);
			} else {
				user.setDeviceLocked(true);
			}
		}
		MLocationDetails location = null;
		if (dto.getUserType().equals(UserType.Admin) || dto.getUserType().equals(UserType.Merchant)
				|| dto.getUserType().equals(UserType.Corporate)) {
			if (dto.getUserType().equals(UserType.Admin)) {
				user.setAuthority(Authorities.ADMINISTRATOR + "," + Authorities.AUTHENTICATED);
			} else if (dto.getUserType().equals(UserType.Merchant)) {
				user.setAuthority(Authorities.MERCHANT + "," + Authorities.AUTHENTICATED);
			} else if (dto.getUserType().equals(UserType.Corporate)) {
				user.setAuthority(Authorities.CORPORATE + "," + Authorities.AUTHENTICATED);
			}
			String hashedPassword = passwordEncoder.encode(dto.getPassword());
			user.setPassword(hashedPassword);
			// history.setPassword(hashedPassword);
			user.setFullyFilled(true);
			user.setMobileStatus(Status.Active);
			user.setEmailStatus(Status.Active);
			user.setUsername(dto.getUsername().toLowerCase());
			user.setUserType(dto.getUserType());
			GroupDetails group = groupDetailsRepository.getGroupDetailsByContact(dto.getGroup());
			if (group != null) {
				user.setGroupDetails(group);
			}

		} else {
			// location=mLocationDetailRespository.findLocationByPin(dto.getLocationCode());
			String hashedPassword = passwordEncoder.encode(dto.getPassword());
			// userDetail.setLocation(location);
			if (dto.isWeb()) {

			}

			user.setAuthority(Authorities.USER + "," + Authorities.AUTHENTICATED);
			user.setPassword(passwordEncoder.encode(dto.getPassword()));
			user.setMobileStatus(Status.Inactive);
			user.setEmailStatus(Status.Active);
			// history.setPassword(hashedPassword);
			user.setUsername(dto.getUsername().toLowerCase());
			user.setUserType(dto.getUserType());
			user.setMobileToken(CommonUtil.generateSixDigitNumericString());
			user.setEmailToken("E" + System.currentTimeMillis());
			user.setFullyFilled(true);

		}
		user.setUserDetail(userDetail);
		MPQAccountDetails pqAccountDetail = new MPQAccountDetails();
		if (dto.getUserType().equals(UserType.Corporate)) {
			MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("SUPERKYC");
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setBranchCode(dto.getBranchCode());
			pqAccountDetail.setAccountType(nonKYCAccountType);
			user.setAccountDetail(pqAccountDetail);
		} else {
			MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("NONKYC");
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setBranchCode(dto.getBranchCode());
			pqAccountDetail.setAccountType(nonKYCAccountType);
			user.setAccountDetail(pqAccountDetail);
		}
		if (dto.getUserType().equals(UserType.Corporate)) {

		}
		MUser tempUser = mUserRespository.findByUsernameAndStatus(user.getUsername(), Status.Inactive);
		if (tempUser == null) {
			mUserDetailRepository.save(userDetail);
			pqAccountDetail.setAccountNumber(SMSUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
			mPQAccountDetailRepository.save(pqAccountDetail);
			mUserRespository.save(user);
			if (dto.getUserType().equals(UserType.Corporate)) {
				/*
				 * MPQAccountType nonKYCAccountType =
				 * mPQAccountTypeRespository.findByCode("SUPERKYC"); MKycDetail kyc = new
				 * MKycDetail(); kyc.setAadharImage(dto.getAadharImagePath());
				 * kyc.setPancardImage(dto.getPanCardImagePath());
				 * kyc.setAccountNo(dto.getBankAccountNo()); kyc.setIfscCode(dto.getIfscCode());
				 * kyc.setUserType(UserType.Corporate); kyc.setAccountType(nonKYCAccountType);
				 * kyc.setUser(user); mKycRepository.save(kyc);
				 */
				CorporateAgentDetails corporateDetails = new CorporateAgentDetails();
				corporateDetails.setCorporateName(dto.getCorporateName());
				corporateDetails.setCorporateLogo1(dto.getCorporateLogo1Path());
				corporateDetails.setCorporateLogo2(dto.getCorporateLogo2Path());
				corporateDetails.setServiceStatus(true);
				corporateDetails.setBankAccountNo(dto.getBankAccountNo());
				corporateDetails.setIfscCode(dto.getIfscCode());
				corporateDetails.setCorporate(user);
				corporateDetails.setContactPersonNumber(dto.getContactNo());
				corporateAgentDetailsRepository.save(corporateDetails);

			}

		} else {

			mUserRespository.delete(tempUser);
			mUserDetailRepository.delete(tempUser.getUserDetail());
			user.setAccountDetail(tempUser.getAccountDetail());
			mUserDetailRepository.save(userDetail);
			mUserRespository.save(user);
			if (dto.getUserType().equals(UserType.Corporate)) {
				/*
				 * MPQAccountType nonKYCAccountType =
				 * mPQAccountTypeRespository.findByCode("SUPERKYC"); MKycDetail kyc = new
				 * MKycDetail(); kyc.setAadharImage(dto.getAadharImagePath());
				 * kyc.setPancardImage(dto.getPanCardImagePath());
				 * kyc.setAccountNo(dto.getBankAccountNo()); kyc.setIfscCode(dto.getIfscCode());
				 * kyc.setUserType(UserType.Corporate); kyc.setAccountType(nonKYCAccountType);
				 * kyc.setUser(user); mKycRepository.save(kyc);
				 */
				CorporateAgentDetails corporateDetails = new CorporateAgentDetails();
				corporateDetails.setCorporateName(dto.getCorporateName());
				corporateDetails.setCorporateLogo1(dto.getCorporateLogo1Path());
				corporateDetails.setCorporateLogo2(dto.getCorporateLogo2Path());
				corporateDetails.setServiceStatus(true);
				corporateDetails.setBankAccountNo(dto.getBankAccountNo());
				corporateDetails.setIfscCode(dto.getIfscCode());
				corporateDetails.setCorporate(user);
				corporateAgentDetailsRepository.save(corporateDetails);
			}
		}
		if (dto.getUserType().equals(UserType.User)) {
			// mailSenderApi.sendNoReplyMail("Email
			// Verification",MailTemplate.VERIFICATION_EMAIL, user,null);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.VERIFICATION_MOBILE, user, null);
		}

	}

	// bulk register corporate

	@Override
	public void saveUserCorpUser(RegisterDTO dto) throws Exception {
		// TODO Auto-generated method stub

		try {
			MUser user = new MUser();
			user.setCorporateUser(true);
			MUserDetails userDetail = new MUserDetails();
			userDetail.setAddress(dto.getAddress());
			userDetail.setContactNo(dto.getContactNo());
			userDetail.setFirstName(dto.getFirstName());
			userDetail.setLastName(dto.getLastName());
			userDetail.setMiddleName(dto.getMiddleName());
			userDetail.setEmail(dto.getEmail());
			userDetail.setDateOfBirth(formatter.parse(dto.getDateOfBirth()));
			userDetail.setRemark(dto.getRemark());
			userDetail.setGroupStatus(Status.Active.getValue());

			// userDetail.setDateOfBirth(formatter.parse(dto.getDateOfBirth()));
			/*
			 * if(!String.valueOf(dto.getUserType().Corporate).equalsIgnoreCase("Corporate")
			 * ){ userDetail.setDateOfBirth(formatter1.parse(dto.getDateOfBirth())); }
			 */
			/*
			 * userDetail.setBrand(dto.getBrand()); userDetail.setModel(dto.getModel());
			 * userDetail.setImeiNo(dto.getImeiNo());
			 */
			userDetail.setIdType(dto.getIdType());
			userDetail.setIdNo(dto.getIdNo());
			/*
			 * if (dto.isHasAadhar()) { userDetail.setAadharNo(dto.getAadharNo()); }
			 */
			GroupDetails group = groupDetailsRepository.getGroupDetailsByEmail(dto.getGroup());
			if (group != null) {
				user.setGroupDetails(group);
			} else {
				GroupDetails g = groupDetailsRepository.getGroupDetailsByContact("None");
				user.setGroupDetails(g);
			}
			if (dto.getAndroidDeviceID() != null) {
				System.err.println(dto.getAndroidDeviceID());
				user.setAndroidDeviceID(dto.getAndroidDeviceID());
				MUser androidUser = mUserRespository.findByAndroidDeviceId(dto.getAndroidDeviceID(), true);
				if (androidUser != null) {
					user.setDeviceLocked(false);
				} else {
					user.setDeviceLocked(true);
				}
			}
			MLocationDetails location = null;
			if (dto.getUserType().equals(UserType.Admin) || dto.getUserType().equals(UserType.Merchant)
					|| dto.getUserType().equals(UserType.Corporate)) {
				if (dto.getUserType().equals(UserType.Admin)) {
					user.setAuthority(Authorities.ADMINISTRATOR + "," + Authorities.AUTHENTICATED);
				} else if (dto.getUserType().equals(UserType.Merchant)) {
					user.setAuthority(Authorities.MERCHANT + "," + Authorities.AUTHENTICATED);
				} else if (dto.getUserType().equals(UserType.Corporate)) {
					user.setAuthority(Authorities.CORPORATE + "," + Authorities.AUTHENTICATED);
				}
				String hashedPassword = passwordEncoder.encode(dto.getPassword());
				user.setPassword(hashedPassword);
				// history.setPassword(hashedPassword);
				user.setFullyFilled(true);
				user.setMobileStatus(Status.Active);
				user.setEmailStatus(Status.Active);
				user.setUsername(dto.getUsername().toLowerCase());
				user.setUserType(dto.getUserType());

			} else {
				// location=mLocationDetailRespository.findLocationByPin(dto.getLocationCode());
				String hashedPassword = passwordEncoder.encode(dto.getPassword());
				// userDetail.setLocation(location);
				if (dto.isWeb()) {

				}

				user.setAuthority(Authorities.USER + "," + Authorities.AUTHENTICATED);
				user.setPassword(passwordEncoder.encode(dto.getPassword()));
				user.setMobileStatus(Status.Active);
				user.setEmailStatus(Status.Active);
				// history.setPassword(hashedPassword);
				user.setUsername(dto.getUsername().toLowerCase());
				user.setUserType(dto.getUserType());
				user.setMobileToken(CommonUtil.generateSixDigitNumericString());
				user.setEmailToken("E" + System.currentTimeMillis());
				user.setFullyFilled(true);
				user.setCorporateUser(false);

			}
			user.setUserDetail(userDetail);
			MPQAccountDetails pqAccountDetail = new MPQAccountDetails();
			if (dto.getUserType().equals(UserType.Corporate)) {
				MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("SUPERKYC");
				pqAccountDetail.setBalance(0);
				pqAccountDetail.setBranchCode(dto.getBranchCode());
				pqAccountDetail.setAccountType(nonKYCAccountType);
				user.setAccountDetail(pqAccountDetail);
			} else {
				MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("NONKYC");
				pqAccountDetail.setBalance(0);
				pqAccountDetail.setBranchCode(dto.getBranchCode());
				pqAccountDetail.setAccountType(nonKYCAccountType);
				user.setAccountDetail(pqAccountDetail);
			}
			if (dto.getUserType().equals(UserType.Corporate)) {

			}
			MUser tempUser = mUserRespository.findByUsernameAndStatus(user.getUsername(), Status.Inactive);
			if (tempUser == null) {
				mUserDetailRepository.save(userDetail);
				pqAccountDetail.setAccountNumber(SMSUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
				mPQAccountDetailRepository.save(pqAccountDetail);
				mUserRespository.save(user);
				if (dto.getUserType().equals(UserType.Corporate)) {
					/*
					 * MPQAccountType nonKYCAccountType =
					 * mPQAccountTypeRespository.findByCode("SUPERKYC"); MKycDetail kyc = new
					 * MKycDetail(); kyc.setAadharImage(dto.getAadharImagePath());
					 * kyc.setPancardImage(dto.getPanCardImagePath());
					 * kyc.setAccountNo(dto.getBankAccountNo()); kyc.setIfscCode(dto.getIfscCode());
					 * kyc.setUserType(UserType.Corporate); kyc.setAccountType(nonKYCAccountType);
					 * kyc.setUser(user); mKycRepository.save(kyc);
					 */
					CorporateAgentDetails corporateDetails = new CorporateAgentDetails();
					corporateDetails.setCorporateName(dto.getCorporateName());
					corporateDetails.setCorporateLogo1(dto.getCorporateLogo1Path());
					corporateDetails.setCorporateLogo2(dto.getCorporateLogo2Path());
					corporateDetails.setServiceStatus(true);
					corporateDetails.setBankAccountNo(dto.getBankAccountNo());
					corporateDetails.setIfscCode(dto.getIfscCode());
					corporateDetails.setCorporate(user);
					corporateDetails.setContactPersonNumber(dto.getContactNo());
					corporateAgentDetailsRepository.save(corporateDetails);

				}

			} else {

				mUserRespository.delete(tempUser);
				mUserDetailRepository.delete(tempUser.getUserDetail());
				user.setAccountDetail(tempUser.getAccountDetail());
				mUserDetailRepository.save(userDetail);
				mUserRespository.save(user);
				if (dto.getUserType().equals(UserType.Corporate)) {
					/*
					 * MPQAccountType nonKYCAccountType =
					 * mPQAccountTypeRespository.findByCode("SUPERKYC"); MKycDetail kyc = new
					 * MKycDetail(); kyc.setAadharImage(dto.getAadharImagePath());
					 * kyc.setPancardImage(dto.getPanCardImagePath());
					 * kyc.setAccountNo(dto.getBankAccountNo()); kyc.setIfscCode(dto.getIfscCode());
					 * kyc.setUserType(UserType.Corporate); kyc.setAccountType(nonKYCAccountType);
					 * kyc.setUser(user); mKycRepository.save(kyc);
					 */
					CorporateAgentDetails corporateDetails = new CorporateAgentDetails();
					corporateDetails.setCorporateName(dto.getCorporateName());
					corporateDetails.setCorporateLogo1(dto.getCorporateLogo1Path());
					corporateDetails.setCorporateLogo2(dto.getCorporateLogo2Path());
					corporateDetails.setServiceStatus(true);
					corporateDetails.setBankAccountNo(dto.getBankAccountNo());
					corporateDetails.setIfscCode(dto.getIfscCode());
					corporateDetails.setCorporate(user);
					corporateAgentDetailsRepository.save(corporateDetails);
				}
			}
			if (dto.getUserType().equals(UserType.User)) {
				// mailSenderApi.sendNoReplyMail("Email
				// Verification",MailTemplate.VERIFICATION_EMAIL, user,null);
				// to be changed
				smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.CORPORATE_USER_PASSWORD, user, null);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Bulk Register

	public void saveUserForBulkRegister(RegisterDTO dto) throws Exception {
		// TODO Auto-generated method stub

		MUser user = new MUser();
		MUserDetails userDetail = new MUserDetails();
		userDetail.setAddress(dto.getAddress());
		userDetail.setContactNo(dto.getContactNo());
		userDetail.setFirstName(dto.getFirstName());
		userDetail.setLastName(dto.getLastName());
		userDetail.setMiddleName(dto.getMiddleName());
		userDetail.setEmail(dto.getEmail());

		// userDetail.setDateOfBirth(formatter.parse(dto.getDateOfBirth()));
		userDetail.setDateOfBirth(formatter1.parse(dto.getDateOfBirth()));
		userDetail.setBrand(dto.getBrand());
		userDetail.setModel(dto.getModel());
		userDetail.setImeiNo(dto.getImeiNo());
		userDetail.setIdType(dto.getIdType());
		userDetail.setIdNo(dto.getIdNo());
		if (dto.isHasAadhar()) {
			userDetail.setAadharNo(dto.getAadharNo());
		}

		if (dto.getAndroidDeviceID() != null) {
			System.err.println(dto.getAndroidDeviceID());
			user.setAndroidDeviceID(dto.getAndroidDeviceID());
			MUser androidUser = mUserRespository.findByAndroidDeviceId(dto.getAndroidDeviceID(), true);
			if (androidUser != null) {
				user.setDeviceLocked(false);
			} else {
				user.setDeviceLocked(true);
			}
		}
		MLocationDetails location = null;
		if (dto.getUserType().equals(UserType.Admin) || dto.getUserType().equals(UserType.Merchant)
				|| dto.getUserType().equals(UserType.Corporate)) {
			if (dto.getUserType().equals(UserType.Admin)) {
				user.setAuthority(Authorities.ADMINISTRATOR + "," + Authorities.AUTHENTICATED);
			} else if (dto.getUserType().equals(UserType.Merchant)) {
				user.setAuthority(Authorities.MERCHANT + "," + Authorities.AUTHENTICATED);
			} else if (dto.getUserType().equals(UserType.Corporate)) {
				user.setAuthority(Authorities.CORPORATE + "," + Authorities.AUTHENTICATED);
			}
			String hashedPassword = passwordEncoder.encode(dto.getPassword());
			user.setPassword(hashedPassword);
			// history.setPassword(hashedPassword);
			user.setFullyFilled(true);
			user.setMobileStatus(Status.Active);
			user.setEmailStatus(Status.Active);
			user.setUsername(dto.getUsername().toLowerCase());
			user.setUserType(dto.getUserType());

		} else {
			// location=mLocationDetailRespository.findLocationByPin(dto.getLocationCode());
			String hashedPassword = passwordEncoder.encode(dto.getPassword());
			// userDetail.setLocation(location);
			if (dto.isWeb()) {

			}

			user.setAuthority(Authorities.USER + "," + Authorities.AUTHENTICATED);
			user.setPassword(passwordEncoder.encode(dto.getPassword()));
			user.setMobileStatus(Status.Inactive);
			user.setEmailStatus(Status.Active);
			// history.setPassword(hashedPassword);
			user.setUsername(dto.getUsername().toLowerCase());
			user.setUserType(dto.getUserType());
			user.setMobileToken(CommonUtil.generateSixDigitNumericString());
			user.setEmailToken("E" + System.currentTimeMillis());
			user.setFullyFilled(true);

		}
		user.setUserDetail(userDetail);
		MPQAccountDetails pqAccountDetail = new MPQAccountDetails();
		if (dto.getUserType().equals(UserType.Corporate)) {
			MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("SUPERKYC");
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setBranchCode(dto.getBranchCode());
			pqAccountDetail.setAccountType(nonKYCAccountType);
			user.setAccountDetail(pqAccountDetail);
		} else {
			MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("NONKYC");
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setBranchCode(dto.getBranchCode());
			pqAccountDetail.setAccountType(nonKYCAccountType);
			user.setAccountDetail(pqAccountDetail);
		}
		MUser tempUser = mUserRespository.findByUsernameAndStatus(user.getUsername(), Status.Inactive);
		if (tempUser == null) {
			mUserDetailRepository.save(userDetail);
			pqAccountDetail.setAccountNumber(SMSUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
			mPQAccountDetailRepository.save(pqAccountDetail);
			mUserRespository.save(user);
			if (dto.getUserType().equals(UserType.Corporate)) {
				MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("SUPERKYC");
				MKycDetail kyc = new MKycDetail();
				kyc.setAadharImage(dto.getAadharImagePath());
				kyc.setPancardImage(dto.getPanCardImagePath());
				kyc.setAccountNo(dto.getBankAccountNo());
				kyc.setIfscCode(dto.getIfscCode());
				kyc.setUserType(UserType.Corporate);
				kyc.setAccountType(nonKYCAccountType);
				kyc.setUser(user);
				mKycRepository.save(kyc);
			}

		} else {

			mUserRespository.delete(tempUser);
			mUserDetailRepository.delete(tempUser.getUserDetail());
			user.setAccountDetail(tempUser.getAccountDetail());
			mUserDetailRepository.save(userDetail);
			mUserRespository.save(user);
			if (dto.getUserType().equals(UserType.Corporate)) {
				MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("SUPERKYC");
				MKycDetail kyc = new MKycDetail();
				kyc.setAadharImage(dto.getAadharImagePath());
				kyc.setPancardImage(dto.getPanCardImagePath());
				kyc.setAccountNo(dto.getBankAccountNo());
				kyc.setIfscCode(dto.getIfscCode());
				kyc.setUserType(UserType.Corporate);
				kyc.setAccountType(nonKYCAccountType);
				kyc.setUser(user);
				mKycRepository.save(kyc);
			}
		}
		if (dto.getUserType().equals(UserType.User)) {
			// mailSenderApi.sendNoReplyMail("Email
			// Verification",MailTemplate.VERIFICATION_EMAIL, user,null);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.VERIFICATION_MOBILE, user, null);
		}

	}

	@Override
	public MUser findByUserName(String name) {
		MUser user = mUserRespository.findByUsername(name);
		return user;
	}

	@Override
	public MMCards findByUserWallet(MUser u) {
		MMCards wallet = mmCardRepository.findCardByUser(u);
		return wallet;
	}

	@Override
	public MMCards findByUserWalletPhysical(MUser u) {
		MMCards wallet = mmCardRepository.getPhysicalCardByUser(u);
		return wallet;
	}

	@Override
	public MMCards findByUserWalletVirtual(MUser u) {
		MMCards wallet = mmCardRepository.getVirtualCardsByCardUser(u);
		return wallet;
	}

	@Override
	public void changePasswordRequest(MUser u) {
		u.setMobileToken(CommonUtil.generateSixDigitNumericString());
		mUserRespository.save(u);
		smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.CHANGE_PASSWORD_REQUEST, u, null);
	}

	@Override
	public boolean checkMobileToken(String key, String mobileNumber) {
		MUser user = mUserRespository.findByMobileTokenAndStatus(key, mobileNumber, Status.Inactive);
		if (user == null) {
			return false;
		} else {
			user.setMobileStatus(Status.Active);
			user.setMobileToken(null);
			mUserRespository.save(user);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.VERIFICATION_SUCCESS, user, null);
			return true;
		}
	}

	@Override
	public boolean resendMobileTokenNew(String username) {
		MUser u = mUserRespository.findByUsernameAndStatus(username, Status.Active);
		if (u == null) {
			MUser uu = mUserRespository.findByUsernameAndStatus(username, Status.Inactive);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.REGENERATE_OTP, uu, null);
			return true;
		} else {
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.REGENERATE_OTP, u, null);
			return true;
		}
	}

	@Override
	public UserDTO getUserById(Long id) {
		try {
			MUser u = mUserRespository.findOne(id);
			if (u != null) {
				UserDTO d = ConvertUtil.convertUser(u);
				MMCards cards = mMCardRepository.getVirtualCardsByCardUser(u);
				if (cards != null) {
					if (cards.getStatus().equals("Inactive")) {
						d.setVirtualCardBlock(true);
					} else {
						d.setVirtualCardBlock(false);
					}
				}
				MMCards card = mMCardRepository.getPhysicalCardByUser(u);
				if (card != null) {
					if (card.getStatus().equals("Inactive")) {
						d.setPhysicalCardBlock(true);
					} else {
						d.setPhysicalCardBlock(false);
					}
				}
				return d;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public int updateGcmId(String gcmId, String username) {
		int rowsUpdated = mUserRespository.updateGCMID(gcmId, username);
		return rowsUpdated;
	}

	@Override
	public void requestNewLoginDeviceForSuperAdmin(MUser user) {
		String otp = CommonUtil.generateNDigitNumericString(6);
		user.setMobileToken(otp);
		mUserRespository.save(user);
		smsSenderApi.sendAnotherUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.SUPERADMIN_VERIFICATION_MOBILE, user, null);
	}

	@Override
	public void requestNewLoginDevice(MUser user) {
		String otp = CommonUtil.generateNDigitNumericString(6);
		user.setMobileToken(otp);
		mUserRespository.save(user);
		smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.VERIFICATION_MOBILE, user, null);
	}

	@Override
	public boolean isValidLoginToken(String otp, MUser user) {
		boolean isValidToken = false;
		String mobileToken = user.getMobileToken();
		if (mobileToken != null) {
			if (otp.equalsIgnoreCase(mobileToken)) {
				isValidToken = true;
				user.setMobileToken(null);
				mUserRespository.save(user);
			}
		}
		return isValidToken;
	}

	@Override
	public String handleLoginFailure(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication, String loginUsername, String ipAddress) {
		// String loginUsername = (String) authentication.getPrincipal();
		if (loginUsername != null) {
			MUser user = mUserRespository.findByUsername(loginUsername);
			if (user != null) {
				if (user.getMobileStatus() == Status.Active) {
					if (user.getAuthority().contains(Authorities.BLOCKED)) {
						return "Your account is blocked! Please contact support.";
					}
					LoginLog loginLog = new LoginLog();
					loginLog.setUser(user);
					loginLog.setRemoteAddress(ipAddress);
					loginLog.setServerIp(request.getRemoteAddr());
					loginLog.setStatus(Status.Failed);
					loginLogRepository.save(loginLog);

					List<LoginLog> llsFailed = loginLogRepository.findTodayEntryForUserWithStatus(user, Status.Failed);
					int failedCount = llsFailed.size();
					int remainingAttempts = getLoginAttempts() - failedCount;
					if (remainingAttempts < 0 || failedCount == getLoginAttempts()) {
						if (user.getUserType() == UserType.Merchant) {
							String authority = Authorities.MERCHANT + "," + Authorities.BLOCKED;
							updateUserAuthority(authority, user.getId());
						} else if (user.getUserType() == UserType.User) {
							String authority = Authorities.USER + "," + Authorities.BLOCKED;
							updateUserAuthority(authority, user.getId());
						} else if (user.getUserType() == UserType.Admin) {
							String authority = Authorities.ADMINISTRATOR + "," + Authorities.BLOCKED;
							updateUserAuthority(authority, user.getId());
						} else if (user.getUserType() == UserType.SuperAdmin) {
							String authority = Authorities.SUPER_ADMIN + "," + Authorities.BLOCKED;
							updateUserAuthority(authority, user.getId());
						}
						return "Your account is blocked! Please try after 24 hrs.";
					}
					return "Incorrect password. Remaining attempts " + remainingAttempts;
				} else {
					return "User doesn't exists";
				}
			} else {
				return "User doesn't exists";
			}
		}
		return "Authentication Failed. Please try again";
	}

	@Override
	public void handleLoginSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication, String loginUsername, String ipAddress) {
		if (loginUsername != null) {
			MUser user = mUserRespository.findByUsername(loginUsername);
			if (user != null) {
				LoginLog loginLog = new LoginLog();
				loginLog.setUser(user);
				loginLog.setRemoteAddress(ipAddress);
				loginLog.setServerIp(request.getRemoteAddr());
				loginLog.setStatus(Status.Success);
				loginLogRepository.save(loginLog);
				List<LoginLog> lls = loginLogRepository.findTodayEntryForUserWithStatus(user, Status.Failed);
				for (LoginLog ll : lls) {
					loginLogRepository.deleteLoginLogForId(Status.Deleted, ll.getId());
				}
			}
		}
	}

	@Override
	public int getMpinAttempts() {
		return 5;
	}

	@Override
	public int getLoginAttempts() {
		return 5;
	}

	@Override
	public void updateUserAuthority(String authority, long id) {
		mUserRespository.updateUserAuthority(authority, id);
	}

	@Override
	public long getMCardCount() {
		long cardCount = 0;
		try {
			cardCount = mmCardRepository.findTotalCards();
		} catch (Exception e) {
		}
		return cardCount;
	}

	@Override
	public List<MMCards> findAllVirtualCards() {
		return (List<MMCards>) mmCardRepository.findAllVirtualCards();
	}

	@Override
	public Page<MMCards> findCardByPage(PagingDTO dto, boolean flag) {
		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
		Date date1 = dto.getFromDate();
		Date date2 = dto.getToDate();
		return mmCardRepository.getAllCardsByPage(pageable, flag, date1, date2, dto.getCardStatus());
	}

	@Override
	public List<MMCards> findAllPhysicalCards() {
		return (List<MMCards>) mmCardRepository.findAllPhysicalCards();
	}

	@Override
	public List<PhysicalCardDetails> findAllPhysicalCardRequestList(String status) {

		if (status.equalsIgnoreCase(Status.Requested + "")) {
			return (List<PhysicalCardDetails>) physicalCardDetailRepository
					.findPhysicalCardRequestList(Status.Requested);
		} else if (status.equalsIgnoreCase(Status.Shipped + "")) {
			return (List<PhysicalCardDetails>) physicalCardDetailRepository.findPhysicalCardRequestList(Status.Shipped);
		} else if (status.equalsIgnoreCase(Status.Declined + "")) {
			return (List<PhysicalCardDetails>) physicalCardDetailRepository
					.findPhysicalCardRequestList(Status.Declined);
		} else if (status.equalsIgnoreCase(Status.Active + "")) {
			return (List<PhysicalCardDetails>) physicalCardDetailRepository.findPhysicalCardRequestList(Status.Active);
		} else {
			return (List<PhysicalCardDetails>) physicalCardDetailRepository
					.findPhysicalCardRequestList(Status.Received);
		}
	}

	@Override
	public List<MMCards> findAllCards() {
		return (List<MMCards>) mmCardRepository.findAllCards();
	}

	@Override
	public List<MTransaction> findAllCardTransactioons(DateDTO dto) {
		try {
			dto.setStartDate(dateTime.parse(dto.getFromDate() + " 23:59:59"));
			dto.setEndDate(dateTime.parse(dto.getToDate() + " 00:00:00"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		MService service = mServiceRepository.findByName(dto.getServiceName());
		return mTransactionRepository.findCardTransactions(dto.getStartDate(), dto.getEndDate(), service);
	}

	@Override
	public MUser findByUserAccount(MPQAccountDetails account) {
		return mUserRespository.findByAccountDetails(account);
	}

	@Override
	public MMCards findCardByUser(MUser usera) {
		return mmCardRepository.findCardByUser(usera);
	}

	@Override
	public boolean changePassword(ChangePasswordRequest dto) {
		MUser user = mUserRespository.findByMobileTokenAndStatus(dto.getKey(), dto.getUsername(), Status.Active);
		if (user == null) {
			return false;
		} else {
			user.setMobileToken(null);
			String pass = CommonUtil.base64Decode(dto.getPassword());
			user.setPassword(passwordEncoder.encode(pass));
			mUserRespository.save(user);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.VERIFICATION_SUCCESS, user, null);
			mailSenderApi.sendChangePassword("Ewire Password Change Successful", MailTemplate.CHANGE_PASSWORD, user);
			return true;
		}
	}

	@Override
	public boolean changeProfilePassword(ChangePasswordRequest dto) {
		MUser user = mUserRespository.findByUsername(dto.getUsername());
		if (user == null) {
			return false;
		} else {
			user.setMobileToken(null);
			String newPass = CommonUtil.base64Decode(dto.getNewPassword());
			user.setPassword(passwordEncoder.encode(newPass));
			mUserRespository.save(user);
			mailSenderApi.sendChangePassword("Ewire Password Change Successful", MailTemplate.CHANGE_PASSWORD, user);
			// smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP,
			// SMSTemplete.VERIFICATION_SUCCESS, user, null);
			return true;
		}
	}

	@Override
	public void saveCustomUser(RegisterDTO dto) throws Exception {
		// TODO Auto-generated method stub

		MUser user = new MUser();
		MUserDetails userDetail = new MUserDetails();
		userDetail.setContactNo(dto.getContactNo());
		userDetail.setGroupStatus(Status.Inactive.getValue());
		userDetail.setLastLoginDevice(dto.getLastLoginDevice());
		if (dto.getAndroidDeviceID() != null) {
			System.err.println(dto.getAndroidDeviceID());
			user.setAndroidDeviceID(dto.getAndroidDeviceID());
			MUser androidUser = mUserRespository.findByAndroidDeviceId(dto.getAndroidDeviceID(), true);
			if (androidUser != null) {
				user.setDeviceLocked(false);
			} else {
				user.setDeviceLocked(true);
			}
		}
		user.setGcmId(dto.getRegistrationId());
		user.setAuthority(Authorities.USER + "," + Authorities.AUTHENTICATED);
		user.setMobileStatus(Status.Inactive);
		user.setEmailStatus(Status.Active);
		// history.setPassword(hashedPassword);
		user.setUsername(dto.getContactNo().toLowerCase());
		user.setUserType(dto.getUserType());
		user.setMobileToken(CommonUtil.generateSixDigitNumericString());
		user.setEmailToken("E" + System.currentTimeMillis());
		user.setFullyFilled(false);
		user.setUserDetail(userDetail);
		GroupDetails gd = groupDetailsRepository.getGroupDetailsByName("None");
		user.setGroupDetails(gd);

		MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("NONKYC");
		MPQAccountDetails pqAccountDetail = new MPQAccountDetails();
		pqAccountDetail.setBalance(0);
		// pqAccountDetail.setVBankCustomer(dto.isVbankCustomer());
		// pqAccountDetail.setVijayaAccountNumber(dto.getAccountNumber());
		pqAccountDetail.setBranchCode(dto.getBranchCode());
		pqAccountDetail.setAccountType(nonKYCAccountType);
		user.setAccountDetail(pqAccountDetail);
		MUser tempUser = mUserRespository.findByUsernameAndStatus(user.getUsername(), Status.Inactive);
		if (tempUser == null) {
			mUserDetailRepository.save(userDetail);
			pqAccountDetail.setAccountNumber(SMSUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
			mPQAccountDetailRepository.save(pqAccountDetail);
			mUserRespository.save(user);
			// history.setUser(user);
			// passwordHistoryRepository.save(history);

		} else {

			mUserRespository.delete(tempUser);
			mUserDetailRepository.delete(tempUser.getUserDetail());
			user.setAccountDetail(tempUser.getAccountDetail());
			mUserDetailRepository.save(userDetail);
			mUserRespository.save(user);
		}
		// mailSenderApi.sendNoReplyMail("Email Verification",
		// MailTemplate.VERIFICATION_EMAIL, user,null);
		smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.VERIFICATION_MOBILE, user, null);
	}

	@Override
	public void saveFullDetails(RegisterDTO dto) throws Exception {
		// TODO Auto-generated method stub
		MUser user = findByUserName(dto.getContactNo());
		MUserDetails userDetail = user.getUserDetail();
		userDetail.setFirstName(dto.getFirstName());
		userDetail.setLastName(dto.getLastName());
		userDetail.setMiddleName(dto.getMiddleName());
		userDetail.setEmail(dto.getEmail());
		userDetail.setDateOfBirth(formatter.parse(dto.getDateOfBirth()));
		userDetail.setIdType(dto.getIdType());
		userDetail.setIdNo(dto.getIdNo());
		userDetail.setGroupStatus(Status.Inactive.getValue());
		userDetail.setRemark(dto.getRemark());
		userDetail.setCountryName("India");
		userDetail.setGender(dto.getGender());
		GroupDetails group = groupDetailsRepository.getGroupDetailsByContact(dto.getGroup());
		if (group != null) {
			user.setGroupDetails(group);
		} else {
			GroupDetails g = groupDetailsRepository.getGroupDetailsByContact("None");
			user.setGroupDetails(g);
		}
		if (dto.isHasAadhar()) {
			userDetail.setAadharNo(dto.getAadharNo());
		}
		String pass = CommonUtil.base64Decode(dto.getPassword());
		user.setPassword(passwordEncoder.encode(pass));
		user.setFullyFilled(true);

		// history.setPassword(hashedPassword);

		mUserDetailRepository.save(userDetail);
		mUserRespository.save(user);
		smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.WELCOME_SMS, user, null);
		mailSenderApi.sendRegistrationSuccess("Registration Successful", MailTemplate.REGISTRATION_SUCCESS, user);
	}
	// mailSenderApi.sendNoReplyMail("Email Verification",
	// MailTemplate.VERIFICATION_EMAIL, user,null);

	@Override
	public List<MKycDetail> findAllCorporateAgent() {
		return (List<MKycDetail>) mKycRepository.findCorporateAgent(UserType.Corporate);
	}

	@Override
	public List<MTransaction> findSingleCorporateTransactioons(DateDTO dto) {
		try {
			dto.setStartDate(dateTime.parse(dto.getFromDate() + " 23:59:59"));
			dto.setEndDate(dateTime.parse(dto.getToDate() + " 00:00:00"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		MPQAccountDetails account = mPQAccountDetailRepository.findAccount(dto.getAccountId());
		return mTransactionRepository.findCorporateTransactions(dto.getStartDate(), dto.getEndDate(), account);
	}

	@Override
	public ResponseDTO saveBulkUser(RegisterDTO dto) {

		ResponseDTO resp = new ResponseDTO();
		// TODO Auto-generated method stub
		try {

			for (int j = 0; j < dto.getBulkRegisterDTO().size(); j++) {
				MBulkRegister reg = mBulkRegisterRepository
						.findByContactno(dto.getBulkRegisterDTO().get(j).getContactNo());
				if (reg == null) {
					MBulkRegister user = new MBulkRegister();
					user.setContactNo(dto.getBulkRegisterDTO().get(j).getContactNo());
					user.setEmail(dto.getBulkRegisterDTO().get(j).getEmail());
					user.setName(dto.getBulkRegisterDTO().get(j).getFirstName());
					user.setDateOfBirth(formatter.parse(dto.getBulkRegisterDTO().get(j).getDateOfBirth()));
					mBulkRegisterRepository.save(user);
				}
			}
			resp.setCode("S00");
			resp.setMessage("Bulk Registration Successful.");
		} catch (Exception e) {
			e.printStackTrace();
			resp.setMessage("Service down, Please try again later.");
		}
		return resp;

	}

	@Override
	public List<MBulkRegister> findLast10Register() {
		return mBulkRegisterRepository.findLast10Register();
	}

	@Override
	public void upgradeAccount(UpgradeAccountDTO dto) {
		MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("NONKYC");
		MUser user = mUserRespository.findByUsername(dto.getContactNo());
		MKycDetail detail = mKycRepository.findByUser(user);
		if (detail != null && detail.getAccountType().getCode().equalsIgnoreCase("KYC")) {

		} else if (detail != null && detail.getAccountType().getCode().equalsIgnoreCase("NONKYC")
				&& dto.getImage2() != null) {
			// MKycDetail check=mKycRepository.findByUser(user);

			logger.info("address1 :: " + dto.getAddress1());
			logger.info("addres2 :: " + dto.getAddres2());
			logger.info("address2:: " + dto.getAddress2());
			logger.info("city:: " + dto.getCity());
			logger.info("state:: " + dto.getState());
			logger.info("pincode:: " + dto.getPincode());
			logger.info("country:: " + dto.getCountry());
			logger.info("idType:: " + dto.getIdType());
			logger.info("idNumber:: " + dto.getIdNumber());

			detail.setUserType(dto.getUserType());
			detail.setIdType(dto.getIdType());
			detail.setIdNumber(dto.getIdNumber());
			if (dto.getAddress1().length() > 25) {
				detail.setAddress1(dto.getAddress1().substring(0, 25).replace('/', ' ').replace('.', ' '));
			} else {
				detail.setAddress1(dto.getAddress1().replace('/', ' ').replace('.', ' '));
			}
			if (dto.getAddres2().length() > 25) {
				detail.setAddress2(dto.getAddres2().substring(0, 25).replace('/', ' ').replace('.', ' '));
			} else {
				detail.setAddress2(dto.getAddres2().replace('/', ' ').replace('.', ' '));
			}
			detail.setCity(dto.getCity());
			detail.setState(dto.getState());
			detail.setPinCode(dto.getPincode());
			detail.setCountry(dto.getCountry());
			detail.setIdImage(dto.getImagePath());
			detail.setIdImage1(dto.getImagePath2());
			if (detail.isRejectionStatus() == true) {
				detail.setRejectionStatus(false);
			}

			mKycRepository.save(detail);
			// UserKycRequest requ=new UserKycRequest();
			/*
			 * requ.setFile(file); resp=matchMoveApi.setImagesForKyc(detail);
			 */

		}

		else {
			MKycDetail check = mKycRepository.findByUser(user);
			if (check == null) {
				MKycDetail kyc = new MKycDetail();
				if (dto.getIdType().trim().equalsIgnoreCase("AADHAAR")) {
					dto.setIdType("aadhaar");
				}
				if (dto.getIdType().trim().equalsIgnoreCase("DRIVING LICENSE")) {
					dto.setIdType("drivers_id");
				}
				if (dto.getIdType().trim().equalsIgnoreCase("PASSPORT")) {
					dto.setIdType("passport");
				}

				logger.info("address1 :: " + dto.getAddress1());
				logger.info("addres2 :: " + dto.getAddres2());
				logger.info("address2:: " + dto.getAddress2());
				logger.info("city:: " + dto.getCity());
				logger.info("state:: " + dto.getState());
				logger.info("pincode:: " + dto.getPincode());
				logger.info("country:: " + dto.getCountry());
				logger.info("idType:: " + dto.getIdType());
				logger.info("idNumber:: " + dto.getIdNumber());

				kyc.setUserType(dto.getUserType());
				kyc.setIdType(dto.getIdType());
				kyc.setIdNumber(dto.getIdNumber());
				/*
				 * kyc.setAddress1(dto.getAddress1().substring(0, 25).replace('/', '
				 * ').replace('.', ' ')); kyc.setAddress2(dto.getAddres2().substring(0,
				 * 25).replace('/', ' ').replace('.', ' '));
				 */
				if (dto.getAddress1().length() > 25) {
					kyc.setAddress1(dto.getAddress1().substring(0, 25).replace('/', ' ').replace('.', ' '));
				} else {
					kyc.setAddress1(dto.getAddress1());
				}
				if (dto.getAddres2().length() > 25) {
					kyc.setAddress2(dto.getAddres2().substring(0, 25).replace('/', ' ').replace('.', ' '));
				} else {
					kyc.setAddress2(dto.getAddres2());
				}
				kyc.setCity(dto.getCity());
				kyc.setState(dto.getState());
				kyc.setPinCode(dto.getPincode());
				kyc.setCountry(dto.getCountry());
				kyc.setIdImage(dto.getImagePath());
				if (dto.getImage2() != null) {
					kyc.setIdImage1(dto.getImagePath2());
				}
				kyc.setAccountType(nonKYCAccountType);
				kyc.setUser(user);
				mKycRepository.save(kyc);
			}
		}
	}

	@Override
	public List<MKycDetail> findAllKycRequest() {
		MPQAccountType AccountType = mPQAccountTypeRespository.findByCode("NONKYC");
		return (List<MKycDetail>) mKycRepository.findKycUser(UserType.User, AccountType);
	}

	@Override
	public void updateKycRequest(UpgradeAccountDTO dto) {
		// TODO Auto-generated method stub
		if ("Success".equalsIgnoreCase(dto.getAction())) {
			MPQAccountType KYCAccountType = mPQAccountTypeRespository.findByCode("KYC");
			long id = Long.valueOf(dto.getId());
			MKycDetail kyc = mKycRepository.findById(id);

			UserKycRequest requ = new UserKycRequest();
			if (kyc.getAddress1().length() >= 35) {
				requ.setAddress1(kyc.getAddress1().replaceAll("[^a-zA-Z0-9]", "").trim().substring(0, 35));
			} else {
				requ.setAddress1(kyc.getAddress1().replaceAll("[^a-zA-Z0-9]", "").trim());
			}
			if (kyc.getAddress2().length() >= 35) {
				requ.setAddress2(kyc.getAddress2().replaceAll("[^a-zA-Z0-9]", "").trim().substring(0, 35));
			} else {
				requ.setAddress2(kyc.getAddress2().replaceAll("[^a-zA-Z0-9]", "").trim());
			}
			requ.setCity(kyc.getCity());
			requ.setCountry(kyc.getCountry());
			requ.setState(kyc.getState());
			requ.setPinCode(kyc.getPinCode());
			requ.setId_number(kyc.getIdNumber());
			requ.setId_type(kyc.getIdType());
			// requ.setAddress1(dto.getAddress1());
			// requ.setAddress2(dto.getAddress2());
			// requ.setCity(dto.getCity());
			// requ.setState(dto.getState());
			// requ.setCountry(dto.getCountry());
			// requ.setPinCode(dto.getPincode());
			System.err.println(dto.getIdType());
			System.err.println(dto.getIdNumber());
			requ.setUser(kyc.getUser());
			requ.setId_type(kyc.getIdType());
			requ.setId_number(kyc.getIdNumber());

			// requ.setFile(dto.getImage());
			requ.setFilePath(kyc.getIdImage());

			UserKycResponse resp = matchMoveApi.kycUserMM(requ);

			if ("S00".equalsIgnoreCase(resp.getCode())) {
				if (kyc.getUser().getUserDetail().getDateOfBirth() == null) {
					requ.setDob("1990-02-03");
					resp = matchMoveApi.setIdDetails(requ);
					if ("S00".equalsIgnoreCase(resp.getCode())) {
						// resp=matchMoveApi.setIdDetails(requ);
						if ("S00".equalsIgnoreCase(resp.getCode())) {
							resp = matchMoveApi.setImagesForKyc(requ);
							if (kyc.getIdImage1() != null) {
								requ.setFilePath(kyc.getIdImage1());
								resp = matchMoveApi.setImagesForKyc(requ);
							}
							if (resp.getCode().equalsIgnoreCase("S00")) {
								matchMoveApi.tempConfirmKyc(kyc.getUser().getUserDetail().getEmail(),
										kyc.getUser().getUsername());
								UserKycResponse userKycResponse = matchMoveApi.tempKycStatusApproval(
										kyc.getUser().getUserDetail().getEmail(), kyc.getUser().getUsername());
								if (userKycResponse.getCode().equalsIgnoreCase("S00")) {
									kyc.setAccountType(KYCAccountType);
									mKycRepository.save(kyc);
									MPQAccountDetails account = kyc.getUser().getAccountDetail();
									account.setAccountType(KYCAccountType);
									mPQAccountDetailRepository.save(account);
									smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_PROMOTIONAL,
											SMSTemplete.KYCREQUEST_SUCCESS, kyc.getUser(),
											String.valueOf(KYCAccountType.getMonthlyLimit()));
								}
							}

						}
					}
				} else {
					if (kyc.getUser().getUserDetail().getDateOfBirth() == null) {
						requ.setDob("1990-02-03");
					} else {
						requ.setDob(formatter.format(kyc.getUser().getUserDetail().getDateOfBirth()));
					}
					resp = matchMoveApi.setIdDetails(requ);
					if ("S00".equalsIgnoreCase(resp.getCode())) {
						// resp=matchMoveApi.setIdDetails(requ);
						if ("S00".equalsIgnoreCase(resp.getCode())) {
							resp = matchMoveApi.setImagesForKyc(requ);
							if (kyc.getIdImage1() != null) {
								requ.setFilePath(kyc.getIdImage1());
								resp = matchMoveApi.setImagesForKyc(requ);
							}
							if (resp.getCode().equalsIgnoreCase("S00")) {
								matchMoveApi.tempConfirmKyc(kyc.getUser().getUserDetail().getEmail(),
										kyc.getUser().getUsername());
								UserKycResponse userKycResponse = matchMoveApi.tempKycStatusApproval(
										kyc.getUser().getUserDetail().getEmail(), kyc.getUser().getUsername());
								if (userKycResponse.getCode().equalsIgnoreCase("S00")) {
									kyc.setAccountType(KYCAccountType);
									mKycRepository.save(kyc);
									MPQAccountDetails account = kyc.getUser().getAccountDetail();
									account.setAccountType(KYCAccountType);
									mPQAccountDetailRepository.save(account);
									smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_PROMOTIONAL,
											SMSTemplete.KYCREQUEST_SUCCESS, kyc.getUser(),
											String.valueOf(KYCAccountType.getMonthlyLimit()));
								}

							}

						}
					}
				}

			}
		} else {
			long id = Long.valueOf(dto.getId());
			MKycDetail kyc = mKycRepository.findById(id);
			kyc.setRejectionStatus(true);
			kyc.setRejectionReason(dto.getRejectionReason());
			mKycRepository.save(kyc);
			// mKycRepository.delete(kyc);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_PROMOTIONAL, SMSTemplete.KYCREQUEST_FAILURE, kyc.getUser(),
					null);
		}
	}

	@Override
	public double monthlyLoadMoneyTransactionTotal(MUser user) {
		Calendar now = Calendar.getInstance();
		double total = 0;
		try {
			total = mTransactionRepository.getMonthlyTransactionTotal(now.get(Calendar.YEAR),
					(now.get(Calendar.MONTH) + 1), user.getAccountDetail());
		} catch (NullPointerException e) {
			total = 0;
		} catch (Exception e) {
			total = -1;
			e.printStackTrace();
		}
		return total;
	}

	@Override
	public PiChartResponseDTO findDebitAndCredit(String contactNo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<MUser> findAllUser() {
		// TODO Auto-generated method stub
		return (List<MUser>) mUserRespository.findAllUser();
	}

	@Override
	public List<MUser> findAllActiveUser() {
		// TODO Auto-generated method stub
		return (List<MUser>) mUserRespository.findAllActiveUserList();
	}

	@Override
	public List<MUser> findAllKYCUser() {
		// TODO Auto-generated method stub
		return (List<MUser>) mUserRespository.findAllKYCUserList();
	}

	@Override
	public Page<MUser> findAllUser(PagingDTO dto) {
		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
		return mUserRespository.findAllUserList(pageable, dto.getFromDate(), dto.getToDate());
	}

	@Override
	public Page<MUser> findAllUserBasedOnBlock(PagingDTO dto) {

		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
		Page<MUser> min = twoMinBlockRepository.findAllBlockUser(pageable, dto.getFromDate(), dto.getToDate(), true,
				"ROLE_USER,ROLE_BLOCKED");
		return min;

	}

	@Override
	public Page<MUser> findUserBasedOnGroup(PagingDTO dto) {
		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
		logger.info("username:: " + dto.getContact());
		GroupDetails gd = groupDetailsRepository.getGroupDetailsByContact(dto.getContact());
		logger.info("grooup details:: " + gd.getGroupName());
		return mUserRespository.findAllUserBasedOnGroup(pageable, dto.getFromDate(), dto.getToDate(), gd);
	}

	@Override
	public ResponseDTO changeUserGroup(PagingDTO dto) {
		ResponseDTO res = new ResponseDTO();
		try {
			GroupDetails gd = groupDetailsRepository.getGroupDetailsByContact(dto.getContact());
			if (gd != null) {
				MUser user = mUserRespository.findByUsername(dto.getUserContactNo());
				if (user != null) {
					/*
					 * user.setGroupDetails(gd); mUserRespository.save(user);
					 */
					MUserDetails ud = user.getUserDetail();
					ud.setGroupStatus(Status.Inactive.getValue());
					ud.setChangedGroupName(gd.getGroupName());
					ud.setGroupChange(true);
					mUserDetailRepository.save(ud);
					res.setCode(ResponseStatus.SUCCESS.getValue());
					res.setMessage("User Change Group Request sent to group : " + gd.getGroupName());
				} else {
					res.setCode(ResponseStatus.FAILURE.getValue());
					res.setMessage("user does not exist");
				}
			} else {
				res.setCode(ResponseStatus.FAILURE.getValue());
				res.setMessage("Group does not exist");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	@Override
	public Page<MUser> findAllActiveUser(PagingDTO dto) {
		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
		return mUserRespository.findAllActiveUserList(pageable, dto.getFromDate(), dto.getToDate());

	}

	@Override
	public Page<MUser> findAllKYCUser(PagingDTO dto) {
		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
		return mUserRespository.findAllKYCUserList(pageable, dto.getFromDate(), dto.getToDate());
	}

	@Override
	public Page<MKycDetail> findAllKycRequest(PagingDTO dto) {
		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
		MPQAccountType AccountType = mPQAccountTypeRespository.findByCode("NONKYC");
		return mKycRepository.findAllKYCUserList(pageable, AccountType);
		// return
		// mKycRepository.findAllKYCUserList(pageable,dto.getFromDate(),dto.getToDate(),AccountType);

	}

	@Override
	public Page<MKycDetail> findUpdatedKycList(PagingDTO dto) {
		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
		return mKycRepository.findUpdatedUserList(pageable, dto.getFromDate(), dto.getToDate(), false);
	}

	@Override
	public Page<MKycDetail> findAllKycRequestRejected(PagingDTO dto) {
		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
		MPQAccountType AccountType = mPQAccountTypeRespository.findByCode("NONKYC");
		return mKycRepository.findAllKYCUserListRejected(pageable, dto.getFromDate(), dto.getToDate(), AccountType);

	}

	@Override
	public Page<PhysicalCardDetails> findAllPhysicalCardRequestUser(PagingDTO dto) {
		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
		return physicalCardDetailRepository.findAllPhysicalCardRequestUserList(pageable, dto.getFromDate(),
				dto.getToDate());

	}

	@Override
	public Page<PhysicalCardDetails> findAllPhysicalCardRequestOnStatusList(PagingDTO dto) {
		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
		if (dto.getStatus().equalsIgnoreCase(Status.Requested + "")) {
			return physicalCardDetailRepository.findPhysicalCardRequestList(pageable, dto.getFromDate(),
					dto.getToDate(), Status.Requested);
		} else if (dto.getStatus().equalsIgnoreCase(Status.Shipped + "")) {
			return physicalCardDetailRepository.findPhysicalCardRequestList(pageable, dto.getFromDate(),
					dto.getToDate(), Status.Shipped);
		} else if (dto.getStatus().equalsIgnoreCase(Status.Declined + "")) {
			return physicalCardDetailRepository.findPhysicalCardRequestList(pageable, dto.getFromDate(),
					dto.getToDate(), Status.Declined);
		} else if (dto.getStatus().equalsIgnoreCase(Status.Active + "")) {
			return physicalCardDetailRepository.findPhysicalCardRequestList(pageable, dto.getFromDate(),
					dto.getToDate(), Status.Active);
		} else {
			return physicalCardDetailRepository.findPhysicalCardRequestList(pageable, dto.getFromDate(),
					dto.getToDate(), Status.Received);
		}
	}

	@Override
	public List<PhysicalCardDetails> findAllPhysicalCardRequest() {
		// TODO Auto-generated method stub
		Sort sort = new Sort(Sort.Direction.DESC, "status");
		Pageable pageable = new PageRequest(0, 1000, sort);
		return (List<PhysicalCardDetails>) physicalCardDetailRepository.findAllPhysicalCardRequest(pageable);
	}

	@Override
	public MKycDetail findKYCREquestByUser(MUser us) {
		MPQAccountType AccountType = mPQAccountTypeRespository.findByCode("NONKYC");
		return mKycRepository.findByUserAndAccount(us, AccountType);
	}

	@Override
	public MKycDetail findKYCRequestByUserUpdated(MUser us) {
		MPQAccountType AccountType = mPQAccountTypeRespository.findByCode("NONKYC");
		return mKycRepository.findByUserAndAccountUpdated(us, AccountType, false);
	}

	@Override
	public MKycDetail findKYCRequestByUserUpdatedRejectedKyc(MUser us) {
		MPQAccountType AccountType = mPQAccountTypeRespository.findByCode("NONKYC");
		return mKycRepository.findByUserAndAccountUpdated(us, AccountType, true);
	}

	@Override
	public long gettotalPhysicalCard() {
		long val = 0;
		try {
			val = mmCardRepository.findTotalPhysicalCards();
		} catch (Exception e) {
		}
		return val;
	}

	@Override
	public long getTotalActiveUser() {
		// TODO Auto-generated method stub
		long val = 0;
		try {
			val = mUserRespository.findAllActiveUser();
		} catch (Exception e) {
		}
		return val;
	}

	@Override
	public long getTotalLoadMoney() {
		// TODO Auto-generated method stub
		long val = 0;
		try {
			MService service = mServiceRepository.findByName("Load Money");
			val = mTransactionRepository.getTotalLoadMoney(service);
		} catch (Exception e) {
		}
		return val;
	}

	@Override
	public ResponseDTO savePromoCode(PromoCodeDTO dto) {
		// TODO Auto-generated method stub
		ResponseDTO resp = new ResponseDTO();
		PromoCode promoCode = promoCodeRepository.fetchByPromoCode(dto.getPromoCode());
		MService service = mServiceRepository.findServiceByCode(dto.getServiceCode());
		if (promoCode == null) {
			try {
				Date sDate = formatter1.parse(dto.getStartDate());
				Date eDate = formatter1.parse(dto.getEndDate());
				PromoCode code = new PromoCode();
				code.setEndDate(sDate);
				code.setStartDate(eDate);
				code.setStatus(Status.Active);
				code.setPromoCode(dto.getPromoCode());
				code.setPercentage(dto.isPercentage());
				code.setMaxCashBack(Long.valueOf(dto.getMaxCashBack()));
				code.setMinTrxAmount(Long.valueOf(dto.getMinTrxAmount()));
				code.setService(service);
				if (dto.isPercentage()) {
					code.setPercentage(dto.getPercentageValue());
				} else {
					code.setAmount(dto.getAmount());
				}
				promoCodeRepository.save(code);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		resp.setCode("S00");
		return resp;
	}

	@Override
	public List<PromoCode> fetchPromoCodes() {
		return (List<PromoCode>) promoCodeRepository.findAllPromoCode();
	}

	@Override
	public List<MService> fetchServices() {
		return (List<MService>) mServiceRepository.findService();
	}

	@Override
	public List<PromoCodeRequest> fetchPromoCodeRequest() {
		return (List<PromoCodeRequest>) promoCodeRequestRepository.findAllRequest();
	}

	@Override
	public List<PromoCodeRequest> fetchPromoCodeRequestWithStatus() {
		return (List<PromoCodeRequest>) promoCodeRequestRepository.findPromoCodeWhichAreNotRedeemed();
	}

	@Override
	public List<VersionLogs> findAllVersion() {
		return (List<VersionLogs>) versionLogsRepository.findAllVersions();
	}

	@Override
	public ResponseDTO updateVersionStatus(String id, String status) {
		ResponseDTO resp = new ResponseDTO();
		try {
			long logId = Long.valueOf(id);
			VersionLogs log = versionLogsRepository.findById(logId);
			if ("Active".equalsIgnoreCase(status)) {
				log.setStatus("Inactive");
			} else {
				log.setStatus("Active");
			}
			log.setLastModified(new Date());
			versionLogsRepository.save(log);
			resp.setCode(ResponseStatus.SUCCESS.getValue());
			resp.setMessage("Status updated succesfully.");
		} catch (Exception e) {
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Failed,Please try again.");
		}
		return resp;
	}

	@Override
	public ResponseDTO updatePhysicalCardStatus(String id, String status, String comment) {
		ResponseDTO resp = new ResponseDTO();
		try {
			long logId = Long.valueOf(id);
			PhysicalCardDetails card = physicalCardDetailRepository.findById(logId);
			if (card != null) {
				if (status.equalsIgnoreCase(Status.Requested + "")) {
					card.setStatus(Status.Requested);
				} else if (status.equalsIgnoreCase(Status.Shipped + "")) {
					card.setStatus(Status.Shipped);
				} else {
					card.setStatus(Status.Declined);
				}
				card.setComment(comment);
				card.setLastModified(new Date());
				physicalCardDetailRepository.save(card);
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				resp.setMessage("Status updated succesfully.");
			} else {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Failed,Please try again.");
			}

		} catch (Exception e) {
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Failed,Please try again.");
		}
		return resp;
	}

	@Override
	public boolean saveUserFromAdmin(RegisterDTO dto) {
		boolean value = false;
		try {
			MUser user = new MUser();
			MUserDetails userDetail = new MUserDetails();
			userDetail.setAddress(dto.getAddress());
			userDetail.setContactNo(dto.getContactNo());
			userDetail.setFirstName(dto.getFirstName());
			userDetail.setLastName(dto.getLastName());
			userDetail.setMiddleName(dto.getMiddleName());
			userDetail.setEmail(dto.getEmail());
			// userDetail.setDateOfBirth(formatter.parse(dto.getDateOfBirth()));
			userDetail.setDateOfBirth(formatter1.parse(dto.getDateOfBirth()));
			userDetail.setBrand(dto.getBrand());
			userDetail.setModel(dto.getModel());
			userDetail.setImeiNo(dto.getImeiNo());
			userDetail.setIdType(dto.getIdType());
			userDetail.setIdNo(dto.getIdNo());

			MLocationDetails location = null;
			user.setAuthority(Authorities.USER + "," + Authorities.AUTHENTICATED);
			user.setPassword(passwordEncoder.encode(dto.getPassword()));
			user.setMobileStatus(Status.Active);
			user.setEmailStatus(Status.Active);
			user.setUsername(dto.getUsername().toLowerCase());
			user.setUserType(dto.getUserType());
			user.setMobileToken(CommonUtil.generateSixDigitNumericString());
			user.setEmailToken("E" + System.currentTimeMillis());
			user.setFullyFilled(true);

			user.setUserDetail(userDetail);
			MPQAccountDetails pqAccountDetail = new MPQAccountDetails();
			MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("NONKYC");
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setBranchCode(dto.getBranchCode());
			pqAccountDetail.setAccountType(nonKYCAccountType);
			user.setAccountDetail(pqAccountDetail);
			MUser tempUser = mUserRespository.findByUsernameAndStatus(user.getUsername(), Status.Inactive);
			if (tempUser == null) {
				mUserDetailRepository.save(userDetail);
				pqAccountDetail.setAccountNumber(SMSUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
				mPQAccountDetailRepository.save(pqAccountDetail);
				mUserRespository.save(user);
				value = true;
				return value;
			} else {
				mUserDetailRepository.delete(tempUser.getUserDetail());
				mPQAccountDetailRepository.delete(tempUser.getAccountDetail());
				mUserRespository.delete(tempUser);
				user.setAccountDetail(tempUser.getAccountDetail());
				mUserDetailRepository.save(userDetail);
				mUserRespository.save(user);
				value = true;
				return value;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}

	@Override
	public ResponseDTO Addversion(ApiVersionDTO dto) {
		ResponseDTO resp = new ResponseDTO();
		try {
			ApiVersion apiVersion = apiVersionRepository.getByNameStatus(dto.getName(), "Active");
			if (apiVersion != null) {
				VersionLogs log1 = versionLogsRepository.findLogByVersion(dto.getVersion(), apiVersion);
				if (log1 == null) {
					VersionLogs log = versionLogsRepository.findPreviouLog(apiVersion);
					if (log != null) {
						log.setNewVersion(false);
						versionLogsRepository.save(log);
					}
					VersionLogs versionLogs = new VersionLogs();
					versionLogs.setAndroidVersion(apiVersion);
					versionLogs.setNewVersion(true);
					versionLogs.setStatus("Active");
					versionLogs.setVersion(dto.getVersion());
					versionLogsRepository.save(versionLogs);
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					resp.setMessage("The version is added.");
				} else {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("The version log with this version is already present.");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public List<MMCards> getListByDateCards(Date fromDate, Date toDate, boolean flag) {
		return mmCardRepository.getCardListByDate(fromDate, toDate, flag);
	}

	@Override
	public boolean setNewMpin(MpinDTO dto) {
		boolean updated = false;
		MUser user = mUserRespository.findByUsername(dto.getUsername());
		if (user != null && user.getUserDetail().getMpin() == null) {
			String mpin = dto.getNewMpin();
			try {
				/*
				 * String hashedMpin = SecurityUtil.sha512(mpin);
				 * userDetailRepository.updateUserMPIN(hashedMpin, user.getUsername()); updated
				 * = true;
				 */
				String hashedMpin = passwordEncoder.encode(mpin);
				mUserDetailRepository.updateUserMPIN(hashedMpin, user.getUsername());
				updated = true;

			} catch (Exception e) {
				updated = false;
			}
		}
		return updated;
	}

	@Override
	public List<BulkRegister> findAllCorporateUser(String username) {
		MUser user = mUserRespository.findByUsername(username);
		CorporateAgentDetails adetail = corporateAgentDetailsRepository.getByCorporateId(user);
		return (List<BulkRegister>) bulkRegisterRepository.getRegisteredUsers(adetail);
	}

	@Override
	public boolean saveUserFromCorporate(RegisterDTO dto) {

		boolean value = false;
		try {
			MUser user = new MUser();
			MUserDetails userDetail = new MUserDetails();
			userDetail.setAddress(dto.getAddress());
			userDetail.setContactNo(dto.getContactNo());
			userDetail.setFirstName(dto.getFirstName());
			userDetail.setLastName(dto.getLastName());
			userDetail.setMiddleName(dto.getMiddleName());
			userDetail.setEmail(dto.getEmail());
			// userDetail.setDateOfBirth(formatter.parse(dto.getDateOfBirth()));
			userDetail.setDateOfBirth(formatter1.parse(dto.getDateOfBirth()));
			userDetail.setBrand(dto.getBrand());
			userDetail.setModel(dto.getModel());
			userDetail.setImeiNo(dto.getImeiNo());
			userDetail.setIdType(dto.getIdType());
			userDetail.setIdNo(dto.getIdNo());

			MLocationDetails location = null;
			user.setAuthority(Authorities.USER + "," + Authorities.AUTHENTICATED);
			user.setPassword(passwordEncoder.encode(dto.getPassword()));
			user.setMobileStatus(Status.Active);
			user.setEmailStatus(Status.Active);
			user.setUsername(dto.getUsername().toLowerCase());
			user.setUserType(dto.getUserType());
			user.setMobileToken(CommonUtil.generateSixDigitNumericString());
			user.setEmailToken("E" + System.currentTimeMillis());
			user.setFullyFilled(true);
			user.setCorporateUser(true);

			user.setUserDetail(userDetail);
			MPQAccountDetails pqAccountDetail = new MPQAccountDetails();
			MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("NONKYC");
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setBranchCode(dto.getBranchCode());
			pqAccountDetail.setAccountType(nonKYCAccountType);
			user.setAccountDetail(pqAccountDetail);
			MUser tempUser = mUserRespository.findByUsernameAndStatus(user.getUsername(), Status.Inactive);
			if (tempUser == null) {
				mUserDetailRepository.save(userDetail);
				pqAccountDetail.setAccountNumber(SMSUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
				mPQAccountDetailRepository.save(pqAccountDetail);
				mUserRespository.save(user);
				value = true;
				return value;
			} else {
				mUserDetailRepository.delete(tempUser.getUserDetail());
				mPQAccountDetailRepository.delete(tempUser.getAccountDetail());
				mUserRespository.delete(tempUser);
				user.setAccountDetail(tempUser.getAccountDetail());
				mUserDetailRepository.save(userDetail);
				mUserRespository.save(user);
				value = true;
				return value;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}

	@Override
	public boolean saveCorporatePartner(RegisterDTO dto) {

		boolean value = false;
		try {
			MUser user = new MUser();
			MUserDetails userDetail = new MUserDetails();
			userDetail.setContactNo(dto.getContactNo());
			userDetail.setEmail(dto.getEmail());
			// userDetail.setDateOfBirth(formatter.parse(dto.getDateOfBirth()));
			user.setAuthority(Authorities.USER + "," + Authorities.CORPORATE_PARTNER);
			user.setPassword(passwordEncoder.encode(dto.getContactNo()));
			user.setMobileStatus(Status.Active);
			user.setEmailStatus(Status.Active);
			user.setUsername(dto.getEmail().toLowerCase());
			user.setUserType(UserType.CorporatePartner);
			user.setMobileToken(CommonUtil.generateSixDigitNumericString());
			user.setEmailToken("E" + System.currentTimeMillis());
			user.setFullyFilled(true);

			user.setUserDetail(userDetail);
			MPQAccountDetails pqAccountDetail = new MPQAccountDetails();
			MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("KYC");
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setBranchCode(dto.getBranchCode());
			pqAccountDetail.setAccountType(nonKYCAccountType);
			user.setAccountDetail(pqAccountDetail);
			PartnerDetails partnerDetails = new PartnerDetails();
			partnerDetails.setPartnerEmail(dto.getEmail());
			partnerDetails.setPartnerMobile(dto.getContactNo());
			partnerDetails.setPartnerName(dto.getFirstName());
			partnerDetails.setPartnerServices(dto.getPartnerServices());
			partnerDetails.setCorporate(dto.getAgentDetails());
			partnerDetails.setLoadCardMaxLimit(dto.getMaxLoadCardLimit());
			partnerDetails.setStatus(Status.Active);

			MUser tempUser = mUserRespository.findByUsernameAndStatus(user.getUsername(), Status.Inactive);
			if (tempUser == null) {
				mUserDetailRepository.save(userDetail);
				pqAccountDetail.setAccountNumber(SMSUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
				mPQAccountDetailRepository.save(pqAccountDetail);
				mUserRespository.save(user);
				partnerDetails.setPartnerUser(user);
				partnerDetailsRepository.save(partnerDetails);
				value = true;
				return value;
			} else {
				mUserDetailRepository.delete(tempUser.getUserDetail());
				mPQAccountDetailRepository.delete(tempUser.getAccountDetail());
				mUserRespository.delete(tempUser);
				user.setAccountDetail(tempUser.getAccountDetail());
				mUserDetailRepository.save(userDetail);
				mUserRespository.save(user);
				partnerDetails.setPartnerUser(user);
				partnerDetailsRepository.save(partnerDetails);
				value = true;
				return value;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}

	@Override
	public boolean changeCurrentMpin(MPinChangeDTO dto) {
		boolean updated = false;
		MUser user = mUserRespository.findByUsername(dto.getUsername());
		if (user != null && user.getUserDetail().getMpin() != null) {
			String mpin = dto.getNewMpin();
			try {
				mUserDetailRepository.updateUserMPIN(passwordEncoder.encode(mpin), user.getUsername());
				updated = true;
			} catch (Exception e) {
				updated = false;
			}
		}
		return updated;
	}

	@Override
	public String handleMpinFailure(HttpServletRequest request, String loginUsername) {
		if (loginUsername != null) {
			MUser user = mUserRespository.findByUsername(loginUsername);
			if (user != null) {
				if (user.getMobileStatus() == Status.Active) {
					if (user.getAuthority().contains(Authorities.BLOCKED)) {
						return "Your account is blocked! Please contact support.";
					}
					MpinLogs mpinLog = new MpinLogs();
					mpinLog.setUser(user);
					mpinLog.setServerIpAddress(request.getRemoteAddr());
					mpinLog.setStatus(Status.Failed);
					mPinLogsRepository.save(mpinLog);
					List<MpinLogs> llsFailed = mPinLogsRepository.findTodayEntryForUserWithStatus(user, Status.Failed);
					int failedCount = llsFailed.size();
					int remainingAttempts = getMpinAttempts() - failedCount;
					if (failedCount == getMpinAttempts()) {
						if (user.getUserType() == UserType.User) {
							String authority = Authorities.USER + "," + Authorities.BLOCKED;
							updateUserAuthority(authority, user.getId());
						}
						return "Your account is blocked! Please try after 24 hrs.";
					}
					return "Incorrect mpin. Remaining attempts " + remainingAttempts;
				} else {
					return "User doesn't exists";
				}
			} else {
				return "User doesn't exists";
			}
		}
		return "Authentication Failed. Please try again";
	}

	@Override
	public boolean deleteCurrentMpin(String username) {
		boolean deleted = false;
		MUser user = mUserRespository.findByUsername(username);
		if (user != null && user.getUserDetail().getMpin() != null) {
			mUserDetailRepository.deleteUserMPIN(username);
			deleted = true;
		}
		return deleted;
	}

	@Override
	public boolean changePasswordCorporate(MatchMoveCreateCardRequest dto) {
		MUser user = mUserRespository.findByUsername(dto.getUsername());
		if (user != null) {
			user.setPassword(passwordEncoder.encode(dto.getPassword()));
			mUserRespository.save(user);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean changePasswordAdmin(MatchMoveCreateCardRequest dto) {
		MUser user = mUserRespository.findByUsername(dto.getUsername());
		if (user != null) {
			user.setPassword(passwordEncoder.encode(dto.getPassword()));
			mUserRespository.save(user);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public ResponseDTO updateKycRequestCorp(UpgradeAccountDTO dto) {
		// TODO Auto-generated method stub
		ResponseDTO response = new ResponseDTO();
		try {
			long id = Long.valueOf(dto.getId());
			MKycDetail kyc = mKycRepository.findById(id);
			UserKycRequest requ = new UserKycRequest();
			requ.setAddress1(dto.getAddress1());
			requ.setAddress2(dto.getAddress2());
			requ.setCity(dto.getCity());
			requ.setState(dto.getState());
			requ.setCountry(dto.getCountry());
			requ.setPinCode(dto.getPincode());
			System.err.println(dto.getIdType());
			System.err.println(dto.getIdNumber());
			requ.setUser(kyc.getUser());
			requ.setId_type(kyc.getIdType());
			requ.setId_number(kyc.getIdNumber());
			requ.setFile(dto.getImage());
			requ.setFilePath(kyc.getIdImage());
			UserKycResponse resp = matchMoveApi.kycUserMM(requ);
			if ("S00".equalsIgnoreCase(resp.getCode())) {
				resp = matchMoveApi.setIdDetails(requ);
				if ("S00".equalsIgnoreCase(resp.getCode())) {
					// resp=matchMoveApi.setIdDetails(requ);

					if ("S00".equalsIgnoreCase(resp.getCode())) {
						resp = matchMoveApi.setImagesForKyc(requ);
						if (kyc.getIdImage1() != null) {
							requ.setFilePath(kyc.getIdImage1());
							resp = matchMoveApi.setImagesForKyc(requ);

						}
						if (resp.getCode().equalsIgnoreCase("S00")) {
							matchMoveApi.tempConfirmKyc(kyc.getUser().getUserDetail().getEmail(),
									kyc.getUser().getUsername());
							UserKycResponse approved = matchMoveApi.tempKycStatusApproval(
									kyc.getUser().getUserDetail().getEmail(), kyc.getUser().getUsername());
							if (approved.getCode().equalsIgnoreCase("S00")) {
								MPQAccountType KYCAccountType = mPQAccountTypeRespository.findByCode("KYC");

								kyc.setAccountType(KYCAccountType);
								mKycRepository.save(kyc);
								MPQAccountDetails account = kyc.getUser().getAccountDetail();
								account.setAccountType(KYCAccountType);
								mPQAccountDetailRepository.save(account);
								smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_PROMOTIONAL, SMSTemplete.KYCREQUEST_SUCCESS,
										kyc.getUser(), String.valueOf(KYCAccountType.getMonthlyLimit()));
								response.setCode("S00");
								return response;

							}
						}

					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			response.setCode("F00");
			return response;
		}
		return response;
	}

	@Override
	public ResponseDTO upgradeAccountCorp(UpgradeAccountDTO dto) {
		ResponseDTO resp = new ResponseDTO();
		try {
			MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("NONKYC");
			MUser user = mUserRespository.findByUsername(dto.getContactNo());
			MKycDetail detail = mKycRepository.findByUser(user);
			if (detail != null && detail.getAccountType().getCode().equalsIgnoreCase("KYC")) {

			} else if (detail != null && detail.getAccountType().getCode().equalsIgnoreCase("NONKYC")
					&& dto.getImage2() != null) {
				// MKycDetail check=mKycRepository.findByUser(user);
				detail.setIdImage(dto.getImagePath());
				detail.setIdImage1(dto.getImagePath2());
				if (detail.isRejectionStatus() == true) {
					detail.setRejectionStatus(false);
				}
				mKycRepository.save(detail);
			}

			else {
				MKycDetail check = mKycRepository.findByUser(user);
				if (check == null) {
					MKycDetail kyc = new MKycDetail();
					if (dto.getIdType().trim().equalsIgnoreCase("AADHAAR")) {
						dto.setIdType("aadhaar");
					}
					if (dto.getIdType().trim().equalsIgnoreCase("DRIVING LICENSE")) {
						dto.setIdType("drivers_id");
					}
					if (dto.getIdType().trim().equalsIgnoreCase("PASSPORT")) {
						dto.setIdType("passport");
					}

					kyc.setUserType(dto.getUserType());
					kyc.setIdType(dto.getIdType());
					kyc.setIdNumber(dto.getIdNumber());
					kyc.setAddress1(dto.getAddress1());
					kyc.setAddress2(dto.getAddres2());
					kyc.setCity(dto.getCity());
					kyc.setState(dto.getState());
					kyc.setPinCode(dto.getPincode());
					kyc.setCountry(dto.getCountry());
					kyc.setIdImage(dto.getImagePath());
					if (dto.getImage2() != null) {
						kyc.setIdImage1(dto.getImagePath2());
					}
					kyc.setAccountType(nonKYCAccountType);
					kyc.setUser(user);
					mKycRepository.save(kyc);
					resp.setCode("S00");
					return resp;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode("F00");
			return resp;
		}
		return resp;
	}

	@Override
	public List<GroupDetails> getGroupDetails() {
		List<GroupDetails> list = new ArrayList<GroupDetails>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<GroupDetails> page = groupDetailsRepository.getGroupName(pageable, Status.Active.getValue());
			if (page != null) {
				list = page.getContent();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<GroupDetailsListDTO> getGroupDetailsmobile() {
		List<GroupDetailsListDTO> list = new ArrayList<GroupDetailsListDTO>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<GroupDetails> page = groupDetailsRepository.getGroupName(pageable, Status.Active.getValue());
			if (page != null) {
				List<GroupDetails> listing = page.getContent();
				for (int i = 0; i < listing.size(); i++) {
					GroupDetailsListDTO gd = new GroupDetailsListDTO();
					gd.setGroupName(listing.get(i).getGroupName());
					gd.setContactNo(listing.get(i).getContactNo());
					gd.setEmail(listing.get(i).getEmail());
					gd.setDname(listing.get(i).getDname());
					gd.setStatus(listing.get(i).getStatus());
					gd.setGroupImage(listing.get(i).getGroupImage());
					list.add(gd);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<MUser> groupAddRequestByContactGet(UserDataRequestDTO dto) {
		List<MUser> lUserDTO = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			logger.info("date from:== " + dto.getFrom());
			logger.info("to :== " + dto.getTo());
			logger.info("status::== " + Status.Inactive.getValue());
			logger.info("contact::== " + dto.getContactNo());

			if (dto.getEmail() == null) {
				Page<MUser> user = mUserRespository.findAllUserByGroupStatusByContactAdminNoDate(pageable,
						Status.Inactive.getValue(), true);
				if (user != null) {
					lUserDTO = user.getContent();
				} else {
					throw new Exception("session is null");
				}
			} else {
				Page<MUser> user = mUserRespository.requestUserGroup(pageable, dto.getEmail(),
						Status.Inactive.getValue());
				if (user != null) {
					lUserDTO = user.getContent();
				} else {
					throw new Exception("session is null");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<MUser> groupAddRequestByContact(UserDataRequestDTO dto) {
		List<MUser> lUserDTO = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			logger.info("date from:== " + dto.getFrom());
			logger.info("to :== " + dto.getTo());
			logger.info("status::== " + Status.Inactive.getValue());
			logger.info("contact::== " + dto.getEmail());

			if (dto.getEmail() == null) {
				Page<MUser> user = mUserRespository.findAllUserByGroupStatusByContactAdmin(pageable, dto.getFrom(),
						dto.getTo(), Status.Inactive.getValue(), true);
				if (user != null) {
					lUserDTO = user.getContent();
				} else {
					throw new Exception("session is null");
				}
			} else {
				Page<MUser> user = mUserRespository.findAllUserByGroupStatusByContact(pageable, dto.getFrom(),
						dto.getTo(), dto.getEmail(), Status.Inactive.getValue());
				if (user != null) {
					lUserDTO = user.getContent();
				} else {
					throw new Exception("session is null");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<MUser> changeRequestListGet(String email) {
		List<MUser> list = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<MUser> page = mUserRespository.getChangeList(pageable, email, true);
			if (page != null) {
				list = page.getContent();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<MUser> changeRequestListPost(UserDataRequestDTO dto) {
		List<MUser> list = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<MUser> page = mUserRespository.getChangeListPost(pageable, dto.getFrom(), dto.getTo(), dto.getEmail(),
					true);
			if (page != null) {
				list = page.getContent();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public long getCountGroupRequest(String email) {
		long count = mUserRespository.findAllUserByGroupStatusByCount(Status.Inactive.getValue(), email);
		long count1 = mUserRespository.findAllUserByGroupStatusByCountChange(Status.Inactive.getValue(), email, true);
		long total = count + count1;
		return total;
	}

	@Override
	public long getCountGroupAccept(String email) {
		long acceptedCount = mUserRespository.findAllUserByGroupStatusCount(Status.Active.getValue(), email);
		return acceptedCount;
	}

	@Override
	public long getCountGroupReject(String contact) {
		long rejectedCount = mUserRespository.findAllUserByCountReject(Status.Cancelled.getValue(), contact);
		return rejectedCount;
	}

	@Override
	public long getBulkRegCount(String email) {
		long bulkRegCount = groupFileCatalogueRepository.getBulkRegCount(email);
		return bulkRegCount;
	}

	@Override
	public long getBulkSmsCount(String email) {
		long bulkSmsCount = groupFileCatalogueRepository.getBulkSmsCount(email);
		return bulkSmsCount;
	}

	@Override
	public long getBulkCardLoadCount(String email) {
		long bulkCardLoadCount = groupFileCatalogueRepository.getBulkCardLoadCount(email);
		return bulkCardLoadCount;
	}

	@Override
	public long getTotalUsersOfGroup(String email) {
		long totalGroupUsers = mUserRespository.getCountOfUsersGroupWise(email, Status.Active,
				Status.Active.getValue());
		return totalGroupUsers;
	}

	@Override
	public List<MUser> getRequestUser(String contact, String email) {
		List<MUser> requestSingleUser = mUserRespository.getSingleRequest(Status.Inactive.getValue(), contact, email);
		return requestSingleUser;
	}

	@Override
	public List<MUser> getAcceptedUser(String contact, String email) {
		List<MUser> acceptedSingleUser = mUserRespository.getSingleRequest(Status.Active.getValue(), contact, email);
		return acceptedSingleUser;
	}

	@Override
	public List<MUser> getRejectedUser(String contact, String phone) {
		List<MUser> rejectedSingleUser = mUserRespository.getSingleRequestReject(Status.Cancelled.getValue(), contact,
				phone);
		return rejectedSingleUser;
	}

	@Override
	public List<GroupBulkRegister> getsingleuserfailbulkreg(String contact, String email) {
		List<GroupBulkRegister> list = groupBulkRegisterRepository.getsingleuserfail(false, contact, email);
		return list;
	}

	@Override
	public List<GroupBulkKyc> getsingleuserfailbulkKyc(String contact, String email) {
		List<GroupBulkKyc> list = groupBulkKycRepository.getsingleuserfail(false, contact, email);
		return list;
	}

	@Override
	public List<MUser> getSingleUserGroupRequest(String contact) {
		List<MUser> userList = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);
			Page<MUser> user = mUserRespository.getListUserbyContact(pageable, contact);
			if (user != null) {
				userList = user.getContent();
			} else {
				throw new Exception("Session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userList;
	}

	@Override
	public LoginResponse loginGroup(LoginDTO login, HttpServletRequest request, HttpServletResponse response) {
		LoginResponse result = new LoginResponse();
		try {
			LoginError error = checkLoginValidation(login);
			if (error.isSuccess()) {
				try {
					MUser user = findByUserName(login.getUsername());
					if (user.getAuthority().contains(Authorities.GROUP)
							|| user.getAuthority().contains(Authorities.SUPER_ADMIN)
							|| user.getAuthority().contains(Authorities.ADMINISTRATOR)) {
						AuthenticationError auth = authentication(login, request);
						if (auth.isSuccess()) {
							Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
							sls.onAuthentication(authentication, request, response);
							UserSession userSession = userSessionRepository.findByActiveSessionId(
									RequestContextHolder.currentRequestAttributes().getSessionId());
							result.setStatus(ResponseStatus.SUCCESS.getKey());
							result.setCode(ResponseStatus.SUCCESS.getValue());
							result.setMessage("Login successful.");
							result.setSuccess(true);
							result.setSessionId(userSession.getSessionId());
							result.setContactNo(userSession.getUser().getGroupDetails().getEmail());
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER.getKey());
							result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
							result.setMessage(auth.getMessage());
							result.setSuccess(false);
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE.getKey());
						result.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
						result.setMessage("Unauthorized Role");
						result.setSuccess(false);
					}
				} catch (Exception e) {
					e.printStackTrace();
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage(ErrorMessage.TRY_AGAIN_MSG);
					result.setSuccess(false);
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(error.getMessage());
				result.setSuccess(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public LoginError checkLoginValidation(LoginDTO login) {
		LoginError loginError = new LoginError();
		loginError.setSuccess(true);
		if (CommonValidation.isNull(login.getUsername())) {
			loginError.setSuccess(false);
			loginError.setMessage("Enter Username");
		} else if (CommonValidation.isNull(login.getPassword())) {
			loginError.setSuccess(false);
			loginError.setMessage("Enter Password");
		} else if (CommonValidation.isNull(login.getIpAddress())) {
			loginError.setMessage("Not a valid device");
			loginError.setSuccess(false);
		}
		return loginError;
	}

	private AuthenticationError authentication(LoginDTO dto, HttpServletRequest request)
			throws ServletException, IOException, Exception {
		AuthenticationError error = new AuthenticationError();
		Authentication auth = null;
		UsernamePasswordAuthenticationToken token = null;
		try {
			token = new UsernamePasswordAuthenticationToken(dto.getUsername(), dto.getPassword());
			auth = authenticationManager.authenticate(token);
			SecurityContext securityContext = SecurityContextHolder.getContext();
			if (auth.isAuthenticated()) {
				securityContext.setAuthentication(auth);
				SecurityContextHolder.getContext().setAuthentication(auth);
				HttpSession session = request.getSession(true);
				session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);

				/* for session 30 min active */

				session.setMaxInactiveInterval(30 * 60);

				/* for session 30 min active */

				error.setSuccess(true);
				error.setMessage("Login successful.");
				handleLoginSuccess(request, null, auth, String.valueOf(token.getPrincipal()), dto.getIpAddress());
				return error;
			} else {
				error.setSuccess(false);
				error.setMessage(handleLoginFailure(request, null, auth, String.valueOf(token.getPrincipal()),
						dto.getIpAddress()));
				SecurityContextHolder.getContext().setAuthentication(null);
				return error;
			}
		} catch (Exception e) {
			e.printStackTrace();
			SecurityContextHolder.getContext().setAuthentication(null);
			error.setSuccess(false);
			error.setMessage(
					handleLoginFailure(request, null, auth, String.valueOf(token.getPrincipal()), dto.getIpAddress()));
			return error;
		}
	}

	@Override
	public CommonResponse createGroup(GroupDetailDTO dto, CommonResponse result) {
		String email = dto.getEmailAddressId();
		GroupDetails gd = groupDetailsRepository.getGroupDetailsByEmail(email);
		if (gd == null) {
			String contactNo = dto.getMobileNoId();
			gd = groupDetailsRepository.getGroupDetailsByContact(contactNo);
			if (gd == null) {
				result = savegroup(dto, result);
				logger.info("contact no:: " + contactNo);
				gd = groupDetailsRepository.getGroupDetailsByContact(contactNo);

				List<GroupOtpMobile> groupOtp = groupOtpRepository.getGroupDetails(contactNo);
				if (groupOtp.isEmpty() || groupOtp == null) {
					GroupOtpMobile gotp = new GroupOtpMobile();
					gotp.setContactName(gd.getGroupName());
					gotp.setGroupName(gd.getDname());
					gotp.setMobileNumber(gd.getContactNo());
					groupOtpRepository.save(gotp);
				}
				result.setStatus(ResponseStatus.SUCCESS.getKey());
				result.setCode(ResponseStatus.SUCCESS.getValue());
				result.setMessage("Group Added Successfully.");
				result.setSuccess(true);
				smsSenderApi.sendGroupCreateSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.CREATED_GROUP, gd,
						dto.getMobileNoId());
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage("Contact no already exist.");
				result.setSuccess(false);
			}
		} else {
			result.setStatus(ResponseStatus.FAILURE.getKey());
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage("Email id already exist.");
			result.setSuccess(false);
		}
		return result;
	}

	@Override
	public void setServicesGroup() {

		try {
			MOperator operator = mOperatorRepository.findOperatorByName("Gcorporate");
			if (operator == null) {
				operator = new MOperator();
				operator.setName("Gcorporate");
				operator.setStatus(Status.Active);
				mOperatorRepository.save(operator);
			}

			MServiceType sertype = mServiceTypeRepository.findServiceTypeByName("CGroup");
			if (sertype == null) {
				sertype = new MServiceType();
				sertype.setName("CGroup");
				sertype.setDescription("Corporate Group");
				mServiceTypeRepository.save(sertype);
			}

			MOperator op = mOperatorRepository.findOperatorByName("Gcorporate");
			MServiceType sert = mServiceTypeRepository.findServiceTypeByName("CGroup");

			MService service = mServiceRepository.findServiceByCode("BLR");
			if (service == null) {
				service = new MService();
				service.setCode("BLR");
				service.setDescription("Corporate Bulk Register");
				service.setMaxAmount(1000);
				service.setMinAmount(1);
				service.setName("Bulk Register");
				service.setOperator(op);
				service.setOperatorCode("BLR");
				service.setServiceType(sert);
				service.setServiceName("Bulk Register Corp");
				service.setStatus(Status.Active);
				mServiceRepository.save(service);
			}

			MService service3 = mServiceRepository.findServiceByCode("BKY");
			if (service3 == null) {
				service3 = new MService();
				service3.setCode("BKY");
				service3.setDescription("Corporate Bulk KYC");
				service3.setMaxAmount(1000);
				service3.setMinAmount(1);
				service3.setName("Bulk KYC");
				service3.setOperator(op);
				service3.setOperatorCode("BKY");
				service3.setServiceType(sert);
				service3.setServiceName("Bulk KYC Corp");
				service3.setStatus(Status.Active);
				mServiceRepository.save(service3);
			}

			MService service2 = mServiceRepository.findServiceByCode("BCL");
			if (service2 == null) {
				service2 = new MService();
				service2.setCode("BCL");
				service2.setDescription("Corporate Bulk Card Load");
				service2.setMaxAmount(1000);
				service2.setMinAmount(1);
				service2.setName("Bulk Card Load");
				service2.setOperator(op);
				service2.setOperatorCode("BCL");
				service2.setServiceType(sert);
				service2.setServiceName("Bulk Card Load");
				service2.setStatus(Status.Active);
				mServiceRepository.save(service2);
			}

			MService service4 = mServiceRepository.findServiceByCode("CSCL");
			if (service4 == null) {
				service4 = new MService();
				service4.setCode("CSCL");
				service4.setDescription("Corporate SingleCard Load");
				service4.setMaxAmount(1000);
				service4.setMinAmount(1);
				service4.setName("Single CardLoad");
				service4.setOperator(op);
				service4.setOperatorCode("CSCL");
				service4.setServiceType(sert);
				service4.setServiceName("Corporate SingleCard Load");
				service4.setStatus(Status.Active);
				mServiceRepository.save(service4);
			}

			MService service5 = mServiceRepository.findServiceByCode("CSCA");
			if (service5 == null) {
				service5 = new MService();
				service5.setCode("CSCA");
				service5.setDescription("Corporate SingleCard Assign");
				service5.setMaxAmount(1000);
				service5.setMinAmount(1);
				service5.setName("Single Card Assign");
				service5.setOperator(op);
				service5.setOperatorCode("CSCA");
				service5.setServiceType(sert);
				service5.setServiceName("Corporate SingleCard Assign");
				service5.setStatus(Status.Active);
				mServiceRepository.save(service5);
			}

			MService service6 = mServiceRepository.findServiceByCode("CPH");
			if (service6 == null) {
				service6 = new MService();
				service6.setCode("CPH");
				service6.setDescription("Corporate Prefund History");
				service6.setMaxAmount(1000);
				service6.setMinAmount(1);
				service6.setName("Prefund History");
				service6.setOperator(op);
				service6.setOperatorCode("CPH");
				service6.setServiceType(sert);
				service6.setServiceName("Corporate Prefund History");
				service6.setStatus(Status.Active);
				mServiceRepository.save(service6);
			}

			MService service7 = mServiceRepository.findServiceByCode("CUR");
			if (service7 == null) {
				service7 = new MService();
				service7.setCode("CUR");
				service7.setDescription("Corporate User Report");
				service7.setMaxAmount(1000);
				service7.setMinAmount(1);
				service7.setName("Corporate User Report");
				service7.setOperator(op);
				service7.setOperatorCode("CUR");
				service7.setServiceType(sert);
				service7.setServiceName("Corporate User Report");
				service7.setStatus(Status.Active);
				mServiceRepository.save(service7);
			}

			MService service8 = mServiceRepository.findServiceByCode("CFR");
			if (service8 == null) {
				service8 = new MService();
				service8.setCode("CFR");
				service8.setDescription("Corporate Failed Registeration");
				service8.setMaxAmount(1000);
				service8.setMinAmount(1);
				service8.setName("Corporate Failed Registeration");
				service8.setOperator(op);
				service8.setOperatorCode("CFR");
				service8.setServiceType(sert);
				service8.setServiceName("Corporate Failed Registeration");
				service8.setStatus(Status.Active);
				mServiceRepository.save(service8);
			}

			MService service9 = mServiceRepository.findServiceByCode("CFLL");
			if (service9 == null) {
				service9 = new MService();
				service9.setCode("CFLL");
				service9.setDescription("Corporate Failed Load List");
				service9.setMaxAmount(1000);
				service9.setMinAmount(1);
				service9.setName("Corporate Failed Load List");
				service9.setOperator(op);
				service9.setOperatorCode("CFLL");
				service9.setServiceType(sert);
				service9.setServiceName("Corporate Failed Load List");
				service9.setStatus(Status.Active);
				mServiceRepository.save(service9);
			}

			MService service10 = mServiceRepository.findServiceByCode("CBCL");
			if (service10 == null) {
				service10 = new MService();
				service10.setCode("CBCL");
				service10.setDescription("Corporate Blocked Card List");
				service10.setMaxAmount(1000);
				service10.setMinAmount(1);
				service10.setName("Corporate Blocked Card List");
				service10.setOperator(op);
				service10.setOperatorCode("CBCL");
				service10.setServiceType(sert);
				service10.setServiceName("Corporate Blocked Card List");
				service10.setStatus(Status.Active);
				mServiceRepository.save(service10);
			}

			MService service11 = mServiceRepository.findServiceByCode("BSMS");
			if (service11 == null) {
				service11 = new MService();
				service11.setCode("BSMS");
				service11.setDescription("Corporate Bulk SMS");
				service11.setMaxAmount(1000);
				service11.setMinAmount(1);
				service11.setName("Corporate Bulk SMS");
				service11.setOperator(op);
				service11.setOperatorCode("BSMS");
				service11.setServiceType(sert);
				service11.setServiceName("Corporate Bulk SMS");
				service11.setStatus(Status.Active);
				mServiceRepository.save(service11);
			}

			MService service12 = mServiceRepository.findServiceByCode("NOTC");
			if (service12 == null) {
				service12 = new MService();
				service12.setCode("NOTC");
				service12.setDescription("Corporate Notification");
				service12.setMaxAmount(1000);
				service12.setMinAmount(1);
				service12.setName("Corporate Notification");
				service12.setOperator(op);
				service12.setOperatorCode("NOTC");
				service12.setServiceType(sert);
				service12.setServiceName("Corporate Notification");
				service12.setStatus(Status.Active);
				mServiceRepository.save(service12);
			}

			MService service13 = mServiceRepository.findServiceByCode("CLP");
			if (service13 == null) {
				service13 = new MService();
				service13.setCode("CLP");
				service13.setDescription("Corporate List Partner");
				service13.setMaxAmount(1000);
				service13.setMinAmount(1);
				service13.setName("Corporate List Partner");
				service13.setOperator(op);
				service13.setOperatorCode("CLP");
				service13.setServiceType(sert);
				service13.setServiceName("Corporate List Partner");
				service13.setStatus(Status.Active);
				mServiceRepository.save(service13);
			}

			MService service14 = mServiceRepository.findServiceByCode("CAP");
			if (service14 == null) {
				service14 = new MService();
				service14.setCode("CAP");
				service14.setDescription("Corporate Add Partner");
				service14.setMaxAmount(1000);
				service14.setMinAmount(1);
				service14.setName("Corporate Add Partner");
				service14.setOperator(op);
				service14.setOperatorCode("CAP");
				service14.setServiceType(sert);
				service14.setServiceName("Corporate Add Partner");
				service14.setStatus(Status.Active);
				mServiceRepository.save(service14);
			}

			MService service15 = mServiceRepository.findServiceByCode("CNGPS");
			if (service15 == null) {
				service15 = new MService();
				service15.setCode("CNGPS");
				service15.setDescription("Corporate Change Password");
				service15.setMaxAmount(1000);
				service15.setMinAmount(1);
				service15.setName("Corporate Change Password");
				service15.setOperator(op);
				service15.setOperatorCode("CNGPS");
				service15.setServiceType(sert);
				service15.setServiceName("Corporate Change Password");
				service15.setStatus(Status.Active);
				mServiceRepository.save(service15);
			}

			MService service16 = mServiceRepository.findServiceByCode("GSRL");
			if (service16 == null) {
				service16 = new MService();
				service16.setCode("GSRL");
				service16.setDescription("Group Services Request");
				service16.setMaxAmount(1000);
				service16.setMinAmount(1);
				service16.setName("Group Services Request");
				service16.setOperator(op);
				service16.setOperatorCode("GSRL");
				service16.setServiceType(sert);
				service16.setServiceName("Group Services Request");
				service16.setStatus(Status.Active);
				mServiceRepository.save(service16);
			}

			MService service17 = mServiceRepository.findServiceByCode("GSAL");
			if (service17 == null) {
				service17 = new MService();
				service17.setCode("GSAL");
				service17.setDescription("Group Services Accept List");
				service17.setMaxAmount(1000);
				service17.setMinAmount(1);
				service17.setName("Group Services Accept");
				service17.setOperator(op);
				service17.setOperatorCode("GSAL");
				service17.setServiceType(sert);
				service17.setServiceName("Group Services Accept");
				service17.setStatus(Status.Active);
				mServiceRepository.save(service17);
			}

			MService service18 = mServiceRepository.findServiceByCode("GSRJ");
			if (service18 == null) {
				service18 = new MService();
				service18.setCode("GSRJ");
				service18.setDescription("Group Services Reject List");
				service18.setMaxAmount(1000);
				service18.setMinAmount(1);
				service18.setName("Group Services Reject");
				service18.setOperator(op);
				service18.setOperatorCode("GSRJ");
				service18.setServiceType(sert);
				service18.setServiceName("Group Services Reject");
				service18.setStatus(Status.Active);
				mServiceRepository.save(service18);
			}

			MService service19 = mServiceRepository.findServiceByCode("GSMS");
			if (service19 == null) {
				service19 = new MService();
				service19.setCode("GSMS");
				service19.setDescription("Group Bulk Sms");
				service19.setMaxAmount(1000);
				service19.setMinAmount(1);
				service19.setName("Group Bulk Sms");
				service19.setOperator(op);
				service19.setOperatorCode("GSMS");
				service19.setServiceType(sert);
				service19.setServiceName("Group Bulk Sms");
				service19.setStatus(Status.Active);
				mServiceRepository.save(service19);
			}

			MService service20 = mServiceRepository.findServiceByCode("GBLR");
			if (service20 == null) {
				service20 = new MService();
				service20.setCode("GBLR");
				service20.setDescription("Group Bulk Register");
				service20.setMaxAmount(1000);
				service20.setMinAmount(1);
				service20.setName("Group Bulk Register");
				service20.setOperator(op);
				service20.setOperatorCode("GBLR");
				service20.setServiceType(sert);
				service20.setServiceName("Bulk Register Group");
				service20.setStatus(Status.Active);
				mServiceRepository.save(service20);
			}

			MService service21 = mServiceRepository.findServiceByCode("CBLR");
			if (service21 == null) {
				service21 = new MService();
				service21.setCode("CBLR");
				service21.setDescription("Corporate Bulk Load Report");
				service21.setMaxAmount(1000);
				service21.setMinAmount(1);
				service21.setName("Corporate Bulk Load Report");
				service21.setOperator(op);
				service21.setOperatorCode("CBLR");
				service21.setServiceType(sert);
				service21.setServiceName("Corporate Bulk Load Report");
				service21.setStatus(Status.Active);
				mServiceRepository.save(service21);
			}

			MService service22 = mServiceRepository.findServiceByCode("CPR");
			if (service22 == null) {
				service22 = new MService();
				service22.setCode("CPR");
				service22.setDescription("Corporate Prefund Request");
				service22.setMaxAmount(1000);
				service22.setMinAmount(1);
				service22.setName("Corporate Prefund Request");
				service22.setOperator(op);
				service22.setOperatorCode("CPR");
				service22.setServiceType(sert);
				service22.setServiceName("Corporate Prefund Request");
				service22.setStatus(Status.Active);
				mServiceRepository.save(service22);
			}

			MService service23 = mServiceRepository.findServiceByCode("GBCA");
			if (service23 == null) {
				service23 = new MService();
				service23.setCode("GBCA");
				service23.setDescription("Group Bulk Card Assignment");
				service23.setMaxAmount(1000);
				service23.setMinAmount(1);
				service23.setName("Group Bulk Card Assignment");
				service23.setOperator(op);
				service23.setOperatorCode("GBCA");
				service23.setServiceType(sert);
				service23.setServiceName("Group Bulk Card Assignment");
				service23.setStatus(Status.Active);
				mServiceRepository.save(service23);
			}

			MService service24 = mServiceRepository.findServiceByCode("GBRF");
			if (service24 == null) {
				service24 = new MService();
				service24.setCode("GBRF");
				service24.setDescription("Group Bulk Register Fail List");
				service24.setMaxAmount(1000);
				service24.setMinAmount(1);
				service24.setName("Group Bulk Register Fail List");
				service24.setOperator(op);
				service24.setOperatorCode("GBRF");
				service24.setServiceType(sert);
				service24.setServiceName("Group Bulk Register Fail List");
				service24.setStatus(Status.Active);
				mServiceRepository.save(service24);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public CommonResponse createGroupAdmin(AddGroupRequest dto) {
		CommonResponse result = new CommonResponse();
		try {
			GroupDetails details = groupDetailsRepository.getGroupDetailsByEmail(dto.getEmail());
			if (details == null) {
				details = groupDetailsRepository.getGroupDetailsByContact(dto.getMobile());
				if (details == null) {
					result = saveGroupAdmin(dto);
					if (result.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
						details = groupDetailsRepository.getGroupDetailsByContact(dto.getMobile());

						/*
						 * List<GroupOtpMobile> groupOtp =
						 * groupOtpRepository.getGroupDetails(dto.getMobile()); if(groupOtp.isEmpty() ||
						 * groupOtp == null) { GroupOtpMobile gotp = new GroupOtpMobile();
						 * gotp.setContactName(details.getGroupName());
						 * gotp.setGroupName(details.getDname());
						 * gotp.setMobileNumber(details.getContactNo()); groupOtpRepository.save(gotp);
						 * }
						 */
						result.setStatus(ResponseStatus.SUCCESS.getKey());
						result.setCode(ResponseStatus.SUCCESS.getValue());
						result.setMessage("Group Added Successfully.");
						result.setSuccess(true);
						smsSenderApi.sendGroupCreateSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.CREATED_GROUP, details,
								dto.getMobile());
					} else {
						result.setCode(ResponseStatus.FAILURE.getValue());
						result.setMessage("Email already used");
					}
				} else {
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage("Contact no already exist");
				}
			} else {
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage("Email already exist");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	private CommonResponse saveGroupAdmin(AddGroupRequest dto) {
		CommonResponse response = new CommonResponse();
		try {
			MUser us = mUserRespository.findByUsernameAndAuthority(dto.getEmail());
			if (us == null) {
				GroupDetails gd = new GroupDetails();
				gd.setContactNo(dto.getMobile());
				gd.setDname(dto.getDisplayname());
				gd.setEmail(dto.getEmail());
				gd.setGroupName(dto.getGroupName());
				gd.setStatus(Status.Active.getValue());
				gd.setCorporate(dto.isCorporate());

				List<MService> assignService = new ArrayList<MService>();

				if (dto.isBulkRegister()) {
					MService s1 = mServiceRepository.findServiceByCode("BLR");
					assignService.add(s1);
				}
				if (dto.isBulkkyc()) {
					MService service3 = mServiceRepository.findServiceByCode("BKY");
					assignService.add(service3);
				}
				if (dto.isBulkcardload()) {
					MService service2 = mServiceRepository.findServiceByCode("BCL");
					assignService.add(service2);
				}
				if (dto.isSincarload()) {
					MService service4 = mServiceRepository.findServiceByCode("CSCL");
					assignService.add(service4);
				}
				if (dto.isSingcarassign()) {
					MService service5 = mServiceRepository.findServiceByCode("GBCA");
					assignService.add(service5);
				}
				if (dto.isPrefundHistory()) {
					MService service6 = mServiceRepository.findServiceByCode("CPH");
					assignService.add(service6);
				}
				if (dto.isUserReport()) {
					MService service7 = mServiceRepository.findServiceByCode("CUR");
					assignService.add(service7);
				}
				if (dto.isFailedReg()) {
					MService service8 = mServiceRepository.findServiceByCode("CFR");
					assignService.add(service8);
				}
				if (dto.isFailedLoad()) {
					MService service9 = mServiceRepository.findServiceByCode("CFLL");
					assignService.add(service9);
				}
				if (dto.isBlockedCard()) {
					MService service10 = mServiceRepository.findServiceByCode("CBCL");
					assignService.add(service10);
				}
				if (dto.isBulkSms()) {
					MService service11 = mServiceRepository.findServiceByCode("BSMS");
					assignService.add(service11);
				}
				if (dto.isNoticenter()) {
					MService service12 = mServiceRepository.findServiceByCode("NOTC");
					assignService.add(service12);
				}
				if (dto.isListPart()) {
					MService service13 = mServiceRepository.findServiceByCode("CLP");
					assignService.add(service13);
				}
				if (dto.isAddPartner()) {
					MService service14 = mServiceRepository.findServiceByCode("CAP");
					assignService.add(service14);
				}
				if (dto.isGroupRequestList()) {
					MService service16 = mServiceRepository.findServiceByCode("GSRL");
					assignService.add(service16);
				}
				if (dto.isGroupAcceptList()) {
					MService service17 = mServiceRepository.findServiceByCode("GSAL");
					assignService.add(service17);
				}
				if (dto.isGroupRejectList()) {
					MService service18 = mServiceRepository.findServiceByCode("GSRJ");
					assignService.add(service18);
				}
				if (dto.isBulkCardLoadList()) {
					MService service19 = mServiceRepository.findServiceByCode("CBLR");
					assignService.add(service19);
				}
				if (dto.isGroupBulkRegister()) {
					MService service20 = mServiceRepository.findServiceByCode("GBLR");
					assignService.add(service20);
				}
				if (dto.isPrefundRequest()) {
					MService service21 = mServiceRepository.findServiceByCode("CPR");
					assignService.add(service21);
				}
				/*
				 * if(dto.isGroupBulkRegisterFail()) { MService service22 =
				 * mServiceRepository.findServiceByCode("GBRF"); assignService.add(service22); }
				 */
				/*
				 * for(int i = 0; i < assignService.size(); i++) {
				 * System.err.println(assignService.get(i).getDescription()); }
				 */
				// gd.setService(assignService);
				groupDetailsRepository.save(gd);

				us = new MUser();
				us = new MUser();
				us.setUserType(UserType.Group);
				us.setUsername(dto.getEmail());
				us.setMobileStatus(Status.Active);
				us.setAuthority(Authorities.GROUP + "," + Authorities.AUTHENTICATED);
				us.setGroupDetails(gd);
				us.setEmailStatus(Status.Active);
				us.setMobileStatus(Status.Active);
				us.setPassword(passwordEncoder.encode(dto.getMobile()));

				MUserDetails ud = new MUserDetails();
				ud.setContactNo(dto.getMobile());
				ud.setEmail(dto.getEmail());
				ud.setCountryName("India");
				ud.setFirstName(dto.getGroupName());
				ud.setLastName(dto.getGroupName());
				ud.setGroupStatus(Status.Closed.getValue());
				us.setUserDetail(ud);
				mUserDetailRepository.save(ud);
				mUserRespository.save(us);

				MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("NONKYC");
				MPQAccountDetails pqAccountDetail = new MPQAccountDetails();
				pqAccountDetail.setBalance(0);
				pqAccountDetail.setAccountType(nonKYCAccountType);
				pqAccountDetail.setAccountNumber(SMSUtil.BASE_ACCOUNT_NUMBER + ud.getId());
				mPQAccountDetailRepository.save(pqAccountDetail);

				us.setAccountDetail(pqAccountDetail);
				mUserRespository.save(us);

				response.setStatus(ResponseStatus.SUCCESS.getKey());
				response.setCode(ResponseStatus.SUCCESS.getValue());
				response.setMessage("Group details added successfully.");
				response.setSuccess(true);

			} else {
				response.setStatus(ResponseStatus.FAILURE.getKey());
				response.setCode(ResponseStatus.FAILURE.getValue());
				response.setMessage("Group Already exist.");
				response.setSuccess(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public ResponseDTO getGroupServiceList(MUser user) {
		ResponseDTO result = new ResponseDTO();
		try {
			List<MService> slist = groupDetailsRepository.getServiceByContact(user.getUserDetail().getContactNo());
			if (slist != null) {
				if (!(slist == null)) {
					result.setDetails(slist);
					result.setCode(ResponseStatus.SUCCESS.getValue());

					System.err.println("service list: " + String.valueOf(slist));
					System.err.println(String.valueOf(result.getDetails()));
				}
				result.setCode(ResponseStatus.SUCCESS.getValue());
			}
			System.err.println("service list: " + String.valueOf(slist));
			System.err.println(String.valueOf(result.getDetails()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	private CommonResponse savegroup(GroupDetailDTO dto, CommonResponse result) {
		try {

			MUser u = mUserRespository.findByUsernameAndAuthority(dto.getEmailAddressId());
			if (u == null) {
				GroupDetails gd = new GroupDetails();
				byte[] byteArr = null;

				gd.setAddress(dto.getAddress());
				gd.setEmail(dto.getEmailAddressId());
				gd.setCity(dto.getCity());
				gd.setContactNo(dto.getMobileNoId());
				gd.setCountry(dto.getCountry());
				gd.setState(dto.getState());
				gd.setGroupName(dto.getName());
				gd.setDname(dto.getDname());
				gd.setStatus(Status.Active.getValue());
				try {
					byteArr = Base64.getDecoder().decode(Base64.getEncoder().encodeToString(dto.getImage().getBytes()));
				} catch (Exception e) {
					e.printStackTrace();
				}
				gd.setImageContent(byteArr);
				gd.setImage(dto.getImage().getContentType());

				List<MService> assignService = new ArrayList<MService>();

				if (dto.isGroupRequestList()) {
					MService service = mServiceRepository.findServiceByCode("GSRL");
					assignService.add(service);
				}

				if (dto.isGroupAcceptList()) {
					MService service = mServiceRepository.findServiceByCode("GSAL");
					assignService.add(service);
				}

				if (dto.isGroupRejectList()) {
					MService service = mServiceRepository.findServiceByCode("GSRJ");
					assignService.add(service);
				}

				if (dto.isGroupBulkRegister()) {
					MService service = mServiceRepository.findServiceByCode("GBLR");
					assignService.add(service);
				}

				if (dto.isBulkCardAssign()) {
					MService service = mServiceRepository.findServiceByCode("GBCA");
					assignService.add(service);
				}

				// if (dto.isBulkKyc()) {
				// MService service = mServiceRepository.findServiceByCode("GBCA");
				// assignService.add(service);
				// }

				if (dto.isBulkSms()) {
					MService service = mServiceRepository.findServiceByCode("BSMS");
					assignService.add(service);
				}

				if (dto.isNotiCenter()) {
					MService service = mServiceRepository.findServiceByCode("NOTC");
					assignService.add(service);
				}

				gd.setService(assignService);
				groupDetailsRepository.save(gd);

				u = new MUser();
				u.setUserType(UserType.Group);
				u.setUsername(dto.getEmailAddressId());
				u.setMobileStatus(Status.Inactive);
				u.setAuthority(Authorities.GROUP + "," + Authorities.AUTHENTICATED);
				u.setGroupDetails(gd);
				u.setEmailStatus(Status.Active);
				u.setMobileStatus(Status.Active);
				u.setPassword(passwordEncoder.encode(dto.getMobileNoId()));

				MUserDetails ud = new MUserDetails();
				ud.setContactNo(dto.getMobileNoId());
				ud.setEmail(dto.getEmailAddressId());
				ud.setCountryName(dto.getCountry());
				ud.setFirstName(dto.getName());
				ud.setLastName(dto.getName());
				ud.setGroupStatus(Status.Closed.getValue());
				u.setUserDetail(ud);
				mUserDetailRepository.save(ud);
				mUserRespository.save(u);

				MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("NONKYC");
				MPQAccountDetails pqAccountDetail = new MPQAccountDetails();
				pqAccountDetail.setBalance(0);
				pqAccountDetail.setAccountType(nonKYCAccountType);
				pqAccountDetail.setAccountNumber(SMSUtil.BASE_ACCOUNT_NUMBER + ud.getId());
				mPQAccountDetailRepository.save(pqAccountDetail);

				u.setAccountDetail(pqAccountDetail);
				mUserRespository.save(u);

				result.setStatus(ResponseStatus.SUCCESS.getKey());
				result.setCode(ResponseStatus.SUCCESS.getValue());
				result.setMessage("Group details added successfully.");
				result.setSuccess(true);
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage("Group Already exist.");
				result.setSuccess(false);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<MUser> groupAddRequest(UserDataRequestDTO dto) {
		List<MUser> lUserDTO = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			logger.info("date from: " + dto.getFrom());
			logger.info("to : " + dto.getTo());
			logger.info("status:: " + Status.Inactive.getValue());

			Page<MUser> user = mUserRespository.getAllRequestListGroup(pageable, Status.Inactive.getValue(), true);
			if (user != null) {
				lUserDTO = user.getContent();
			} else {
				throw new Exception("session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<GroupDetails> getGroupDetailsAdmin() {
		List<GroupDetails> list = new ArrayList<GroupDetails>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<GroupDetails> page = groupDetailsRepository.getGroupNameAdmin(pageable);
			if (page != null) {
				list = page.getContent();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<Donation> getDonationDetails() {
		List<Donation> list = new ArrayList<Donation>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<Donation> page = donationRepository.getDonationDetails(pageable);
			if (page != null) {
				list = page.getContent();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public CommonResponse acceptGroupAddRequest(String contactNo, CommonResponse result) {
		try {
			MUser u = mUserRespository.findByUsername(contactNo);
			if (u != null) {
				MUserDetails detail = u.getUserDetail();
				GroupDetails g = u.getGroupDetails();
				if (detail.getGroupStatus().equals(Status.Inactive.getValue())) {
					detail.setGroupStatus(Status.Active.getValue());
					mUserDetailRepository.save(detail);
					if (detail.isGroupChange() == true) {
						GroupDetails gr = groupDetailsRepository.getGroupDetailsByName(detail.getChangedGroupName());
						if (gr != null) {
							detail.setGroupChange(false);
							mUserDetailRepository.save(detail);
							u.setGroupDetails(gr);
							mUserRespository.save(u);
						} else {
							result.setCode(ResponseStatus.FAILURE.getValue());
							result.setMessage("Group does not exist");
						}
					}
					smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.GROUP_ADD, u, null);
					smsSenderApi.sendGroupSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.GROUP_MESSAGE, g, u, null);
					result.setCode(ResponseStatus.SUCCESS.getValue());
					result.setSuccess(true);
					result.setMessage("User added to Group Successfully");
					result.setStatus(ResponseStatus.SUCCESS.getKey());
				} else {
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setSuccess(false);
					result.setMessage("User Already in Group");
				}
			} else {
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setSuccess(false);
				result.setMessage("User Not added to Group");
				result.setStatus(ResponseStatus.FAILURE.getKey());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public CommonResponse rejectGroupRequest(PagingDTO dto, CommonResponse result) {
		try {
			MUser u = mUserRespository.findByUsername(dto.getUsername());
			if (u != null) {
				MUserDetails detail = u.getUserDetail();
				GroupDetails g = u.getGroupDetails();
				if (detail.getGroupStatus().equals(Status.Inactive.getValue())) {
					detail.setGroupStatus(Status.Cancelled.getValue());
					detail.setGroupRejectReason(dto.getStatus());
					detail.setPreviousGroupContact(g.getContactNo());
					mUserDetailRepository.save(detail);

					smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.REJECTED_GROUP, u, null);
					smsSenderApi.sendGroupSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.GROUP_MESSAGE_REJECTED, g, u, null);
					GroupDetails gr = groupDetailsRepository.getGroupDetailsByName("None");
					u.setGroupDetails(gr);
					mUserRespository.save(u);
					if (detail.isGroupChange() == true) {
						detail.setGroupChange(false);
						mUserDetailRepository.save(detail);
					}

					result.setCode(ResponseStatus.SUCCESS.getValue());
					result.setStatus(ResponseStatus.SUCCESS.getKey());
					result.setMessage("User is Rejected From Group");
					result.setSuccess(true);
				} else if (detail.getGroupStatus().equals(Status.Active.getValue())) {
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setMessage("User Already in Group");
					result.setSuccess(false);
				} else if (detail.getGroupStatus().equals(Status.Cancelled.getValue())) {
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setMessage("User is Rejected");
					result.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public CommonResponse deleteUserFromGroup(PagingDTO page, CommonResponse result) {
		try {
			MUser u = mUserRespository.findByUsername(page.getUsername());
			if (u != null) {
				MUserDetails detail = u.getUserDetail();
				if (detail.getGroupStatus().equalsIgnoreCase(Status.Active.getValue())) {
					detail.setGroupStatus(Status.Deleted.getValue());
					detail.setDeletionGroupReason(page.getStatus());
					mUserDetailRepository.save(detail);
					GroupDetails group = groupDetailsRepository.getGroupDetailsByName("None");
					if (group != null) {
						u.setGroupDetails(group);
						mUserRespository.save(u);
						result.setCode(ResponseStatus.SUCCESS.getValue());
						result.setStatus(ResponseStatus.SUCCESS.getKey());
						result.setMessage("User is Deleted From Group");
						result.setSuccess(true);
					} else {
						result.setCode(ResponseStatus.FAILURE.getValue());
						result.setStatus(ResponseStatus.FAILURE.getKey());
						result.setMessage("Group does not exist");
						result.setSuccess(false);
					}
				} else {
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setMessage("User is Inactive");
					result.setSuccess(false);
				}
			} else {
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setMessage("User Does not exist");
				result.setSuccess(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	// group sms
	@Override
	public void sendGroupSMS(String groupName, String text) {
		GroupDetails gd = groupDetailsRepository.getGroupDetailsByName(groupName);
		List<MUser> mobileList = mUserRespository.getGroupUsersSms(gd);
		for (MUser mobile : mobileList) {
			GroupSms g = new GroupSms();
			g.setCronStatus(false);
			g.setGroupName(gd.getGroupName());
			g.setMobile(mobile.getUserDetail().getContactNo());
			g.setSingle(false);
			g.setStatus(Status.Inactive.getValue());
			g.setText(text);
			groupSmsRepository.save(g);
		}
	}

	@Override
	public GroupDetails getGroupDetails(String groupName) {
		GroupDetails gd = groupDetailsRepository.getGroupDetailsByName(groupName);
		if (gd != null) {
			return gd;
		} else {
			GroupDetails gr = groupDetailsRepository.getGroupDetailsByName("None");
			return gr;
		}
	}

	// send single sms
	@Override
	public void sendSingleGroupSMS(String mobile, String text) {
		smsSenderApi.sendUserGroupSms(text, mobile);
	}

	// bulk sms
	@Override
	public void sendBulkSMS() {
		List<String> mobileList = mUserRespository.getAllActiveUsername(UserType.User);
		for (String mobile : mobileList) {
			sendSingleSMS(mobile);
		}
	}

	@Override
	public void sendSingleSMS(String mobile) {
		MUser user = findByUserName(mobile);
		if (user != null) {
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.BULK_SMS, user, null);
		}
	}

	@Override
	public List<MUser> groupAcceptedUserByContact(UserDataRequestDTO dto) {
		List<MUser> lUserDTO = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			logger.info("date from: " + dto.getFrom());
			logger.info("to : " + dto.getTo());
			logger.info("status:: " + Status.Inactive.getValue());

			Page<MUser> user = mUserRespository.acceptedGroupStatusByContact(pageable, dto.getEmail(),
					Status.Active.getValue());
			if (user != null) {
				lUserDTO = user.getContent();
			} else {
				throw new Exception("session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<MUser> acceptedUserByContactPostGroup(UserDataRequestDTO dto) {
		List<MUser> lUserDTO = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			logger.info("date from: " + dto.getFrom());
			logger.info("to : " + dto.getTo());
			logger.info("status:: " + Status.Inactive.getValue());

			Page<MUser> user = mUserRespository.acceptedGroupStatusByContactPost(pageable, dto.getFrom(), dto.getTo(),
					dto.getEmail(), Status.Active.getValue());
			if (user != null) {
				lUserDTO = user.getContent();
			} else {
				throw new Exception("session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<MUser> groupRejectedUserByContact(UserDataRequestDTO dto) {
		List<MUser> lUserDTO = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			logger.info("date from: " + dto.getFrom());
			logger.info("to : " + dto.getTo());
			logger.info("status:: " + Status.Inactive.getValue());

			Page<MUser> user = mUserRespository.rejectedGroupByContactStatusGet(pageable, dto.getContactNo(),
					Status.Cancelled.getValue());
			if (user != null) {
				lUserDTO = user.getContent();
			} else {
				throw new Exception("session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<MUser> groupRejectedUserByContactPost(UserDataRequestDTO dto) {
		List<MUser> lUserDTO = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			logger.info("date from: " + dto.getFrom());
			logger.info("to : " + dto.getTo());
			logger.info("status:: " + Status.Inactive.getValue());

			Page<MUser> user = mUserRespository.rejectedGroupByContactStatus(pageable, dto.getFrom(), dto.getTo(),
					dto.getContactNo(), Status.Cancelled.getValue());
			if (user != null) {
				lUserDTO = user.getContent();
			} else {
				throw new Exception("session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	// Admin

	@Override
	public List<MUser> groupAcceptedUser(UserDataRequestDTO dto) {
		List<MUser> lUserDTO = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			logger.info("date from: " + dto.getFrom());
			logger.info("to : " + dto.getTo());
			logger.info("status:: " + Status.Inactive.getValue());

			Page<MUser> user = mUserRespository.acceptedGroupStatusGet(pageable, Status.Active.getValue());
			if (user != null) {

				lUserDTO = user.getContent();
				for (int i = 0; i < lUserDTO.size(); i++) {
					System.err.println("accepted user :: " + lUserDTO.get(i).getUsername());
				}
			} else {
				throw new Exception("session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<MUser> groupAcceptedUserPost(UserDataRequestDTO dto) {
		List<MUser> lUserDTO = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			logger.info("date from: " + dto.getFrom());
			logger.info("to : " + dto.getTo());
			logger.info("status:: " + Status.Inactive.getValue());

			Page<MUser> user = mUserRespository.acceptedGroupStatus(pageable, dto.getFrom(), dto.getTo(),
					Status.Active.getValue());
			if (user != null) {
				lUserDTO = user.getContent();
			} else {
				throw new Exception("session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<MUser> groupRejectedUser(UserDataRequestDTO dto) {
		List<MUser> lUserDTO = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			logger.info("date from: " + dto.getFrom());
			logger.info("to : " + dto.getTo());
			logger.info("status:: " + Status.Inactive.getValue());

			Page<MUser> user = mUserRespository.rejectedGroupStatusGet(pageable, Status.Cancelled.getValue());
			if (user != null) {
				lUserDTO = user.getContent();
			} else {
				throw new Exception("session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<MUser> groupRejectedUserPost(UserDataRequestDTO dto) {
		List<MUser> lUserDTO = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			logger.info("date from: " + dto.getFrom());
			logger.info("to : " + dto.getTo());
			logger.info("status:: " + Status.Inactive.getValue());

			Page<MUser> user = mUserRespository.rejectedGroupStatus(pageable, dto.getFrom(), dto.getTo(),
					Status.Cancelled.getValue());
			if (user != null) {
				lUserDTO = user.getContent();
			} else {
				throw new Exception("session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<MUser> getDeletedUser(UserDataRequestDTO dto) {
		List<MUser> lUserDTO = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			logger.info("date from: " + dto.getFrom());
			logger.info("to : " + dto.getTo());
			Page<MUser> user = mUserRespository.findAllUserByGroupStatusDeleted(pageable, dto.getFrom(), dto.getTo(),
					Status.Deleted.getValue());
			if (user != null) {
				lUserDTO = user.getContent();
			} else {
				throw new Exception("session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public ResponseDTO saveDonatee(GroupDetailDTO dto) {
		ResponseDTO result = new ResponseDTO();
		try {
			logger.info("contact :: " + dto.getMobileNoId());
			Donation donate = donationRepository.getDonation(dto.getMobileNoId());
			if (donate == null) {
				donate = new Donation();
				donate.setContactNo(dto.getMobileNoId());
				donate.setEmail(dto.getEmailAddressId());
				donate.setDonateeName(dto.getName());
				donate.setAccount(dto.getAccount());
				donate.setOrganization(dto.getOrganization());
				donate.setMessage(dto.getRemark());
				donationRepository.save(donate);

				result.setSuccess(true);
				result.setCode(ResponseStatus.SUCCESS.getValue());
				result.setStatus(ResponseStatus.SUCCESS.getKey());
				result.setMessage(ErrorMessage.DONATEE);

				smsSenderApi.sendUserSMSDonation(SMSAccount.PAYQWIK_OTP, SMSTemplete.DONATION_ADD, dto.getMobileNoId());
			} else {
				result.setSuccess(false);
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setMessage(ErrorMessage.DONATEE_EXIST);
			}
		} catch (Exception e) {
			result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
			result.setMessage(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
			result.setSuccess(false);
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<GroupDetails> getGroupListGet(PagingDTO dto) {
		List<GroupDetails> lUserDTO = new ArrayList<GroupDetails>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<GroupDetails> page = groupDetailsRepository.getGroupListGet(pageable);
			if (page != null) {
				lUserDTO = page.getContent();
			} else {
				throw new Exception("Session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<BulkCardAssignmentGroup> getgroupBulkCarFailList(String email) {
		List<BulkCardAssignmentGroup> list = new ArrayList<>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<BulkCardAssignmentGroup> page = bulkCardAssignmentGroupRepository.getBulkCardFailList(pageable,
					Status.Failed, email);
			if (page != null) {
				list = page.getContent();
			} else {
				throw new Exception("Session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<BulkCardAssignmentGroup> getgroupBulkCarFailListPost(UserDataRequestDTO dto) {
		List<BulkCardAssignmentGroup> list = new ArrayList<>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<BulkCardAssignmentGroup> page = bulkCardAssignmentGroupRepository.getBulkCardFailListPost(pageable,
					dto.getFrom(), dto.getTo(), Status.Failed, dto.getEmail());
			if (page != null) {
				list = page.getContent();
			} else {
				throw new Exception("Session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<GroupBulkRegister> getgroupBulkRegisterFailList(String email) {
		List<GroupBulkRegister> list = new ArrayList<>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<GroupBulkRegister> page = groupBulkRegisterRepository.getBulkRegisterFailList(pageable, false, email);
			if (page != null) {
				list = page.getContent();
			} else {
				throw new Exception("Session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<GroupBulkRegister> getgroupBulkRegisterFailListPost(UserDataRequestDTO dto) {
		List<GroupBulkRegister> list = new ArrayList<>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<GroupBulkRegister> page = groupBulkRegisterRepository.getBulkRegisterFailListPost(pageable,
					dto.getFrom(), dto.getTo(), false, dto.getEmail());
			if (page != null) {
				list = page.getContent();
			} else {
				throw new Exception("Session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<GroupBulkKyc> getgroupBulkKycFailList(String email) {
		List<GroupBulkKyc> list = new ArrayList<>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<GroupBulkKyc> page = groupBulkKycRepository.getBulkKycFailList(pageable, false, email);
			if (page != null) {
				list = page.getContent();
			} else {
				throw new Exception("Session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<GroupBulkKyc> getgroupBulkLKycFailListPost(UserDataRequestDTO dto) {
		List<GroupBulkKyc> list = new ArrayList<>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<GroupBulkKyc> page = groupBulkKycRepository.getBulkKycFailListPost(pageable, dto.getFrom(),
					dto.getTo(), false, dto.getEmail());
			if (page != null) {
				list = page.getContent();
			} else {
				throw new Exception("Session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<GroupDetails> getGroupListPost(PagingDTO dto) {
		List<GroupDetails> lUserDTO = new ArrayList<GroupDetails>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<GroupDetails> page = groupDetailsRepository.getGroupListPost(pageable, dto.getFromDate(),
					dto.getToDate());
			if (page != null) {
				lUserDTO = page.getContent();
			} else {
				throw new Exception("Session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<GroupDetails> getSingleGroupList(PagingDTO dto) {
		List<GroupDetails> lUserDTO = new ArrayList<GroupDetails>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<GroupDetails> page = groupDetailsRepository.getGroupListPost(pageable, dto.getUserContactNo());
			if (page != null) {
				lUserDTO = page.getContent();
			} else {
				throw new Exception("Session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<Donation> addDonationList(UserDataRequestDTO dto) {
		List<Donation> lUserDTO = new ArrayList<Donation>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<Donation> page = donationRepository.getDonationDetailsDate(pageable);
			if (page != null) {
				lUserDTO = page.getContent();
			} else {
				throw new Exception("session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<Donation> addDonationListPost(UserDataRequestDTO dto) {
		List<Donation> lUserDTO = new ArrayList<Donation>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<Donation> page = donationRepository.getDonationDetailsDatePost(pageable, dto.getFrom(), dto.getTo());
			if (page != null) {
				lUserDTO = page.getContent();
			} else {
				throw new Exception("session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<DonationDTO> donationListBasedOnGroup(UserDataRequestDTO dto) {
		List<Donation> lUserDTO = new ArrayList<Donation>();
		List<DonationDTO> list = new ArrayList<DonationDTO>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<Donation> page = donationRepository.getDonationDetailsDatePost(pageable, dto.getFrom(), dto.getTo());
			if (page != null) {
				lUserDTO = page.getContent();
				for (int i = 0; i < lUserDTO.size(); i++) {
					DonationDTO userList = new DonationDTO();
					MUser u = mUserRespository.getUserBasedOnUserNameAndAuthority(lUserDTO.get(i).getContactNo(),
							Status.Active);
					if (u != null) {
						userList.setDonateeName(lUserDTO.get(i).getDonateeName());
						userList.setContactNo(lUserDTO.get(i).getContactNo());
						userList.setGroupName(u.getGroupDetails().getGroupName());
						userList.setOrganization(lUserDTO.get(i).getOrganization());
						userList.setEmail(lUserDTO.get(i).getEmail());
						userList.setCreated(lUserDTO.get(i).getCreated().toLocaleString());
						list.add(userList);
					} else {
						continue;
					}
				}
			} else {
				throw new Exception("session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public boolean resendMobileTokenDeviceBindingGroup(String username) {

		String otp = CommonUtil.generateSixDigitNumericString();

		MUser u = mUserRespository.getUserBasedOnUserNameAndAuthorityGroup(username, Status.Active);
		if (u == null) {
			return false;
		} else {
			// List<String> mobileNumbers =
			// groupOtpRepository.getMobileGroup(u.getGroupDetails().getDname());
			u.setMobileToken(otp);
			mUserRespository.save(u);
			// for(int i = 0; i < mobileNumbers.size(); i++) {
			smsSenderApi.sendUserSMSForAdmin(SMSAccount.PAYQWIK_OTP, SMSTemplete.REGENERATE_OTP,
					u.getGroupDetails().getContactNo(), otp);
			// }
			return true;
		}
	}

	@Override
	public boolean resendMobileTokenDeviceBinding(String username) {
		List<String> users = otpRepository.getMobileNumbers();
		String otp = CommonUtil.generateSixDigitNumericString();
		for (int i = 0; i < users.size(); i++) {
			smsSenderApi.sendUserSMSForAdmin(SMSAccount.PAYQWIK_OTP, SMSTemplete.REGENERATE_OTP, users.get(i), otp);
		}
		MUser u = mUserRespository.findByUsernameAndStatus(username, Status.Active);
		if (u == null) {
			return false;
		} else {
			u.setMobileToken(otp);
			mUserRespository.save(u);
			return true;
		}
	}

	@Override
	public ResponseDTO loadCardOtp() {
		ResponseDTO result = new ResponseDTO();
		List<LoadCardOtp> users = loadCardOtpRepository.getLoadCardList();
		String otp = CommonUtil.generateSixDigitNumericString();
		for (int i = 0; i < users.size(); i++) {
			smsSenderApi.sendUserSMSForAdmin(SMSAccount.PAYQWIK_OTP, SMSTemplete.REGENERATE_OTP,
					users.get(i).getMobile(), otp);
			users.get(i).setOtp(otp);
			loadCardOtpRepository.save(users);
		}
		result.setCode(ResponseStatus.SUCCESS.getValue());
		result.setMessage("OTP sent");
		result.setStatus(otp);
		return result;
	}

	@Override
	public String inactivePartener(MUser partner) {
		if (partner != null) {
			PartnerDetails details = partnerDetailsRepository.getPartnerDetails(partner);
			if (details != null) {
				details.setStatus(Status.Inactive);
				partner.setMobileStatus(Status.Inactive);
				partnerDetailsRepository.save(details);
				mUserRespository.save(partner);
				return ResponseStatus.SUCCESS.getKey();
			}
		}
		return null;
	}

	@Override
	public MUser getPartnerById(long parseLong) {
		return mUserRespository.findOne(parseLong);
	}

	@Override
	public List<MPQAccountDetails> getUserAccounts(PartnerDetails partner) {
		if (partner != null) {
			List<BulkRegister> register = bulkRegisterRepository.getPartnerUsers(partner);
			if (register != null) {
				List<MPQAccountDetails> accounts = new ArrayList<>();
				for (BulkRegister bulkRegister : register) {
					accounts.add(bulkRegister.getUser().getAccountDetail());
				}
				return accounts;
			}
		}
		return null;
	}

	@Override
	public PartnerDetails getPartnerDetails(long parseLong) {
		return partnerDetailsRepository.getPartnerById(parseLong, Status.Active);
	}

	@Override
	public List<BusTicket> getBusDetailsForAdminByDate(Date from, Date to) {
		List<BusTicket> lUserDTO = new ArrayList<BusTicket>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);
			Page<BusTicket> page = busTicketRepository.getBusTicketByDate(pageable, from, to);
			if (page != null) {
				lUserDTO = page.getContent();
				logger.info("date from :: " + from);
				logger.info("date to:: " + to);
			} else {
				throw new Exception("session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public CommonResponse setMultipleAdmin(SuperAdminDTO dto) {
		CommonResponse result = new CommonResponse();
		try {
			String role = "ROLE_ADMINISTRATOR,ROLE_AUTHENTICATED";
			MUser user = mUserRespository.findByUsernameAndAuthority(dto.getEmail(), role);
			if (user == null) {
				user = new MUser();
				user.setCorporateUser(false);
				user.setUsername(dto.getEmail());
				user.setAuthority(Authorities.ADMINISTRATOR + "," + Authorities.AUTHENTICATED);
				user.setPassword(passwordEncoder.encode(dto.getMobile()));
				user.setMobileStatus(Status.Active);
				user.setEmailStatus(Status.Active);
				user.setUserType(UserType.Admin);
				user.setMobileToken(CommonUtil.generateSixDigitNumericString());
				user.setEmailToken("E" + System.currentTimeMillis());
				user.setFullyFilled(true);
				user.setDeviceLocked(false);

				GroupDetails detail = groupDetailsRepository.getGroupDetailsByName("None");
				user.setGroupDetails(detail);

				MUserDetails userDetail = new MUserDetails();
				userDetail.setAddress("Bangalore");
				userDetail.setContactNo(dto.getMobile());
				userDetail.setFirstName(dto.getFirstName());
				userDetail.setLastName(dto.getLastName());
				userDetail.setGroupStatus(Status.Inactive.getValue());
				userDetail.setEmail(dto.getEmail());
				userDetail.setDateOfBirth(formatter.parse(dto.getDob()));
				userDetail.setIdType(dto.getIdType());
				userDetail.setIdNo(dto.getIdNumber());

				user.setUserDetail(userDetail);

				MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("NONKYC");
				MPQAccountDetails pqAccountDetail = new MPQAccountDetails();
				pqAccountDetail.setBalance(0);
				pqAccountDetail.setAccountType(nonKYCAccountType);
				pqAccountDetail.setAccountNumber(SMSUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
				mPQAccountDetailRepository.save(pqAccountDetail);

				user.setAccountDetail(pqAccountDetail);
				mUserRespository.save(user);

				result.setCode(ResponseStatus.SUCCESS.getValue());
				result.setMessage("Admin Added Successfully");
			} else {
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage("User already exist");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<MUser> getAddedAdminList(PagingDTO dto) {
		List<MUser> userList = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<MUser> page = mUserRespository.getAllAddedAdmin(pageable, UserType.Admin);
			if (page != null) {
				userList = page.getContent();
			} else {
				throw new Exception("session is null");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return userList;
	}

	@Override
	public Page<MUser> findByNameOrEmailorUsername(RequestDTO dto) {
		Sort sort = new Sort(Direction.DESC, "id");
		Pageable pageable = new PageRequest(Integer.parseInt(dto.getPage()), Integer.parseInt(dto.getSize()), sort);
		Page<MUser> user = mUserRespository.findByNameOrEmailorUsername(dto.getUserName(), pageable);
		return user;
	}

	@Override
	public Page<MMCards> findByNameOrEmailOrContactNoOrCardHashId(PagingDTO dto) {
		Sort sort = new Sort(Direction.DESC, "id");
		Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
		Page<MMCards> user = mmCardRepository.findByNameOrEmailOrContactNoOrCardHashId(dto.getUsername(), pageable);
		return user;
	}

	@Override
	public Page<MMCards> findPhysicalCardByNameOrEmailOrContactNoOrCardHashId(PagingDTO dto) {
		Sort sort = new Sort(Direction.DESC, "id");
		Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
		Page<MMCards> user = mmCardRepository.findPhysicalCardByNameOrEmailOrContactNoOrCardHashId(dto.getUsername(),
				pageable);
		return user;
	}
}
