package com.msscard.app.api.impl;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang.StringEscapeUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.CMYKColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.msscard.app.api.IFlightApi;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.ITransactionApi;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.sms.util.SMSAccount;
import com.msscard.app.sms.util.SMSTemplete;
import com.msscard.entity.ERCredentialsManager;
import com.msscard.entity.FlightAirLineList;
import com.msscard.entity.FlightDetails;
import com.msscard.entity.FlightTicket;
import com.msscard.entity.FlightTravellers;
import com.msscard.entity.MCommission;
import com.msscard.entity.MPQAccountDetails;
import com.msscard.entity.MService;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.model.CompareCountry;
import com.msscard.model.EngineID;
import com.msscard.model.FlghtTicketResp;
import com.msscard.model.FlightPayment;
import com.msscard.model.FlightPaymentRequest;
import com.msscard.model.FlightPaymentResponse;
import com.msscard.model.FlightResponseEmail;
import com.msscard.model.FlightTicketdtoForname;
import com.msscard.model.PagingDTO;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Status;
import com.msscard.model.TicketDetailsDTOFlight;
import com.msscard.model.TicketsResp;
import com.msscard.model.TravelFlightRequest;
import com.msscard.repositories.ERCredentialManager;
import com.msscard.repositories.FlightDetailsRepository;
import com.msscard.repositories.FlightListRepository;
import com.msscard.repositories.FlightTicketRepository;
import com.msscard.repositories.FlightTravellerDetailsRepository;
import com.msscard.repositories.MTransactionRepository;
import com.msscard.util.CommonUtil;
import com.msscard.util.JSONParserUtil;
import com.msscard.util.MailTemplate;
import com.msscard.util.MatchMoveUtil;
import com.msscard.util.ServiceCodeUtil;
import com.msscard.validation.TransactionTypeValidation;

public class FlightApi implements IFlightApi{
	
	private static final String pdfUrlLive="/usr/local/tomcat1/webapps/ROOT/WEB-INF/classes/HelloWorld.pdf";
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());	
	
	private final ERCredentialManager eRCredentialManager;
	private final ITransactionApi transactionApi;
	private final FlightDetailsRepository flightDetailsRepository;
	private final FlightListRepository flightListRepository;
	private final TransactionTypeValidation transactionTypeValidation;
	private final FlightTicketRepository flightTicketRepository;
	private final FlightTravellerDetailsRepository flightTravellerDetailsRepository;
	private final MTransactionRepository mTransactionRepository;
	private final IMatchMoveApi matchMoveApi;
	
	private final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	
	public FlightApi(ERCredentialManager eRCredentialManager, ITransactionApi transactionApi,
			FlightDetailsRepository flightDetailsRepository, FlightListRepository flightListRepository,
			TransactionTypeValidation transactionTypeValidation, FlightTicketRepository flightTicketRepository,
			FlightTravellerDetailsRepository flightTravellerDetailsRepository, MTransactionRepository mTransactionRepository,
			IMatchMoveApi matchMoveApi) {
		this.eRCredentialManager = eRCredentialManager;
		this.transactionApi = transactionApi;
		this.flightDetailsRepository = flightDetailsRepository;
		this.flightListRepository = flightListRepository;
		this.transactionTypeValidation = transactionTypeValidation;
		this.flightTicketRepository = flightTicketRepository;
		this.flightTravellerDetailsRepository = flightTravellerDetailsRepository;
		this.mTransactionRepository = mTransactionRepository;
		this.matchMoveApi = matchMoveApi;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public org.codehaus.jettison.json.JSONObject getFlightSerachJson(TravelFlightRequest req) {
		org.codehaus.jettison.json.JSONObject payload = new org.codehaus.jettison.json.JSONObject();
		
		String domain = null;
		String basicAuth=null;
		String key=null;
		String token=null;
		if(MatchMoveUtil.PRODUCTION){
		ERCredentialsManager credentialManager=eRCredentialManager.getCredentials("LIVE");
		if(credentialManager!=null){
			domain=credentialManager.getMdex_base_url();
			basicAuth=credentialManager.getBasic_auth_header();
			key=credentialManager.getMdex_keys();
			token=credentialManager.getMdex_token();
		}
		} else {
			ERCredentialsManager credentialManager=eRCredentialManager.getCredentials("TEST");
			if(credentialManager!=null){
			domain=credentialManager.getMdex_base_url();
			basicAuth=credentialManager.getBasic_auth_header();
			key=credentialManager.getMdex_keys();
			token=credentialManager.getMdex_token();
			}
		}
		try {
			org.json.simple.JSONArray flightSearchDetails = new org.json.simple.JSONArray();
			org.codehaus.jettison.json.JSONObject flightSearchDetailsobj = new org.codehaus.jettison.json.JSONObject();
			org.codehaus.jettison.json.JSONObject roundtripDetailsobj = new org.codehaus.jettison.json.JSONObject();
			payload.put("clientIp", Inet4Address.getLocalHost().getHostAddress());
			payload.put("cllientKey", key);
			payload.put("clientToken", token);
			payload.put("clientApiName", "Flight Search");

			flightSearchDetailsobj.put("origin", req.getOrigin());
			flightSearchDetailsobj.put("destination", req.getDestination());
			flightSearchDetailsobj.put("beginDate",req.getBeginDate());

			flightSearchDetails.add(flightSearchDetailsobj);
			if (req.getTripType().equals("RoundTrip")) {
				roundtripDetailsobj.put("origin", req.getDestination());
				roundtripDetailsobj.put("destination", req.getOrigin());
				roundtripDetailsobj.put("beginDate",req.getEndDate());
				flightSearchDetails.add(roundtripDetailsobj);
			}

			ArrayList<String> objhasmap = new ArrayList<>();
			objhasmap.add("" + EngineID.Indigo);
			objhasmap.add("" + EngineID.AirAsia);
			objhasmap.add("" + EngineID.AirCosta);
			objhasmap.add("" + EngineID.GoAir);
			objhasmap.add("" + EngineID.Spicjet);
			objhasmap.add("" + EngineID.Trujet);
			objhasmap.add("" + EngineID.TravelPort);
			payload.put("engineIDs", objhasmap);
			payload.put("flightSearchDetails", flightSearchDetails);
			payload.put("tripType", req.getTripType());
			payload.put("cabin", req.getCabin());
			payload.put("adults", req.getAdults());
			payload.put("childs", req.getChilds());
			payload.put("infants", req.getInfants());
			payload.put("traceId", "AYTM00011111111110002");
				
		} catch (Exception e) {
			e.printStackTrace();
		}
		return payload;		
	}
	
	@Override
	public FlightPaymentResponse placeOrder(FlightPaymentRequest order, MService service, MUser senderUser) {
		String transactionRefNo = String.valueOf(System.currentTimeMillis());
		String pnr = getUniquePnrNo();
		FlightPaymentResponse response = new FlightPaymentResponse();
		MTransaction valid = initiateTransaction(transactionRefNo, order.getPaymentAmount(), service, senderUser);
		if (valid != null) {
			FlightDetails orders = saveFlightBooking(order, valid, senderUser);
			if (orders != null) {
				response.setTransactionRefNo(transactionRefNo);
				response.setValid(true);
				response.setMessage("Transaction Initiated");

			} else {
				response.setValid(false);
				response.setMessage("Order not saved,Try again later");
			}
		} else {
			response.setValid(false);
			response.setMessage("Transaction  not saved,Try again later");
		}
		return response;
	}
	
	@Override
	public MTransaction initiateTransaction(String transactionRefNo, double amount, MService service,
			MUser senderUser) {
		String description = "Booking of Flight of Rs." + amount;
		return transactionApi.initiateFlightPayment(amount, description, service, transactionRefNo, senderUser,
				ServiceCodeUtil.FLIGHT_BOOKING);
	}
	
	public static String getUniquePnrNo() {
		char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
		Random rnd = new Random();
		StringBuilder sb = new StringBuilder((100000 + rnd.nextInt(900000)) + "-");
		for (int i = 0; i < 5; i++)
			sb.append(chars[rnd.nextInt(chars.length)]);

		return sb.toString();
	}
	
	@Override
	public FlightDetails saveFlightBooking(FlightPaymentRequest order, MTransaction transaction, MUser user) {
		FlightDetails orders = flightDetailsRepository.getByTransaction(transaction);
		if (orders == null) {
			orders = new FlightDetails();
			orders.setFirstName(order.getFirstName());
			orders.setPaymentAmount(order.getPaymentAmount());
			orders.setFlightStatus(Status.Processing);
			orders.setPaymentstatus(Status.Initiated);
			orders.setPaymentmethod(order.getPaymentmethod());
			orders.setTicketDetails(order.getTicketDetails());
			orders.setTransaction(transaction);
			orders.setUser(user);
			flightDetailsRepository.save(orders);
		}
		return orders;
	}
	
	
	//new code
	
	@Override
	public List<FlightAirLineList> getAllAirLineList() {

		List<FlightAirLineList> airLineLists=(List<FlightAirLineList>)flightListRepository.findAll();

		return airLineLists;
	}
	
	@Override
	public boolean comCountry(CompareCountry dto) {
		String orgCountry=flightListRepository.getCountryByCode(dto.getOrg());
		String destCountry=flightListRepository.getCountryByCode(dto.getDest());
		boolean international=false;
		if(orgCountry!=null && destCountry!=null) {
		if (orgCountry.equalsIgnoreCase(destCountry)) {
			international=false;
		}else {
			international=true;
		}
		return international;
	}
		return international;
	}
	
	@Override
	public FlightPaymentResponse flightInit(FlightPayment order, MService service, MUser senderUser) {
		String transactionRefNo = String.valueOf(System.currentTimeMillis());
		FlightPaymentResponse response = new FlightPaymentResponse();
		MTransaction valid = initiateTransactionFlight(transactionRefNo, order.getPaymentAmount(), service, senderUser,
				order.getConvenienceFee(),order.getBaseFare());
		if (valid != null) {			
			FlightTicket orders = saveFlight(order, valid, senderUser,order.getBaseFare());
			if (orders != null) {
				UserKycResponse resp = matchMoveApi.debitFromCardInFlight(senderUser.getUserDetail().getEmail(), senderUser.getUsername(), String.valueOf(valid.getAmount()), transactionRefNo);
				if(resp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
					response.setTransactionRefNo(transactionRefNo);
					response.setValid(true);
					response.setMessage("Transaction Initiated");
				} else {
					response.setTransactionRefNo(transactionRefNo);
					response.setValid(false);
					response.setMessage("Transaction Failed");
				}
			} else {
				response.setValid(false);
				response.setMessage("Order not saved,Try again later");
			}
		} else {
			response.setValid(false);
			response.setMessage("Transaction  not saved,Try again later");
		}
		return response;
	}
	
	@Override
	public MTransaction initiateTransactionFlight(String transactionRefNo, double amount, MService service,
			MUser senderUser,String convenienceFee, String baseFare) {
		String description = "Booking of Flight of Rs." + amount;
		return transactionApi.initiatePaymentFlight(amount, description, service, transactionRefNo, senderUser,
				ServiceCodeUtil.FLIGHT_BOOKING,convenienceFee,baseFare);
	}
	
	@Override
	public FlightTicket saveFlight(FlightPayment order, MTransaction transaction, MUser user,String baseFare) {
		FlightTicket orders = flightTicketRepository.getByTransaction(transaction);
		Date ageOfTraveller=null;
		if (orders == null) {
			double comAmt=Double.parseDouble(baseFare);
			
			MCommission senderCommission = transactionTypeValidation.findCommissionByService(transaction.getService());
			double netCommissionValue = transactionTypeValidation.getCommissionValue(senderCommission, comAmt);
			orders = new FlightTicket();
			orders.setPaymentAmount(order.getPaymentAmount());
			orders.setFlightStatus(Status.Processing);
			orders.setPaymentStatus(Status.Initiated);
			orders.setPaymentMethod(order.getPaymentMethod());
			orders.setTicketDetails(order.getTicketDetails());
			String firstAirlineName="";
			String secondAirlineName="";
			String tripType="";
			try {
				if (order.getTicketDetails()!=null && !order.getTicketDetails().isEmpty() &&  !order.getTicketDetails().equalsIgnoreCase("null")) {
				JSONObject obj = new JSONObject(order.getTicketDetails());
				JSONObject ticketDetails=obj.getJSONObject("Tickets");
				JSONArray oneWay=ticketDetails.getJSONArray("Oneway");
				JSONArray roundWay=ticketDetails.getJSONArray("Roundway");
				if (roundWay.length()==0) {
					tripType="OneWay";
				}
				for (int i = 0; i < oneWay.length(); i++) {
					JSONObject objd=oneWay.getJSONObject(i);
					if (i==0) {
						firstAirlineName=firstAirlineName+objd.get("airlineName");
					}
					else{
						firstAirlineName=firstAirlineName+" | "+objd.get("airlineName");
					}
				}
				for (int i = 0; i < roundWay.length(); i++) {
					tripType="RoundTrip";
					JSONObject objd=roundWay.getJSONObject(i);
					if (i==0) {
						secondAirlineName=secondAirlineName+objd.get("airlineName");
					}
					else{
						secondAirlineName=secondAirlineName+" | "+objd.get("airlineName");
					}
				}
			}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			orders.setTransaction(transaction);
			orders.setMobile(order.getMobile());
			orders.setEmail(order.getEmail());
			orders.setCommissionAmt(netCommissionValue+200);
			orders.setUser(user);
			orders.setFlightNumberOnward(firstAirlineName);
			orders.setFlightNumberReturn(secondAirlineName);
			orders.setTripType(tripType);
			orders.setBaseFare(Double.parseDouble(baseFare));
			flightTicketRepository.save(orders);
			try{
			if (order.getTravellerDetails()!=null) {
				for (int i = 0; i < order.getTravellerDetails().size(); i++) {
					FlightTravellers travellerDetails=new FlightTravellers();
					travellerDetails.setfName(order.getTravellerDetails().get(i).getfName());
					travellerDetails.setlName(order.getTravellerDetails().get(i).getlName());
					if(order.getTravellerDetails().get(i).getAge()!=null && 
							!order.getTravellerDetails().get(i).getAge().isEmpty()){
					ageOfTraveller=format.parse(order.getTravellerDetails().get(i).getAge());
					travellerDetails.setAge(String.valueOf(CommonUtil.calculateAge(ageOfTraveller)));
					}
					travellerDetails.setGender(order.getTravellerDetails().get(i).getGender());
					travellerDetails.setFare(order.getTravellerDetails().get(i).getFare());
					travellerDetails.setType(order.getTravellerDetails().get(i).getTravellerType());
					travellerDetails.setTicketNo(order.getTravellerDetails().get(i).getTicketNo());
					travellerDetails.setFlightTicket(orders);
					flightTravellerDetailsRepository.save(travellerDetails);
				}
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		return orders;
	}
	
	@Override
	public FlightPaymentResponse flightPayment(FlightPayment dto, MUser user) throws Exception {

		FlightPaymentResponse response = new FlightPaymentResponse();
		if (dto.isSuccess()) {
			MTransaction senderTransaction = transactionApi.successFlightPayment(dto.getTxnRefno());
			if (senderTransaction != null) {
				if (senderTransaction.getStatus().equals(Status.Success)) {
					FlightTicket orders = flightTicketRepository.getByTransaction(senderTransaction);
					if (orders != null) {
						orders.setPaymentAmount(dto.getPaymentAmount());
						if (dto.getFlightStatus()!=null) {
							orders.setFlightStatus(dto.getFlightStatus());
						}
						orders.setPaymentStatus(Status.Success);
						orders.setPaymentMethod(dto.getPaymentMethod());
						orders.setTicketDetails(dto.getTicketDetails());
						orders.setBookingRefId(dto.getBookingRefId());
						orders.setMdexTxnRefNo(dto.getMdexTxnRefNo());
						orders.setMobile(dto.getMobile());
						orders.setEmail(dto.getEmail());
						orders.setUser(user);
						orders.setTransaction(senderTransaction);

						flightTicketRepository.save(orders);

						List<FlightTravellers> travellerDetails=flightTravellerDetailsRepository.getTravellersByTicket(orders);
						for (int i = 0; i < dto.getTravellerDetails().size(); i++) {
							travellerDetails.get(i).setTicketNo(dto.getTravellerDetails().get(i).getTicketNo());
							flightTravellerDetailsRepository.save(travellerDetails.get(i));
						}
						response.setValid(true);
						response.setMessage("Transaction Successful");
						JSONObject obj = new JSONObject(dto.getTicketDetails());
						String	ticketobj = null;
						if(obj!=null){
							ticketobj = JSONParserUtil.getStringOrg(obj, "Tickets");

						}
						String unescape = StringEscapeUtils.unescapeJava(ticketobj);
						System.out.println(unescape);
						JSONObject obj1 = new JSONObject(unescape);
						JSONArray legs = JSONParserUtil.getArrayOrg(obj1, "Oneway");
						JSONArray legs1 =  JSONParserUtil.getArrayOrg(obj1, "Roundway");

						List<FlightResponseEmail> flightresponse = new ArrayList<FlightResponseEmail>();
						List<FlightResponseEmail> flightresponsearrreturn = new ArrayList<FlightResponseEmail>();
						List<FlightTicketdtoForname> nameandticket = new ArrayList<FlightTicketdtoForname>();

						flightresponsearrreturn.clear();
						flightresponse.clear();
						for (int k = 0; k < legs.length(); k++) {
							FlightResponseEmail flightresponseobj = new FlightResponseEmail();
							//							flightresponseobj.setJourneyTime("" + legs.getJSONObject(k).getString("journeyTime"));
							flightresponseobj.setDepartureDate("" + legs.getJSONObject(k).getString("departureDate"));
							flightresponseobj.setFlightNumber("" + legs.getJSONObject(k).getString("flightNumber"));
							flightresponseobj.setArrivalDate("" + legs.getJSONObject(k).getString("arrivalDate"));
							flightresponseobj.setAirlineName("" + legs.getJSONObject(k).getString("airlineName"));
							flightresponseobj.setArrivalTime("" + legs.getJSONObject(k).getString("arrivalTime"));
							flightresponseobj.setCabin("" + legs.getJSONObject(k).getString("cabin"));
							flightresponseobj.setDepartureTime("" + legs.getJSONObject(k).getString("departureTime"));
							flightresponseobj.setDestination("" + legs.getJSONObject(k).getString("destination"));
							flightresponseobj.setDuration("" + legs.getJSONObject(k).getString("duration"));
							flightresponseobj.setFlightNumber("" + legs.getJSONObject(k).getString("flightNumber"));
							flightresponseobj.setOrigin("" + legs.getJSONObject(k).getString("origin"));
							flightresponseobj.setBaggageUnit("" + legs.getJSONObject(k).getString("baggageUnit"));
							flightresponseobj.setBaggageWeight("" + legs.getJSONObject(k).getString("baggageWeight"));
							flightresponse.add(flightresponseobj);
						}
						for (int k = 0; k < legs1.length(); k++) {
							System.err.println(k);
							FlightResponseEmail flightresponseobj = new FlightResponseEmail();
							//flightresponseobj
							//.setJourneyTimereturn("" + legs1.getJSONObject(k).getString("journeyTime"));
							flightresponseobj
							.setDepartureDatereturn("" + legs1.getJSONObject(k).getString("departureDate"));
							flightresponseobj
							.setFlightNumberreturn("" + legs1.getJSONObject(k).getString("flightNumber"));
							flightresponseobj
							.setArrivalDatereturn("" + legs1.getJSONObject(k).getString("arrivalDate"));
							flightresponseobj
							.setAirlineNamereturn("" + legs1.getJSONObject(k).getString("airlineName"));
							flightresponseobj
							.setArrivalTimereturn("" + legs1.getJSONObject(k).getString("arrivalTime"));
							flightresponseobj.setCabinreturn("" + legs1.getJSONObject(k).getString("cabin"));
							flightresponseobj
							.setDepartureTimereturn("" + legs1.getJSONObject(k).getString("departureTime"));
							flightresponseobj
							.setDestinationreturn("" + legs1.getJSONObject(k).getString("destination"));
							flightresponseobj.setDurationreturn("" + legs1.getJSONObject(k).getString("duration"));
							flightresponseobj
							.setFlightNumberreturn("" + legs1.getJSONObject(k).getString("flightNumber"));
							flightresponseobj.setOriginreturn("" + legs1.getJSONObject(k).getString("origin"));
							flightresponseobj
							.setBaggageUnitreturn("" + legs1.getJSONObject(k).getString("baggageUnit"));
							flightresponseobj
							.setBaggageWeightreturn("" + legs1.getJSONObject(k).getString("baggageWeight"));
							flightresponsearrreturn.add(flightresponseobj);
						}

						TicketDetailsDTOFlight additionalInfo = new TicketDetailsDTOFlight();
						additionalInfo.setBookingRefId(dto.getBookingRefId());
						additionalInfo.setFlightresponse(flightresponse);
						additionalInfo.setFlightresponsearrreturn(flightresponsearrreturn);
						additionalInfo.setNameandticket(nameandticket);
						additionalInfo.setPaymentAmount(dto.getPaymentAmount());
						additionalInfo.setTravellerDetails(travellerDetails);
						additionalInfo.setPnrNo(dto.getPnrNo());
						
						user.getUserDetail().setEmail(dto.getEmail());
						user.setUsername(dto.getMobile());

						System.err.println("Ticket Email:: "+user.getUserDetail().getEmail());
						System.err.println("Ticket Mobile:: "+user.getUsername());

					/*	smsSenderApi.sendFlightTicketSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
								SMSTemplete.FLIGHTTICKET_SUCCESS, user, senderTransaction, additionalInfo);
						
						mailSenderApi.sendFlightTicketMail("Ewire Flight Ticket Booking",
								MailTemplate.FLIGHTBOOKING_SUCCESS, user, senderTransaction, additionalInfo);*/
						
						List<FlightTravellers> list=flightTravellerDetailsRepository.getTravellersByTicket(orders);
						test(list);

					} else {
						response.setValid(false);
						response.setMessage("Order Details not found");
					}
				} else {
					response.setValid(false);
					response.setMessage("Transaction is " + senderTransaction.getStatus());
				}
			} else {
				response.setValid(false);
				response.setMessage("Transaction not found with this reference no");
			}
		} else {

			try{
				MTransaction senderTransaction=transactionApi.getTransactionByRefNo(dto.getTxnRefno()+"D");
				transactionApi.failedFlightPayment(dto.getTxnRefno());
				FlightDetails orders = flightDetailsRepository.getByTransaction(senderTransaction);
				if (orders != null) {
					orders.setPaymentAmount(dto.getPaymentAmount());
					orders.setFlightStatus(dto.getFlightStatus());
					orders.setPaymentstatus(dto.getPaymentStatus());
					orders.setPaymentmethod(dto.getPaymentMethod());
					flightDetailsRepository.save(orders);
				}
			}
			catch(Exception e){
				e.printStackTrace();
				System.err.println("Unable to reversed amount");
			}
			response.setValid(false);
			response.setMessage("Transaction Failed");
		}
		return response;
	}
	
	public void test(List<FlightTravellers> list)
	{

		Font blueFont = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.NORMAL, new CMYKColor(255, 0, 0, 0));
		Font redFont = FontFactory.getFont(FontFactory.COURIER, 12, Font.BOLD, new CMYKColor(0, 255, 0, 0));
		Font yellowFont = FontFactory.getFont(FontFactory.COURIER, 14, Font.BOLD, new CMYKColor(0, 0, 255, 0));
		Document document = new Document();

		try
		{
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfUrlLive));
			document.open();

			String img="https://rupay.liveewire.com/resources/admin/assets/images/logo.png";
			Image image2 = Image.getInstance(new URL(img));
			//image2.setAbsolutePosition(1f, 1f);
			image2.setAlignment(Image.RIGHT);
			image2.scaleAbsolute(50, 50);

			FlightTicket flightTicket= list.get(0).getFlightTicket();


			String ticktStr=flightTicket.getTicketDetails();

			JSONObject ticket=new JSONObject(ticktStr);
			ticket=ticket.getJSONObject("Tickets");
			JSONArray oneway=ticket.getJSONArray("Oneway");
			JSONArray roundway=ticket.getJSONArray("Roundway");
			String org=oneway.getJSONObject(0).getString("origin");
			String dest=oneway.getJSONObject(oneway.length()-1).getString("destination");

			FlightAirLineList origin=flightListRepository.getCityByCode(org);
			FlightAirLineList destination=flightListRepository.getCityByCode(dest);
			String bk=list.get(0).getFlightTicket().getBookingRefId();
			String bkd=list.get(0).getFlightTicket().getCreated().toString();
			String arr[]=bkd.split(" ");
			bkd=arr[0];
			PdfPTable table = new PdfPTable(2);

			table.setWidthPercentage(100);
			table.addCell(getCell("E-TICKET", PdfPCell.ALIGN_LEFT));
			table.addCell(image2);
			document.add(table);

			PdfPTable table2 = new PdfPTable(2);
			table2.setWidthPercentage(100);
			table2.addCell(getCell("Booking Ref Id: "+bk, PdfPCell.ALIGN_LEFT));
			table2.addCell(getCell("Customer Care: +918025011300", PdfPCell.ALIGN_LEFT));
			document.add(table2);

			PdfPTable table3= new PdfPTable(1);
			table3.setWidthPercentage(100);
			table3.addCell(getCell("Booked on: "+bkd, PdfPCell.ALIGN_LEFT));
			document.add(table3);

			document.add(new Paragraph("----------------------------------------------------------------------------------------------------------------------------------"));
			document.add(new Paragraph("Flight Details"));
			document.add(new Paragraph(origin.getCityName() +" to "+destination.getCityName()));

			PdfPTable table4= new PdfPTable(1);
			table4.addCell(getCell("Journey Date: "+oneway.getJSONObject(0).getString("departureDate"), PdfPCell.ALIGN_RIGHT));
			document.add(table4);
			document.add(new Paragraph(""));
			document.add( Chunk.NEWLINE );

			PdfPTable table5= new PdfPTable(2);


			for (int i = 0; i < oneway.length(); i++) {
				table5.addCell(getCell(oneway.getJSONObject(i).getString("origin"), PdfPCell.ALIGN_LEFT));
				table5.addCell(getCell(oneway.getJSONObject(i).getString("destination"),PdfPCell.ALIGN_RIGHT));
				table5.addCell(getCell(oneway.getJSONObject(i).getString("departureDate"), PdfPCell.ALIGN_LEFT));
				table5.addCell(getCell(oneway.getJSONObject(i).getString("departureDate"),PdfPCell.ALIGN_RIGHT));
				table5.addCell(getCell(oneway.getJSONObject(i).getString("departureTime"), PdfPCell.ALIGN_LEFT));
				table5.addCell(getCell(oneway.getJSONObject(i).getString("arrivalTime"), PdfPCell.ALIGN_RIGHT));
				table5.addCell(getCell(" ", PdfPCell.ALIGN_CENTER));
				table5.addCell(getCell(" ", PdfPCell.ALIGN_CENTER));
			}

			document.add(table5);
			document.add(Chunk.NEWLINE);
			if (roundway.length()>0) {
				document.add(new Paragraph("Return"));
				document.add(Chunk.NEWLINE);
			}

			PdfPTable table0= new PdfPTable(2);

			for (int i = 0; i < roundway.length(); i++) {
				table0.addCell(getCell(roundway.getJSONObject(i).getString("origin"), PdfPCell.ALIGN_LEFT));
				table0.addCell(getCell(roundway.getJSONObject(i).getString("destination"),PdfPCell.ALIGN_RIGHT));
				table0.addCell(getCell(roundway.getJSONObject(i).getString("departureDate"), PdfPCell.ALIGN_LEFT));
				table0.addCell(getCell(roundway.getJSONObject(i).getString("departureDate"),PdfPCell.ALIGN_RIGHT));
				table0.addCell(getCell(roundway.getJSONObject(i).getString("departureTime"), PdfPCell.ALIGN_RIGHT));
				table0.addCell(getCell(roundway.getJSONObject(i).getString("arrivalTime"), PdfPCell.ALIGN_LEFT));
				table0.addCell(getCell(" ", PdfPCell.ALIGN_CENTER));
				table0.addCell(getCell(" ", PdfPCell.ALIGN_CENTER));
			}

			document.add(table0);
			document.add(Chunk.NEWLINE);

			document.add(new Paragraph("Traveller Details: "));

			document.add(Chunk.NEWLINE);

			PdfPTable table6= new PdfPTable(6);

			table6.addCell("SL No");
			table6.addCell("First Name");
			table6.addCell("Last Name");
			table6.addCell("Gender");
			table6.addCell("Ticket No");
			table6.addCell("Passenger Type");

			for (int i = 0; i < list.size(); i++) {

				table6.addCell(i+1+"");
				table6.addCell(list.get(i).getfName());
				table6.addCell(list.get(i).getlName());
				table6.addCell(list.get(i).getGender());
				table6.addCell(list.get(i).getTicketNo());
				table6.addCell(list.get(i).getType());

			}
			document.add(table6);
			document.close();
			writer.close();

		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public static PdfPCell getCell(String text, int alignment) {
		PdfPCell cell = new PdfPCell(new Phrase(text));
		cell.setPadding(0);
		cell.setHorizontalAlignment(alignment);
		cell.setBorder(PdfPCell.NO_BORDER);
		return cell;
	}
	
	@Override
	public FlightPaymentResponse paymentGateWaySuccess(FlightPayment dto, MUser user) throws JSONException {

		FlightPaymentResponse response = new FlightPaymentResponse();


		MTransaction senderTransaction = mTransactionRepository.findByTransactionRefNo("FL" + dto.getMerchantRefNo() + "D");
		if (senderTransaction != null) {

			if (senderTransaction.getStatus().equals(Status.Booked)) {
				FlightTicket orders = flightTicketRepository.getByTransaction(senderTransaction);
				if (orders != null) {
					orders.setPaymentAmount(dto.getPaymentAmount());
					if (dto.getFlightStatus()!=null) {
						orders.setFlightStatus(dto.getFlightStatus());
					}
					orders.setPaymentStatus(Status.Success);
					orders.setPaymentMethod(dto.getPaymentMethod());
					orders.setTicketDetails(dto.getTicketDetails());
					orders.setBookingRefId(dto.getBookingRefId());
					orders.setMdexTxnRefNo(dto.getMdexTxnRefNo());
					orders.setMobile(dto.getMobile());
					orders.setEmail(dto.getEmail());
					
					JSONObject obj = new JSONObject(dto.getTicketDetails());
					String	ticketobj = null;
					if(obj!=null){
						ticketobj = JSONParserUtil.getStringOrg(obj, "Tickets");

					}
					String unescape = StringEscapeUtils.unescapeJava(ticketobj);
					System.out.println(unescape);
					JSONObject obj1 = new JSONObject(unescape);
					JSONArray oneway = JSONParserUtil.getArrayOrg(obj1, "Oneway");
					JSONArray roundway =  JSONParserUtil.getArrayOrg(obj1, "Roundway");
					
					String firstName="";
					String secoundName="";
					String tripType="";
					try {
						
					
						if (roundway.length()==0) {
							tripType="OneWay";
						}
						
						for (int i = 0; i < oneway.length(); i++) {
							JSONObject objd=oneway.getJSONObject(i);
							if (i==0) {
								firstName=firstName+objd.get("airlineName");
							}
							else{
								firstName=firstName+" | "+objd.get("airlineName");
							}
						}
						for (int i = 0; i < roundway.length(); i++) {
							tripType="RoundTrip";
							JSONObject objd=roundway.getJSONObject(i);
							if (i==0) {
								secoundName=secoundName+objd.get("airlineName");
							}
							else{
								secoundName=secoundName+" | "+objd.get("airlineName");
							}
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
					
					orders.setFlightNumberOnward(firstName);
					orders.setFlightNumberReturn(secoundName);
					orders.setTripType(tripType);
					orders.setUser(user);
					orders.setTransaction(senderTransaction);

					flightTicketRepository.save(orders);

					List<FlightTravellers> travellerDetailsArr=new ArrayList<>();
					List<FlightTravellers> travellerDetail=flightTravellerDetailsRepository.getTravellersByTicket(orders);
					if (!travellerDetail.isEmpty()) {

						for (int i = 0; i < dto.getTravellerDetails().size(); i++) {

							travellerDetail.get(i).setTicketNo(dto.getTravellerDetails().get(i).getTicketNo());
							travellerDetailsArr.add(travellerDetail.get(i));
							flightTravellerDetailsRepository.save(travellerDetail.get(i));
						}
					}
					else{
						if (dto.getTravellerDetails()!=null) {
							for (int i = 0; i < dto.getTravellerDetails().size(); i++) {
								FlightTravellers travellerDetails=new FlightTravellers();
								travellerDetails.setfName(dto.getTravellerDetails().get(i).getfName());
								travellerDetails.setlName(dto.getTravellerDetails().get(i).getlName());
								travellerDetails.setAge(dto.getTravellerDetails().get(i).getAge());
								travellerDetails.setGender(dto.getTravellerDetails().get(i).getGender());
								travellerDetails.setFare(dto.getTravellerDetails().get(i).getFare());
								travellerDetails.setType(dto.getTravellerDetails().get(i).getTravellerType());
								travellerDetails.setTicketNo(dto.getTravellerDetails().get(i).getTicketNo());
								travellerDetails.setFlightTicket(orders);
								travellerDetailsArr.add(travellerDetails);
								flightTravellerDetailsRepository.save(travellerDetails);
							}
						}
					}
					response.setValid(true);
					response.setMessage("Transaction Successful");

					

					List<FlightResponseEmail> flightresponse = new ArrayList<FlightResponseEmail>();
					List<FlightResponseEmail> flightresponsearrreturn = new ArrayList<FlightResponseEmail>();
					List<FlightTicketdtoForname> nameandticket = new ArrayList<FlightTicketdtoForname>();

					flightresponsearrreturn.clear();
					flightresponse.clear();
					for (int k = 0; k < oneway.length(); k++) {
						FlightResponseEmail flightresponseobj = new FlightResponseEmail();
						//flightresponseobj.setJourneyTime("" + legs.getJSONObject(k).getString("journeyTime"));
						flightresponseobj.setDepartureDate("" + oneway.getJSONObject(k).getString("departureDate"));
						flightresponseobj.setFlightNumber("" + oneway.getJSONObject(k).getString("flightNumber"));
						flightresponseobj.setArrivalDate("" + oneway.getJSONObject(k).getString("arrivalDate"));
						flightresponseobj.setAirlineName("" + oneway.getJSONObject(k).getString("airlineName"));
						flightresponseobj.setArrivalTime("" + oneway.getJSONObject(k).getString("arrivalTime"));
						flightresponseobj.setCabin("" + oneway.getJSONObject(k).getString("cabin"));
						flightresponseobj.setDepartureTime("" + oneway.getJSONObject(k).getString("departureTime"));
						flightresponseobj.setDestination("" + oneway.getJSONObject(k).getString("destination"));
						flightresponseobj.setDuration("" + oneway.getJSONObject(k).getString("duration"));
						flightresponseobj.setFlightNumber("" + oneway.getJSONObject(k).getString("flightNumber"));
						flightresponseobj.setOrigin("" + oneway.getJSONObject(k).getString("origin"));
						flightresponseobj.setBaggageUnit("" + oneway.getJSONObject(k).getString("baggageUnit"));
						flightresponseobj.setBaggageWeight("" + oneway.getJSONObject(k).getString("baggageWeight"));
						flightresponse.add(flightresponseobj);
					}
					for (int k = 0; k < roundway.length(); k++) {
						System.err.println(k);
						FlightResponseEmail flightresponseobj = new FlightResponseEmail();
						//flightresponseobj.setJourneyTimereturn("" + legs1.getJSONObject(k).getString("journeyTime"));
						flightresponseobj
						.setDepartureDatereturn("" + roundway.getJSONObject(k).getString("departureDate"));
						flightresponseobj
						.setFlightNumberreturn("" + roundway.getJSONObject(k).getString("flightNumber"));
						flightresponseobj
						.setArrivalDatereturn("" + roundway.getJSONObject(k).getString("arrivalDate"));
						flightresponseobj
						.setAirlineNamereturn("" + roundway.getJSONObject(k).getString("airlineName"));
						flightresponseobj
						.setArrivalTimereturn("" + roundway.getJSONObject(k).getString("arrivalTime"));
						flightresponseobj.setCabinreturn("" + roundway.getJSONObject(k).getString("cabin"));
						flightresponseobj
						.setDepartureTimereturn("" + roundway.getJSONObject(k).getString("departureTime"));
						flightresponseobj
						.setDestinationreturn("" + roundway.getJSONObject(k).getString("destination"));
						flightresponseobj.setDurationreturn("" + roundway.getJSONObject(k).getString("duration"));
						flightresponseobj
						.setFlightNumberreturn("" + roundway.getJSONObject(k).getString("flightNumber"));
						flightresponseobj.setOriginreturn("" + roundway.getJSONObject(k).getString("origin"));
						flightresponseobj
						.setBaggageUnitreturn("" + roundway.getJSONObject(k).getString("baggageUnit"));
						flightresponseobj
						.setBaggageWeightreturn("" + roundway.getJSONObject(k).getString("baggageWeight"));
						flightresponsearrreturn.add(flightresponseobj);
					}

					TicketDetailsDTOFlight additionalInfo = new TicketDetailsDTOFlight();
					additionalInfo.setBookingRefId(dto.getBookingRefId());
					additionalInfo.setFlightresponse(flightresponse);
					additionalInfo.setFlightresponsearrreturn(flightresponsearrreturn);
					additionalInfo.setNameandticket(nameandticket);
					additionalInfo.setPaymentAmount(dto.getPaymentAmount());
					additionalInfo.setTravellerDetails(travellerDetailsArr);
					additionalInfo.setPnrNo(dto.getPnrNo());
					
					user.getUserDetail().setEmail(dto.getEmail());
					user.setUsername(dto.getMobile());

					System.err.println("Ticket Email:: "+user.getUserDetail().getEmail());
					System.err.println("Ticket Mobile:: "+user.getUsername());

					List<FlightTravellers> list=flightTravellerDetailsRepository.getTravellersByTicket(orders);
					test(list);

					/*mailSenderApi.sendFlightTicketMail("VPayQwik Flight Ticket Booking",
							MailTemplate.FLIGHTBOOKING_SUCCESS, user, senderTransaction, additionalInfo);

					smsSenderApi.sendFlightTicketSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
							SMSTemplate.FLIGHTTICKET_SUCCESS, user, senderTransaction, additionalInfo);*/

				} else {
					response.setValid(false);
					response.setMessage("Order Details not found");
				}
			} else {
				response.setValid(false);
				response.setMessage("Transaction is " + senderTransaction.getStatus());
			}
		}
		return response;
	}
	
	@Override
	public List<FlghtTicketResp> getAllTickets(MUser user) {

		List<FlghtTicketResp> list=new ArrayList<>();
		
		MPQAccountDetails accountDetail=user.getAccountDetail();
		List<FlightTicket> flightTickets=flightTicketRepository.getByStatusAndUser(Status.Initiated,user);
		for (int i = 0; i < flightTickets.size(); i++) {
			FlghtTicketResp resp=new FlghtTicketResp();
			FlightTicket flightTicketDB=flightTickets.get(i);
			List<FlightTravellers> travellerDetails2=flightTravellerDetailsRepository.getTravellersByTicket(flightTicketDB);
			resp.setFlightTicket(flightTicketDB);
			resp.setTravellerDetails(travellerDetails2);
			list.add(resp);
		}
		return list;
	}

	@Override
	public List<FlightTicket> getFlightDetailForAdmin() {
		List<FlightTicket> flightTickets=(List<FlightTicket>)flightTicketRepository.getAllTicket();
		return flightTickets;
	}
	
	@Override
	public List<FlightTicket> getFlightDetailForAdminDate(PagingDTO dto) {
		List<FlightTicket> flightTickets=(List<FlightTicket>)flightTicketRepository.getAllTicketByDate(dto.getFromDate(), dto.getToDate());
		return flightTickets;
	}
	
	@Override
	public List<FlightTravellers> getFlightTravellersForAdmin(long flightTicketId) {

		FlightTicket flightTicket=flightTicketRepository.findOne(flightTicketId);
		List<FlightTravellers> list=flightTravellerDetailsRepository.getTravellersByTicket(flightTicket);
		test(list);		
		return list;
	}
	
	@Override
	public TicketsResp getFlightTravellersNameForAdmin(long flightTicketId) {
		FlightTicket flightTicket=flightTicketRepository.findOne(flightTicketId);
		List<FlightTravellers> list=flightTravellerDetailsRepository.getTravellersByTicket(flightTicket);
		TicketsResp	 ticketDeatilsDTO= new TicketsResp(); 
		FlightTicket tickets=list.get(0).getFlightTicket();
		String ticketsStr = tickets.getTicketDetails();
		
		if (!ticketsStr.equalsIgnoreCase("null")) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				ticketDeatilsDTO = mapper.readValue(ticketsStr, TicketsResp.class);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			logger.info("ticketDeatilsDTO:: "+ticketDeatilsDTO);
			for(int j = 0; j < ticketDeatilsDTO.getTickets().getOneway().size(); j++) {
				String origin = ticketDeatilsDTO.getTickets().getOneway().get(j).getOrigin();
				String desti = ticketDeatilsDTO.getTickets().getOneway().get(j).getDestination();
				FlightAirLineList originName=flightTicketRepository.getCityByCode(origin);
				FlightAirLineList destNmae =flightTicketRepository.getCityByCode(desti);
				ticketDeatilsDTO.getTickets().getOneway().get(j).setDestinationName(destNmae.getCityName());
				ticketDeatilsDTO.getTickets().getOneway().get(j).setSourceName(originName.getCityName());
			}
		}
		return ticketDeatilsDTO;
	}
}
