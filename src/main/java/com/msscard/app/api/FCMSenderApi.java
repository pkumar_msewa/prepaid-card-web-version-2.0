package com.msscard.app.api;

import com.msscard.entity.FCMDetails;
import com.msscard.model.FCMResponseDTO;

public interface FCMSenderApi {


	FCMResponseDTO sendNotification(FCMDetails dto);
}
