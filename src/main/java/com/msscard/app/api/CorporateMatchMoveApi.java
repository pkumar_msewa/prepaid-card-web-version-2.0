package com.msscard.app.api;

import com.msscard.app.model.request.MatchMoveCreateCardRequest;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.request.UserKycRequest;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.entity.MMCards;
import com.msscard.entity.MUser;

public interface CorporateMatchMoveApi {

	ResponseDTO createUserOnMM(MUser request);

	WalletResponse createWallet(MatchMoveCreateCardRequest request);

	WalletResponse assignVirtualCard(MUser mUser);

	WalletResponse inquireCard(MatchMoveCreateCardRequest request);

	ResponseDTO fetchMMUser(MatchMoveCreateCardRequest request);

	WalletResponse fetchWalletMM(MatchMoveCreateCardRequest request);

	ResponseDTO deActivateCards(MatchMoveCreateCardRequest request);

	ResponseDTO reActivateCard(MatchMoveCreateCardRequest request);

	WalletResponse assignPhysicalCard(MUser mUser, String proxy_number);

	ResponseDTO activationPhysicalCard(String activationCode, MUser user);

	WalletResponse transferFundsToMMCard(MUser mUser, String amount, String cardId);

	UserKycResponse confirmKyc(UserKycRequest request);

	UserKycResponse setImagesForKyc(UserKycRequest request);

	UserKycResponse setIdDetails(UserKycRequest request);

	UserKycResponse kycUserMM(UserKycRequest request);

	double getBalance(MUser request);

	UserKycResponse tempConfirmKyc(String email, String mobile);

	UserKycResponse tempSetImagesForKyc(String email, String mobile);

	UserKycResponse tempSetIdDetails(String email, String mobile);

	UserKycResponse tempKycUserMM(String email, String mobile);

	UserKycResponse debitFromMMWalletToCard(String email, String mobile, String cardId, String amount);

	UserKycResponse tempKycStatusApproval(String email, String mobile);

	UserKycResponse getTransactions(MUser userRequest);

	UserKycResponse debitFromCard(String email, String mobile, String cardId, String amount);

	MMCards findCardByCardId(String cardId);

	UserKycResponse resetPins(MUser userRequest);

	MMCards findCardByUserAndStatus(MUser user);

	boolean checkCreditPrefundBalance(String amount);

	UserKycResponse edittempSetIdDetails(String email, String mobile);

	UserKycResponse getUsers(String email, String mobile);


	WalletResponse assignPhysicalCardFromAdmin(MUser user1, String proxyNumber);

	UserKycResponse getConsumers();


	WalletResponse getFundTransferDetails(MUser user, String type);

	WalletResponse getFundTransferDetails(String id);

	UserKycResponse getTransactions(MUser userRequest, String page);

	WalletResponse cancelFundTransfer(MUser recipient, String amount, String id);

	ResponseDTO activateMMPhysicalCard(String cardId, String activationCode);

	WalletResponse assignPhysicalCardCorp(MUser mUser, String proxy_number);

}
