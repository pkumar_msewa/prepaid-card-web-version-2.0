package com.msscard.app.api;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.msscard.entity.MUser;
import com.msscard.entity.SessionLog;

public interface ISessionLogApi {

	void logUserLoggedIn(long userId, String sessionId, String remoteAddress, String machineId, String simId);

	void logUserLoggedOut(long userId, String sessionId);

	long getTotalOnlineUsers();

	List<SessionLog> getUserHistory(long userId);

	void endUserSession(long userId);

	String getUserAccountActivity();

	String getUserAccountActivity(MUser u);
	
	boolean checkRequest(String sessionId, HttpServletRequest request);
}
