package com.msscard.app.api;

import java.util.Date;

import com.msscard.app.model.request.MatchMoveCreateCardRequest;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.request.UserKycRequest;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.entity.MKycDetail;
import com.msscard.entity.MMCards;
import com.msscard.entity.MService;
import com.msscard.entity.MUser;
import com.msscard.entity.MatchMoveWallet;

public interface IMatchMoveApi {

	ResponseDTO createUserOnMM(MUser request);

	WalletResponse createWallet(MatchMoveCreateCardRequest request);

	WalletResponse assignVirtualCard(MUser mUser);

	WalletResponse inquireCard(MatchMoveCreateCardRequest request);

	ResponseDTO fetchMMUser(MatchMoveCreateCardRequest request);

	WalletResponse fetchWalletMM(MatchMoveCreateCardRequest request);

	ResponseDTO deActivateCards(MatchMoveCreateCardRequest request);

	ResponseDTO reActivateCard(MatchMoveCreateCardRequest request);

	WalletResponse assignPhysicalCard(MUser mUser, String proxy_number);

	ResponseDTO activationPhysicalCard(String activationCode, MUser user);

	WalletResponse transferFundsToMMCard(MUser mUser, String amount, String cardId);

	WalletResponse confirmFundsToMMWallet(MUser mUser, String amount, String authRetrievalNo);

	WalletResponse initiateLoadFundsToMMWallet(MUser mUser, String amount);

	UserKycResponse confirmKyc(UserKycRequest request);

	UserKycResponse setImagesForKyc(UserKycRequest request);

	UserKycResponse setIdDetails(UserKycRequest request);

	UserKycResponse kycUserMM(UserKycRequest request);

	double getBalance(MUser request);

	UserKycResponse tempConfirmKyc(String email, String mobile);

	UserKycResponse tempSetImagesForKyc(String email, String mobile);

	UserKycResponse tempSetIdDetails(String email, String mobile);

	UserKycResponse tempKycUserMM(String email, String mobile);

	UserKycResponse debitFromMMWalletToCard(String email, String mobile, String cardId, String amount);

	UserKycResponse tempKycStatusApproval(String email, String mobile);

	UserKycResponse getTransactions(MUser userRequest);

	UserKycResponse debitFromCard(String email, String mobile, String cardId, String amount);

	MMCards findCardByCardId(String cardId);

	UserKycResponse resetPins(MUser userRequest);

	MMCards findCardByUserAndStatus(MUser user);

	boolean checkCreditPrefundBalance(String amount);

	UserKycResponse edittempSetIdDetails(String email, String mobile);

	UserKycResponse getUsers(String email, String mobile);


	WalletResponse assignPhysicalCardFromAdmin(MUser user1, String proxyNumber);

	UserKycResponse getConsumers();


	WalletResponse walletToWalletTransfer(String amount, MUser user, String cardId, MUser recipient);

	WalletResponse acknowledgeFundTransfer(String amount,String transactionId, String recipient);

	WalletResponse transferFundFromCardToWallet(String amount, MUser user, String cardId, String recipient);

	WalletResponse getFundTransferDetails(MUser user, String type);

	WalletResponse getFundTransferDetails(String id);

	UserKycResponse getTransactions(MUser userRequest, String page);

	WalletResponse cancelFundTransfer(MUser recipient, String amount, String id);

	ResponseDTO activateMMPhysicalCard(String cardId, String activationCode);
	
	WalletResponse assignPhysicalCardFromAgent(MUser mUser, String proxy_number);

	double myBalance(MUser userMob);

	ResponseDTO getBalanceExp(MUser request);

	UserKycResponse deductFromWalletToConsumer(String userName,String amount);

	UserKycResponse deductFromCardConfirmation(String id);

	WalletResponse getFundTransfer(MUser recipient);

	UserKycResponse getTransactionsCustom(MUser userRequest, String page);

	UserKycResponse getTransactionsCustomAdmin(MUser userRequest);

	UserKycResponse resetPinsCorp(MUser userRequest);

	UserKycResponse updateEmail(MUser userRequest, String email);

	UserKycResponse checkKYCStatus(MUser userRequest);

	UserKycResponse kycUserMMCorporate(MKycDetail request);

	UserKycResponse setIdDetailsCorporate(MKycDetail request);

	UserKycResponse setImagesForKyc2(MKycDetail request);

	UserKycResponse setImagesForKyc1(MKycDetail request);

	UserKycResponse tempConfirmKycCorporate(MKycDetail detail);

	UserKycResponse setPIN(MUser detail, String pin);

	UserKycResponse setPINConfirm(MUser detail, String pin);

	WalletResponse fetchWalletMMOnLogin(MatchMoveCreateCardRequest request, MUser u);

	Double getBalanceApp(MUser request);

	WalletResponse transferFundFromCardToWalletDonation(String amount, MUser user, String cardId, String recipient);

	WalletResponse fetchWalletMMOld(MatchMoveCreateCardRequest request);

	UserKycResponse debitFromCardInHouse(String email, String mobile, String amount,String transactionRefNo);

	Double getBalanceCus(MUser request);

	ResponseDTO reActivateCardCorp(MatchMoveCreateCardRequest request);

	UserKycResponse debitFromCardCorp(String email, String mobile, String cardId, String amount);

	WalletResponse transferFundsToMMCardTemp(MUser mUser, String amount, String cardId);

	UserKycResponse getUsersOath(String email, String mobile);

	UserKycResponse debitFromCardFingoole(String email, String mobile, String cardId, String amount);

	Double getBalanceAppWallet(MUser request);

	UserKycResponse debitFromWalletToPool(MatchMoveWallet wallet, String amount);

	WalletResponse initiateLoadFundsToMMWalletUpi(MUser mUser, String amount, String transactionRefNo, String upiid);

	UserKycResponse checkStatusFromTransaction(String id);

	UserKycResponse setPINConfirmReset(MUser detail, String pin, Date dob);

	WalletResponse moveToCardFromWallet(MUser mUser, String amount, String cardId);

	WalletResponse initiateLoadFundsToMMWalletCorp(MUser mUser, String amount);

	WalletResponse refundToWalletFromCustomAdmin(MUser mUser, String amount, String serviceCode,
			String trxIdOfOriginalTrx, String ipAdrr);

	WalletResponse initiateLoadFundsToMMWalletPG(MUser mUser, String amount, String transactionRefNo, String upiid);

	WalletResponse reversalToWalletForRechargeFailure(MUser mUser, String amount, String serviceCode,
			String trxIdOfOriginalTrx, String desc);

	WalletResponse assignPhysicalCardFromGroup(String mobile, String proxy_number);

	UserKycResponse debitFromCardInBus(String email, String mobile, String amount, String transactionRefNo);

	UserKycResponse debitFromCardInFlight(String email, String mobile, String amount, String transactionRefNo);

	WalletResponse initiateLoadFundsToMMWalletRazorpay(MUser mUser, String amount, String transactionRefNo);

	//UserKycResponse setIdDetailsCorp(MUser request);

	
}
