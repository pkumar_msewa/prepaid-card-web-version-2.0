package com.msscard.app.api;

import java.util.List;
import com.msscard.model.BusTicketResp;

public interface IBusApi {
	
	public List<BusTicketResp> getAllTickets(String sessionId);
}
