package com.msscard.app.api;

import com.msscard.app.model.request.BusGetTxnIdRequest;
import com.msscard.app.model.request.BusSaveSeatDetailsRequest;
import com.msscard.app.model.response.BusSaveSeatDetailsResponse;
import com.msscard.app.model.response.CommonResponse;

public interface ITravelBusApi {

	BusSaveSeatDetailsResponse saveSeatDetails(BusSaveSeatDetailsRequest dto, BusSaveSeatDetailsResponse result);

	CommonResponse getTxnIdUpdated(BusGetTxnIdRequest dto, CommonResponse result);
	
	CommonResponse getAllBookTickets(String sessionId, CommonResponse result);

}
