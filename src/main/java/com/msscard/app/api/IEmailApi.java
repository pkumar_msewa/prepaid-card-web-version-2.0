package com.msscard.app.api;

import java.util.Date;

public interface IEmailApi {
	
	public void getSummary(Date startDate,Date endDate,String subject,String heading,String fileName) throws Exception;

}
