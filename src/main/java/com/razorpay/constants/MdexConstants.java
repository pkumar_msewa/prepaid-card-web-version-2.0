package com.razorpay.constants;


import java.nio.charset.StandardCharsets;

import javax.xml.bind.DatatypeConverter;

import com.msscard.app.model.request.ResponseDTO;
import com.msscard.entity.ERCredentialsManager;
import com.msscard.repositories.ERCredentialManager;
import com.msscard.util.MatchMoveUtil;

public class MdexConstants {
	
	private static ERCredentialManager eRCredentialManager;
	
	public MdexConstants(ERCredentialManager eRCredentialManager) {
		this.eRCredentialManager = eRCredentialManager;
	}
	
	/*public static String MDEX_DOMAIN;
	
	public static String MDEX_KEY;
	
	public static String MDEX_TOKEN;
	
	


	
	public static void setEnvironment(boolean flag){
		if(flag){
			MDEX_DOMAIN="https://mdex.msewa.com";
			MDEX_KEY="ED219A6C39BC7610849661BC5B748654B83C5926";
			MDEX_DOMAIN="MTU0NzIxMjkwNzAxMjM1NjAwMDAwMDAwMDAwMzc=";
		}else{
			MDEX_DOMAIN="http://106.51.8.246/mdex";
			MDEX_KEY="";
			MDEX_DOMAIN="";
		}
	}
*/	
	
	
	public static ResponseDTO getMdexCredentials() {
		ResponseDTO result = new ResponseDTO();
		try {
			String domain = null;
			String basicAuth=null;
			String key=null;
			String token=null;
			if(MatchMoveUtil.PRODUCTION){
			ERCredentialsManager credentialManager=eRCredentialManager.getCredentials("LIVE");
			if(credentialManager!=null){
				domain=credentialManager.getMdex_base_url();
				basicAuth=credentialManager.getBasic_auth_header();
				key=credentialManager.getMdex_keys();
				token=credentialManager.getMdex_token();
			}
			} else {
				ERCredentialsManager credentialManager=eRCredentialManager.getCredentials("TEST");
				if(credentialManager!=null){
				domain=credentialManager.getMdex_base_url();
				basicAuth=credentialManager.getBasic_auth_header();
				key=credentialManager.getMdex_keys();
				token=credentialManager.getMdex_token();
				}
			}
			result.setCode(key);
			result.setStatus(token);
		} catch(Exception e) {
			e.printStackTrace();
			
		}
		return result;
	}
	
	
	public static final String PREPAID_SERVICE="PSU";
	
	//public static final String MDEX_SERVICES = "services";

	public  static final String MDEX_URL="/ws/api/v1/Client/en/"; //live

     public static final String MDEX_SERVICES="/api/recharge/services/";
     
     public static final String BUS_CITY_LIST="/api/travel/bus/citylist";
     
   public static final String MDEX_KEY = "";
   public static final String MDEX_TOKEN = "";
	
	
	
	//live
	public static final String MDEX_CLIENTIP = "52.15.165.153";
	
	//test
	//public static final String MDEX_CLIENTIP = "106.51.8.246";
	
	//for bus API 
	//live

	public static final String MDEX_BROWSE_PLANS ="/api/recharge/browseplans";
    public static final String MDEX_MNPLOOKUP="/api/recharge/mnplookup";


	//test   
	//public static final String MDEX_DOMAIN = "http://106.51.8.246/mdex";
	public static final String MDEX_TRAVEL_URL ="/Api/Client/Website/en";
	public static final String BUS_GET_ALL_AVAILABLE_TRIPS ="/Bus/GetAllAvailableTrips";
	public static final String BUS_GET_SEAT_DETAILS = "/Bus/GetSeatDetails";
	public static final String BUS_GET_TXN_ID_UPDATED = "/Bus/GetTransactionIdUpdated";
	public static final String BUS_GET_BOOK_TICKET_UPDATED = "/Bus/BookTicketUpdated";
	public static final String BUS_GET_ISCANCELABLE_TICKET = "/Bus/IsCancellable";
	public static final String BUS_GET_CANCEL_TICKET = "/Bus/CancelTicket";
	public static final String TELCO_OPERATOR ="/api/recharge/gettelco";
	public static final String TELCO_PLANS ="/api/recharge/getplans";
	public static final String DUE_AMOUNT ="/api/recharge/dueamount";
	
	//Flight url	
	public static final String Flight_SOURCE = "/Flight/AirlineNameRQ";
	public static final String Flight_Search_API = "/Flight/FlightSearch";
	public static final String Flight_AirRePriceRQ_API = "/Flight/AirRePriceRQ";
	public static final String Flight_AirRePriceRQ_API_Connecting = "/Flight/AirRePrice";
	public static final String Flight_FlightBookingInitiate_API = "/Flight/FlightBookingInitiate";
	public static final String Flight_AirBookRQ_API = "/Flight/AirBookRQ";
	public static final String FLIGHT_ISCANCELABLE_TICKET = "/Flight/FlightBookingDetail";
	
	//live base url
	
	//test base url
	//public static final String BASE_URL = "http://106.51.8.246/mdex/Api/v1/Client/en/insurance";	
	public static final String FINGOOLE_REG ="/Api/v1/Client/en/insurance/RegisterUser";
	public static final String FINGOOLE_BATCH ="/Api/v1/Client/en/insurance/GetBatchNo";
	public static final String FINGOOLE_PROCESS ="/Api/v1/Client/en/insurance/ProcessTxn";
	
	//live service url
	public static final String FINGOOLE_SERVICES = "/api/recharge/services/FING";
	
	
	
	
}
