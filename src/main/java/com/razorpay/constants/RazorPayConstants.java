package com.razorpay.constants;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import com.msscard.app.model.request.PGTransaction;

public class RazorPayConstants {
	
	/**
	 * LATEST NEW LIVE
	 * */
	
	public static final String KEY_ID="rzp_live_1PSfjQPHqWf0WC";
	public static final String KEY_SECRET="x9prAI3gv5jcPceXAOOHjfrd";
	
	/**
	 * LATEST NEW TEST
	 * */
	

	/*public static final String KEY_ID="rzp_test_FbHAUNTWtCOWEo";
	public static final String KEY_SECRET="tfN2vNOuGwJkxw32Al26IbtU";*/
		


	

	/**
	 * NEW LIVE
	 * */
	
/*	public static final String KEY_ID="rzp_live_LWp2uO2ICAy86u";
	public static final String KEY_SECRET="CjzrGdRdsS8PdCJRcaZynEd9";*/
	
	/**
	 * MSEWA ACCOUNT
	 * */
	
	/*public static final String KEY_ID="rzp_live_2nJCkWyOINZfqw";
	public static final String KEY_SECRET="NH9ChiRbmogh46WvWxAsD5IH";*/
	
	//razorpay test
	/*public static final String KEY_ID = "rzp_test_bbwNHXln05VpIZ";
	public static final String KEY_SECRET = "vGEYjz4fhJxJGlbPVTfWdrym";*/
	
	
	/**
	 * TEST
	 * */
	
	/*public static final String KEY_ID="rzp_test_zPEcivDLFCp2ls";
	public static final String KEY_SECRET="NGBZu0hfUfGLNQmSVnFx0cT5";*/
	
	/**
	 * LIVE
	 * */
	
	
	/*public static final String KEY_ID="rzp_live_eYc8EMcMqmEdtu";
	public static final String KEY_SECRET="mdHD4LisabhYuZ17s9BSgDZX";*/

	public static final String LOGO_PATH="resources/images/logo1.png";
	public static final String SERVICE_CODE="LMS";
	public static final String PROMO_SERVICES="PROMO";
	public static final String DESCRIPTION_CARD_ISSUANCE="Payment for Physical card Issuance";
	public static final String DESCRIPTION="Load Money to card";
	
	//test upi ewire
	
	/*public static final String UPI_URL="http://106.51.8.246/cashier/ws/api/authValidate";
	public static final String UPI_STATUS="http://106.51.8.246/cashier/ws/api/UpiStatus";
	
	public static final String UPI_MERCHANT_ID="52";
	public static final String UPI_TOKEN="D9622E5A540E36C832F946987CDF7FA0";
	
	public static final String UPI_SERVICE="UPS";*/
	
	
	
	/**
	 * LIVE
	 * */
	
	public static final String UPI_URL="https://msspay.in/ws/api/authValidate";
	public static final String UPI_STATUS="https://msspay.in/ws/api/UpiStatus";
	public static final String UPI_MERCHANT_ID="56";
	public static final String UPI_TOKEN="064D1302F04EB182F005C210A517485C";
	public static final String UPI_SERVICE="UPS";
	
		/**
	 * PAYPHI TEST
	 * */
	
	/*public static final String SALE_URL="https://qa.phicommerce.com/pg/api/sale?v=2";
	public static final String STATUS_CHECK="https://qa.phicommerce.com/pg/api/command?v=2";
	public static final String MERCHANT_ID="T_10046";
	public static final String CURRENCY_CODE="356";
	public static final String PAY_TYPE="0";
	public static final String RETURN_URL="http://13.233.137.181/Ewire-Rupay/Api/v1/User/Android/en/LoadMoney/PGHandler";
	public static final String TRANSACTION_TYPE="SALE";
	public static final String TRANSACTION_KEY_TEST="ce826553e5df4899a1a4a362670603ea";*/

	
	/**
	 * PAYPHI LIVE
	 */
	
	
	public static final String SALE_URL="https://secure-ptg.payphi.com/pg/api/sale?v=2";
	public static final String STATUS_CHECK="https://secure-ptg.payphi.com/pg/api/command?v=2";
	public static final String MERCHANT_ID="P_30016";
	public static final String CURRENCY_CODE="356";
	public static final String PAY_TYPE="0";
	public static final String RETURN_URL="https://liveewire.com:8443/Api/v1/User/Android/en/LoadMoney/PGHandler";
	public static final String TRANSACTION_TYPE="SALE";
	public static final String TRANSACTION_KEY_TEST="f1fc4e1d22ae4c20a188dc0dcc1f297f";

	
	
	


	public static String hmacDigest(String msg, String keyString) {
		String digest = null;
		try {
		SecretKeySpec key = new SecretKeySpec((keyString).getBytes("UTF-8"),"HmacSHA256");
		Mac mac = Mac.getInstance("HmacSHA256");
		mac.init(key);
		byte[] bytes = mac.doFinal(msg.getBytes("ASCII"));
		StringBuffer hash = new StringBuffer();
		for (int i = 0; i <bytes.length; i++) {
		String hex = Integer.toHexString(0xFF & bytes[i]);
		if (hex.length() == 1) {
		hash.append('0');
		}
		hash.append(hex);
		}
		digest = hash.toString();
		} catch (UnsupportedEncodingException e) {
		} catch (InvalidKeyException e) {
		} catch (NoSuchAlgorithmException e) {
		}
		return digest;
		}
	
	
	public static String composeMessage(PGTransaction message){
		
		String msg=message.getAmount()+message.getCurrencyCode()+message.getCustomerEmailId()+message.getCustomerMobileNo()+message.getMerchantId()
		+ message.getMerchantTxnNo()+message.getPayType()+message.getReturnURL()+message.getTransactionType()+message.getTxnDate();
		
		return hmacDigest(msg,TRANSACTION_KEY_TEST);
		
	}
	
	public static String composeMessageStatusCheck(String merchantID,String merchantTxnNo,String originalTxnNo,String transactionType){
		String msg=merchantID+merchantTxnNo+originalTxnNo+transactionType;
		return hmacDigest(msg, TRANSACTION_KEY_TEST);
	}

	}
