package com.ewire.cron;

import java.sql.SQLIntegrityConstraintViolationException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.msscard.app.api.CorporateMatchMoveApi;
import com.msscard.app.api.FCMSenderApi;
import com.msscard.app.api.IEmailApi;
import com.msscard.app.api.IMailSenderApi;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.IMdexApi;
import com.msscard.app.api.ISMSSenderApi;
import com.msscard.app.api.ITransactionApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.MatchMoveCreateCardRequest;
import com.msscard.app.model.request.MdexTransactionRequestDTO;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.request.UserKycRequest;
import com.msscard.app.model.response.AddressResponseDTO;
import com.msscard.app.model.response.MdexTransactionResponseDTO;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.app.sms.util.SMSAccount;
import com.msscard.app.sms.util.SMSTemplete;
import com.msscard.entity.BulkCardAssignmentGroup;
import com.msscard.entity.BulkLoadMoney;
import com.msscard.entity.BulkRegister;
import com.msscard.entity.BulkRegisterGroup;
import com.msscard.entity.CorporateAgentDetails;
import com.msscard.entity.CorporateFileCatalogue;
import com.msscard.entity.FCMDetails;
import com.msscard.entity.GroupBulkKyc;
import com.msscard.entity.GroupBulkRegister;
import com.msscard.entity.GroupDetails;
import com.msscard.entity.GroupFileCatalogue;
import com.msscard.entity.GroupSms;
import com.msscard.entity.MKycDetail;
import com.msscard.entity.MMCards;
import com.msscard.entity.MPQAccountDetails;
import com.msscard.entity.MPQAccountType;
import com.msscard.entity.MService;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.entity.SendMoneyDetails;
import com.msscard.model.RegisterDTO;
import com.msscard.model.RequestDTO;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Status;
import com.msscard.model.UserType;
import com.msscard.model.error.GroupCardGenerationErrorDTO;
import com.msscard.model.error.RegisterError;
import com.msscard.model.error.TransactionError;
import com.msscard.repositories.BulkCardAssignmentGroupRepository;
import com.msscard.repositories.BulkLoadMoneyRepository;
import com.msscard.repositories.BulkRegisterGroupRepository;
import com.msscard.repositories.BulkRegisterRepository;
import com.msscard.repositories.CorporateAgentDetailsRepository;
import com.msscard.repositories.CorporateFileCatalogueRepository;
import com.msscard.repositories.FCMDetailsRepository;
import com.msscard.repositories.GroupBulkKycRepository;
import com.msscard.repositories.GroupBulkRegisterRepository;
import com.msscard.repositories.GroupFileCatalogueRepository;
import com.msscard.repositories.GroupSmsRepository;
import com.msscard.repositories.LoadCardRepository;
import com.msscard.repositories.MKycRepository;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MPQAccountDetailRepository;
import com.msscard.repositories.MPQAccountTypeRepository;
import com.msscard.repositories.MServiceRepository;
import com.msscard.repositories.MTransactionRepository;
import com.msscard.repositories.MUserRespository;
import com.msscard.repositories.SendMoneyDetailsRepository;
import com.msscard.util.CSVReader;
import com.msscard.util.CommonUtil;
import com.msscard.util.KYCMail;
import com.msscard.util.MailTemplate;
import com.msscard.util.StartUpUtil;
import com.msscard.validation.BulkLoadMoneyValidation;
import com.msscard.validation.CardCreationGroupValidation;
import com.msscard.validation.LoadMoneyValidation;
import com.msscard.validation.RegisterValidation;

public class AutoReverse {
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private final SendMoneyDetailsRepository sendMoneyDetailsRepository;
	private final IMatchMoveApi matchMoveApi;
	private final MMCardRepository cardRepository;
	private final MUserRespository userRespository;
	private final ISMSSenderApi senderApi;
	private final KYCMail kycMail;
	private final CorporateAgentDetailsRepository corporateAgentDetailsRepository;
	private final CorporateFileCatalogueRepository corporateFileCatalogueRepository;
	private final CorporateMatchMoveApi corporateMatchMoveApi;
	private final BulkRegisterRepository bulkRegisterRepository;
	private final IUserApi userApi;
	private final RegisterValidation registerValidation;
	private final BulkLoadMoneyValidation bulkLoadMoneyValidation;
	private final ITransactionApi transactionApi;
	private final MTransactionRepository transactionRepository;
	private final BulkLoadMoneyRepository bulkLoadMoneyRepository;
	private final IMdexApi mdexApi;
	private final IEmailApi iEmailApi;
	private final FCMDetailsRepository fcmDetailsRepository;
	private final FCMSenderApi fcmSenderApi;
	private final MServiceRepository mServiceRepository;
	private final LoadMoneyValidation loadMoneyValidation;
	private final MKycRepository mKycRepository;
	private final MPQAccountTypeRepository mPQAccountTypeRepository;
	private final MPQAccountDetailRepository mPQAccountDetailRepository;
	private final GroupSmsRepository groupSmsRepository;
	private final MTransactionRepository mTransactionRepository;
	private final LoadCardRepository loadCardRepository;
	private final GroupFileCatalogueRepository groupFileCatalogueRepository;
	private final BulkRegisterGroupRepository bulkRegisterGroupRepository;
	private final IMailSenderApi mailSenderApi;
	private final GroupBulkRegisterRepository groupBulkRegisterRepository;
	private final BulkCardAssignmentGroupRepository bulkCardAssignmentGroupRepository;
	private final CardCreationGroupValidation cardCreationGroupValidation;
	private final GroupBulkKycRepository groupBulkKycRepository;

	public AutoReverse(SendMoneyDetailsRepository sendMoneyDetailsRepository, IMatchMoveApi matchMoveApi,
			MMCardRepository cardRepository, MUserRespository userRespository, ISMSSenderApi senderApi, KYCMail kycMail,
			CorporateAgentDetailsRepository corporateAgentDetailsRepository,
			CorporateFileCatalogueRepository corporateFileCatalogueRepository,
			CorporateMatchMoveApi corporateMatchMoveApi, BulkRegisterRepository bulkRegisterRepository,
			IUserApi userApi, RegisterValidation registerValidation, BulkLoadMoneyValidation bulkLoadMoneyValidation,
			ITransactionApi transactionApi, MTransactionRepository transactionRepository,
			BulkLoadMoneyRepository bulkLoadMoneyRepository, IMdexApi mdexApi, IEmailApi iEmailApi,
			FCMDetailsRepository fcmDetailsRepository, FCMSenderApi fcmSenderApi, MServiceRepository mServiceRepository,
			LoadMoneyValidation loadMoneyValidation, MKycRepository mKycRepository,
			MPQAccountTypeRepository mPQAccountTypeRepository, MPQAccountDetailRepository mPQAccountDetailRepository,
			GroupSmsRepository groupSmsRepository, MTransactionRepository mTransactionRepository,
			LoadCardRepository loadCardRepository, GroupFileCatalogueRepository groupFileCatalogueRepository,
			BulkRegisterGroupRepository bulkRegisterGroupRepository, IMailSenderApi mailSenderApi,
			GroupBulkRegisterRepository groupBulkRegisterRepository,
			BulkCardAssignmentGroupRepository bulkCardAssignmentGroupRepository,
			CardCreationGroupValidation cardCreationGroupValidation, GroupBulkKycRepository groupBulkKycRepository) {
		super();
		this.sendMoneyDetailsRepository = sendMoneyDetailsRepository;
		this.matchMoveApi = matchMoveApi;
		this.cardRepository = cardRepository;
		this.userRespository = userRespository;
		this.senderApi = senderApi;
		this.kycMail = kycMail;
		this.corporateAgentDetailsRepository = corporateAgentDetailsRepository;
		this.corporateFileCatalogueRepository = corporateFileCatalogueRepository;
		this.corporateMatchMoveApi = corporateMatchMoveApi;
		this.bulkRegisterRepository = bulkRegisterRepository;
		this.userApi = userApi;
		this.registerValidation = registerValidation;
		this.bulkLoadMoneyValidation = bulkLoadMoneyValidation;
		this.transactionApi = transactionApi;
		this.transactionRepository = transactionRepository;
		this.bulkLoadMoneyRepository = bulkLoadMoneyRepository;
		this.mdexApi = mdexApi;
		this.iEmailApi = iEmailApi;
		this.fcmDetailsRepository = fcmDetailsRepository;
		this.fcmSenderApi = fcmSenderApi;
		this.mServiceRepository = mServiceRepository;
		this.loadMoneyValidation = loadMoneyValidation;
		this.mKycRepository = mKycRepository;
		this.mPQAccountTypeRepository = mPQAccountTypeRepository;
		this.mPQAccountDetailRepository = mPQAccountDetailRepository;
		this.groupSmsRepository = groupSmsRepository;
		this.mTransactionRepository = mTransactionRepository;
		this.loadCardRepository = loadCardRepository;
		this.groupFileCatalogueRepository = groupFileCatalogueRepository;
		this.bulkRegisterGroupRepository = bulkRegisterGroupRepository;
		this.mailSenderApi = mailSenderApi;
		this.groupBulkRegisterRepository = groupBulkRegisterRepository;
		this.bulkCardAssignmentGroupRepository = bulkCardAssignmentGroupRepository;
		this.cardCreationGroupValidation = cardCreationGroupValidation;
		this.groupBulkKycRepository = groupBulkKycRepository;
	}

	public void checkForMoneyTransfer() {
		List<SendMoneyDetails> sendDetails = sendMoneyDetailsRepository.getTransactionWithPendingStatus();
		if (sendDetails != null) {
			for (SendMoneyDetails sendMoneyDetails : sendDetails) {
				String refNo = sendMoneyDetails.getTransactionId();
				String recipeintNo = sendMoneyDetails.getRecipientNo();
				String amount = sendMoneyDetails.getAmount();
				MUser sender = sendMoneyDetails.getUser();
				MUser recipientUser = userRespository.findByUsername(recipeintNo);
				WalletResponse resp = matchMoveApi.acknowledgeFundTransfer(amount, refNo, recipeintNo);
				if (resp != null && ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(resp.getCode())) {
					SendMoneyDetails trxDetails = sendMoneyDetailsRepository.getByTransactionRefNo(refNo);
					trxDetails.setStatus("Success");
					trxDetails.setChecked(true);
					sendMoneyDetailsRepository.save(trxDetails);
					senderApi.sendUserSMSFund(SMSAccount.PAYQWIK_OTP, SMSTemplete.SENDMONEY_SUCCESS, sender, amount,
							recipientUser);
					senderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.SENDMONEY_SUCCESS_RECEIVER, recipientUser,
							amount + " from " + sender.getUsername());
					mailSenderApi.sendFundTransfer("Fund Transfer Successful", MailTemplate.FUND_TRANSFER, sender,
							recipientUser, sendMoneyDetails);
				} else {
					SendMoneyDetails trxDetails = sendMoneyDetailsRepository.getByTransactionRefNo(refNo);
					trxDetails.setChecked(true);
					sendMoneyDetailsRepository.save(trxDetails);
				}
			}
		}
	}

	public void kycStatusCheck() {
		List<MKycDetail> users = mKycRepository.findAllKyc(false);
		if (users != null && users.size() > 0) {
			for (int i = 0; i < users.size(); i++) {
				UserKycResponse resp = matchMoveApi.checkKYCStatus(users.get(i).getUser());
				if (resp.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
					if (resp.getStatus().equalsIgnoreCase("rejected")) {
						MKycDetail detail = users.get(i);
						// detail.setRejectionStatus(true);
						MPQAccountDetails account = users.get(i).getUser().getAccountDetail();
						MPQAccountType type = users.get(i).getAccountType();
						if (type.getCode().equals("KYC")) {
							MPQAccountType mat = mPQAccountTypeRepository.findByCode("NONKYC");
							account.setAccountType(mat);
							mPQAccountDetailRepository.save(account);
							detail.setRejectionStatus(true);
							detail.setRejectionReason("rejected on matchmove end");
							mKycRepository.save(detail);
						}
					}
				}
			}
		}
	}

	/*
	 * public void RefundStatusCheck() { System.err.println("refund status");
	 * MService service = mServiceRepository.findServiceByCode("LMS"); DateFormat
	 * dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"); Calendar cal =
	 * Calendar.getInstance(); cal.add(Calendar.DATE, -1);
	 * System.out.println(dateFormat.format(cal.getTime())); Date from =
	 * cal.getTime(); Calendar calendar = Calendar.getInstance();
	 * calendar.setTime(cal.getTime()); calendar.add(Calendar.HOUR_OF_DAY, 24); Date
	 * to = calendar.getTime(); System.err.println("to" +
	 * dateFormat.format(calendar.getTime())); List<MTransaction> transaction =
	 * mTransactionRepository.getTransactionAutoRefund(from, to, service, false,
	 * Status.Success); MMCards physical = null; MMCards virtual = null;
	 * WalletResponse cardTransferResposne = null; if(transaction != null) { Double
	 * prefund = 0.0; UserKycResponse walletResponse=matchMoveApi.getConsumers();
	 * if(walletResponse!=null){ prefund =
	 * Double.parseDouble(walletResponse.getPrefundingBalance()); } for(int i = 0; i
	 * < transaction.size(); i++) { if(prefund > transaction.get(i).getAmount()) {
	 * MUser user = userRespository.getByAccount(transaction.get(i).getAccount());
	 * MatchMoveCreateCardRequest reque = new MatchMoveCreateCardRequest();
	 * reque.setUsername(user.getUsername()); WalletResponse waResponse =
	 * matchMoveApi.fetchWalletMM(reque);
	 * if(waResponse.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue()))
	 * { double availableBal = Double.valueOf(waResponse.getAvailableAmt());
	 * if(availableBal > 0) { physical = cardRepository.getPhysicalCardByUser(user);
	 * if(physical != null && !physical.isBlocked()) { cardTransferResposne =
	 * matchMoveApi.transferFundsToMMCard(user, waResponse.getAvailableAmt(),
	 * physical.getCardId());
	 * if(cardTransferResposne.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.
	 * getValue())) {
	 * transaction.get(i).setCardLoadStatus(Status.Success.getValue());
	 * mTransactionRepository.save(transaction.get(i)); } } virtual =
	 * cardRepository.getVirtualCardsByCardUser(user); if (virtual != null &&
	 * virtual.getStatus().equalsIgnoreCase("Active")) { cardTransferResposne =
	 * matchMoveApi.transferFundsToMMCard(user, waResponse.getAvailableAmt(),
	 * virtual.getCardId()); if
	 * (cardTransferResposne.getCode().equalsIgnoreCase("S00")) {
	 * transaction.get(i).setCardLoadStatus(Status.Success.getValue());
	 * mTransactionRepository.save(transaction.get(i)); } } } else
	 * if(transaction.get(i).getStatus().equals(Status.Success)){ WalletResponse
	 * walletRespon = matchMoveApi.initiateLoadFundsToMMWallet(user,
	 * String.valueOf(transaction.get(i).getAmount())); if
	 * (walletRespon.getCode().equalsIgnoreCase("S00")) { String
	 * authRefNo=walletRespon.getAuthRetrivalNo();
	 * transaction.get(i).setAuthReferenceNo(authRefNo);
	 * mTransactionRepository.save(transaction.get(i)); physical =
	 * cardRepository.getPhysicalCardByUser(user); if (physical != null &&
	 * !physical.isBlocked()) { cardTransferResposne =
	 * matchMoveApi.transferFundsToMMCard(user,
	 * String.valueOf(transaction.get(i).getAmount()), physical.getCardId()); if
	 * (cardTransferResposne.getCode().equalsIgnoreCase("S00")) {
	 * transaction.get(i).setCardLoadStatus(Status.Success.getValue());
	 * mTransactionRepository.save(transaction.get(i)); } } virtual =
	 * cardRepository.getVirtualCardsByCardUser(user); if (virtual != null &&
	 * !virtual.isBlocked()) { cardTransferResposne =
	 * matchMoveApi.transferFundsToMMCard(user,
	 * String.valueOf(transaction.get(i).getAmount()), virtual.getCardId()); if
	 * (cardTransferResposne.getCode().equalsIgnoreCase("S00")) {
	 * transaction.get(i).setCardLoadStatus(Status.Success.getValue());
	 * mTransactionRepository.save(transaction.get(i)); } } } } } } } }
	 * 
	 * }
	 * 
	 * public void RefundStatusChecknineteen() {
	 * System.err.println("refund status"); MService service =
	 * mServiceRepository.findServiceByCode("LMS"); Date from = null; Date to =
	 * null; try { String sDate1="2018-11-01"; from= sdformat.parse(sDate1 +
	 * " 00:00:00"); System.out.println("from "+from);
	 * 
	 * String sDate2="2018-11-30"; to = sdformat.parse(sDate2 + " 23:59:59");
	 * System.err.println("to" + to); } catch(Exception e) { e.printStackTrace(); }
	 * List<MTransaction> transaction =
	 * mTransactionRepository.getTransactionAutoRefund(from, to, service, false,
	 * Status.Success); MMCards physical = null; MMCards virtual = null;
	 * WalletResponse cardTransferResposne = null; if(transaction != null) { Double
	 * prefund = 0.0; UserKycResponse walletResponse=matchMoveApi.getConsumers();
	 * if(walletResponse!=null){ prefund =
	 * Double.parseDouble(walletResponse.getPrefundingBalance()); } for(int i = 0; i
	 * < transaction.size(); i++) { if(prefund > transaction.get(i).getAmount()) {
	 * MUser user = userRespository.getByAccount(transaction.get(i).getAccount());
	 * MatchMoveCreateCardRequest reque = new MatchMoveCreateCardRequest();
	 * reque.setUsername(user.getUsername()); WalletResponse waResponse =
	 * matchMoveApi.fetchWalletMM(reque);
	 * if(waResponse.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue()))
	 * { double availableBal = Double.valueOf(waResponse.getAvailableAmt());
	 * if(availableBal > 0) { physical = cardRepository.getPhysicalCardByUser(user);
	 * if(physical != null && !physical.isBlocked()) { cardTransferResposne =
	 * matchMoveApi.transferFundsToMMCard(user, waResponse.getAvailableAmt(),
	 * physical.getCardId());
	 * if(cardTransferResposne.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.
	 * getValue())) {
	 * transaction.get(i).setCardLoadStatus(Status.Success.getValue());
	 * mTransactionRepository.save(transaction.get(i)); } } virtual =
	 * cardRepository.getVirtualCardsByCardUser(user); if (virtual != null &&
	 * virtual.getStatus().equalsIgnoreCase("Active")) { cardTransferResposne =
	 * matchMoveApi.transferFundsToMMCard(user, waResponse.getAvailableAmt(),
	 * virtual.getCardId()); if
	 * (cardTransferResposne.getCode().equalsIgnoreCase("S00")) {
	 * transaction.get(i).setCardLoadStatus(Status.Success.getValue());
	 * mTransactionRepository.save(transaction.get(i)); } } } else
	 * if(transaction.get(i).getStatus().equals(Status.Success)){ WalletResponse
	 * walletRespon = matchMoveApi.initiateLoadFundsToMMWallet(user,
	 * String.valueOf(transaction.get(i).getAmount())); if
	 * (walletRespon.getCode().equalsIgnoreCase("S00")) { String
	 * authRefNo=walletRespon.getAuthRetrivalNo();
	 * transaction.get(i).setAuthReferenceNo(authRefNo);
	 * mTransactionRepository.save(transaction.get(i)); physical =
	 * cardRepository.getPhysicalCardByUser(user); if (physical != null &&
	 * !physical.isBlocked()) { cardTransferResposne =
	 * matchMoveApi.transferFundsToMMCard(user,
	 * String.valueOf(transaction.get(i).getAmount()), physical.getCardId()); if
	 * (cardTransferResposne.getCode().equalsIgnoreCase("S00")) {
	 * transaction.get(i).setCardLoadStatus(Status.Success.getValue());
	 * mTransactionRepository.save(transaction.get(i)); } } virtual =
	 * cardRepository.getVirtualCardsByCardUser(user); if (virtual != null &&
	 * !virtual.isBlocked()) { cardTransferResposne =
	 * matchMoveApi.transferFundsToMMCard(user,
	 * String.valueOf(transaction.get(i).getAmount()), virtual.getCardId()); if
	 * (cardTransferResposne.getCode().equalsIgnoreCase("S00")) {
	 * transaction.get(i).setCardLoadStatus(Status.Success.getValue());
	 * mTransactionRepository.save(transaction.get(i)); } } } } } } } }
	 * 
	 * }
	 */

	public void virtualcard() {
		System.err.println("virtual Card cron");
		List<MMCards> physicalCard = cardRepository.getPhysicalCards(true, false);
		for (int i = 0; i < physicalCard.size(); i++) {
			MUser user = physicalCard.get(i).getWallet().getUser();
			MMCards virtualCard = cardRepository.getVirtualCardsByCardUser(user);
			if (virtualCard != null) {
				if (!virtualCard.isBlocked() || virtualCard.getStatus().equalsIgnoreCase("Active")) {
					virtualCard.setBlocked(true);
					virtualCard.setStatus(Status.Inactive.getValue());
					cardRepository.save(virtualCard);
				}
			}
		}
	}

	public void recheckTransactions() {
		List<SendMoneyDetails> sendMoneyDetails = sendMoneyDetailsRepository.getTransactionWithFailedStatus();
		if (sendMoneyDetails != null) {
			for (SendMoneyDetails sendMoneyDetails2 : sendMoneyDetails) {
				String refNo = sendMoneyDetails2.getTransactionId();
				String recipeintNo = sendMoneyDetails2.getRecipientNo();
				String amount = sendMoneyDetails2.getAmount();
				MUser sender = sendMoneyDetails2.getUser();
				MUser recipientUser = userRespository.findByUsername(recipeintNo);
				WalletResponse resp = matchMoveApi.acknowledgeFundTransfer(amount, refNo, recipeintNo);
				if (resp.getCode().equalsIgnoreCase("S00")) {
					SendMoneyDetails trxDetails = sendMoneyDetailsRepository.getByTransactionRefNo(refNo);
					trxDetails.setStatus("Success");
					trxDetails.setChecked(true);
					sendMoneyDetailsRepository.save(trxDetails);
					senderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.SENDMONEY_SUCCESS, sender, amount);
					senderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.SENDMONEY_SUCCESS_RECEIVER, recipientUser,
							amount + " from " + sender.getUsername());

				} else {
					SendMoneyDetails trxDetails = sendMoneyDetailsRepository.getByTransactionRefNo(refNo);
					trxDetails.setChecked(true);
					trxDetails.setError(resp.getMessage());
					trxDetails.setStatus("Failed");
					sendMoneyDetailsRepository.save(trxDetails);
					WalletResponse refund = matchMoveApi.cancelFundTransfer(recipientUser, amount, refNo);
					if (refund.getCode().equalsIgnoreCase("S00")) {
						senderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.REFUND_SENDMONEY, sender, amount);
					}
				}
			}
		}
	}

	public void bulkSms() {
		System.err.println("bulk SMS");
		List<GroupFileCatalogue> fileList = groupFileCatalogueRepository.getListOfBulkSms();
		if (fileList != null && fileList.size() > 0) {
			for (GroupFileCatalogue corporateFileCatalogue : fileList) {
				System.err.println(corporateFileCatalogue);
				if (corporateFileCatalogue.isFileRejectionStatus() == false) {
					if (corporateFileCatalogue.isReviewStatus() == true) {
						if (corporateFileCatalogue.getCategoryType().equalsIgnoreCase("BLKSMS")) {
							String absPath = corporateFileCatalogue.getAbsPath();
							String filePath = StartUpUtil.CSV_FILE + absPath;
							try {
								System.err.println("This is file Path:" + filePath);
								JSONObject fileJsonObject = CSVReader.readCsvBulkUploadSms(filePath);
								if (fileJsonObject != null) {
									JSONArray jArray = fileJsonObject.getJSONArray("UserList");
									if (jArray != null) {
										for (int i = 0; i < jArray.length(); i++) {
											JSONObject jObject = jArray.getJSONObject(i);
											if (jObject != null) {
												String mobile = jObject.getString("mobile");
												String message = jObject.getString("message");
												GroupDetails gd = corporateFileCatalogue.getUser().getGroupDetails();
												MUser u = userApi.findByUserName(mobile);
												if (u != null) {
													if (u.getGroupDetails().equals(gd) && u.getUserDetail()
															.getGroupStatus().equals(Status.Active.getValue())) {
														userApi.sendSingleGroupSMS(mobile, message);
													}
												}
											}
										}
										corporateFileCatalogue.setSchedulerStatus(true);
										groupFileCatalogueRepository.save(corporateFileCatalogue);
									}
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
		}
	}

	public void bulkRegisterWhileFailedGroup() {
		System.err.println("bulk register for failed group is running MM>>>>>>>>>>>");
		List<GroupFileCatalogue> fileList = groupFileCatalogueRepository.getListOfBulkRegister();
		if (fileList != null && fileList.size() > 0) {
			for (GroupFileCatalogue corporateFileCatalogue : fileList) {
				if (corporateFileCatalogue.isFileRejectionStatus() == false) {
					if (corporateFileCatalogue.isReviewStatus() == true) {
						if (corporateFileCatalogue.getCategoryType().equalsIgnoreCase("BLKREGISTER")) {
							String absPath = corporateFileCatalogue.getAbsPath();
							String filePath = StartUpUtil.CSV_FILE + absPath;
							try {
								System.err.println("This is file Path:::::" + filePath);
								JSONObject fileJsonObject = CSVReader.readCsvBulkUploadRegisterGroup(filePath);
								System.err.println(fileJsonObject);
								if (fileJsonObject != null) {
									JSONArray jArray = fileJsonObject.getJSONArray("UserList");
									if (jArray != null) {
										for (int i = 0; i < jArray.length(); i++) {
											JSONObject jObject = jArray.getJSONObject(i);
											if (jObject != null) {
												String fname = jObject.getString("firstName");
												String mname = jObject.getString("middleName");
												String lname = jObject.getString("lastName");
												String mobile = jObject.getString("mobile");
												String email = jObject.getString("email");
												String dob = jObject.getString("dob");
												String idType = jObject.getString("idType");
												String idNumber = jObject.getString("idNumber");
												if (dob == null) {
													dob = "1970-01-01";
												}

												RegisterDTO registerDTO = new RegisterDTO();
												registerDTO.setFirstName(fname);
												registerDTO.setMiddleName(mname);
												registerDTO.setLastName(lname);
												registerDTO.setUserType(UserType.User);
												registerDTO.setPassword("123456");
												registerDTO.setIdType(idType);
												registerDTO.setIdNo(idNumber);
												SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
												Date d1 = sdf.parse(dob);
												registerDTO.setDateOfBirth(sdf.format(d1));
												registerDTO.setEmail(email);
												registerDTO.setContactNo(mobile);
												registerDTO.setUsername(mobile);
												registerDTO.setGroup(
														corporateFileCatalogue.getUser().getGroupDetails().getEmail());
												registerDTO.setRemark(
														corporateFileCatalogue.getUser().getGroupDetails().getDname());
												ResponseDTO res = registerValidation.addAllUsers(registerDTO,
														corporateFileCatalogue);
												if (res.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
													RegisterError regErr = registerValidation.validateGroupUserRecheck(
															registerDTO, corporateFileCatalogue, false);
													System.err.println(regErr);
													if (regErr.isValid()) {
														UserKycResponse users = matchMoveApi.getUsers(email, mobile);
														MUser usermob = userApi.findByUserName(mobile);
														if (usermob == null) {
															userApi.saveUserCorpUser(registerDTO);
														}
														usermob = userApi.findByUserName(mobile);
														BulkRegisterGroup bulkRegister = bulkRegisterGroupRepository
																.getByUser(usermob);
														if (bulkRegister == null) {
															bulkRegister = new BulkRegisterGroup();
															bulkRegister.setDob(dob);
															bulkRegister.setEmail(email);
															bulkRegister.setMobile(mobile);
															bulkRegister.setUser(usermob);

															bulkRegister.setName(fname + " " + lname);

														}
														if (!users.getCode().equalsIgnoreCase("S00")) {
															ResponseDTO resUser = corporateMatchMoveApi
																	.createUserOnMM(usermob);
															if (resUser.getCode().equalsIgnoreCase("S00")) {
																bulkRegister.setUserCreationStatus(true);
																bulkRegister.setWalletCreationStatus(true);
																/*
																 * MMCards cardUser=
																 * cardRepository.getVirtualCardsByCardUser(usermob);
																 * if(cardUser==null){ WalletResponse
																 * phyCardCreate=corporateMatchMoveApi.assignVirtualCard
																 * (usermob);
																 * if(phyCardCreate.getCode().equalsIgnoreCase("S00")){
																 * bulkRegister.setCardCreationStatus(true);
																 * //bulkRegister.setPhyCardActivationStatus(true); //
																 * ResponseDTO activateResp=corporateMatchMoveApi.
																 * activateMMPhysicalCard(phyCardCreate.getCardId(),
																 * phyCardCreate.getActivationCode());
																 * if(activateResp.getCode().equalsIgnoreCase("S00")){
																 * bulkRegister.setPhyCardActivationStatus(true); }
																 * GroupBulkRegister gbr =
																 * groupBulkRegisterRepository.getUserRowByContact(
																 * usermob.getUsername(), corporateFileCatalogue,
																 * false); gbr.setSuccessStatus(true);
																 * groupBulkRegisterRepository.save(gbr); }
																 * 
																 * }
																 */UserKycRequest requ = new UserKycRequest();
																requ.setId_type(usermob.getUserDetail().getIdType());
																requ.setId_number(usermob.getUserDetail().getIdNo());
																requ.setUser(usermob);
																requ.setCountry("India");
																UserKycResponse resp = matchMoveApi.setIdDetails(requ);
																if (resp.getCode()
																		.equals(ResponseStatus.SUCCESS.getValue())) {
																	GroupBulkRegister gbr = groupBulkRegisterRepository
																			.getUserRowByContact(usermob.getUsername(),
																					corporateFileCatalogue, false,
																					Status.Initiated);
																	if (gbr != null) {
																		gbr.setSuccessStatus(true);
																		gbr.setStatus(Status.Success);
																		gbr.setFailReason("NA");
																		groupBulkRegisterRepository.save(gbr);
																	}
																} else {
																	GroupBulkRegister gbr = groupBulkRegisterRepository
																			.getUserRowByContact(usermob.getUsername(),
																					corporateFileCatalogue, false,
																					Status.Initiated);
																	if (gbr != null) {
																		gbr.setSuccessStatus(false);
																		gbr.setStatus(Status.Failed);
																		gbr.setFailReason(resp.getMessage());
																		groupBulkRegisterRepository.save(gbr);
																	}
																}

															} else {
																MatchMoveCreateCardRequest cd = new MatchMoveCreateCardRequest();
																cd.setEmail(usermob.getUserDetail().getEmail());
																cd.setMobile(usermob.getUsername());
																cd.setUsername(usermob.getUsername());
																WalletResponse walletResp = matchMoveApi
																		.fetchWalletMM(cd);
																if (walletResp != null && walletResp.getCode()
																		.equalsIgnoreCase("F00")) {
																	GroupBulkRegister gbr = groupBulkRegisterRepository
																			.getUserRowByContact(usermob.getUsername(),
																					corporateFileCatalogue, false,
																					Status.Initiated);
																	if (gbr != null) {
																		gbr.setFailReason(resUser.getMessage());
																		gbr.setSuccessStatus(false);
																		gbr.setStatus(Status.Failed);
																		groupBulkRegisterRepository.save(gbr);
																	}
																} else {
																	UserKycRequest requ = new UserKycRequest();
																	requ.setId_type(
																			usermob.getUserDetail().getIdType());
																	requ.setId_number(
																			usermob.getUserDetail().getIdNo());
																	requ.setUser(usermob);
																	requ.setCountry("India");
																	UserKycResponse resp = matchMoveApi
																			.setIdDetails(requ);
																	if (resp.getCode().equals(
																			ResponseStatus.SUCCESS.getValue())) {
																		GroupBulkRegister gbr = groupBulkRegisterRepository
																				.getUserRowByContact(
																						usermob.getUsername(),
																						corporateFileCatalogue, false,
																						Status.Initiated);
																		if (gbr != null) {
																			gbr.setFailReason("NA");
																			gbr.setSuccessStatus(true);
																			gbr.setStatus(Status.Success);
																			groupBulkRegisterRepository.save(gbr);
																		}
																	} else {
																		GroupBulkRegister gbr = groupBulkRegisterRepository
																				.getUserRowByContact(
																						usermob.getUsername(),
																						corporateFileCatalogue, false,
																						Status.Initiated);
																		if (gbr != null) {
																			gbr.setFailReason(resp.getMessage());
																			gbr.setSuccessStatus(false);
																			gbr.setStatus(Status.Failed);
																			groupBulkRegisterRepository.save(gbr);
																		}
																	}
																}
															}
															// Thread.sleep(5000);

															// bulkRegisterRepository.save(bulkRegister);
															// WalletResponse
															// wallResp=corporateMatchMoveApi.assignVirtualCard(usermob);
															// if(wallResp.getCode().equalsIgnoreCase("S00")){

														} else {

															bulkRegister.setUserCreationStatus(true);
															bulkRegister.setWalletCreationStatus(true);
															/*
															 * MMCards cardUser=
															 * cardRepository.getVirtualCardsByCardUser(usermob);
															 * if(cardUser==null){ WalletResponse
															 * phyCardCreate=corporateMatchMoveApi.assignVirtualCard(
															 * usermob);
															 * if(phyCardCreate.getCode().equalsIgnoreCase("S00")){
															 * bulkRegister.setCardCreationStatus(true);
															 * //bulkRegister.setPhyCardActivationStatus(true);
															 * //ResponseDTO
															 * activateResp=corporateMatchMoveApi.activateMMPhysicalCard
															 * (phyCardCreate.getCardId(),
															 * phyCardCreate.getActivationCode());
															 * if(activateResp.getCode().equalsIgnoreCase("S00")){
															 * bulkRegister.setPhyCardActivationStatus(true); }
															 * GroupBulkRegister gbr =
															 * groupBulkRegisterRepository.getUserRowByContactAndEmail(
															 * usermob.getUsername(), corporateFileCatalogue, false,
															 * usermob.getUserDetail().getEmail());
															 * gbr.setSuccessStatus(true);
															 * groupBulkRegisterRepository.save(gbr); }
															 * 
															 * 
															 * } else { GroupBulkRegister gbr =
															 * groupBulkRegisterRepository.getUserRowByContactAndEmail(
															 * usermob.getUsername(), corporateFileCatalogue, false,
															 * usermob.getUserDetail().getEmail());
															 * gbr.setSuccessStatus(true);
															 * groupBulkRegisterRepository.save(gbr); }
															 */
															MatchMoveCreateCardRequest cd = new MatchMoveCreateCardRequest();
															cd.setEmail(usermob.getUserDetail().getEmail());
															cd.setMobile(usermob.getUsername());
															cd.setUsername(usermob.getUsername());
															WalletResponse walletResp = matchMoveApi.fetchWalletMM(cd);
															if (walletResp != null
																	&& walletResp.getCode().equalsIgnoreCase("F00")) {
																GroupBulkRegister gbr = groupBulkRegisterRepository
																		.getUserRowByContact(usermob.getUsername(),
																				corporateFileCatalogue, false,
																				Status.Initiated);
																gbr.setSuccessStatus(false);
																gbr.setStatus(Status.Failed);
																gbr.setFailReason(walletResp.getMessage());
																groupBulkRegisterRepository.save(gbr);
															} else {
																UserKycRequest requ = new UserKycRequest();
																requ.setId_type(usermob.getUserDetail().getIdType());
																requ.setId_number(usermob.getUserDetail().getIdNo());
																requ.setUser(usermob);
																requ.setCountry("India");
																UserKycResponse resp = matchMoveApi.setIdDetails(requ);
																if (resp.getCode()
																		.equals(ResponseStatus.SUCCESS.getValue())) {

																	GroupBulkRegister gbr = groupBulkRegisterRepository
																			.getUserRowByContact(usermob.getUsername(),
																					corporateFileCatalogue, false,
																					Status.Initiated);
																	gbr.setFailReason("NA");
																	gbr.setStatus(Status.Success);
																	gbr.setSuccessStatus(true);
																	groupBulkRegisterRepository.save(gbr);
																} else {
																	GroupBulkRegister gbr = groupBulkRegisterRepository
																			.getUserRowByContact(usermob.getUsername(),
																					corporateFileCatalogue, false,
																					Status.Initiated);
																	gbr.setFailReason(resp.getMessage());
																	gbr.setStatus(Status.Failed);
																	gbr.setSuccessStatus(false);
																	groupBulkRegisterRepository.save(gbr);
																}
															}
														}
														bulkRegisterGroupRepository.save(bulkRegister);
													}
												}
											}
										}
									}
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				}
				corporateFileCatalogue.setSchedulerStatus(true);
				groupFileCatalogueRepository.save(corporateFileCatalogue);

			}
		}

	}

	public void bulkKycRegGroup() {
		logger.info("bulk kyc Group running>>>>>");
		List<GroupFileCatalogue> fileList = groupFileCatalogueRepository.getListOfBulkKyc();
		if (fileList != null && fileList.size() > 0) {
			for (GroupFileCatalogue fileCatalogue : fileList) {
				if (fileCatalogue.isFileRejectionStatus() == false) {
					if (fileCatalogue.isReviewStatus() == true) {
						if (fileCatalogue.getCategoryType().equalsIgnoreCase("BLKKYCUPLOAD")) {
							String absPath = fileCatalogue.getAbsPath();
							String filePath = StartUpUtil.CSV_FILE + absPath;
							try {
								JSONObject fileJsonObject = CSVReader.readCsvBulkKycUpload(filePath);
								if (fileJsonObject != null) {
									JSONArray jArray = fileJsonObject.getJSONArray("UserList");
									if (jArray != null) {
										for (int i = 0; i < jArray.length(); i++) {
											JSONObject jObject = jArray.getJSONObject(i);
											if (jObject != null) {
												MPQAccountType accountTypeKYC = mPQAccountTypeRepository
														.findByCode("KYC");
												MPQAccountType accountTypeNONKYC = mPQAccountTypeRepository
														.findByCode("NONKYC");

												String mobile = jObject.getString("mobile");
												String email = jObject.getString("email");
												String dob = jObject.getString("dob");
												String address1 = jObject.getString("address1");
												String address2 = jObject.getString("address2");
												String idType = jObject.getString("idType");
												String idNumber = jObject.getString("idNumber");
												String pinCode = jObject.getString("pinCode");
												String documentUrl = jObject.getString("documentUrl");
												String documentUrl1 = jObject.getString("documentUrl1");

												logger.info("documenturl:: " + documentUrl);
												logger.info("documenturl1:: " + documentUrl1);

												RegisterDTO reg = new RegisterDTO();
												reg.setContactNo(mobile);
												reg.setEmail(email);
												reg.setDateOfBirth(dob);
												if (address1.length() >= 35) {
													reg.setAddress(address1.replaceAll("[^a-zA-Z0-9]", "").trim()
															.substring(0, 35));
												} else {
													reg.setAddress(address1.replaceAll("[^a-zA-Z0-9]", "").trim());
												}
												if (address2.length() >= 35) {
													reg.setServices(address2.replaceAll("[^a-zA-Z0-9]", "").trim()
															.substring(0, 35));
												} else {
													reg.setServices(address2.replaceAll("[^a-zA-Z0-9]", "").trim());
												}
												reg.setIdType(idType);
												reg.setIdNo(idNumber);
												reg.setFileName(pinCode);
												reg.setAadharImagePath1(documentUrl);
												reg.setAadharImagePath2(documentUrl1);
												ResponseDTO res = registerValidation.addKycData(reg, fileCatalogue);
												if (res.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
													RegisterError regErr = registerValidation.validateKyc(reg,
															fileCatalogue, false);
													if (regErr.isValid()) {
														MUser user = userApi.findByUserName(mobile);
														if (user != null) {
															MKycDetail kycDetail = mKycRepository.findByUser(user);
															if (kycDetail == null) {
																kycDetail = new MKycDetail();
																if (address1.length() >= 35) {
																	kycDetail.setAddress1(
																			address1.replaceAll("[^a-zA-Z0-9]", "")
																					.trim().substring(0, 35));
																} else {
																	kycDetail.setAddress1(address1
																			.replaceAll("[^a-zA-Z0-9]", "").trim());
																}
																if (address2.length() >= 35) {
																	kycDetail.setAddress2(
																			address2.replaceAll("[^a-zA-Z0-9]", "")
																					.trim().substring(0, 35));
																} else {
																	kycDetail.setAddress2(address2
																			.replaceAll("[^a-zA-Z0-9]", "").trim());
																}
																kycDetail.setIdImage(documentUrl);
																kycDetail.setIdImage1(documentUrl1);
																kycDetail.setIdType(idType);
																kycDetail.setIdNumber(idNumber);
																kycDetail.setPinCode(pinCode);
																kycDetail.setUser(user);
																kycDetail.setUserType(UserType.User);
																AddressResponseDTO addResp = CommonUtil
																		.getAddress(pinCode);
																if (addResp.getCode().equalsIgnoreCase("S00")) {
																	kycDetail.setCity(addResp.getCity());
																	kycDetail.setState(addResp.getState());
																	kycDetail.setCountry(addResp.getCountry());
																	UserKycResponse userKyc = matchMoveApi
																			.kycUserMMCorporate(kycDetail);
																	if (userKyc.getCode().equalsIgnoreCase("S00")) {
																		UserKycResponse userIdDetails = matchMoveApi
																				.setIdDetailsCorporate(kycDetail);
																		if (userIdDetails.getCode()
																				.equalsIgnoreCase("S00")) {
																			UserKycResponse image1Upload = matchMoveApi
																					.setImagesForKyc1(kycDetail);
																			if (image1Upload.getCode()
																					.equalsIgnoreCase("S00")) {
																				UserKycResponse imageUpload2 = matchMoveApi
																						.setImagesForKyc2(kycDetail);
																				if (imageUpload2.getCode()
																						.equalsIgnoreCase("S00")) {
																					UserKycResponse approvalResp = matchMoveApi
																							.tempConfirmKycCorporate(
																									kycDetail);
																					if (approvalResp.getCode()
																							.equalsIgnoreCase("S00")) {
																						MPQAccountDetails accDetails = user
																								.getAccountDetail();
																						if (accDetails != null) {
																							MPQAccountType accountType = mPQAccountTypeRepository
																									.findByCode("KYC");
																							accDetails.setAccountType(
																									accountType);
																							mPQAccountDetailRepository
																									.save(accDetails);
																							kycDetail.setAccountType(
																									accountType);
																							kycDetail
																									.setRejectionStatus(
																											false);
																							mKycRepository
																									.save(kycDetail);

																							GroupBulkKyc gbr = groupBulkKycRepository
																									.getUserRowByContact(
																											mobile,
																											fileCatalogue,
																											false,
																											Status.Initiated);
																							if (gbr != null) {
																								gbr.setFailReason("NA");
																								gbr.setStatus(
																										Status.Success);
																								gbr.setSuccessStatus(
																										true);
																								groupBulkKycRepository
																										.save(gbr);
																							}
																						} else {
																							kycDetail.setAccountType(
																									accountTypeNONKYC);
																							kycDetail
																									.setRejectionStatus(
																											true);
																							kycDetail
																									.setRejectionReason(
																											"No Account found");
																							mKycRepository
																									.save(kycDetail);

																							GroupBulkKyc gbr = groupBulkKycRepository
																									.getUserRowByContact(
																											mobile,
																											fileCatalogue,
																											false,
																											Status.Initiated);
																							if (gbr != null) {
																								gbr.setFailReason(
																										"No Account found");
																								gbr.setStatus(
																										Status.Failed);
																								gbr.setSuccessStatus(
																										false);
																								groupBulkKycRepository
																										.save(gbr);
																							}
																						}
																					} else {
																						kycDetail.setAccountType(
																								accountTypeNONKYC);
																						kycDetail.setRejectionStatus(
																								true);
																						kycDetail.setRejectionReason(
																								approvalResp
																										.getMessage());
																						mKycRepository.save(kycDetail);

																						GroupBulkKyc gbr = groupBulkKycRepository
																								.getUserRowByContact(
																										mobile,
																										fileCatalogue,
																										false,
																										Status.Initiated);
																						if (gbr != null) {
																							gbr.setFailReason(
																									approvalResp
																											.getMessage());
																							gbr.setStatus(
																									Status.Failed);
																							gbr.setSuccessStatus(false);
																							groupBulkKycRepository
																									.save(gbr);
																						}
																					}
																				} else {
																					kycDetail.setAccountType(
																							accountTypeNONKYC);
																					kycDetail.setRejectionStatus(true);
																					kycDetail.setRejectionReason(
																							imageUpload2.getMessage());
																					mKycRepository.save(kycDetail);

																					GroupBulkKyc gbr = groupBulkKycRepository
																							.getUserRowByContact(mobile,
																									fileCatalogue,
																									false,
																									Status.Initiated);
																					if (gbr != null) {
																						gbr.setFailReason(imageUpload2
																								.getMessage());
																						gbr.setStatus(Status.Failed);
																						gbr.setSuccessStatus(false);
																						groupBulkKycRepository
																								.save(gbr);
																					}
																				}
																			} else {
																				kycDetail.setAccountType(
																						accountTypeNONKYC);
																				kycDetail.setRejectionStatus(true);
																				kycDetail.setRejectionReason(
																						image1Upload.getMessage());
																				mKycRepository.save(kycDetail);

																				GroupBulkKyc gbr = groupBulkKycRepository
																						.getUserRowByContact(mobile,
																								fileCatalogue, false,
																								Status.Initiated);
																				if (gbr != null) {
																					gbr.setFailReason(
																							image1Upload.getMessage());
																					gbr.setStatus(Status.Failed);
																					gbr.setSuccessStatus(false);
																					groupBulkKycRepository.save(gbr);
																				}
																			}
																		} else {
																			kycDetail.setAccountType(accountTypeNONKYC);
																			kycDetail.setRejectionStatus(true);
																			kycDetail.setRejectionReason(
																					userIdDetails.getMessage());
																			mKycRepository.save(kycDetail);

																			GroupBulkKyc gbr = groupBulkKycRepository
																					.getUserRowByContact(mobile,
																							fileCatalogue, false,
																							Status.Initiated);
																			if (gbr != null) {
																				gbr.setFailReason(
																						userIdDetails.getMessage());
																				gbr.setStatus(Status.Failed);
																				gbr.setSuccessStatus(false);
																				groupBulkKycRepository.save(gbr);
																			}

																		}
																	} else {
																		kycDetail.setAccountType(accountTypeNONKYC);
																		kycDetail.setRejectionStatus(true);
																		kycDetail.setRejectionReason(
																				userKyc.getMessage());
																		mKycRepository.save(kycDetail);

																		GroupBulkKyc gbr = groupBulkKycRepository
																				.getUserRowByContact(mobile,
																						fileCatalogue, false,
																						Status.Initiated);
																		if (gbr != null) {
																			gbr.setFailReason(userKyc.getMessage());
																			gbr.setStatus(Status.Failed);
																			gbr.setSuccessStatus(false);
																			groupBulkKycRepository.save(gbr);
																		}
																	}
																} else {
																	kycDetail.setCity("Bangalore");
																	kycDetail.setState("Karnataka");
																	kycDetail.setCountry("India");

																	UserKycResponse userKyc = matchMoveApi
																			.kycUserMMCorporate(kycDetail);
																	if (userKyc.getCode().equalsIgnoreCase("S00")) {
																		UserKycResponse userIdDetails = matchMoveApi
																				.setIdDetailsCorporate(kycDetail);
																		if (userIdDetails.getCode()
																				.equalsIgnoreCase("S00")) {
																			UserKycResponse image1Upload = matchMoveApi
																					.setImagesForKyc1(kycDetail);
																			if (image1Upload.getCode()
																					.equalsIgnoreCase("S00")) {
																				UserKycResponse imageUpload2 = matchMoveApi
																						.setImagesForKyc2(kycDetail);
																				if (imageUpload2.getCode()
																						.equalsIgnoreCase("S00")) {
																					UserKycResponse approvalResp = matchMoveApi
																							.tempConfirmKycCorporate(
																									kycDetail);
																					if (approvalResp.getCode()
																							.equalsIgnoreCase("S00")) {
																						MPQAccountDetails accDetails = user
																								.getAccountDetail();
																						if (accDetails != null) {
																							MPQAccountType accountType = mPQAccountTypeRepository
																									.findByCode("KYC");
																							accDetails.setAccountType(
																									accountType);
																							mPQAccountDetailRepository
																									.save(accDetails);
																							kycDetail.setAccountType(
																									accountType);
																							kycDetail
																									.setRejectionStatus(
																											false);
																							mKycRepository
																									.save(kycDetail);

																							GroupBulkKyc gbr = groupBulkKycRepository
																									.getUserRowByContact(
																											mobile,
																											fileCatalogue,
																											false,
																											Status.Initiated);
																							if (gbr != null) {
																								gbr.setFailReason("NA");
																								gbr.setStatus(
																										Status.Success);
																								gbr.setSuccessStatus(
																										true);
																								groupBulkKycRepository
																										.save(gbr);
																							}
																						} else {
																							kycDetail.setAccountType(
																									accountTypeNONKYC);
																							kycDetail
																									.setRejectionStatus(
																											true);
																							kycDetail
																									.setRejectionReason(
																											"No Account found");
																							mKycRepository
																									.save(kycDetail);

																							GroupBulkKyc gbr = groupBulkKycRepository
																									.getUserRowByContact(
																											mobile,
																											fileCatalogue,
																											false,
																											Status.Initiated);
																							if (gbr != null) {
																								gbr.setFailReason(
																										"No Account found");
																								gbr.setStatus(
																										Status.Failed);
																								gbr.setSuccessStatus(
																										false);
																								groupBulkKycRepository
																										.save(gbr);
																							}
																						}
																					} else {
																						kycDetail.setAccountType(
																								accountTypeNONKYC);
																						kycDetail.setRejectionStatus(
																								true);
																						kycDetail.setRejectionReason(
																								approvalResp
																										.getMessage());
																						mKycRepository.save(kycDetail);

																						GroupBulkKyc gbr = groupBulkKycRepository
																								.getUserRowByContact(
																										mobile,
																										fileCatalogue,
																										false,
																										Status.Initiated);
																						if (gbr != null) {
																							gbr.setFailReason(
																									approvalResp
																											.getMessage());
																							gbr.setStatus(
																									Status.Failed);
																							gbr.setSuccessStatus(false);
																							groupBulkKycRepository
																									.save(gbr);
																						}
																					}
																				} else {
																					kycDetail.setAccountType(
																							accountTypeNONKYC);
																					kycDetail.setRejectionStatus(true);
																					kycDetail.setRejectionReason(
																							imageUpload2.getMessage());
																					mKycRepository.save(kycDetail);

																					GroupBulkKyc gbr = groupBulkKycRepository
																							.getUserRowByContact(mobile,
																									fileCatalogue,
																									false,
																									Status.Initiated);
																					if (gbr != null) {
																						gbr.setFailReason(imageUpload2
																								.getMessage());
																						gbr.setStatus(Status.Failed);
																						gbr.setSuccessStatus(false);
																						groupBulkKycRepository
																								.save(gbr);
																					}
																				}
																			} else {
																				kycDetail.setAccountType(
																						accountTypeNONKYC);
																				kycDetail.setRejectionStatus(true);
																				kycDetail.setRejectionReason(
																						image1Upload.getMessage());
																				mKycRepository.save(kycDetail);

																				GroupBulkKyc gbr = groupBulkKycRepository
																						.getUserRowByContact(mobile,
																								fileCatalogue, false,
																								Status.Initiated);
																				if (gbr != null) {
																					gbr.setFailReason(
																							image1Upload.getMessage());
																					gbr.setStatus(Status.Failed);
																					gbr.setSuccessStatus(false);
																					groupBulkKycRepository.save(gbr);
																				}
																			}
																		} else {
																			kycDetail.setAccountType(accountTypeNONKYC);
																			kycDetail.setRejectionStatus(true);
																			kycDetail.setRejectionReason(
																					userIdDetails.getMessage());
																			mKycRepository.save(kycDetail);

																			GroupBulkKyc gbr = groupBulkKycRepository
																					.getUserRowByContact(mobile,
																							fileCatalogue, false,
																							Status.Initiated);
																			if (gbr != null) {
																				gbr.setFailReason(
																						userIdDetails.getMessage());
																				gbr.setStatus(Status.Failed);
																				gbr.setSuccessStatus(false);
																				groupBulkKycRepository.save(gbr);
																			}

																		}
																	} else {
																		kycDetail.setAccountType(accountTypeNONKYC);
																		kycDetail.setRejectionStatus(true);
																		kycDetail.setRejectionReason(
																				userKyc.getMessage());
																		mKycRepository.save(kycDetail);

																		GroupBulkKyc gbr = groupBulkKycRepository
																				.getUserRowByContact(mobile,
																						fileCatalogue, false,
																						Status.Initiated);
																		if (gbr != null) {
																			gbr.setFailReason(userKyc.getMessage());
																			gbr.setStatus(Status.Failed);
																			gbr.setSuccessStatus(false);
																			groupBulkKycRepository.save(gbr);
																		}
																	}
																}
															} else {
																MPQAccountDetails accountDetails = kycDetail.getUser()
																		.getAccountDetail();
																if (accountDetails != null) {
																	MPQAccountType accountType = accountDetails
																			.getAccountType();
																	if (accountType.getCode()
																			.equalsIgnoreCase("NONKYC")) {
																		mKycRepository.delete(kycDetail);
																		MKycDetail kycDetails = new MKycDetail();
																		kycDetails.setAddress1(address1);
																		kycDetails.setAddress2(address2);
																		kycDetails.setIdImage(documentUrl);
																		kycDetails.setIdImage1(documentUrl);
																		kycDetails.setIdType(idType);
																		kycDetails.setIdNumber(idNumber);
																		kycDetails.setPinCode(pinCode);
																		kycDetail.setUser(user);
																		kycDetail.setUserType(UserType.User);
																		AddressResponseDTO addResp = CommonUtil
																				.getAddress(pinCode);
																		if (addResp.getCode().equalsIgnoreCase("S00")) {
																			kycDetails.setCity(addResp.getCity());
																			kycDetails.setState(addResp.getState());
																			kycDetails.setCountry(addResp.getCountry());
																			UserKycResponse userKyc = matchMoveApi
																					.kycUserMMCorporate(kycDetail);
																			if (userKyc.getCode()
																					.equalsIgnoreCase("S00")) {
																				UserKycResponse userIdDetails = matchMoveApi
																						.setIdDetailsCorporate(
																								kycDetail);
																				if (userIdDetails.getCode()
																						.equalsIgnoreCase("S00")) {
																					UserKycResponse image1Upload = matchMoveApi
																							.setImagesForKyc1(
																									kycDetail);
																					if (image1Upload.getCode()
																							.equalsIgnoreCase("S00")) {
																						UserKycResponse imageUpload2 = matchMoveApi
																								.setImagesForKyc2(
																										kycDetail);
																						if (imageUpload2.getCode()
																								.equalsIgnoreCase(
																										"S00")) {
																							UserKycResponse approvalResp = matchMoveApi
																									.tempConfirmKycCorporate(
																											kycDetail);
																							if (approvalResp.getCode()
																									.equalsIgnoreCase(
																											"S00")) {
																								MPQAccountDetails accDetails = user
																										.getAccountDetail();
																								if (accDetails != null) {
																									// MPQAccountType
																									// accountType=mPQAccountTypeRepository.findByCode("KYC");
																									accDetails
																											.setAccountType(
																													accountTypeKYC);
																									mPQAccountDetailRepository
																											.save(accDetails);
																									kycDetail
																											.setAccountType(
																													accountTypeKYC);
																									kycDetail
																											.setRejectionStatus(
																													false);
																									mKycRepository.save(
																											kycDetail);

																									GroupBulkKyc gbr = groupBulkKycRepository
																											.getUserRowByContact(
																													mobile,
																													fileCatalogue,
																													false,
																													Status.Initiated);
																									if (gbr != null) {
																										gbr.setFailReason(
																												"NA");
																										gbr.setStatus(
																												Status.Success);
																										gbr.setSuccessStatus(
																												true);
																										groupBulkKycRepository
																												.save(gbr);
																									}
																								} else {
																									kycDetail
																											.setAccountType(
																													accountTypeNONKYC);
																									kycDetail
																											.setRejectionStatus(
																													true);
																									kycDetail
																											.setRejectionReason(
																													"No Account found");
																									mKycRepository.save(
																											kycDetail);

																									GroupBulkKyc gbr = groupBulkKycRepository
																											.getUserRowByContact(
																													mobile,
																													fileCatalogue,
																													false,
																													Status.Initiated);
																									if (gbr != null) {
																										gbr.setFailReason(
																												"No Account found");
																										gbr.setStatus(
																												Status.Failed);
																										gbr.setSuccessStatus(
																												false);
																										groupBulkKycRepository
																												.save(gbr);
																									}
																								}
																							} else {
																								kycDetail
																										.setAccountType(
																												accountTypeNONKYC);
																								kycDetail
																										.setRejectionStatus(
																												true);
																								kycDetail
																										.setRejectionReason(
																												approvalResp
																														.getMessage());
																								mKycRepository.save(
																										kycDetail);

																								GroupBulkKyc gbr = groupBulkKycRepository
																										.getUserRowByContact(
																												mobile,
																												fileCatalogue,
																												false,
																												Status.Initiated);
																								if (gbr != null) {
																									gbr.setFailReason(
																											approvalResp
																													.getMessage());
																									gbr.setStatus(
																											Status.Failed);
																									gbr.setSuccessStatus(
																											false);
																									groupBulkKycRepository
																											.save(gbr);
																								}
																							}
																						} else {
																							kycDetail.setAccountType(
																									accountTypeNONKYC);
																							kycDetail
																									.setRejectionStatus(
																											true);
																							kycDetail
																									.setRejectionReason(
																											imageUpload2
																													.getMessage());
																							mKycRepository
																									.save(kycDetail);

																							GroupBulkKyc gbr = groupBulkKycRepository
																									.getUserRowByContact(
																											mobile,
																											fileCatalogue,
																											false,
																											Status.Initiated);
																							if (gbr != null) {
																								gbr.setFailReason(
																										imageUpload2
																												.getMessage());
																								gbr.setStatus(
																										Status.Failed);
																								gbr.setSuccessStatus(
																										false);
																								groupBulkKycRepository
																										.save(gbr);
																							}
																						}
																					} else {
																						kycDetail.setAccountType(
																								accountTypeNONKYC);
																						kycDetail.setRejectionStatus(
																								true);
																						kycDetail.setRejectionReason(
																								image1Upload
																										.getMessage());
																						mKycRepository.save(kycDetail);

																						GroupBulkKyc gbr = groupBulkKycRepository
																								.getUserRowByContact(
																										mobile,
																										fileCatalogue,
																										false,
																										Status.Initiated);
																						if (gbr != null) {
																							gbr.setFailReason(
																									image1Upload
																											.getMessage());
																							gbr.setStatus(
																									Status.Failed);
																							gbr.setSuccessStatus(false);
																							groupBulkKycRepository
																									.save(gbr);
																						}
																					}
																				} else {
																					kycDetail.setAccountType(
																							accountTypeNONKYC);
																					kycDetail.setRejectionStatus(true);
																					kycDetail.setRejectionReason(
																							userIdDetails.getMessage());
																					mKycRepository.save(kycDetail);

																					GroupBulkKyc gbr = groupBulkKycRepository
																							.getUserRowByContact(mobile,
																									fileCatalogue,
																									false,
																									Status.Initiated);
																					if (gbr != null) {
																						gbr.setFailReason(userIdDetails
																								.getMessage());
																						gbr.setStatus(Status.Failed);
																						gbr.setSuccessStatus(false);
																						groupBulkKycRepository
																								.save(gbr);
																					}

																				}
																			} else {
																				kycDetail.setAccountType(
																						accountTypeNONKYC);
																				kycDetail.setRejectionStatus(true);
																				kycDetail.setRejectionReason(
																						userKyc.getMessage());
																				mKycRepository.save(kycDetail);

																				GroupBulkKyc gbr = groupBulkKycRepository
																						.getUserRowByContact(mobile,
																								fileCatalogue, false,
																								Status.Initiated);
																				if (gbr != null) {
																					gbr.setFailReason(
																							userKyc.getMessage());
																					gbr.setStatus(Status.Failed);
																					gbr.setSuccessStatus(false);
																					groupBulkKycRepository.save(gbr);
																				}
																			}
																		} else {
																			kycDetail.setCity("Bangalore");
																			kycDetail.setState("Karnataka");
																			kycDetail.setCountry("India");

																			UserKycResponse userKyc = matchMoveApi
																					.kycUserMMCorporate(kycDetail);
																			if (userKyc.getCode()
																					.equalsIgnoreCase("S00")) {
																				UserKycResponse userIdDetails = matchMoveApi
																						.setIdDetailsCorporate(
																								kycDetail);
																				if (userIdDetails.getCode()
																						.equalsIgnoreCase("S00")) {
																					UserKycResponse image1Upload = matchMoveApi
																							.setImagesForKyc1(
																									kycDetail);
																					if (image1Upload.getCode()
																							.equalsIgnoreCase("S00")) {
																						UserKycResponse imageUpload2 = matchMoveApi
																								.setImagesForKyc2(
																										kycDetail);
																						if (imageUpload2.getCode()
																								.equalsIgnoreCase(
																										"S00")) {
																							UserKycResponse approvalResp = matchMoveApi
																									.tempConfirmKycCorporate(
																											kycDetail);
																							if (approvalResp.getCode()
																									.equalsIgnoreCase(
																											"S00")) {
																								MPQAccountDetails accDetails = user
																										.getAccountDetail();
																								if (accDetails != null) {
																									MPQAccountType accType = mPQAccountTypeRepository
																											.findByCode(
																													"KYC");
																									accDetails
																											.setAccountType(
																													accType);
																									mPQAccountDetailRepository
																											.save(accDetails);
																									kycDetail
																											.setAccountType(
																													accType);
																									kycDetail
																											.setRejectionStatus(
																													false);
																									mKycRepository.save(
																											kycDetail);

																									GroupBulkKyc gbr = groupBulkKycRepository
																											.getUserRowByContact(
																													mobile,
																													fileCatalogue,
																													false,
																													Status.Initiated);
																									if (gbr != null) {
																										gbr.setFailReason(
																												"NA");
																										gbr.setStatus(
																												Status.Success);
																										gbr.setSuccessStatus(
																												true);
																										groupBulkKycRepository
																												.save(gbr);
																									}
																								} else {
																									kycDetail
																											.setAccountType(
																													accountTypeNONKYC);
																									kycDetail
																											.setRejectionStatus(
																													true);
																									kycDetail
																											.setRejectionReason(
																													"No Account found");
																									mKycRepository.save(
																											kycDetail);

																									GroupBulkKyc gbr = groupBulkKycRepository
																											.getUserRowByContact(
																													mobile,
																													fileCatalogue,
																													false,
																													Status.Initiated);
																									if (gbr != null) {
																										gbr.setFailReason(
																												"No Account found");
																										gbr.setStatus(
																												Status.Failed);
																										gbr.setSuccessStatus(
																												false);
																										groupBulkKycRepository
																												.save(gbr);
																									}
																								}
																							} else {
																								kycDetail
																										.setAccountType(
																												accountTypeNONKYC);
																								kycDetail
																										.setRejectionStatus(
																												true);
																								kycDetail
																										.setRejectionReason(
																												approvalResp
																														.getMessage());
																								mKycRepository.save(
																										kycDetail);

																								GroupBulkKyc gbr = groupBulkKycRepository
																										.getUserRowByContact(
																												mobile,
																												fileCatalogue,
																												false,
																												Status.Initiated);
																								if (gbr != null) {
																									gbr.setFailReason(
																											approvalResp
																													.getMessage());
																									gbr.setStatus(
																											Status.Failed);
																									gbr.setSuccessStatus(
																											false);
																									groupBulkKycRepository
																											.save(gbr);
																								}
																							}
																						} else {
																							kycDetail.setAccountType(
																									accountTypeNONKYC);
																							kycDetail
																									.setRejectionStatus(
																											true);
																							kycDetail
																									.setRejectionReason(
																											imageUpload2
																													.getMessage());
																							mKycRepository
																									.save(kycDetail);

																							GroupBulkKyc gbr = groupBulkKycRepository
																									.getUserRowByContact(
																											mobile,
																											fileCatalogue,
																											false,
																											Status.Initiated);
																							if (gbr != null) {
																								gbr.setFailReason(
																										imageUpload2
																												.getMessage());
																								gbr.setStatus(
																										Status.Failed);
																								gbr.setSuccessStatus(
																										false);
																								groupBulkKycRepository
																										.save(gbr);
																							}
																						}
																					} else {
																						kycDetail.setAccountType(
																								accountTypeNONKYC);
																						kycDetail.setRejectionStatus(
																								true);
																						kycDetail.setRejectionReason(
																								image1Upload
																										.getMessage());
																						mKycRepository.save(kycDetail);

																						GroupBulkKyc gbr = groupBulkKycRepository
																								.getUserRowByContact(
																										mobile,
																										fileCatalogue,
																										false,
																										Status.Initiated);
																						if (gbr != null) {
																							gbr.setFailReason(
																									image1Upload
																											.getMessage());
																							gbr.setStatus(
																									Status.Failed);
																							gbr.setSuccessStatus(false);
																							groupBulkKycRepository
																									.save(gbr);
																						}
																					}
																				} else {
																					kycDetail.setAccountType(
																							accountTypeNONKYC);
																					kycDetail.setRejectionStatus(true);
																					kycDetail.setRejectionReason(
																							userIdDetails.getMessage());
																					mKycRepository.save(kycDetail);

																					GroupBulkKyc gbr = groupBulkKycRepository
																							.getUserRowByContact(mobile,
																									fileCatalogue,
																									false,
																									Status.Initiated);
																					if (gbr != null) {
																						gbr.setFailReason(userIdDetails
																								.getMessage());
																						gbr.setStatus(Status.Failed);
																						gbr.setSuccessStatus(false);
																						groupBulkKycRepository
																								.save(gbr);
																					}

																				}
																			} else {
																				kycDetail.setAccountType(
																						accountTypeNONKYC);
																				kycDetail.setRejectionStatus(true);
																				kycDetail.setRejectionReason(
																						userKyc.getMessage());
																				mKycRepository.save(kycDetail);

																				GroupBulkKyc gbr = groupBulkKycRepository
																						.getUserRowByContact(mobile,
																								fileCatalogue, false,
																								Status.Initiated);
																				if (gbr != null) {
																					gbr.setFailReason(
																							userKyc.getMessage());
																					gbr.setStatus(Status.Failed);
																					gbr.setSuccessStatus(false);
																					groupBulkKycRepository.save(gbr);
																				}
																			}
																		}
																	} else {
																		kycDetail.setAccountType(accountType);
																		kycDetail.setRejectionStatus(true);
																		kycDetail
																				.setRejectionReason("Already KYC user");
																		mKycRepository.save(kycDetail);

																		GroupBulkKyc gbr = groupBulkKycRepository
																				.getUserRowByContact(mobile,
																						fileCatalogue, false,
																						Status.Initiated);
																		if (gbr != null) {
																			gbr.setFailReason("Already KYC user");
																			gbr.setStatus(Status.Failed);
																			gbr.setSuccessStatus(false);
																			groupBulkKycRepository.save(gbr);
																		}
																	}
																} else {
																	kycDetail.setAccountType(accountTypeNONKYC);
																	kycDetail.setRejectionStatus(true);
																	kycDetail.setRejectionReason(
																			"Account Details does not exist");
																	mKycRepository.save(kycDetail);

																	GroupBulkKyc gbr = groupBulkKycRepository
																			.getUserRowByContact(mobile, fileCatalogue,
																					false, Status.Initiated);
																	if (gbr != null) {
																		gbr.setFailReason(
																				"Account Details does not exist");
																		gbr.setStatus(Status.Failed);
																		gbr.setSuccessStatus(false);
																		groupBulkKycRepository.save(gbr);
																	}
																}
															}
														} else {
															GroupBulkKyc gbr = groupBulkKycRepository
																	.getUserRowByContact(mobile, fileCatalogue, false,
																			Status.Initiated);
															if (gbr != null) {
																gbr.setFailReason("user not found");
																gbr.setStatus(Status.Failed);
																gbr.setSuccessStatus(false);
																groupBulkKycRepository.save(gbr);
															}
														}
													}
												}
											}
										}
									}
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				}
				fileCatalogue.setSchedulerStatus(true);
				groupFileCatalogueRepository.save(fileCatalogue);
			}
		}

	}

	public void getBulkLoadMoney() {
		System.err.println("bulk fund transfer running>>>>");
		try {
			List<CorporateFileCatalogue> fileList = corporateFileCatalogueRepository.getListOfBulkFundTransfer();
			if (fileList != null && fileList.size() > 0) {
				for (CorporateFileCatalogue corporateFileCatalogue : fileList) {
					System.err.println(corporateFileCatalogue);
					if (corporateFileCatalogue.isFileRejectionStatus() == false) {
						if (corporateFileCatalogue.isReviewStatus() == true
								&& corporateFileCatalogue.isTransactionStatus() == false) {
							if (corporateFileCatalogue.getCategoryType().equalsIgnoreCase("BLKCARDLOAD")) {
								String absPath = corporateFileCatalogue.getAbsPath();
								String filePath = StartUpUtil.CSV_FILE + absPath;
								System.out.println("filePath: " + filePath);
								JSONObject fileJsonObject = CSVReader.readCsvBulkUploadLoadBalance(filePath);
								if (fileJsonObject != null) {
									System.out.println("fileJsonObject: " + fileJsonObject.toString());
									JSONArray filejArray = fileJsonObject.getJSONArray("LoadMoneyList");
									if (filejArray != null) {
										System.out.println("filejArray: " + filejArray.toString());
										for (int i = 0; i < filejArray.length(); i++) {
											JSONObject loadObject = filejArray.getJSONObject(i);
											if (loadObject != null) {
												System.out.println("loadObject: " + loadObject.toString());
												String mobile = loadObject.getString("mobile");
												String amount = loadObject.getString("amount");
												System.out.println("mobile: " + mobile + "  amount: " + amount);
												MUser agent = corporateFileCatalogue.getCorporate();
												System.out.println("Balance: " + agent.getAccountDetail().getBalance());
												if (agent.getAccountDetail().getBalance() > Long.valueOf(amount)) {
													MUser cardUser = userApi.findByUserName(mobile.trim());
													System.out.println("cardUser: " + cardUser.getUsername());
													if (cardUser != null) {
														MMCards Physical = null;
														WalletResponse cardTransferResposne = null;
														ResponseDTO resp = new ResponseDTO();
														MService service = mServiceRepository
																.findServiceByCode("BRCSL");
														TransactionError transactionError = loadMoneyValidation
																.validateLoadCardTransaction(amount, mobile.trim(),
																		service);
														System.out.println(transactionError.toString());
														if (transactionError.isValid()) {
															RequestDTO cardRequest = new RequestDTO();
															cardRequest.setAmount(amount);
															cardRequest.setContactNo(mobile);
															BulkLoadMoney card = new BulkLoadMoney();

															resp = transactionApi.initiateLoadCardTransactionCorporate(
																	cardRequest, agent, service);
															if (resp.getCode().equalsIgnoreCase("S00")) {
																System.out
																		.println("WalletResponse: " + resp.toString());
																WalletResponse walletRespon = matchMoveApi
																		.initiateLoadFundsToMMWalletCorp(cardUser,
																				cardRequest.getAmount());
																if (walletRespon.getCode().equalsIgnoreCase("S00")) {
																	System.out.println(
																			"Physical: " + walletRespon.toString());
																	Physical = cardRepository
																			.getPhysicalCardByUser(cardUser);
																	if (Physical != null && Physical.getStatus()
																			.equalsIgnoreCase("Active")) {
																		System.out
																				.println("Physical card: " + Physical);
																		cardTransferResposne = matchMoveApi
																				.transferFundsToMMCard(cardUser,
																						cardRequest.getAmount(),
																						Physical.getCardId());
																		if (cardTransferResposne.getCode()
																				.equalsIgnoreCase("S00")) {
																			System.out.println("cardTransferResposne: "
																					+ cardTransferResposne);
																			card.setAmount(amount);
																			card.setCardLoadError(false);
																			card.setCorporate(agent);
																			card.setDateOfTransaction(resp.getDate());
																			card.setEmail(cardUser.getUserDetail()
																					.getEmail());
																			card.setMobile(cardUser.getUsername());
																			if (corporateFileCatalogue
																					.getPartnerDetails() != null) {
																				card.setPartnerDetails(
																						corporateFileCatalogue
																								.getPartnerDetails());
																			}
																			card.setSchedulerStatus(true);
																			card.setTransactionStatus(true);
																			transactionApi.updateLoadCardByCorporate(
																					resp.getTxnId(),
																					Status.Success.getValue());
																			// MTransaction
																			// succTrx=transactionApi.getTransactionByRefNo(resp.getTxnId());
																			bulkLoadMoneyRepository.save(card);
																		} else {
																			transactionApi.updateLoadCardByCorporate(
																					resp.getTxnId(),
																					Status.Failed.getValue());
																			card.setCardLoadError(true);
																			bulkLoadMoneyRepository.save(card);
																		}
																	} else {
																		transactionApi.updateLoadCardByCorporate(
																				resp.getTxnId(),
																				Status.Failed.getValue());
																		card.setCardLoadError(true);
																		bulkLoadMoneyRepository.save(card);
																	}
																} else {
																	transactionApi.updateLoadCardByCorporate(
																			resp.getTxnId(), Status.Failed.getValue());
																	card.setCardLoadError(true);
																	bulkLoadMoneyRepository.save(card);

																}
															} else {
																transactionApi.updateLoadCardByCorporate(
																		resp.getTxnId(), Status.Failed.getValue());
																card.setCardLoadError(true);
																bulkLoadMoneyRepository.save(card);

															}
														} else {
															System.out.println(transactionError.toString());
														}
													}
												}
											}
										}

									}
								}
							}
						}
					}
					corporateFileCatalogue.setSchedulerStatus(true);
					corporateFileCatalogueRepository.save(corporateFileCatalogue);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// To be done
	public void checkIpayStatusnew() {
		List<MTransaction> checkStatus = transactionRepository.getAllSuspicious();
		for (MTransaction mTransaction : checkStatus) {
			MdexTransactionRequestDTO req = new MdexTransactionRequestDTO();
			String trx = mTransaction.getTransactionRefNo();
			String[] splittedtrx = trx.split("D");
			req.setTransactionId(splittedtrx[0]);
			MdexTransactionResponseDTO statusresp = mdexApi.checkStatus(req);
			if (statusresp.getCode().equalsIgnoreCase("S00")) {
				mTransaction.setSuspicious(false);
				mTransaction.setMdexTransactionStatus("Success");
				transactionRepository.save(mTransaction);
			} else if (statusresp.getCode().equalsIgnoreCase("F00")) {
				MUser user = userRespository.findByAccountDetails(mTransaction.getAccount());
				WalletResponse walletResp = matchMoveApi.reversalToWalletForRechargeFailure(user,
						String.valueOf(mTransaction.getAmount()), mTransaction.getService().getCode(),
						mTransaction.getTransactionRefNo(), mTransaction.getDescription());
				if (walletResp != null) {
					String code = walletResp.getCode();
					if (code.equalsIgnoreCase("S00")) {
						mTransaction.setStatus(Status.Refunded);
						String authRefNo = walletResp.getAuthRetrivalNo();
						mTransaction.setRetrivalReferenceNo(authRefNo);
						mTransaction.setMdexTransactionStatus("Failed");
						mTransaction.setSuspicious(false);
						transactionRepository.save(mTransaction);
					} else {
						mTransaction.setStatus(Status.Refund_Failed);
						mTransaction.setSuspicious(false);
						mTransaction.setMdexTransactionStatus("Failed");
						transactionRepository.save(mTransaction);
					}
				} else {
					mTransaction.setStatus(Status.Refund_Failed);
					mTransaction.setSuspicious(false);
					mTransaction.setMdexTransactionStatus("Failed");
					transactionRepository.save(mTransaction);
				}
			}
		}

	}

	public void sendNoti() {
		try {
			List<FCMDetails> fcmDetailList = fcmDetailsRepository.getFcmDetails();
			if (fcmDetailList != null) {
				for (FCMDetails fcmDetails : fcmDetailList) {
					if (fcmDetails.getStatus().equalsIgnoreCase("Active")) {
						fcmSenderApi.sendNotification(fcmDetails);
						fcmDetails.setStatus("Inactive");
						fcmDetails.setCronStatus(true);
						fcmDetailsRepository.save(fcmDetails);

					}
				}

			}
		} catch (Exception e) {
			System.out.println("in exception");
			e.printStackTrace();
		}
	}

	public void sendgroupnotification() {
		try {
			System.err.println("in group sms cron");
			List<GroupSms> groupSmsSingle = groupSmsRepository.getAllSingleNumbers(false, true,
					Status.Inactive.getValue());
			if (groupSmsSingle != null) {
				for (GroupSms gs : groupSmsSingle) {
					if (gs.getStatus().equals(Status.Inactive.getValue())) {
						userApi.sendSingleGroupSMS(gs.getMobile(), gs.getText());
						gs.setCronStatus(true);
						gs.setStatus(Status.Active.getValue());
						groupSmsRepository.save(gs);
					}
				}
			}
			List<GroupSms> groupSms = groupSmsRepository.getAllGroupNumbers(false, false, Status.Inactive.getValue());
			if (groupSms != null) {
				for (GroupSms g : groupSms) {
					if (g.getStatus().equals(Status.Inactive.getValue())) {
						userApi.sendSingleGroupSMS(g.getMobile(), g.getText());
						g.setCronStatus(true);
						g.setStatus(Status.Active.getValue());
						groupSmsRepository.save(g);
					}
				}
			}

			List<GroupSms> groupSms1 = groupSmsRepository.getAllGroupNumbers(true, true, Status.Inactive.getValue());
			if (groupSms1 != null) {
				for (GroupSms g : groupSms1) {
					if (g.getStatus().equals(Status.Inactive.getValue())) {
						userApi.sendSingleGroupSMS(g.getMobile(), g.getText());
						g.setCronStatus(true);
						g.setStatus(Status.Active.getValue());
						groupSmsRepository.save(g);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void bulkKYCUpload() {
		System.err.println("bulk register for failed is running>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
				+ ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + "______________________________________________"
				+ "+++++++++++++++++++++++++++++++++++++++++++++");
		List<CorporateFileCatalogue> fileList = corporateFileCatalogueRepository.getListOfBulkRegister();
		System.err.println("This is file size:::::::::::::::::::" + fileList.size());
		if (fileList != null) {
			for (CorporateFileCatalogue corporateFileCatalogue : fileList) {
				System.err.println(corporateFileCatalogue);
				if (corporateFileCatalogue.isFileRejectionStatus() == false) {
					if (corporateFileCatalogue.isReviewStatus() == true) {
						if (corporateFileCatalogue.getCategoryType().equalsIgnoreCase("BLKKYCUPLOAD")) {
							String absPath = corporateFileCatalogue.getAbsPath();
							String filePath = StartUpUtil.CSV_FILE + absPath;

							try {
								System.err.println(
										"This is file Path::::::::::::______________________________:" + filePath);
								JSONObject fileJsonObject = CSVReader.readCsvBulkKycUpload(filePath);
								System.err.println(fileJsonObject);
								if (fileJsonObject != null) {
									JSONArray jArray = fileJsonObject.getJSONArray("UserList");
									if (jArray != null) {
										for (int i = 0; i < jArray.length(); i++) {
											System.err.println(
													"Length of Array>>>>>>>>>>>>>>>>>>>>>>>>" + jArray.length());
											JSONObject jObject = jArray.getJSONObject(i);
											if (jObject != null) {
												MPQAccountType accountTypeKYC = mPQAccountTypeRepository
														.findByCode("KYC");
												MPQAccountType accountTypeNONKYC = mPQAccountTypeRepository
														.findByCode("NONKYC");

												String mobile = jObject.getString("mobile");
												String address1 = jObject.getString("address1");
												String address2 = jObject.getString("address2");
												String pinCode = jObject.getString("pinCode");
												String documentUrl = jObject.getString("documentUrl");
												String documentUrl1 = jObject.getString("documentUrl1");
												String idType = jObject.getString("idType");
												String idNumber = jObject.getString("idNumber");
												MUser user = userApi.findByUserName(mobile);
												if (user != null) {
													MKycDetail kycDetail = mKycRepository.findByUser(user);
													if (kycDetail == null) {
														kycDetail = new MKycDetail();
														kycDetail.setAddress1(address1);
														kycDetail.setAddress2(address2);
														kycDetail.setIdImage(documentUrl);
														kycDetail.setIdImage1(documentUrl1);
														kycDetail.setIdType(idType);
														kycDetail.setIdNumber(idNumber);
														kycDetail.setPinCode(pinCode);
														kycDetail.setUser(user);
														kycDetail.setUserType(UserType.User);
														AddressResponseDTO addResp = CommonUtil.getAddress(pinCode);
														if (addResp.getCode().equalsIgnoreCase("S00")) {
															kycDetail.setCity(addResp.getCity());
															kycDetail.setState(addResp.getState());
															kycDetail.setCountry(addResp.getCountry());
															UserKycResponse userKyc = matchMoveApi
																	.kycUserMMCorporate(kycDetail);
															if (userKyc.getCode().equalsIgnoreCase("S00")) {
																UserKycResponse userIdDetails = matchMoveApi
																		.setIdDetailsCorporate(kycDetail);
																if (userIdDetails.getCode().equalsIgnoreCase("S00")) {
																	UserKycResponse image1Upload = matchMoveApi
																			.setImagesForKyc1(kycDetail);
																	if (image1Upload.getCode()
																			.equalsIgnoreCase("S00")) {
																		UserKycResponse imageUpload2 = matchMoveApi
																				.setImagesForKyc2(kycDetail);
																		if (imageUpload2.getCode()
																				.equalsIgnoreCase("S00")) {
																			UserKycResponse approvalResp = matchMoveApi
																					.tempConfirmKycCorporate(kycDetail);
																			if (approvalResp.getCode()
																					.equalsIgnoreCase("S00")) {
																				MPQAccountDetails accDetails = user
																						.getAccountDetail();
																				if (accDetails != null) {
																					MPQAccountType accountType = mPQAccountTypeRepository
																							.findByCode("KYC");
																					accDetails.setAccountType(
																							accountType);
																					mPQAccountDetailRepository
																							.save(accDetails);
																					kycDetail.setAccountType(
																							accountType);
																					kycDetail.setRejectionStatus(false);
																					mKycRepository.save(kycDetail);

																				} else {
																					kycDetail.setAccountType(
																							accountTypeNONKYC);
																					kycDetail.setRejectionStatus(true);
																					kycDetail.setRejectionReason(
																							"No Account found");
																					mKycRepository.save(kycDetail);

																				}
																			} else {
																				kycDetail.setAccountType(
																						accountTypeNONKYC);
																				kycDetail.setRejectionStatus(true);
																				kycDetail.setRejectionReason(
																						approvalResp.getMessage());
																				mKycRepository.save(kycDetail);
																			}
																		} else {
																			kycDetail.setAccountType(accountTypeNONKYC);
																			kycDetail.setRejectionStatus(true);
																			kycDetail.setRejectionReason(
																					imageUpload2.getMessage());
																			mKycRepository.save(kycDetail);
																		}
																	} else {
																		kycDetail.setAccountType(accountTypeNONKYC);
																		kycDetail.setRejectionStatus(true);
																		kycDetail.setRejectionReason(
																				image1Upload.getMessage());
																		mKycRepository.save(kycDetail);
																	}
																} else {
																	kycDetail.setAccountType(accountTypeNONKYC);
																	kycDetail.setRejectionStatus(true);
																	kycDetail.setRejectionReason(
																			userIdDetails.getMessage());
																	mKycRepository.save(kycDetail);
																}
															} else {
																kycDetail.setAccountType(accountTypeNONKYC);
																kycDetail.setRejectionStatus(true);
																kycDetail.setRejectionReason(userKyc.getMessage());
																mKycRepository.save(kycDetail);
															}

														} else {
															kycDetail.setCity("Bangalore");
															kycDetail.setState("Karnataka");
															kycDetail.setCountry("India");

															// kycDetail.setCity(addResp.getCity());
															// kycDetail.setState(addResp.getState());
															// kycDetail.setCountry(addResp.getCountry());
															UserKycResponse userKyc = matchMoveApi
																	.kycUserMMCorporate(kycDetail);
															if (userKyc.getCode().equalsIgnoreCase("S00")) {
																UserKycResponse userIdDetails = matchMoveApi
																		.setIdDetailsCorporate(kycDetail);
																if (userIdDetails.getCode().equalsIgnoreCase("S00")) {
																	UserKycResponse image1Upload = matchMoveApi
																			.setImagesForKyc1(kycDetail);
																	if (image1Upload.getCode()
																			.equalsIgnoreCase("S00")) {
																		UserKycResponse imageUpload2 = matchMoveApi
																				.setImagesForKyc2(kycDetail);
																		if (imageUpload2.getCode()
																				.equalsIgnoreCase("S00")) {
																			UserKycResponse approvalResp = matchMoveApi
																					.tempConfirmKycCorporate(kycDetail);
																			if (approvalResp.getCode()
																					.equalsIgnoreCase("S00")) {
																				MPQAccountDetails accDetails = user
																						.getAccountDetail();
																				if (accDetails != null) {
																					MPQAccountType accountType = mPQAccountTypeRepository
																							.findByCode("KYC");
																					accDetails.setAccountType(
																							accountType);
																					mPQAccountDetailRepository
																							.save(accDetails);
																					kycDetail.setAccountType(
																							accountType);
																					kycDetail.setRejectionStatus(false);
																					mKycRepository.save(kycDetail);

																				} else {
																					kycDetail.setAccountType(
																							accountTypeNONKYC);
																					kycDetail.setRejectionStatus(true);
																					kycDetail.setRejectionReason(
																							"No Account found");
																					mKycRepository.save(kycDetail);

																				}
																			} else {
																				kycDetail.setAccountType(
																						accountTypeNONKYC);
																				kycDetail.setRejectionStatus(true);
																				kycDetail.setRejectionReason(
																						approvalResp.getMessage());
																				mKycRepository.save(kycDetail);
																			}
																		} else {
																			kycDetail.setAccountType(accountTypeNONKYC);
																			kycDetail.setRejectionStatus(true);
																			kycDetail.setRejectionReason(
																					imageUpload2.getMessage());
																			mKycRepository.save(kycDetail);
																		}
																	} else {
																		kycDetail.setAccountType(accountTypeNONKYC);
																		kycDetail.setRejectionStatus(true);
																		kycDetail.setRejectionReason(
																				image1Upload.getMessage());
																		mKycRepository.save(kycDetail);
																	}
																} else {
																	kycDetail.setAccountType(accountTypeNONKYC);
																	kycDetail.setRejectionStatus(true);
																	kycDetail.setRejectionReason(
																			userIdDetails.getMessage());
																	mKycRepository.save(kycDetail);
																}
															} else {
																kycDetail.setAccountType(accountTypeNONKYC);
																kycDetail.setRejectionStatus(true);
																kycDetail.setRejectionReason(userKyc.getMessage());
																mKycRepository.save(kycDetail);
															}

														}
													} else {
														MPQAccountDetails accountDetails = kycDetail.getUser()
																.getAccountDetail();
														if (accountDetails != null) {
															MPQAccountType accountType = accountDetails
																	.getAccountType();
															if (accountType.getCode().equalsIgnoreCase("NONKYC")) {
																mKycRepository.delete(kycDetail);
																MKycDetail kycDetails = new MKycDetail();
																kycDetails.setAddress1(address1);
																kycDetails.setAddress2(address2);
																kycDetails.setIdImage(documentUrl);
																kycDetails.setIdImage1(documentUrl);
																kycDetails.setIdType(idType);
																kycDetails.setIdNumber(idNumber);
																kycDetails.setPinCode(pinCode);
																kycDetail.setUser(user);
																kycDetail.setUserType(UserType.User);
																AddressResponseDTO addResp = CommonUtil
																		.getAddress(pinCode);
																if (addResp.getCode().equalsIgnoreCase("S00")) {
																	kycDetails.setCity(addResp.getCity());
																	kycDetails.setState(addResp.getState());
																	kycDetails.setCountry(addResp.getCountry());
																	UserKycResponse userKyc = matchMoveApi
																			.kycUserMMCorporate(kycDetail);
																	if (userKyc.getCode().equalsIgnoreCase("S00")) {
																		UserKycResponse userIdDetails = matchMoveApi
																				.setIdDetailsCorporate(kycDetail);
																		if (userIdDetails.getCode()
																				.equalsIgnoreCase("S00")) {
																			UserKycResponse image1Upload = matchMoveApi
																					.setImagesForKyc1(kycDetail);
																			if (image1Upload.getCode()
																					.equalsIgnoreCase("S00")) {
																				UserKycResponse imageUpload2 = matchMoveApi
																						.setImagesForKyc2(kycDetail);
																				if (imageUpload2.getCode()
																						.equalsIgnoreCase("S00")) {
																					UserKycResponse approvalResp = matchMoveApi
																							.tempConfirmKycCorporate(
																									kycDetail);
																					if (approvalResp.getCode()
																							.equalsIgnoreCase("S00")) {
																						MPQAccountDetails accDetails = user
																								.getAccountDetail();
																						if (accDetails != null) {
																							// MPQAccountType
																							// accountType=mPQAccountTypeRepository.findByCode("KYC");
																							accDetails.setAccountType(
																									accountTypeKYC);
																							mPQAccountDetailRepository
																									.save(accDetails);
																							kycDetail.setAccountType(
																									accountTypeKYC);
																							kycDetail
																									.setRejectionStatus(
																											false);
																							mKycRepository
																									.save(kycDetail);

																						} else {
																							kycDetail.setAccountType(
																									accountTypeNONKYC);
																							kycDetail
																									.setRejectionStatus(
																											true);
																							kycDetail
																									.setRejectionReason(
																											"No Account found");
																							mKycRepository
																									.save(kycDetail);

																						}
																					} else {
																						kycDetail.setAccountType(
																								accountTypeNONKYC);
																						kycDetail.setRejectionStatus(
																								true);
																						kycDetail.setRejectionReason(
																								approvalResp
																										.getMessage());
																						mKycRepository.save(kycDetail);
																					}
																				} else {
																					kycDetail.setAccountType(
																							accountTypeNONKYC);
																					kycDetail.setRejectionStatus(true);
																					kycDetail.setRejectionReason(
																							imageUpload2.getMessage());
																					mKycRepository.save(kycDetail);
																				}
																			} else {
																				kycDetail.setAccountType(
																						accountTypeNONKYC);
																				kycDetail.setRejectionStatus(true);
																				kycDetail.setRejectionReason(
																						image1Upload.getMessage());
																				mKycRepository.save(kycDetail);
																			}
																		} else {
																			kycDetail.setAccountType(accountTypeNONKYC);
																			kycDetail.setRejectionStatus(true);
																			kycDetail.setRejectionReason(
																					userIdDetails.getMessage());
																			mKycRepository.save(kycDetail);
																		}
																	} else {
																		kycDetail.setAccountType(accountTypeNONKYC);
																		kycDetail.setRejectionStatus(true);
																		kycDetail.setRejectionReason(
																				userKyc.getMessage());
																		mKycRepository.save(kycDetail);
																	}

																} else {
																	kycDetails.setCity("Bangalore");
																	kycDetails.setState("Karnataka");
																	kycDetails.setCountry("India");

																	UserKycResponse userKyc = matchMoveApi
																			.kycUserMMCorporate(kycDetail);
																	if (userKyc.getCode().equalsIgnoreCase("S00")) {
																		UserKycResponse userIdDetails = matchMoveApi
																				.setIdDetailsCorporate(kycDetail);
																		if (userIdDetails.getCode()
																				.equalsIgnoreCase("S00")) {
																			UserKycResponse image1Upload = matchMoveApi
																					.setImagesForKyc1(kycDetail);
																			if (image1Upload.getCode()
																					.equalsIgnoreCase("S00")) {
																				UserKycResponse imageUpload2 = matchMoveApi
																						.setImagesForKyc2(kycDetail);
																				if (imageUpload2.getCode()
																						.equalsIgnoreCase("S00")) {
																					UserKycResponse approvalResp = matchMoveApi
																							.tempConfirmKycCorporate(
																									kycDetail);
																					if (approvalResp.getCode()
																							.equalsIgnoreCase("S00")) {
																						MPQAccountDetails accDetails = user
																								.getAccountDetail();
																						if (accDetails != null) {
																							// MPQAccountType
																							// accountType=mPQAccountTypeRepository.findByCode("KYC");
																							accDetails.setAccountType(
																									accountTypeKYC);
																							mPQAccountDetailRepository
																									.save(accDetails);
																							kycDetail.setAccountType(
																									accountTypeKYC);
																							kycDetail
																									.setRejectionStatus(
																											false);
																							mKycRepository
																									.save(kycDetail);

																						} else {
																							kycDetail.setAccountType(
																									accountTypeNONKYC);
																							kycDetail
																									.setRejectionStatus(
																											true);
																							kycDetail
																									.setRejectionReason(
																											"No Account found");
																							mKycRepository
																									.save(kycDetail);

																						}
																					} else {
																						kycDetail.setAccountType(
																								accountTypeNONKYC);
																						kycDetail.setRejectionStatus(
																								true);
																						kycDetail.setRejectionReason(
																								approvalResp
																										.getMessage());
																						mKycRepository.save(kycDetail);
																					}
																				} else {
																					kycDetail.setAccountType(
																							accountTypeNONKYC);
																					kycDetail.setRejectionStatus(true);
																					kycDetail.setRejectionReason(
																							imageUpload2.getMessage());
																					mKycRepository.save(kycDetail);
																				}
																			} else {
																				kycDetail.setAccountType(
																						accountTypeNONKYC);
																				kycDetail.setRejectionStatus(true);
																				kycDetail.setRejectionReason(
																						image1Upload.getMessage());
																				mKycRepository.save(kycDetail);
																			}
																		} else {
																			kycDetail.setAccountType(accountTypeNONKYC);
																			kycDetail.setRejectionStatus(true);
																			kycDetail.setRejectionReason(
																					userIdDetails.getMessage());
																			mKycRepository.save(kycDetail);
																		}
																	} else {
																		kycDetail.setAccountType(accountTypeNONKYC);
																		kycDetail.setRejectionStatus(true);
																		kycDetail.setRejectionReason(
																				userKyc.getMessage());
																		mKycRepository.save(kycDetail);
																	}

																}
															} else {
																MPQAccountDetails accDetails = user.getAccountDetail();
																UserKycResponse kycResponse = matchMoveApi
																		.checkKYCStatus(user);
																if (!(kycResponse.getStatus()
																		.equalsIgnoreCase("Approved")
																		|| kycResponse.getCode()
																				.equalsIgnoreCase("S00"))) {

																	mKycRepository.delete(kycDetail);
																	MKycDetail kycDetails = new MKycDetail();
																	kycDetails.setAddress1(address1);
																	kycDetails.setAddress2(address2);
																	kycDetails.setIdImage(documentUrl);
																	kycDetails.setIdImage1(documentUrl);
																	kycDetails.setIdType(idType);
																	kycDetails.setIdNumber(idNumber);
																	kycDetails.setPinCode(pinCode);
																	kycDetail.setUser(user);
																	kycDetail.setUserType(UserType.User);
																	AddressResponseDTO addResp = CommonUtil
																			.getAddress(pinCode);
																	if (addResp.getCode().equalsIgnoreCase("S00")) {
																		kycDetails.setCity(addResp.getCity());
																		kycDetails.setState(addResp.getState());
																		kycDetails.setCountry(addResp.getCountry());
																		UserKycResponse userKyc = matchMoveApi
																				.kycUserMMCorporate(kycDetail);
																		if (userKyc.getCode().equalsIgnoreCase("S00")) {
																			UserKycResponse userIdDetails = matchMoveApi
																					.setIdDetailsCorporate(kycDetail);
																			if (userIdDetails.getCode()
																					.equalsIgnoreCase("S00")) {
																				UserKycResponse image1Upload = matchMoveApi
																						.setImagesForKyc1(kycDetail);
																				if (image1Upload.getCode()
																						.equalsIgnoreCase("S00")) {
																					UserKycResponse imageUpload2 = matchMoveApi
																							.setImagesForKyc2(
																									kycDetail);
																					if (imageUpload2.getCode()
																							.equalsIgnoreCase("S00")) {
																						UserKycResponse approvalResp = matchMoveApi
																								.tempConfirmKycCorporate(
																										kycDetail);
																						if (approvalResp.getCode()
																								.equalsIgnoreCase(
																										"S00")) {

																							if (accDetails != null) {
																								// MPQAccountType
																								// accountType=mPQAccountTypeRepository.findByCode("KYC");
																								accDetails
																										.setAccountType(
																												accountTypeKYC);
																								mPQAccountDetailRepository
																										.save(accDetails);
																								kycDetail
																										.setAccountType(
																												accountTypeKYC);
																								kycDetail
																										.setRejectionStatus(
																												false);
																								mKycRepository.save(
																										kycDetail);

																							} else {
																								kycDetail
																										.setAccountType(
																												accountTypeNONKYC);
																								kycDetail
																										.setRejectionStatus(
																												true);
																								kycDetail
																										.setRejectionReason(
																												"No Account found");
																								mKycRepository.save(
																										kycDetail);

																							}
																						} else {
																							kycDetail.setAccountType(
																									accountTypeNONKYC);
																							kycDetail
																									.setRejectionStatus(
																											true);
																							kycDetail
																									.setRejectionReason(
																											approvalResp
																													.getMessage());
																							mKycRepository
																									.save(kycDetail);
																							accDetails.setAccountType(
																									accountTypeNONKYC);
																							mPQAccountDetailRepository
																									.save(accDetails);
																						}
																					} else {
																						kycDetail.setAccountType(
																								accountTypeNONKYC);
																						kycDetail.setRejectionStatus(
																								true);
																						kycDetail.setRejectionReason(
																								imageUpload2
																										.getMessage());
																						mKycRepository.save(kycDetail);
																						accDetails.setAccountType(
																								accountTypeNONKYC);
																						mPQAccountDetailRepository
																								.save(accDetails);
																					}
																				} else {
																					kycDetail.setAccountType(
																							accountTypeNONKYC);
																					kycDetail.setRejectionStatus(true);
																					kycDetail.setRejectionReason(
																							image1Upload.getMessage());
																					mKycRepository.save(kycDetail);
																					accDetails.setAccountType(
																							accountTypeNONKYC);
																					mPQAccountDetailRepository
																							.save(accDetails);
																				}
																			} else {
																				kycDetail.setAccountType(
																						accountTypeNONKYC);
																				kycDetail.setRejectionStatus(true);
																				kycDetail.setRejectionReason(
																						userIdDetails.getMessage());
																				mKycRepository.save(kycDetail);
																				accDetails.setAccountType(
																						accountTypeNONKYC);
																				mPQAccountDetailRepository
																						.save(accDetails);
																			}
																		} else {
																			kycDetail.setAccountType(accountTypeNONKYC);
																			kycDetail.setRejectionStatus(true);
																			kycDetail.setRejectionReason(
																					userKyc.getMessage());
																			mKycRepository.save(kycDetail);
																			accDetails
																					.setAccountType(accountTypeNONKYC);
																			mPQAccountDetailRepository.save(accDetails);
																		}

																	} else {
																		kycDetails.setCity("Bangalore");
																		kycDetails.setState("Karnataka");
																		kycDetails.setCountry("India");

																		UserKycResponse userKyc = matchMoveApi
																				.kycUserMMCorporate(kycDetail);
																		if (userKyc.getCode().equalsIgnoreCase("S00")) {
																			UserKycResponse userIdDetails = matchMoveApi
																					.setIdDetailsCorporate(kycDetail);
																			if (userIdDetails.getCode()
																					.equalsIgnoreCase("S00")) {
																				UserKycResponse image1Upload = matchMoveApi
																						.setImagesForKyc1(kycDetail);
																				if (image1Upload.getCode()
																						.equalsIgnoreCase("S00")) {
																					UserKycResponse imageUpload2 = matchMoveApi
																							.setImagesForKyc2(
																									kycDetail);
																					if (imageUpload2.getCode()
																							.equalsIgnoreCase("S00")) {
																						UserKycResponse approvalResp = matchMoveApi
																								.tempConfirmKycCorporate(
																										kycDetail);
																						if (approvalResp.getCode()
																								.equalsIgnoreCase(
																										"S00")) {
																							if (accDetails != null) {
																								// MPQAccountType
																								// accountType=mPQAccountTypeRepository.findByCode("KYC");
																								accDetails
																										.setAccountType(
																												accountTypeKYC);
																								mPQAccountDetailRepository
																										.save(accDetails);
																								kycDetail
																										.setAccountType(
																												accountTypeKYC);
																								kycDetail
																										.setRejectionStatus(
																												false);
																								mKycRepository.save(
																										kycDetail);

																							} else {
																								kycDetail
																										.setAccountType(
																												accountTypeNONKYC);
																								kycDetail
																										.setRejectionStatus(
																												true);
																								kycDetail
																										.setRejectionReason(
																												"No Account found");
																								mKycRepository.save(
																										kycDetail);

																							}
																						} else {
																							kycDetail.setAccountType(
																									accountTypeNONKYC);
																							kycDetail
																									.setRejectionStatus(
																											true);
																							kycDetail
																									.setRejectionReason(
																											approvalResp
																													.getMessage());
																							mKycRepository
																									.save(kycDetail);
																							accDetails.setAccountType(
																									accountTypeNONKYC);
																							mPQAccountDetailRepository
																									.save(accDetails);
																						}
																					} else {
																						kycDetail.setAccountType(
																								accountTypeNONKYC);
																						kycDetail.setRejectionStatus(
																								true);
																						kycDetail.setRejectionReason(
																								imageUpload2
																										.getMessage());
																						mKycRepository.save(kycDetail);
																						accDetails.setAccountType(
																								accountTypeNONKYC);
																						mPQAccountDetailRepository
																								.save(accDetails);
																					}
																				} else {
																					kycDetail.setAccountType(
																							accountTypeNONKYC);
																					kycDetail.setRejectionStatus(true);
																					kycDetail.setRejectionReason(
																							image1Upload.getMessage());
																					mKycRepository.save(kycDetail);
																					accDetails.setAccountType(
																							accountTypeNONKYC);
																					mPQAccountDetailRepository
																							.save(accDetails);
																				}
																			} else {
																				kycDetail.setAccountType(
																						accountTypeNONKYC);
																				kycDetail.setRejectionStatus(true);
																				kycDetail.setRejectionReason(
																						userIdDetails.getMessage());
																				mKycRepository.save(kycDetail);
																				accDetails.setAccountType(
																						accountTypeNONKYC);
																				mPQAccountDetailRepository
																						.save(accDetails);
																			}
																		} else {
																			kycDetail.setAccountType(accountTypeNONKYC);
																			kycDetail.setRejectionStatus(true);
																			kycDetail.setRejectionReason(
																					userKyc.getMessage());
																			mKycRepository.save(kycDetail);
																			accDetails
																					.setAccountType(accountTypeNONKYC);
																			mPQAccountDetailRepository.save(accDetails);
																		}

																	}

																}
															}
														}
													}
												}

											}
										}
									}
								}
							} catch (Exception e) {
								e.printStackTrace();

							}
						}
					}
				}
				corporateFileCatalogue.setSchedulerStatus(true);
				corporateFileCatalogueRepository.save(corporateFileCatalogue);

			}

		}
	}

	public void bulkCardAssignmentGroup() {
		System.err.println("bulk card assignmentGroup>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
				+ ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + "______________________________________________"
				+ "+++++++++++++++++++++++++++++++++++++++++++++");
		List<GroupFileCatalogue> fileList = groupFileCatalogueRepository.getBulkCardAssignmentGroup();
		System.err.println("This is file size:::::::::::::::::::" + fileList.size());
		if (fileList != null) {
			for (GroupFileCatalogue corporateFileCatalogue : fileList) {
				System.err.println(corporateFileCatalogue);
				if (corporateFileCatalogue.isFileRejectionStatus() == false) {
					if (corporateFileCatalogue.isReviewStatus() == true) {
						if (corporateFileCatalogue.getCategoryType().equalsIgnoreCase("BLKCARDASSIGNGROUP")) {
							String absPath = corporateFileCatalogue.getAbsPath();
							String filePath = StartUpUtil.CSV_FILE + absPath;

							try {
								JSONObject fileJsonObject = CSVReader.readCsvBulkCardAssignmentGroup(filePath);
								System.err.println(fileJsonObject);
								if (fileJsonObject != null) {
									JSONArray jArray = fileJsonObject.getJSONArray("CardList");
									if (jArray != null) {
										for (int i = 0; i < jArray.length(); i++) {
											System.err.println(
													"Length of Array>>>>>>>>>>>>>>>>>>>>>>>>" + jArray.length());
											JSONObject jObject = jArray.getJSONObject(i);
											if (jObject != null) {
												BulkCardAssignmentGroup bulkCardAssignment = new BulkCardAssignmentGroup();
												String mobile = jObject.getString("mobile");
												String proxyNumber = jObject.getString("proxyNumber");
												try {
													GroupCardGenerationErrorDTO error = cardCreationGroupValidation
															.validateCardCreation(mobile);
													if (error != null) {
														if (error.isValid()) {
															bulkCardAssignment.setMobileNumber(mobile);
															bulkCardAssignment.setProxyNumber(proxyNumber);
															bulkCardAssignment
																	.setGroupDetails(corporateFileCatalogue.getUser());
															bulkCardAssignment.setFileName(absPath);
															System.err.println("calling assign");
															WalletResponse walletResponse = matchMoveApi
																	.assignPhysicalCardFromGroup(mobile, proxyNumber);
															if (walletResponse.getCode().equalsIgnoreCase("S00")) {
																bulkCardAssignment.setCreationStatus(Status.Success);
																bulkCardAssignment.setActivationStatus(Status.Active);
															} else if (walletResponse.getCode()
																	.equalsIgnoreCase("A00")) {
																bulkCardAssignment
																		.setRemarks(walletResponse.getMessage());
																bulkCardAssignment.setCreationStatus(Status.Success);
																bulkCardAssignment.setActivationStatus(Status.Inactive);

															} else {
																bulkCardAssignment.setCreationStatus(Status.Failed);
																bulkCardAssignment.setActivationStatus(Status.Inactive);
																bulkCardAssignment
																		.setRemarks(walletResponse.getMessage());
															}
														} else {
															bulkCardAssignment.setMobileNumber(mobile);
															bulkCardAssignment.setProxyNumber(proxyNumber);
															bulkCardAssignment
																	.setGroupDetails(corporateFileCatalogue.getUser());
															bulkCardAssignment.setFileName(absPath);

															bulkCardAssignment.setCreationStatus(Status.Failed);
															bulkCardAssignment.setActivationStatus(Status.Inactive);
															bulkCardAssignment.setRemarks(error.getMessage());
														}
													} else {
														bulkCardAssignment.setCreationStatus(Status.Failed);
														bulkCardAssignment.setActivationStatus(Status.Inactive);
														bulkCardAssignment.setRemarks("Fatal Error");
														bulkCardAssignment
																.setGroupDetails(corporateFileCatalogue.getUser());
														bulkCardAssignment.setFileName(absPath);
														bulkCardAssignment.setMobileNumber(mobile);
														bulkCardAssignment.setProxyNumber(proxyNumber);

													}
													bulkCardAssignmentGroupRepository.save(bulkCardAssignment);
												} catch (Exception e) {
													e.printStackTrace();
													if (e instanceof SQLIntegrityConstraintViolationException) {
														bulkCardAssignment.setCreationStatus(Status.Failed);
														bulkCardAssignment.setActivationStatus(Status.Inactive);
														bulkCardAssignment.setRemarks("Duplicate Entry");
														bulkCardAssignment
																.setGroupDetails(corporateFileCatalogue.getUser());
														bulkCardAssignment.setFileName(absPath);
														bulkCardAssignment.setMobileNumber(mobile);
														bulkCardAssignment.setProxyNumber(proxyNumber + "D");
														bulkCardAssignmentGroupRepository.save(bulkCardAssignment);
													} else {
														bulkCardAssignment.setCreationStatus(Status.Failed);
														bulkCardAssignment.setActivationStatus(Status.Inactive);
														bulkCardAssignment.setRemarks(e.getMessage());
														bulkCardAssignment
																.setGroupDetails(corporateFileCatalogue.getUser());
														bulkCardAssignment.setFileName(absPath);
														bulkCardAssignment.setMobileNumber(mobile);
														bulkCardAssignment.setProxyNumber(proxyNumber + "D");
														bulkCardAssignmentGroupRepository.save(bulkCardAssignment);
													}

												}

											}
										}
									}

								}

							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				}
				corporateFileCatalogue.setSchedulerStatus(true);
				groupFileCatalogueRepository.save(corporateFileCatalogue);

			}
		}

	}

	public void bulkRegisterWhileFailed() {
		System.err.println("bulk register for failed is running MM---");
		List<CorporateFileCatalogue> fileList = corporateFileCatalogueRepository.getListOfBulkRegister();
		if (fileList != null && fileList.size() > 0) {
			for (CorporateFileCatalogue corporateFileCatalogue : fileList) {
				if (corporateFileCatalogue.isFileRejectionStatus() == false) {
					if (corporateFileCatalogue.isReviewStatus() == true) {
						if (corporateFileCatalogue.getCategoryType().equalsIgnoreCase("BLKREGISTER")) {
							String absPath = corporateFileCatalogue.getAbsPath();
							String filePath = StartUpUtil.CSV_FILE + absPath;
							try {
								JSONObject fileJsonObject = CSVReader.readCsvBulkUploadRegister(filePath);
								System.err.println(fileJsonObject);
								if (fileJsonObject != null) {
									JSONArray jArray = fileJsonObject.getJSONArray("UserList");
									if (jArray != null) {
										for (int i = 0; i < jArray.length(); i++) {
											JSONObject jObject = jArray.getJSONObject(i);
											if (jObject != null) {
												String fname = jObject.getString("firstName");
												String lname = jObject.getString("lastName");
												String mobile = jObject.getString("mobile");
												String email = jObject.getString("email");
												String dob = jObject.getString("dob");
												if (dob == null) {
													dob = "1970-01-01";
												}
												String proxyNo = "0000" + jObject.getString("proxyNo");
												RegisterDTO registerDTO = new RegisterDTO();
												registerDTO.setFirstName(fname);
												registerDTO.setLastName(lname);
												registerDTO.setUserType(UserType.User);
												registerDTO.setPassword("123456");
												SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
												Date d1 = sdf.parse(dob);
												registerDTO
														.setGroup(corporateFileCatalogue.getCorporate().getUsername());
												registerDTO.setDateOfBirth(sdf.format(d1));
												registerDTO.setEmail(email);
												registerDTO.setContactNo(mobile);
												registerDTO.setUsername(mobile);
												RegisterError regErr = registerValidation
														.validateCorporateUserRecheck(registerDTO);
												if (regErr.isValid()) {
													UserKycResponse users = matchMoveApi.getUsers(email, mobile);
													MUser usermob = userApi.findByUserName(mobile);
													if (usermob == null) {
														userApi.saveUserCorpUser(registerDTO);
													}
													usermob = userApi.findByUserName(mobile);
													CorporateAgentDetails corpDetails = corporateAgentDetailsRepository
															.getByCorporateId(corporateFileCatalogue.getCorporate());
													BulkRegister bulkRegister = bulkRegisterRepository
															.getByUser(usermob);
													if (bulkRegister == null) {
														bulkRegister = new BulkRegister();
														bulkRegister.setDob(dob);
														bulkRegister.setEmail(email);
														bulkRegister.setMobile(mobile);
														bulkRegister.setUser(usermob);
														bulkRegister.setAgentDetails(corpDetails);
														bulkRegister.setName(fname + " " + lname);
														if (corporateFileCatalogue.getPartnerDetails() != null) {
															bulkRegister.setPartnerDetails(
																	corporateFileCatalogue.getPartnerDetails());
														}
													}
													if (!users.getCode().equalsIgnoreCase("S00")) {
														ResponseDTO resUser = corporateMatchMoveApi
																.createUserOnMM(usermob);
														if (resUser.getCode().equalsIgnoreCase("S00")) {
															bulkRegister.setUserCreationStatus(true);
															bulkRegister.setWalletCreationStatus(true);
															MMCards cardUser = cardRepository
																	.getPhysicalCardByUser(usermob);
															if (cardUser == null) {
																WalletResponse phyCardCreate = corporateMatchMoveApi
																		.assignPhysicalCard(usermob, proxyNo);
																if (phyCardCreate.getCode().equalsIgnoreCase("S00")) {
																	bulkRegister.setCardCreationStatus(true);
																	bulkRegister.setPhyCardActivationStatus(true);
																	ResponseDTO activateResp = corporateMatchMoveApi
																			.activateMMPhysicalCard(
																					phyCardCreate.getCardId(),
																					phyCardCreate.getActivationCode());

																	if (activateResp.getCode()
																			.equalsIgnoreCase("S00")) {
																		bulkRegister.setPhyCardActivationStatus(true);
																	}
																}

															}
														}
													} else {
														bulkRegister.setUserCreationStatus(true);
														bulkRegister.setWalletCreationStatus(true);
														MMCards cardUser = cardRepository
																.getPhysicalCardByUser(usermob);
														if (cardUser == null) {
															WalletResponse phyCardCreate = corporateMatchMoveApi
																	.assignPhysicalCard(usermob, proxyNo);
															if (phyCardCreate.getCode().equalsIgnoreCase("S00")) {
																bulkRegister.setCardCreationStatus(true);
																bulkRegister.setPhyCardActivationStatus(true);
																ResponseDTO activateResp = corporateMatchMoveApi
																		.activateMMPhysicalCard(
																				phyCardCreate.getCardId(),
																				phyCardCreate.getActivationCode());
																if (activateResp.getCode().equalsIgnoreCase("S00")) {
																	bulkRegister.setPhyCardActivationStatus(true);
																}
															}
														}

													}
													bulkRegisterRepository.save(bulkRegister);
												}
											}
										}
									}
								}
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				}
				corporateFileCatalogue.setSchedulerStatus(true);
				corporateFileCatalogueRepository.save(corporateFileCatalogue);

			}
		}
		System.out.println("No file to upload");
	}

	public void checkGroupCardActivationStatus() {

		List<BulkCardAssignmentGroup> inactiveCardList = bulkCardAssignmentGroupRepository
				.getBulkCardAssignmentWithStatusInactive();
		if (inactiveCardList != null) {
			for (BulkCardAssignmentGroup bulkCardAssignmentGroup : inactiveCardList) {
				MMCards cards = cardRepository
						.getPhysicalCardByUserName(bulkCardAssignmentGroup.getMobileNumber().trim());
				if (cards != null) {
					if (cards.getStatus().equalsIgnoreCase("Inactive")) {
						if (cards.getActivationCode() != null) {
							ResponseDTO resp = matchMoveApi.activateMMPhysicalCard(cards.getCardId(),
									cards.getActivationCode());
							if (resp != null) {
								if (resp.getCode().equalsIgnoreCase("S00")) {
									bulkCardAssignmentGroup.setActivationStatus(Status.Active);
									bulkCardAssignmentGroup.setRemarks("Activated");
									bulkCardAssignmentGroupRepository.save(bulkCardAssignmentGroup);
								} else {
									bulkCardAssignmentGroup.setRemarks(resp.getMessage());
									bulkCardAssignmentGroupRepository.save(bulkCardAssignmentGroup);

								}
							}

						}
					}

				}
			}
		}
	}

}
