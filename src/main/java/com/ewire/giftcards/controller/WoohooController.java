package com.ewire.giftcards.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ewire.giftcards.api.WoohooApi;
import com.ewire.giftcards.response.GiftCardListResponse;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.MatchMoveCreateCardRequest;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.app.sms.util.SMSAccount;
import com.msscard.app.sms.util.SMSTemplete;
import com.msscard.entity.MatchMoveWallet;
import com.msscard.entity.UserSession;
import com.msscard.model.ResponseStatus;
import com.msscard.model.UserDTO;
import com.msscard.repositories.MUserRespository;
import com.msscard.repositories.UserSessionRepository;
import com.msscard.util.Authorities;
import com.msscard.util.SecurityUtil;
import com.msscards.session.PersistingSessionRegistry;

public class WoohooController {

	private final WoohooApi woohooApi;
	private final MUserRespository userRespository;
	private final UserSessionRepository sessionRepository;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final IUserApi userApi;
	
	public WoohooController(WoohooApi woohooApi, MUserRespository userRespository,
			UserSessionRepository sessionRepository, PersistingSessionRegistry persistingSessionRegistry,
			IUserApi userApi) {
		super();
		this.woohooApi = woohooApi;
		this.userRespository = userRespository;
		this.sessionRepository = sessionRepository;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.userApi = userApi;
	}
	
	/*@RequestMapping(value="/FetchGiftCard",method=RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE}
	,produces={MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<GiftCardListResponse> fetchGiftCardList(@PathVariable(value="role") String role,@PathVariable(value="device") String device,@PathVariable(value="language") String language, 
			@RequestBody MatchMoveCreateCardRequest cardRequest,@RequestHeader(value="hash",required=true) String hash, HttpServletRequest request,HttpServletResponse response){
		GiftCardListResponse walletResponse=new GiftCardListResponse(); 
		boolean isValidHash = SecurityUtil.isHashMatches(cardRequest.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = cardRequest.getSessionId();
				UserSession userSession = sessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						MatchMoveCreateCardRequest createCardRequest=new MatchMoveCreateCardRequest();
						createCardRequest.setEmail(user.getEmail());
						try {
							createCardRequest.setPassword(SecurityUtil.md5(user.getContactNo()));
						} catch (Exception e) {
							e.printStackTrace();
						}
					 MatchMoveWallet matchMoveWallet=matchMoveWalletRepository.findByUser(userSession.getUser());
					 if(matchMoveWallet!=null){
									WalletResponse walletResponse4=matchMoveApi.assignVirtualCard(userSession.getUser());
									if(walletResponse4.getCode().equalsIgnoreCase("S00")){
										senderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.VIRTUAL_CARD_GENERATION, userSession.getUser(), null);
										return new ResponseEntity<>(walletResponse4,HttpStatus.OK);
									}
								
							
					 }else{
						 walletResponse.setCode("F00");
						 walletResponse.setMessage("You are not eligible for card.Please contact Customer care.");
					 }
					 }else{
						walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						walletResponse.setMessage("Unauthorized User..");
						return new ResponseEntity<>(walletResponse,HttpStatus.OK);
					}
				}else{
					walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
					walletResponse.setMessage("Invalid Session...");
					return new ResponseEntity<>(walletResponse,HRttpStatus.OK);
				}
			}else{
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				walletResponse.setMessage("Unauthorized Role...");
				return new ResponseEntity<>(walletResponse,HttpStatus.OK);
			}
		}else{
			walletResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
			walletResponse.setMessage("Invalid Hash");
			return new ResponseEntity<>(walletResponse,HttpStatus.OK);
		}
		walletResponse.setCode("F00");
		walletResponse.setMessage("Operation Failed.Please contact Customer Care");
		return new ResponseEntity<>(walletResponse,HttpStatus.OK);
		

	}
*/	
	
}
