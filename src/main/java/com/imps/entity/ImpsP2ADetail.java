package com.imps.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

import com.msscard.entity.AbstractEntity;
import com.msscard.entity.MTransaction;


@Entity
public class ImpsP2ADetail extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;

	
	@Column(nullable=false)
	private String accountNo;
	
	
	@Column(nullable=false)
	private String ifscCode;
	
	@Column(nullable=true)
	private String name;
	
	@Column(nullable=false)
	private String amount;
	
	@Column(nullable=true)
	private String retrivalRefNo;
	
	@Column(nullable=true)
	private String description;
	
	@Column(nullable=false)
	private String status;
	
	@OneToOne(fetch=FetchType.EAGER)
	private MTransaction transactionId;

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getRetrivalRefNo() {
		return retrivalRefNo;
	}

	public void setRetrivalRefNo(String retrivalRefNo) {
		this.retrivalRefNo = retrivalRefNo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public MTransaction getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(MTransaction transactionId) {
		this.transactionId = transactionId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
}
