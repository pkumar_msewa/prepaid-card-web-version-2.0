package com.imps.model;

public class ImpsRequest {

	private String sessionId;
	private String amount;
	private String mmidNo;
	private String accountNo;
	private String ifscNo;
	private String contactNo;
	private String name;
	private String transactionRefNo;
	private String retrivalRefNo;
	private String description;
	private String remName;
	private String remContactNo;
	private String message;
	private String confirmAcountNo;
	private boolean saveBeneficiary;
	private String throughBeneficiary;
	private String bankName;
	
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getThroughBeneficiary() {
		return throughBeneficiary;
	}
	public void setThroughBeneficiary(String throughBeneficiary) {
		this.throughBeneficiary = throughBeneficiary;
	}
	public boolean isSaveBeneficiary() {
		return saveBeneficiary;
	}
	public void setSaveBeneficiary(boolean saveBeneficiary) {
		this.saveBeneficiary = saveBeneficiary;
	}
	public String getConfirmAcountNo() {
		return confirmAcountNo;
	}
	public void setConfirmAcountNo(String confirmAcountNo) {
		this.confirmAcountNo = confirmAcountNo;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getMmidNo() {
		return mmidNo;
	}
	public void setMmidNo(String mmidNo) {
		this.mmidNo = mmidNo;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getIfscNo() {
		return ifscNo;
	}
	public void setIfscNo(String ifscNo) {
		this.ifscNo = ifscNo;
	}
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTransactionRefNo() {
		return transactionRefNo;
	}
	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}
	public String getRetrivalRefNo() {
		return retrivalRefNo;
	}
	public void setRetrivalRefNo(String retrivalRefNo) {
		this.retrivalRefNo = retrivalRefNo;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getRemName() {
		return remName;
	}
	public void setRemName(String remName) {
		this.remName = remName;
	}
	public String getRemContactNo() {
		return remContactNo;
	}
	public void setRemContactNo(String remContactNo) {
		this.remContactNo = remContactNo;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
