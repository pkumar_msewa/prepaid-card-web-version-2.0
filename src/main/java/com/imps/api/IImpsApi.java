package com.imps.api;

import com.imps.model.ImpsRequest;
import com.imps.model.ImpsResponse;

public interface IImpsApi {

	ImpsResponse initiateP2ATransfer(ImpsRequest dto);

	ImpsResponse processP2ATransfer(ImpsRequest dto);

	ImpsResponse responseP2ATransfer(ImpsResponse response);

}
