package com.imps.validation;

import com.imps.model.ImpsRequest;
import com.imps.model.error.ImpsError;
import com.msscard.validation.CommonValidation;

public class ImpsValidation {

	public static ImpsError checkP2AError(ImpsRequest dto) {
		ImpsError error= new ImpsError();
		boolean valid=true; 

		if (CommonValidation.isNull(dto.getAccountNo())) {
			error.setMessage("Please enter the Account number");
			valid = false;}
		
		if (CommonValidation.isNull(dto.getIfscNo())) {
			error.setMessage("Please enter the IFSC number");
			valid = false;}
		
		if (CommonValidation.isNull(dto.getAmount())) {
			error.setMessage("Please enter the Amount");
			valid = false;}

		if (!dto.getAccountNo().matches(dto.getConfirmAcountNo())) {
			error.setMessage("Account number mismatch.");
			valid = false;
		}
		error.setValid(valid);
		return error;
		
	}

}
